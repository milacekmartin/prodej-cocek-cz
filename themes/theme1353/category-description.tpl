{if $page_name =='category'}
    {if isset($category)}
        {if $category->id && $category->active}
            <h1 class="page-heading{if (isset($subcategories) && !$products) || (isset($subcategories) && $products) || !isset($subcategories) && $products} product-listing{/if} mobile-heading hidden">
                <span class="cat-name">{$category->name|escape:'html':'UTF-8'}{if isset($categoryNameComplement)}&ensp;{$categoryNameComplement|escape:'html':'UTF-8'}{/if}</span>
            </h1>
            {if $scenes || $category->description || $category->id_image}
                <div class="content_scene_cat">
                    <div class="container">
                        {if $scenes}
                            <div class="content_scene">
                                <!-- Scenes -->
                                {include file="$tpl_dir./scenes.tpl" scenes=$scenes}
                                {if $category->description}
                                    <div class="cat_desc rte">
                                        {if Tools::strlen($category->description) > 350}
                                            <div id="category_description_short">{$description_short}</div>
                                            <div id="category_description_full" class="unvisible">{$category->description}</div>
                                            <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
                                        {else}
                                            <div>{$category->description}</div>
                                        {/if}
                                    </div>
                                {/if}
                            </div>
                        {else}
                            <!-- Category image -->
                            <div class="content_scene_cat_bg row">
                                {if $category->description}
                                    <div class="cat_desc col-xs-12 col-sm-7 col-lg-6">
                    <span class="category-name">
                      {strip}
                          {$category->name|escape:'html':'UTF-8'}
                          {if isset($categoryNameComplement)}
                              {$categoryNameComplement|escape:'html':'UTF-8'}
                          {/if}
                      {/strip}
                    </span>
                                        {if Tools::strlen($category->description) > 350}
                                            <div id="category_description_short" class="rte">{$description_short}</div>
                                            <div id="category_description_full" class="unvisible rte">{$category->description}</div>
                                            <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more" title="{l s='More'}">{l s='More'}</a>
                                        {else}
                                            <div class="rte">{$category->description}</div>
                                        {/if}
                                    </div>
                                {/if}
                                {if $category->id_image}
                                    <div class="category-image hidden-xs col-xs-12 col-sm-5 col-lg-6">
                                        <span class="cat-preloader"></span>
                                        <div class="category-image-wrap">
                                            <div class="category-parallax">
                                                <img class="img-responsive" src="{$link->getCatImageLink($category->link_rewrite, $category->id_image, 'tm_home_default')|escape:'html':'UTF-8'}" alt="{$category->name|escape:'html':'UTF-8'}"/>
                                            </div>
                                        </div>
                                    </div>
                                {/if}
                            </div>
                        {/if}
                    </div>
                </div>
            {/if}
        {/if}
    {/if}
{/if}