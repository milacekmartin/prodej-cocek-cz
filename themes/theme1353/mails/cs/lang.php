<?php

global $_LANGMAIL;
$_LANGMAIL = array();
$_LANGMAIL['Welcome!'] = 'Děkuejem za vytvoření zákaznického účtu.';
$_LANGMAIL['Process the payment of your order'] = 'Zpracování Vaší platby';
$_LANGMAIL['Message from contact form'] = 'Zpráva z kontaktního formuláře';
$_LANGMAIL['Your message has been correctly sent #ct%1$s #tc%2$s'] = 'Vaše zpráva byla v pořádku odeslána #ct%1$s #tc%2$s ';
$_LANGMAIL['Your message has been correctly sent'] = 'Vaše zpráva byla v pořádku odeslána';
$_LANGMAIL['New credit slip regarding your order'] = 'Nový slevový kupón k Vaší objednávce';
$_LANGMAIL['The virtual product that you bought is available for download'] = 'Produkt, který jste si koupili je připraven ke stažení';
$_LANGMAIL['Your new password'] = 'Vaše nové heslo';
$_LANGMAIL['Fwd: Customer message'] = 'Fwd: Zpráva zákazníka';
$_LANGMAIL['Your guest account has been transformed into a customer account'] = 'Váš návštěvnický účet byl převeden na zákaznický účet';
$_LANGMAIL['Package in transit'] = 'Zásilka na cestě';
$_LANGMAIL['Log: You have a new alert from your shop'] = 'Log: Máte nový záznam v logu z Vašeho obchodu';
$_LANGMAIL['Your order has been changed'] = 'Změna objednávky na Prodej-čoček.cz';
$_LANGMAIL['Order confirmation'] = 'Potvrzení objednávky';
$_LANGMAIL['Message from a customer'] = 'Zpráva od zákazníka';
$_LANGMAIL['New message regarding your order'] = 'Nová zpráva k objednávce';
$_LANGMAIL['Your order return status has changed'] = 'Stav vratky objednávky byl změněn';
$_LANGMAIL['Password query confirmation'] = 'Potvrzení požadavku na nové heslo';
$_LANGMAIL['An answer to your message is available #ct%1$s #tc%2$s'] = 'Je k dispozici odpověď na vaší zprávu, #ct%1$s #tc%2$s';
$_LANGMAIL['New voucher for your order #%s'] = 'Nový slevový kupón k vaší objednávce %';
$_LANGMAIL['New voucher for your order %s'] = 'Nový slevový kupón k vaší objednávce %';
$_LANGMAIL['Newsletter confirmation'] = 'Potvrzení přihlášky k odběru novinek';
$_LANGMAIL['Email verification'] = 'Potvrzení e-mailu';
$_LANGMAIL['Newsletter voucher'] = 'Odběr novinek – slevový kupón';
$_LANGMAIL['%1$s sent you a link to %2$s'] = '%1$s Vám poslal odkaz %2$s';

?>