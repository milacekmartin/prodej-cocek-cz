$(document).ready(function () {
    countItemsCross();
    if ($('#crossselling_list_car').length && !!$.prototype.bxSlider) {
        crosseling_slider = $('#crossselling_list_car').bxSlider({
            minSlides: cross_carousel_items,
            maxSlides: cross_carousel_items,
            slideWidth: 500,
            slideMargin: 30,
            pager: false,
            nextText: '',
            prevText: '',
            moveSlides: 1,
            infiniteLoop: false,
            hideControlOnEnd: true,
            responsive: true,
            useCSS: false,
            autoHover: false,
            speed: 500,
            pause: 3000,
            controls: true,
            autoControls: false
        });
    }
});
$(window).resize(function () {
    if ($('#crossselling_list_car').length) {
        resizeCarouselCross()
    }
});
function resizeCarouselCross() {
    countItemsCross();
    crosseling_slider.reloadSlider({
        minSlides: cross_carousel_items,
        maxSlides: cross_carousel_items,
        slideWidth: 500,
        slideMargin: 30,
        pager: false,
        nextText: '',
        prevText: '',
        moveSlides: 1,
        infiniteLoop: false,
        hideControlOnEnd: true,
        responsive: true,
        useCSS: false,
        autoHover: false,
        speed: 500,
        pause: 3000,
        controls: true,
        autoControls: false
    });
}
function countItemsCross() {
    if ($('#crossselling').width() < 400) {
        cross_carousel_items = 1;
    }
    if ($('#crossselling').width() > 400) {
        cross_carousel_items = 2;
    }
    if ($('#crossselling').width() >= 550) {
        cross_carousel_items = 3;
    }
    if ($('#crossselling').width() >= 900) {
        cross_carousel_items = 4;
    }
    if ($('#crossselling').width() >= 1200) {
        cross_carousel_items = 5;
    }
    if ($('#crossselling').width() >= 1500) {
        cross_carousel_items = 6;
    }
    if ($('#crossselling').width() >= 1800) {
        cross_carousel_items = 7;
    }
    if ($('#crossselling').width() >= 2048) {
        cross_carousel_items = 8;
    }
}
