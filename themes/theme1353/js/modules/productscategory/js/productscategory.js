$(document).ready(function () {
    countItemsCategory();
    if ($('#bxslider1').length && !!$.prototype.bxSlider) {
        productscategory_slider = $('#bxslider1').bxSlider({
            minSlides: cross_carousel_items,
            maxSlides: cross_carousel_items,
            slideWidth: 500,
            slideMargin: 30,
            pager: false,
            nextText: '',
            prevText: '',
            moveSlides: 1,
            infiniteLoop: false,
            hideControlOnEnd: true,
            responsive: true,
            useCSS: false,
            autoHover: false,
            speed: 500,
            pause: 3000,
            controls: true,
            autoControls: false
        });
    }
});
$(window).resize(function () {
    if ($('#bxslider1').length) {
        resizeCarouselCategory()
    }
});
function resizeCarouselCategory() {
    countItemsCategory();
    productscategory_slider.reloadSlider({
        minSlides: category_carousel_items,
        maxSlides: category_carousel_items,
        slideWidth: 500,
        slideMargin: 30,
        pager: false,
        nextText: '',
        prevText: '',
        moveSlides: 1,
        infiniteLoop: false,
        hideControlOnEnd: true,
        responsive: true,
        useCSS: false,
        autoHover: false,
        speed: 500,
        pause: 3000,
        controls: true,
        autoControls: false
    });
}
function countItemsCategory() {
    if ($('#productscategory_list').width() < 400) {
        category_carousel_items = 1;
    }
    if ($('#productscategory_list').width() > 400) {
        category_carousel_items = 2;
    }
    if ($('#productscategory_list').width() >= 550) {
        category_carousel_items = 3;
    }
    if ($('#productscategory_list').width() >= 900) {
        category_carousel_items = 4;
    }
    if ($('#productscategory_list').width() >= 1200) {
        category_carousel_items = 5;
    }
    if ($('#productscategory_list').width() >= 1500) {
        category_carousel_items = 6;
    }
    if ($('#productscategory_list').width() >= 1800) {
        category_carousel_items = 7;
    }
    if ($('#productscategory_list').width() >= 2048) {
        category_carousel_items = 8;
    }
}