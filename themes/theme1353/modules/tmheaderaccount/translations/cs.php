<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_7cf71ba7eb98833640624bc9ec63d30e'] = 'Blok Můj účet';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_3d01f28f9b86f0fe9bbb9bc3a71fce5b'] = 'Zobrazit informace o zákaznickém účtu v záhlaví webu.';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_a0623b78a5f2cfe415d9dbbd4428ea40'] = 'Můj účet';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_b6d4223e60986fa4c9af77ee5f7149c5'] = 'Přihlásit';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_74ecd9234b2a42ca13e775193f391833'] = 'Moje objednávky';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_5973e925605a501b18e48280f04f0347'] = 'Moje vrácené zboží';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_89080f0eedbd5491a93157930f1e45fc'] = 'Moje vrácené zboží';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_9132bc7bac91dd4e1c453d4e96edf219'] = 'Moje kreditní body';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_e45be0a0d4a0b62b15694c1a631e6e62'] = 'Moje adresa';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_b4b80a59559e84e8497f746aac634674'] = 'Správa osobních údajů';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_63b1ba91576576e6cf2da6fab7617e58'] = 'Osobní údaje';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_95d2137c196c7f84df5753ed78f18332'] = 'Moje vouchery';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_c87aacf5673fada1108c9f809d354311'] = 'Odhlásit se';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_b357b524e740bc85b9790a0712d84a30'] = 'Emailová adresa';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_dc647eb65e6711e155375218212b3964'] = 'Heslo';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_2fdfd506efea08144c0794c32ca8250a'] = 'Vytvořit účet';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_01a569ddc6cf67ddec2a683f0a5f5956'] = 'Zapomněli jste heslo?';
$_MODULE['<{tmheaderaccount}theme1353>tmheaderaccount_8e756404c01867410817b8601522e88c'] = 'Přihlašovací formulář';
