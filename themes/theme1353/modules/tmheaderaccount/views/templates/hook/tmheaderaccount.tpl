{*
* 2002-2015 TemplateMonster
*
* TM Header Account Block
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0

* @author     TemplateMonster (Alexander Grosul)
* @copyright  2002-2015 TemplateMonster
* @license    http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div id="header-login">
    <div class="current header_user_info">
        <a {if !$is_logged} id="headeraccount-link" {/if} href="#" onclick="return false;">{if $is_logged}{l s='Your Account' mod='tmheaderaccount'}{else}{l s='Sign in' mod='tmheaderaccount'}{/if}</a>
    </div>
    {if $is_logged}
        <ul id="header-login-content" class="toogle_content">
            <li>
                <ul>
                    <li>
                        <a href="{$link->getPageLink('history', true)|escape:'htmlall':'UTF-8'}" title="{l s='My orders' mod='tmheaderaccount'}" rel="nofollow">{l s='My orders' mod='tmheaderaccount'}</a>
                    </li>
                    {if $returnAllowed}
                        <li>
                        <a href="{$link->getPageLink('order-follow', true)|escape:'htmlall':'UTF-8'}" title="{l s='My returns' mod='tmheaderaccount'}" rel="nofollow">{l s='My merchandise returns' mod='tmheaderaccount'}</a>
                        </li>{/if}
                    <li>
                        <a href="{$link->getPageLink('order-slip', true)|escape:'htmlall':'UTF-8'}" title="{l s='My credit slips' mod='tmheaderaccount'}" rel="nofollow">{l s='My credit slips' mod='tmheaderaccount'}</a>
                    </li>
                    <li>
                        <a href="{$link->getPageLink('addresses', true)|escape:'htmlall':'UTF-8'}" title="{l s='My addresses' mod='tmheaderaccount'}" rel="nofollow">{l s='My addresses' mod='tmheaderaccount'}</a>
                    </li>
                    <li>
                        <a href="{$link->getPageLink('identity', true)|escape:'htmlall':'UTF-8'}" title="{l s='Manage my personal information' mod='tmheaderaccount'}" rel="nofollow">{l s='My personal info' mod='tmheaderaccount'}</a>
                    </li>
                    {if $voucherAllowed}
                        <li>
                        <a href="{$link->getPageLink('discount', true)|escape:'htmlall':'UTF-8'}" title="{l s='My vouchers' mod='tmheaderaccount'}" rel="nofollow">{l s='My vouchers' mod='tmheaderaccount'}</a>
                        </li>{/if}
                    {if isset($HOOK_BLOCK_MY_ACCOUNT) && $HOOK_BLOCK_MY_ACCOUNT !=''}
                        {$HOOK_BLOCK_MY_ACCOUNT}
                    {/if}
                </ul>
                <p class="logout">
                    <a class="btn btn-default btn-sm" href="{$link->getPageLink('index')}?mylogout" title="{l s='Sign out' mod='tmheaderaccount'}" rel="nofollow">
                        <i class="fa fa-unlock left"></i>
                        {l s='Sign out' mod='tmheaderaccount'}
                    </a>
                </p>
            </li>
        </ul>
    {else}
        <div id="headeraccount-popup">
            <div class="popup-box">
                <span id="popup-close"></span>
                <h2>{l s='Login form' mod='tmheaderaccount'}</h2>
                <form action="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}" method="post" id="header_login_form">
                    <div class="popup-body">
                        <hr>
                        <div class="alert alert-info">Měli jste účet na starém eshopu? Váš účet zůstal zachován, ale pro bezpečnost vašich osobních dat Vás musíme požádat o <a href="{$link->getPageLink('password')|escape:'html':'UTF-8'}" title="{l s='Recover your forgotten password'}" rel="nofollow">obnovení hesla</a>.</div>
                        <hr>
                        <div id="create_header_account_error" class="alert alert-danger" style="display:none;"></div>
                        <div class="form_content clearfix">
                            <div class="form-group">
                                <input class="is_required validate account_input form-control" data-validate="isEmail" type="text" id="header-email" name="header-email" placeholder="{l s='Email address' mod='tmheaderaccount'}" value="{if isset($smarty.post.email)}{$smarty.post.email|stripslashes}{/if}"/>
                            </div>
                            <div class="form-group">
                                <span><input class="is_required validate account_input form-control" type="password" data-validate="isPasswd" id="header-passwd" name="header-passwd" placeholder="{l s='Password' mod='tmheaderaccount'}" value="{if isset($smarty.post.passwd)}{$smarty.post.passwd|stripslashes}{/if}" autocomplete="off"/></span>
                            </div>
                            <p class="submit">
                                <button type="button" id="HeaderSubmitLogin" name="HeaderSubmitLogin" class="btn btn-default btn-sm">
                                    <i class="fa fa-lock left"></i>
                                    {l s='Sign in' mod='tmheaderaccount'}
                                </button>
                            </p>
                            <div class="clearfix">
                                {hook h="displayHeaderLoginButtons"}
                            </div>
                        </div>
                    </div>
                    <div class="popup-footer">
                        <p>
                            <a href="{$link->getPageLink('my-account', true)|escape:'htmlall':'UTF-8'}" class="create btn btn-sm">{l s='Create an account' mod='tmheaderaccount'}</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    {/if}
</div>