{if $comment.id_smart_blog_comment != ''}
    <ul class="commentList">
        <li id="comment-{$comment.id_smart_blog_comment}" class="even">
            <div class="well">
                <div class="comment-content clearfix box">
                    <div class="box_aside">
                        <img class="avatar" alt="Avatar" src="{$modules_dir}/smartblog/images/avatar/avatar-author-default.jpg">
                    </div>

                    <div class="box_cnt__no_flow">
                        <div class="name">{$childcommnets.name}</div>

                        <div class="created">
                            <span itemprop="commentTime">{$childcommnets.created|date_format}</span>
                        </div>

                        <p class="content_comment">{$childcommnets.content}</p>

                        {if Configuration::get('smartenablecomment') == 1}
                            {if $comment_status == 1}
                                <div class="reply">
                                    <a onclick="return addComment.moveForm('comment-{$comment.id_smart_blog_comment}', '{$comment.id_smart_blog_comment}', 'respond', '{$smarty.get.id_post}')" class="comment-reply-link btn btn-default btn-sm">
                                        <span class="fa fa-angle-right"></span><span>{l s='Reply' mod='smartblog'}</span></a>
                                </div>
                            {/if}
                        {/if}
                    </div>
                </div>
            </div>

            {if isset($childcommnets.child_comments)}
                {foreach from=$childcommnets.child_comments item=comment}
                    {if isset($childcommnets.child_comments)}
                        {include file="./comment_loop.tpl" childcommnets=$comment}
                        {$i=$i+1}
                    {/if}
                {/foreach}
            {/if}
        </li>
    </ul>
{/if} 