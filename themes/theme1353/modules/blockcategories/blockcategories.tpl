{if $blockCategTree && $blockCategTree.children|@count}
    <!-- Block categories module -->
    <section id="categories_block_left" class="block">
        <div class="block_content">
            <ul class="tree {if $isDhtml}dhtml{/if}">
                {foreach from=$blockCategTree.children item=child name=blockCategTree}
                    {if $smarty.foreach.blockCategTree.last}
                        {include file="$branche_tpl_path" node=$child last='true'}
                    {else}
                        {include file="$branche_tpl_path" node=$child}
                    {/if}
                {/foreach}
            </ul>
        </div>
    </section>
    <!-- /Block categories module -->
{/if}