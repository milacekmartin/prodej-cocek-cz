<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{googleshopping}theme1353>googleshopping_d4cb75caae5a73ac6b01171816b33bfb'] = 'Nákupy Google';
$_MODULE['<{googleshopping}theme1353>googleshopping_646cec48b6a30036d7e7bf664be1ea12'] = 'Používejte data z obchodu PrestaShop k propagaci svých produktů na Googlu.';
$_MODULE['<{googleshopping}theme1353>googleshopping_99e487074de370fa1d3d6ae6cfd03ec3'] = 'Odinstalace modulu způsobí, že nabídky v Nákupech Google vyprší.';
$_MODULE['<{googleshopping}theme1353>configure_cab22fb8bb457662802b30f547fd17aa'] = '{link_start}Nákupy Google{link_end} pomáhají využít sílu zákaznického záměru k oslovení správných lidí s reklamami na relevantní produkty ve správnou chvíli. Použijte tento modul k nahrání vašich dat z obchodu a produktových dat do služby Google Merchant Center a zpřístupněte je v Nákupech Google a v dalších službách Google, čímž získáte možnost oslovit miliony nových zákazníků, kteří hledají to, co vy prodáváte.';
$_MODULE['<{googleshopping}theme1353>configure_09ee2625250f96439812889e4d5fb88d'] = 'Propojte svůj účet PrestaShop se službou Merchant Center a začněte importovat svá produktová data prostřednictvím rozhraní API obchodu. Až budou vaše účty ve službách PrestaShop a Merchant Center propojeny, jakékoli aktualizace provedené v obchodě PrestaShop se automaticky promítnou též do služby Merchant Center.';
$_MODULE['<{googleshopping}theme1353>configure_6c75de3331637454dbba750e7290e177'] = 'Začněte kliknutím na možnost \"Zaregistrovat se\" níže. Automaticky dojde k vytvoření nového účtu API PrestaShop a přihlašovací údaje a informace o obchodu budou přes zabezpečené připojení odeslány do služby Merchant Center. Postupujte podle pokynů k registraci pro vytvoření účtu ve službě Merchant Center – pokud nevytvoříte nový účet, budou vaše informace API po 60 dnech vymazány. {link_start}Další informace{link_end}';
$_MODULE['<{googleshopping}theme1353>configure_9506f0fd0f7f1b07960b15b4c9e68d1a'] = 'Spustit';
