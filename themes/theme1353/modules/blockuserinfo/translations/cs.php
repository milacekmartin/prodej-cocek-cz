<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_a2e9cd952cda8ba167e62b25a496c6c1'] = 'Blok s informacemi o uživateli';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_970a31aa19d205f92ccfd1913ca04dc0'] = 'Přidá blok zobrazující informace o uživateli.';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_0c3bf3014aafb90201805e45b5e62881'] = 'Zobrazit můj nákupní košík';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_a85eba4c6c699122b2bb1387ea4813ad'] = 'Košík';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_deb10517653c255364175796ace3553f'] = 'x';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_068f80c7519d0528fb08e82137a72131'] = 'Produkty';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_9e65b51e82f2a9b9f72ebe3e083582bb'] = '(prázdný)';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_2cbfb6731610056e1d0aaacde07096c1'] = 'Zobrazit můj zákaznický účet';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_a0623b78a5f2cfe415d9dbbd4428ea40'] = 'Váš účet';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_83218ac34c1834c26781fe4bde918ee4'] = 'Vítejte';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_4b877ba8588b19f1b278510bf2b57ebb'] = 'Odhlásit se';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_c87aacf5673fada1108c9f809d354311'] = 'Odhlásit';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_d4151a9a3959bdd43690735737034f27'] = 'Přihlášení k vašemu zákaznickému účtu';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_b6d4223e60986fa4c9af77ee5f7149c5'] = 'Přihlásit se';
$_MODULE['<{blockuserinfo}theme1353>nav_2cbfb6731610056e1d0aaacde07096c1'] = 'Zobrazit můj zákaznický účet';
$_MODULE['<{blockuserinfo}theme1353>nav_4b877ba8588b19f1b278510bf2b57ebb'] = 'Odhlásit se';
$_MODULE['<{blockuserinfo}theme1353>nav_c87aacf5673fada1108c9f809d354311'] = 'Odhlásit';
$_MODULE['<{blockuserinfo}theme1353>nav_d4151a9a3959bdd43690735737034f27'] = 'Přihlášení k vašemu zákaznickému účtu';
$_MODULE['<{blockuserinfo}theme1353>nav_b6d4223e60986fa4c9af77ee5f7149c5'] = 'Přihlásit se';
$_MODULE['<{blockuserinfo}theme1353>blockuserinfo_b145abfd6b2f88971d725cbd94a5879f'] = 'Přihlášení k vašemu zákaznickému účtu';
$_MODULE['<{blockuserinfo}theme1353>nav_a0623b78a5f2cfe415d9dbbd4428ea40'] = 'Váš účet';
