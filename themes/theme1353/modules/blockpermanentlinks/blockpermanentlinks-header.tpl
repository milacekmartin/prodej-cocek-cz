<!-- Block permanent links module HEADER -->
<ul id="header_links">
    <!--li id="header_link_wishlist">
        <a {if $page_name =='module-blockwishlist-mywishlist'}class="active"{/if} href="{$link->getModuleLink ('blockwishlist', 'mywishlist', array(), true)|escape:'html':'UTF-8'}" title="{l s='wishlist' mod='blockpermanentlinks'}">{l s='wishlist' mod='blockpermanentlinks'}</a>
    </li-->
    <!--li id="header_link_contact">
        <a {if $page_name =='contact'}class="active"{/if} href="{$link->getPageLink('contact', true)|escape:'html'}" title="{l s='contact' mod='blockpermanentlinks'}">{l s='contact' mod='blockpermanentlinks'}</a>
    </li-->
    {* rucne zadany link ke kontaktni strance v CMS *}
    <li id="header_link_contact">
        <a {if $page_name =='contact'}class="active"{/if} href="/content/5-kontakt" title="{l s='contact' mod='blockpermanentlinks'}">{l s='contact' mod='blockpermanentlinks'}</a>
    </li>
    <li id="header_link_sitemap">
        <a {if $page_name =='sitemap'}class="active"{/if} href="{$link->getPageLink('sitemap')|escape:'html'}" title="{l s='sitemap' mod='blockpermanentlinks'}" style="text-transform: none;">{l s='sitemap' mod='blockpermanentlinks'}</a>
    </li>
</ul><!-- /Block permanent links module HEADER -->