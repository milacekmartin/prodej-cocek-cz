<!--
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#
--><h1 class="page-heading">
    {l s='Order summation' mod='PrestaAdapter'}
</h1>


{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

<p>
    {$methodName}
    <img src="{$uniModulLogo}" alt="{$methodName}" style="float:right; margin: 0px 10px 5px 0px;"/>{$minilogo}

<h3>{l s='Payment rejected' mod='PrestaAdapter'}</h3>


<p>
    {l s='Your payment has been rejected' mod='PrestaAdapter'}
    <br/>
    {$errorMessage}
</p>

<p class="cart_navigation">
    <a href="{$link->getPageLink('order.php', true)}?step=3" class="button_large">{l s='Try again' mod='PrestaAdapter'}</a>
</p>

