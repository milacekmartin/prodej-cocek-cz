<!--
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#
-->

{foreach from=$submetTempl item=item}
    <div class="row">
        <div class="col-xs-12">
            <p class="payment_module">
                <a href="{$item.redirUrl}" title="{$item.methodName}" class="bankwire" style="padding-left:1em; padding-right:1em; background-image:none">
                    <img src="{$item.uniModulLogo}" alt="{$item.methodName}" width="75"/>
                    {$item.methodName}
                </a>
            </p>
        </div>
    </div>
{/foreach}
