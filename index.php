<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

// Report all errors
error_reporting(E_ALL);
ini_set("display_errors", 1);
//ini_set('memory_limit', -1);

// Tracy debugger
if (is_file('tracy.phar')) {

    require_once('tracy.phar');
    //die('tracy.phar');

    \Tracy\Debugger::enable(['89.103.246.45'], __DIR__ . "/log/");
}

require(dirname(__FILE__) . '/config/config.inc.php');
Dispatcher::getInstance()->dispatch();