<?php
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#

$dict['cs']['payment_method_name'] = 'Platba kartou online (GPWebPay)';
$dict['en']['payment_method_name'] = 'Online credit card payment (GPWebPay)';
$dict['sk']['payment_method_name'] = 'Platba kartou online (GPWebPay)';
$dict['ru']['payment_method_name'] = 'Оплата кредитной картой онлайн (GPWebPay)';
$dict['de']['payment_method_name'] = 'Online-Kreditkartenzahlung (GpWebPay)';
$dict['es']['payment_method_name'] = 'Pago con tarjeta de crédito en línea (GPWebPay)';

$dict['cs']['orderDetailGwOrdNum'] = 'Číslo transakce GPWebPay';
$dict['en']['orderDetailGwOrdNum'] = 'GPWebPay transaction number';
$dict['sk']['orderDetailGwOrdNum'] = 'Číslo transakce GPWebPay';
$dict['ru']['orderDetailGwOrdNum'] = 'GPWebPay номер операции';
$dict['de']['orderDetailGwOrdNum'] = 'Transaktionnummer GPWebPay';
$dict['es']['orderDetailGwOrdNum'] = 'Número de transacción GPWebPay ';

$dict['cs']['infoBoxTitle'] = 'Přijímáme karty';
$dict['en']['infoBoxTitle'] = 'Accepted cards';
$dict['sk']['infoBoxTitle'] = 'Prijímáme karty';
$dict['ru']['infoBoxTitle'] = 'Принимаемые карты';
$dict['de']['infoBoxTitle'] = 'Wir akzeptieren Kreditkarten';
$dict['es']['infoBoxTitle'] = 'Tarjetas aceptadas';


// chyby

$dict['cs']['GPWP_E_3DSECURE'] = 'Chyba platby: Vaše karta nepodporuje online platbu. Kontaktujte vydavatelskou banku vaší karty.';
$dict['en']['GPWP_E_3DSECURE'] = 'Payment error: Your card does not support online payment 3D Secure. Contact your card issuer bank.';
$dict['sk']['GPWP_E_3DSECURE'] = 'Chyba platby: Vaše karta nepodporuje online platbu. Kontaktujte vydavatelskou banku vaší karty.';
$dict['ru']['GPWP_E_3DSECURE'] = 'Ошибка: Ваша карта не поддерживает оплату онлайн. Свяжитесь с банком, выдавшим вашу карту.';
$dict['de']['GPWP_E_3DSECURE'] = 'Zahlungsfehler: Ihre Karte unterstütz nicht die Online-Zahlung. Kontaktieren Sie Ihre kartenausgebende Bank.';
$dict['es']['GPWP_E_3DSECURE'] = 'Error de pago: Tu tarjeta no admite el pago en línea 3D Secure. Contacta con el banco emisor de la tarjeta.';

$dict['cs']['GPWP_E_BLOCKED'] = 'Chyba platby: Vaše karta je blokována nebo zamítnuta. Kontaktujte vydavatelskou banku vaší karty.';
$dict['en']['GPWP_E_BLOCKED'] = 'Payment error: Your card is blocked or rejected. Contact your card issuer bank.';
$dict['sk']['GPWP_E_BLOCKED'] = 'Chyba platby: Vaše karta je blokována nebo zamítnuta. Kontaktujte vydavatelskou banku vaší karty.';
$dict['ru']['GPWP_E_BLOCKED'] = 'Ошибка: Ваша карта заблокирована или отклонена. Свяжитесь с банком, выдавшим вашу карту.';
$dict['de']['GPWP_E_BLOCKED'] = 'Zahlungsfehler: Ihre Karte ist gesperrt oder abgelehnt. Kontaktieren Sie Ihre kartenausgebende Bank.';
$dict['es']['GPWP_E_BLOCKED'] = 'Error de pago: Tu tarjeta está bloqueada o es rechazada. Contacta con el banco emisor de la tarjeta.';

$dict['cs']['GPWP_E_LIMIT'] = 'Chyba platby: Limit karty překročen, nedostatek peněz na účtu nebo karta nepodporuje online platbu. Kontaktujte vydavatelskou banku vaší karty.';
$dict['en']['GPWP_E_LIMIT'] = 'Payment error: Your card limit exceeded, not enough money, or card does not support online payment. Contact your card issuer bank.';
$dict['sk']['GPWP_E_LIMIT'] = 'Chyba platby: Limit karty překročen, nedostatek peněz na účtu nebo karta nepodporuje online platbu. Kontaktujte vydavatelskou banku vaší karty.';
$dict['ru']['GPWP_E_LIMIT'] = 'Ошибка: Лимит карты превышен, недостаток средств на счете или карта не поддерживает онлайн платежи. Свяжитесь с банком, выдавшим вашу карту.';
$dict['de']['GPWP_E_LIMIT'] = 'Zahlungsfehler: Limit der Karte überschritten, wenig Geld auf dem Konto oder Ihre Karte unterstütz nicht die Online-Zahlung. Kontaktieren Sie Ihre kartenausgebende Bank.';
$dict['es']['GPWP_E_LIMIT'] = 'Error de pago: Límite de la tarjeta excedido, no hay suficiente dinero o la tarjeta no admite el pago en línea. Contacta con el banco emisor de la tarjeta.';

$dict['cs']['GPWP_E_CANCELED'] = 'Platba zrušena uživatelem.';
$dict['en']['GPWP_E_CANCELED'] = 'Payment canceled by customer.';
$dict['sk']['GPWP_E_CANCELED'] = 'Platba zrušena uživatelem.';
$dict['ru']['GPWP_E_CANCELED'] = 'Оплата отменена пользователем.';
$dict['de']['GPWP_E_CANCELED'] = 'Zahlung storniert vom Karteninhaber.';
$dict['es']['GPWP_E_CANCELED'] = 'Pago cancelado por el cliente.';

$dict['cs']['GPWP_E_TECHNICAL'] = 'Chyba platby: Technický problém. Kontaktujte podporu eshopu.';
$dict['en']['GPWP_E_TECHNICAL'] = 'Payment error: There is a technical problem. Contact the website support.';
$dict['sk']['GPWP_E_TECHNICAL'] = 'Chyba platby: Technický problém. Kontaktujte podporu eshopu.';
$dict['ru']['GPWP_E_TECHNICAL'] = 'Ошибка: Техническая проблема. Свяжитесь со службой поддержки сайта.';
$dict['de']['GPWP_E_TECHNICAL'] = 'Zahlungsfehler: Ein technisches Problem. Kontaktieren Sie die Unterstützung der Webseite.';
$dict['es']['GPWP_E_TECHNICAL'] = 'Error de pago: Hay un problema técnico. Contacta con el sitio web de asistencia.';


// admin

$dict['cs']['urlMuzoCreateOrder'] = 'Adresa brány GPWebPay';
$dict['en']['urlMuzoCreateOrder'] = 'GPWebPay gateway URL';
$dict['sk']['urlMuzoCreateOrder'] = 'Adresa brány GPWebPay';
$dict['ru']['urlMuzoCreateOrder'] = 'GPWebPay URL шлюз';
$dict['es']['urlMuzoCreateOrder'] = 'URL de la plataforma GPWebPay';

$dict['cs']['merchantNumberCZK'] = 'Číslo obchodníka pro CZK';
$dict['en']['merchantNumberCZK'] = 'Merchant Number for CZK';
$dict['sk']['merchantNumberCZK'] = 'Číslo obchodníka pro CZK';
$dict['ru']['merchantNumberCZK'] = 'Торговец номер для CZK';
$dict['es']['merchantNumberCZK'] = 'Número de comerciante para CZK';

$dict['cs']['merchantNumberEUR'] = 'Číslo obchodníka pro EUR';
$dict['en']['merchantNumberEUR'] = 'Merchant Number for EUR';
$dict['sk']['merchantNumberEUR'] = 'Číslo obchodníka pro EUR';
$dict['ru']['merchantNumberEUR'] = 'Торговец номер для EUR';
$dict['es']['merchantNumberEUR'] = 'Número de comerciante para EUR';

$dict['cs']['merchantNumberUSD'] = 'Číslo obchodníka pro USD';
$dict['en']['merchantNumberUSD'] = 'Merchant Number for USD';
$dict['sk']['merchantNumberUSD'] = 'Číslo obchodníka pro USD';
$dict['ru']['merchantNumberUSD'] = 'Торговец номер для USD';
$dict['es']['merchantNumberUSD'] = 'Número de comerciante para USD';

$dict['cs']['merchantNumberGBP'] = 'Číslo obchodníka pro GBP';
$dict['en']['merchantNumberGBP'] = 'Merchant Number for GBP';
$dict['sk']['merchantNumberGBP'] = 'Číslo obchodníka pro GBP';
$dict['ru']['merchantNumberGBP'] = 'Торговец номер для GBP';
$dict['es']['merchantNumberGBP'] = 'Número de comerciante para GBP';

$dict['cs']['merchantNumberPLN'] = 'Číslo obchodníka pro PLN';
$dict['en']['merchantNumberPLN'] = 'Merchant Number for PLN';
$dict['sk']['merchantNumberPLN'] = 'Číslo obchodníka pro PLN';
$dict['ru']['merchantNumberPLN'] = 'Торговец номер для PLN';
$dict['es']['merchantNumberPLN'] = 'Número de comerciante para PLN';

$dict['cs']['merchantNumberHUF'] = 'Číslo obchodníka pro HUF';
$dict['en']['merchantNumberHUF'] = 'Merchant Number for HUF';
$dict['sk']['merchantNumberHUF'] = 'Číslo obchodníka pro HUF';
$dict['ru']['merchantNumberHUF'] = 'Торговец номер для HUF';
$dict['es']['merchantNumberHUF'] = 'Número de comerciante para HUF';

$dict['cs']['merchantNumberRUB'] = 'Číslo obchodníka pro RUB';
$dict['en']['merchantNumberRUB'] = 'Merchant Number for RUB';
$dict['sk']['merchantNumberRUB'] = 'Číslo obchodníka pro RUB';
$dict['ru']['merchantNumberRUB'] = 'Торговец номер для RUB';
$dict['es']['merchantNumberRUB'] = 'Número de comerciante para RUB';

$dict['cs']['publicKeyFile'] = 'Veřejný klíč GPWebPay';
$dict['en']['publicKeyFile'] = 'GPWebPay public key file';
$dict['sk']['publicKeyFile'] = 'Veřejný klíč GPWebPay';
$dict['ru']['publicKeyFile'] = 'GPWebPay файл с открытым ключом';
$dict['es']['publicKeyFile'] = 'Archivo de clave pública de GPWebPay';

$dict['cs']['privateKeyFile'] = 'Soubor privátního klíče';
$dict['en']['privateKeyFile'] = 'Merchant private key file';
$dict['sk']['privateKeyFile'] = 'Soubor privátního klíče';
$dict['ru']['privateKeyFile'] = 'Торговец файл закрытый ключ';
$dict['es']['privateKeyFile'] = 'Archivo de clave privada del comerciante';

$dict['cs']['privateKeyPass'] = 'Heslo privátního klíče';
$dict['en']['privateKeyPass'] = 'Merchant private key password';
$dict['sk']['privateKeyPass'] = 'Heslo privátního klíče';
$dict['ru']['privateKeyPass'] = 'Торговец пароль на секретный ключ';
$dict['es']['privateKeyPass'] = 'Contraseña de clave privada del comerciante';

$dict['cs']['depositFlag'] = 'Převod peněz';
$dict['en']['depositFlag'] = 'Deposit flag';
$dict['sk']['depositFlag'] = 'Převod peněz';
$dict['ru']['depositFlag'] = 'Перевод денег';
$dict['es']['depositFlag'] = 'Bandera del depósito';


$dict['cs']['deposit_1'] = 'převést ihned';
$dict['en']['deposit_1'] = 'immediately';
$dict['sk']['deposit_1'] = 'převést ihned';
$dict['ru']['deposit_1'] = 'немедленно';
$dict['es']['deposit_1'] = 'inmediatamente';


$dict['cs']['deposit_0'] = 'pouze předautorizace';
$dict['en']['deposit_0'] = 'authorize only';
$dict['sk']['deposit_0'] = 'pouze předautorizace';
$dict['ru']['deposit_0'] = 'разрешить только';
$dict['es']['deposit_0'] = 'autorizar solo';


$dict['cs']['gwOrderNumberOffset'] = 'Číslo první platby na platební bráně (nastavit na 1000 a pak už neměnit)';
$dict['en']['gwOrderNumberOffset'] = 'Gateway order number offset (recommended value 1000, do not change after it is once set)';
$dict['sk']['gwOrderNumberOffset'] = 'Číslo první platby na platební bráně (nastavit na 1000 a pak už neměnit)';
$dict['ru']['gwOrderNumberOffset'] = 'Шлюз номер заказа смещение (рекомендуемое значение 1000, не изменяются после однократного установить)';
$dict['es']['gwOrderNumberOffset'] = 'Offset del número de pedido de plataforma (valor recomendado 1000, no cambiar después de configurado)';

/*
$dict['cs'][''] = '';
$dict['en'][''] = '';

*/


$dict['cs']['orderStatusSuccessfull'] = 'Stav objednávky po úspěšném zaplacení';
$dict['en']['orderStatusSuccessfull'] = 'Order state after successful payment';
$dict['ru']['orderStatusSuccessfull'] = 'Состояние заказа после успешной оплаты';
$dict['es']['orderStatusSuccessfull'] = 'Estado del pedido después del pago aceptado';
