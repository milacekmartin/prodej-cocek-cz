-- phpMyAdmin SQL Dump
-- version 3.2.5
-- http://www.phpmyadmin.net
--
-- Počítač: z-web-3.zcom.cz:3306
-- Vygenerováno: Pondělí 03. dubna 2017, 15:15
-- Verze MySQL: 5.5.52
-- Verze PHP: 5.2.6-1+lenny16

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `test_prodej_cocek_cz`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_access`
--

CREATE TABLE IF NOT EXISTS `ps_access` (
  `id_profile` INT(10) UNSIGNED NOT NULL,
  `id_tab`     INT(10) UNSIGNED NOT NULL,
  `view`       INT(11)          NOT NULL,
  `add`        INT(11)          NOT NULL,
  `edit`       INT(11)          NOT NULL,
  `delete`     INT(11)          NOT NULL,
  PRIMARY KEY (`id_profile`, `id_tab`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_access`
--

INSERT INTO `ps_access` (`id_profile`, `id_tab`, `view`, `add`, `edit`, `delete`) VALUES
  (1, 0, 1, 1, 1, 1),
  (1, 1, 1, 1, 1, 1),
  (1, 5, 1, 1, 1, 1),
  (1, 7, 1, 1, 1, 1),
  (1, 9, 1, 1, 1, 1),
  (1, 10, 1, 1, 1, 1),
  (1, 11, 1, 1, 1, 1),
  (1, 13, 1, 1, 1, 1),
  (1, 14, 1, 1, 1, 1),
  (1, 15, 1, 1, 1, 1),
  (1, 16, 1, 1, 1, 1),
  (1, 19, 1, 1, 1, 1),
  (1, 20, 1, 1, 1, 1),
  (1, 21, 1, 1, 1, 1),
  (1, 22, 1, 1, 1, 1),
  (1, 23, 1, 1, 1, 1),
  (1, 24, 1, 1, 1, 1),
  (1, 25, 1, 1, 1, 1),
  (1, 26, 1, 1, 1, 1),
  (1, 27, 1, 1, 1, 1),
  (1, 28, 1, 1, 1, 1),
  (1, 29, 1, 1, 1, 1),
  (1, 31, 1, 1, 1, 1),
  (1, 32, 1, 1, 1, 1),
  (1, 33, 1, 1, 1, 1),
  (1, 34, 1, 1, 1, 1),
  (1, 35, 1, 1, 1, 1),
  (1, 36, 1, 1, 1, 1),
  (1, 38, 1, 1, 1, 1),
  (1, 39, 1, 1, 1, 1),
  (1, 40, 1, 1, 1, 1),
  (1, 41, 1, 1, 1, 1),
  (1, 42, 1, 1, 1, 1),
  (1, 44, 1, 1, 1, 1),
  (1, 45, 1, 1, 1, 1),
  (1, 48, 1, 1, 1, 1),
  (1, 50, 1, 1, 1, 1),
  (1, 52, 1, 1, 1, 1),
  (1, 53, 1, 1, 1, 1),
  (1, 54, 1, 1, 1, 1),
  (1, 55, 1, 1, 1, 1),
  (1, 56, 1, 1, 1, 1),
  (1, 57, 1, 1, 1, 1),
  (1, 58, 1, 1, 1, 1),
  (1, 59, 1, 1, 1, 1),
  (1, 61, 1, 1, 1, 1),
  (1, 62, 1, 1, 1, 1),
  (1, 63, 1, 1, 1, 1),
  (1, 66, 1, 1, 1, 1),
  (1, 67, 1, 1, 1, 1),
  (1, 68, 1, 1, 1, 1),
  (1, 69, 1, 1, 1, 1),
  (1, 70, 1, 1, 1, 1),
  (1, 71, 1, 1, 1, 1),
  (1, 73, 1, 1, 1, 1),
  (1, 74, 1, 1, 1, 1),
  (1, 75, 1, 1, 1, 1),
  (1, 76, 1, 1, 1, 1),
  (1, 77, 1, 1, 1, 1),
  (1, 78, 1, 1, 1, 1),
  (1, 80, 1, 1, 1, 1),
  (1, 81, 1, 1, 1, 1),
  (1, 82, 1, 1, 1, 1),
  (1, 83, 1, 1, 1, 1),
  (1, 84, 1, 1, 1, 1),
  (1, 86, 1, 1, 1, 1),
  (1, 87, 1, 1, 1, 1),
  (1, 88, 1, 1, 1, 1),
  (1, 89, 1, 1, 1, 1),
  (1, 92, 1, 1, 1, 1),
  (1, 93, 1, 1, 1, 1),
  (1, 94, 1, 1, 1, 1),
  (1, 95, 1, 1, 1, 1),
  (1, 96, 1, 1, 1, 1),
  (1, 99, 1, 1, 1, 1),
  (1, 100, 1, 1, 1, 1),
  (1, 101, 1, 1, 1, 1),
  (1, 102, 1, 1, 1, 1),
  (1, 103, 1, 1, 1, 1),
  (1, 104, 1, 1, 1, 1),
  (1, 105, 1, 1, 1, 1),
  (1, 107, 1, 1, 1, 1),
  (1, 108, 1, 1, 1, 1),
  (1, 109, 1, 1, 1, 1),
  (1, 110, 1, 1, 1, 1),
  (1, 111, 1, 1, 1, 1),
  (1, 112, 1, 1, 1, 1),
  (1, 113, 1, 1, 1, 1),
  (1, 114, 1, 1, 1, 1),
  (1, 115, 1, 1, 1, 1),
  (1, 116, 1, 1, 1, 1),
  (1, 117, 1, 1, 1, 1),
  (1, 118, 1, 1, 1, 1),
  (1, 119, 1, 1, 1, 1),
  (1, 120, 1, 1, 1, 1),
  (2, 0, 1, 1, 1, 1),
  (2, 1, 0, 0, 0, 0),
  (2, 2, 0, 0, 0, 0),
  (2, 3, 0, 0, 0, 0),
  (2, 4, 0, 0, 0, 0),
  (2, 5, 0, 0, 0, 0),
  (2, 6, 0, 0, 0, 0),
  (2, 7, 0, 0, 0, 0),
  (2, 8, 0, 0, 0, 0),
  (2, 9, 1, 1, 1, 1),
  (2, 10, 1, 1, 1, 1),
  (2, 11, 1, 1, 1, 1),
  (2, 12, 0, 0, 0, 0),
  (2, 13, 1, 0, 1, 0),
  (2, 14, 1, 1, 1, 1),
  (2, 15, 0, 0, 0, 0),
  (2, 16, 0, 0, 0, 0),
  (2, 17, 0, 0, 0, 0),
  (2, 18, 0, 0, 0, 0),
  (2, 19, 0, 0, 0, 0),
  (2, 20, 1, 1, 1, 1),
  (2, 21, 1, 1, 1, 1),
  (2, 22, 1, 1, 1, 1),
  (2, 23, 1, 1, 1, 1),
  (2, 24, 0, 0, 0, 0),
  (2, 25, 0, 0, 0, 0),
  (2, 26, 0, 0, 0, 0),
  (2, 27, 1, 1, 1, 1),
  (2, 28, 0, 0, 0, 0),
  (2, 29, 0, 0, 0, 0),
  (2, 30, 1, 1, 1, 1),
  (2, 31, 1, 1, 1, 1),
  (2, 32, 1, 1, 1, 1),
  (2, 33, 1, 1, 1, 1),
  (2, 34, 1, 1, 1, 1),
  (2, 35, 1, 1, 1, 1),
  (2, 36, 0, 0, 0, 0),
  (2, 37, 1, 1, 1, 1),
  (2, 38, 1, 1, 1, 1),
  (2, 39, 0, 0, 0, 0),
  (2, 40, 0, 0, 0, 0),
  (2, 41, 0, 0, 0, 0),
  (2, 42, 0, 0, 0, 0),
  (2, 43, 0, 0, 0, 0),
  (2, 44, 0, 0, 0, 0),
  (2, 45, 0, 0, 0, 0),
  (2, 46, 0, 0, 0, 0),
  (2, 47, 0, 0, 0, 0),
  (2, 48, 1, 1, 1, 1),
  (2, 49, 1, 1, 1, 1),
  (2, 50, 0, 0, 0, 0),
  (2, 51, 0, 0, 0, 0),
  (2, 52, 0, 0, 0, 0),
  (2, 53, 0, 0, 0, 0),
  (2, 54, 0, 0, 0, 0),
  (2, 55, 0, 0, 0, 0),
  (2, 56, 0, 0, 0, 0),
  (2, 57, 0, 0, 0, 0),
  (2, 58, 0, 0, 0, 0),
  (2, 59, 0, 0, 0, 0),
  (2, 60, 1, 0, 1, 0),
  (2, 61, 0, 0, 0, 0),
  (2, 62, 0, 0, 0, 0),
  (2, 63, 0, 0, 0, 0),
  (2, 64, 0, 0, 0, 0),
  (2, 65, 0, 0, 0, 0),
  (2, 66, 0, 0, 0, 0),
  (2, 67, 0, 0, 0, 0),
  (2, 68, 0, 0, 0, 0),
  (2, 69, 0, 0, 0, 0),
  (2, 70, 0, 0, 0, 0),
  (2, 71, 0, 0, 0, 0),
  (2, 72, 0, 0, 0, 0),
  (2, 73, 0, 0, 0, 0),
  (2, 74, 0, 0, 0, 0),
  (2, 75, 0, 0, 0, 0),
  (2, 76, 0, 0, 0, 0),
  (2, 77, 0, 0, 0, 0),
  (2, 78, 0, 0, 0, 0),
  (2, 79, 0, 0, 0, 0),
  (2, 80, 0, 0, 0, 0),
  (2, 81, 0, 0, 0, 0),
  (2, 82, 0, 0, 0, 0),
  (2, 83, 0, 0, 0, 0),
  (2, 84, 0, 0, 0, 0),
  (2, 85, 0, 0, 0, 0),
  (2, 86, 0, 0, 0, 0),
  (2, 87, 0, 0, 0, 0),
  (2, 88, 0, 0, 0, 0),
  (2, 89, 0, 0, 0, 0),
  (2, 90, 0, 0, 0, 0),
  (2, 91, 0, 0, 0, 0),
  (2, 92, 0, 0, 0, 0),
  (2, 93, 0, 0, 0, 0),
  (2, 94, 1, 1, 1, 1),
  (2, 95, 1, 1, 1, 1),
  (2, 96, 1, 1, 1, 1),
  (2, 97, 0, 0, 0, 0),
  (2, 98, 0, 0, 0, 0),
  (2, 99, 1, 1, 1, 1),
  (2, 100, 1, 1, 1, 1),
  (2, 101, 0, 0, 0, 0),
  (2, 102, 0, 0, 0, 0),
  (2, 103, 0, 0, 0, 0),
  (2, 104, 0, 0, 0, 0),
  (2, 105, 0, 0, 0, 0),
  (2, 107, 0, 0, 0, 0),
  (2, 108, 0, 0, 0, 0),
  (2, 109, 0, 0, 0, 0),
  (2, 110, 0, 0, 0, 0),
  (2, 111, 0, 0, 0, 0),
  (2, 112, 0, 0, 0, 0),
  (2, 113, 0, 0, 0, 0),
  (2, 114, 0, 0, 0, 0),
  (2, 115, 0, 0, 0, 0),
  (2, 116, 0, 0, 0, 0),
  (2, 117, 0, 0, 0, 0),
  (2, 118, 0, 0, 0, 0),
  (2, 119, 0, 0, 0, 0),
  (2, 120, 0, 0, 0, 0),
  (3, 0, 1, 1, 1, 1),
  (3, 1, 0, 0, 0, 0),
  (3, 2, 0, 0, 0, 0),
  (3, 3, 0, 0, 0, 0),
  (3, 4, 0, 0, 0, 0),
  (3, 5, 1, 0, 0, 0),
  (3, 6, 0, 0, 0, 0),
  (3, 7, 0, 0, 0, 0),
  (3, 8, 0, 0, 0, 0),
  (3, 9, 1, 1, 1, 1),
  (3, 10, 0, 0, 0, 0),
  (3, 11, 0, 0, 0, 0),
  (3, 12, 0, 0, 0, 0),
  (3, 13, 0, 0, 0, 0),
  (3, 14, 0, 0, 0, 0),
  (3, 15, 1, 0, 0, 0),
  (3, 16, 1, 0, 0, 0),
  (3, 17, 0, 0, 0, 0),
  (3, 18, 0, 0, 0, 0),
  (3, 19, 0, 0, 0, 0),
  (3, 20, 0, 0, 0, 0),
  (3, 21, 1, 1, 1, 1),
  (3, 22, 1, 1, 1, 1),
  (3, 23, 0, 0, 0, 0),
  (3, 24, 0, 0, 0, 0),
  (3, 25, 0, 0, 0, 0),
  (3, 26, 0, 0, 0, 0),
  (3, 27, 0, 0, 0, 0),
  (3, 28, 0, 0, 0, 0),
  (3, 29, 0, 0, 0, 0),
  (3, 30, 0, 0, 0, 0),
  (3, 31, 0, 0, 0, 0),
  (3, 32, 0, 0, 0, 0),
  (3, 33, 0, 0, 0, 0),
  (3, 34, 0, 0, 0, 0),
  (3, 35, 0, 0, 0, 0),
  (3, 36, 0, 0, 0, 0),
  (3, 37, 0, 0, 0, 0),
  (3, 38, 0, 0, 0, 0),
  (3, 39, 0, 0, 0, 0),
  (3, 40, 0, 0, 0, 0),
  (3, 41, 0, 0, 0, 0),
  (3, 42, 0, 0, 0, 0),
  (3, 43, 0, 0, 0, 0),
  (3, 44, 0, 0, 0, 0),
  (3, 45, 0, 0, 0, 0),
  (3, 46, 0, 0, 0, 0),
  (3, 47, 0, 0, 0, 0),
  (3, 48, 0, 0, 0, 0),
  (3, 49, 0, 0, 0, 0),
  (3, 50, 0, 0, 0, 0),
  (3, 51, 0, 0, 0, 0),
  (3, 52, 0, 0, 0, 0),
  (3, 53, 0, 0, 0, 0),
  (3, 54, 0, 0, 0, 0),
  (3, 55, 0, 0, 0, 0),
  (3, 56, 0, 0, 0, 0),
  (3, 57, 0, 0, 0, 0),
  (3, 58, 0, 0, 0, 0),
  (3, 59, 1, 1, 1, 1),
  (3, 60, 0, 0, 0, 0),
  (3, 61, 0, 0, 0, 0),
  (3, 62, 0, 0, 0, 0),
  (3, 63, 0, 0, 0, 0),
  (3, 64, 0, 0, 0, 0),
  (3, 65, 0, 0, 0, 0),
  (3, 66, 0, 0, 0, 0),
  (3, 67, 0, 0, 0, 0),
  (3, 68, 0, 0, 0, 0),
  (3, 69, 0, 0, 0, 0),
  (3, 70, 1, 1, 1, 1),
  (3, 71, 0, 0, 0, 0),
  (3, 72, 0, 0, 0, 0),
  (3, 73, 0, 0, 0, 0),
  (3, 74, 0, 0, 0, 0),
  (3, 75, 0, 0, 0, 0),
  (3, 76, 0, 0, 0, 0),
  (3, 77, 0, 0, 0, 0),
  (3, 78, 0, 0, 0, 0),
  (3, 79, 0, 0, 0, 0),
  (3, 80, 0, 0, 0, 0),
  (3, 81, 0, 0, 0, 0),
  (3, 82, 0, 0, 0, 0),
  (3, 83, 0, 0, 0, 0),
  (3, 84, 0, 0, 0, 0),
  (3, 85, 0, 0, 0, 0),
  (3, 86, 0, 0, 0, 0),
  (3, 87, 0, 0, 0, 0),
  (3, 88, 0, 0, 0, 0),
  (3, 89, 0, 0, 0, 0),
  (3, 90, 0, 0, 0, 0),
  (3, 91, 0, 0, 0, 0),
  (3, 92, 0, 0, 0, 0),
  (3, 93, 0, 0, 0, 0),
  (3, 94, 0, 0, 0, 0),
  (3, 95, 0, 0, 0, 0),
  (3, 96, 0, 0, 0, 0),
  (3, 97, 0, 0, 0, 0),
  (3, 98, 0, 0, 0, 0),
  (3, 99, 0, 0, 0, 0),
  (3, 100, 0, 0, 0, 0),
  (3, 101, 0, 0, 0, 0),
  (3, 102, 0, 0, 0, 0),
  (3, 103, 0, 0, 0, 0),
  (3, 104, 0, 0, 0, 0),
  (3, 105, 0, 0, 0, 0),
  (3, 107, 0, 0, 0, 0),
  (3, 108, 0, 0, 0, 0),
  (3, 109, 0, 0, 0, 0),
  (3, 110, 0, 0, 0, 0),
  (3, 111, 0, 0, 0, 0),
  (3, 112, 0, 0, 0, 0),
  (3, 113, 0, 0, 0, 0),
  (3, 114, 0, 0, 0, 0),
  (3, 115, 0, 0, 0, 0),
  (3, 116, 0, 0, 0, 0),
  (3, 117, 0, 0, 0, 0),
  (3, 118, 0, 0, 0, 0),
  (3, 119, 0, 0, 0, 0),
  (3, 120, 0, 0, 0, 0),
  (4, 0, 1, 1, 1, 1),
  (4, 1, 0, 0, 0, 0),
  (4, 2, 0, 0, 0, 0),
  (4, 3, 0, 0, 0, 0),
  (4, 4, 0, 0, 0, 0),
  (4, 5, 1, 0, 0, 0),
  (4, 6, 0, 0, 0, 0),
  (4, 7, 0, 0, 0, 0),
  (4, 8, 0, 0, 0, 0),
  (4, 9, 1, 1, 1, 1),
  (4, 10, 1, 1, 1, 1),
  (4, 11, 1, 1, 1, 1),
  (4, 12, 0, 0, 0, 0),
  (4, 13, 1, 0, 1, 0),
  (4, 14, 0, 0, 0, 0),
  (4, 15, 0, 0, 0, 0),
  (4, 16, 0, 0, 0, 0),
  (4, 17, 0, 0, 0, 0),
  (4, 18, 0, 0, 0, 0),
  (4, 19, 1, 1, 1, 1),
  (4, 20, 1, 0, 0, 0),
  (4, 21, 1, 1, 1, 1),
  (4, 22, 1, 1, 1, 1),
  (4, 23, 0, 0, 0, 0),
  (4, 24, 0, 0, 0, 0),
  (4, 25, 0, 0, 0, 0),
  (4, 26, 1, 0, 0, 0),
  (4, 27, 0, 0, 0, 0),
  (4, 28, 0, 0, 0, 0),
  (4, 29, 0, 0, 0, 0),
  (4, 30, 1, 1, 1, 1),
  (4, 31, 1, 1, 1, 1),
  (4, 32, 0, 0, 0, 0),
  (4, 33, 0, 0, 0, 0),
  (4, 34, 1, 1, 1, 1),
  (4, 35, 0, 0, 0, 0),
  (4, 36, 1, 1, 1, 1),
  (4, 37, 1, 1, 1, 1),
  (4, 38, 1, 1, 1, 1),
  (4, 39, 1, 1, 1, 1),
  (4, 40, 1, 1, 1, 1),
  (4, 41, 0, 0, 0, 0),
  (4, 42, 0, 0, 0, 0),
  (4, 43, 0, 0, 0, 0),
  (4, 44, 0, 0, 0, 0),
  (4, 45, 0, 0, 0, 0),
  (4, 46, 0, 0, 0, 0),
  (4, 47, 0, 0, 0, 0),
  (4, 48, 0, 0, 0, 0),
  (4, 49, 0, 0, 0, 0),
  (4, 50, 0, 0, 0, 0),
  (4, 51, 0, 0, 0, 0),
  (4, 52, 0, 0, 0, 0),
  (4, 53, 0, 0, 0, 0),
  (4, 54, 0, 0, 0, 0),
  (4, 55, 0, 0, 0, 0),
  (4, 56, 0, 0, 0, 0),
  (4, 57, 0, 0, 0, 0),
  (4, 58, 0, 0, 0, 0),
  (4, 59, 0, 0, 0, 0),
  (4, 60, 1, 0, 1, 0),
  (4, 61, 0, 0, 0, 0),
  (4, 62, 0, 0, 0, 0),
  (4, 63, 0, 0, 0, 0),
  (4, 64, 0, 0, 0, 0),
  (4, 65, 0, 0, 0, 0),
  (4, 66, 0, 0, 0, 0),
  (4, 67, 0, 0, 0, 0),
  (4, 68, 0, 0, 0, 0),
  (4, 69, 0, 0, 0, 0),
  (4, 70, 0, 0, 0, 0),
  (4, 71, 0, 0, 0, 0),
  (4, 72, 0, 0, 0, 0),
  (4, 73, 0, 0, 0, 0),
  (4, 74, 0, 0, 0, 0),
  (4, 75, 0, 0, 0, 0),
  (4, 76, 0, 0, 0, 0),
  (4, 77, 0, 0, 0, 0),
  (4, 78, 0, 0, 0, 0),
  (4, 79, 0, 0, 0, 0),
  (4, 80, 0, 0, 0, 0),
  (4, 81, 0, 0, 0, 0),
  (4, 82, 0, 0, 0, 0),
  (4, 83, 0, 0, 0, 0),
  (4, 84, 1, 1, 1, 1),
  (4, 85, 0, 0, 0, 0),
  (4, 86, 0, 0, 0, 0),
  (4, 87, 0, 0, 0, 0),
  (4, 88, 0, 0, 0, 0),
  (4, 89, 0, 0, 0, 0),
  (4, 90, 0, 0, 0, 0),
  (4, 91, 1, 1, 1, 1),
  (4, 92, 0, 0, 0, 0),
  (4, 93, 1, 1, 1, 1),
  (4, 94, 0, 0, 0, 0),
  (4, 95, 0, 0, 0, 0),
  (4, 96, 0, 0, 0, 0),
  (4, 97, 0, 0, 0, 0),
  (4, 98, 0, 0, 0, 0),
  (4, 99, 1, 0, 0, 0),
  (4, 100, 0, 0, 0, 0),
  (4, 101, 0, 0, 0, 0),
  (4, 102, 0, 0, 0, 0),
  (4, 103, 0, 0, 0, 0),
  (4, 104, 0, 0, 0, 0),
  (4, 105, 0, 0, 0, 0),
  (4, 107, 0, 0, 0, 0),
  (4, 108, 0, 0, 0, 0),
  (4, 109, 0, 0, 0, 0),
  (4, 110, 0, 0, 0, 0),
  (4, 111, 0, 0, 0, 0),
  (4, 112, 0, 0, 0, 0),
  (4, 113, 0, 0, 0, 0),
  (4, 114, 0, 0, 0, 0),
  (4, 115, 0, 0, 0, 0),
  (4, 116, 0, 0, 0, 0),
  (4, 117, 0, 0, 0, 0),
  (4, 118, 0, 0, 0, 0),
  (4, 119, 0, 0, 0, 0),
  (4, 120, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_accessory`
--

CREATE TABLE IF NOT EXISTS `ps_accessory` (
  `id_product_1` INT(10) UNSIGNED NOT NULL,
  `id_product_2` INT(10) UNSIGNED NOT NULL,
  KEY `accessory_product` (`id_product_1`, `id_product_2`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_accessory`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_address`
--

CREATE TABLE IF NOT EXISTS `ps_address` (
  `id_address`      INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_country`      INT(10) UNSIGNED    NOT NULL,
  `id_state`        INT(10) UNSIGNED             DEFAULT NULL,
  `id_customer`     INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `id_manufacturer` INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `id_supplier`     INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `id_warehouse`    INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `alias`           VARCHAR(32)         NOT NULL,
  `company`         VARCHAR(64)                  DEFAULT NULL,
  `lastname`        VARCHAR(32)         NOT NULL,
  `firstname`       VARCHAR(32)         NOT NULL,
  `address1`        VARCHAR(128)        NOT NULL,
  `address2`        VARCHAR(128)                 DEFAULT NULL,
  `postcode`        VARCHAR(12)                  DEFAULT NULL,
  `city`            VARCHAR(64)         NOT NULL,
  `other`           TEXT,
  `phone`           VARCHAR(32)                  DEFAULT NULL,
  `phone_mobile`    VARCHAR(32)                  DEFAULT NULL,
  `vat_number`      VARCHAR(32)                  DEFAULT NULL,
  `dni`             VARCHAR(16)                  DEFAULT NULL,
  `date_add`        DATETIME            NOT NULL,
  `date_upd`        DATETIME            NOT NULL,
  `active`          TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `deleted`         TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_address`),
  KEY `address_customer` (`id_customer`),
  KEY `id_country` (`id_country`),
  KEY `id_state` (`id_state`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `id_supplier` (`id_supplier`),
  KEY `id_warehouse` (`id_warehouse`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 5;

--
-- Vypisuji data pro tabulku `ps_address`
--

INSERT INTO `ps_address` (`id_address`, `id_country`, `id_state`, `id_customer`, `id_manufacturer`, `id_supplier`, `id_warehouse`, `alias`, `company`, `lastname`, `firstname`, `address1`, `address2`, `postcode`, `city`, `other`, `phone`, `phone_mobile`, `vat_number`, `dni`, `date_add`, `date_upd`, `active`, `deleted`)
VALUES
  (1, 8, 0, 1, 0, 0, 0, 'Mon adresse', 'My Company', 'DOE', 'John', '16, Main street', '2nd floor', '75002', 'Paris ',
                                                                    '', '0102030405', '', '', '', '2016-08-04 12:42:31',
   '2016-08-04 12:42:31', 1, 0),
  (2, 21, 32, 0, 0, 1, 0, 'supplier', 'Fashion', 'supplier', 'supplier', '767 Fifth Ave.', '', '10153', 'New York', '',
                                                                         '(212) 336-1440', '', '', '',
                                                                         '2016-08-04 12:42:31', '2016-08-04 12:42:31',
   1, 0),
  (3, 21, 32, 0, 1, 0, 0, 'manufacturer', 'Fashion', 'manufacturer', 'manufacturer', '767 Fifth Ave.', '', '10154',
                                                                                     'New York', '', '(212) 336-1666',
                                                                                     '', '', '', '2016-08-04 12:42:31',
   '2017-01-23 19:08:09', 1, 1),
  (4, 21, 9, 1, 0, 0, 0, 'My address', 'My Company', 'DOE', 'John', '16, Main street', '2nd floor', '33133', 'Miami',
                                                                    '', '0102030405', '', '', '', '2016-08-04 12:42:31',
   '2016-08-04 12:42:31', 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_address_format`
--

CREATE TABLE IF NOT EXISTS `ps_address_format` (
  `id_country` INT(10) UNSIGNED NOT NULL,
  `format`     VARCHAR(255)     NOT NULL DEFAULT '',
  PRIMARY KEY (`id_country`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_address_format`
--

INSERT INTO `ps_address_format` (`id_country`, `format`) VALUES
  (1, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (2, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (3, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (4, 'firstname lastname\ncompany\naddress1\naddress2\ncity State:name postcode\nCountry:name\nphone\nphone_mobile'),
  (5, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (6, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (7, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (8, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (9, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (10, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
  (11, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
  (12, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (13, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (14, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (15, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (16, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (17, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\ncity\npostcode\nCountry:name\nphone\nphone_mobile'),
  (18, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (19, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (20, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (21, 'firstname lastname\ncompany\naddress1 address2\ncity, State:name postcode\nCountry:name\nphone\nphone_mobile'),
  (22, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (23, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (24, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (25, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (26, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (27, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (28, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (29, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (30, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (31, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (32, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (33, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (34, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (35, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (36, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (37, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (38, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (39, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (40, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (41, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (42, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (43, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (44, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
  (45, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (46, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (47, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (48, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (49, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (50, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (51, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (52, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (53, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (54, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (55, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (56, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (57, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (58, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (59, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (60, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (61, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (62, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (63, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (64, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (65, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (66, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (67, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (68, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (69, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (70, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (71, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (72, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (73, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (74, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (75, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (76, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (77, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (78, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (79, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (80, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (81, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (82, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (83, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (84, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (85, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (86, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (87, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (88, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (89, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (90, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (91, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (92, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (93, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (94, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (95, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (96, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (97, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (98, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (99, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (100, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (101, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (102, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (103, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (104, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (105, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (106, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (107, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (108, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (109, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (110, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (111, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
  (112, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (113, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (114, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (115, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (116, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (117, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (118, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (119, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (120, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (121, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (122, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (123, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (124, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (125, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (126, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (127, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (128, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (129, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (130, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (131, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (132, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (133, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (134, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (135, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (136, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (137, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (138, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (139, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (140, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (141, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (142, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (143, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (144, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (145, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
  (146, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (147, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (148, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (149, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (150, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (151, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (152, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (153, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (154, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (155, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (156, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (157, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (158, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (159, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (160, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (161, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (162, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (163, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (164, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (165, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (166, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (167, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (168, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (169, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (170, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (171, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (172, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (173, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (174, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (175, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (176, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (177, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (178, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (179, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (180, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (181, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (182, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (183, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (184, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (185, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (186, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (187, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (188, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (189, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (190, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (191, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (192, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (193, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (194, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (195, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (196, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (197, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (198, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (199, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (200, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (201, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (202, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (203, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (204, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (205, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (206, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (207, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (208, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (209, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (210, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (211, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (212, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (213, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (214, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (215, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (216, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (217, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (218, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (219, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (220, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (221, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (222, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (223, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (224, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (225, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (226, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (227, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (228, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (229, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (230, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (231, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (232, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (233, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (234, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (235, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (236, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (237, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (238, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (239, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (240, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (241, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (242,
   'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (243,
   'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
  (244,
   'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_advice`
--

CREATE TABLE IF NOT EXISTS `ps_advice` (
  `id_advice`    INT(11)                  NOT NULL AUTO_INCREMENT,
  `id_ps_advice` INT(11)                  NOT NULL,
  `id_tab`       INT(11)                  NOT NULL,
  `ids_tab`      TEXT,
  `validated`    TINYINT(1) UNSIGNED      NOT NULL DEFAULT '0',
  `hide`         TINYINT(1)               NOT NULL DEFAULT '0',
  `location`     ENUM ('after', 'before') NOT NULL,
  `selector`     VARCHAR(255)                      DEFAULT NULL,
  `start_day`    INT(11)                  NOT NULL DEFAULT '0',
  `stop_day`     INT(11)                  NOT NULL DEFAULT '0',
  `weight`       INT(11)                           DEFAULT '1',
  PRIMARY KEY (`id_advice`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 53;

--
-- Vypisuji data pro tabulku `ps_advice`
--

INSERT INTO `ps_advice` (`id_advice`, `id_ps_advice`, `id_tab`, `ids_tab`, `validated`, `hide`, `location`, `selector`, `start_day`, `stop_day`, `weight`)
VALUES
  (49, 353, 59, NULL, 1, 0, 'before', '#typeTranslationForm', 0, 0, 1),
  (50, 378, 63, NULL, 1, 0, 'before', 'div.leadin', 0, 0, 1),
  (51, 379, 65, NULL, 0, 0, 'before', 'form.form-horizontal:first, form.toolbar-placeholder', 0, 0, 1),
  (52, 389, 0, NULL, 1, 0, 'before', '#upgradeButtonBlock', 0, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_advice_lang`
--

CREATE TABLE IF NOT EXISTS `ps_advice_lang` (
  `id_advice` INT(11) NOT NULL,
  `id_lang`   INT(11) NOT NULL,
  `html`      TEXT,
  PRIMARY KEY (`id_advice`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_advice_lang`
--

INSERT INTO `ps_advice_lang` (`id_advice`, `id_lang`, `html`) VALUES
  (49, 1,
   '<div id="wrap_id_advice_353" ><style>\r\n.hide{display:none}.text-right{text-align:right}.text-left{text-align:left}.text-center{text-align:center}hr.clear{visibility:hidden;margin-bottom:20px}.gamification-tip{width:100%;height:40px;margin:0 0 20px 0;position:relative;line-height:43px;background-color:#f8f8f8;border-bottom:solid 5px #d2d2d2}.gamification-tip div.gamification-tip-title{position:absolute;top:0;left:0;height:40px;width:90px;padding:0 0 0 40px;background:url("https://gamification.prestashop.com/images/interface/gamification-lightbulb.png") 10px 5px no-repeat;color:#556e26;font-size:14px;font-weight:bold}.gamification-tip div.gamification-tip-description-container{height:40px;padding:0 130px 0 130px;display:table-cell;vertical-align:middle;font-size:13px;color:#666666;background:url("https://gamification.prestashop.com/images/interface/gamification-tip-bg.png") 100px top no-repeat}.gamification-tip div.gamification-tip-description-container span.gamification-tip-description{display:inline-block;line-height:15px;max-height:30px;overflow:hidden}.gamification-tip span.gamification-tip-cta{position:absolute;line-height:43px;height:40px;width:70px;top:0;right:0;padding:0 10px 0 30px;border-bottom:solid 5px #739334;background:url("https://gamification.prestashop.com/images/interface/gamification-cta-bg.png") left top no-repeat #a6c964}.gamification-tip span.gamification-tip-cta a{display:inline-block;width:100%;font-size:14px;text-transform:uppercase;font-weight:bold;text-shadow:rgba(255, 255, 255, 0.5) 0 1px 0;color:#556e26;background:url("https://gamification.prestashop.com/images/interface/gamification-popin.png") right 10px no-repeat}.gamification-tip-infobox{padding:0 20px 20px 20px;position:relative}.gamification-tip-infobox .gamification-tip-infobox-title{display:inline-block;margin:0 0 20px -20px;width:100%;padding:10px 20px 5px;border-bottom:solid 3px #739334;font:800 18px/20px arial;text-transform:uppercase;color:#556e26;background-color:#e7f0d6}.gamification-tip-infobox .gamification-tip-infobox-title span.gamification-tip-infobox-title-prefix{display:inline-block;height:40px;padding-left:30px;line-height:40px;text-transform:none;font-size:16px;font-weight:500;margin-right:10px;background:url("https://gamification.prestashop.com/images/interface/gamification-lightbulb.png") left top no-repeat}.gamification-tip-infobox .gamification-tip-infobox-content{display:block;width:100%}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-image{float:left;width:170px;height:200px;background:url("https://gamification.prestashop.com/images/interface/gamification-infobox-separator.png") no-repeat right center}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-description{float:left;width:370px;padding:0 0 25px 25px}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-description p{line-height:20px}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-description ul li{padding:0 0 0 20px;line-height:25px;background:url("https://gamification.prestashop.com/images/interface/gamification-bullet-check.png") left top no-repeat}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls{padding:20px 0 0 0}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button{display:inline-block;height:30px;padding:0 20px;margin-right:10px;border:none;border-bottom:solid 3px #ababab;line-height:33px;text-transform:uppercase;font-weight:bold;color:#929292;text-shadow:rgba(255, 255, 255, 0.5) 0 1px 0;border-radius:3px;background:#d2d2d2}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button.success{color:#384819;background:#a6c964;border-color:#739334;text-shadow:rgba(255, 255, 255, 0.5) 0 1px 0}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button:hover{color:#f8f8f8;background:#5f5f5f;border-color:#2c2c2c;text-shadow:rgba(0, 0, 0, 0.5) 0 1px 0}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button:active{color:white;background:#2c2c2c;border-color:black;text-shadow:rgba(0, 0, 0, 0.5) 0 1px 0}.gamification-tip-infobox a.infobox-close{display:inline-block;width:14px;height:14px;position:absolute;top:20px;right:20px;text-indent:-9999px;background:url("https://gamification.prestashop.com/images/interface/gamification-infobox-close.png") no-repeat}\r\n</style>\r\n<hr class="clear"/>\r\n<div id="advice-16">\r\n<div class="gamification-tip">\r\n	<div class="gamification-tip-title">Advice</div>\r\n	<span class="gamification-tip-cta"><a class="" href="https://gamification.prestashop.com/get_advice_link.php?id_advice=353&url=http%3A%2F%2Fcrowdin.net%2Fproject%2Fprestashop-official%2F">Join</a><a class="gamification_close" style="display:none"  id="353" href="#advice_content_353">close</a></span>\r\n	<div class="gamification-tip-description-container">\r\n		<span class="gamification-tip-description">\r\n			Help us translate PrestaShop 1.6 into your language by <a href="https://gamification.prestashop.com/get_advice_link.php?id_advice=353&url=http%3A%2F%2Fcrowdin.net%2Fproject%2Fprestashop-official%2F">joining us on Crowdin</a>!\r\n		</span>\r\n<div style="display:none"><img src="https://gamification.prestashop.com/api/getAdviceImg/353.png" /></div>\r\n	</div>\r\n</div>\r\n</div>\r\n</div>'),
  (50, 1,
   '<div id="wrap_id_advice_378" ><style>\n.hide{display:none}.text-right{text-align:right}.text-left{text-align:left}.text-center{text-align:center}hr.clear{visibility:hidden;margin-bottom:20px}.gamification-tip{width:100%;height:40px;margin:0 0 20px 0;position:relative;line-height:43px;background-color:#f8f8f8;border-bottom:solid 5px #d2d2d2}.gamification-tip div.gamification-tip-title{position:absolute;top:0;left:0;height:40px;width:90px;padding:0 0 0 40px;background:url("https://gamification.prestashop.com/images/interface/gamification-lightbulb.png") 10px 5px no-repeat;color:#556e26;font-size:14px;font-weight:bold}.gamification-tip div.gamification-tip-description-container{height:40px;padding:0 130px 0 130px;display:table-cell;vertical-align:middle;font-size:13px;color:#666666;background:url("https://gamification.prestashop.com/images/interface/gamification-tip-bg.png") 100px top no-repeat}.gamification-tip div.gamification-tip-description-container span.gamification-tip-description{display:inline-block;line-height:15px;max-height:30px;overflow:hidden}.gamification-tip span.gamification-tip-cta{position:absolute;line-height:43px;height:40px;width:70px;top:0;right:0;padding:0 10px 0 30px;border-bottom:solid 5px #739334;background:url("https://gamification.prestashop.com/images/interface/gamification-cta-bg.png") left top no-repeat #a6c964}.gamification-tip span.gamification-tip-cta a{display:inline-block;width:100%;font-size:14px;text-transform:uppercase;font-weight:bold;text-shadow:rgba(255, 255, 255, 0.5) 0 1px 0;color:#556e26;background:url("https://gamification.prestashop.com/images/interface/gamification-popin.png") right 10px no-repeat}.gamification-tip-infobox{padding:0 20px 20px 20px;position:relative}.gamification-tip-infobox .gamification-tip-infobox-title{display:inline-block;margin:0 0 20px -20px;width:100%;padding:10px 20px 5px;border-bottom:solid 3px #739334;font:800 18px/20px arial;text-transform:uppercase;color:#556e26;background-color:#e7f0d6}.gamification-tip-infobox .gamification-tip-infobox-title span.gamification-tip-infobox-title-prefix{display:inline-block;height:40px;padding-left:30px;line-height:40px;text-transform:none;font-size:16px;font-weight:500;margin-right:10px;background:url("https://gamification.prestashop.com/images/interface/gamification-lightbulb.png") left top no-repeat}.gamification-tip-infobox .gamification-tip-infobox-content{display:block;width:100%}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-image{float:left;width:120px;height:200px;background:url("https://gamification.prestashop.com/images/interface/gamification-infobox-separator.png") no-repeat right center}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-description{float:left;width:430px;padding:0 0 25px 25px}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-description p{line-height:20px}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-description ul li{padding:0 0 0 20px;line-height:25px;background:url("https://gamification.prestashop.com/images/interface/gamification-bullet-check.png") left center no-repeat}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls{padding:20px 0 0 0}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button{display:inline-block;height:30px;padding:0 20px;margin-right:10px;border:none;border-bottom:solid 3px #ababab;line-height:33px;text-transform:uppercase;font-weight:bold;color:#929292;text-shadow:rgba(255, 255, 255, 0.5) 0 1px 0;border-radius:3px;background:#d2d2d2}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button.success{color:#384819;background:#a6c964;border-color:#739334;text-shadow:rgba(255, 255, 255, 0.5) 0 1px 0}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button:hover{color:#f8f8f8;background:#5f5f5f;border-color:#2c2c2c;text-shadow:rgba(0, 0, 0, 0.5) 0 1px 0}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button:active{color:white;background:#2c2c2c;border-color:black;text-shadow:rgba(0, 0, 0, 0.5) 0 1px 0}.gamification-tip-infobox a.infobox-close{display:inline-block;width:14px;height:14px;position:absolute;top:20px;right:20px;text-indent:-9999px;background:url("https://gamification.prestashop.com/images/interface/gamification-infobox-close.png") no-repeat}\n</style>\n<hr class="clear"/>\n<div id="advice-16">\n<div class="gamification-tip">\n	<div class="gamification-tip-title">Advice</div>\n	<span class="gamification-tip-cta"><a class="gamification_fancybox"  id="378" href="#advice_content_378">Read</a><a class="gamification_close" style="display:none"  id="378" href="#advice_content_378">Close</a></span>\n\n	<div class="gamification-tip-description-container">\n		<span class="gamification-tip-description">\n					</span>\n	</div>\n	<div class="hide">\n		<div id="advice_content_378" class="gamification-tip-infobox">\n			<div class="gamification-tip-infobox-title">\n				<span class="gamification-tip-infobox-title-prefix">Advice</span>\n							</div>\n			<div class="gamification-tip-infobox-content">\n				<div class="gamification-tip-infobox-content-image">\n					<img src="https://gamification.prestashop.com/api/getAdviceImg/378.png" alt="logo" style="max-width: 85%">\n				</div>\n				<div class="gamification-tip-infobox-content-description">\n										<div class="gamification-tip-infobox-content-controls right">\n						<a href="javascript:$.fancybox.close();" class="button"></a>\n						<a href="{link}AdminModules{/link}&install=paypal&module_name=paypal" class="button success"></a>\n					</div>\n				</div>\n			</div>\n		</div>\n	</div>\n</div>\n</div>\n<script>\n	$(document).ready( function () {\n		$(''.gamification_fancybox'').bind(''click'', function () {\n			id_advice = 378;\n			popin_url = ''https://gamification.prestashop.com/get_advice_link.php?id_advice=''+id_advice+''&url='';\n			url = window.location.origin+window.location.pathname.replace(''index.php'', '''')+$(''.gamification-tip-infobox-content-controls a.success'').attr(''href'');\n			$(''.gamification-tip-infobox-content-controls a.success'').attr(''href'', popin_url+encodeURIComponent(url));\n			\n			$(''.gamification-tip-infobox'').after(''<img style="display:none" src="https://gamification.prestashop.com/api/getAdvicePopinImg/''+id_advice+''.png" />'');\n			\n		});\n	});\n</script></div>'),
  (51, 1,
   '<div id="wrap_id_advice_379" ><style>\n.hide{display:none}.text-right{text-align:right}.text-left{text-align:left}.text-center{text-align:center}hr.clear{visibility:hidden;margin-bottom:20px}.gamification-tip{width:100%;height:40px;margin:0 0 20px 0;position:relative;line-height:43px;background-color:#f8f8f8;border-bottom:solid 5px #d2d2d2}.gamification-tip div.gamification-tip-title{position:absolute;top:0;left:0;height:40px;width:90px;padding:0 0 0 40px;background:url("https://gamification.prestashop.com/images/interface/gamification-lightbulb.png") 10px 5px no-repeat;color:#556e26;font-size:14px;font-weight:bold}.gamification-tip div.gamification-tip-description-container{height:40px;padding:0 130px 0 130px;display:table-cell;vertical-align:middle;font-size:13px;color:#666666;background:url("https://gamification.prestashop.com/images/interface/gamification-tip-bg.png") 100px top no-repeat}.gamification-tip div.gamification-tip-description-container span.gamification-tip-description{display:inline-block;line-height:15px;max-height:30px;overflow:hidden}.gamification-tip span.gamification-tip-cta{position:absolute;line-height:43px;height:40px;width:70px;top:0;right:0;padding:0 10px 0 30px;border-bottom:solid 5px #739334;background:url("https://gamification.prestashop.com/images/interface/gamification-cta-bg.png") left top no-repeat #a6c964}.gamification-tip span.gamification-tip-cta a{display:inline-block;width:100%;font-size:14px;text-transform:uppercase;font-weight:bold;text-shadow:rgba(255, 255, 255, 0.5) 0 1px 0;color:#556e26;background:url("https://gamification.prestashop.com/images/interface/gamification-popin.png") right 10px no-repeat}.gamification-tip-infobox{padding:0 20px 20px 20px;position:relative}.gamification-tip-infobox .gamification-tip-infobox-title{display:inline-block;margin:0 0 20px -20px;width:100%;padding:10px 20px 5px;border-bottom:solid 3px #739334;font:800 18px/20px arial;text-transform:uppercase;color:#556e26;background-color:#e7f0d6}.gamification-tip-infobox .gamification-tip-infobox-title span.gamification-tip-infobox-title-prefix{display:inline-block;height:40px;padding-left:30px;line-height:40px;text-transform:none;font-size:16px;font-weight:500;margin-right:10px;background:url("https://gamification.prestashop.com/images/interface/gamification-lightbulb.png") left top no-repeat}.gamification-tip-infobox .gamification-tip-infobox-content{display:block;width:100%}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-image{float:left;width:120px;height:200px;background:url("https://gamification.prestashop.com/images/interface/gamification-infobox-separator.png") no-repeat right center}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-description{float:left;width:430px;padding:0 0 25px 25px}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-description p{line-height:20px}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-description ul li{padding:0 0 0 20px;line-height:25px;background:url("https://gamification.prestashop.com/images/interface/gamification-bullet-check.png") left center no-repeat}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls{padding:20px 0 0 0}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button{display:inline-block;height:30px;padding:0 20px;margin-right:10px;border:none;border-bottom:solid 3px #ababab;line-height:33px;text-transform:uppercase;font-weight:bold;color:#929292;text-shadow:rgba(255, 255, 255, 0.5) 0 1px 0;border-radius:3px;background:#d2d2d2}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button.success{color:#384819;background:#a6c964;border-color:#739334;text-shadow:rgba(255, 255, 255, 0.5) 0 1px 0}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button:hover{color:#f8f8f8;background:#5f5f5f;border-color:#2c2c2c;text-shadow:rgba(0, 0, 0, 0.5) 0 1px 0}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button:active{color:white;background:#2c2c2c;border-color:black;text-shadow:rgba(0, 0, 0, 0.5) 0 1px 0}.gamification-tip-infobox a.infobox-close{display:inline-block;width:14px;height:14px;position:absolute;top:20px;right:20px;text-indent:-9999px;background:url("https://gamification.prestashop.com/images/interface/gamification-infobox-close.png") no-repeat}\n</style>\n<hr class="clear"/>\n<div id="advice-16">\n<div class="gamification-tip">\n	<div class="gamification-tip-title">Advice</div>\n	<span class="gamification-tip-cta"><a class="gamification_fancybox"  id="379" href="#advice_content_379">Read</a><a class="gamification_close" style="display:none"  id="379" href="#advice_content_379">Close</a></span>\n\n	<div class="gamification-tip-description-container">\n		<span class="gamification-tip-description">\n					</span>\n	</div>\n	<div class="hide">\n		<div id="advice_content_379" class="gamification-tip-infobox">\n			<div class="gamification-tip-infobox-title">\n				<span class="gamification-tip-infobox-title-prefix">Advice</span>\n							</div>\n			<div class="gamification-tip-infobox-content">\n				<div class="gamification-tip-infobox-content-image">\n					<img src="https://gamification.prestashop.com/api/getAdviceImg/379.png" alt="logo" style="max-width: 85%">\n				</div>\n				<div class="gamification-tip-infobox-content-description">\n										<div class="gamification-tip-infobox-content-controls right">\n						<a href="javascript:$.fancybox.close();" class="button"></a>\n						<a href="{link}AdminModules{/link}&install=paypal&module_name=paypal" class="button success"></a>\n					</div>\n				</div>\n			</div>\n		</div>\n	</div>\n</div>\n</div>\n<script>\n	$(document).ready( function () {\n		$(''.gamification_fancybox'').bind(''click'', function () {\n			id_advice = 379;\n			popin_url = ''https://gamification.prestashop.com/get_advice_link.php?id_advice=''+id_advice+''&url='';\n			url = window.location.origin+window.location.pathname.replace(''index.php'', '''')+$(''.gamification-tip-infobox-content-controls a.success'').attr(''href'');\n			$(''.gamification-tip-infobox-content-controls a.success'').attr(''href'', popin_url+encodeURIComponent(url));\n			\n			$(''.gamification-tip-infobox'').after(''<img style="display:none" src="https://gamification.prestashop.com/api/getAdvicePopinImg/''+id_advice+''.png" />'');\n			\n		});\n	});\n</script></div>'),
  (52, 1,
   '<div id="wrap_id_advice_389" ><style>\n.hide{display:none}.text-right{text-align:right}.text-left{text-align:left}.text-center{text-align:center}hr.clear{visibility:hidden;margin-bottom:20px}.gamification-tip{width:100%;height:40px;margin:0 0 20px 0;position:relative;line-height:43px;background-color:#f8f8f8;border-bottom:solid 5px #d2d2d2}.gamification-tip div.gamification-tip-title{position:absolute;top:0;left:0;height:40px;width:90px;padding:0 0 0 40px;background:url("https://gamification.prestashop.com/images/interface/gamification-lightbulb.png") 10px 5px no-repeat;color:#556e26;font-size:14px;font-weight:bold}.gamification-tip div.gamification-tip-description-container{height:40px;padding:0 130px 0 130px;display:table-cell;vertical-align:middle;font-size:13px;color:#666666;background:url("https://gamification.prestashop.com/images/interface/gamification-tip-bg.png") 100px top no-repeat}.gamification-tip div.gamification-tip-description-container span.gamification-tip-description{display:inline-block;line-height:15px;max-height:30px;overflow:hidden}.gamification-tip span.gamification-tip-cta{position:absolute;line-height:43px;height:40px;width:70px;top:0;right:0;padding:0 10px 0 30px;border-bottom:solid 5px #739334;background:url("https://gamification.prestashop.com/images/interface/gamification-cta-bg.png") left top no-repeat #a6c964}.gamification-tip span.gamification-tip-cta a{display:inline-block;width:100%;font-size:14px;text-transform:uppercase;font-weight:bold;text-shadow:rgba(255, 255, 255, 0.5) 0 1px 0;color:#556e26;background:url("https://gamification.prestashop.com/images/interface/gamification-popin.png") right 10px no-repeat}.gamification-tip-infobox{padding:0 20px 20px 20px;position:relative}.gamification-tip-infobox .gamification-tip-infobox-title{display:inline-block;margin:0 0 20px -20px;width:100%;padding:10px 20px 5px;border-bottom:solid 3px #739334;font:800 18px/20px arial;text-transform:uppercase;color:#556e26;background-color:#e7f0d6}.gamification-tip-infobox .gamification-tip-infobox-title span.gamification-tip-infobox-title-prefix{display:inline-block;height:40px;padding-left:30px;line-height:40px;text-transform:none;font-size:16px;font-weight:500;margin-right:10px;background:url("https://gamification.prestashop.com/images/interface/gamification-lightbulb.png") left top no-repeat}.gamification-tip-infobox .gamification-tip-infobox-content{display:block;width:100%}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-image{float:left;width:120px;height:200px;background:url("https://gamification.prestashop.com/images/interface/gamification-infobox-separator.png") no-repeat right center}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-description{float:left;width:430px;padding:0 0 25px 25px}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-description p{line-height:20px}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-description ul li{padding:0 0 0 20px;line-height:25px;background:url("https://gamification.prestashop.com/images/interface/gamification-bullet-check.png") left center no-repeat}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls{padding:20px 0 0 0}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button{display:inline-block;height:30px;padding:0 20px;margin-right:10px;border:none;border-bottom:solid 3px #ababab;line-height:33px;text-transform:uppercase;font-weight:bold;color:#929292;text-shadow:rgba(255, 255, 255, 0.5) 0 1px 0;border-radius:3px;background:#d2d2d2}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button.success{color:#384819;background:#a6c964;border-color:#739334;text-shadow:rgba(255, 255, 255, 0.5) 0 1px 0}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button:hover{color:#f8f8f8;background:#5f5f5f;border-color:#2c2c2c;text-shadow:rgba(0, 0, 0, 0.5) 0 1px 0}.gamification-tip-infobox .gamification-tip-infobox-content .gamification-tip-infobox-content-controls a.button:active{color:white;background:#2c2c2c;border-color:black;text-shadow:rgba(0, 0, 0, 0.5) 0 1px 0}.gamification-tip-infobox a.infobox-close{display:inline-block;width:14px;height:14px;position:absolute;top:20px;right:20px;text-indent:-9999px;background:url("https://gamification.prestashop.com/images/interface/gamification-infobox-close.png") no-repeat}\n</style>\n<hr class="clear"/>\n<div id="advice-16">\n<div class="gamification-tip">\n	<img style="display:none" src="https://gamification.prestashop.com/api/getAdviceImg/389.png" />\n	<div class="gamification-tip-title">Advice</div>\n	<span class="gamification-tip-cta"><a class="gamification_fancybox"  id="389" href="http://www.prestashop.com/club/?utm_source=back-office&utm_medium=gamification" target="_blank">Learn more</a><a class="gamification_close" style="display:none"  id="389" href="#advice_content_389">Close</a></span>\n	<div class="gamification-tip-description-container">\n		<span class="gamification-tip-description">\n			About to upgrade? How about giving your opinion on future releases before anyone else?		</span>\n	</div>\n</div>\n</div></div>');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_alias`
--

CREATE TABLE IF NOT EXISTS `ps_alias` (
  `id_alias` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `alias`    VARCHAR(255)     NOT NULL,
  `search`   VARCHAR(255)     NOT NULL,
  `active`   TINYINT(1)       NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_alias`),
  UNIQUE KEY `alias` (`alias`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 3;

--
-- Vypisuji data pro tabulku `ps_alias`
--

INSERT INTO `ps_alias` (`id_alias`, `alias`, `search`, `active`) VALUES
  (1, 'bloose', 'blouse', 1),
  (2, 'blues', 'blouse', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_attachment`
--

CREATE TABLE IF NOT EXISTS `ps_attachment` (
  `id_attachment` INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `file`          VARCHAR(40)         NOT NULL,
  `file_name`     VARCHAR(128)        NOT NULL,
  `file_size`     BIGINT(10) UNSIGNED NOT NULL DEFAULT '0',
  `mime`          VARCHAR(128)        NOT NULL,
  PRIMARY KEY (`id_attachment`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_attachment`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_attachment_lang`
--

CREATE TABLE IF NOT EXISTS `ps_attachment_lang` (
  `id_attachment` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_lang`       INT(10) UNSIGNED NOT NULL,
  `name`          VARCHAR(32)               DEFAULT NULL,
  `description`   TEXT,
  PRIMARY KEY (`id_attachment`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_attachment_lang`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_attribute`
--

CREATE TABLE IF NOT EXISTS `ps_attribute` (
  `id_attribute`       INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_attribute_group` INT(10) UNSIGNED NOT NULL,
  `color`              VARCHAR(32)               DEFAULT NULL,
  `position`           INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_attribute`),
  KEY `attribute_group` (`id_attribute_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 198;

--
-- Vypisuji data pro tabulku `ps_attribute`
--

INSERT INTO `ps_attribute` (`id_attribute`, `id_attribute_group`, `color`, `position`) VALUES
  (1, 1, '', 0),
  (2, 1, '', 1),
  (3, 1, '', 2),
  (4, 1, '', 3),
  (5, 3, '#AAB2BD', 0),
  (6, 3, '#CFC4A6', 1),
  (7, 3, '#f5f5dc', 2),
  (8, 3, '#ffffff', 3),
  (9, 3, '#faebd7', 4),
  (10, 3, '#E84C3D', 5),
  (11, 3, '#434A54', 6),
  (12, 3, '#C19A6B', 7),
  (13, 3, '#F39C11', 8),
  (14, 3, '#5D9CEC', 9),
  (15, 3, '#A0D468', 10),
  (16, 3, '#F1C40F', 11),
  (17, 3, '#964B00', 12),
  (24, 3, '#FCCACD', 13),
  (25, 2, '', 0),
  (26, 2, '', 1),
  (27, 2, '', 2),
  (28, 2, '', 3),
  (29, 2, '', 4),
  (30, 2, '', 5),
  (31, 2, '', 6),
  (32, 2, '', 7),
  (33, 2, '', 8),
  (34, 2, '', 9),
  (35, 2, '', 10),
  (36, 2, '', 11),
  (37, 2, '', 12),
  (38, 2, '', 13),
  (39, 2, '', 14),
  (40, 2, '', 15),
  (41, 2, '', 16),
  (42, 2, '', 17),
  (43, 2, '', 18),
  (44, 2, '', 19),
  (45, 2, '', 20),
  (46, 2, '', 21),
  (47, 2, '', 22),
  (48, 2, '', 23),
  (49, 2, '', 24),
  (50, 2, '', 25),
  (51, 2, '', 26),
  (52, 2, '', 27),
  (53, 2, '', 28),
  (54, 2, '', 29),
  (55, 2, '', 30),
  (56, 2, '', 31),
  (57, 2, '', 32),
  (58, 2, '', 33),
  (59, 2, '', 34),
  (60, 2, '', 35),
  (61, 2, '', 36),
  (62, 2, '', 37),
  (63, 2, '', 38),
  (64, 2, '', 39),
  (65, 2, '', 40),
  (66, 2, '', 41),
  (67, 2, '', 42),
  (68, 2, '', 43),
  (69, 2, '', 44),
  (70, 2, '', 45),
  (71, 2, '', 46),
  (72, 2, '', 47),
  (73, 2, '', 48),
  (74, 2, '', 49),
  (75, 2, '', 50),
  (76, 2, '', 51),
  (77, 2, '', 52),
  (78, 2, '', 53),
  (79, 2, '', 54),
  (80, 2, '', 55),
  (81, 2, '', 56),
  (82, 2, '', 57),
  (83, 2, '', 58),
  (84, 2, '', 59),
  (85, 2, '', 60),
  (86, 2, '', 61),
  (87, 2, '', 62),
  (88, 2, '', 63),
  (89, 2, '', 64),
  (90, 2, '', 65),
  (91, 2, '', 66),
  (92, 2, '', 67),
  (93, 2, '', 68),
  (94, 2, '', 69),
  (95, 2, '', 70),
  (96, 2, '', 71),
  (97, 2, '', 72),
  (98, 2, '', 73),
  (99, 2, '', 74),
  (100, 2, '', 75),
  (101, 2, '', 76),
  (102, 2, '', 77),
  (103, 2, '', 78),
  (104, 2, '', 79),
  (105, 2, '', 80),
  (106, 5, '', 0),
  (107, 5, '', 1),
  (108, 5, '', 2),
  (109, 5, '', 3),
  (110, 5, '', 4),
  (111, 5, '', 5),
  (112, 5, '', 6),
  (113, 5, '', 7),
  (114, 5, '', 8),
  (115, 5, '', 9),
  (116, 5, '', 10),
  (117, 5, '', 11),
  (118, 5, '', 12),
  (119, 5, '', 13),
  (120, 5, '', 14),
  (121, 5, '', 15),
  (122, 5, '', 16),
  (123, 5, '', 17),
  (124, 5, '', 18),
  (125, 5, '', 19),
  (126, 5, '', 20),
  (127, 5, '', 21),
  (128, 5, '', 22),
  (129, 5, '', 23),
  (130, 5, '', 24),
  (131, 5, '', 25),
  (132, 5, '', 26),
  (133, 5, '', 27),
  (134, 5, '', 28),
  (135, 5, '', 29),
  (136, 5, '', 30),
  (137, 5, '', 31),
  (138, 5, '', 32),
  (139, 5, '', 33),
  (140, 5, '', 34),
  (141, 5, '', 35),
  (142, 5, '', 36),
  (143, 5, '', 37),
  (144, 5, '', 38),
  (145, 5, '', 39),
  (146, 5, '', 40),
  (147, 5, '', 41),
  (148, 5, '', 42),
  (149, 5, '', 43),
  (150, 5, '', 44),
  (151, 5, '', 45),
  (152, 5, '', 46),
  (153, 5, '', 47),
  (154, 5, '', 48),
  (155, 5, '', 49),
  (156, 5, '', 50),
  (157, 4, '', 0),
  (158, 4, '', 1),
  (159, 4, '', 2),
  (160, 4, '', 3),
  (161, 4, '', 4),
  (162, 4, '', 5),
  (163, 4, '', 6),
  (164, 4, '', 7),
  (165, 4, '', 8),
  (166, 4, '', 9),
  (167, 4, '', 10),
  (168, 4, '', 11),
  (169, 4, '', 12),
  (170, 4, '', 13),
  (171, 4, '', 14),
  (172, 4, '', 15),
  (173, 4, '', 16),
  (174, 4, '', 17),
  (175, 4, '', 18),
  (176, 4, '', 19),
  (177, 4, '', 20),
  (178, 4, '', 21),
  (179, 4, '', 22),
  (180, 4, '', 23),
  (181, 4, '', 24),
  (182, 4, '', 25),
  (183, 4, '', 26),
  (184, 4, '', 27),
  (185, 4, '', 28),
  (186, 4, '', 29),
  (187, 4, '', 30),
  (188, 4, '', 31),
  (189, 4, '', 32),
  (190, 4, '', 33),
  (191, 4, '', 34),
  (192, 4, '', 35),
  (193, 4, '', 36),
  (194, 4, '', 37),
  (195, 4, '', 38),
  (196, 4, '', 39),
  (197, 4, '', 40);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_attribute_group`
--

CREATE TABLE IF NOT EXISTS `ps_attribute_group` (
  `id_attribute_group` INT(10) UNSIGNED                  NOT NULL AUTO_INCREMENT,
  `is_color_group`     TINYINT(1)                        NOT NULL DEFAULT '0',
  `group_type`         ENUM ('select', 'radio', 'color') NOT NULL DEFAULT 'select',
  `position`           INT(10) UNSIGNED                  NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_attribute_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 6;

--
-- Vypisuji data pro tabulku `ps_attribute_group`
--

INSERT INTO `ps_attribute_group` (`id_attribute_group`, `is_color_group`, `group_type`, `position`) VALUES
  (1, 0, 'select', 0),
  (2, 0, 'select', 1),
  (3, 1, 'color', 2),
  (4, 0, 'select', 3),
  (5, 0, 'select', 4);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_attribute_group_lang`
--

CREATE TABLE IF NOT EXISTS `ps_attribute_group_lang` (
  `id_attribute_group` INT(10) UNSIGNED NOT NULL,
  `id_lang`            INT(10) UNSIGNED NOT NULL,
  `name`               VARCHAR(128)     NOT NULL,
  `public_name`        VARCHAR(64)      NOT NULL,
  PRIMARY KEY (`id_attribute_group`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_attribute_group_lang`
--

INSERT INTO `ps_attribute_group_lang` (`id_attribute_group`, `id_lang`, `name`, `public_name`) VALUES
  (1, 1, 'Velikost', 'Velikost'),
  (2, 1, 'Dioptrie/PWR', 'Dioptrie/PWR'),
  (3, 1, 'Barva', 'Barva'),
  (4, 1, 'Zakřivení/BC', 'Zakřivení/BC'),
  (5, 1, 'Průměr/DIA', 'Průměr/DIA');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_attribute_group_shop`
--

CREATE TABLE IF NOT EXISTS `ps_attribute_group_shop` (
  `id_attribute_group` INT(11) UNSIGNED NOT NULL,
  `id_shop`            INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_attribute_group`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_attribute_group_shop`
--

INSERT INTO `ps_attribute_group_shop` (`id_attribute_group`, `id_shop`) VALUES
  (1, 1),
  (2, 1),
  (3, 1),
  (4, 1),
  (5, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_attribute_impact`
--

CREATE TABLE IF NOT EXISTS `ps_attribute_impact` (
  `id_attribute_impact` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product`          INT(11) UNSIGNED NOT NULL,
  `id_attribute`        INT(11) UNSIGNED NOT NULL,
  `weight`              DECIMAL(20, 6)   NOT NULL,
  `price`               DECIMAL(17, 2)   NOT NULL,
  PRIMARY KEY (`id_attribute_impact`),
  UNIQUE KEY `id_product` (`id_product`, `id_attribute`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_attribute_impact`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_attribute_lang`
--

CREATE TABLE IF NOT EXISTS `ps_attribute_lang` (
  `id_attribute` INT(10) UNSIGNED NOT NULL,
  `id_lang`      INT(10) UNSIGNED NOT NULL,
  `name`         VARCHAR(128)     NOT NULL,
  PRIMARY KEY (`id_attribute`, `id_lang`),
  KEY `id_lang` (`id_lang`, `name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_attribute_lang`
--

INSERT INTO `ps_attribute_lang` (`id_attribute`, `id_lang`, `name`) VALUES
  (66, 1, '-0,5'),
  (67, 1, '-1'),
  (68, 1, '-1,5'),
  (85, 1, '-10'),
  (86, 1, '-10,5'),
  (87, 1, '-11'),
  (88, 1, '-11,5'),
  (89, 1, '-12'),
  (90, 1, '-12,5'),
  (91, 1, '-13'),
  (92, 1, '-13,5'),
  (93, 1, '-14'),
  (94, 1, '-14,5'),
  (95, 1, '-15'),
  (96, 1, '-15,5'),
  (97, 1, '-16'),
  (98, 1, '-16,5'),
  (99, 1, '-17'),
  (100, 1, '-17,5'),
  (101, 1, '-18'),
  (102, 1, '-18,5'),
  (103, 1, '-19'),
  (104, 1, '-19,5'),
  (69, 1, '-2'),
  (70, 1, '-2,5'),
  (105, 1, '-20'),
  (71, 1, '-3'),
  (72, 1, '-3,5'),
  (73, 1, '-4'),
  (74, 1, '-4,5'),
  (75, 1, '-5'),
  (76, 1, '-5,5'),
  (77, 1, '-6'),
  (78, 1, '-6,5'),
  (79, 1, '-7'),
  (80, 1, '-7,5'),
  (81, 1, '-8'),
  (82, 1, '-8,5'),
  (83, 1, '-9'),
  (84, 1, '-9,5'),
  (64, 1, '0,5'),
  (63, 1, '1'),
  (62, 1, '1,5'),
  (45, 1, '10'),
  (192, 1, '10'),
  (193, 1, '10,1'),
  (194, 1, '10,2'),
  (195, 1, '10,3'),
  (196, 1, '10,4'),
  (44, 1, '10,5'),
  (197, 1, '10,5'),
  (43, 1, '11'),
  (42, 1, '11,5'),
  (41, 1, '12'),
  (106, 1, '12'),
  (107, 1, '12,1'),
  (108, 1, '12,2'),
  (109, 1, '12,3'),
  (110, 1, '12,4'),
  (40, 1, '12,5'),
  (111, 1, '12,5'),
  (112, 1, '12,6'),
  (113, 1, '12,7'),
  (114, 1, '12,8'),
  (115, 1, '12,9'),
  (39, 1, '13'),
  (116, 1, '13'),
  (117, 1, '13,1'),
  (118, 1, '13,2'),
  (119, 1, '13,3'),
  (120, 1, '13,4'),
  (38, 1, '13,5'),
  (121, 1, '13,5'),
  (122, 1, '13,6'),
  (123, 1, '13,7'),
  (124, 1, '13,8'),
  (125, 1, '13,9'),
  (37, 1, '14'),
  (126, 1, '14'),
  (127, 1, '14,1'),
  (128, 1, '14,2'),
  (129, 1, '14,3'),
  (130, 1, '14,4'),
  (36, 1, '14,5'),
  (131, 1, '14,5'),
  (132, 1, '14,6'),
  (133, 1, '14,7'),
  (134, 1, '14,8'),
  (135, 1, '14,9'),
  (35, 1, '15'),
  (136, 1, '15'),
  (137, 1, '15,1'),
  (138, 1, '15,2'),
  (139, 1, '15,3'),
  (140, 1, '15,4'),
  (34, 1, '15,5'),
  (141, 1, '15,5'),
  (142, 1, '15,6'),
  (143, 1, '15,7'),
  (144, 1, '15,8'),
  (145, 1, '15,9'),
  (33, 1, '16'),
  (146, 1, '16'),
  (147, 1, '16,1'),
  (148, 1, '16,2'),
  (149, 1, '16,3'),
  (150, 1, '16,4'),
  (32, 1, '16,5'),
  (151, 1, '16,5'),
  (152, 1, '16,6'),
  (153, 1, '16,7'),
  (154, 1, '16,8'),
  (155, 1, '16,9'),
  (31, 1, '17'),
  (156, 1, '17'),
  (30, 1, '17,5'),
  (29, 1, '18'),
  (28, 1, '18,5'),
  (27, 1, '19'),
  (26, 1, '19,5'),
  (61, 1, '2'),
  (60, 1, '2,5'),
  (25, 1, '20'),
  (59, 1, '3'),
  (58, 1, '3,5'),
  (57, 1, '4'),
  (56, 1, '4,5'),
  (55, 1, '5'),
  (54, 1, '5,5'),
  (53, 1, '6'),
  (52, 1, '6,5'),
  (157, 1, '6,5'),
  (158, 1, '6,6'),
  (159, 1, '6,7'),
  (160, 1, '6,8'),
  (161, 1, '6,9'),
  (51, 1, '7'),
  (162, 1, '7'),
  (163, 1, '7,1'),
  (164, 1, '7,2'),
  (165, 1, '7,3'),
  (166, 1, '7,4'),
  (50, 1, '7,5'),
  (167, 1, '7,5'),
  (168, 1, '7,6'),
  (169, 1, '7,7'),
  (170, 1, '7,8'),
  (171, 1, '7,9'),
  (49, 1, '8'),
  (172, 1, '8'),
  (173, 1, '8,1'),
  (174, 1, '8,2'),
  (175, 1, '8,3'),
  (176, 1, '8,4'),
  (48, 1, '8,5'),
  (177, 1, '8,5'),
  (178, 1, '8,6'),
  (179, 1, '8,7'),
  (180, 1, '8,8'),
  (181, 1, '8,9'),
  (47, 1, '9'),
  (182, 1, '9'),
  (183, 1, '9,1'),
  (184, 1, '9,2'),
  (185, 1, '9,3'),
  (186, 1, '9,4'),
  (46, 1, '9,5'),
  (187, 1, '9,5'),
  (188, 1, '9,6'),
  (189, 1, '9,7'),
  (190, 1, '9,8'),
  (191, 1, '9,9'),
  (7, 1, 'Beige'),
  (11, 1, 'Black'),
  (14, 1, 'Blue'),
  (17, 1, 'Brown'),
  (12, 1, 'Camel'),
  (15, 1, 'Green'),
  (5, 1, 'Grey'),
  (3, 1, 'L'),
  (2, 1, 'M'),
  (65, 1, 'neutral'),
  (9, 1, 'Off White'),
  (4, 1, 'One size'),
  (13, 1, 'Orange'),
  (24, 1, 'Pink'),
  (10, 1, 'Red'),
  (1, 1, 'S'),
  (6, 1, 'Taupe'),
  (8, 1, 'White'),
  (16, 1, 'Yellow');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_attribute_shop`
--

CREATE TABLE IF NOT EXISTS `ps_attribute_shop` (
  `id_attribute` INT(11) UNSIGNED NOT NULL,
  `id_shop`      INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_attribute`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_attribute_shop`
--

INSERT INTO `ps_attribute_shop` (`id_attribute`, `id_shop`) VALUES
  (1, 1),
  (2, 1),
  (3, 1),
  (4, 1),
  (5, 1),
  (6, 1),
  (7, 1),
  (8, 1),
  (9, 1),
  (10, 1),
  (11, 1),
  (12, 1),
  (13, 1),
  (14, 1),
  (15, 1),
  (16, 1),
  (17, 1),
  (24, 1),
  (25, 1),
  (26, 1),
  (27, 1),
  (28, 1),
  (29, 1),
  (30, 1),
  (31, 1),
  (32, 1),
  (33, 1),
  (34, 1),
  (35, 1),
  (36, 1),
  (37, 1),
  (38, 1),
  (39, 1),
  (40, 1),
  (41, 1),
  (42, 1),
  (43, 1),
  (44, 1),
  (45, 1),
  (46, 1),
  (47, 1),
  (48, 1),
  (49, 1),
  (50, 1),
  (51, 1),
  (52, 1),
  (53, 1),
  (54, 1),
  (55, 1),
  (56, 1),
  (57, 1),
  (58, 1),
  (59, 1),
  (60, 1),
  (61, 1),
  (62, 1),
  (63, 1),
  (64, 1),
  (65, 1),
  (66, 1),
  (67, 1),
  (68, 1),
  (69, 1),
  (70, 1),
  (71, 1),
  (72, 1),
  (73, 1),
  (74, 1),
  (75, 1),
  (76, 1),
  (77, 1),
  (78, 1),
  (79, 1),
  (80, 1),
  (81, 1),
  (82, 1),
  (83, 1),
  (84, 1),
  (85, 1),
  (86, 1),
  (87, 1),
  (88, 1),
  (89, 1),
  (90, 1),
  (91, 1),
  (92, 1),
  (93, 1),
  (94, 1),
  (95, 1),
  (96, 1),
  (97, 1),
  (98, 1),
  (99, 1),
  (100, 1),
  (101, 1),
  (102, 1),
  (103, 1),
  (104, 1),
  (105, 1),
  (106, 1),
  (107, 1),
  (108, 1),
  (109, 1),
  (110, 1),
  (111, 1),
  (112, 1),
  (113, 1),
  (114, 1),
  (115, 1),
  (116, 1),
  (117, 1),
  (118, 1),
  (119, 1),
  (120, 1),
  (121, 1),
  (122, 1),
  (123, 1),
  (124, 1),
  (125, 1),
  (126, 1),
  (127, 1),
  (128, 1),
  (129, 1),
  (130, 1),
  (131, 1),
  (132, 1),
  (133, 1),
  (134, 1),
  (135, 1),
  (136, 1),
  (137, 1),
  (138, 1),
  (139, 1),
  (140, 1),
  (141, 1),
  (142, 1),
  (143, 1),
  (144, 1),
  (145, 1),
  (146, 1),
  (147, 1),
  (148, 1),
  (149, 1),
  (150, 1),
  (151, 1),
  (152, 1),
  (153, 1),
  (154, 1),
  (155, 1),
  (156, 1),
  (157, 1),
  (158, 1),
  (159, 1),
  (160, 1),
  (161, 1),
  (162, 1),
  (163, 1),
  (164, 1),
  (165, 1),
  (166, 1),
  (167, 1),
  (168, 1),
  (169, 1),
  (170, 1),
  (171, 1),
  (172, 1),
  (173, 1),
  (174, 1),
  (175, 1),
  (176, 1),
  (177, 1),
  (178, 1),
  (179, 1),
  (180, 1),
  (181, 1),
  (182, 1),
  (183, 1),
  (184, 1),
  (185, 1),
  (186, 1),
  (187, 1),
  (188, 1),
  (189, 1),
  (190, 1),
  (191, 1),
  (192, 1),
  (193, 1),
  (194, 1),
  (195, 1),
  (196, 1),
  (197, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_badge`
--

CREATE TABLE IF NOT EXISTS `ps_badge` (
  `id_badge`       INT(11)             NOT NULL AUTO_INCREMENT,
  `id_ps_badge`    INT(11)             NOT NULL,
  `type`           VARCHAR(32)         NOT NULL,
  `id_group`       INT(11)             NOT NULL,
  `group_position` INT(11)             NOT NULL,
  `scoring`        INT(11)             NOT NULL,
  `awb`            INT(11)                      DEFAULT '0',
  `validated`      TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_badge`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 248;

--
-- Vypisuji data pro tabulku `ps_badge`
--

INSERT INTO `ps_badge` (`id_badge`, `id_ps_badge`, `type`, `id_group`, `group_position`, `scoring`, `awb`, `validated`)
VALUES
  (1, 139, 'feature', 41, 1, 5, 1, 0),
  (2, 140, 'feature', 41, 2, 10, 1, 0),
  (3, 141, 'feature', 41, 3, 15, 1, 0),
  (4, 142, 'feature', 41, 4, 20, 1, 0),
  (5, 143, 'feature', 41, 1, 5, 1, 0),
  (6, 144, 'feature', 41, 2, 10, 1, 0),
  (7, 145, 'feature', 41, 3, 15, 1, 0),
  (8, 146, 'feature', 41, 4, 20, 1, 0),
  (9, 147, 'feature', 41, 1, 5, 1, 0),
  (10, 148, 'feature', 41, 2, 10, 1, 0),
  (11, 149, 'feature', 41, 3, 15, 1, 0),
  (12, 150, 'feature', 41, 4, 20, 1, 0),
  (13, 159, 'feature', 41, 1, 5, 1, 0),
  (14, 160, 'feature', 41, 2, 10, 1, 0),
  (15, 161, 'feature', 41, 3, 15, 1, 0),
  (16, 162, 'feature', 41, 4, 20, 1, 0),
  (17, 163, 'feature', 41, 1, 5, 1, 0),
  (18, 164, 'feature', 41, 2, 10, 1, 0),
  (19, 165, 'feature', 41, 3, 15, 1, 0),
  (20, 166, 'feature', 41, 4, 20, 1, 0),
  (21, 206, 'feature', 41, 1, 5, 1, 0),
  (22, 207, 'feature', 41, 2, 10, 1, 0),
  (23, 208, 'feature', 41, 3, 15, 1, 0),
  (24, 209, 'feature', 41, 4, 20, 1, 0),
  (25, 222, 'feature', 41, 1, 5, 1, 0),
  (26, 223, 'feature', 41, 3, 15, 1, 0),
  (27, 224, 'feature', 41, 4, 20, 1, 0),
  (28, 233, 'feature', 41, 1, 5, 1, 0),
  (29, 234, 'feature', 41, 2, 10, 1, 0),
  (30, 235, 'feature', 41, 3, 15, 1, 0),
  (31, 236, 'feature', 41, 4, 20, 1, 0),
  (32, 249, 'feature', 41, 1, 5, 1, 0),
  (33, 250, 'feature', 41, 2, 10, 1, 0),
  (34, 251, 'feature', 41, 3, 15, 1, 0),
  (35, 252, 'feature', 41, 4, 20, 1, 0),
  (36, 253, 'feature', 41, 1, 5, 1, 0),
  (37, 254, 'feature', 41, 2, 10, 1, 0),
  (38, 255, 'feature', 41, 3, 15, 1, 0),
  (39, 256, 'feature', 41, 4, 20, 1, 0),
  (40, 261, 'feature', 41, 1, 5, 1, 0),
  (41, 262, 'feature', 41, 2, 10, 1, 0),
  (42, 269, 'feature', 41, 1, 5, 1, 0),
  (43, 270, 'feature', 41, 2, 10, 1, 0),
  (44, 271, 'feature', 41, 3, 15, 1, 0),
  (45, 272, 'feature', 41, 4, 20, 1, 0),
  (46, 273, 'feature', 41, 1, 5, 1, 0),
  (47, 274, 'feature', 41, 2, 10, 1, 0),
  (48, 275, 'feature', 41, 3, 15, 1, 0),
  (49, 276, 'feature', 41, 4, 20, 1, 0),
  (50, 277, 'feature', 41, 1, 5, 1, 0),
  (51, 278, 'feature', 41, 2, 10, 1, 0),
  (52, 279, 'feature', 41, 3, 15, 1, 0),
  (53, 280, 'feature', 41, 4, 20, 1, 0),
  (54, 281, 'feature', 41, 1, 5, 1, 0),
  (55, 282, 'feature', 41, 2, 10, 1, 0),
  (56, 283, 'feature', 41, 3, 15, 1, 0),
  (57, 284, 'feature', 41, 4, 20, 1, 0),
  (58, 285, 'feature', 41, 1, 5, 1, 0),
  (59, 286, 'feature', 41, 2, 10, 1, 0),
  (60, 287, 'feature', 41, 3, 15, 1, 0),
  (61, 288, 'feature', 41, 4, 20, 1, 0),
  (62, 289, 'feature', 41, 1, 5, 1, 0),
  (63, 290, 'feature', 41, 2, 10, 1, 0),
  (64, 291, 'feature', 41, 3, 15, 1, 0),
  (65, 292, 'feature', 41, 4, 20, 1, 0),
  (66, 293, 'feature', 41, 1, 5, 1, 0),
  (67, 294, 'feature', 41, 2, 10, 1, 0),
  (68, 295, 'feature', 41, 3, 15, 1, 0),
  (69, 296, 'feature', 41, 4, 20, 1, 0),
  (70, 297, 'feature', 41, 1, 5, 1, 0),
  (71, 298, 'feature', 41, 2, 10, 1, 0),
  (72, 299, 'feature', 41, 3, 15, 1, 0),
  (73, 300, 'feature', 41, 4, 20, 1, 0),
  (74, 301, 'feature', 41, 1, 5, 1, 0),
  (75, 302, 'feature', 41, 2, 10, 1, 0),
  (76, 303, 'feature', 41, 3, 15, 1, 0),
  (77, 304, 'feature', 41, 4, 20, 1, 0),
  (78, 305, 'feature', 41, 1, 5, 1, 0),
  (79, 306, 'feature', 41, 2, 10, 1, 0),
  (80, 307, 'feature', 41, 3, 15, 1, 0),
  (81, 308, 'feature', 41, 4, 20, 1, 0),
  (82, 309, 'feature', 41, 1, 5, 1, 0),
  (83, 310, 'feature', 41, 2, 10, 1, 0),
  (84, 311, 'feature', 41, 3, 15, 1, 0),
  (85, 312, 'feature', 41, 4, 20, 1, 0),
  (86, 313, 'feature', 41, 1, 5, 1, 1),
  (87, 314, 'feature', 41, 2, 10, 1, 1),
  (88, 315, 'feature', 41, 3, 15, 1, 0),
  (89, 316, 'feature', 41, 4, 20, 1, 0),
  (90, 317, 'feature', 41, 1, 5, 1, 0),
  (91, 318, 'feature', 41, 2, 10, 1, 0),
  (92, 319, 'feature', 41, 3, 15, 1, 0),
  (93, 320, 'feature', 41, 4, 20, 1, 0),
  (94, 321, 'feature', 41, 1, 5, 1, 0),
  (95, 322, 'feature', 41, 2, 10, 1, 0),
  (96, 323, 'feature', 41, 3, 15, 1, 0),
  (97, 324, 'feature', 41, 4, 20, 1, 0),
  (98, 325, 'feature', 41, 1, 5, 1, 0),
  (99, 326, 'feature', 41, 2, 10, 1, 0),
  (100, 327, 'feature', 41, 3, 15, 1, 0),
  (101, 328, 'feature', 41, 4, 20, 1, 0),
  (102, 329, 'feature', 41, 1, 5, 1, 0),
  (103, 330, 'feature', 41, 2, 10, 1, 0),
  (104, 331, 'feature', 41, 3, 15, 1, 0),
  (105, 332, 'feature', 41, 4, 20, 1, 0),
  (106, 333, 'feature', 41, 1, 5, 1, 0),
  (107, 334, 'feature', 41, 2, 10, 1, 0),
  (108, 335, 'feature', 41, 3, 15, 1, 0),
  (109, 336, 'feature', 41, 4, 20, 1, 0),
  (110, 337, 'feature', 41, 1, 5, 1, 0),
  (111, 338, 'feature', 41, 2, 10, 1, 0),
  (112, 339, 'feature', 41, 3, 15, 1, 0),
  (113, 340, 'feature', 41, 4, 20, 1, 0),
  (114, 341, 'feature', 41, 1, 5, 1, 0),
  (115, 342, 'feature', 41, 2, 10, 1, 0),
  (116, 343, 'feature', 41, 3, 15, 1, 0),
  (117, 344, 'feature', 41, 4, 20, 1, 0),
  (118, 345, 'feature', 41, 1, 5, 1, 0),
  (119, 346, 'feature', 41, 2, 10, 1, 0),
  (120, 347, 'feature', 41, 3, 15, 1, 0),
  (121, 348, 'feature', 41, 4, 20, 1, 0),
  (122, 349, 'feature', 41, 1, 5, 1, 0),
  (123, 350, 'feature', 41, 2, 10, 1, 0),
  (124, 351, 'feature', 41, 3, 15, 1, 0),
  (125, 352, 'feature', 41, 4, 20, 1, 0),
  (126, 353, 'feature', 41, 1, 5, 1, 0),
  (127, 354, 'feature', 41, 2, 10, 1, 0),
  (128, 355, 'feature', 41, 3, 15, 1, 0),
  (129, 356, 'feature', 41, 4, 20, 1, 0),
  (130, 357, 'feature', 41, 1, 5, 1, 0),
  (131, 358, 'feature', 41, 2, 10, 1, 0),
  (132, 359, 'feature', 41, 3, 15, 1, 0),
  (133, 360, 'feature', 41, 4, 20, 1, 0),
  (134, 1, 'feature', 1, 1, 10, 0, 1),
  (135, 2, 'feature', 2, 1, 10, 0, 0),
  (136, 3, 'feature', 2, 2, 15, 0, 0),
  (137, 4, 'feature', 3, 1, 15, 0, 0),
  (138, 5, 'feature', 3, 2, 15, 0, 0),
  (139, 6, 'feature', 4, 1, 15, 0, 0),
  (140, 7, 'feature', 4, 2, 15, 0, 0),
  (141, 8, 'feature', 5, 1, 5, 0, 0),
  (142, 9, 'feature', 5, 2, 10, 0, 0),
  (143, 10, 'feature', 6, 1, 15, 0, 1),
  (144, 11, 'feature', 6, 2, 10, 0, 0),
  (145, 12, 'feature', 6, 3, 10, 0, 0),
  (146, 13, 'feature', 5, 3, 10, 0, 0),
  (147, 14, 'feature', 5, 4, 15, 0, 0),
  (148, 15, 'feature', 5, 5, 20, 0, 0),
  (149, 16, 'feature', 5, 6, 20, 0, 0),
  (150, 17, 'achievement', 7, 1, 5, 0, 1),
  (151, 18, 'achievement', 7, 2, 10, 0, 1),
  (152, 19, 'feature', 8, 1, 15, 0, 1),
  (153, 20, 'feature', 8, 2, 15, 0, 0),
  (154, 21, 'feature', 9, 1, 15, 0, 0),
  (155, 22, 'feature', 10, 1, 10, 0, 0),
  (156, 23, 'feature', 10, 2, 10, 0, 0),
  (157, 24, 'feature', 10, 3, 10, 0, 0),
  (158, 25, 'feature', 10, 4, 10, 0, 0),
  (159, 26, 'feature', 10, 5, 10, 0, 0),
  (160, 27, 'feature', 4, 3, 10, 0, 0),
  (161, 28, 'feature', 3, 3, 10, 0, 0),
  (162, 29, 'achievement', 11, 1, 5, 0, 0),
  (163, 30, 'achievement', 11, 2, 10, 0, 0),
  (164, 31, 'achievement', 11, 3, 15, 0, 0),
  (165, 32, 'achievement', 11, 4, 20, 0, 0),
  (166, 33, 'achievement', 11, 5, 25, 0, 0),
  (167, 34, 'achievement', 11, 6, 30, 0, 0),
  (168, 35, 'achievement', 7, 3, 15, 0, 1),
  (169, 36, 'achievement', 7, 4, 20, 0, 0),
  (170, 37, 'achievement', 7, 5, 25, 0, 0),
  (171, 38, 'achievement', 7, 6, 30, 0, 0),
  (172, 39, 'achievement', 12, 1, 5, 0, 1),
  (173, 40, 'achievement', 12, 2, 10, 0, 1),
  (174, 41, 'achievement', 12, 3, 15, 0, 0),
  (175, 42, 'achievement', 12, 4, 20, 0, 0),
  (176, 43, 'achievement', 12, 5, 25, 0, 0),
  (177, 44, 'achievement', 12, 6, 30, 0, 0),
  (178, 45, 'achievement', 13, 1, 5, 0, 1),
  (179, 46, 'achievement', 13, 2, 10, 0, 0),
  (180, 47, 'achievement', 13, 3, 15, 0, 0),
  (181, 48, 'achievement', 13, 4, 20, 0, 0),
  (182, 49, 'achievement', 13, 5, 25, 0, 0),
  (183, 50, 'achievement', 13, 6, 30, 0, 0),
  (184, 51, 'achievement', 14, 1, 5, 0, 0),
  (185, 52, 'achievement', 14, 2, 10, 0, 0),
  (186, 53, 'achievement', 14, 3, 15, 0, 0),
  (187, 54, 'achievement', 14, 4, 20, 0, 0),
  (188, 55, 'achievement', 14, 5, 25, 0, 0),
  (189, 56, 'achievement', 14, 6, 30, 0, 0),
  (190, 57, 'achievement', 15, 1, 5, 0, 0),
  (191, 58, 'achievement', 15, 2, 10, 0, 0),
  (192, 59, 'achievement', 15, 3, 15, 0, 0),
  (193, 60, 'achievement', 15, 4, 20, 0, 0),
  (194, 61, 'achievement', 15, 5, 25, 0, 0),
  (195, 62, 'achievement', 15, 6, 30, 0, 0),
  (196, 63, 'achievement', 16, 1, 5, 0, 0),
  (197, 64, 'achievement', 16, 2, 10, 0, 0),
  (198, 65, 'achievement', 16, 3, 15, 0, 0),
  (199, 66, 'achievement', 16, 4, 20, 0, 0),
  (200, 67, 'achievement', 16, 5, 25, 0, 0),
  (201, 68, 'achievement', 16, 6, 30, 0, 0),
  (202, 69, 'international', 17, 1, 10, 0, 0),
  (203, 70, 'international', 18, 1, 10, 0, 0),
  (204, 71, 'international', 19, 1, 10, 0, 0),
  (205, 72, 'international', 20, 1, 10, 0, 0),
  (206, 73, 'international', 21, 1, 10, 0, 0),
  (207, 74, 'international', 22, 1, 10, 0, 0),
  (208, 75, 'international', 23, 1, 10, 0, 0),
  (209, 83, 'international', 31, 1, 10, 0, 0),
  (210, 84, 'international', 25, 1, 10, 0, 0),
  (211, 86, 'international', 33, 1, 10, 0, 0),
  (212, 87, 'international', 34, 1, 10, 0, 0),
  (213, 88, 'feature', 35, 1, 5, 0, 1),
  (214, 89, 'feature', 35, 2, 10, 0, 0),
  (215, 90, 'feature', 35, 3, 10, 0, 0),
  (216, 91, 'feature', 35, 4, 10, 0, 0),
  (217, 92, 'feature', 35, 5, 10, 0, 0),
  (218, 93, 'feature', 35, 6, 10, 0, 0),
  (219, 94, 'feature', 36, 1, 5, 0, 0),
  (220, 95, 'feature', 36, 2, 5, 0, 0),
  (221, 96, 'feature', 36, 3, 10, 0, 0),
  (222, 97, 'feature', 36, 4, 10, 0, 0),
  (223, 98, 'feature', 36, 5, 20, 0, 0),
  (224, 99, 'feature', 36, 6, 20, 0, 0),
  (225, 100, 'feature', 8, 3, 15, 0, 1),
  (226, 101, 'achievement', 37, 1, 5, 0, 0),
  (227, 102, 'achievement', 37, 2, 5, 0, 0),
  (228, 103, 'achievement', 37, 3, 10, 0, 0),
  (229, 104, 'achievement', 37, 4, 10, 0, 0),
  (230, 105, 'achievement', 37, 5, 15, 0, 0),
  (231, 106, 'achievement', 37, 6, 15, 0, 0),
  (232, 107, 'achievement', 38, 1, 10, 0, 0),
  (233, 108, 'achievement', 38, 2, 10, 0, 0),
  (234, 109, 'achievement', 38, 3, 15, 0, 0),
  (235, 110, 'achievement', 38, 4, 20, 0, 0),
  (236, 111, 'achievement', 38, 5, 25, 0, 0),
  (237, 112, 'achievement', 38, 6, 30, 0, 0),
  (238, 113, 'achievement', 39, 1, 10, 0, 1),
  (239, 114, 'achievement', 39, 2, 20, 0, 0),
  (240, 115, 'achievement', 39, 3, 30, 0, 0),
  (241, 116, 'achievement', 39, 4, 40, 0, 0),
  (242, 117, 'achievement', 39, 5, 50, 0, 0),
  (243, 118, 'achievement', 39, 6, 50, 0, 0),
  (244, 119, 'feature', 40, 1, 10, 0, 0),
  (245, 120, 'feature', 40, 2, 15, 0, 0),
  (246, 121, 'feature', 40, 3, 20, 0, 0),
  (247, 122, 'feature', 40, 4, 25, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_badge_lang`
--

CREATE TABLE IF NOT EXISTS `ps_badge_lang` (
  `id_badge`    INT(11) NOT NULL,
  `id_lang`     INT(11) NOT NULL,
  `name`        VARCHAR(64)  DEFAULT NULL,
  `description` VARCHAR(255) DEFAULT NULL,
  `group_name`  VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id_badge`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_badge_lang`
--

INSERT INTO `ps_badge_lang` (`id_badge`, `id_lang`, `name`, `description`, `group_name`) VALUES
  (1, 1, 'HiPay installed', 'You have installed the HiPay module', 'Partners'),
  (2, 1, 'HiPay configured', 'You have configured the HiPay module', 'Partners'),
  (3, 1, 'HiPay active', 'Your Hipay module is active', 'Partners'),
  (4, 1, 'HiPay very active', 'Your HiPay module is very active', 'Partners'),
  (5, 1, 'Ogone installed', 'You have installed the Ogone module', 'Partners'),
  (6, 1, 'Ogone configuré', 'You have configured the Ogone module', 'Partners'),
  (7, 1, 'Ogone active', 'Your Ogone module is active', 'Partners'),
  (8, 1, 'Ogone very active', 'Your Ogone module is very active', 'Partners'),
  (9, 1, 'PayPal installed', 'You have installed the PayPal module', 'Partners'),
  (10, 1, 'PayPal configured', 'You have configured the PayPal module', 'Partners'),
  (11, 1, 'PayPal active', 'Your PayPal module is active', 'Partners'),
  (12, 1, 'PayPal very active', 'Your PayPal module is very active', 'Partners'),
  (13, 1, 'Shopgate installed', 'You have installed the Shopgate module', 'Partners'),
  (14, 1, 'Shopgate configured', 'You have configured the Shopgate module', 'Partners'),
  (15, 1, 'Shopgate active', 'Your Shopgate module is active', 'Partners'),
  (16, 1, 'Shopgate very active', 'Your Shopgate module is very active', 'Partners'),
  (17, 1, 'Skrill installed', 'You have installed the Skrill module', 'Partners'),
  (18, 1, 'Skrill configured', 'You have configured the Skrill module', 'Partners'),
  (19, 1, 'Skrill active', 'Your Skrill module is active', 'Partners'),
  (20, 1, 'Skrill very active', 'Your Skrill module is very active', 'Partners'),
  (21, 1, 'TextMaster installed', 'You have installed the TextMaster module', 'Partners'),
  (22, 1, 'TextMaster configured', 'You have configured the TextMaster module', 'Partners'),
  (23, 1, 'TextMaster active', 'Your TextMaster module is active', 'Partners'),
  (24, 1, 'TextMaster very active', 'Your TextMaster module is very active', 'Partners'),
  (25, 1, 'Paymill installed', 'You have installed the Paymill module', 'Partners'),
  (26, 1, 'Paymill active', 'Your Paymill module is active', 'Partners'),
  (27, 1, 'Paymill very active', 'Your Paymill module is very active', 'Partners'),
  (28, 1, 'Authorize Aim installed', 'You have installed the Authorize Aim module', 'Partners'),
  (29, 1, 'Authorize Aim configured', 'You have configured the Authorize Aim module', 'Partners'),
  (30, 1, 'Authorize Aim active', 'Your Authorize Aim module is active', 'Partners'),
  (31, 1, 'Authorize Aim very active', 'Your Authorize Aim module is very active', 'Partners'),
  (32, 1, 'Ebay installed', 'You have installed the Ebay module', 'Partners'),
  (33, 1, 'Ebay configured', 'You have configured the Ebay module', 'Partners'),
  (34, 1, 'Ebay active', 'Your Ebay module is active', 'Partners'),
  (35, 1, 'Ebay very active', 'Your Ebay module is very active', 'Partners'),
  (36, 1, 'PayPlug installed', 'You have installed the PayPlug module', 'Partners'),
  (37, 1, 'PayPlug configured', 'You have configured the PayPlug module', 'Partners'),
  (38, 1, 'PayPlug active', 'Your PayPlug module is active', 'Partners'),
  (39, 1, 'PayPlug very active', 'Your PayPlug module is very active', 'Partners'),
  (40, 1, 'Affinity Items installed', 'You have installed the Affinity Items module', 'Partners'),
  (41, 1, 'Affinity Items configured', 'You have configured the Affinity Items module', 'Partners'),
  (42, 1, 'DPD Poland installed', 'You have installed the DPD Poland module', 'Partners'),
  (43, 1, 'DPD Poland configured', 'You have configured the DPD Poland module', 'Partners'),
  (44, 1, 'DPD Poland active', 'Your DPD Poland module is active', 'Partners'),
  (45, 1, 'DPD Poland very active', 'Your DPD Poland module is very active', 'Partners'),
  (46, 1, 'Envoimoinscher installed', 'You have installed the Envoimoinscher module', 'Partners'),
  (47, 1, 'Envoimoinscher configured', 'You have configured the Envoimoinscher module', 'Partners'),
  (48, 1, 'Envoimoinscher active', 'Your Envoimoinscher module is active', 'Partners'),
  (49, 1, 'Envoimoinscher very active', 'Your Envoimoinscher module is very active', 'Partners'),
  (50, 1, 'Klik&Pay installed', 'You have installed the Klik&Pay module', 'Partners'),
  (51, 1, 'Klik&Pay configured', 'You have configured the Klik&Pay module', 'Partners'),
  (52, 1, 'Klik&Pay active', 'Your Klik&Pay module is active', 'Partners'),
  (53, 1, 'Klik&Pay very active', 'Your Klik&Pay module is very active', 'Partners'),
  (54, 1, 'Clickline installed', 'You have installed the Clickline module', 'Partners'),
  (55, 1, 'Clickline configured', 'You have configured the Clickline module', 'Partners'),
  (56, 1, 'Clickline active', 'Your Clickline module is active', 'Partners'),
  (57, 1, 'Clickline very active', 'Your Clickline module is very active', 'Partners'),
  (58, 1, 'CDiscount installed', 'You have installed the CDiscount module', 'Partners'),
  (59, 1, 'CDiscount configured', 'You have configured the CDiscount module', 'Partners'),
  (60, 1, 'CDiscount active', 'Your CDiscount module is active', 'Partners'),
  (61, 1, 'CDiscount very active', 'Your CDiscount module is very active', 'Partners'),
  (62, 1, 'illicoPresta installed', 'You have installed the illicoPresta module', 'Partners'),
  (63, 1, 'illicoPresta configured', 'You have configured the illicoPresta module', 'Partners'),
  (64, 1, 'illicoPresta active', 'Your illicoPresta module is active', 'Partners'),
  (65, 1, 'illicoPresta very active', 'Your illicoPresta module is very active', 'Partners'),
  (66, 1, 'NetReviews installed', 'You have installed the NetReviews module', 'Partners'),
  (67, 1, 'NetReviews configured', 'You have configured the NetReviews module', 'Partners'),
  (68, 1, 'NetReviews active', 'Your NetReviews module is active', 'Partners'),
  (69, 1, 'NetReviews very active', 'Your NetReviews module is very active', 'Partners'),
  (70, 1, 'Bluesnap installed', 'You have installed the Bluesnap module', 'Partners'),
  (71, 1, 'Bluesnap configured', 'You have configured the Bluesnap module', 'Partners'),
  (72, 1, 'Bluesnap active', 'Your Bluesnap module is active', 'Partners'),
  (73, 1, 'Bluesnap very active', 'Your Bluesnap module is very active', 'Partners'),
  (74, 1, 'Desjardins installed', 'You have installed the Desjardins module', 'Partners'),
  (75, 1, 'Desjardins configured', 'You have configured the Desjardins module', 'Partners'),
  (76, 1, 'Desjardins active', 'Your Desjardins module is active', 'Partners'),
  (77, 1, 'Desjardins very active', 'Your Desjardins module is very active', 'Partners'),
  (78, 1, 'First Data installed', 'You have installed the First Data module', 'Partners'),
  (79, 1, 'First Data configured', 'You have configured the First Data module', 'Partners'),
  (80, 1, 'First Data active', 'Your First Data module is active', 'Partners'),
  (81, 1, 'First Data very active', 'Your First Data module is very active', 'Partners'),
  (82, 1, 'Give.it installed', 'You have installed the Give.it module', 'Partners'),
  (83, 1, 'Give.it configured', 'You have configured the Give.it module', 'Partners'),
  (84, 1, 'Give.it active', 'Your Give.it module is active', 'Partners'),
  (85, 1, 'Give.it very active', 'Your Give.it module is very active', 'Partners'),
  (86, 1, 'Google Analytics installed', 'You have installed the Google Analytics module', 'Partners'),
  (87, 1, 'Google Analytics configured', 'You have configured the Google Analytics module', 'Partners'),
  (88, 1, 'Google Analytics active', 'Your Google Analytics module is active', 'Partners'),
  (89, 1, 'Google Analytics very active', 'Your Google Analytics module is very active', 'Partners'),
  (90, 1, 'PagSeguro installed', 'You have installed the PagSeguro module', 'Partners'),
  (91, 1, 'PagSeguro configured', 'You have configured the PagSeguro module', 'Partners'),
  (92, 1, 'PagSeguro active', 'Your PagSeguro module is active', 'Partners'),
  (93, 1, 'PagSeguro very active', 'Your PagSeguro module is very active', 'Partners'),
  (94, 1, 'Paypal MX installed', 'You have installed the Paypal MX module', 'Partners'),
  (95, 1, 'Paypal MX configured', 'You have configured the Paypal MX module', 'Partners'),
  (96, 1, 'Paypal MX active', 'Your Paypal MX module is active', 'Partners'),
  (97, 1, 'Paypal MX very active', 'Your Paypal MX module is very active', 'Partners'),
  (98, 1, 'Paypal USA installed', 'You have installed the Paypal USA module', 'Partners'),
  (99, 1, 'Paypal USA configured', 'You have configured the Paypal USA module', 'Partners'),
  (100, 1, 'Paypal USA active', 'Your Paypal USA module is active', 'Partners'),
  (101, 1, 'Paypal USA very active', 'Your Paypal USA module is very active', 'Partners'),
  (102, 1, 'PayULatam installed', 'You have installed the PayULatam module', 'Partners'),
  (103, 1, 'PayULatam configured', 'You have configured the PayULatam module', 'Partners'),
  (104, 1, 'PayULatam active', 'Your PayULatam module is active', 'Partners'),
  (105, 1, 'PayULatam very active', 'Your PayULatam module is very active', 'Partners'),
  (106, 1, 'PrestaStats installed', 'You have installed the PrestaStats module', 'Partners'),
  (107, 1, 'PrestaStats configured', 'You have configured the PrestaStats module', 'Partners'),
  (108, 1, 'PrestaStats active', 'Your PrestaStats module is active', 'Partners'),
  (109, 1, 'PrestaStats very active', 'Your PrestaStats module is very active', 'Partners'),
  (110, 1, 'Riskified installed', 'You have installed the Riskified module', 'Partners'),
  (111, 1, 'Riskified configured', 'You have configured the Riskified module', 'Partners'),
  (112, 1, 'Riskified active', 'Your Riskified module is active', 'Partners'),
  (113, 1, 'Riskified very active', 'Your Riskified module is very active', 'Partners'),
  (114, 1, 'Simplify installed', 'You have installed the Simplify module', 'Partners'),
  (115, 1, 'Simplify configured', 'You have configured the Simplify module', 'Partners'),
  (116, 1, 'Simplify active', 'Your Simplify module is active', 'Partners'),
  (117, 1, 'Simplify very active', 'Your Simplify module is very active', 'Partners'),
  (118, 1, 'VTPayment installed', 'You have installed the VTPayment module', 'Partners'),
  (119, 1, 'VTPayment configured', 'You have configured the VTPayment module', 'Partners'),
  (120, 1, 'VTPayment active', 'Your VTPayment module is active', 'Partners'),
  (121, 1, 'VTPayment very active', 'Your VTPayment module is very active', 'Partners'),
  (122, 1, 'Yotpo installed', 'You have installed the Yotpo module', 'Partners'),
  (123, 1, 'Yotpo configured', 'You have configured the Yotpo module', 'Partners'),
  (124, 1, 'Yotpo active', 'Your Yotpo module is active', 'Partners'),
  (125, 1, 'Yotpo very active', 'Your Yotpo module is very active', 'Partners'),
  (126, 1, 'Youstice installed', 'You have installed the Youstice module', 'Partners'),
  (127, 1, 'Youstice configured', 'You have configured the Youstice module', 'Partners'),
  (128, 1, 'Youstice active', 'Your Youstice module is active', 'Partners'),
  (129, 1, 'Youstice very active', 'Your Youstice module is very active', 'Partners'),
  (130, 1, 'Loyalty Lion installed', 'You have installed the Loyalty Lion module', 'Partners'),
  (131, 1, 'Loyalty Lion configured', 'You have configured the Loyalty Lion module', 'Partners'),
  (132, 1, 'Loyalty Lion active', 'Your Loyalty Lion module is active', 'Partners'),
  (133, 1, 'Loyalty Lion very active', 'Your Loyalty Lion module is very active', 'Partners'),
  (134, 1, 'SEO', 'You enabled the URL rewriting through the tab "Preferences > SEO and URLs".', 'SEO'),
  (135, 1, 'Site Performance', 'You enabled CCC (Combine, Compress and Cache), Rijndael and Smarty through the tab \r\nAdvanced Parameters > Performance.', 'Site Performance'),
  (136, 1, 'Site Performance', 'You enabled media servers through the tab "Advanced parameters > Performance".', 'Site Performance'),
  (137, 1, 'Payment', 'You configured a payment solution on your shop.', 'Payment'),
  (138, 1, 'Payment', 'You offer two different payment methods to your customers.', 'Payment'),
  (139, 1, 'Shipping', 'You configured a carrier on your shop.', 'Shipping'),
  (140, 1, 'Shipping', 'You offer two shipping solutions (carriers) to your customers.', 'Shipping'),
  (141, 1, 'Catalog Size', 'You added your first product to your catalog!', 'Catalog Size'),
  (142, 1, 'Catalog Size', 'You have 10 products within your catalog.', 'Catalog Size'),
  (143, 1, 'Contact information', 'You configured your phone number so your customers can reach you!', 'Contact information'),
  (144, 1, 'Contact information', 'You added a third email address to your contact form.', 'Contact information'),
  (145, 1, 'Contact information', 'You suggest a total of 5 departments to be reached by your customers via your contact form.', 'Contact information'),
  (146, 1, 'Catalog Size', 'You have 100 products within your catalog.', 'Catalog Size'),
  (147, 1, 'Catalog Size', 'You have 1,000 products within your catalog.', 'Catalog Size'),
  (148, 1, 'Catalog Size', 'You have 10,000 products within your catalog.', 'Catalog Size'),
  (149, 1, 'Catalog Size', 'You have 100,000 products within your catalog.', 'Catalog Size'),
  (150, 1, 'Days of Experience', 'You just installed PrestaShop!', 'Days of Experience'),
  (151, 1, 'Days of Experience', 'You installed PrestaShop a week ago!', 'Days of Experience'),
  (152, 1, 'Customization', 'You uploaded your own logo.', 'Customization'),
  (153, 1, 'Customization', 'You installed a new template.', 'Customization'),
  (154, 1, 'Addons', 'You connected your back-office to the Addons platform using your PrestaShop Addons account.', 'Addons'),
  (155, 1, 'Multistores', 'You enabled the Multistores feature.', 'Multistores'),
  (156, 1, 'Multistores', 'You manage two shops with the Multistores feature.', 'Multistores'),
  (157, 1, 'Multistores', 'You manage two different groups of shops using the Multistores feature.', 'Multistores'),
  (158, 1, 'Multistores', 'You manage five shops with the Multistores feature.', 'Multistores'),
  (159, 1, 'Multistores', 'You manage five different groups of shops using the Multistores feature.', 'Multistores'),
  (160, 1, 'Shipping', 'You offer three different shipping solutions (carriers) to your customers.', 'Shipping'),
  (161, 1, 'Payment', 'You offer three different payment methods to your customers.', 'Payment'),
  (162, 1, 'Revenue', 'You get this badge when you reach 3000 CZK in sales.', 'Revenue'),
  (163, 1, 'Revenue', 'You get this badge when you reach 10000 CZK in sales.', 'Revenue'),
  (164, 1, 'Revenue', 'You get this badge when you reach 10000 CZK in sales.', 'Revenue'),
  (165, 1, 'Revenue', 'You get this badge when you reach 3000 CZK in sales.', 'Revenue'),
  (166, 1, 'Revenue', 'You get this badge when you reach 10000 CZK in sales.', 'Revenue'),
  (167, 1, 'Revenue', 'You get this badge when you reach 10000 CZK in sales.', 'Revenue'),
  (168, 1, 'Days of Experience', 'You installed PrestaShop a month ago!', 'Days of Experience'),
  (169, 1, 'Days of Experience', 'You installed PrestaShop six months ago!', 'Days of Experience'),
  (170, 1, 'Days of Experience', 'You installed PrestaShop a year ago!', 'Days of Experience'),
  (171, 1, 'Days of Experience', 'You installed PrestaShop two years ago!', 'Days of Experience'),
  (172, 1, 'Visitors', 'You reached 10 visitors!', 'Visitors'),
  (173, 1, 'Visitors', 'You reached 100 visitors!', 'Visitors'),
  (174, 1, 'Visitors', 'You reached 1,000 visitors!', 'Visitors'),
  (175, 1, 'Visitors', 'You reached 10,000 visitors!', 'Visitors'),
  (176, 1, 'Visitors', 'You reached 100,000 visitors!', 'Visitors'),
  (177, 1, 'Visitors', 'You reached 1,000,000 visitors!', 'Visitors'),
  (178, 1, 'Customer Carts', 'Two carts have been created by visitors', 'Customer Carts'),
  (179, 1, 'Customer Carts', 'Ten carts have been created by visitors.', 'Customer Carts'),
  (180, 1, 'Customer Carts', 'A hundred carts have been created by visitors on your shop.', 'Customer Carts'),
  (181, 1, 'Customer Carts', 'A thousand carts have been created by visitors on your shop.', 'Customer Carts'),
  (182, 1, 'Customer Carts', '10,000 carts have been created by visitors.', 'Customer Carts'),
  (183, 1, 'Customer Carts', '100,000 carts have been created by visitors.', 'Customer Carts'),
  (184, 1, 'Orders', 'You received your first order.', 'Orders'),
  (185, 1, 'Orders', '10 orders have been placed through your online shop.', 'Orders'),
  (186, 1, 'Orders', 'You received 100 orders through your online shop!', 'Orders'),
  (187, 1, 'Orders', 'You received 1,000 orders through your online shop, congrats!', 'Orders'),
  (188, 1, 'Orders', 'You received 10,000 orders through your online shop, cheers!', 'Orders'),
  (189, 1, 'Orders', 'You received 100,000 orders through your online shop!', 'Orders'),
  (190, 1, 'Customer Service Threads', 'You received  your first customer''s message.', 'Customer Service Threads'),
  (191, 1, 'Customer Service Threads', 'You received 10 messages from your customers.', 'Customer Service Threads'),
  (192, 1, 'Customer Service Threads', 'You received 100 messages from your customers.', 'Customer Service Threads'),
  (193, 1, 'Customer Service Threads', 'You received 1,000 messages from your customers.', 'Customer Service Threads'),
  (194, 1, 'Customer Service Threads', 'You received 10,000 messages from your customers.', 'Customer Service Threads'),
  (195, 1, 'Customer Service Threads', 'You received 100,000 messages from your customers.', 'Customer Service Threads'),
  (196, 1, 'Customers', 'You got the first customer registered on your shop!', 'Customers'),
  (197, 1, 'Customers', 'You have over 10 customers registered on your shop.', 'Customers'),
  (198, 1, 'Customers', 'You have over 100 customers registered on your shop.', 'Customers'),
  (199, 1, 'Customers', 'You have over 1,000 customers registered on your shop.', 'Customers'),
  (200, 1, 'Customers', 'You have over 10,000 customers registered on your shop.', 'Customers'),
  (201, 1, 'Customers', 'You have over 100,000 customers registered on your shop.', 'Customers'),
  (202, 1, 'Western Europe', 'You got your first sale in Western Europe!', 'Western Europe'),
  (203, 1, 'Southern Europe', 'You got your first sale in Southern Europe!', 'Southern Europe'),
  (204, 1, 'Eastern Europe', 'You got your first sale in Eastern Europe!', 'Eastern Europe'),
  (205, 1, 'Central Europe', 'You got your first sale in Central Europe!', 'Central Europe'),
  (206, 1, 'Northern Europe', 'You got your first sale in Northern Europe!', 'Northern Europe'),
  (207, 1, 'North America', 'You got your first sale in North America', 'North America'),
  (208, 1, 'Oceania', 'You got your first sale in Oceania', 'Oceania'),
  (209, 1, 'Asia', 'You got your first sale in Asia', 'Asia'),
  (210, 1, 'South America', 'You got your first sale in South America', 'South America'),
  (211, 1, 'Africa', 'You got your first sale in Africa', 'Africa'),
  (212, 1, 'Maghreb', 'You got your first sale in Maghreb', 'Maghreb'),
  (213, 1, 'Your Team''s Employees', 'First employee account added to your shop', 'Your Team''s Employees'),
  (214, 1, 'Your Team''s Employees', '3 employee accounts added to your shop', 'Your Team''s Employees'),
  (215, 1, 'Your Team''s Employees', '5 employee accounts added to your shop', 'Your Team''s Employees'),
  (216, 1, 'Your Team''s Employees', '10 employee accounts added to your shop', 'Your Team''s Employees'),
  (217, 1, 'Your Team''s Employees', '20 employee accounts added to your shop', 'Your Team''s Employees'),
  (218, 1, 'Your Team''s Employees', '40 employee accounts added to your shop', 'Your Team''s Employees'),
  (219, 1, 'Product Pictures', 'First photo added to your catalog', 'Product Pictures'),
  (220, 1, 'Product Pictures', '50 photos added to your catalog', 'Product Pictures'),
  (221, 1, 'Product Pictures', '100 photos added to your catalog', 'Product Pictures'),
  (222, 1, 'Product Pictures', '1,000 photos added to your catalog', 'Product Pictures'),
  (223, 1, 'Product Pictures', '10,000 photos added to your catalog', 'Product Pictures'),
  (224, 1, 'Product Pictures', '50,000 photos added to your catalog', 'Product Pictures'),
  (225, 1, 'Customization', 'First CMS page added to your catalog', 'Customization'),
  (226, 1, 'Cart Rules', 'First cart rules configured on your shop', 'Cart Rules'),
  (227, 1, 'Cart Rules', 'You have 10 cart rules configured on your shop', 'Cart Rules'),
  (228, 1, 'Cart Rules', 'You have 100 cart rules configured on your shop', 'Cart Rules'),
  (229, 1, 'Cart Rules', 'You have 500 cart rules configured on your shop', 'Cart Rules'),
  (230, 1, 'Cart Rules', 'You have 1,000 cart rules configured on your shop', 'Cart Rules'),
  (231, 1, 'Cart Rules', 'You have 5,000 cart rules configured on your shop', 'Cart Rules'),
  (232, 1, 'International Orders', 'First international order placed on your shop.', 'International Orders'),
  (233, 1, 'International Orders', '10 international orders placed on your shop.', 'International Orders'),
  (234, 1, 'International Orders', '100 international orders placed on your shop!', 'International Orders'),
  (235, 1, 'International Orders', '1,000 international orders placed on your shop!', 'International Orders'),
  (236, 1, 'International Orders', '5,000 international orders placed on your shop!', 'International Orders'),
  (237, 1, 'International Orders', '10,000 international orders placed on your shop!', 'International Orders'),
  (238, 1, 'Store', 'First store configured on your shop!', 'Store'),
  (239, 1, 'Store', 'You have 2 stores configured on your shop', 'Store'),
  (240, 1, 'Store', 'You have 5 stores configured on your shop', 'Store'),
  (241, 1, 'Store', 'You have 10 stores configured on your shop', 'Store'),
  (242, 1, 'Store', 'You have 20 stores configured on your shop', 'Store'),
  (243, 1, 'Store', 'You have 50 stores configured on your shop', 'Store'),
  (244, 1, 'Webservice x1', 'First webservice account added to your shop', 'WebService'),
  (245, 1, 'Webservice x2', '2 webservice accounts added to your shop', 'WebService'),
  (246, 1, 'Webservice x3', '3 webservice accounts added to your shop', 'WebService'),
  (247, 1, 'Webservice x4', '4 webservice accounts added to your shop', 'WebService');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_carrier`
--

CREATE TABLE IF NOT EXISTS `ps_carrier` (
  `id_carrier`           INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_reference`         INT(10) UNSIGNED    NOT NULL,
  `id_tax_rules_group`   INT(10) UNSIGNED             DEFAULT '0',
  `name`                 VARCHAR(64)         NOT NULL,
  `url`                  VARCHAR(255)                 DEFAULT NULL,
  `active`               TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `deleted`              TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `shipping_handling`    TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `range_behavior`       TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_module`            TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_free`              TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `shipping_external`    TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `need_range`           TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `external_module_name` VARCHAR(64)                  DEFAULT NULL,
  `shipping_method`      INT(2)              NOT NULL DEFAULT '0',
  `position`             INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `max_width`            INT(10)                      DEFAULT '0',
  `max_height`           INT(10)                      DEFAULT '0',
  `max_depth`            INT(10)                      DEFAULT '0',
  `max_weight`           DECIMAL(20, 6)               DEFAULT '0.000000',
  `grade`                INT(10)                      DEFAULT '0',
  PRIMARY KEY (`id_carrier`),
  KEY `deleted` (`deleted`, `active`),
  KEY `id_tax_rules_group` (`id_tax_rules_group`),
  KEY `reference` (`id_reference`, `deleted`, `active`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 3;

--
-- Vypisuji data pro tabulku `ps_carrier`
--

INSERT INTO `ps_carrier` (`id_carrier`, `id_reference`, `id_tax_rules_group`, `name`, `url`, `active`, `deleted`, `shipping_handling`, `range_behavior`, `is_module`, `is_free`, `shipping_external`, `need_range`, `external_module_name`, `shipping_method`, `position`, `max_width`, `max_height`, `max_depth`, `max_weight`, `grade`)
VALUES
  (1, 1, 0, '0', '', 1, 0, 0, 0, 0, 1, 0, 0, '', 0, 0, 0, 0, 0, 0.000000, 0),
  (2, 2, 0, 'My carrier', '', 1, 0, 1, 0, 0, 0, 0, 0, '', 0, 1, 0, 0, 0, 0.000000, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_carrier_group`
--

CREATE TABLE IF NOT EXISTS `ps_carrier_group` (
  `id_carrier` INT(10) UNSIGNED NOT NULL,
  `id_group`   INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_carrier`, `id_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_carrier_group`
--

INSERT INTO `ps_carrier_group` (`id_carrier`, `id_group`) VALUES
  (1, 1),
  (1, 2),
  (1, 3),
  (2, 1),
  (2, 2),
  (2, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_carrier_lang`
--

CREATE TABLE IF NOT EXISTS `ps_carrier_lang` (
  `id_carrier` INT(10) UNSIGNED NOT NULL,
  `id_shop`    INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang`    INT(10) UNSIGNED NOT NULL,
  `delay`      VARCHAR(128)              DEFAULT NULL,
  PRIMARY KEY (`id_lang`, `id_shop`, `id_carrier`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_carrier_lang`
--

INSERT INTO `ps_carrier_lang` (`id_carrier`, `id_shop`, `id_lang`, `delay`) VALUES
  (1, 1, 1, 'Pick up in-store'),
  (2, 1, 1, 'Delivery next day!');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_carrier_shop`
--

CREATE TABLE IF NOT EXISTS `ps_carrier_shop` (
  `id_carrier` INT(11) UNSIGNED NOT NULL,
  `id_shop`    INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_carrier`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_carrier_shop`
--

INSERT INTO `ps_carrier_shop` (`id_carrier`, `id_shop`) VALUES
  (1, 1),
  (2, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_carrier_tax_rules_group_shop`
--

CREATE TABLE IF NOT EXISTS `ps_carrier_tax_rules_group_shop` (
  `id_carrier`         INT(11) UNSIGNED NOT NULL,
  `id_tax_rules_group` INT(11) UNSIGNED NOT NULL,
  `id_shop`            INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_carrier`, `id_tax_rules_group`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_carrier_tax_rules_group_shop`
--

INSERT INTO `ps_carrier_tax_rules_group_shop` (`id_carrier`, `id_tax_rules_group`, `id_shop`) VALUES
  (1, 1, 1),
  (2, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_carrier_zone`
--

CREATE TABLE IF NOT EXISTS `ps_carrier_zone` (
  `id_carrier` INT(10) UNSIGNED NOT NULL,
  `id_zone`    INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_carrier`, `id_zone`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_carrier_zone`
--

INSERT INTO `ps_carrier_zone` (`id_carrier`, `id_zone`) VALUES
  (1, 1),
  (2, 1),
  (2, 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cart`
--

CREATE TABLE IF NOT EXISTS `ps_cart` (
  `id_cart`                 INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_shop_group`           INT(11) UNSIGNED    NOT NULL DEFAULT '1',
  `id_shop`                 INT(11) UNSIGNED    NOT NULL DEFAULT '1',
  `id_carrier`              INT(10) UNSIGNED    NOT NULL,
  `delivery_option`         TEXT                NOT NULL,
  `id_lang`                 INT(10) UNSIGNED    NOT NULL,
  `id_address_delivery`     INT(10) UNSIGNED    NOT NULL,
  `id_address_invoice`      INT(10) UNSIGNED    NOT NULL,
  `id_currency`             INT(10) UNSIGNED    NOT NULL,
  `id_customer`             INT(10) UNSIGNED    NOT NULL,
  `id_guest`                INT(10) UNSIGNED    NOT NULL,
  `secure_key`              VARCHAR(32)         NOT NULL DEFAULT '-1',
  `recyclable`              TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `gift`                    TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift_message`            TEXT,
  `mobile_theme`            TINYINT(1)          NOT NULL DEFAULT '0',
  `allow_seperated_package` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add`                DATETIME            NOT NULL,
  `date_upd`                DATETIME            NOT NULL,
  PRIMARY KEY (`id_cart`),
  KEY `cart_customer` (`id_customer`),
  KEY `id_address_delivery` (`id_address_delivery`),
  KEY `id_address_invoice` (`id_address_invoice`),
  KEY `id_carrier` (`id_carrier`),
  KEY `id_lang` (`id_lang`),
  KEY `id_currency` (`id_currency`),
  KEY `id_guest` (`id_guest`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `id_shop_2` (`id_shop`, `date_upd`),
  KEY `id_shop` (`id_shop`, `date_add`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 8;

--
-- Vypisuji data pro tabulku `ps_cart`
--

INSERT INTO `ps_cart` (`id_cart`, `id_shop_group`, `id_shop`, `id_carrier`, `delivery_option`, `id_lang`, `id_address_delivery`, `id_address_invoice`, `id_currency`, `id_customer`, `id_guest`, `secure_key`, `recyclable`, `gift`, `gift_message`, `mobile_theme`, `allow_seperated_package`, `date_add`, `date_upd`)
VALUES
  (1, 1, 1, 2, 'a:1:{i:3;s:2:"2,";}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0,
   '2016-08-04 12:42:31', '2016-08-04 12:42:31'),
  (2, 1, 1, 2, 'a:1:{i:3;s:2:"2,";}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0,
   '2016-08-04 12:42:31', '2016-08-04 12:42:31'),
  (3, 1, 1, 2, 'a:1:{i:3;s:2:"2,";}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0,
   '2016-08-04 12:42:31', '2016-08-04 12:42:31'),
  (4, 1, 1, 2, 'a:1:{i:3;s:2:"2,";}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0,
   '2016-08-04 12:42:31', '2016-08-04 12:42:31'),
  (5, 1, 1, 2, 'a:1:{i:3;s:2:"2,";}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0,
   '2016-08-04 12:42:31', '2016-08-04 12:42:31'),
  (6, 1, 1, 0, '', 1, 0, 0, 1, 0, 53, '', 0, 0, '', 0, 0, '2016-09-19 13:50:20', '2016-10-05 11:43:22'),
  (7, 1, 1, 0, '', 1, 0, 0, 1, 0, 188, '', 0, 0, '', 0, 0, '2017-01-25 15:05:10', '2017-01-25 15:05:10');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cart_cart_rule`
--

CREATE TABLE IF NOT EXISTS `ps_cart_cart_rule` (
  `id_cart`      INT(10) UNSIGNED NOT NULL,
  `id_cart_rule` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cart`, `id_cart_rule`),
  KEY `id_cart_rule` (`id_cart_rule`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cart_cart_rule`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cart_product`
--

CREATE TABLE IF NOT EXISTS `ps_cart_product` (
  `id_cart`              INT(10) UNSIGNED NOT NULL,
  `id_product`           INT(10) UNSIGNED NOT NULL,
  `id_address_delivery`  INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_shop`              INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_product_attribute` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `quantity`             INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `date_add`             DATETIME         NOT NULL,
  PRIMARY KEY (`id_cart`, `id_product`, `id_product_attribute`, `id_address_delivery`),
  KEY `id_product_attribute` (`id_product_attribute`),
  KEY `id_cart_order` (`id_cart`, `date_add`, `id_product`, `id_product_attribute`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cart_product`
--

INSERT INTO `ps_cart_product` (`id_cart`, `id_product`, `id_address_delivery`, `id_shop`, `id_product_attribute`, `quantity`, `date_add`)
VALUES
  (1, 3, 3, 1, 13, 1, '0000-00-00 00:00:00'),
  (2, 6, 3, 1, 32, 1, '0000-00-00 00:00:00'),
  (2, 7, 3, 1, 34, 1, '0000-00-00 00:00:00'),
  (3, 1, 3, 1, 1, 1, '0000-00-00 00:00:00'),
  (3, 6, 3, 1, 32, 1, '0000-00-00 00:00:00'),
  (4, 1, 3, 1, 1, 1, '0000-00-00 00:00:00'),
  (4, 3, 3, 1, 13, 1, '0000-00-00 00:00:00'),
  (4, 5, 3, 1, 19, 1, '0000-00-00 00:00:00'),
  (4, 7, 3, 1, 34, 1, '0000-00-00 00:00:00'),
  (5, 1, 3, 1, 1, 1, '0000-00-00 00:00:00'),
  (5, 3, 3, 1, 13, 1, '0000-00-00 00:00:00'),
  (6, 3, 0, 1, 13, 1, '2016-10-05 11:43:22'),
  (6, 5, 0, 1, 19, 1, '2016-10-05 11:11:31'),
  (7, 3, 0, 1, 13, 1, '2017-01-25 15:05:10');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cart_rule`
--

CREATE TABLE IF NOT EXISTS `ps_cart_rule` (
  `id_cart_rule`            INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_customer`             INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `date_from`               DATETIME            NOT NULL,
  `date_to`                 DATETIME            NOT NULL,
  `description`             TEXT,
  `quantity`                INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `quantity_per_user`       INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `priority`                INT(10) UNSIGNED    NOT NULL DEFAULT '1',
  `partial_use`             TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `code`                    VARCHAR(254)        NOT NULL,
  `minimum_amount`          DECIMAL(17, 2)      NOT NULL DEFAULT '0.00',
  `minimum_amount_tax`      TINYINT(1)          NOT NULL DEFAULT '0',
  `minimum_amount_currency` INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `minimum_amount_shipping` TINYINT(1)          NOT NULL DEFAULT '0',
  `country_restriction`     TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `carrier_restriction`     TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `group_restriction`       TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `cart_rule_restriction`   TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `product_restriction`     TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `shop_restriction`        TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `free_shipping`           TINYINT(1)          NOT NULL DEFAULT '0',
  `reduction_percent`       DECIMAL(5, 2)       NOT NULL DEFAULT '0.00',
  `reduction_amount`        DECIMAL(17, 2)      NOT NULL DEFAULT '0.00',
  `reduction_tax`           TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `reduction_currency`      INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `reduction_product`       INT(10)             NOT NULL DEFAULT '0',
  `gift_product`            INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `gift_product_attribute`  INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `highlight`               TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `active`                  TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add`                DATETIME            NOT NULL,
  `date_upd`                DATETIME            NOT NULL,
  PRIMARY KEY (`id_cart_rule`),
  KEY `id_customer` (`id_customer`, `active`, `date_to`),
  KEY `group_restriction` (`group_restriction`, `active`, `date_to`),
  KEY `id_customer_2` (`id_customer`, `active`, `highlight`, `date_to`),
  KEY `group_restriction_2` (`group_restriction`, `active`, `highlight`, `date_to`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_cart_rule`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cart_rule_carrier`
--

CREATE TABLE IF NOT EXISTS `ps_cart_rule_carrier` (
  `id_cart_rule` INT(10) UNSIGNED NOT NULL,
  `id_carrier`   INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cart_rule`, `id_carrier`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cart_rule_carrier`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cart_rule_combination`
--

CREATE TABLE IF NOT EXISTS `ps_cart_rule_combination` (
  `id_cart_rule_1` INT(10) UNSIGNED NOT NULL,
  `id_cart_rule_2` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cart_rule_1`, `id_cart_rule_2`),
  KEY `id_cart_rule_1` (`id_cart_rule_1`),
  KEY `id_cart_rule_2` (`id_cart_rule_2`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cart_rule_combination`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cart_rule_country`
--

CREATE TABLE IF NOT EXISTS `ps_cart_rule_country` (
  `id_cart_rule` INT(10) UNSIGNED NOT NULL,
  `id_country`   INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cart_rule`, `id_country`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cart_rule_country`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cart_rule_group`
--

CREATE TABLE IF NOT EXISTS `ps_cart_rule_group` (
  `id_cart_rule` INT(10) UNSIGNED NOT NULL,
  `id_group`     INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cart_rule`, `id_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cart_rule_group`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cart_rule_lang`
--

CREATE TABLE IF NOT EXISTS `ps_cart_rule_lang` (
  `id_cart_rule` INT(10) UNSIGNED NOT NULL,
  `id_lang`      INT(10) UNSIGNED NOT NULL,
  `name`         VARCHAR(254)     NOT NULL,
  PRIMARY KEY (`id_cart_rule`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cart_rule_lang`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cart_rule_product_rule`
--

CREATE TABLE IF NOT EXISTS `ps_cart_rule_product_rule` (
  `id_product_rule`       INT(10) UNSIGNED                                                            NOT NULL AUTO_INCREMENT,
  `id_product_rule_group` INT(10) UNSIGNED                                                            NOT NULL,
  `type`                  ENUM ('products', 'categories', 'attributes', 'manufacturers', 'suppliers') NOT NULL,
  PRIMARY KEY (`id_product_rule`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_cart_rule_product_rule`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cart_rule_product_rule_group`
--

CREATE TABLE IF NOT EXISTS `ps_cart_rule_product_rule_group` (
  `id_product_rule_group` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_cart_rule`          INT(10) UNSIGNED NOT NULL,
  `quantity`              INT(10) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_product_rule_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_cart_rule_product_rule_group`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cart_rule_product_rule_value`
--

CREATE TABLE IF NOT EXISTS `ps_cart_rule_product_rule_value` (
  `id_product_rule` INT(10) UNSIGNED NOT NULL,
  `id_item`         INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product_rule`, `id_item`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cart_rule_product_rule_value`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cart_rule_shop`
--

CREATE TABLE IF NOT EXISTS `ps_cart_rule_shop` (
  `id_cart_rule` INT(10) UNSIGNED NOT NULL,
  `id_shop`      INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cart_rule`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cart_rule_shop`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_category`
--

CREATE TABLE IF NOT EXISTS `ps_category` (
  `id_category`      INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_parent`        INT(10) UNSIGNED    NOT NULL,
  `id_shop_default`  INT(10) UNSIGNED    NOT NULL DEFAULT '1',
  `level_depth`      TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `nleft`            INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `nright`           INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `active`           TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add`         DATETIME            NOT NULL,
  `date_upd`         DATETIME            NOT NULL,
  `position`         INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `is_root_category` TINYINT(1)          NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_category`),
  KEY `category_parent` (`id_parent`),
  KEY `nleftrightactive` (`nleft`, `nright`, `active`),
  KEY `level_depth` (`level_depth`),
  KEY `nright` (`nright`),
  KEY `activenleft` (`active`, `nleft`),
  KEY `activenright` (`active`, `nright`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 35;

--
-- Vypisuji data pro tabulku `ps_category`
--

INSERT INTO `ps_category` (`id_category`, `id_parent`, `id_shop_default`, `level_depth`, `nleft`, `nright`, `active`, `date_add`, `date_upd`, `position`, `is_root_category`)
VALUES
  (1, 0, 1, 0, 1, 54, 1, '2016-08-04 12:42:23', '2016-08-04 12:42:23', 0, 0),
  (2, 1, 1, 1, 2, 53, 1, '2016-08-04 12:42:23', '2016-08-04 12:42:23', 0, 1),
  (3, 2, 1, 2, 3, 26, 1, '2016-08-04 12:42:30', '2017-01-23 19:21:03', 0, 0),
  (4, 20, 1, 4, 5, 6, 1, '2016-08-04 12:42:30', '2017-02-10 11:09:59', 0, 0),
  (8, 20, 1, 4, 7, 8, 1, '2016-08-04 12:42:30', '2017-01-23 19:13:19', 1, 0),
  (12, 2, 1, 2, 31, 52, 1, '2016-09-19 13:59:10', '2017-01-23 19:34:38', 2, 0),
  (13, 20, 1, 4, 9, 10, 1, '2017-01-23 18:40:28', '2017-02-10 11:10:28', 2, 0),
  (14, 20, 1, 4, 11, 12, 1, '2017-01-23 18:41:17', '2017-01-23 19:13:15', 3, 0),
  (15, 20, 1, 4, 13, 14, 1, '2017-01-23 18:41:47', '2017-02-10 10:35:41', 4, 0),
  (16, 21, 1, 4, 17, 18, 1, '2017-01-23 18:42:49', '2017-01-23 19:01:49', 0, 0),
  (17, 21, 1, 4, 19, 20, 1, '2017-01-23 18:43:25', '2017-01-23 19:01:49', 1, 0),
  (18, 21, 1, 4, 21, 22, 1, '2017-01-23 18:43:54', '2017-01-23 19:01:49', 2, 0),
  (19, 21, 1, 4, 23, 24, 1, '2017-01-23 18:44:19', '2017-01-23 19:01:49', 3, 0),
  (20, 3, 1, 3, 4, 15, 1, '2017-01-23 18:56:45', '2017-01-25 14:35:12', 9, 0),
  (21, 3, 1, 3, 16, 25, 1, '2017-01-23 19:00:02', '2017-01-23 19:00:02', 0, 0),
  (22, 2, 1, 2, 27, 28, 1, '2017-01-23 19:18:44', '2017-01-23 19:34:59', 3, 0),
  (23, 12, 1, 3, 32, 33, 1, '2017-01-23 19:18:56', '2017-01-23 19:33:29', 1, 0),
  (24, 12, 1, 3, 34, 51, 1, '2017-01-23 19:19:13', '2017-01-23 19:20:40', 2, 0),
  (26, 2, 1, 2, 29, 30, 1, '2017-01-23 19:21:27', '2017-01-23 19:34:27', 1, 0),
  (27, 24, 1, 4, 35, 36, 1, '2017-01-23 19:29:58', '2017-01-23 19:29:58', 0, 0),
  (28, 24, 1, 4, 37, 38, 1, '2017-01-23 19:30:28', '2017-01-23 19:30:28', 0, 0),
  (29, 24, 1, 4, 39, 40, 1, '2017-01-23 19:30:56', '2017-01-23 19:30:56', 0, 0),
  (30, 24, 1, 4, 41, 42, 1, '2017-01-23 19:31:19', '2017-01-23 19:31:19', 0, 0),
  (31, 24, 1, 4, 43, 44, 1, '2017-01-23 19:31:41', '2017-01-23 19:31:41', 0, 0),
  (32, 24, 1, 4, 45, 46, 1, '2017-01-23 19:31:57', '2017-01-23 19:31:57', 0, 0),
  (33, 24, 1, 4, 47, 48, 1, '2017-01-23 19:32:08', '2017-01-23 19:32:08', 0, 0),
  (34, 24, 1, 4, 49, 50, 1, '2017-01-23 19:32:23', '2017-01-23 19:32:23', 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_category_group`
--

CREATE TABLE IF NOT EXISTS `ps_category_group` (
  `id_category` INT(10) UNSIGNED NOT NULL,
  `id_group`    INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_category`, `id_group`),
  KEY `id_category` (`id_category`),
  KEY `id_group` (`id_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_category_group`
--

INSERT INTO `ps_category_group` (`id_category`, `id_group`) VALUES
  (2, 0),
  (2, 1),
  (2, 2),
  (2, 3),
  (3, 1),
  (3, 2),
  (3, 3),
  (4, 1),
  (4, 2),
  (4, 3),
  (8, 1),
  (8, 2),
  (8, 3),
  (12, 1),
  (12, 2),
  (12, 3),
  (13, 1),
  (13, 2),
  (13, 3),
  (14, 1),
  (14, 2),
  (14, 3),
  (15, 1),
  (15, 2),
  (15, 3),
  (16, 1),
  (16, 2),
  (16, 3),
  (17, 1),
  (17, 2),
  (17, 3),
  (18, 1),
  (18, 2),
  (18, 3),
  (19, 1),
  (19, 2),
  (19, 3),
  (20, 1),
  (20, 2),
  (20, 3),
  (21, 1),
  (21, 2),
  (21, 3),
  (22, 1),
  (22, 2),
  (22, 3),
  (23, 1),
  (23, 2),
  (23, 3),
  (24, 1),
  (24, 2),
  (24, 3),
  (26, 1),
  (26, 2),
  (26, 3),
  (27, 1),
  (27, 2),
  (27, 3),
  (28, 1),
  (28, 2),
  (28, 3),
  (29, 1),
  (29, 2),
  (29, 3),
  (30, 1),
  (30, 2),
  (30, 3),
  (31, 1),
  (31, 2),
  (31, 3),
  (32, 1),
  (32, 2),
  (32, 3),
  (33, 1),
  (33, 2),
  (33, 3),
  (34, 1),
  (34, 2),
  (34, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_category_lang`
--

CREATE TABLE IF NOT EXISTS `ps_category_lang` (
  `id_category`      INT(10) UNSIGNED NOT NULL,
  `id_shop`          INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang`          INT(10) UNSIGNED NOT NULL,
  `name`             VARCHAR(128)     NOT NULL,
  `description`      TEXT,
  `link_rewrite`     VARCHAR(128)     NOT NULL,
  `meta_title`       VARCHAR(128)              DEFAULT NULL,
  `meta_keywords`    VARCHAR(255)              DEFAULT NULL,
  `meta_description` VARCHAR(255)              DEFAULT NULL,
  PRIMARY KEY (`id_category`, `id_shop`, `id_lang`),
  KEY `category_name` (`name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_category_lang`
--

INSERT INTO `ps_category_lang` (`id_category`, `id_shop`, `id_lang`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`)
VALUES
  (1, 1, 1, 'Root', '', 'root', '', '', ''),
  (2, 1, 1, 'Domů', '', 'domu', '', '', ''),
  (3, 1, 1, 'Oční čočky', '', 'ocni-cocky', '', '', ''),
  (4, 1, 1, 'Jednodenní', '<p>Lorem ipsum dolor sit amet consectetuer dui consequat Aenean enim natoque. Nibh at Curabitur quam volutpat Aliquam Vivamus orci vel orci at. Non vel nulla auctor lorem quis adipiscing dui Maecenas vel Curabitur. Porta dui Mauris eget convallis sem Phasellus tortor feugiat id In. Enim lacus mauris in quis interdum ligula congue adipiscing tempor velit. Suspendisse Vestibulum volutpat scelerisque laoreet Suspendisse ligula consequat auctor convallis tincidunt. Tincidunt orci.<br /><br />Sem tellus congue non tortor magna cursus commodo orci tellus et. Tincidunt Aliquam Sed a justo semper enim Nam Phasellus turpis vitae. Auctor non ut interdum urna nec nibh enim lorem urna metus. Proin sem justo convallis semper laoreet Praesent Vestibulum augue odio ligula. Condimentum semper Nullam nascetur enim feugiat nibh In magnis Sed pretium. Leo augue Donec vestibulum ornare Sed Aenean lacinia non vitae.<br /><br /><br /></p>', 'jednodenni', '', '', ''),
  (8, 1, 1, 'Čtrnáctidenní', '', 'ctrnactidenni', '', '', ''),
  (12, 1, 1, 'Příslušenství', '', 'prislusenstvi', '', '', ''),
  (13, 1, 1, 'Měsíční', '<p>popis</p>', 'mesicni', '', '', ''),
  (14, 1, 1, 'Čtvrtletní', '', 'ctvrtletni', '', '', ''),
  (15, 1, 1, 'Roční', '<p>Lorem ipsum dolor sit amet consectetuer dui consequat Aenean enim natoque. Nibh at Curabitur quam volutpat Aliquam Vivamus orci vel orci at. Non vel nulla auctor lorem quis adipiscing dui Maecenas vel Curabitur. Porta dui Mauris eget convallis sem Phasellus tortor feugiat id In. Enim lacus mauris in quis interdum ligula congue adipiscing tempor velit. Suspendisse Vestibulum volutpat scelerisque laoreet Suspendisse ligula consequat auctor convallis tincidunt. Tincidunt orci.</p>', 'rocni-kontaktni-cocky', 'Roční kontaktní čočky', '', ''),
  (16, 1, 1, 'Bifokální', '', 'bifokalni', '', '', ''),
  (17, 1, 1, 'Multifokální', '', 'multifokalni', '', '', ''),
  (18, 1, 1, 'Torické', '', 'toricke', '', '', ''),
  (19, 1, 1, 'Barevné', '', 'barevne', '', '', ''),
  (20, 1, 1, 'Časové čočky', '<p>jsou fajn</p>', 'casove-cocky', '', '', ''),
  (21, 1, 1, 'Typy čoček', '', 'typy-cocek', '', '', ''),
  (22, 1, 1, 'Vyšetření a aplikace', '', 'vysetreni-a-aplikace', '', '', ''),
  (23, 1, 1, 'Roztoky', '', 'roztoky', '', '', ''),
  (24, 1, 1, 'Pouzdra', '', 'pouzdra', '', '', ''),
  (26, 1, 1, 'Výživa zraku', '', 'vyziva-zraku', '', '', ''),
  (27, 1, 1, 'Antibakteriální', '', 'antibakterialni', '', '', ''),
  (28, 1, 1, 'Se zrcátkem', '', 'se-zrcatkem', '', '', ''),
  (29, 1, 1, 'Kožená', '', 'kozena', '', '', ''),
  (30, 1, 1, 'Klasická ozdobná', '', 'klasicka-ozdobna', '', '', ''),
  (31, 1, 1, 'Na jednodenní čočky', '', 'na-jednodenni-cocky', '', '', ''),
  (32, 1, 1, 'Barely', '', 'barely', '', '', ''),
  (33, 1, 1, 'Náhradní', '', 'nahradni', '', '', ''),
  (34, 1, 1, 'Na tvrdé čočky', '', 'na-tvrde-cocky', '', '', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_category_product`
--

CREATE TABLE IF NOT EXISTS `ps_category_product` (
  `id_category` INT(10) UNSIGNED NOT NULL,
  `id_product`  INT(10) UNSIGNED NOT NULL,
  `position`    INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_category`, `id_product`),
  KEY `id_product` (`id_product`),
  KEY `id_category` (`id_category`, `position`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_category_product`
--

INSERT INTO `ps_category_product` (`id_category`, `id_product`, `position`) VALUES
  (2, 1, 0),
  (2, 3, 1),
  (2, 4, 2),
  (2, 5, 3),
  (2, 6, 4),
  (2, 7, 5),
  (3, 1, 0),
  (3, 3, 1),
  (3, 4, 2),
  (3, 5, 3),
  (3, 6, 4),
  (3, 7, 5),
  (4, 1, 0),
  (8, 3, 0),
  (8, 4, 1),
  (8, 5, 2),
  (8, 6, 3),
  (8, 7, 4),
  (8, 1, 5),
  (13, 1, 0),
  (14, 1, 0),
  (15, 3, 0),
  (15, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_category_shop`
--

CREATE TABLE IF NOT EXISTS `ps_category_shop` (
  `id_category` INT(11)          NOT NULL,
  `id_shop`     INT(11)          NOT NULL,
  `position`    INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_category`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_category_shop`
--

INSERT INTO `ps_category_shop` (`id_category`, `id_shop`, `position`) VALUES
  (1, 1, 0),
  (2, 1, 0),
  (3, 1, 0),
  (4, 1, 0),
  (8, 1, 1),
  (12, 1, 2),
  (13, 1, 2),
  (14, 1, 3),
  (15, 1, 4),
  (16, 1, 0),
  (17, 1, 1),
  (18, 1, 2),
  (19, 1, 3),
  (20, 1, 9),
  (21, 1, 10),
  (22, 1, 3),
  (23, 1, 1),
  (24, 1, 2),
  (26, 1, 1),
  (27, 1, 0),
  (28, 1, 1),
  (29, 1, 2),
  (30, 1, 3),
  (31, 1, 4),
  (32, 1, 5),
  (33, 1, 6),
  (34, 1, 7);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cms`
--

CREATE TABLE IF NOT EXISTS `ps_cms` (
  `id_cms`          INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_cms_category` INT(10) UNSIGNED    NOT NULL,
  `position`        INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `active`          TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `indexation`      TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_cms`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 8;

--
-- Vypisuji data pro tabulku `ps_cms`
--

INSERT INTO `ps_cms` (`id_cms`, `id_cms_category`, `position`, `active`, `indexation`) VALUES
  (1, 2, 0, 1, 0),
  (2, 3, 0, 1, 0),
  (3, 2, 1, 1, 0),
  (4, 2, 2, 1, 0),
  (5, 2, 3, 1, 0),
  (6, 3, 1, 1, 0),
  (7, 3, 2, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cms_block`
--

CREATE TABLE IF NOT EXISTS `ps_cms_block` (
  `id_cms_block`    INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_cms_category` INT(10) UNSIGNED    NOT NULL,
  `location`        TINYINT(1) UNSIGNED NOT NULL,
  `position`        INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `display_store`   TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_cms_block`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_cms_block`
--

INSERT INTO `ps_cms_block` (`id_cms_block`, `id_cms_category`, `location`, `position`, `display_store`) VALUES
  (1, 2, 0, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cms_block_lang`
--

CREATE TABLE IF NOT EXISTS `ps_cms_block_lang` (
  `id_cms_block` INT(10) UNSIGNED NOT NULL,
  `id_lang`      INT(10) UNSIGNED NOT NULL,
  `name`         VARCHAR(40)      NOT NULL DEFAULT '',
  PRIMARY KEY (`id_cms_block`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cms_block_lang`
--

INSERT INTO `ps_cms_block_lang` (`id_cms_block`, `id_lang`, `name`) VALUES
  (1, 1, 'Informace');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cms_block_page`
--

CREATE TABLE IF NOT EXISTS `ps_cms_block_page` (
  `id_cms_block_page` INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_cms_block`      INT(10) UNSIGNED    NOT NULL,
  `id_cms`            INT(10) UNSIGNED    NOT NULL,
  `is_category`       TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cms_block_page`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 11;

--
-- Vypisuji data pro tabulku `ps_cms_block_page`
--

INSERT INTO `ps_cms_block_page` (`id_cms_block_page`, `id_cms_block`, `id_cms`, `is_category`) VALUES
  (6, 1, 1, 0),
  (7, 1, 3, 0),
  (8, 1, 4, 0),
  (9, 1, 5, 0),
  (10, 1, 3, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cms_block_shop`
--

CREATE TABLE IF NOT EXISTS `ps_cms_block_shop` (
  `id_cms_block` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop`      INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cms_block`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_cms_block_shop`
--

INSERT INTO `ps_cms_block_shop` (`id_cms_block`, `id_shop`) VALUES
  (1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cms_category`
--

CREATE TABLE IF NOT EXISTS `ps_cms_category` (
  `id_cms_category` INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_parent`       INT(10) UNSIGNED    NOT NULL,
  `level_depth`     TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `active`          TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add`        DATETIME            NOT NULL,
  `date_upd`        DATETIME            NOT NULL,
  `position`        INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_cms_category`),
  KEY `category_parent` (`id_parent`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 4;

--
-- Vypisuji data pro tabulku `ps_cms_category`
--

INSERT INTO `ps_cms_category` (`id_cms_category`, `id_parent`, `level_depth`, `active`, `date_add`, `date_upd`, `position`)
VALUES
  (1, 0, 1, 1, '2016-08-04 12:42:23', '2016-08-04 12:42:23', 0),
  (2, 1, 2, 1, '2017-01-23 19:50:24', '2017-01-23 19:51:41', 0),
  (3, 2, 3, 1, '2017-01-23 19:52:09', '2017-01-23 19:53:15', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cms_category_lang`
--

CREATE TABLE IF NOT EXISTS `ps_cms_category_lang` (
  `id_cms_category`  INT(10) UNSIGNED NOT NULL,
  `id_lang`          INT(10) UNSIGNED NOT NULL,
  `id_shop`          INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `name`             VARCHAR(128)     NOT NULL,
  `description`      TEXT,
  `link_rewrite`     VARCHAR(128)     NOT NULL,
  `meta_title`       VARCHAR(128)              DEFAULT NULL,
  `meta_keywords`    VARCHAR(255)              DEFAULT NULL,
  `meta_description` VARCHAR(255)              DEFAULT NULL,
  PRIMARY KEY (`id_cms_category`, `id_shop`, `id_lang`),
  KEY `category_name` (`name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cms_category_lang`
--

INSERT INTO `ps_cms_category_lang` (`id_cms_category`, `id_lang`, `id_shop`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`)
VALUES
  (1, 1, 1, 'Domů', '', 'domu', '', '', ''),
  (2, 1, 1, 'Vše o nákupu', '', 'vse-o-nakupu', '', '', ''),
  (3, 1, 1, 'O kontaktních čočkách ', '', 'o-kontaktnich-cockach', '', '', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cms_category_shop`
--

CREATE TABLE IF NOT EXISTS `ps_cms_category_shop` (
  `id_cms_category` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop`         INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cms_category`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 4;

--
-- Vypisuji data pro tabulku `ps_cms_category_shop`
--

INSERT INTO `ps_cms_category_shop` (`id_cms_category`, `id_shop`) VALUES
  (1, 1),
  (2, 1),
  (3, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cms_lang`
--

CREATE TABLE IF NOT EXISTS `ps_cms_lang` (
  `id_cms`           INT(10) UNSIGNED NOT NULL,
  `id_lang`          INT(10) UNSIGNED NOT NULL,
  `id_shop`          INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `meta_title`       VARCHAR(128)     NOT NULL,
  `meta_description` VARCHAR(255)              DEFAULT NULL,
  `meta_keywords`    VARCHAR(255)              DEFAULT NULL,
  `content`          LONGTEXT,
  `link_rewrite`     VARCHAR(128)     NOT NULL,
  PRIMARY KEY (`id_cms`, `id_shop`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cms_lang`
--

INSERT INTO `ps_cms_lang` (`id_cms`, `id_lang`, `id_shop`, `meta_title`, `meta_description`, `meta_keywords`, `content`, `link_rewrite`)
VALUES
  (1, 1, 1, 'Rychlá objednávka', 'Rychlá objednávka', '',
   '<p><strong>Objednání zboží</strong> z našeho internetového obchodu <strong>je tak jednoduché</strong>, že ho zvládne skutečně každý. Stačí si jednoduše <strong>vybrat zboží</strong>, pohodlně <strong>vložit do košíku</strong>, kde můžete měnit počty kusů nebo i jednotlivé položky mazat, a <strong>vyplnit objednací formulář</strong>. Objednávky Vašich čoček je možné provést také prostřednictvím telefonu nebo emailem. V kamenných prodejnách je možné <strong>kontaktní čočky </strong>objednat a vyzvednout osobně.</p>\n<p><strong>Objednávky po telefonu:</strong> využijte naši telefonní linku <strong>+420 <strong><span style="font-size:small;">776 780 080</span></strong></strong> od pondělí do soboty v 8-15 hodin.</p>\n<p><strong>Objednávky e-mailem:</strong> pošlete nám kdykoliv Vaši objednávku na e-mailovou adresu <a><strong>objednavky@prodej-cocek.cz</strong></a>, na které okamžitě reagujeme.</p>\n<p>V případě jakýchkoli dotazů využijte naši zákaznickou linku <strong>+420 <span style="font-size:small;">776 780 080</span></strong><span style="font-size:small;">.</span></p>',
   'rychla-objednavka'),
  (2, 1, 1, 'Návod k používání kontaktních čoček', 'Návod k používání kontaktních čoček', '',
   '<h4>Návod k používání kontaktních čoček</h4>\n<p>Připravili jsme pro vás <strong>vyčerpávající návod</strong> k používání <strong>kontaktních čoček</strong>.</p>\n<p>Najdete v něm kompletní postup <strong>nasazování</strong>, <strong>sundávání</strong> a <strong>práce s čočkami</strong> a další užitečné rady.</p>\n<p><a href="http://www.prodej-cocek.cz/otevreni-baleni-kontaktnich-cocek.html">Přečíst návod krok po kroku</a></p>\n<h5><a name="obsah-navodu"></a>Obsah:</h5>\n<ul><li><a href="http://www.prodej-cocek.cz/otevreni-baleni-kontaktnich-cocek.html">Otevření balení <strong>kontaktních čoček</strong></a></li>\n<li><a href="http://www.prodej-cocek.cz/nasazeni-kontaktnich-cocek-3.html">Nasazení <strong>kontaktní čočky</strong></a></li>\n<li><a href="http://www.prodej-cocek.cz/noseni-kontaktni-cocky.html">Nošení <strong>kontaktní čočky</strong></a></li>\n<li><a href="http://www.prodej-cocek.cz/vyjmuti-kontaktni-cocky.html">Vyjmutí <strong>kontaktní čočky</strong></a></li>\n<li><a href="http://www.prodej-cocek.cz/cisteni-a-dezinfekce-kontaktnich-cocek-3.html">Čištění a dezinfekce <strong>kontaktní čočky</strong></a></li>\n<li><a href="http://www.prodej-cocek.cz/doporuceni-ocniho-specialisty-3.html">Doporučení <strong>očního specialisty</strong></a></li>\n<li><a href="http://www.prodej-cocek.cz/obecne-o-ocnich-vadach.html">Obecně o <strong>očních vadách</strong></a></li>\n<li><a href="http://www.prodej-cocek.cz/kosmetika-a-kontaktni-cocky.html">Kosmetika a <strong>kontaktní čočky</strong></a></li>\n<li><a href="http://www.prodej-cocek.cz/casto-kladene-otazky-k-pouzivani-kontaktnich-cocek.html">Často kladené otázky k používání <strong>kontaktních čoček</strong></a></li>\n</ul><h4>Jak začít?</h4>\n<p>Chcete si vyzkoušet <strong>kontaktní čočky</strong> a nevíte, jak začít?<strong> Vyhledejte nás</strong> v <a href="http://www.ocni-centrum.eu/kontakt">ordinaci</a> a objednejte se k některému z našich očních lékařů na <strong>odborné vyšetření</strong>.</p>\n<p>V ordinaci Vám <strong>změříme zrak</strong> a <strong>vybereme </strong>Vám <strong>nejvhodnější typ</strong> kontaktních čoček, které jsou vhodné právě pro Vás. Pak se půjdete, již v kontaktních čočkách na chvíli projít ven, znovu Vás zkontrolujeme a pokud bude vše v pořádku, nacvičíme s Vámi nasazování a vyjmutí <strong>kontaktních čoček</strong> a vysvětlíme Vám, jak se o ně máte starat.</p>\n<p>Z ordinace si kromě všech materiálů ke <strong>kontaktním čočkám</strong> odnesete také <strong>startovací balíček</strong>, ve kterém máte <strong>pouzdro na kontaktní čočky</strong>, <strong>čistící roztok</strong> a CD, na kterém máte možnost shlédnout veškerou manipulaci s <strong>kontaktními čočkami</strong>. V ceně je tzv. <strong>zkušební pár</strong>, což jsou ty <strong>kontaktní čočky</strong>, které jste si v průběhu návštěvy vyzkoušeli.</p>\n<p>Vidíte tedy, že to není nic složitého a brzy si již můžete užívat všechny výhody <strong>kontaktních čoček</strong>.</p>\n<h4><a href="http://www.prodej-cocek.cz/videa-operaci-oci.html">Videa operací očí</a></h4>\n<p>Zde naleznete <strong>podrobná videa</strong> z různých druhů <strong>očních operací</strong>. Jsou připraveny tak, aby vám <strong>názorně</strong> ukázaly důležitý průběh <strong>operace</strong>. Uvidíte, že opravdu <strong>není čeho se bát</strong>.</p>\n<p><a href="http://www.prodej-cocek.cz/videa-operaci-oci.html">Shlédnout videa</a></p>',
   'navod-na-pouzivani-kontaktich-cocek'),
  (3, 1, 1, 'Obchodní podmínky', 'Obchodní podmínky', 'conditions, terms, use, sell,obchodní,podmínky',
   '<h4>Obchodní podmínky</h4>\n<p>Toto jsou závazné obchodní podmínky internetového obchodu <a href="http://www.prodej-cocek.cz">www.prodej-cocek.cz</a>. Tyto podmínky popisují a upravují vzájemný obchodní vztah mezi provozovatelem Korda - oční ambulance, spol s r.o., (viz sekce <a href="http://www.prodej-cocek.cz/kontakt.html">Kontakt</a>) na straně prodávajícího a zákazníkem, návštěvníkem na straně kupujícího. Tyto oboustranně závazné obchodní podmínky jsou návrhem kupní smlouvy a navazují na cenovou nabídku.</p>\n<h5>1. Realizace kupní smlouvy</h5>\n<p>Kupní smlouva vzniká na základě závazného odeslání objednávky do našeho obchodního systému. Obratem obdržíte potvrzující e-mail, kde budou uvedeny údaje o Vaší objednávce. Dle dané kupní smlouvy bude následně realizována expedice zboží. <strong>Platnost cen na těchto stránkách je platná od 1.1.2016 až do 31.12.2016.</strong></p>\n<h5>2. Storno objednávky</h5>\n<p>Do doby než je Vaše objednávka odeslána na Vaši adresu máte možnost celou objednávku zrušit. Bezplatné zrušení je možné pouze u zboží, které bylo v době objednání skladem. Pokud si však přejete zrušit objednávku, kde již došlo z naší strany k prokazatelným nákladům (např. objednání zboží od dodavatelů, zbytečná doprava, zbytečné balení atd…), má prodávající právo požadovat tyto poplatky v plné výši na kupujícím.</p>\n<h5>3. Odstoupení od kupní smlouvy (zákon č.367/2000 Sb.)</h5>\n<p>V souladu se zákonem č. 367/2000 má zákazník právo odstoupit od kupní smlouvy do 14 dní od převzetí zboží, pokud je obchod realizován elektronickou cestou.</p>\n<p>Pokud se rozhodnete odstoupit od kupní smlouvy, <strong>informujte nás</strong> o Vašem úmyslu <strong>předem</strong>, a to písemně, e-mailem či telefonicky (viz sekce <a href="http://www.prodej-cocek.cz/kontakt.html">Kontakt</a>) spolu <strong>s uvedením čísla dané objednávky</strong> (variabilní symbol) a <strong>data uskutečnění nákupu</strong>.</p>\n<p><strong>Podmínky pro odeslání zboží zpět na naši adresu (viz sekce <a href="http://www.prodej-cocek.cz/kontakt.html">Kontakt</a>):</strong></p>\n<ul><li>Zboží musí být v původním nepoškozeném a orginálním obalu, nesmí být použité či poškozené</li>\n<li>Zboží je lépe zaslat doporučeně a pojištěné, neručíme za poškození během přepravy (za to odpovídá dopravce)</li>\n<li>Ke zboží musí být přiložen orginální doklad o koupi</li>\n</ul><p>Při splnění všech výše uvedených podmínek pro vrácení zboží dle výše uvedeného zákona Vám peníze za zboží zašleme dle Vašeho výběru složenkou nebo převodem na Váš účet a to nejpozději do 14 pracovních dnů po obdržení neporušeného zboží zpět. V případě nesplnění výše uvedených podmínek nebude odstoupení akceptováno a zboží bude vráceno na Vaše náklady zpět. Poplatky za poštovné se nevrací.</p>\n<h5>4. Dodací podmínky</h5>\n<p>Zboží zasíláme na všechny adresy na celém území České republiky a na Slovesko. V případě zájmu o zaslání do jiné země nás kontaktujte. Zásilky jsou dodávány přepravcem DPD. Veškeré zásilky jsou pojištěny na hodnotu obsahu, proto velmi doporučujeme si zkontrolovat neporušení obalu i obsahu a v případě poškození okamžitě kontaktovat poštu a sepsat protokol o škodě.</p>\n<p>Zboží objednané v sobotu, v neděli a ve svátek zasíláme nejdříve následující pracovní den. Dodací doba v případě platby <span style="text-decoration:underline;">dobírkou</span> je 1-7 pracovních dnů od objednání zboží. V případě <strong>platby předem</strong> je dodací doba 1-7 pracovních dnů od přijetí platby na náš účet. U <strong>barevných</strong> a <strong>torických</strong> <strong>kontaktních čoček</strong> může být dodací lhůta delší.</p>\n<p>V případě, že dodací lhůta přesáhne 7 pracovních dnů, Vás budeme informovat e-mailem nebo telefonicky (pokud uvedete telefonický kontakt). Doporučujeme uvádět přesnou dodací adresu, na které budete v dopoledních hodinách, aby Vám bylo zboží dodáno co možná nejdříve.</p>\n<p>Ceny dopravy naleznete v následujícím oddílu: Platba za zboží.</p>\n<h5><a name="platba"></a>5. Platba za zboží</h5>\n<p>Zboží je možné zaplatit hotově při převzetí zásilky (dobírkou či osobně), předem převodem částky na náš účet nebo online pomocí služby PaySec, PayPal či kartou.</p>\n<p>Při platbě dobírkou či online zboží odesíláme většinou ještě tentýž den nebo den následující. V případě platby předem odesíláme zboží až po přijetí celé částky na náš účet.</p>\n<p>K ceně nákupu je připočítáváno poštovné dle Vašeho výběru platby a doručení:</p>\n<table class="tabulka" width="542" cellspacing="1" cellpadding="1" border="0"><tbody><tr><td style="text-align:left;"><strong>Způsob platby a doručení</strong></td>\n<td style="text-align:center;"><strong>Poštovné</strong></td>\n</tr><tr><td>platba dobírkou (při nákupu do výše 1800 Kč vč. DPH)</td>\n<td style="text-align:center;">89,-</td>\n</tr><tr><td>platba dobírkou (při nákupu nad 1800 Kč vč. DPH)</td>\n<td style="text-align:center;">0,-</td>\n</tr><tr><td>platba PayPal (při nákupu do výše 400 Kč vč. DPH)</td>\n<td style="text-align:center;">89,-</td>\n</tr><tr><td>platba PayPal (při nákupu nad 400 Kč vč. DPH)</td>\n<td style="text-align:center;">0,-</td>\n</tr><tr><td>platba kartou (při nákupu do výše 400 Kč vč. DPH)<span class="Apple-tab-span" style="white-space:pre;"> </span></td>\n<td style="text-align:center;">89,-</td>\n</tr><tr><td>platba kartou (při nákupu nad 400 Kč vč. DPH)</td>\n<td style="text-align:center;">0,-</td>\n</tr><tr><td>platba předem - převodem na náš účet (při nákupu do výše 400 Kč vč. DPH)</td>\n<td style="text-align:center;">89,-</td>\n</tr><tr><td>platba předem - převodem na náš účet (při nákupu nad 400 Kč vč. DPH)</td>\n<td style="text-align:center;">0,-</td>\n</tr><tr><td>osobní převzetí v naší provozovně (viz sekce <a href="http://www.prodej-cocek.cz/kontakt.html">Kontakt</a>)</td>\n<td style="text-align:center;">0,-</td>\n</tr></tbody></table><p><strong>Platbu předem</strong> provádějte na účet číslo: ČSOB <strong><span class="quote2">216845408/0300</span></strong> s konstatním symbolem <strong>0308</strong>. Jako <strong>variabilní symbol uveďte číslo Vaší objednávky</strong> (k identifikaci platby). Zboží zasíláme okamžitě po přijetí platby na náš účet.</p>\n<p><strong>Výhody platby předem:</strong></p>\n<ul><li>Poštovné zdarma již při nákupu nad 400 Kč (vč. DPH)</li>\n<li>Zboží zasíláme doporučeně</li>\n</ul><p><a name="platba_paypal"></a><strong>Výhody platby pomocou služby PayPal:</strong><a href="https://www.paypal.com/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside%27"><img class="kgmqpdlbsdrzydkvlhgg" alt="Další informace" style="float:right;" src="https://www.paypal.com/en_US/Marketing/i/banner/bnr_accept_120x30_y.gif" border="0" /></a></p>\n<ul><li>Poštovné zdarma již při nákupu nad 400 Kč (vč. DPH)</li>\n<li>Platba probíhá ihned po potvrzení objednávky</li>\n<li>Platba probíhá přímo a bezpečně z vašeho PayPal účtu či karty</li>\n<li>Zboží zasíláme doporučeně</li>\n</ul><p><strong><a name="platba_kartou"></a>Výhody platby kartou:</strong><a target="_blank" title="GP webpay" href="http://www.gpe.cz/"><img style="float:right;" title="GP webpay" alt="GP webpay" src="http://static2.prodej-cocek.cz/images/GP_webpay100.gif" /></a></p>\n<ul><li>Poštovné zdarma již při nákupu nad 400 Kč (vč. DPH)</li>\n<li>Platba probíhá ihned po potvrzení objednávky</li>\n<li>Platba probíhá prostřednictvím platební brány <a target="_blank" href="http://www.gpe.cz/">GP webpay</a>, která využívá nejmodernější bezpečnostní standardy (např. <a href="http://en.wikipedia.org/wiki/SecureCode">3-D Secure</a>)</li>\n</ul><p>Akceptujeme následující typy karet<br /><img alt="Akceptované karty" src="http://static2.prodej-cocek.cz/images/credit-card-icons.gif" /></p>\n<p>Pro online platbu přijímáme karty VISA, VISA Electron, MasterCard a Maestro. Vydavatelské banky umožňují používání embosovaných (reliéfních) karet. Použití elektronických karet VISA Electron a Maestro je <strong>možné pouze v případě, že to umožňuje Vaše vzdavatelská banka</strong>.<br /> Prosím, informujte se u vydavatele své karty o možnosti jejího využití pro internetové platby.<br /><br /> Registrace pro Vás neznamená žádná omezení, ba naopak.</p>\n<h5>6. Reklamační řád</h5>\n<p>Reklamace zboží se řídí příslušnými ustanoveními občanského zákoníku a zákonem na ochranu spotřebitele.</p>\n<p><strong>Kupující je povinen prohlédnout zboží bezprostředně při jeho převzetí.</strong> Pokud je zjištěno mechanické poškození obalu výrobku, je kupující povinen vyhotovit záznam o poškození za přítomnosti dopravce. Odpovědnost za poškození v průběhu přepravy nese dopravce, každá zásilka je pojištěna. Na základě vyhotoveného záznamu bude zákazníkovi po uzavření škodní události s dopravcem poskytnuta přiměřená sleva nebo dodán nový výrobek. <strong>Na pozdější reklamace způsobené vinou dopravy nelze brát zřetel a budou zamítnuty</strong>.</p>\n<p>Prodávající nepřebírá odpovědnost za škody vzniklé z neodborného používání produktů, stejně jako škod způsobených vnějšími událostmi a chybnou manipulací. Na vady tohoto původu se nevztahuje ani poskytnutá záruka.</p>\n<p><strong>K reklamaci je nutné ve všech případech zaslat reklamovaný produkt</strong>, předložit <strong>kopii faktury</strong> a <strong>doklad o zaplacení a dodání zboží</strong>, jehož vady jsou reklamovány. Prodávající se zavazuje informovat zákazníka o reklamaci nejpozději do 7 pracovních dní na jeho uvedený e-mail, pokud se s kupujícím nedohodne jinak. Reklamace bude vyřízena bez zbytečného odkladu, nejpozději do 30 dnů ode dne uplatnění reklamace. Veškeré náklady na zaslání (dopravu) reklamovaného zboží nese zákazník.</p>\n<h5>7. Platnost cen</h5>\n<p>Platnost cen na těchto stránkách je platná od 1.1.2016 až do 31.12.2016.</p>\n<h5>8. Nevyzvednutí zásilky</h5>\n<p>Na základě špatných zkušeností s některými zákazníky jsme nuceni zavést pravidla jako jiné obchody. Podle platných zákonů ČR je <strong>dodavatel </strong>(společnost Ophtha Trade, spol. s r.o.) <strong>oprávněn </strong><strong>účtovat skutečně vzniklé náklady na odeslání zásilky konečnému zákazníkovi</strong>.</p>\n<p>S okamžitou účinností začínáme na tomto právu trvat a náklady budou vždy vymáhány nejprve upomínkou. Pokud pohledávka nebude uhrazena, budeme muset podstoupit další kroky, včetně soudních a exekučních. Tyto náklady vždy jdou k tíži dlužníka a mohou se vyšplhat až do desetitisíců. Níže uvádíme orientační náklady. Skutečně vzniklé náklady budou vždy vyúčtovány.</p>\n<p><strong>Současné náklady:</strong></p>\n<ul><li>Poštovné - 120Kč (obchodní)</li>\n<li>Objednávka od dodavatele - cca 90Kč (pouze zboží na objednávku)</li>\n<li>Mzda expedičního oddělení - 80Kč</li>\n<li>Balící materiál - 50Kč</li>\n<li>Svoz na poštu - 30Kč</li>\n<li>Administrativní náklady - 90Kč</li>\n</ul><h5>9. Ochrana osobních dat</h5>\n<p>Pro zaslání Vámi objednaného zboží potřebujeme znát některá <strong>Vaše osobní data</strong> - identifikační údaje, tj. jméno, adresu dodání, telefon a e-mail. Údaje jsou nutné k vyřízení objednávky.</p>\n<p>Tato data <strong>chráníme před zneužitím</strong> a nebudou nikdy poskytnuty žádnému třetímu subjektu. Používáním tohoto internetového obchodu souhlasíte se shromažďováním a používáním informací o Vás a Vašich nákupech za výše stanovených podmínek. Náš internetový obchod si vyhrazuje právo ustoupit od záruky bezpečnosti pouze v případě napadení serveru neznámým pachatelem (hackerem). V takovém případě neplatí výše uvedené záruky.</p>\n<p>Informace o zákaznících jsou uchovávány v souladu s platnými zákony České republiky, zejména se zákonem o ochraně osobních údajů č. 101/2000 Sb. ve znění pozdějších dodatků a předpisů. Společnost Korda - oční ambulance, spol. s r.o., je registrovaným správcem osobních údajů. Veškeré údaje, získané od zákazníků, užívá výhradně pro vnitřní potřebu společnosti a neposkytuje je třetím osobám. Výjimku představují externí dopravci, kterým jsou osobní údaje zákazníků předávány v minimálním rozsahu, který je nutný pro bezproblémové doručení zboží.</p>\n<p>lip</p>',
   'obchodni-podminky'),
  (4, 1, 1, 'O nás', 'O nás', 'informations,informace,o nás',
   '<h4>Jsme očním centrem provozujícím internetový obchod a velkoobchod s kontaktními čočkami a zdravotnickými prostředky</h4>\n<p>Nabízíme <strong>široký sortiment kontaktních čoček, roztoků, kapek a gelů</strong> vysoké kvality. Jsme exkluzivním dovozcem a distributorem produktů značky <strong>Barnaux </strong>a <strong>Bauch&amp;Lomb</strong> a též distributorem produktů značky <strong>Alcon, Clear, Cooper Vision </strong>a<strong> Jonhson &amp; Jonhson</strong>.</p>\n<h4>Historie</h4>\n<p>V očním lékařství působíme <strong>již 15 let</strong>. Díky naší <strong>dlouholeté praxi</strong> Vám poskytneme <strong>vysoce kvalifikované</strong> rady a doporučení.</p>\n<p><strong>V roce 1998</strong> jsme se rozhodli vstoupit na <strong>trh kontaktních čoček</strong>, protože věříme v jejich budoucnost. První internetové obchody provozujeme již 8 let.</p>\n<p>Nejsme pouhými obchodníky. Prvořadé<strong> </strong>je pro nás <strong>zdraví Vašich očí </strong>a <strong>kvalita Vašeho vidění</strong>. Nabízíme Vám proto <strong>rozsáhlou nabídku</strong> kontaktních čoček, kde si ty ideální vybere i nejnáročnější zákazník.</p>\n<h4>Hlavní prioritou je pro nás spokojenost zákazníka</h4>\n<p>Velice dbáme na <strong>kvalitu dodávaných výrobků</strong>. Orientujeme se pouze na osvědčené výrobky od předních výrobců, držitelů osvědčení ISO 9001.</p>\n<p>Náš internetový obchod Vám zaručuje <strong>rychlé dodávky</strong> Vašich objednávek. Zboží uložené na skladě Vám dodáme <strong>do 48 hodin</strong> od objednání.</p>\n<h4>Certifikát a diplom - záruka kvalifikovanosti</h4>\n<p>Náš internetový obchod je ojedinělý nejen cenami, ale také svou kvalifikovaností. <strong>Provozovatelem </strong>internetového obchodu je <strong>diplomovaný oční lékář - specialista na kontaktní čočky s certifikátem!</strong> Máte tak jistotu kvalifikované rady při výběru a podpory při aplikaci. Samozřejmostí je aplikace čoček zdarma v naší provozovně v Pardubicích, v Chrudimi, v Hradci Králové, v Hlinsku, v České Třebové, v Lanškrouně, v Rychnově nad Kněžnou a ve Svitavách. Naskenovaný certifikát a licenci ve vysokém rozlišení najdete po kliknutí na náhled.</p>\n<div style="text-align:center;"><a title="Certifikát" href="http://www.prodej-cocek.cz/files/cerifikat.jpg"><img style="margin-right:20px;" alt="Cerifikát" src="http://www.prodej-cocek.cz/images/stories/cerifikat-mini.jpg" width="200" height="146" /></a><a title="Licence" href="http://www.prodej-cocek.cz/files/licence.jpg"><img alt="Licence" src="http://www.prodej-cocek.cz/images/stories/licence-mini.jpg" width="200" height="146" /></a></div>\n<h4>Naše strategie</h4>\n<p>Strategií našeho internetového obchodu je nabídnout co <strong>nejširší sortiment </strong>kontaktních čoček a příslušenství za <strong>nejlepší možné ceny</strong> a doručit je zákazníkovi v co <strong>nejkratší době</strong> přímo na jeho adresu. Tento způsob prodeje umožnila změna legislativy v roce 2005. Od té doby je možné touto cestou zjednodušit přístup ke kontaktním čočkám jejich nositelů.</p>',
   'o-nas'),
  (5, 1, 1, 'Kontakt', 'Kontakt', '',
   '<h4>Kontakt</h4>\n<p>V případě jakýchkoliv dotazů nás neváhejte kontaktovat. Na všechny Vaše dotazy odpovíme v co možná nejkratší době.</p>\n<h5>OPHTHA TRADE, s.r.o.</h5>\n<p><strong>Sídlo: </strong>Loretánské náměstí 102/8, 118 00 Praha 1 - Hradčany</p>\n<h5>Výdejní místa:</h5>\n<p><strong>Oční ordinace Hradec Králové</strong><span style="color:#003300;"><strong>:</strong></span></p>\n<p><strong>Adresa: </strong>Švehlova 504, z Masarykova náměstí první vchod za restaurací Atlanta, 500 02 Hradec Králové<br /><strong>Telefon:</strong> 608 800 886 - objednání na vyšetření, vyzvednutí objednávek<br /><strong>Mobil:</strong> +420 608 800 886 nonstop linka 24 hodin denně!!! Zavolejte nám ZDARMA<br /><strong>E-mail:</strong> info@ocni-centrum.eu</p>\n<p><strong><br /> Oční optika OFTEX Pardubice:</strong></p>\n<p><strong>Adresa:</strong> Rokycanova 2798, Polyfunkční dům 530 02 Pardubice<br /><strong>Telefon: </strong>464 007 801 - objednání, zakázky, vyzvednutí objednávek<br /><strong>Mobil:</strong> +420 773 660 686 - objednání, zakázky, vyzvednutí objednávek</p>\n<p><br /><strong>Oční optika OFTEX Chrudim:</strong></p>\n<p><strong>Adresa:</strong> Palackého 191, 537 01 Chrudim, budova Polikliniky ve 2. patře vlevo (součást Očního centra Chrudim)<br /><strong>Telefon:</strong> 469 638 945 - objednání na vyšetření<br /><strong>Mobil:</strong> +420 776 880 980 - objednání na vyšetření, vyzvednutí objednávek<br /><br /><strong>Oční optika II OFTEX Chrudim</strong><br /><strong>Adresa:</strong> Palackého 183,  537 01 Chrudim<br /><strong>Mobil:</strong> +420 608 066 880 -<strong> </strong>objednání na vyšetření, vyzvednutí objednávek </p>\n<p><br /><strong>Oční optika OFTEX Hlinsko:</strong></p>\n<p><strong>Adresa:</strong> Kavánova 422, 539 01 Hlinsko<br /><strong>Telefon: </strong>469 314 483 - objednání na vyšetření, vyzvednutí objednávek<br /><strong>Mobil</strong>: + 420 608 866 098 - objednání na vyšetření, zakázky</p>\n<p><br /><strong>Oční optika OFTEX Lanškroun:</strong></p>\n<p><strong>Adresa: </strong>Purkyňova 103 (výjezdová ulice z náměstí), 563 01 Lanškroun<br /><strong>Telefon:</strong> 465 635 842 - objednání na vyšetření, vyzvednutí objednávek<br /><strong>Mobil:  </strong>+ 420 608 890 896 - objednání na vyšetření, zakázky</p>\n<p><br /><strong>Oční optika OFTEX Rychnov n. Kněžnou:</strong></p>\n<p><strong>Adresa:</strong> Komenského 44, 516 01 Rychnov nad Kněžnou (součást Očního centra OFTEX)<br /><strong>Telefon: </strong>494 531 523 - objednání na vyšetření, vyzvednutí objednávek<br /><strong>Mobil:</strong> +420 608 800 886 - infolinka nonstop</p>\n<p><strong> </strong></p>\n<p><strong>Oční optika OFTEX Česká Třebová:</strong></p>\n<p><strong>Adresa: </strong>E.Beneše 1513 (další ulice nad Poliklinikou), 560 02 Česká Třebová<br /><strong>Telefon:</strong> 465 534 560 - objednání na vyšetření, vyzvednutí objednávek<br /><strong>Mobil: </strong>+ 420 608 906 809 - objednání na vyšetření, vyzvednutí objednávek<strong> </strong></p>\n<p><br /><br /><strong>Oční optika OFTEX Ústí nad Orlicí:</strong></p>\n<p><strong>Adresa: </strong>Smetanova 1390, 562 01 Ústí nad Orlicí<br /><strong>Telefon: </strong>465 382 800 - objednání na vyšetření, vyzvednutí objednávek<br /><strong>Mobil:</strong> +420 773 880 888 - objednání na vyšetření, vyzvednutí objednávek<br /><br /><strong>Oční optika OFTEX Svitavy</strong><br /><strong>Adresa:</strong> U nemocnice 4, 568 02 Svitavy, ve 2. patře budovy vlevo, možno použít i výtah<br /><strong>Telefon:</strong> 461 532 835 - objednání, zakázky, objednání na přeměření brýlí<br /><strong>Mobil:</strong> 608 906 809 - objednání, zakázky, objednání na přeměření brýlí</p>\n<p> </p>\n<p>Společnost je vedená u rejstříkového soudu v Hradci Králové pod značkou C 23967.</p>\n<p><strong>IČ:</strong> 27522806</p>\n<p><strong>DIČ:</strong> CZ27522806</p>\n<p><strong>Mobil:</strong> + 420 608 800 886</p>\n<p><strong>Číslo účtu:</strong> 216845408/0300</p>\n<p></p>\n<p><strong>Objednávky, dotazy expedice, reklamace:</strong></p>\n<p><strong>Petra Mencová<br /></strong></p>\n<p><strong>Telefon:</strong> + 420 776 780 080 pro dotazy k dispozici po - so 8 - 15 h</p>\n<p><strong>E-mail:</strong> <a title="Napsat e-mail">info@prodej-cocek.cz</a><br /><br /> Adresa pro reklamace:<br /> Ophtha Trade, s.r.o.<br /> Petra Mencová<br /> Palackého 191<br /> 537 01 Chrudim</p>',
   'kontakt'),
  (6, 1, 1, 'Často kladené otázky', 'Často kladené otázky', '',
   '<h4>Často kladené otázky</h4>\n<ul><li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#precitlivele">Mám přecitlivělé oči při nošení <strong>kontaktních čoček</strong>.</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#barevne">Jaké použít barevné <strong>kontaktní čočky</strong>?</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#pary">V baleních jsou kusy či páry <strong>kontaktních čoček</strong>?</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#svetlo">Co proti zvýšené přecitlivělosti na světlo?</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#tablety">Opravdu se mi po tabletách zlepší zrak?</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#roztok">Jak nahradit zapomenutý <strong>roztok </strong>na <strong>kontaktní čočky</strong>?</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#prechod">Jak správně přejít z <strong>brýlí</strong> na <strong>čočky</strong>?</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#spat">Mohu spát v <strong>kontaktních čočkách</strong>?</a></li>\n</ul><h5 class="maly"><a name="precitlivele"></a><strong>Čočky</strong> jsem začala používat před několika lety (air optix), ale po vypotřebování celého balení a následného použití nového, mi přestaly vyhovovat. Začalo to zčervenáním očí,pálení a velmi silné slzení. Takto jsem vyzkoušela několik druhů. Naposledy mi doktor dal <strong>čočky</strong>, po kterých jsem skončila s těžkým zánětem rohovky. Na pravém oku mám -1,25, na levém -2. Jaké <strong>čočky</strong> by jste mi doporučili a lze si objednat na začátek jen <strong>zkušební pár</strong> (kdyby mi nevyhovovaly)?</h5>\n<p>Podle Vašich zkušeností máte zřejmě velmi citlivé oči na běžně užívané <strong>kontaktní čočky</strong>. Doporučuji zvolit takové <strong>kontaktní čočky</strong>, které budou pro Vaše oči nejen pohodlné, ale také i zhlediska zdravého způsobu nošení nejbezpečnější bez následných poškození oka. Doporučuji zvolit <a href="http://www.prodej-cocek.cz/kontaktni-cocky/ctrnactidenni/johnson-johnson/acuvue-oasys-6-cocek.html"><strong>kontaktní čočky Acuvue oasys</strong></a>, svým materiálem a způsobem nošení spňují nejvyšší požadavky uživatele <strong>kontaktních čoček</strong>. Lze objednat <a href="http://www.prodej-cocek.cz/startovaci-balicek.html"><strong>zkušební pár</strong></a>, v objednávce uveďte typ a rozsah <strong>kontaktních čoček</strong>.</p>\n<h5 class="maly"><a name="barevne"></a>Dobrý den, je mi 52 let a mám + 3,72 dioptrií na blízko a +2,5 dioptrií na dálku. Chtěla bych <strong>barevné kontaktní čočky</strong>. Pracuji jako sekretářka a většinu času pracuji na PC. Jaké <strong>kontaktní čočky</strong> by jste mi prosím doporučil? Děkuji za odpověď.</h5>\n<p>Doporučuji zakoupit <strong>kontaktní čočky</strong> +2,5 sféry dioptrie na blízko, na našem webu v sekci <a href="http://www.prodej-cocek.cz/barevne-kontaktni-cocky.html"><strong>barevné kontaktní čočky</strong></a> a to typu <strong>krycí</strong>, které změní Vaši barvu nebo tónovací, které jen lehce dobarví barvu Vašich očí.</p>\n<h5 class="maly"><a name="pary"></a>Chtěla bych se zeptat, v balení je 6 kusů nebo párů? Je tam uvedeno, že balení vydrží půl roku, což by znamenalo 6 párů, ale když jsem koukala všude na webu po různých čočkách, znamenalo to kusy. SOFLENS 66 TORIC - 6 kusů v balení Děkuji za odpověď.</h5>\n<p>Balení 6 kusů Soflens toric je balení na půl roku na jedno oko. Jedna <strong>kontaktní čočka</strong> je na jeden měsíc na jedno oko. Dvě <strong>kontaktní čočky</strong> jsou na jeden měsíc na dvě oči.</p>\n<h5 class="maly"><a name="svetlo"></a>Dobrý den, po delším užívání očních kapek Jodid draselný Unimed Pharma 2% se u mě objevila zvýšená citlivost na světlo, hlavně na světlomety automobilů i ve dne. Oční lékař prý žádnou příčinu nezjistil. Poraďte mi prosím, jaké přípravky jsou nejvhodnější, aby se oči vrátily do normálního stavu a nebyly citlivé na světlo. Předem děkuji za odpověď.</h5>\n<p>Doporučuji Vám navštívit nás v <a href="http://www.ocni-centrum.eu/chrudim">očním centru v Chrudimi</a>, kde se Vám budou věnovat zkušení <a href="http://www.ocni-centrum.eu">ophtalmologové</a> a budou se moci na základě vyšetření vyjádřit aktuálně k Vašemu problému.</p>\n<h5 class="maly"><a name="tablety"></a>Je mi 68 používám brýle na čtení, a slabé na dálku, ale ty jen na řízení. Je možné, že by se mi braním tablet zlepšil aspoň částečně zrak? Jsem sportovní střelec a střelba s brýlemi mi moc nevyhovuje. Buď mám částečně rozmazaná miřidla, nebo terč.</h5>\n<p>Užíváním tablet, které jsou dostupné na trhu je základem karotenoid lutein, který mj. zabraňuje peroxidaci tuků, která je značná jak v krevním séru, tak v očích a má schopnost zpomalovat destruktivní proces v sítnici oka a pohlcovat škodlivé frakce světelného spektra. <a href="http://www.prodej-cocek.cz/kontaktni-cocky/doplnky-vyzivy-zraku/alcon/ocuvite-lutein-180-tablet.html"><strong>Lutein</strong></a>, jako mnohostranný likvidátor volných radikálů, hraje klíčovou roli v antioxidační ochraně. Vyvážené složení přípravků mimo jiné obsahují zinek, vitamín C a D, taurin a kyselinu listovou, zlepšuje ochranu očí a zneškodňuje volné radikály, které poškozují nejen oči, ale negativně působí na celý organismus. Užíváním těchto tablet se zcela určitě vylepší zraková ostrost, ale nenahradí dioptrické brýle. Při Vašem sportu doporučuji použít <strong>kontaktní čočky</strong>.</p>\n<h5 class="maly"><a name="roztok"></a>Můžete mi poradit co mám dělat, když si zapomenu <strong>roztok na čočky</strong>? Je pro to nějáká náhrada(oční kapky) a nebo si je mám radši nechat v očích přes noc?</h5>\n<p>Doporučuji zvolit typ <strong>kontaktních čoček</strong>, které se nemusejí vyjmout z oka přes noc i několik dnů. Můžete zapomenout na každodenní nasazování a sundávání <strong>čoček</strong> a jejich zdlouhavou údržbu - <strong>čočky</strong> si nasazujete a sundáváte pouze jednou za měsíc. Obsažený materiál umožňuje překonat dosavadní nedostatečné okysličování rohovky během spánku při zavřených víčkách. Doporučuji zvolit <strong>měsíční kontaktní čočky</strong> <strong>Pure vision</strong> nebo <strong>čtrnáctidenní kontaktní čočky</strong> <strong>Acuvue oasys</strong>.</p>\n<h5 class="maly"><a name="prechod"></a>Nosím brýle a chtěla bych si pořídit <strong>kontaktní čočky</strong>. Mám dost dioptrií..nějak -4,5 a -5,75. Nevím jestli mám jít k doktorovi, který ty <strong>čočky</strong> dává a u kterého se zařizují...dělá i vyšetření...nebo si mám zajít do optiky a tam si je zařídit, koupit. Kam mám jít? Před 2 měsíci jsem byla u očního, takže mám napsané kolik mám dioptrií.</h5>\n<p><strong>Kontaktní čočky</strong> si můžete koupit u nás v internetovém obchodě, ve Vašem případě doporučuji zakoupit <strong>startovací balíček</strong>, který obsahuje dvě <strong>kontaktní čočky</strong>, <strong>pouzdro na kontaktní čočky</strong>, <strong>roztok</strong>, kterým se <strong>kontaktní čočky</strong> desinfikují a kompakt, na kterém je velmi srozumitelně i pro naprostého laika popsána instruktáž, jak se s <strong>kontaktní čočkou</strong> manipuluje.</p>\n<h5 class="maly"><a name="spat"></a>Mohu spát v <strong>kontakntích čočkách</strong>?</h5>\n<p>To záleží na druhu <strong>kontaktních čoček</strong>, které používáte. Jsou druhy <strong>kontaktních čoček</strong>, které jsou kyslíku propustné a další druhy <strong>měkkých kontaktních čoček</strong>, v kterých je možné spát, ale vždy se raději ujistěte u Vašeho <a href="http://www.ocni-centrum.cz">očního lékaře</a>.<br /><br />  </p>\n<p><a href="http://www.eshoplink.cz/">EshopLink.cz</a> <a href="http://www.najdu.net">Najdu.net</a> <a href="http://www.iobchody.com/" title="Online obchody" target="_blank">Všechny internetové obchody</a> <a href="http://www.cenyzbozi.cz/" title="Ceny zboží">CenyZboží.cz</a> <a href="http://www.az-katalog.com/">katalog odkazů</a></p>',
   'casto-kladene-otazky');
INSERT INTO `ps_cms_lang` (`id_cms`, `id_lang`, `id_shop`, `meta_title`, `meta_description`, `meta_keywords`, `content`, `link_rewrite`)
VALUES
  (7, 1, 1, 'Online poradna', '', '',
   '<h4>Služba Volejte mne kdykoliv</h4>\n<p><strong>"VOLEJTE MNE KDYKOLIV"</strong> tak se jmenuje naše nejnovější služba. Využijte nabídky<strong> telefonické </strong>konzultace našich očních specialistů na lince:<strong> 900 340 340</strong>. Po vytočení zadejte kód: <strong>25 40 45 </strong>a<strong> </strong>my Vám poradíme <strong>s jakýmkoliv očním problémem</strong>.</p>\n<h4>Často kladené otázky</h4>\n<ul><li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#precitlivele">Mám přecitlivělé oči při nošení kontaktních čoček.</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#barevne">Jaké použít barevné kontaktní čočky?</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#pary">V baleních jsou kusy či páry kontaktních čoček?</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#svetlo">Co proti zvýšené přecitlivělosti na světlo?</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#tablety">Opravdu se mi po tabletách zlepší zrak?</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#roztok">Jak nahradit zapomenutý roztok na kontaktní čočky?</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#prechod">Jak správně přejít z brýlí na čočky?</a></li>\n<li><a href="http://www.prodej-cocek.cz/clanky/poradna/casto-kladene-otazky.html#spat">Mohu spát v kontaktních čočkách?</a></li>\n</ul><h4>Otázky našich zákazníků</h4>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Šilhání u desetiměsíční dcery ... je dědičné?<br /></span></span></span></span></span></span></h2>\n<p>Má <strong>desetiměsíční dcera</strong> občas<strong> zašilhá levým očíčkem</strong>. Já sama jsem podstoupila již <strong>3× operaci strabismu na levém oku</strong>. <strong>2× v dětství a 1× v roce 2009</strong>. Vím, že u dětí do 6. měsíce se to objevit může, ale moje dítko je už starší. Problém je, že jí očíčko „ujede“ jen občas (několikrát za den) a to si myslím, že by v jistou chvilku lékař stejně nezpozoroval. Měla bych <strong>navšívit očního lékaře?</strong> Je dědičné šilhání?</p>\n<h3>16. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Oftalmologa určitě naštivte. <strong>Nejdůležitější bude zjištění dioptrické vady ujíždějícího oka</strong>. Nejčastější příčina ujíždění jen jednoho oka je <strong>vyšší diotrická vada jednoho oka</strong>, která může vést až k tupozrakosti. <strong>A bohužel některé typy strabismu mají dědičný podklad</strong>. I občasné ujetí oka je možné v některých případech <strong>pozorovat pomocí zkrývacího testu</strong>.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/silhani-u-ctyrlete-holcicky-jak-postupovat.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Bolest očí při zaměření se na předmět<br /></span></span></span></span></span></span></h2>\n<p>Moje 12letá dcera začala v poslední době mít <strong>problémy s očima</strong>. Při<strong> delším soustředění na jeden předmět</strong> (tenisový míček při tréninku, noty při hraní na flétnu) ji <strong>začnou bolet oči</strong>, zamlží se jí vidění a pak ji <strong>rozbolí hlava</strong>. V poslední době je <strong>velmi bledá, občas si stěžuje na únavu</strong>. Je možné, že má nějakou oční vadu, nebo to jen souvisí s dospíváním? Měla bych s ní pro jistotu zajít na vyšetření k očnímu lékaři?</p>\n<h3>16. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Dcera je zřejmě <strong>dalekozraká</strong>, případně je <strong>přítomen astigmatismus</strong>. Proto ji při zrakově namáhavé činnosti rozbolí hlava a rozostří se jí vidění. <strong>Navštivte oftalmologa</strong> pro předpis brýlí.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/vyresila-by-muj-problem-nitroocni-cocka-a-ktera.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Brnění poloviny tváře a jazyka<br /></span></span></span></span></span></span></h2>\n<p>Měla bych k Vám dotaz. Tohle se mi stalo v 15-ti letech, začalo to<strong> jiskřením před oběma očima</strong> (počátek taková jiskřící skvrnka, která přešla ve „vlnění“ v celém oku), potom následovalo<strong> brnění poloviny tváře i jazyka</strong>. Občas se tohle opakuje, po porodu v mých 30-ti letech až 4 krát v roce. Někdy po tom <strong>následovala bolest opačné poloviny hlavy</strong> nežli bylo tohle brnění. Někdy bylo přítomno tohle <strong>jiskření a posléze brnění</strong> na jedné polovině bez následné bolesti hlavy. Testy KO, TSH, T3 a T4 byly v pořádku, i výsledky CT, EMG a EEG. Slyšela jsem, že se podobně může manifestovat i RMS. Prosím o Vaší radu (vzhledem k těmto občasným očním obtížím -jiskření, vlnění)</p>\n<h3>16. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Vzhledem k tomu, co vše jste již absolvovala, tak se jedná o <strong>projevy oftalmologické migrény</strong>. Při takové frekvenci není třeba ani <strong>žádná dlouhodoběší medikace</strong>, pouze při manifestaci nějaké <strong>analgetikum či antimigrenotikum</strong>. RS se sice může <strong>projevovat brněním prstů i očními problémy</strong>, ale ty jsou v první fázi spíše v podobě drobného poklesu kvality vidění jednoho oka a případně jeho<strong> bolesti při pohybu</strong> (zánět/otok zrakového nervu). Navíc vyšetření, která jste podstoupila by na toto onemocnění poukazovala.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/problemy-po-zavedeni-multifokalnich-cocek.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Poškrábání rohovky<br /></span></span></span></span></span></span></h2>\n<p>Chtěla bych se zeptat. Již 3. den mám <strong>divný pocit v levém oku</strong>. Jako bych v něm <strong>měla smítko</strong>, které mě tam plave. Když si <strong>oko vykápnu</strong>, na chvilku to přestane. Ale potom se to objeví znovu. Pracuji celý den na PC. Co to může být?</p>\n<h3>16. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Může se <strong>jednat o poškrábání rohovky</strong> (tzv. erozi). <strong>Navštivte oftalmologa</strong>, který předepíše preparát na <strong>podporu zhojení.</strong> Doma můžete podpořit <strong>léčbu zalepení oka</strong> (zabránit mrkání) s nějakou naaplikovanou mastí (př. vidic).</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/zkusenosti-s-trifokalni-cockou-ostatnich-pacientu-kliniky-oftex.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Je možné ve Vašich optikách nechat vyrobit brýle na počkání?<br /></span></span></span></span></span></span></h2>\n<p>Hledám optiku, ve které mě <strong>dokáží dle předpisu lékaře vyrobit čočky</strong> a <strong>zbrousit je do stávajících obrouček</strong>. Jde o <strong>dětské kulaté brýle</strong> (dítě 2,5 roku) a nyní má skla 1 a 1.5 dioptrie a bude potřeba silnější, které budeme chtít<strong> zasadit do stávajících obrouček</strong>. Jste ve vaší optice na to zařízeni? Dítě <strong>nosí brýle z důvodu šilhání</strong>, má jenom jedny a bez brýlí nesmí být. Je toto možné na počkání?</p>\n<h3>16. 5. 2013, Odpověď týmu očního centra:</h3>\n<p><strong>Na počkání je možné zhovit brýle v oční optice v Pardubicích nebo v Ústí n.O</strong>. Nejlepší je <strong>kontaktovat pobočku přímo</strong>. Domluví se <strong>jaká skla do brýlí budete chtít</strong>, a <strong>kdy přijedete s obroučkou</strong>. Pak už je to otázka půl hodiny, než se zabrousí nová skla<strong>. </strong>Kontakty na obě pracoviště zde <a href="http://www.oftex.cz/kontakt.html">http://www.ocni-centrum.eu/kontakt.html</a></p>\n<p> </p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/jaka-jsou-rizika-operace-ppv.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Kapsulotomie yag laserem <br /></span></span></span></span></span></span></h2>\n<p>Maminka podstoupila včera <strong>kapsulotomii yag laserem</strong> po sekundárním šedým zákalu. Vše proběhlo v pořádku, <strong>mamince se hned rozjasnilo,</strong> ale teď má pocit že má v oku jakoby nejakou <strong>pavučinku, která se pohybuje</strong>. Je možné že je to nejaký <strong>zbytek z toho čištení pouzdra</strong> a že se to samo časem vstřebá nebo odezní? Za jak asi dlouho,kontrolu máme až za týden.</p>\n<h3>16. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Může to být <strong>zbytek odlaserových usazenin</strong> nebo <strong>zadního pouzdra</strong>, obojí se může <strong>časem vstřebat</strong>...řádově týdny. Pokud by se tak nestalo, tak se do toho útvaru stejným laserem střelí a drobnější kousky se již vstřebají. Kontrolu není nutné urychlit.</p>\n<p> </p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/cystoidni-makularni-edem.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">První kontaktní čočky<br /></span></span></span></span></span></span></h2>\n<p>Ráda bych si koupila <strong>kontaktní čočky Soflens 59 Soflens Comfort</strong> (6 čoček) za cenu 350Kč. Budu je mít poprvé a tak nevím co to obnáší, zda mám přijít k Vám a vy mi více řeknete nebo si je mám objednat přes internet.</p>\n<h3>16. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Z dotazu chápu, že jste <strong>kontakní čočky ještě nikdy nenosila</strong>. Proto je nutné, aby Vás někdo <strong>zaučil v manipulaci</strong>, péči o čočky, a zejména bude nutné <strong>zhodnotit vhodnost těchto čoček pro Vaše oči</strong>. Nejlepší bude <strong>kontaktovat naši optiku</strong> (kontakt na <a href="http://www.oftex.cz/ocni-optika-usti-nad-orlici.html">http://www.ocni-centrum.eu/ocni-optika-usti-nad-orlici.html</a>) a <strong>domluvit se s optometristkou</strong> na zkušebním páru se zácvikem. Pokud bude vše v pořádku, tak si již budete <strong>moci bez obav objednávat</strong> čočky <strong>prostřednictvím internetu</strong>.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/operace-silhani-nepomohla.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Tupá bolest očí a horší vidění<br /></span></span></span></span></span></span></h2>\n<p>Poměrně často mne <strong>bolí hlava</strong> a přijde mi, že <strong>příčinou bolesti jsou mé oči</strong>. Cítím v nich takovou <strong>tupou bolest</strong> - tlak, a od toho mě začne bolet i hlava. Když mne to "chytne" hodně tak bych si nejraději zatlačil oči (paradoxně mi přijde, že to pomáhá)...Mám poměrně <strong>dost dioptrií cca 5 a nosím kontaktní čočky</strong>. Můj oční lékař mne objednal až za dva měsíce a nerad bych něco zanedbal. Jsou to bolesti, které určitou dobu trvají a většinou pokud nepoužiji prášek proti bolesti, přestanou až druhý den. Poslední dobou se opakují častěji. Zároveň mám pocit, že<strong> i vidím hůře</strong>. Je možno takto na dálku nějak poradit? A mám to řešit jako akutní problém? Sestřička mi sdělila, že pokud přijdu a problém mít zrovna v tu chvíli mít nebudu, že neví, zda mi pomohou.</p>\n<h3>16. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Jako první bych Vám doporučil <strong>nechat si přeměřit dioptrickou vadu</strong>, a to třeba i <strong>zkušenou optmetristkou při některé optice</strong>, kde berou bez objednání. Je velmi pravděpodobné, že jen tato úprava Vás zcela zbaví prolémů. Nadměrná akomodační námaha (případně neustálé mřimhuřování u krátkorakosti) může takovéto pocity vyvolat. Pokud by tomu tak nebylo, tak pk bych to na vašem místě <strong>řešil akutní návštěvou</strong>.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/hnisani-a-neslzeni-oci-u-sestimesicni-dcerky.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Zhoršování dioptrií<br /></span></span></span></span></span></span></h2>\n<p>Muj syn je mu <strong>11 let a nosi ted bryle na dalku</strong> (2,5 dioptrii). Myslitte si ze muzou <strong>bryle zvysovat dioptrie</strong>?? Nebo <strong>kdyz se nenosi zmensujou se</strong>?</p>\n<h3>16. 5. 2013, Odpověď týmu očního centra:</h3>\n<p><strong>Brýle jsou řešením dioprické vady</strong>. Jejich <strong>nenošením se vada nezlepší a ani nemohou způsobovat zhoršení dioptrií</strong>. Nárůst dioptrií (předpokládám, že je krátkozraký) je způsoben ještě nezastaveným růstem těla. S růstem těla roste i oko, a tak se <strong>zvyšují dioptrie</strong>. Ano, dočtete se, že když nebude korekci nosit, tak je nebude brýle potřebovat, ale všechny tyto techniky jsou založeny na faktu, že <strong>mozek dokáže přivyknout</strong> si a <strong>vyrovnat se se situací</strong>, kdy vidí velmi špatně (viz těžce slabozrací lidé). Ovšem vada tam je stále. Jedinými metody k odstranění dioptrií jsou <strong>metody refrakční chirurgie.</strong> Ke korekci vady je možné do budoucnu <strong>využívat i kontaktní čočky</strong>.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/muzete-vysvetlit-vysledek-vysetreni-oci.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Léčba nitroočního tlaku<br /></span></span></span></span></span></span></h2>\n<p>Třetí rok se <strong>léčím na glaukom</strong>. AT jsem měla 26-32,začala jsem kapat Travatan na noc,po čase ještě Carteol. Potom Duotrav 1x,pak Dorzogen combi 2x a Xalatan 1x. AT mám stále vyšší-27.Po Dorzogenu mne hodně pálí oči - víc než půl hodiny po nakapání. Lékařka mi řekla,že si musím zvyknout (používám ho 5 měsíců.Uvažuji nad změnou lékaře.Co mi poradíte?</p>\n<h3>8. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Objednejte se do <strong>glaukomové poradny</strong>, které jsou při FN nebo při soukromých klinikách. Evidentně <strong>nedosahujete s farmakologickou léčbou cílového nitroočního tlaku</strong> a bylo by tedy vhodné zvážit <strong>drobný operační zákrok</strong> (např. laserový). Ovšem bez znalosti anatomického nálezu oku, perimetru a případně OCT či HRT, není možné poradit více.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/operace-tupozrakosti.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Vyhlídky v léčbě tupozrakosti<br /></span></span></span></span></span></span></h2>\n<p>U male mi pred mesicem zjistili, ze je <strong>tupozraka</strong> v dubnu ji byly 3roky. Oko <strong>zalepujeme</strong> 3:1(3dny zdrave oko,1 den spatne oko) a bryle mame s 5 dioptriemy. Jak jsem se docetla<strong> u deti se da tupozrakost lecit</strong>, nerada bych neco prosvihla. Muzete mi prosim poradit na koho se obratit? A na kolik cca ta operace vyjde?</p>\n<h3>8. 5. 2013, Odpověď týmu očního centra:</h3>\n<p><strong>Žádná operace nedokáže odstranit tupozrakost</strong>. Pouze se v dospělosti dá provést <strong>refrakční zákrok k odstranění doptrické vady</strong>, čímž se vidění vylepší, ALE tupozrakost se tím nevyléčí. <strong>Efektivní léčba je možná jen do věku 6 (max. 8) let</strong>. V podstatě je <strong>nutné přinutit mozek, aby obnovil plnou komunikaci s horším okem</strong>. Toto se děje <strong>na úrovni nervových impulsů</strong>. Léčba probíhá právě <strong>pomocí předpisu brýlí</strong> (tak se co nejvíce zlepší vidění) a <strong>zalepuje se lepší oko</strong>. Tím je mozek nucen koukat jen tím horším okem, a aby byla byla orientace v prostoru lepší, tak se tupozrakost bude postupně odblokovávat. Léčbu je možné podpořit <strong>pleoptickým a eventuálně ortoptickým cvičením</strong>. Nebojte se, nic jste neprošvihla ani nezanedbala. Lepte dle pokynů ošetřujícího oftalmologa a buďte nekompromisní v nošení brýlí.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/ocni-vysetreni-v-usti-nad-orlici.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Fuchsova skvrna, prořídlá sítnice<br /></span></span></span></span></span></span></h2>\n<p>V 17. letech mi byla diagnostikována <strong>Fuchsova skvrna na levém oku</strong>. Od té doby je můj stav stejný a chodím pravidelně na vyšetření. <strong>Pravou sítnici mám prořídlou</strong>. Před asi 4 lety jsem si všimla, že při běhu vidím <strong>na pravém oku</strong> jakoby <strong>tmavé a světlé půlkruhy</strong> (jsou uložené do kulata), vždy na světlém pozadí. V poslední době se mi to <strong>stává už i při chůzi</strong> (ne když se na něco konkrétního zadívám), spíš na nebi nebo omítce domu. Všimla jsem si také, že někdy <strong>pulsují podle rytmu srdce</strong>, vidím je taky, když si mnu oko. Jinak <strong>vidím pořád stejně</strong> a nepozoruju nějaké další zhoršení. Může to mít souvislost s tlakem? <strong>Mám nízký tlak</strong>. V současné době jsem také <strong>v 17 týdnu těhotenství</strong>.</p>\n<h3>8. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Příčin to může mít více. Vzhledem k přítomnosti <strong>Fuchsovy skvrny</strong> trpíte <strong>vysokou krátozrakostí</strong>, a to s sebou nese <strong>urychlení přirozených změn sklivce</strong>. Typické je <strong>zvýšené množství sklivcových zákalků</strong> (pro ně svědčí vidění zejména na světlém podkladu) a <strong>dřívější odloučení zadní sklivcové membrány</strong>. Tato membrána je <strong>přichycená v makule</strong> a při pohybu jakoby tahá za sítnici a vdění kruhů je s tímto spojeno. Nízký krevní tlak způsobí spíše větší výpadek v zorném poli. Rozhodně by Vás ještě <strong>před porodem měl vidět oftalmolog</strong>.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/odstraneni-zakalku.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Zánět oka<br /></span></span></span></span></span></span></h2>\n<p>Už asi týden mívám <strong>červené oko cívky</strong> úplně jakoby <strong>nalité a už to i bolí</strong>. Nosím čočky a večer si je hned musím sundat, protože mě to řeže. Bojím se, že je budu muset přestat nosit, nechci nosit brýle. Neznáte na to nějaké kapky?</p>\n<h3>8. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Navštivte oftalmologa. Pravděpodobně máte <strong>v očích zánět</strong>. <strong>Po vyléčení se budete moci ke kontaktním čočkám vrátit</strong>. Dalším krokem by pak byla <strong>změna parametrů kontatní čočky</strong>.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/analyza-lekarske-zpravy.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Zarudlé bělmo i spojivkový vak po operaci katarakty<br /></span></span></span></span></span></span></h2>\n<p>Jsem měsíc<strong> po operaci katarakty levého oka</strong>. Oko nebolí, ale <strong>stále mám krvavé bělmo i spojivkový vak pod dolním víčkem</strong>. <strong>Ráno je oko slepené</strong>. Kapala jsem Tobradex, Maxitrol a nyní mi lékař předepsal Vigamox. Zlepšení nevidím. V prosinci 2012 jsem <strong>prodělala recidivu hl. flebothrombosy</strong> a stále užívám 1 tbl. Warfarinu/5mg/ denně. Poslední INR 2,6. Také mě byl před dvaceti lety <strong>diagnostikován glaukom</strong>. Kapu pravidelně do obou očí Trusopt.</p>\n<h3>8. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Doporučuji nechat si provést <strong>stěr ze spojivkového vaku</strong>. Tím se přesně zjistí <strong>původce případně setrvávajícího zánětu</strong> a léčbu bude možné lépe zacílit. Ovšem Vigamox je velmi účinné širokospektré antibiotikum a tak předpokládám negativní výsledek. Dalšími možnostmi je vzniknuvší <strong>syndrom suchého oka</strong>, ale to by oko během dne slzelo, nebo jste pociťovala v očích písek atd. Léčbou je<strong> pravidelná aplikace umělých slz</strong>, které jsou volně prodejné. A třetí možností je <strong>projevení přecitlivosti na aplikovaný Trusopt</strong>, který sám o sobě může způsobit zarudnutí očí, otoky víček atd. V takovém případě stačí změnit medikaci.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/sucha-odlupujici-se-kuze-na-ocnich-vickach.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Bolest, pálení oka, mravenčení půlky tváře<br /></span></span></span></span></span></span></h2>\n<p>Prosím o radu, nejdříve mě<strong> svědilo v okolí levého oka a víčka</strong>, po protření místo <strong>bolestivě pálilo</strong>, slzí a svědí mě až <strong>mravenčí půlka levé tváře</strong>, mám <strong>mírně skleslé víčko</strong> a <strong>místo bolestivě svědí</strong> je <strong>zarudlé</strong>. Poraďte mi prosím co by to mohlo být a co mám dělat.</p>\n<h3>8. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Navštivte oftalmologa. Podle popisu by se mohlo jednat o<strong> pásový opar</strong>, který <strong>mohl zasáhnout i nervy</strong>, to <strong>může být příčinu proč Vám pokleslo horní víčko</strong> a <strong>mravenčí půlka obličeje</strong>.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/mohu-nosit-kontaktni-cocky-2.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Využití jednodenních čoček do moře nebo bazénu<br /></span></span></span></span></span></span></h2>\n<p>Chci se zeptat, zda je <strong>možné využít kontaktní čočky na jedno použití do moře</strong>, nebo alespoň do bazénu nebo jestli i tak, hrozí zánět spojivek atd.</p>\n<h3>8. 5. 2013, Odpověď týmu očního centra:</h3>\n<p><strong>Jednodenní čočky jsou pro takové příležitosti optimální</strong>. Riziko vzniku zánětu je pak v podstatě stejné jako by jste je neměla vůbec.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/je-mozne-zlepsit-astigmatismus-oka-ditete-cvicenim.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Rozdíl ceny čoček u lékaře a na internetovém obchodu<br /></span></span></span></span></span></span></h2>\n<p>Rád bych se zeptal na <strong>rozdílnou cenu čoček</strong>. Jak je možný tak velký rozdíl <strong>ceny od lékaře a na internetovém obchodu</strong>. Např. čočky Soflens 59 ( Bausch Lomb) stojí na českých stránkách cca 260Kč, v jiných státech cca 290Kč za 3páry. Jak je možné, že oční lékař si naučtuje cca 200Kč za jeden pár. Rád bych o upřímnou odpověď. Mnoho lidí láká cena na internetu, ale oči máme jedny.</p>\n<h3>8. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Každý internetový obchod či oční lékař si <strong>stanoví cenu kontaktních čoček dle svého rozhodnutí</strong> s přihlédnutím k mnoha faktorům. Náklady na provoz, nákupní cena produktu / mnohdy velmi rozdílná cena dle dodavatelů/,... Aplikace kontaktních čoček <strong>vyžaduje oční vyšetření</strong>. Nevím, zda Vám oční lékař poskytl pouze zkušební pár, ale <strong>v ceně zkušebního páru nejsou zahrnuty poplatky za vyšetření nebo výběr vhodných čoček</strong>. Toto je s největší pravděpodobností cena, kterou jste u lékaře uhradil. Lékař Vám navíc zkontroluje, zda Vám kont. čočka vyhovuje, případně doporučí jinou vhodnou kont. čočku.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/postupna-ztrata-periferniho-videni.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Řezání v oku<br /></span></span></span></span></span></span></h2>\n<p>Uz asi dva roky me trapi <strong>problem s rezanim oka</strong>. Budi me to vzdycky v noci, oko zacne hodne rezat a mam pocit, jako bych pod hornim vickem mela nejaky cizi predmet, ktery mi poskrabe cele oko. Vzdycky to trva tak <strong>15 min</strong>, nez se oko zklidni a prestane rezat a slzet. Driv se mi tento problem objevoval asi<strong> 1-2x mesicne</strong>, nicmene od Vanoc mam male miminko, ke kteremu v noci vstavam a oci jsou tudiz unavenejsi a toto rezani se zacalo objevovat dost casto, <strong>nekolikrat tydne</strong>. <strong>Oko je potom zarudle, natekle</strong>. Obcas se mi to stane i 2x za noc, prakticky lekar mi rekl, ze tam mam zanet a dal mi na nej Maxitrol, ocni lekarka si mysli, ze mam<strong> syndrom sucheho oka</strong> a kapu umele slzy, pripadne <strong>zkousim lecit jako alergii</strong>. Nicmene nic nepomaha.nProsim, poradte, co by to mohlo byt, ci jake vysetreni si mam vyzadat.</p>\n<h3>8. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>V první řadě zkuste na <strong>noc používat umělé slzy ve formě gelu</strong> (př. Visidic). Na oku <strong>vytvoří silnější film</strong>, který by mohl vydržet celou noc. A co bych doporučil...kdyby jste byla moje pacientka, tak bych se pokoušel s Vámi domluvit na <strong>akutní ranní návštěvě v den vyskytnutí obtíží</strong>. Vyloučit je třeba tzv. <strong>recidivující erozi</strong>, tedy opakovaně se vyskytující poškrábání rohovky které se nedaří zcela přehojit. Dále je třeba se <strong>soustředit na endotelovou vrstvu rohovky</strong>, na nichž se mohou projevit změny způsobující stejné potíže jaké popisujete. <strong>Eroze se vyšetřuje pomocí fluoresceinového barviva a štěrbinové lampy</strong>. Na té e také možné vyšetřit i endotel...existuje i endotelový mikroskop (speciální vyšetření).</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/usheruv-syndrom.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Suspektní glaukom<br /></span></span></span></span></span></span></h2>\n<p>Mohl bych se prosim zeptat<strong> co to znamena suspektni glaukom</strong>? Byl jsem dnes na <strong>vysetreni ocniho nervu</strong>, pan doktor mi rekl, ze je vse v poradku, jen jsem byl trochu zaskoceny, kdyz jsem cetl zpravu co mi dal a tam bylo napsano Diagnoza: H 400 suspektni glaukom.</p>\n<h3>8. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Suspektní znamená, že <strong>lékař měl podezření na přítomnost onemocnění</strong> a je třeba <strong>dalších vyšetřeních k potvrzení či vyvrácení diagnózy</strong>. Takže diagnóza H400 se používá <strong>při nálezu</strong>, který není <strong>zcela jednoznačný</strong>, ale jsou přítomny např. <strong>změny na terči zrakového nervu</strong> nebo byl zjištěn <strong>vysoký nitrooční tlak</strong>. Ve Vašem případě se jednalo o <strong>atypický vzhled terčů zrakových nervů</strong>. Jak je řečeno ve zprávě - jsou oproti populačnímu průměru malé a tak je vhodné sledování. Ale zelený zákal nemáte.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/atrofie-ocniho-nervu.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table><p><span class="article_separator"> </span></p>\n<table class="contentpaneopen"><tbody><tr><td colspan="2" valign="top"><br /><hr /><h2><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1"><span class="quote2"><span class="quote1">Špatné zaostřování s novými brýlemi<br /></span></span></span></span></span></span></h2>\n<p>Dle mych stavajicich bryli na dalku jsem si <strong>nechal vyrobit slunecni bryle</strong> - tzn. stejne dioptrie, jen s 85% sedym zabarvenim. Ovsem pri jejich noseni mam <strong>obraz ostry pouze pri primem pohledu</strong>, do boku je obraz rozostren, nahoru a dolu se obraz "kymaci" a meni perspektivu, <strong>spatne se zaostruje mezi vzdalenymi a blizkymi predmety</strong>. Optik tvrdi, ze <strong>cocky jsou v poradku a mam si zvyknout</strong>. Jiz tyden je zkousim nosit, vydrzim to max. 15min., je mi nevolno, az zvracim. Vzhledem k tomu, ze takto <strong>zabarvene dioptricke bryle nosim radu let</strong>, mam s nimi jen dobre zkusenosti. Jak lze <strong>takoveto bryle reklamovat</strong>, kdyz: cocky jsou v poradku, dioptrie jsou stejne jako stavajici bezne nosene bryle, ale presto je mi z nich spatne a spatne vidim?</p>\n<h3>8. 5. 2013, Odpověď týmu očního centra:</h3>\n<p>Pokud jsou brýle <strong>astigmatické nebo mají vyšší dioptrie</strong>, tak velkou roli hraje <strong>prohnutí brýlí a centrace dioptrických čoček.</strong> Mohu doporučit pouze <strong>nezávislé překontrolování zábrusu</strong> v jiné optice (vemte s sebou nové i staré brýle). Můžete ještě <strong>trvat na odeslání brýlových skel na reklamaci k výrobci</strong>, aby překontroloval, zda jsou všechny optické vlastnosti brýlové čočky v pořádku - to v optice zkontrolovat nelze. Pokud bude vše v pořádku, tak <strong>reklamace je velmi komplikovaná</strong>, protože nic nebylo provedeno nesprávně a <strong>chyba je s velkou pravděpodobností ve výběru obruby</strong>. Přivyknutí je maximálně do měsíce.</p>\n</td>\n</tr><tr><td colspan="2"><a href="http://www.prodej-cocek.cz/clanky/poradna/zmereni-dioptricke-vady-na-klinice-oftex.html" class="readon"> Celý článek...</a></td>\n</tr></tbody></table>',
   'online-poradna');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cms_role`
--

CREATE TABLE IF NOT EXISTS `ps_cms_role` (
  `id_cms_role` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`        VARCHAR(50)      NOT NULL,
  `id_cms`      INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cms_role`, `id_cms`),
  UNIQUE KEY `name` (`name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_cms_role`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cms_role_lang`
--

CREATE TABLE IF NOT EXISTS `ps_cms_role_lang` (
  `id_cms_role` INT(11) UNSIGNED NOT NULL,
  `id_lang`     INT(11) UNSIGNED NOT NULL,
  `id_shop`     INT(11) UNSIGNED NOT NULL,
  `name`        VARCHAR(128) DEFAULT NULL,
  PRIMARY KEY (`id_cms_role`, `id_lang`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cms_role_lang`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cms_shop`
--

CREATE TABLE IF NOT EXISTS `ps_cms_shop` (
  `id_cms`  INT(11) UNSIGNED NOT NULL,
  `id_shop` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_cms`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_cms_shop`
--

INSERT INTO `ps_cms_shop` (`id_cms`, `id_shop`) VALUES
  (1, 1),
  (2, 1),
  (3, 1),
  (4, 1),
  (5, 1),
  (6, 1),
  (7, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_compare`
--

CREATE TABLE IF NOT EXISTS `ps_compare` (
  `id_compare`  INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_customer` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_compare`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_compare`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_compare_product`
--

CREATE TABLE IF NOT EXISTS `ps_compare_product` (
  `id_compare` INT(10) UNSIGNED NOT NULL,
  `id_product` INT(10) UNSIGNED NOT NULL,
  `date_add`   DATETIME         NOT NULL,
  `date_upd`   DATETIME         NOT NULL,
  PRIMARY KEY (`id_compare`, `id_product`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_compare_product`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_condition`
--

CREATE TABLE IF NOT EXISTS `ps_condition` (
  `id_condition`       INT(11)                                  NOT NULL AUTO_INCREMENT,
  `id_ps_condition`    INT(11)                                  NOT NULL,
  `type`               ENUM ('configuration', 'install', 'sql') NOT NULL,
  `request`            TEXT,
  `operator`           VARCHAR(32)                                       DEFAULT NULL,
  `value`              VARCHAR(64)                                       DEFAULT NULL,
  `result`             VARCHAR(64)                                       DEFAULT NULL,
  `calculation_type`   ENUM ('hook', 'time')                             DEFAULT NULL,
  `calculation_detail` VARCHAR(64)                                       DEFAULT NULL,
  `validated`          TINYINT(1) UNSIGNED                      NOT NULL DEFAULT '0',
  `date_add`           DATETIME                                 NOT NULL,
  `date_upd`           DATETIME                                 NOT NULL,
  PRIMARY KEY (`id_condition`, `id_ps_condition`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 254;

--
-- Vypisuji data pro tabulku `ps_condition`
--

INSERT INTO `ps_condition` (`id_condition`, `id_ps_condition`, `type`, `request`, `operator`, `value`, `result`, `calculation_type`, `calculation_detail`, `validated`, `date_add`, `date_upd`)
VALUES
  (1, 19, 'install', '', '>', '0', '1', 'time', '1', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (2, 40, 'install', '', '>=', '730', '', 'time', '2', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (3, 121, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%moneybookers%"', '==', '0', '0', 'time', '1', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (4, 147, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%hipay%"', '==', '0', '0', 'time', '1', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (5, 152, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%paypal%"', '==', '0', '0', 'time', '1', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (6, 155, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%trustly%"', '==', '0', '0', 'time', '1', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (7, 142, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%paypal%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (8, 6, 'sql', 'SELECT COUNT(distinct m.id_module) FROM PREFIX_hook h LEFT JOIN PREFIX_hook_module hm ON h.id_hook = hm.id_hook LEFT JOIN PREFIX_module m ON hm.id_module = m.id_module\r\nWHERE (h.name = "displayPayment" OR h.name = "payment") AND m.name NOT IN ("bankwire", "cheque", "cashondelivery")', '>', '0', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (9, 1, 'configuration', 'PS_REWRITING_SETTINGS', '==', '1', '1', 'hook', 'actionAdminMetaControllerUpdate_optionsAfter', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (10, 2, 'configuration', 'PS_SMARTY_FORCE_COMPILE', '!=', '2', '1', 'hook', 'actionAdminPerformanceControllerSaveAfter', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (11, 3, 'configuration', 'PS_CSS_THEME_CACHE', '==', '1', '', 'hook', 'actionAdminPerformanceControllerSaveAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (12, 4, 'configuration', 'PS_CIPHER_ALGORITHM', '==', '1', '1', 'hook', 'actionAdminPerformanceControllerSaveAfter', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (13, 5, 'configuration', 'PS_MEDIA_SERVERS', '==', '1', '', 'hook', 'actionAdminPerformanceControllerSaveAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (14, 7, 'sql', 'SELECT COUNT(distinct m.id_module) FROM PREFIX_hook h LEFT JOIN PREFIX_hook_module hm ON h.id_hook = hm.id_hook LEFT JOIN PREFIX_module m ON hm.id_module = m.id_module\r\nWHERE (h.name = "displayPayment" OR h.name = "payment") AND m.name NOT IN ("bankwire", "cheque", "cashondelivery")', '>', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (15, 8, 'sql', 'SELECT COUNT(*) FROM PREFIX_carrier WHERE name NOT IN ("0", "My carrier")', '>', '0', '0', 'hook', 'actionObjectCarrierAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (16, 9, 'sql', 'SELECT COUNT(*) FROM PREFIX_carrier WHERE name NOT IN ("0", "My carrier")', '>', '1', '0', 'hook', 'actionObjectCarrierAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (17, 10, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE "demo_%"', '>', '0', '0', 'hook', 'actionObjectProductAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (18, 11, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE "demo_%"', '>', '9', '0', 'hook', 'actionObjectProductAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (19, 16, 'configuration', 'PS_SHOP_PHONE', '!=', '0', '1', 'hook', 'actionAdminStoresControllerUpdate_optionsAfter', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (20, 17, 'sql', 'SELECT COUNT(*) FROM PREFIX_contact', '>', '2', '2', 'hook', 'actionObjectContactAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (21, 18, 'sql', 'SELECT COUNT(*) FROM PREFIX_contact', '>', '4', '2', 'hook', 'actionObjectContactAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (22, 12, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE "demo_%"', '>', '99', '0', 'hook', 'actionObjectProductAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (23, 13, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE "demo_%"', '>', '999', '0', 'hook', 'actionObjectProductAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (24, 14, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE "demo_%"', '>', '9999', '0', 'hook', 'actionObjectProductAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (25, 15, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE "demo_%"', '>', '99999', '0', 'hook', 'actionObjectProductAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (26, 20, 'install', '', '>=', '7', '1', 'time', '1', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (27, 21, 'configuration', 'PS_LOGO', '!=', 'logo.jpg', '1', 'hook', 'actionAdminThemesControllerUpdate_optionsAfter', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (28, 22, 'sql', 'SELECT COUNT(*) FROM PREFIX_theme WHERE directory != "default" AND directory != "prestashop" AND directory ! "default-bootstrap"', '>', '0', '0', 'hook', 'actionObjectShopUpdateAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (29, 23, 'configuration', 'PS_LOGGED_ON_ADDONS', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (30, 24, 'configuration', 'PS_MULTISHOP_FEATURE_ACTIVE', '==', '1', '', 'hook', 'actionAdminPreferencesControllerUpdate_optionsAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (31, 25, 'sql', 'SELECT COUNT(*) FROM PREFIX_shop', '>', '1', '1', 'hook', 'actionObjectShopAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (32, 28, 'sql', 'SELECT COUNT(*) FROM PREFIX_shop_group', '>', '1', '1', 'hook', 'actionObjectShopGroupAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (33, 26, 'sql', 'SELECT COUNT(*) FROM PREFIX_shop', '>', '4', '1', 'hook', 'actionObjectShopAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (34, 27, 'sql', 'SELECT COUNT(*) FROM PREFIX_shop_group', '>', '5', '1', 'hook', 'actionObjectShopGroupAddAfter 	', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (35, 30, 'sql', 'SELECT COUNT(*) FROM PREFIX_carrier WHERE name NOT IN ("0", "My carrier")', '>', '2', '0', 'hook', 'actionObjectCarrierAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (36, 29, 'sql', 'SELECT COUNT(distinct m.id_module) FROM PREFIX_hook h LEFT JOIN PREFIX_hook_module hm ON h.id_hook = hm.id_hook LEFT JOIN PREFIX_module m ON hm.id_module = m.id_module\r\nWHERE (h.name = "displayPayment" OR h.name = "payment") AND m.name NOT IN ("bankwire", "cheque", "cashondelivery")', '>', '2', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (37, 31, 'sql', 'SELECT SUM(total_paid_tax_excl / c.conversion_rate)\r\nFROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1 AND reference != "XKBKNABJK"', '>=', '3000', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (38, 32, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1 AND reference != "XKBKNABJK"', '>=', '30000', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (39, 33, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1 AND reference != "XKBKNABJK"', '>=', '300000', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (40, 34, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1', '>=', '3000000', '0', 'time', '7', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (41, 35, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1', '>=', '30000000', '0', 'time', '7', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (42, 36, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1', '>=', '300000000', '0', 'time', '7', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (43, 37, 'install', '', '>=', '30', '1', 'time', '1', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (44, 38, 'install', '', '>=', '182', '', 'time', '2', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (45, 39, 'install', '', '>=', '365', '', 'time', '2', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (46, 41, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '10', '54', 'time', '1', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (47, 42, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '100', '189', 'time', '1', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (48, 43, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '1000', '222', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (49, 44, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '10000', '191', 'time', '2', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (50, 45, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '100000', '2', 'time', '3', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (51, 46, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '1000000', '2', 'time', '4', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (52, 47, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != "b44a6d9efd7a0076a0fbce6b15eaf3b1"', '>=', '2', '2', 'hook', 'actionObjectCartAddAfter', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (53, 48, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != "b44a6d9efd7a0076a0fbce6b15eaf3b1"', '>=', '10', '2', 'hook', 'actionObjectCartAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (54, 49, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != "b44a6d9efd7a0076a0fbce6b15eaf3b1"', '>=', '100', '2', 'hook', 'actionObjectCartAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (55, 50, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != "b44a6d9efd7a0076a0fbce6b15eaf3b1"', '>=', '1000', '2', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (56, 51, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != "b44a6d9efd7a0076a0fbce6b15eaf3b1"', '>=', '10000', '0', 'time', '4', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (57, 52, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != "b44a6d9efd7a0076a0fbce6b15eaf3b1"', '>=', '100000', '0', 'time', '8', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (58, 53, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL")', '>=', '1', '0', 'hook', 'actionObjectOrderAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (59, 54, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL")', '>=', '10', '0', 'hook', 'actionObjectOrderAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (60, 55, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL")', '>=', '100', '0', 'hook', 'actionObjectOrderAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (61, 56, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL")', '>=', '1000', '0', 'time', '2', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (62, 57, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL")', '>=', '10000', '0', 'time', '4', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (63, 58, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL")', '>=', '100000', '0', 'time', '8', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (64, 65, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '1', '0', 'hook', 'actionObjectCustomerThreadAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (65, 66, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '10', '0', 'hook', 'actionObjectCustomerThreadAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (66, 67, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '100', '0', 'hook', 'actionObjectCustomerThreadAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (67, 68, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '1000', '0', 'time', '2', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (68, 69, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '10000', '0', 'time', '4', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (69, 70, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '100000', '0', 'time', '8', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (70, 59, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != "pub@prestashop.com"', '>=', '1', '0', 'hook', 'actionObjectCustomerAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (71, 60, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != "pub@prestashop.com"', '>=', '10', '0', 'hook', 'actionObjectCustomerAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (72, 61, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != "pub@prestashop.com"', '>=', '100', '0', 'hook', 'actionObjectCustomerAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (73, 62, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != "pub@prestashop.com"', '>=', '1000', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (74, 63, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != "pub@prestashop.com"', '>=', '10000', '0', 'time', '2', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (75, 64, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != "pub@prestashop.com"', '>=', '100000', '0', 'time', '4', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (76, 71, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN ("BE","DE","FR","FX","GB","IE","LU","MC","NL")', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (77, 72, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN ("IT",\r\n"MT",\r\n"SM",\r\n"VA",\r\n"AD",\r\n"ES",\r\n"GI",\r\n"PT")', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (78, 73, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"BY",\r\n"EE",\r\n"LT",\r\n"LV",\r\n"MD",\r\n"PL",\r\n"UA",\r\n"AL",\r\n"BA",\r\n"BG",\r\n"GR",\r\n"HR",\r\n"MK",\r\n"RO",\r\n"SI",\r\n"YU",\r\n"RU"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (79, 74, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"AT",\r\n"CH",\r\n"CZ",\r\n"HU",\r\n"LI",\r\n"SK"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (80, 75, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"DK",\r\n"FI",\r\n"FO",\r\n"IS",\r\n"NO",\r\n"SE",\r\n"SJ"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (81, 76, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"CA",\r\n"GL",\r\n"PM",\r\n"US"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (82, 79, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"UM",\r\n"AS",\r\n"AU",\r\n"CK",\r\n"FJ",\r\n"FM",\r\n"GU",\r\n"KI",\r\n"MH,"\r\n"MP",\r\n"NC",\r\n"NF",\r\n"NR",\r\n"NU",\r\n"NZ",\r\n"PF",\r\n"PG",\r\n"PN",\r\n"PW",\r\n"SB",\r\n"TK",\r\n"TO",\r\n"TV",\r\n"VU",\r\n"WF",\r\n"WS"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (83, 85, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"KG",\r\n"KZ",\r\n"TJ",\r\n"TM",\r\n"UZ",\r\n"AE",\r\n"AM",\r\n"AZ",\r\n"BH",\r\n"CY",\r\n"GE",\r\n"IL",\r\n"IQ",\r\n"IR",\r\n"JO",\r\n"KW",\r\n"LB",\r\n"OM",\r\n"QA",\r\n"SA",\r\n"SY",\r\n"TR",\r\n"YE",\r\n"AF",\r\n"BD",\r\n"BT",\r\n"IN",\r\n"IO",\r\n"LK",\r\n"MV",\r\n"NP",\r\n"PK",\r\n"CN",\r\n"HK",\r\n"JP",\r\n"KP",\r\n"KR",\r\n"MO",\r\n"TW",\r\n"MN",\r\n"BN",\r\n"CC",\r\n"CX",\r\n"ID",\r\n"KH",\r\n"LA",\r\n"MM",\r\n"MY",\r\n"PH",\r\n"SG",\r\n"TH",\r\n"TP",\r\n"VN"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (84, 86, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"BZ",\r\n"CR",\r\n"GT",\r\n"HN",\r\n"MX",\r\n"NI",\r\n"PA",\r\n"SV",\r\n"AG",\r\n"AI",\r\n"AN",\r\n"AW",\r\n"BB",\r\n"BM",\r\n"BS",\r\n"CU",\r\n"DM",\r\n"DO",\r\n"GD",\r\n"GP",\r\n"HT",\r\n"JM",\r\n"KN",\r\n"KY",\r\n"LC",\r\n"MQ",\r\n"MS",\r\n"PR",\r\n"TC",\r\n"TT",\r\n"VC",\r\n"VG",\r\n"VI",\r\n"AR",\r\n"BO",\r\n"BR",\r\n"CL",\r\n"CO",\r\n"EC",\r\n"FK",\r\n"GF",\r\n"GY",\r\n"PE",\r\n"PY",\r\n"SR",\r\n"UY",\r\n"VE"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (85, 88, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"BI",\r\n"CF",\r\n"CG",\r\n"RW",\r\n"TD",\r\n"ZR",\r\n"DJ",\r\n"ER",\r\n"ET",\r\n"KE",\r\n"SO",\r\n"TZ",\r\n"UG",\r\n"KM",\r\n"MG",\r\n"MU",\r\n"RE",\r\n"SC",\r\n"YT",\r\n"AO",\r\n"BW",\r\n"LS",\r\n"MW",\r\n"MZ",\r\n"NA",\r\n"SZ",\r\n"ZA",\r\n"ZM",\r\n"ZW",\r\n"BF",\r\n"BJ",\r\n"CI",\r\n"CM",\r\n"CV",\r\n"GA",\r\n"GH",\r\n"GM",\r\n"GN",\r\n"GQ",\r\n"GW",\r\n"LR",\r\n"ML",\r\n"MR",\r\n"NE",\r\n"NG",\r\n"SL",\r\n"SN",\r\n"ST",\r\n"TG"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (86, 89, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"DZ",\r\n"EG",\r\n"EH",\r\n"LY",\r\n"MA",\r\n"SD",\r\n"TN"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (87, 90, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '2', '2', 'hook', 'actionObjectEmployeeAddAfter', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (88, 91, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '3', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (89, 92, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '5', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (90, 93, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '10', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (91, 94, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '20', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (92, 95, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '40', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (93, 96, 'sql', 'SELECT id_image FROM PREFIX_image WHERE id_image > 26', '>', '0', '0', 'hook', 'actionObjectImageAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (94, 97, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '50', '26', 'hook', 'actionObjectImageAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (95, 98, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '100', '26', 'hook', 'actionObjectImageAddAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (96, 99, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '1000', '23', 'time', '2', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (97, 100, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '10000', '23', 'time', '4', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (98, 101, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '50000', '23', 'time', '8', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (99, 102, 'sql', 'SELECT id_cms FROM PREFIX_cms WHERE id_cms > 5', '>', '0', '6', 'hook', 'actionObjectCMSAddAfter', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (100, 103, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '1', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (101, 104, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '10', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (102, 105, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '100', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (103, 107, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '500', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (104, 106, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '1000', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (105, 108, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '5000', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (106, 109, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL") AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}"', '>=', '1', '0', 'hook', 'newOrder', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (107, 110, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL") AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}"', '>=', '10', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (108, 111, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL") AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}"', '>=', '100', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (109, 113, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL") AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}"', '>=', '1000', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (110, 114, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL") AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}"', '>=', '5000', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (111, 112, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL") AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}"', '>=', '10000', '0', 'hook', 'actionOrderStatusUpdate', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (112, 165, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (''25.76500500'', ''26.13793600'', ''26.00998700'', ''25.73629600'', ''25.88674000'') AND `longitude` NOT IN (''-80.24379700'', ''-80.13943500'', ''-80.29447200'', ''-80.24479700'', ''-80.16329200'')', '>', '0', '1', 'hook', 'actionAdminStoresControllerSaveAfter', 1, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (113, 166, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (''25.76500500'', ''26.13793600'', ''26.00998700'', ''25.73629600'', ''25.88674000'') AND `longitude` NOT IN (''-80.24379700'', ''-80.13943500'', ''-80.29447200'', ''-80.24479700'', ''-80.16329200'')', '>', '1', '1', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (114, 167, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (''25.76500500'', ''26.13793600'', ''26.00998700'', ''25.73629600'', ''25.88674000'') AND `longitude` NOT IN (''-80.24379700'', ''-80.13943500'', ''-80.29447200'', ''-80.24479700'', ''-80.16329200'')', '>', '4', '1', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (115, 168, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (''25.76500500'', ''26.13793600'', ''26.00998700'', ''25.73629600'', ''25.88674000'') AND `longitude` NOT IN (''-80.24379700'', ''-80.13943500'', ''-80.29447200'', ''-80.24479700'', ''-80.16329200'')', '>', '9', '1', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (116, 169, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (''25.76500500'', ''26.13793600'', ''26.00998700'', ''25.73629600'', ''25.88674000'') AND `longitude` NOT IN (''-80.24379700'', ''-80.13943500'', ''-80.29447200'', ''-80.24479700'', ''-80.16329200'')', '>', '19', '1', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (117, 170, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (''25.76500500'', ''26.13793600'', ''26.00998700'', ''25.73629600'', ''25.88674000'') AND `longitude` NOT IN (''-80.24379700'', ''-80.13943500'', ''-80.29447200'', ''-80.24479700'', ''-80.16329200'')', '>', '49', '1', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (118, 171, 'sql', 'SELECT COUNT(*) FROM PREFIX_webservice_account', '>=', '1', '0', 'hook', 'actionAdminWebserviceControllerSaveAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (119, 172, 'sql', 'SELECT COUNT(*) FROM PREFIX_webservice_account', '>=', '2', '0', 'hook', 'actionAdminWebserviceControllerSaveAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (120, 173, 'sql', 'SELECT COUNT(*) FROM PREFIX_webservice_account', '>=', '3', '0', 'hook', 'actionAdminWebserviceControllerSaveAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (121, 174, 'sql', 'SELECT COUNT(*) FROM PREFIX_webservice_account', '>=', '4', '0', 'hook', 'actionAdminWebserviceControllerSaveAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (122, 137, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "hipay%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (123, 255, 'configuration', 'HIPAY_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (124, 362, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%hipay%" AND os.logable = 1', '>=', '1', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (125, 363, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%hipay%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (126, 141, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%ogone%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (127, 291, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''OGONE_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''OGONE_MODE'') AND ( value = ''live''))', '==', '2', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (128, 369, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%ogone%" AND os.logable = 1', '>=', '1', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (129, 391, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%ogone%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (130, 301, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''PAYPAL_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''PAYPAL_SANDBOX'') AND ( value = ''0''))', '==', '2', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (131, 371, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%paypal%" AND os.logable = 1', '>=', '1', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (132, 372, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%paypal%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (133, 320, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%shopgate%" ', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (134, 322, 'configuration', 'SHOPGATE_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (135, 375, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%shopgate%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '1', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (136, 376, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%shopgate%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (137, 140, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%moneybookers%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (138, 326, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''MONEYBOOKERS_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''MB_PAY_TO_EMAIL '') AND ( value != ''testaccount2@moneybookers.com ''))', '==', '2', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (139, 377, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%moneybookers%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '1', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (140, 394, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%sofortbanking%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (141, 332, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%textmaster%" ', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (142, 334, 'configuration', 'TEXTMASTER_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (143, 381, 'sql', 'SELECT COUNT(*) FROM PREFIX_textmaster_project WHERE status = "completed"', '>=', '1', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (144, 396, 'sql', 'SELECT COUNT(*) FROM PREFIX_textmaster_project WHERE status = "completed"', '>=', '10', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (145, 416, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%pigmbhpaymill%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (146, 418, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%pigmbhpaymill%" AND os.logable = 1', '>=', '1', '0', 'time', '7', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (147, 419, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%pigmbhpaymill%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (148, 428, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%authorizeaim%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (149, 429, 'configuration', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''AUTHORIZEAIM_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''AUTHORIZE_AIM_SANDBOX'') AND ( value = ''0''))', '==', '2', '', 'time', '2', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (150, 430, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%authorizeaim%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (151, 431, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%authorizeaim%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (152, 136, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%ebay%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (153, 209, 'configuration', 'EBAY_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (154, 358, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%ebay%" AND os.logable = 1', '>=', '1', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (155, 359, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%ebay%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (156, 438, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%payplug%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (157, 439, 'configuration', 'PAYPLUG_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (158, 440, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%payplug%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (159, 441, 'sql', 'SELECT SUM(o.total_paid) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%payplug%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '10000', '0', 'time', '7', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (160, 442, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%affinityitems%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (161, 443, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE name LIKE ''AFFINITYITEMS_CONFIGURATION_OK'' AND value = ''1''', '==', '1', '0', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (162, 446, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%dpdpoland%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (163, 447, 'configuration', 'DPDPOLAND_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:06', '2017-02-22 15:58:06'),
  (164, 448, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like "%dpdpoland%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (165, 449, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like "%dpdpoland%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '100', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (166, 450, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%envoimoinscher%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (167, 451, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''ENVOIMOINSCHER_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''EMC_ENV '') AND ( value != ''TEST''))', '==', '2', '0', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (168, 452, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like "%envoimoinscher%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (169, 453, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like "%envoimoinscher%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '100', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (170, 454, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%klikandpay%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (171, 455, 'configuration', 'KLIKANDPAY_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (172, 456, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%klikandpay%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (173, 457, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%klikandpay%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (174, 458, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%clickline%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (175, 459, 'configuration', 'CLICKLINE_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (176, 460, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like "%clickline%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (177, 461, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like "%clickline%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '100', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (178, 462, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%cdiscount%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (179, 463, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (180, 464, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%cdiscount%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (181, 465, 'sql', 'SELECT SUM(o.total_paid) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%cdiscount%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 365 DAY)', '>=', '500', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (182, 467, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%erpillicopresta%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (183, 468, 'configuration', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''ERPILLICOPRESTA_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''ERP_LICENCE_VALIDITY '') AND ( value == ''1'')) OR (( name LIKE ''ERP_MONTH_FREE_ACTIVE '') AND ( value == ''0''))', '==', '3', '', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (184, 469, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (185, 470, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (186, 471, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%netreviews%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (187, 472, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''NETREVIEWS_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''AVISVERIFIES_URLCERTIFICAT '') AND ( value IS NOT LIKE ''%preprod%''))', '==', '2', '0', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (188, 473, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (189, 474, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (190, 475, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%bluesnap%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (191, 476, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''BLUESNAP_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''BLUESNAP_SANDBOX '') AND ( value NOT LIKE ''%sandbox%''))', '==', '2', '0', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (192, 477, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%bluesnap%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (193, 478, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%bluesnap%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (194, 479, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%desjardins%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (195, 480, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''DESJARDINS_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''DESJARDINS_MODE '') AND ( value NOT LIKE ''%test%''))', '==', '2', '0', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (196, 481, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%desjardins%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (197, 482, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%desjardins%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (198, 483, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%firstdata%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (199, 484, 'configuration', 'FIRSTDATA_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (200, 485, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%firstdata%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (201, 486, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%firstdata%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (202, 487, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%giveit%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (203, 488, 'sql', 'GIVEIT_CONFIGURATION_OK', '>=', '1', '0', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (204, 489, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (205, 490, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (206, 491, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%ganalytics%"', '>=', '1', '1', 'hook', 'actionModuleInstallAfter', 1, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (207, 492, 'configuration', 'GANALYTICS_CONFIGURATION_OK', '==', '1', '1', 'time', '1', 1, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (208, 493, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (209, 494, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (210, 496, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%pagseguro%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (211, 497, 'configuration', 'PAGSEGURO_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (212, 498, 'sql',
        'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%pagseguro%" AND os.logable = 1',
        '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07');
INSERT INTO `ps_condition` (`id_condition`, `id_ps_condition`, `type`, `request`, `operator`, `value`, `result`, `calculation_type`, `calculation_detail`, `validated`, `date_add`, `date_upd`)
VALUES
  (213, 499, 'sql',
        'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%pagseguro%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)',
        '>=', '30', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (214, 500, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%paypalmx%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (215, 501, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''PAYPALMX_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''PAYPAL_MX_SANDBOX'') AND ( value = ''0''))', '==', '2', '0', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (216, 502, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%paypalmx%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (217, 503, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%paypalmx%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (218, 505, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%paypalusa%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (219, 506, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''PAYPALUSA_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''PAYPAL_USA_SANDBOX'') AND ( value = ''0''))', '==', '2', '0', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (220, 507, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%paypalusa%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (221, 508, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%paypalmx%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (222, 509, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%payulatam%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (223, 510, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''PAYULATAM_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''PAYU_LATAM_TEST'') AND ( value = ''1''))', '==', '2', '0', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (224, 511, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%payulatam%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (225, 512, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%payulatam%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (226, 513, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%prestastats%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (227, 514, 'configuration', 'PRESTASTATS_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (228, 515, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (229, 516, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (230, 517, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%riskified%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (231, 518, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''RISKIFIED_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''RISKIFIED_MODE'') AND ( value = ''1''))', '==', '2', '0', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (232, 519, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%riskified%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (233, 520, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%riskified%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (234, 521, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%simplifycommerce%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (235, 522, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''SIMPLIFY_CONFIGURATION_OK'') AND ( value = ''1'')) OR (( name LIKE ''SIMPLIFY_MODE'') AND ( value = ''1''))', '==', '2', '0', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (236, 523, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%simplifycommerce%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (237, 524, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%simplifycommerce%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (238, 525, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%vtpayment%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (239, 526, 'configuration', 'VTPAYMENT_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (240, 527, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%vtpayment%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (241, 528, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%vtpayment%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (242, 529, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%yotpo%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (243, 530, 'configuration', 'YOTPO_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (244, 531, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (245, 532, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (246, 533, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%yotpo%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (247, 534, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE ''YOUSTICERESOLUTIONSYSTEM_CONF_OK'') AND ( value = ''1'')) OR (( name LIKE ''YRS_SANDBOX'') AND ( value = ''0''))', '==', '2', '0', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (248, 535, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (249, 536, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (250, 537, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%loyaltylion%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (251, 538, 'configuration', 'LOYALTYLION_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (252, 539, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07'),
  (253, 540, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2017-02-22 15:58:07', '2017-02-22 15:58:07');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_condition_advice`
--

CREATE TABLE IF NOT EXISTS `ps_condition_advice` (
  `id_condition` INT(11)             NOT NULL,
  `id_advice`    INT(11)             NOT NULL,
  `display`      TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_condition`, `id_advice`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_condition_advice`
--

INSERT INTO `ps_condition_advice` (`id_condition`, `id_advice`, `display`) VALUES
  (1, 1, 1),
  (1, 2, 1),
  (1, 4, 1),
  (1, 5, 1),
  (1, 6, 1),
  (1, 8, 1),
  (1, 9, 1),
  (1, 10, 1),
  (1, 12, 1),
  (1, 13, 1),
  (1, 14, 1),
  (1, 16, 1),
  (1, 17, 1),
  (1, 18, 1),
  (1, 20, 1),
  (1, 21, 1),
  (1, 22, 1),
  (1, 24, 1),
  (1, 25, 1),
  (1, 26, 1),
  (1, 28, 1),
  (1, 29, 1),
  (1, 30, 1),
  (1, 32, 1),
  (1, 33, 1),
  (1, 34, 1),
  (1, 36, 1),
  (1, 37, 1),
  (1, 38, 1),
  (1, 40, 1),
  (1, 41, 1),
  (1, 42, 1),
  (1, 44, 1),
  (1, 45, 1),
  (1, 46, 1),
  (1, 48, 1),
  (1, 49, 1),
  (1, 50, 1),
  (1, 52, 1),
  (2, 1, 0),
  (2, 5, 0),
  (2, 9, 0),
  (2, 13, 0),
  (2, 17, 0),
  (2, 21, 0),
  (2, 25, 0),
  (2, 29, 0),
  (2, 33, 0),
  (2, 37, 0),
  (2, 41, 0),
  (2, 45, 0),
  (2, 49, 0),
  (3, 2, 1),
  (3, 6, 1),
  (3, 10, 1),
  (3, 14, 1),
  (3, 18, 1),
  (3, 22, 1),
  (3, 26, 1),
  (3, 30, 1),
  (3, 34, 1),
  (3, 38, 1),
  (3, 42, 1),
  (3, 46, 1),
  (3, 50, 1),
  (4, 2, 1),
  (4, 6, 1),
  (4, 10, 1),
  (4, 14, 1),
  (4, 18, 1),
  (4, 22, 1),
  (4, 26, 1),
  (4, 30, 1),
  (4, 34, 1),
  (4, 38, 1),
  (4, 42, 1),
  (4, 46, 1),
  (4, 50, 1),
  (5, 2, 1),
  (5, 3, 1),
  (5, 6, 1),
  (5, 7, 1),
  (5, 10, 1),
  (5, 11, 1),
  (5, 14, 1),
  (5, 15, 1),
  (5, 18, 1),
  (5, 19, 1),
  (5, 22, 1),
  (5, 23, 1),
  (5, 26, 1),
  (5, 27, 1),
  (5, 30, 1),
  (5, 31, 1),
  (5, 34, 1),
  (5, 35, 1),
  (5, 38, 1),
  (5, 39, 1),
  (5, 42, 1),
  (5, 43, 1),
  (5, 46, 1),
  (5, 47, 1),
  (5, 50, 1),
  (5, 51, 1),
  (6, 2, 1),
  (6, 6, 1),
  (6, 10, 1),
  (6, 14, 1),
  (6, 18, 1),
  (6, 22, 1),
  (6, 26, 1),
  (6, 30, 1),
  (6, 34, 1),
  (6, 38, 1),
  (6, 42, 1),
  (6, 46, 1),
  (6, 50, 1),
  (7, 2, 0),
  (7, 3, 0),
  (7, 6, 0),
  (7, 7, 0),
  (7, 10, 0),
  (7, 11, 0),
  (7, 14, 0),
  (7, 15, 0),
  (7, 18, 0),
  (7, 19, 0),
  (7, 22, 0),
  (7, 23, 0),
  (7, 26, 0),
  (7, 27, 0),
  (7, 30, 0),
  (7, 31, 0),
  (7, 34, 0),
  (7, 35, 0),
  (7, 38, 0),
  (7, 39, 0),
  (7, 42, 0),
  (7, 43, 0),
  (7, 46, 0),
  (7, 47, 0),
  (7, 50, 0),
  (7, 51, 0),
  (8, 3, 1),
  (8, 7, 1),
  (8, 11, 1),
  (8, 15, 1),
  (8, 19, 1),
  (8, 23, 1),
  (8, 27, 1),
  (8, 31, 1),
  (8, 35, 1),
  (8, 39, 1),
  (8, 43, 1),
  (8, 47, 1),
  (8, 51, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_condition_badge`
--

CREATE TABLE IF NOT EXISTS `ps_condition_badge` (
  `id_condition` INT(11) NOT NULL,
  `id_badge`     INT(11) NOT NULL,
  PRIMARY KEY (`id_condition`, `id_badge`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_condition_badge`
--

INSERT INTO `ps_condition_badge` (`id_condition`, `id_badge`) VALUES
  (1, 150),
  (2, 171),
  (7, 9),
  (8, 137),
  (9, 134),
  (10, 135),
  (11, 135),
  (12, 135),
  (13, 136),
  (14, 138),
  (15, 139),
  (16, 140),
  (17, 141),
  (18, 142),
  (19, 143),
  (20, 144),
  (21, 145),
  (22, 146),
  (23, 147),
  (24, 148),
  (25, 149),
  (26, 151),
  (27, 152),
  (28, 153),
  (29, 154),
  (30, 155),
  (31, 156),
  (32, 157),
  (33, 158),
  (34, 159),
  (35, 160),
  (36, 161),
  (37, 162),
  (38, 163),
  (39, 164),
  (40, 165),
  (41, 166),
  (42, 167),
  (43, 168),
  (44, 169),
  (45, 170),
  (46, 172),
  (47, 173),
  (48, 174),
  (49, 175),
  (50, 176),
  (51, 177),
  (52, 178),
  (53, 179),
  (54, 180),
  (55, 181),
  (56, 182),
  (57, 183),
  (58, 184),
  (59, 185),
  (60, 186),
  (61, 187),
  (62, 188),
  (63, 189),
  (64, 190),
  (65, 191),
  (66, 192),
  (67, 193),
  (68, 194),
  (69, 195),
  (70, 196),
  (71, 197),
  (72, 198),
  (73, 199),
  (74, 200),
  (75, 201),
  (76, 202),
  (77, 203),
  (78, 204),
  (79, 205),
  (80, 206),
  (81, 207),
  (82, 208),
  (83, 209),
  (84, 210),
  (85, 211),
  (86, 212),
  (87, 213),
  (88, 214),
  (89, 215),
  (90, 216),
  (91, 217),
  (92, 218),
  (93, 219),
  (94, 220),
  (95, 221),
  (96, 222),
  (97, 223),
  (98, 224),
  (99, 225),
  (100, 226),
  (101, 227),
  (102, 228),
  (103, 229),
  (104, 230),
  (105, 231),
  (106, 232),
  (107, 233),
  (108, 234),
  (109, 235),
  (110, 236),
  (111, 237),
  (112, 238),
  (113, 239),
  (114, 240),
  (115, 241),
  (116, 242),
  (117, 243),
  (118, 244),
  (119, 245),
  (120, 246),
  (121, 247),
  (122, 1),
  (123, 2),
  (124, 3),
  (125, 4),
  (126, 5),
  (127, 6),
  (128, 7),
  (129, 8),
  (130, 10),
  (131, 11),
  (132, 12),
  (133, 13),
  (134, 14),
  (135, 15),
  (136, 16),
  (137, 17),
  (138, 18),
  (139, 19),
  (140, 20),
  (141, 21),
  (142, 22),
  (143, 23),
  (144, 24),
  (145, 25),
  (146, 26),
  (147, 27),
  (148, 28),
  (149, 29),
  (150, 30),
  (151, 31),
  (152, 32),
  (153, 33),
  (154, 34),
  (155, 35),
  (156, 36),
  (157, 37),
  (158, 38),
  (159, 39),
  (160, 40),
  (161, 41),
  (162, 42),
  (163, 43),
  (164, 44),
  (165, 45),
  (166, 46),
  (167, 47),
  (168, 48),
  (169, 49),
  (170, 50),
  (171, 51),
  (172, 52),
  (173, 53),
  (174, 54),
  (175, 55),
  (176, 56),
  (177, 57),
  (178, 58),
  (179, 59),
  (180, 60),
  (181, 61),
  (182, 62),
  (183, 63),
  (184, 64),
  (185, 65),
  (186, 66),
  (187, 67),
  (188, 68),
  (189, 69),
  (190, 70),
  (191, 71),
  (192, 72),
  (193, 73),
  (194, 74),
  (195, 75),
  (196, 76),
  (197, 77),
  (198, 78),
  (199, 79),
  (200, 80),
  (201, 81),
  (202, 82),
  (203, 83),
  (204, 84),
  (205, 85),
  (206, 86),
  (207, 87),
  (208, 88),
  (209, 89),
  (210, 90),
  (211, 91),
  (212, 92),
  (213, 93),
  (214, 94),
  (215, 95),
  (216, 96),
  (217, 97),
  (218, 98),
  (219, 99),
  (220, 100),
  (221, 101),
  (222, 102),
  (223, 103),
  (224, 104),
  (225, 105),
  (226, 106),
  (227, 107),
  (228, 108),
  (229, 109),
  (230, 110),
  (231, 111),
  (232, 112),
  (233, 113),
  (234, 114),
  (235, 115),
  (236, 116),
  (237, 117),
  (238, 118),
  (239, 119),
  (240, 120),
  (241, 121),
  (242, 122),
  (243, 123),
  (244, 124),
  (245, 125),
  (246, 126),
  (247, 127),
  (248, 128),
  (249, 129),
  (250, 130),
  (251, 131),
  (252, 132),
  (253, 133);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_configuration`
--

CREATE TABLE IF NOT EXISTS `ps_configuration` (
  `id_configuration` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop_group`    INT(11) UNSIGNED          DEFAULT NULL,
  `id_shop`          INT(11) UNSIGNED          DEFAULT NULL,
  `name`             VARCHAR(254)     NOT NULL,
  `value`            TEXT,
  `date_add`         DATETIME         NOT NULL,
  `date_upd`         DATETIME         NOT NULL,
  PRIMARY KEY (`id_configuration`),
  KEY `name` (`name`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 511;

--
-- Vypisuji data pro tabulku `ps_configuration`
--

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`)
VALUES
  (1, NULL, NULL, 'PS_LANG_DEFAULT', '1', '2016-08-04 12:42:22', '2016-08-04 12:42:22'),
  (2, NULL, NULL, 'PS_VERSION_DB', '1.6.1.6', '2016-08-04 12:42:22', '2016-08-04 12:42:22'),
  (3, NULL, NULL, 'PS_INSTALL_VERSION', '1.6.1.6', '2016-08-04 12:42:22', '2016-08-04 12:42:22'),
  (4, NULL, NULL, 'PS_GROUP_FEATURE_ACTIVE', '1', '2016-08-04 12:42:23', '2016-08-04 12:42:23'),
  (5, NULL, NULL, 'PS_CARRIER_DEFAULT', '1', '2016-08-04 12:42:24', '2016-08-04 12:42:24'),
  (6, NULL, NULL, 'PS_SEARCH_INDEXATION', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (7, NULL, NULL, 'PS_ONE_PHONE_AT_LEAST', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (8, NULL, NULL, 'PS_CURRENCY_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (9, NULL, NULL, 'PS_COUNTRY_DEFAULT', '16', '0000-00-00 00:00:00', '2016-08-04 12:42:28'),
  (10, NULL, NULL, 'PS_REWRITING_SETTINGS', '1', '0000-00-00 00:00:00', '2017-01-23 18:32:29'),
  (11, NULL, NULL, 'PS_ORDER_OUT_OF_STOCK', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (12, NULL, NULL, 'PS_LAST_QTIES', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (13, NULL, NULL, 'PS_CART_REDIRECT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (14, NULL, NULL, 'PS_CONDITIONS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (15, NULL, NULL, 'PS_RECYCLABLE_PACK', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (16, NULL, NULL, 'PS_GIFT_WRAPPING', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (17, NULL, NULL, 'PS_GIFT_WRAPPING_PRICE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (18, NULL, NULL, 'PS_STOCK_MANAGEMENT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (19, NULL, NULL, 'PS_NAVIGATION_PIPE', '>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (20, NULL, NULL, 'PS_PRODUCTS_PER_PAGE', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (21, NULL, NULL, 'PS_PURCHASE_MINIMUM', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (22, NULL, NULL, 'PS_PRODUCTS_ORDER_WAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (23, NULL, NULL, 'PS_PRODUCTS_ORDER_BY', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (24, NULL, NULL, 'PS_DISPLAY_QTIES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (25, NULL, NULL, 'PS_SHIPPING_HANDLING', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (26, NULL, NULL, 'PS_SHIPPING_FREE_PRICE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (27, NULL, NULL, 'PS_SHIPPING_FREE_WEIGHT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (28, NULL, NULL, 'PS_SHIPPING_METHOD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (29, NULL, NULL, 'PS_TAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (30, NULL, NULL, 'PS_SHOP_ENABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (31, NULL, NULL, 'PS_NB_DAYS_NEW_PRODUCT', '20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (32, NULL, NULL, 'PS_SSL_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (33, NULL, NULL, 'PS_WEIGHT_UNIT', 'kg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (34, NULL, NULL, 'PS_BLOCK_CART_AJAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (35, NULL, NULL, 'PS_ORDER_RETURN', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (36, NULL, NULL, 'PS_ORDER_RETURN_NB_DAYS', '14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (37, NULL, NULL, 'PS_MAIL_TYPE', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (38, NULL, NULL, 'PS_PRODUCT_PICTURE_MAX_SIZE', '8388608', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (39, NULL, NULL, 'PS_PRODUCT_PICTURE_WIDTH', '64', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (40, NULL, NULL, 'PS_PRODUCT_PICTURE_HEIGHT', '64', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (41, NULL, NULL, 'PS_INVOICE_PREFIX', '#IN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (42, NULL, NULL, 'PS_INVCE_INVOICE_ADDR_RULES', '{"avoid":[]}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (43, NULL, NULL, 'PS_INVCE_DELIVERY_ADDR_RULES', '{"avoid":[]}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (44, NULL, NULL, 'PS_DELIVERY_PREFIX', '#DE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (45, NULL, NULL, 'PS_DELIVERY_NUMBER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (46, NULL, NULL, 'PS_RETURN_PREFIX', '#RE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (47, NULL, NULL, 'PS_INVOICE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (48, NULL, NULL, 'PS_PASSWD_TIME_BACK', '360', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (49, NULL, NULL, 'PS_PASSWD_TIME_FRONT', '360', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (50, NULL, NULL, 'PS_DISP_UNAVAILABLE_ATTR', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (51, NULL, NULL, 'PS_SEARCH_MINWORDLEN', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (52, NULL, NULL, 'PS_SEARCH_BLACKLIST', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (53, NULL, NULL, 'PS_SEARCH_WEIGHT_PNAME', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (54, NULL, NULL, 'PS_SEARCH_WEIGHT_REF', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (55, NULL, NULL, 'PS_SEARCH_WEIGHT_SHORTDESC', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (56, NULL, NULL, 'PS_SEARCH_WEIGHT_DESC', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (57, NULL, NULL, 'PS_SEARCH_WEIGHT_CNAME', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (58, NULL, NULL, 'PS_SEARCH_WEIGHT_MNAME', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (59, NULL, NULL, 'PS_SEARCH_WEIGHT_TAG', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (60, NULL, NULL, 'PS_SEARCH_WEIGHT_ATTRIBUTE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (61, NULL, NULL, 'PS_SEARCH_WEIGHT_FEATURE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (62, NULL, NULL, 'PS_SEARCH_AJAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (63, NULL, NULL, 'PS_TIMEZONE', 'Europe/Prague', '0000-00-00 00:00:00', '2016-08-04 12:42:28'),
  (64, NULL, NULL, 'PS_THEME_V11', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (65, NULL, NULL, 'PRESTASTORE_LIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (66, NULL, NULL, 'PS_TIN_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (67, NULL, NULL, 'PS_SHOW_ALL_MODULES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (68, NULL, NULL, 'PS_BACKUP_ALL', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (69, NULL, NULL, 'PS_1_3_UPDATE_DATE', '2011-12-27 10:20:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (70, NULL, NULL, 'PS_PRICE_ROUND_MODE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (71, NULL, NULL, 'PS_1_3_2_UPDATE_DATE', '2011-12-27 10:20:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (72, NULL, NULL, 'PS_CONDITIONS_CMS_ID', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (73, NULL, NULL, 'TRACKING_DIRECT_TRAFFIC', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (74, NULL, NULL, 'PS_META_KEYWORDS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (75, NULL, NULL, 'PS_DISPLAY_JQZOOM', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (76, NULL, NULL, 'PS_VOLUME_UNIT', 'cl', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (77, NULL, NULL, 'PS_CIPHER_ALGORITHM', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (78, NULL, NULL, 'PS_ATTRIBUTE_CATEGORY_DISPLAY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (79, NULL, NULL, 'PS_CUSTOMER_SERVICE_FILE_UPLOAD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (80, NULL, NULL, 'PS_CUSTOMER_SERVICE_SIGNATURE', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (81, NULL, NULL, 'PS_BLOCK_BESTSELLERS_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (82, NULL, NULL, 'PS_BLOCK_NEWPRODUCTS_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (83, NULL, NULL, 'PS_BLOCK_SPECIALS_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (84, NULL, NULL, 'PS_STOCK_MVT_REASON_DEFAULT', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (85, NULL, NULL, 'PS_COMPARATOR_MAX_ITEM', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (86, NULL, NULL, 'PS_ORDER_PROCESS_TYPE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (87, NULL, NULL, 'PS_SPECIFIC_PRICE_PRIORITIES', 'id_shop;id_currency;id_country;id_group', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (88, NULL, NULL, 'PS_TAX_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (89, NULL, NULL, 'PS_SMARTY_FORCE_COMPILE', '1', '0000-00-00 00:00:00', '2016-09-20 15:36:28'),
  (90, NULL, NULL, 'PS_DISTANCE_UNIT', 'km', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (91, NULL, NULL, 'PS_STORES_DISPLAY_CMS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (92, NULL, NULL, 'PS_STORES_DISPLAY_FOOTER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (93, NULL, NULL, 'PS_STORES_SIMPLIFIED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (94, NULL, NULL, 'SHOP_LOGO_WIDTH', '300', '0000-00-00 00:00:00', '2016-11-03 08:06:50'),
  (95, NULL, NULL, 'SHOP_LOGO_HEIGHT', '47', '0000-00-00 00:00:00', '2016-11-03 08:06:50'),
  (96, NULL, NULL, 'EDITORIAL_IMAGE_WIDTH', '530', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (97, NULL, NULL, 'EDITORIAL_IMAGE_HEIGHT', '228', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (98, NULL, NULL, 'PS_STATSDATA_CUSTOMER_PAGESVIEWS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (99, NULL, NULL, 'PS_STATSDATA_PAGESVIEWS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (100, NULL, NULL, 'PS_STATSDATA_PLUGINS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (101, NULL, NULL, 'PS_GEOLOCATION_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (102, NULL, NULL, 'PS_ALLOWED_COUNTRIES', 'AF;ZA;AX;AL;DZ;DE;AD;AO;AI;AQ;AG;AN;SA;AR;AM;AW;AU;AT;AZ;BS;BH;BD;BB;BY;BE;BZ;BJ;BM;BT;BO;BA;BW;BV;BR;BN;BG;BF;MM;BI;KY;KH;CM;CA;CV;CF;CL;CN;CX;CY;CC;CO;KM;CG;CD;CK;KR;KP;CR;CI;HR;CU;DK;DJ;DM;EG;IE;SV;AE;EC;ER;ES;EE;ET;FK;FO;FJ;FI;FR;GA;GM;GE;GS;GH;GI;GR;GD;GL;GP;GU;GT;GG;GN;GQ;GW;GY;GF;HT;HM;HN;HK;HU;IM;MU;VG;VI;IN;ID;IR;IQ;IS;IL;IT;JM;JP;JE;JO;KZ;KE;KG;KI;KW;LA;LS;LV;LB;LR;LY;LI;LT;LU;MO;MK;MG;MY;MW;MV;ML;MT;MP;MA;MH;MQ;MR;YT;MX;FM;MD;MC;MN;ME;MS;MZ;NA;NR;NP;NI;NE;NG;NU;NF;NO;NC;NZ;IO;OM;UG;UZ;PK;PW;PS;PA;PG;PY;NL;PE;PH;PN;PL;PF;PR;PT;QA;DO;CZ;RE;RO;GB;RU;RW;EH;BL;KN;SM;MF;PM;VA;VC;LC;SB;WS;AS;ST;SN;RS;SC;SL;SG;SK;SI;SO;SD;LK;SE;CH;SR;SJ;SZ;SY;TJ;TW;TZ;TD;TF;TH;TL;TG;TK;TO;TT;TN;TM;TC;TR;TV;UA;UY;US;VU;VE;VN;WF;YE;ZM;ZW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (103, NULL, NULL, 'PS_GEOLOCATION_BEHAVIOR', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (104, NULL, NULL, 'PS_LOCALE_LANGUAGE', 'cs', '0000-00-00 00:00:00', '2016-08-04 12:42:28'),
  (105, NULL, NULL, 'PS_LOCALE_COUNTRY', 'cz', '0000-00-00 00:00:00', '2016-08-04 12:42:28'),
  (106, NULL, NULL, 'PS_ATTACHMENT_MAXIMUM_SIZE', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (107, NULL, NULL, 'PS_SMARTY_CACHE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (108, NULL, NULL, 'PS_DIMENSION_UNIT', 'cm', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (109, NULL, NULL, 'PS_GUEST_CHECKOUT_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (110, NULL, NULL, 'PS_DISPLAY_SUPPLIERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (111, NULL, NULL, 'PS_DISPLAY_BEST_SELLERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (112, NULL, NULL, 'PS_CATALOG_MODE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (113, NULL, NULL, 'PS_GEOLOCATION_WHITELIST', '127;209.185.108;209.185.253;209.85.238;209.85.238.11;209.85.238.4;216.239.33.96;216.239.33.97;216.239.33.98;216.239.33.99;216.239.37.98;216.239.37.99;216.239.39.98;216.239.39.99;216.239.41.96;216.239.41.97;216.239.41.98;216.239.41.99;216.239.45.4;216.239.46;216.239.51.96;216.239.51.97;216.239.51.98;216.239.51.99;216.239.53.98;216.239.53.99;216.239.57.96;91.240.109;216.239.57.97;216.239.57.98;216.239.57.99;216.239.59.98;216.239.59.99;216.33.229.163;64.233.173.193;64.233.173.194;64.233.173.195;64.233.173.196;64.233.173.197;64.233.173.198;64.233.173.199;64.233.173.200;64.233.173.201;64.233.173.202;64.233.173.203;64.233.173.204;64.233.173.205;64.233.173.206;64.233.173.207;64.233.173.208;64.233.173.209;64.233.173.210;64.233.173.211;64.233.173.212;64.233.173.213;64.233.173.214;64.233.173.215;64.233.173.216;64.233.173.217;64.233.173.218;64.233.173.219;64.233.173.220;64.233.173.221;64.233.173.222;64.233.173.223;64.233.173.224;64.233.173.225;64.233.173.226;64.233.173.227;64.233.173.228;64.233.173.229;64.233.173.230;64.233.173.231;64.233.173.232;64.233.173.233;64.233.173.234;64.233.173.235;64.233.173.236;64.233.173.237;64.233.173.238;64.233.173.239;64.233.173.240;64.233.173.241;64.233.173.242;64.233.173.243;64.233.173.244;64.233.173.245;64.233.173.246;64.233.173.247;64.233.173.248;64.233.173.249;64.233.173.250;64.233.173.251;64.233.173.252;64.233.173.253;64.233.173.254;64.233.173.255;64.68.80;64.68.81;64.68.82;64.68.83;64.68.84;64.68.85;64.68.86;64.68.87;64.68.88;64.68.89;64.68.90.1;64.68.90.10;64.68.90.11;64.68.90.12;64.68.90.129;64.68.90.13;64.68.90.130;64.68.90.131;64.68.90.132;64.68.90.133;64.68.90.134;64.68.90.135;64.68.90.136;64.68.90.137;64.68.90.138;64.68.90.139;64.68.90.14;64.68.90.140;64.68.90.141;64.68.90.142;64.68.90.143;64.68.90.144;64.68.90.145;64.68.90.146;64.68.90.147;64.68.90.148;64.68.90.149;64.68.90.15;64.68.90.150;64.68.90.151;64.68.90.152;64.68.90.153;64.68.90.154;64.68.90.155;64.68.90.156;64.68.90.157;64.68.90.158;64.68.90.159;64.68.90.16;64.68.90.160;64.68.90.161;64.68.90.162;64.68.90.163;64.68.90.164;64.68.90.165;64.68.90.166;64.68.90.167;64.68.90.168;64.68.90.169;64.68.90.17;64.68.90.170;64.68.90.171;64.68.90.172;64.68.90.173;64.68.90.174;64.68.90.175;64.68.90.176;64.68.90.177;64.68.90.178;64.68.90.179;64.68.90.18;64.68.90.180;64.68.90.181;64.68.90.182;64.68.90.183;64.68.90.184;64.68.90.185;64.68.90.186;64.68.90.187;64.68.90.188;64.68.90.189;64.68.90.19;64.68.90.190;64.68.90.191;64.68.90.192;64.68.90.193;64.68.90.194;64.68.90.195;64.68.90.196;64.68.90.197;64.68.90.198;64.68.90.199;64.68.90.2;64.68.90.20;64.68.90.200;64.68.90.201;64.68.90.202;64.68.90.203;64.68.90.204;64.68.90.205;64.68.90.206;64.68.90.207;64.68.90.208;64.68.90.21;64.68.90.22;64.68.90.23;64.68.90.24;64.68.90.25;64.68.90.26;64.68.90.27;64.68.90.28;64.68.90.29;64.68.90.3;64.68.90.30;64.68.90.31;64.68.90.32;64.68.90.33;64.68.90.34;64.68.90.35;64.68.90.36;64.68.90.37;64.68.90.38;64.68.90.39;64.68.90.4;64.68.90.40;64.68.90.41;64.68.90.42;64.68.90.43;64.68.90.44;64.68.90.45;64.68.90.46;64.68.90.47;64.68.90.48;64.68.90.49;64.68.90.5;64.68.90.50;64.68.90.51;64.68.90.52;64.68.90.53;64.68.90.54;64.68.90.55;64.68.90.56;64.68.90.57;64.68.90.58;64.68.90.59;64.68.90.6;64.68.90.60;64.68.90.61;64.68.90.62;64.68.90.63;64.68.90.64;64.68.90.65;64.68.90.66;64.68.90.67;64.68.90.68;64.68.90.69;64.68.90.7;64.68.90.70;64.68.90.71;64.68.90.72;64.68.90.73;64.68.90.74;64.68.90.75;64.68.90.76;64.68.90.77;64.68.90.78;64.68.90.79;64.68.90.8;64.68.90.80;64.68.90.9;64.68.91;64.68.92;66.249.64;66.249.65;66.249.66;66.249.67;66.249.68;66.249.69;66.249.70;66.249.71;66.249.72;66.249.73;66.249.78;66.249.79;72.14.199;8.6.48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (114, NULL, NULL, 'PS_LOGS_BY_EMAIL', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (115, NULL, NULL, 'PS_COOKIE_CHECKIP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (116, NULL, NULL, 'PS_STORES_CENTER_LAT', '50.2113572', '0000-00-00 00:00:00', '2017-01-23 17:51:24'),
  (117, NULL, NULL, 'PS_STORES_CENTER_LONG', '15.8258344', '0000-00-00 00:00:00', '2017-01-23 17:51:24'),
  (118, NULL, NULL, 'PS_USE_ECOTAX', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (119, NULL, NULL, 'PS_CANONICAL_REDIRECT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (120, NULL, NULL, 'PS_IMG_UPDATE_TIME', '1487685574', '0000-00-00 00:00:00', '2017-02-21 14:59:34'),
  (121, NULL, NULL, 'PS_BACKUP_DROP_TABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (122, NULL, NULL, 'PS_OS_CHEQUE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (123, NULL, NULL, 'PS_OS_PAYMENT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (124, NULL, NULL, 'PS_OS_PREPARATION', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (125, NULL, NULL, 'PS_OS_SHIPPING', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (126, NULL, NULL, 'PS_OS_DELIVERED', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (127, NULL, NULL, 'PS_OS_CANCELED', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (128, NULL, NULL, 'PS_OS_REFUND', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (129, NULL, NULL, 'PS_OS_ERROR', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (130, NULL, NULL, 'PS_OS_OUTOFSTOCK', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (131, NULL, NULL, 'PS_OS_BANKWIRE', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (132, NULL, NULL, 'PS_OS_PAYPAL', '11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (133, NULL, NULL, 'PS_OS_WS_PAYMENT', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (134, NULL, NULL, 'PS_OS_OUTOFSTOCK_PAID', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (135, NULL, NULL, 'PS_OS_OUTOFSTOCK_UNPAID', '13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (136, NULL, NULL, 'PS_OS_COD_VALIDATION', '14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (137, NULL, NULL, 'PS_LEGACY_IMAGES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (138, NULL, NULL, 'PS_IMAGE_QUALITY', 'png', '0000-00-00 00:00:00', '2016-09-20 14:59:48'),
  (139, NULL, NULL, 'PS_PNG_QUALITY', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (140, NULL, NULL, 'PS_JPEG_QUALITY', '90', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (141, NULL, NULL, 'PS_COOKIE_LIFETIME_FO', '480', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (142, NULL, NULL, 'PS_COOKIE_LIFETIME_BO', '480', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (143, NULL, NULL, 'PS_RESTRICT_DELIVERED_COUNTRIES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (144, NULL, NULL, 'PS_SHOW_NEW_ORDERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (145, NULL, NULL, 'PS_SHOW_NEW_CUSTOMERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (146, NULL, NULL, 'PS_SHOW_NEW_MESSAGES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (147, NULL, NULL, 'PS_FEATURE_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (148, NULL, NULL, 'PS_COMBINATION_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (149, NULL, NULL, 'PS_SPECIFIC_PRICE_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '2017-01-25 15:22:42'),
  (150, NULL, NULL, 'PS_SCENE_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (151, NULL, NULL, 'PS_VIRTUAL_PROD_FEATURE_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (152, NULL, NULL, 'PS_CUSTOMIZATION_FEATURE_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (153, NULL, NULL, 'PS_CART_RULE_FEATURE_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (154, NULL, NULL, 'PS_PACK_FEATURE_ACTIVE', NULL, '0000-00-00 00:00:00', '2017-02-10 11:44:49'),
  (155, NULL, NULL, 'PS_ALIAS_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (156, NULL, NULL, 'PS_TAX_ADDRESS_TYPE', 'id_address_delivery', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (157, NULL, NULL, 'PS_SHOP_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (158, NULL, NULL, 'PS_CARRIER_DEFAULT_SORT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (159, NULL, NULL, 'PS_STOCK_MVT_INC_REASON_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (160, NULL, NULL, 'PS_STOCK_MVT_DEC_REASON_DEFAULT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (161, NULL, NULL, 'PS_ADVANCED_STOCK_MANAGEMENT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (162, NULL, NULL, 'PS_ADMINREFRESH_NOTIFICATION', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (163, NULL, NULL, 'PS_STOCK_MVT_TRANSFER_TO', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (164, NULL, NULL, 'PS_STOCK_MVT_TRANSFER_FROM', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (165, NULL, NULL, 'PS_CARRIER_DEFAULT_ORDER', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (166, NULL, NULL, 'PS_STOCK_MVT_SUPPLY_ORDER', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (167, NULL, NULL, 'PS_STOCK_CUSTOMER_ORDER_REASON', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (168, NULL, NULL, 'PS_UNIDENTIFIED_GROUP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (169, NULL, NULL, 'PS_GUEST_GROUP', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (170, NULL, NULL, 'PS_CUSTOMER_GROUP', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (171, NULL, NULL, 'PS_SMARTY_CONSOLE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (172, NULL, NULL, 'PS_INVOICE_MODEL', 'invoice', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (173, NULL, NULL, 'PS_LIMIT_UPLOAD_IMAGE_VALUE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (174, NULL, NULL, 'PS_LIMIT_UPLOAD_FILE_VALUE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (175, NULL, NULL, 'MB_PAY_TO_EMAIL', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (176, NULL, NULL, 'MB_SECRET_WORD', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (177, NULL, NULL, 'MB_HIDE_LOGIN', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (178, NULL, NULL, 'MB_ID_LOGO', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (179, NULL, NULL, 'MB_ID_LOGO_WALLET', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (180, NULL, NULL, 'MB_PARAMETERS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (181, NULL, NULL, 'MB_PARAMETERS_2', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (182, NULL, NULL, 'MB_DISPLAY_MODE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (183, NULL, NULL, 'MB_CANCEL_URL', 'http://www.yoursite.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (184, NULL, NULL, 'MB_LOCAL_METHODS', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (185, NULL, NULL, 'MB_INTER_METHODS', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (186, NULL, NULL, 'BANK_WIRE_CURRENCIES', '2,1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (187, NULL, NULL, 'CHEQUE_CURRENCIES', '2,1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (188, NULL, NULL, 'PRODUCTS_VIEWED_NBR', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (189, NULL, NULL, 'BLOCK_CATEG_DHTML', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (190, NULL, NULL, 'BLOCK_CATEG_MAX_DEPTH', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (191, NULL, NULL, 'MANUFACTURER_DISPLAY_FORM', '1', '0000-00-00 00:00:00', '2016-08-04 12:42:43'),
  (192, NULL, NULL, 'MANUFACTURER_DISPLAY_TEXT', '1', '0000-00-00 00:00:00', '2016-08-04 12:42:43'),
  (193, NULL, NULL, 'MANUFACTURER_DISPLAY_TEXT_NB', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (194, NULL, NULL, 'NEW_PRODUCTS_NBR', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (195, NULL, NULL, 'PS_TOKEN_ENABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (196, NULL, NULL, 'PS_STATS_RENDER', 'graphnvd3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (197, NULL, NULL, 'PS_STATS_OLD_CONNECT_AUTO_CLEAN', 'never', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (198, NULL, NULL, 'PS_STATS_GRID_RENDER', 'gridhtml', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (199, NULL, NULL, 'BLOCKTAGS_NBR', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (200, NULL, NULL, 'CHECKUP_DESCRIPTIONS_LT', '100', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (201, NULL, NULL, 'CHECKUP_DESCRIPTIONS_GT', '400', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (202, NULL, NULL, 'CHECKUP_IMAGES_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (203, NULL, NULL, 'CHECKUP_IMAGES_GT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (204, NULL, NULL, 'CHECKUP_SALES_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (205, NULL, NULL, 'CHECKUP_SALES_GT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (206, NULL, NULL, 'CHECKUP_STOCK_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (207, NULL, NULL, 'CHECKUP_STOCK_GT', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (208, NULL, NULL, 'FOOTER_CMS', '0_3|0_4', '0000-00-00 00:00:00', '2016-08-04 12:42:43'),
  (209, NULL, NULL, 'FOOTER_BLOCK_ACTIVATION', '1', '0000-00-00 00:00:00', '2017-01-23 16:10:19'),
  (210, NULL, NULL, 'FOOTER_POWEREDBY', '0', '0000-00-00 00:00:00', '2017-01-23 16:10:19'),
  (211, NULL, NULL, 'BLOCKADVERT_LINK', 'http://www.prestashop.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (212, NULL, NULL, 'BLOCKSTORE_IMG', 'store.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (213, NULL, NULL, 'BLOCKADVERT_IMG_EXT', 'jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (214, NULL, NULL, 'MOD_BLOCKTOPMENU_ITEMS', 'CAT3,CAT8,CAT5,LNK1', '0000-00-00 00:00:00', '2016-08-04 12:42:44'),
  (215, NULL, NULL, 'MOD_BLOCKTOPMENU_SEARCH', '0', '0000-00-00 00:00:00', '2016-08-04 12:42:44'),
  (216, NULL, NULL, 'BLOCKSOCIAL_FACEBOOK', 'http://www.facebook.com/prestashop', '0000-00-00 00:00:00', '2016-08-04 12:42:40'),
  (217, NULL, NULL, 'BLOCKSOCIAL_TWITTER', 'http://www.twitter.com/prestashop', '0000-00-00 00:00:00', '2016-08-04 12:42:40'),
  (218, NULL, NULL, 'BLOCKSOCIAL_RSS', 'http://www.prestashop.com/blog/en/', '0000-00-00 00:00:00', '2016-08-04 12:42:40'),
  (219, NULL, NULL, 'BLOCKCONTACTINFOS_COMPANY', 'Ophtha Trade, s.r.o.', '0000-00-00 00:00:00', '2017-01-23 16:06:10'),
  (220, NULL, NULL, 'BLOCKCONTACTINFOS_ADDRESS', 'Petra Mencová\r\nPalackého 191\r\n537 01 Chrudim', '0000-00-00 00:00:00', '2017-01-23 16:06:10'),
  (221, NULL, NULL, 'BLOCKCONTACTINFOS_PHONE', '+420 608 800 886', '0000-00-00 00:00:00', '2017-01-23 16:06:10'),
  (222, NULL, NULL, 'BLOCKCONTACTINFOS_EMAIL', 'info@prodej-cocek.cz', '0000-00-00 00:00:00', '2017-01-23 16:06:10'),
  (223, NULL, NULL, 'BLOCKCONTACT_TELNUMBER', '0123-456-789', '0000-00-00 00:00:00', '2016-08-04 12:42:43'),
  (224, NULL, NULL, 'BLOCKCONTACT_EMAIL', 'sales@yourcompany.com', '0000-00-00 00:00:00', '2016-08-04 12:42:43'),
  (225, NULL, NULL, 'SUPPLIER_DISPLAY_TEXT', '1', '0000-00-00 00:00:00', '2016-08-04 12:42:44'),
  (226, NULL, NULL, 'SUPPLIER_DISPLAY_TEXT_NB', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (227, NULL, NULL, 'SUPPLIER_DISPLAY_FORM', '1', '0000-00-00 00:00:00', '2016-08-04 12:42:44'),
  (228, NULL, NULL, 'BLOCK_CATEG_NBR_COLUMN_FOOTER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (229, NULL, NULL, 'UPGRADER_BACKUPDB_FILENAME', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (230, NULL, NULL, 'UPGRADER_BACKUPFILES_FILENAME', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (231, NULL, NULL, 'BLOCKREINSURANCE_NBBLOCKS', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (232, NULL, NULL, 'HOMESLIDER_WIDTH', '2100', '0000-00-00 00:00:00', '2016-10-05 12:43:34'),
  (233, NULL, NULL, 'HOMESLIDER_SPEED', '500', '0000-00-00 00:00:00', '2016-08-04 12:42:45'),
  (234, NULL, NULL, 'HOMESLIDER_PAUSE', '3000', '0000-00-00 00:00:00', '2016-08-04 12:42:45'),
  (235, NULL, NULL, 'HOMESLIDER_LOOP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (236, NULL, NULL, 'PS_BASE_DISTANCE_UNIT', 'm', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (237, NULL, NULL, 'PS_SHOP_DOMAIN', 'cockytest.zcom.cz', '0000-00-00 00:00:00', '2016-08-04 12:42:28'),
  (238, NULL, NULL, 'PS_SHOP_DOMAIN_SSL', 'cockytest.zcom.cz', '0000-00-00 00:00:00', '2016-08-04 12:42:28'),
  (239, NULL, NULL, 'PS_SHOP_NAME', 'Levné kontaktní čočky - OFTEX', '0000-00-00 00:00:00', '2016-08-04 12:42:28'),
  (240, NULL, NULL, 'PS_SHOP_EMAIL', 'info@prodej-cocek.cz', '0000-00-00 00:00:00', '2017-01-23 17:51:24'),
  (241, NULL, NULL, 'PS_MAIL_METHOD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (242, NULL, NULL, 'PS_SHOP_ACTIVITY', '5', '0000-00-00 00:00:00', '2016-08-04 12:42:28'),
  (243, NULL, NULL, 'PS_LOGO', 'levne-kontaktni-cocky-oftex-logo-1486729886.jpg', '0000-00-00 00:00:00', '2017-02-10 13:31:26'),
  (244, NULL, NULL, 'PS_FAVICON', 'favicon.ico', '0000-00-00 00:00:00', '2017-02-10 13:31:26'),
  (245, NULL, NULL, 'PS_STORES_ICON', 'levne-kontaktni-cocky-oftex-logo_stores-1474285735.gif', '0000-00-00 00:00:00', '2016-09-19 13:48:55'),
  (246, NULL, NULL, 'PS_ROOT_CATEGORY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (247, NULL, NULL, 'PS_HOME_CATEGORY', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (248, NULL, NULL, 'PS_CONFIGURATION_AGREMENT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (249, NULL, NULL, 'PS_MAIL_SERVER', 'smtp.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (250, NULL, NULL, 'PS_MAIL_USER', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (251, NULL, NULL, 'PS_MAIL_PASSWD', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (252, NULL, NULL, 'PS_MAIL_SMTP_ENCRYPTION', 'off', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (253, NULL, NULL, 'PS_MAIL_SMTP_PORT', '25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (254, NULL, NULL, 'PS_MAIL_COLOR', '#db3484', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (255, NULL, NULL, 'NW_SALT', 'HE0VKzVXjMfGF4BR', '0000-00-00 00:00:00', '2016-08-04 12:42:44'),
  (256, NULL, NULL, 'PS_PAYMENT_LOGO_CMS_ID', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (257, NULL, NULL, 'HOME_FEATURED_NBR', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (258, NULL, NULL, 'SEK_MIN_OCCURENCES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (259, NULL, NULL, 'SEK_FILTER_KW', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (260, NULL, NULL, 'PS_ALLOW_MOBILE_DEVICE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (261, NULL, NULL, 'PS_CUSTOMER_CREATION_EMAIL', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (262, NULL, NULL, 'PS_SMARTY_CONSOLE_KEY', 'SMARTY_DEBUG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (263, NULL, NULL, 'PS_DASHBOARD_USE_PUSH', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (264, NULL, NULL, 'PS_ATTRIBUTE_ANCHOR_SEPARATOR', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (265, NULL, NULL, 'CONF_AVERAGE_PRODUCT_MARGIN', '40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (266, NULL, NULL, 'PS_DASHBOARD_SIMULATION', '0', '0000-00-00 00:00:00', '2016-09-19 22:12:34'),
  (267, NULL, NULL, 'PS_QUICK_VIEW', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (268, NULL, NULL, 'PS_USE_HTMLPURIFIER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (269, NULL, NULL, 'PS_SMARTY_CACHING_TYPE', 'filesystem', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (270, NULL, NULL, 'PS_SMARTY_CLEAR_CACHE', 'everytime', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (271, NULL, NULL, 'PS_DETECT_LANG', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (272, NULL, NULL, 'PS_DETECT_COUNTRY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (273, NULL, NULL, 'PS_ROUND_TYPE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (274, NULL, NULL, 'PS_PRICE_DISPLAY_PRECISION', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (275, NULL, NULL, 'PS_LOG_EMAILS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (276, NULL, NULL, 'PS_CUSTOMER_NWSL', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (277, NULL, NULL, 'PS_CUSTOMER_OPTIN', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (278, NULL, NULL, 'PS_PACK_STOCK_TYPE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (279, NULL, NULL, 'PS_LOG_MODULE_PERFS_MODULO', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (280, NULL, NULL, 'PS_DISALLOW_HISTORY_REORDERING', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (281, NULL, NULL, 'PS_DISPLAY_PRODUCT_WEIGHT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (282, NULL, NULL, 'PS_PRODUCT_WEIGHT_PRECISION', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (283, NULL, NULL, 'PS_ADVANCED_PAYMENT_API', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
  (284, NULL, NULL, 'PS_SC_TWITTER', '1', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (285, NULL, NULL, 'PS_SC_FACEBOOK', '1', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (286, NULL, NULL, 'PS_SC_GOOGLE', '1', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (287, NULL, NULL, 'PS_SC_PINTEREST', '1', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (288, NULL, NULL, 'BLOCKBANNER_IMG', NULL, '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (289, NULL, NULL, 'BLOCKBANNER_LINK', NULL, '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (290, NULL, NULL, 'BLOCKBANNER_DESC', NULL, '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (291, NULL, NULL, 'CONF_BANKWIRE_FIXED', '0.2', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (292, NULL, NULL, 'CONF_BANKWIRE_VAR', '2', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (293, NULL, NULL, 'CONF_BANKWIRE_FIXED_FOREIGN', '0.2', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (294, NULL, NULL, 'CONF_BANKWIRE_VAR_FOREIGN', '2', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (295, NULL, NULL, 'PS_BLOCK_BESTSELLERS_TO_DISPLAY', '10', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (296, NULL, NULL, 'PS_BLOCK_CART_XSELL_LIMIT', '12', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (297, NULL, NULL, 'PS_BLOCK_CART_SHOW_CROSSSELLING', '1', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (298, NULL, NULL, 'BLOCKSOCIAL_YOUTUBE', NULL, '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (299, NULL, NULL, 'BLOCKSOCIAL_GOOGLE_PLUS', 'https://www.google.com/+prestashop', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (300, NULL, NULL, 'BLOCKSOCIAL_PINTEREST', NULL, '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (301, NULL, NULL, 'BLOCKSOCIAL_VIMEO', NULL, '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (302, NULL, NULL, 'BLOCKSOCIAL_INSTAGRAM', NULL, '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (303, NULL, NULL, 'BLOCK_CATEG_ROOT_CATEGORY', '1', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (304, NULL, NULL, 'blockfacebook_url', 'https://www.facebook.com/prestashop', '2016-08-04 12:42:40', '2016-08-04 12:42:40'),
  (305, NULL, NULL, 'PS_LAYERED_HIDE_0_VALUES', '1', '2016-08-04 12:42:41', '2016-08-04 12:42:41'),
  (306, NULL, NULL, 'PS_LAYERED_SHOW_QTIES', '1', '2016-08-04 12:42:41', '2016-08-04 12:42:41'),
  (307, NULL, NULL, 'PS_LAYERED_FULL_TREE', '1', '2016-08-04 12:42:41', '2016-08-04 12:42:41'),
  (308, NULL, NULL, 'PS_LAYERED_FILTER_PRICE_USETAX', '1', '2016-08-04 12:42:41', '2016-08-04 12:42:41'),
  (309, NULL, NULL, 'PS_LAYERED_FILTER_CATEGORY_DEPTH', '1', '2016-08-04 12:42:41', '2016-08-04 12:42:41'),
  (310, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_QTY', '0', '2016-08-04 12:42:41', '2016-08-04 12:42:41'),
  (311, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_CDT', '0', '2016-08-04 12:42:41', '2016-08-04 12:42:41'),
  (312, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_MNF', '0', '2016-08-04 12:42:41', '2016-08-04 12:42:41'),
  (313, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_CAT', '0', '2016-08-04 12:42:41', '2016-08-04 12:42:41'),
  (314, NULL, NULL, 'PS_LAYERED_FILTER_PRICE_ROUNDING', '1', '2016-08-04 12:42:41', '2016-08-04 12:42:41'),
  (315, NULL, NULL, 'PS_LAYERED_INDEXED', '1', '2016-08-04 12:42:42', '2016-08-04 12:42:42'),
  (316, NULL, NULL, 'FOOTER_PRICE-DROP', '1', '2016-08-04 12:42:43', '2016-08-04 12:42:43'),
  (317, NULL, NULL, 'FOOTER_NEW-PRODUCTS', '1', '2016-08-04 12:42:43', '2016-08-04 12:42:43'),
  (318, NULL, NULL, 'FOOTER_BEST-SALES', '1', '2016-08-04 12:42:43', '2016-08-04 12:42:43'),
  (319, NULL, NULL, 'FOOTER_CONTACT', '1', '2016-08-04 12:42:43', '2016-08-04 12:42:43'),
  (320, NULL, NULL, 'FOOTER_SITEMAP', '1', '2016-08-04 12:42:43', '2016-08-04 12:42:43'),
  (321, NULL, NULL, 'PS_NEWSLETTER_RAND', '12767369841359653260', '2016-08-04 12:42:44', '2016-08-04 12:42:44'),
  (322, NULL, NULL, 'BLOCKSPECIALS_NB_CACHES', '20', '2016-08-04 12:42:44', '2016-08-04 12:42:44'),
  (323, NULL, NULL, 'BLOCKSPECIALS_SPECIALS_NBR', '5', '2016-08-04 12:42:44', '2016-08-04 12:42:44'),
  (324, NULL, NULL, 'BLOCKTAGS_MAX_LEVEL', '3', '2016-08-04 12:42:44', '2016-08-04 12:42:44'),
  (325, NULL, NULL, 'CONF_CHEQUE_FIXED', '0.2', '2016-08-04 12:42:44', '2016-08-04 12:42:44'),
  (326, NULL, NULL, 'CONF_CHEQUE_VAR', '2', '2016-08-04 12:42:44', '2016-08-04 12:42:44'),
  (327, NULL, NULL, 'CONF_CHEQUE_FIXED_FOREIGN', '0.2', '2016-08-04 12:42:44', '2016-08-04 12:42:44'),
  (328, NULL, NULL, 'CONF_CHEQUE_VAR_FOREIGN', '2', '2016-08-04 12:42:44', '2016-08-04 12:42:44'),
  (329, NULL, NULL, 'DASHACTIVITY_CART_ACTIVE', '30', '2016-08-04 12:42:44', '2016-08-04 12:42:44'),
  (330, NULL, NULL, 'DASHACTIVITY_CART_ABANDONED_MIN', '24', '2016-08-04 12:42:44', '2016-08-04 12:42:44'),
  (331, NULL, NULL, 'DASHACTIVITY_CART_ABANDONED_MAX', '48', '2016-08-04 12:42:44', '2016-08-04 12:42:44'),
  (332, NULL, NULL, 'DASHACTIVITY_VISITOR_ONLINE', '30', '2016-08-04 12:42:44', '2016-08-04 12:42:44'),
  (333, NULL, NULL, 'PS_DASHGOALS_CURRENT_YEAR', '2016', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (334, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_LAST_ORDER', '10', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (335, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_BEST_SELLER', '10', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (336, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_MOST_VIEWED', '10', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (337, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_TOP_SEARCH', '10', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (338, NULL, NULL, 'HOME_FEATURED_CAT', '2', '2016-08-04 12:42:46', '2016-08-04 12:42:46'),
  (339, NULL, NULL, 'PRODUCTPAYMENTLOGOS_IMG', 'payment-logo.png', '2016-08-04 12:42:46', '2016-08-04 12:42:46'),
  (340, NULL, NULL, 'PRODUCTPAYMENTLOGOS_LINK', NULL, '2016-08-04 12:42:46', '2016-08-04 12:42:46'),
  (341, NULL, NULL, 'PRODUCTPAYMENTLOGOS_TITLE', NULL, '2016-08-04 12:42:46', '2016-08-04 12:42:46'),
  (342, NULL, NULL, 'PS_TC_THEMES', 'a:9:{i:0;s:6:"theme1";i:1;s:6:"theme2";i:2;s:6:"theme3";i:3;s:6:"theme4";i:4;s:6:"theme5";i:5;s:6:"theme6";i:6;s:6:"theme7";i:7;s:6:"theme8";i:8;s:6:"theme9";}', '2016-08-04 12:42:48', '2016-08-04 12:42:48'),
  (343, NULL, NULL, 'PS_TC_FONTS', 'a:10:{s:5:"font1";s:9:"Open Sans";s:5:"font2";s:12:"Josefin Slab";s:5:"font3";s:4:"Arvo";s:5:"font4";s:4:"Lato";s:5:"font5";s:7:"Volkorn";s:5:"font6";s:13:"Abril Fatface";s:5:"font7";s:6:"Ubuntu";s:5:"font8";s:7:"PT Sans";s:5:"font9";s:15:"Old Standard TT";s:6:"font10";s:10:"Droid Sans";}', '2016-08-04 12:42:48', '2016-08-04 12:42:48'),
  (344, NULL, NULL, 'PS_TC_THEME', NULL, '2016-08-04 12:42:48', '2016-08-04 12:42:48'),
  (345, NULL, NULL, 'PS_TC_FONT', NULL, '2016-08-04 12:42:48', '2016-08-04 12:42:48'),
  (346, NULL, NULL, 'PS_TC_ACTIVE', '0', '2016-08-04 12:42:48', '2016-10-05 11:13:37'),
  (347, NULL, NULL, 'PS_SET_DISPLAY_SUBCATEGORIES', '1', '2016-08-04 12:42:48', '2016-08-04 12:42:48'),
  (348, NULL, NULL, 'GF_INSTALL_CALC', '1', '2016-08-04 12:42:51', '2016-08-04 12:44:19'),
  (349, NULL, NULL, 'GF_CURRENT_LEVEL', '2', '2016-08-04 12:42:51', '2017-01-23 19:58:36'),
  (350, NULL, NULL, 'GF_CURRENT_LEVEL_PERCENT', '35', '2016-08-04 12:42:51', '2017-01-25 15:21:02'),
  (351, NULL, NULL, 'GF_NOTIFICATION', '0', '2016-08-04 12:42:51', '2017-02-10 10:26:14'),
  (352, NULL, NULL, 'CRONJOBS_ADMIN_DIR', '4b3b9c9945331f2a66073fdced8aff2d', '2016-08-04 12:42:51', '2016-08-04 12:44:06'),
  (353, NULL, NULL, 'CRONJOBS_MODE', 'webservice', '2016-08-04 12:42:51', '2016-08-04 12:42:51'),
  (354, NULL, NULL, 'CRONJOBS_MODULE_VERSION', '1.3.4', '2016-08-04 12:42:51', '2016-09-20 15:02:30'),
  (355, NULL, NULL, 'CRONJOBS_WEBSERVICE_ID', '2030487', '2016-08-04 12:42:51', '2016-08-04 12:42:52'),
  (356, NULL, NULL, 'CRONJOBS_EXECUTION_TOKEN', 'c6b470977aa7e00dda4cc3d01034c2b1', '2016-08-04 12:42:51', '2016-08-04 12:42:51'),
  (357, NULL, NULL, 'PS_ONBOARDING_CURRENT_STEP', '0', '2016-08-04 12:42:52', '2016-08-04 12:42:52'),
  (358, NULL, NULL, 'PS_ONBOARDING_LAST_VALIDATE_STEP', '0', '2016-08-04 12:42:52', '2016-08-04 12:42:52'),
  (359, NULL, NULL, 'PS_ONBOARDING_STEP_1_COMPLETED', '0', '2016-08-04 12:42:52', '2016-08-04 12:42:52'),
  (360, NULL, NULL, 'PS_ONBOARDING_STEP_2_COMPLETED', '0', '2016-08-04 12:42:52', '2016-08-04 12:42:52'),
  (361, NULL, NULL, 'PS_ONBOARDING_STEP_3_COMPLETED', '0', '2016-08-04 12:42:52', '2016-08-04 12:42:52'),
  (362, NULL, NULL, 'PS_ONBOARDING_STEP_4_COMPLETED', '0', '2016-08-04 12:42:52', '2016-08-04 12:42:52'),
  (363, NULL, NULL, 'GF_NOT_VIEWED_BADGE', '178', '2016-08-04 12:44:20', '2017-01-25 15:21:02'),
  (364, NULL, NULL, 'smartpostperpage', '5', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (365, NULL, NULL, 'smartshowauthorstyle', '1', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (366, NULL, NULL, 'smartmainblogurl', 'smartblog', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (367, NULL, NULL, 'smartusehtml', '1', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (368, NULL, NULL, 'smartenablecomment', '1', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (369, NULL, NULL, 'smartcaptchaoption', '1', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (370, NULL, NULL, 'smartshowviewed', '1', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (371, NULL, NULL, 'smartshownoimg', '1', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (372, NULL, NULL, 'smartshowcolumn', '3', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (373, NULL, NULL, 'smartacceptcomment', '1', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (374, NULL, NULL, 'smartcustomcss', NULL, '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (375, NULL, NULL, 'smartdisablecatimg', '1', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (376, NULL, NULL, 'smartblogmetatitle', 'Smart Bolg Title', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (377, NULL, NULL, 'smartblogmetakeyword', 'smart,blog,smartblog,prestashop blog,prestashop,blog', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (378, NULL, NULL, 'smartblogmetadescrip', 'Prestashop powerfull blog site developing module. It has hundrade of extra plugins. This module developed by SmartDataSoft.com', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (379, NULL, NULL, 'smartblog_quick_access', '4', '2016-09-19 13:46:17', '2016-09-19 13:46:17'),
  (380, NULL, NULL, 'smart_update_period', 'hourly', '2016-09-19 13:46:20', '2016-09-19 13:46:20'),
  (381, NULL, NULL, 'smart_update_frequency', '1', '2016-09-19 13:46:20', '2016-09-19 13:46:20'),
  (382, NULL, NULL, 'smartshowhomepost', '4', '2016-09-19 13:46:21', '2016-09-19 13:46:21'),
  (383, NULL, NULL, 'smartshowhomecomments', '5', '2016-09-19 13:46:22', '2016-09-19 13:46:22'),
  (384, NULL, NULL, 'smartshowpopularpost', '5', '2016-09-19 13:46:23', '2016-09-19 13:46:23'),
  (385, NULL, NULL, 'smartshowrecentpost', '5', '2016-09-19 13:46:24', '2016-09-19 13:46:24'),
  (386, NULL, NULL, 'smartshowrelatedpost', '5', '2016-09-19 13:46:24', '2016-09-19 13:46:24'),
  (387, NULL, NULL, 'smartshowposttag', '5', '2016-09-19 13:46:25', '2016-09-19 13:46:25'),
  (388, NULL, NULL, 'TMOLARKCHAT_ID', NULL, '2016-09-19 13:46:26', '2016-09-19 13:46:26'),
  (389, NULL, NULL, 'TMPV_YT_CONTROLS', '1', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (390, NULL, NULL, 'TMPV_YT_AUTOHIDE', '0', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (391, NULL, NULL, 'TMPV_YT_FS', '1', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (392, NULL, NULL, 'TMPV_YT_INFO', '1', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (393, NULL, NULL, 'TMPV_YT_THEME', '0', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (394, NULL, NULL, 'TMPV_V_AUTOPLAY', '0', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (395, NULL, NULL, 'TMPV_V_AUTOPAUSE', '0', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (396, NULL, NULL, 'TMPV_V_BADGE', '1', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (397, NULL, NULL, 'TMPV_V_BYLINE', '1', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (398, NULL, NULL, 'TMPV_V_LOOP', '0', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (399, NULL, NULL, 'TMPV_V_PORTRAIT', '0', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (400, NULL, NULL, 'TMPV_V_TITLE', '1', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (401, NULL, NULL, 'TMPV_CV_CONTROLS', '1', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (402, NULL, NULL, 'TMPV_CV_PRELOAD', '0', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (403, NULL, NULL, 'TMPV_CV_AUTOPLAY', '0', '2016-09-19 13:46:28', '2016-09-19 13:46:28'),
  (404, NULL, NULL, 'TMPV_CV_LOOP', '1', '2016-09-19 13:46:29', '2016-09-19 13:46:29'),
  (405, NULL, NULL, 'RELATED_DISPLAY_PRICE', '1', '2016-09-19 13:46:29', '2017-01-23 16:39:16'),
  (406, NULL, NULL, 'RELATED_NBR', '4', '2016-09-19 13:46:29', '2017-01-23 16:38:40'),
  (407, NULL, NULL, 'PS_SEARCH_AJAX_IMAGE', '1', '2016-09-19 13:46:33', '2016-09-19 13:46:33'),
  (408, NULL, NULL, 'PS_SEARCH_AJAX_DESCRIPTION', '1', '2016-09-19 13:46:33', '2016-09-19 13:46:33'),
  (409, NULL, NULL, 'PS_SEARCH_AJAX_PRICE', '1', '2016-09-19 13:46:33', '2016-09-19 13:46:33'),
  (410, NULL, NULL, 'PS_SEARCH_AJAX_REFERENCE', '1', '2016-09-19 13:46:33', '2016-09-19 13:46:33'),
  (411, NULL, NULL, 'PS_SEARCH_AJAX_MANUFACTURER', '1', '2016-09-19 13:46:33', '2016-09-19 13:46:33'),
  (412, NULL, NULL, 'PS_SEARCH_AJAX_LIMIT', '1', '2016-09-19 13:46:33', '2016-09-19 13:46:33'),
  (413, NULL, NULL, 'PS_SEARCH_AJAX_LIMIT_NUM', '3', '2016-09-19 13:46:33', '2016-09-19 13:46:33'),
  (414, NULL, NULL, 'PS_SEARCH_AJAX_SCROLL_HEIGHT', '180', '2016-09-19 13:46:33', '2016-09-19 13:46:33'),
  (415, NULL, NULL, 'TMHOMECOAROUSEL_ITEM_NB', '6', '2016-09-19 13:46:34', '2017-01-23 16:41:40'),
  (416, NULL, NULL, 'TMHOMECOAROUSEL_ITEM_WIDTH', '290', '2016-09-19 13:46:34', '2016-09-19 13:46:34'),
  (417, NULL, NULL, 'TMHOMECOAROUSEL_ITEM_MARGIN', '30', '2016-09-19 13:46:34', '2016-09-19 13:46:34'),
  (418, NULL, NULL, 'TMHOMECOAROUSEL_AUTO', '1', '2016-09-19 13:46:34', '2016-09-19 13:46:34'),
  (419, NULL, NULL, 'TMHOMECOAROUSEL_ITEM_SCROLL', '6', '2016-09-19 13:46:34', '2017-01-23 16:43:18'),
  (420, NULL, NULL, 'TMHOMECOAROUSEL_SPEED', '500', '2016-09-19 13:46:34', '2016-09-19 13:46:34'),
  (421, NULL, NULL, 'TMHOMECOAROUSEL_AUTO_PAUSE', '3000', '2016-09-19 13:46:34', '2016-09-19 13:46:34'),
  (422, NULL, NULL, 'TMHOMECOAROUSEL_LOOP', '1', '2016-09-19 13:46:34', '2016-09-19 13:46:34'),
  (423, NULL, NULL, 'TMHOMECOAROUSEL_HIDE_CONTROL', '1', '2016-09-19 13:46:34', '2016-09-19 13:46:34'),
  (424, NULL, NULL, 'TMHOMECOAROUSEL_CONTROL', '1', '2016-09-19 13:46:34', '2016-09-19 13:46:34'),
  (425, NULL, NULL, 'TMHOMECOAROUSEL_AUTO_HOVER', '1', '2016-09-19 13:46:34', '2016-09-19 13:46:34'),
  (426, NULL, NULL, 'TM_CPS_CAROUSEL_NB', '4', '2016-09-19 13:46:36', '2016-09-19 13:46:36'),
  (427, NULL, NULL, 'TM_CPS_CAROUSEL_SLIDE_WIDTH', '180', '2016-09-19 13:46:36', '2016-09-19 13:46:36'),
  (428, NULL, NULL, 'TM_CPS_CAROUSEL_SLIDE_MARGIN', '20', '2016-09-19 13:46:36', '2016-09-19 13:46:36'),
  (429, NULL, NULL, 'TM_CPS_CAROUSEL_ITEM_SCROLL', '1', '2016-09-19 13:46:36', '2016-09-19 13:46:36'),
  (430, NULL, NULL, 'TM_CPS_CAROUSEL_SPEED', '500', '2016-09-19 13:46:36', '2016-09-19 13:46:36'),
  (431, NULL, NULL, 'TM_CPS_CAROUSEL_AUTO_PAUSE', '3000', '2016-09-19 13:46:36', '2016-09-19 13:46:36'),
  (432, NULL, NULL, 'TM_CPS_CAROUSEL_LOOP', '1', '2016-09-19 13:46:36', '2016-09-19 13:46:36'),
  (433, NULL, NULL, 'TM_CPS_CAROUSEL_HIDE_CONTROL', '1', '2016-09-19 13:46:36', '2016-09-19 13:46:36'),
  (434, NULL, NULL, 'TM_CPS_CAROUSEL_AUTO_HOVER', '1', '2016-09-19 13:46:36', '2016-09-19 13:46:36'),
  (435, NULL, NULL, 'TMGOOGLE_STYLE', 'subtle_grayscale', '2016-09-19 13:46:38', '2017-01-23 17:26:56'),
  (436, NULL, NULL, 'TMGOOGLE_TYPE', 'roadmap', '2016-09-19 13:46:38', '2016-09-19 13:46:38'),
  (437, NULL, NULL, 'TMGOOGLE_ZOOM', '8', '2016-09-19 13:46:38', '2016-09-19 13:46:38'),
  (438, NULL, NULL, 'TMGOOGLE_SCROLL', '0', '2016-09-19 13:46:38', '2016-09-19 13:46:38'),
  (439, NULL, NULL, 'TMGOOGLE_TYPE_CONTROL', '1', '2016-09-19 13:46:38', '2017-01-23 17:49:55'),
  (440, NULL, NULL, 'TMGOOGLE_STREET_VIEW', '1', '2016-09-19 13:46:38', '2017-01-23 17:49:55'),
  (441, NULL, NULL, 'TMTWITTERFEED_ID', NULL, '2016-09-19 13:46:38', '2016-09-19 13:46:38'),
  (442, NULL, NULL, 'TMFACEBOOK_ID', NULL, '2016-09-19 13:46:38', '2016-09-19 13:46:38'),
  (443, NULL, NULL, 'TMPINTEREST_ID', NULL, '2016-09-19 13:46:38', '2016-09-19 13:46:38'),
  (444, NULL, NULL, 'TMINSTAGRAM_ID', NULL, '2016-09-19 13:46:38', '2016-09-19 13:46:38'),
  (445, NULL, NULL, 'TMINSTAGRAM_USERNAME', NULL, '2016-09-19 13:46:38', '2016-09-19 13:46:38'),
  (446, NULL, NULL, 'TMINSTAGRAM_ACCESSTOKEN', NULL, '2016-09-19 13:46:38', '2016-09-19 13:46:38'),
  (447, NULL, NULL, 'TMINSTAGRAM_USERID', NULL, '2016-09-19 13:46:38', '2016-09-19 13:46:38'),
  (448, NULL, NULL, 'TMINSTAGRAM_TYPE', NULL, '2016-09-19 13:46:38', '2016-09-19 13:46:38'),
  (449, NULL, NULL, 'TMINSTAGRAM_TAG', NULL, '2016-09-19 13:46:38', '2016-09-19 13:46:38'),
  (450, NULL, NULL, 'CROSSSELLING_DISPLAY_PRICE', '0', '2016-09-19 13:46:43', '2016-09-19 13:46:43'),
  (451, NULL, NULL, 'CROSSSELLING_NBR', '10', '2016-09-19 13:46:43', '2016-09-19 13:46:43'),
  (452, NULL, NULL, 'PRODUCTSCATEGORY_DISPLAY_PRICE', '0', '2016-09-19 13:46:43', '2016-09-19 13:46:43'),
  (453, NULL, NULL, 'PS_IMAGE_GENERATION_METHOD', '0', '2016-09-20 14:59:48', '2016-09-20 14:59:48'),
  (454, NULL, NULL, 'PS_HIGHT_DPI', '0', '2016-09-20 14:59:48', '2016-09-20 14:59:48'),
  (455, NULL, NULL, 'PS_SHOW_CAT_MODULES_1', NULL, '2016-09-20 15:02:48', '2017-02-22 12:51:51'),
  (456, NULL, NULL, 'TMHOMECOAROUSEL_STATUS', '1', '2016-09-20 15:21:36', '2016-09-20 15:21:36'),
  (457, NULL, NULL, 'TMHOMECOAROUSEL_RANDOM', '0', '2016-09-20 15:21:36', '2016-09-20 15:21:36'),
  (458, NULL, NULL, 'TMHOMECOAROUSEL_PAGER', '0', '2016-09-20 15:21:36', '2016-09-20 15:21:36'),
  (459, NULL, NULL, 'TMHOMECOAROUSEL_AUTO_CONTROL', '0', '2016-09-20 15:21:36', '2016-09-20 15:21:36'),
  (460, NULL, NULL, 'PS_CSS_THEME_CACHE', '0', '2016-09-20 15:36:28', '2016-09-20 15:36:28'),
  (461, NULL, NULL, 'PS_JS_THEME_CACHE', '0', '2016-09-20 15:36:28', '2016-09-20 15:36:28'),
  (462, NULL, NULL, 'PS_HTML_THEME_COMPRESSION', '0', '2016-09-20 15:36:28', '2016-09-20 15:36:28'),
  (463, NULL, NULL, 'PS_JS_HTML_THEME_COMPRESSION', '0', '2016-09-20 15:36:28', '2016-09-20 15:36:28'),
  (464, NULL, NULL, 'PS_JS_DEFER', '0', '2016-09-20 15:36:28', '2016-09-20 15:36:28'),
  (465, NULL, NULL, 'PS_HTACCESS_CACHE_CONTROL', '0', '2016-09-20 15:36:28', '2016-09-20 15:36:28'),
  (466, NULL, NULL, 'PS_DISABLE_NON_NATIVE_MODULE', '0', '2016-09-20 15:36:28', '2016-09-20 15:36:28'),
  (467, NULL, NULL, 'PS_DISABLE_OVERRIDES', '0', '2016-09-20 15:36:28', '2016-09-20 15:36:28'),
  (468, NULL, NULL, 'PS_GRID_PRODUCT', '0', '2016-10-05 11:13:38', '2016-10-05 11:13:38'),
  (469, NULL, NULL, 'GSITEMAP_PRIORITY_HOME', '1', '2016-10-05 11:22:09', '2016-10-05 11:22:09'),
  (470, NULL, NULL, 'GSITEMAP_PRIORITY_PRODUCT', '0.9', '2016-10-05 11:22:09', '2016-10-05 11:22:09'),
  (471, NULL, NULL, 'GSITEMAP_PRIORITY_CATEGORY', '0.8', '2016-10-05 11:22:09', '2016-10-05 11:22:09'),
  (472, NULL, NULL, 'GSITEMAP_PRIORITY_MANUFACTURER', '0.7', '2016-10-05 11:22:09', '2016-10-05 11:22:09'),
  (473, NULL, NULL, 'GSITEMAP_PRIORITY_SUPPLIER', '0.6', '2016-10-05 11:22:09', '2016-10-05 11:22:09'),
  (474, NULL, NULL, 'GSITEMAP_PRIORITY_CMS', '0.5', '2016-10-05 11:22:09', '2016-10-05 11:22:09'),
  (475, NULL, NULL, 'GSITEMAP_FREQUENCY', 'weekly', '2016-10-05 11:22:09', '2016-10-05 11:22:09'),
  (476, NULL, NULL, 'PS_LOYALTY_POINT_VALUE', '0.20', '2016-10-05 11:23:07', '2016-10-05 11:23:07'),
  (477, NULL, NULL, 'PS_LOYALTY_MINIMAL', '0', '2016-10-05 11:23:07', '2016-10-05 11:23:07');
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`)
VALUES
  (478, NULL, NULL, 'PS_LOYALTY_POINT_RATE', '10', '2016-10-05 11:23:07', '2016-10-05 11:23:07'),
  (479, NULL, NULL, 'PS_LOYALTY_NONE_AWARD', '1', '2016-10-05 11:23:07', '2016-10-05 11:23:07'),
  (480, NULL, NULL, 'PS_LOYALTY_TAX', '0', '2016-10-05 11:23:07', '2016-10-05 11:23:07'),
  (481, NULL, NULL, 'PS_LOYALTY_VALIDITY_PERIOD', '0', '2016-10-05 11:23:07', '2016-10-05 11:23:07'),
  (482, NULL, NULL, 'PS_LOYALTY_VOUCHER_DETAILS', NULL, '2016-10-05 11:23:07', '2016-10-05 11:23:07'),
  (483, NULL, NULL, 'PS_LOYALTY_VOUCHER_CATEGORY', '2,3,4,5,6,7,8,9,10,11,12', '2016-10-05 11:23:07', '2016-10-05 11:23:07'),
  (484, NULL, NULL, 'MA_MERCHANT_ORDER', '1', '2016-10-05 11:27:10', '2016-10-05 11:27:10'),
  (485, NULL, NULL, 'MA_MERCHANT_OOS', '1', '2016-10-05 11:27:10', '2016-10-05 11:27:10'),
  (486, NULL, NULL, 'MA_CUSTOMER_QTY', '1', '2016-10-05 11:27:10', '2016-10-05 11:27:10'),
  (487, NULL, NULL, 'MA_ORDER_EDIT', '1', '2016-10-05 11:27:10', '2016-10-05 11:27:10'),
  (488, NULL, NULL, 'MA_RETURN_SLIP', '1', '2016-10-05 11:27:10', '2016-10-05 11:27:10'),
  (489, NULL, NULL, 'MA_MERCHANT_MAILS', 'stoupa.bat@seznam.cz', '2016-10-05 11:27:10', '2016-10-05 11:27:10'),
  (490, NULL, NULL, 'MA_LAST_QTIES', '3', '2016-10-05 11:27:10', '2016-10-05 11:27:10'),
  (491, NULL, NULL, 'MA_MERCHANT_COVERAGE', '0', '2016-10-05 11:27:10', '2016-10-05 11:27:10'),
  (492, NULL, NULL, 'MA_PRODUCT_COVERAGE', '0', '2016-10-05 11:27:10', '2016-10-05 11:27:10'),
  (493, NULL, NULL, 'GA_ACCOUNT_ID', 'UA-2216773-9', '2016-10-05 11:31:41', '2016-10-05 11:31:41'),
  (494, NULL, NULL, 'GANALYTICS_CONFIGURATION_OK', '1', '2016-10-05 11:31:41', '2016-10-05 11:31:41'),
  (495, NULL, NULL, 'PS_LOGO_INVOICE', 'levne-kontaktni-cocky-oftex-logo_invoice-1485183707.jpg', '2017-01-23 16:01:47', '2017-01-23 16:01:47'),
  (496, NULL, NULL, 'FOOTER_CMS_TEXT_1', NULL, '2017-01-23 16:10:19', '2017-01-23 19:43:56'),
  (497, NULL, NULL, 'PS_REGISTRATION_PROCESS_TYPE', '0', '2017-01-23 16:26:14', '2017-01-23 16:26:14'),
  (498, NULL, NULL, 'PS_CART_FOLLOWING', '1', '2017-01-23 16:26:14', '2017-01-23 16:26:14'),
  (499, NULL, NULL, 'PS_B2B_ENABLE', '0', '2017-01-23 16:26:14', '2017-01-23 16:26:14'),
  (500, NULL, NULL, 'PS_STORES_DISPLAY_SITEMAP', '1', '2017-01-23 17:51:24', '2017-01-23 17:51:24'),
  (501, NULL, NULL, 'PS_SHOP_DETAILS', NULL, '2017-01-23 17:51:24', '2017-01-23 17:51:24'),
  (502, NULL, NULL, 'PS_SHOP_ADDR1', NULL, '2017-01-23 17:51:24', '2017-01-23 17:51:24'),
  (503, NULL, NULL, 'PS_SHOP_ADDR2', NULL, '2017-01-23 17:51:24', '2017-01-23 17:51:24'),
  (504, NULL, NULL, 'PS_SHOP_CODE', NULL, '2017-01-23 17:51:24', '2017-01-23 17:51:24'),
  (505, NULL, NULL, 'PS_SHOP_CITY', NULL, '2017-01-23 17:51:24', '2017-01-23 17:51:24'),
  (506, NULL, NULL, 'PS_SHOP_COUNTRY_ID', '16', '2017-01-23 17:51:24', '2017-01-23 17:51:24'),
  (507, NULL, NULL, 'PS_SHOP_COUNTRY', 'Czech Republic', '2017-01-23 17:51:24', '2017-01-23 17:51:24'),
  (508, NULL, NULL, 'PS_SHOP_PHONE', NULL, '2017-01-23 17:51:24', '2017-01-23 17:51:24'),
  (509, NULL, NULL, 'PS_SHOP_FAX', NULL, '2017-01-23 17:51:24', '2017-01-23 17:51:24'),
  (510, NULL, NULL, 'PS_SHOW_CAT_MODULES_2', NULL, '2017-01-24 20:12:41', '2017-01-25 14:22:42');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_configuration_kpi`
--

CREATE TABLE IF NOT EXISTS `ps_configuration_kpi` (
  `id_configuration_kpi` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop_group`        INT(11) UNSIGNED          DEFAULT NULL,
  `id_shop`              INT(11) UNSIGNED          DEFAULT NULL,
  `name`                 VARCHAR(64)      NOT NULL,
  `value`                TEXT,
  `date_add`             DATETIME         NOT NULL,
  `date_upd`             DATETIME         NOT NULL,
  PRIMARY KEY (`id_configuration_kpi`),
  KEY `name` (`name`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 69;

--
-- Vypisuji data pro tabulku `ps_configuration_kpi`
--

INSERT INTO `ps_configuration_kpi` (`id_configuration_kpi`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`)
VALUES
  (1, NULL, NULL, 'DASHGOALS_TRAFFIC_01_2016', '600', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (2, NULL, NULL, 'DASHGOALS_CONVERSION_01_2016', '2', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (3, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_01_2016', '80', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (4, NULL, NULL, 'DASHGOALS_TRAFFIC_02_2016', '600', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (5, NULL, NULL, 'DASHGOALS_CONVERSION_02_2016', '2', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (6, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_02_2016', '80', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (7, NULL, NULL, 'DASHGOALS_TRAFFIC_03_2016', '600', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (8, NULL, NULL, 'DASHGOALS_CONVERSION_03_2016', '2', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (9, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_03_2016', '80', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (10, NULL, NULL, 'DASHGOALS_TRAFFIC_04_2016', '600', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (11, NULL, NULL, 'DASHGOALS_CONVERSION_04_2016', '2', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (12, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_04_2016', '80', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (13, NULL, NULL, 'DASHGOALS_TRAFFIC_05_2016', '600', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (14, NULL, NULL, 'DASHGOALS_CONVERSION_05_2016', '2', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (15, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_05_2016', '80', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (16, NULL, NULL, 'DASHGOALS_TRAFFIC_06_2016', '600', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (17, NULL, NULL, 'DASHGOALS_CONVERSION_06_2016', '2', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (18, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_06_2016', '80', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (19, NULL, NULL, 'DASHGOALS_TRAFFIC_07_2016', '600', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (20, NULL, NULL, 'DASHGOALS_CONVERSION_07_2016', '2', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (21, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_07_2016', '80', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (22, NULL, NULL, 'DASHGOALS_TRAFFIC_08_2016', '600', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (23, NULL, NULL, 'DASHGOALS_CONVERSION_08_2016', '2', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (24, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_08_2016', '80', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (25, NULL, NULL, 'DASHGOALS_TRAFFIC_09_2016', '600', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (26, NULL, NULL, 'DASHGOALS_CONVERSION_09_2016', '2', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (27, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_09_2016', '80', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (28, NULL, NULL, 'DASHGOALS_TRAFFIC_10_2016', '600', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (29, NULL, NULL, 'DASHGOALS_CONVERSION_10_2016', '2', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (30, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_10_2016', '80', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (31, NULL, NULL, 'DASHGOALS_TRAFFIC_11_2016', '600', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (32, NULL, NULL, 'DASHGOALS_CONVERSION_11_2016', '2', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (33, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_11_2016', '80', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (34, NULL, NULL, 'DASHGOALS_TRAFFIC_12_2016', '600', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (35, NULL, NULL, 'DASHGOALS_CONVERSION_12_2016', '2', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (36, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_12_2016', '80', '2016-08-04 12:42:45', '2016-08-04 12:42:45'),
  (37, NULL, NULL, 'DISABLED_PRODUCTS', '0%', '2016-09-19 13:58:31', '2016-09-19 13:58:31'),
  (38, NULL, NULL, 'DISABLED_PRODUCTS_EXPIRE', '1487686214', '2016-09-19 13:58:31', '2017-02-21 13:10:14'),
  (39, NULL, NULL, '8020_SALES_CATALOG', '0% z vašeho katalogu', '2016-09-19 13:58:31', '2016-09-19 13:58:31'),
  (40, NULL, NULL, '8020_SALES_CATALOG_EXPIRE', '1487722214', '2016-09-19 13:58:31', '2017-02-21 13:10:14'),
  (41, NULL, NULL, 'PERCENT_PRODUCT_OUT_OF_STOCK', '15.38%', '2016-09-19 13:58:31', '2017-02-10 10:26:01'),
  (42, NULL, NULL, 'PERCENT_PRODUCT_OUT_OF_STOCK_EXPIRE', '1487693414', '2016-09-19 13:58:31', '2017-02-21 13:10:14'),
  (43, NULL, NULL, 'PRODUCT_AVG_GROSS_MARGIN', '70%', '2016-09-19 13:58:31', '2016-09-19 13:58:31'),
  (44, NULL, NULL, 'PRODUCT_AVG_GROSS_MARGIN_EXPIRE', '1487700614', '2016-09-19 13:58:31', '2017-02-21 13:10:14'),
  (45, NULL, NULL, 'DISABLED_CATEGORIES', '0', '2016-09-19 13:58:43', '2017-01-24 20:11:24'),
  (46, NULL, NULL, 'EMPTY_CATEGORIES', '15', '2016-09-19 13:58:43', '2017-02-10 15:39:37'),
  (47, NULL, NULL, 'DISABLED_CATEGORIES_EXPIRE', '1487686026', '2016-09-19 13:58:43', '2017-02-21 13:07:06'),
  (48, NULL, NULL, 'EMPTY_CATEGORIES_EXPIRE', '1487686026', '2016-09-19 13:58:43', '2017-02-21 13:07:06'),
  (49, NULL, NULL, 'TOP_CATEGORY', NULL, '2016-09-19 13:58:43', '2016-09-19 13:58:43'),
  (50, NULL, NULL, 'TOP_CATEGORY_EXPIRE', NULL, '2016-09-19 13:58:43', '2016-09-19 13:58:43'),
  (51, NULL, NULL, 'PRODUCTS_PER_CATEGORY', '0', '2016-09-19 13:58:43', '2017-01-23 19:32:29'),
  (52, NULL, NULL, 'PRODUCTS_PER_CATEGORY_EXPIRE', '1487686429', '2016-09-19 13:58:43', '2017-02-21 14:13:49'),
  (53, NULL, NULL, 'UPDATE_MODULES', '0', '2016-09-20 15:01:58', '2017-02-10 10:48:15'),
  (54, NULL, NULL, 'INSTALLED_MODULES', '96', '2016-09-20 15:02:07', '2016-10-05 12:21:06'),
  (55, NULL, NULL, 'INSTALLED_MODULES_EXPIRE', '1487757363', '2016-09-20 15:02:07', '2017-02-22 10:54:03'),
  (56, NULL, NULL, 'DISABLED_MODULES', '9', '2016-09-20 15:02:07', '2016-10-05 12:21:07'),
  (57, NULL, NULL, 'DISABLED_MODULES_EXPIRE', '1487757363', '2016-09-20 15:02:07', '2017-02-22 10:54:03'),
  (58, NULL, NULL, 'UPDATE_MODULES_EXPIRE', '1487757394', '2016-09-20 15:02:08', '2017-02-22 10:54:34'),
  (59, NULL, NULL, 'ENABLED_LANGUAGES', '1', '2016-10-05 13:00:49', '2016-10-05 13:00:49'),
  (60, NULL, NULL, 'FRONTOFFICE_TRANSLATIONS', '98.5%', '2016-10-05 13:00:49', '2016-10-05 13:15:58'),
  (61, NULL, NULL, 'MAIN_COUNTRY', NULL, '2016-10-05 13:00:49', '2016-10-05 13:00:49'),
  (62, NULL, NULL, 'ENABLED_LANGUAGES_EXPIRE', '1487775548', '2016-10-05 13:00:50', '2017-02-22 15:58:08'),
  (63, NULL, NULL, 'FRONTOFFICE_TRANSLATIONS_EXPIRE', '1487775608', '2016-10-05 13:00:50', '2017-02-22 15:58:08'),
  (64, NULL, NULL, 'MAIN_COUNTRY_EXPIRE', NULL, '2016-10-05 13:00:51', '2016-10-05 13:00:51'),
  (65, NULL, NULL, 'TRANSLATE_TOTAL_THEME1353_CS', '135', '2016-10-05 13:07:45', '2017-01-24 20:52:00'),
  (66, NULL, NULL, 'TRANSLATE_DONE_THEME1353_CS', '135', '2016-10-05 13:07:45', '2017-01-24 21:00:39'),
  (67, NULL, NULL, 'TRANSLATE_TOTAL_DEFAULT-BOOTSTRA', '889', '2016-10-05 13:15:47', '2016-10-05 13:15:47'),
  (68, NULL, NULL, 'TRANSLATE_DONE_DEFAULT-BOOTSTRA', '876', '2016-10-05 13:15:48', '2016-10-05 13:15:48');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_configuration_kpi_lang`
--

CREATE TABLE IF NOT EXISTS `ps_configuration_kpi_lang` (
  `id_configuration_kpi` INT(10) UNSIGNED NOT NULL,
  `id_lang`              INT(10) UNSIGNED NOT NULL,
  `value`                TEXT,
  `date_upd`             DATETIME DEFAULT NULL,
  PRIMARY KEY (`id_configuration_kpi`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_configuration_kpi_lang`
--

INSERT INTO `ps_configuration_kpi_lang` (`id_configuration_kpi`, `id_lang`, `value`, `date_upd`) VALUES
  (49, 1, 'Oční čočky', '2017-02-21 13:07:06'),
  (50, 1, '1487765226', '2017-02-21 13:07:06'),
  (61, 1, 'Žádné objednávky', '2016-10-05 13:00:50'),
  (64, 1, '1487861888', '2017-02-22 15:58:08');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_configuration_lang`
--

CREATE TABLE IF NOT EXISTS `ps_configuration_lang` (
  `id_configuration` INT(10) UNSIGNED NOT NULL,
  `id_lang`          INT(10) UNSIGNED NOT NULL,
  `value`            TEXT,
  `date_upd`         DATETIME DEFAULT NULL,
  PRIMARY KEY (`id_configuration`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_configuration_lang`
--

INSERT INTO `ps_configuration_lang` (`id_configuration`, `id_lang`, `value`, `date_upd`) VALUES
  (41, 1, '#FA', NULL),
  (44, 1, '#DE', NULL),
  (46, 1, '#RE', NULL),
  (52, 1,
   'a|about|above|after|again|against|all|am|an|and|any|are|aren|as|at|be|because|been|before|being|below|between|both|but|by|can|cannot|could|couldn|did|didn|do|does|doesn|doing|don|down|during|each|few|for|from|further|had|hadn|has|hasn|have|haven|having|he|ll|her|here|hers|herself|him|himself|his|how|ve|if|in|into|is|isn|it|its|itself|let|me|more|most|mustn|my|myself|no|nor|not|of|off|on|once|only|or|other|ought|our|ours|ourselves|out|over|own|same|shan|she|should|shouldn|so|some|such|than|that|the|their|theirs|them|themselves|then|there|these|they|re|this|those|through|to|too|under|until|up|very|was|wasn|we|were|weren|what|when|where|which|while|who|whom|why|with|won|would|wouldn|you|your|yours|yourself|yourselves',
   NULL),
  (74, 1, '0', NULL),
  (80, 1, 'Dear Customer,\r\n\r\nRegards,\r\nCustomer service', NULL),
  (288, 1, 'sale70.png', '2016-08-04 12:42:40'),
  (289, 1, '', '2016-08-04 12:42:40'),
  (290, 1, '', '2016-08-04 12:42:40'),
  (482, 1, 'Loyalty reward', '2016-10-05 11:23:07');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_connections`
--

CREATE TABLE IF NOT EXISTS `ps_connections` (
  `id_connections` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop_group`  INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop`        INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_guest`       INT(10) UNSIGNED NOT NULL,
  `id_page`        INT(10) UNSIGNED NOT NULL,
  `ip_address`     BIGINT(20)                DEFAULT NULL,
  `date_add`       DATETIME         NOT NULL,
  `http_referer`   VARCHAR(255)              DEFAULT NULL,
  PRIMARY KEY (`id_connections`),
  KEY `id_guest` (`id_guest`),
  KEY `date_add` (`date_add`),
  KEY `id_page` (`id_page`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 270;

--
-- Vypisuji data pro tabulku `ps_connections`
--

INSERT INTO `ps_connections` (`id_connections`, `id_shop_group`, `id_shop`, `id_guest`, `id_page`, `ip_address`, `date_add`, `http_referer`)
VALUES
  (1, 1, 1, 1, 1, 2130706433, '2016-08-04 12:42:32', 'http://www.prestashop.com'),
  (2, 1, 1, 2, 1, 1572749186, '2016-08-04 12:43:24', ''),
  (3, 1, 1, 3, 1, 1542483275, '2016-08-05 01:31:22', ''),
  (4, 1, 1, 4, 1, 1382287998, '2016-08-05 09:17:08', ''),
  (5, 1, 1, 5, 1, 1542483306, '2016-08-06 01:32:55', ''),
  (6, 1, 1, 6, 1, 1542483301, '2016-08-07 01:33:15', ''),
  (7, 1, 1, 7, 1, 1542483288, '2016-08-08 01:34:03', ''),
  (8, 1, 1, 8, 1, 1542483303, '2016-08-09 01:36:32', ''),
  (9, 1, 1, 4, 1, 1382287998, '2016-08-09 12:33:27', ''),
  (10, 1, 1, 9, 1, 1542483302, '2016-08-10 01:40:12', ''),
  (11, 1, 1, 10, 1, 1542483323, '2016-08-11 01:40:49', ''),
  (12, 1, 1, 11, 1, 1542483272, '2016-08-12 03:21:44', ''),
  (13, 1, 1, 12, 1, 1542483313, '2016-08-13 03:17:42', ''),
  (14, 1, 1, 13, 1, 1542483323, '2016-08-14 03:16:31', ''),
  (15, 1, 1, 14, 1, 1542483302, '2016-08-15 03:13:38', ''),
  (16, 1, 1, 15, 1, 1542483299, '2016-08-16 03:14:05', ''),
  (17, 1, 1, 16, 1, 1542483278, '2016-08-17 03:08:05', ''),
  (18, 1, 1, 17, 1, 1542483302, '2016-08-18 03:24:45', ''),
  (19, 1, 1, 18, 1, 1542483293, '2016-08-19 03:12:11', ''),
  (20, 1, 1, 19, 1, 1542483279, '2016-08-20 03:15:07', ''),
  (21, 1, 1, 20, 1, 1542483272, '2016-08-21 03:17:22', ''),
  (22, 1, 1, 21, 1, 1542483299, '2016-08-22 03:11:57', ''),
  (23, 1, 1, 22, 1, 1542483320, '2016-08-23 03:15:59', ''),
  (24, 1, 1, 23, 1, 1542483301, '2016-08-24 03:15:08', ''),
  (25, 1, 1, 24, 1, 1542483306, '2016-08-25 03:14:34', ''),
  (26, 1, 1, 25, 1, 1542483275, '2016-08-26 03:16:47', ''),
  (27, 1, 1, 26, 1, 1542483292, '2016-08-27 03:20:37', ''),
  (28, 1, 1, 27, 1, 1542483291, '2016-08-28 03:21:54', ''),
  (29, 1, 1, 28, 1, 1542483305, '2016-08-29 03:14:04', ''),
  (30, 1, 1, 30, 1, 1542483291, '2016-08-30 03:23:27', ''),
  (31, 1, 1, 29, 1, 1542483293, '2016-08-30 03:23:28', ''),
  (32, 1, 1, 31, 1, 1542483307, '2016-08-31 03:13:31', ''),
  (33, 1, 1, 32, 1, 1542483290, '2016-09-01 03:11:11', ''),
  (34, 1, 1, 33, 1, 1542483277, '2016-09-01 03:11:11', ''),
  (35, 1, 1, 34, 1, 1542483299, '2016-09-02 03:19:46', ''),
  (36, 1, 1, 35, 1, 1542483268, '2016-09-03 03:17:22', ''),
  (37, 1, 1, 36, 1, 1542483321, '2016-09-04 03:26:09', ''),
  (38, 1, 1, 37, 1, 1542483273, '2016-09-04 03:26:09', ''),
  (39, 1, 1, 38, 1, 1542483299, '2016-09-05 03:19:38', ''),
  (40, 1, 1, 39, 1, 1542483274, '2016-09-06 03:26:23', ''),
  (41, 1, 1, 40, 1, 1542483274, '2016-09-07 03:19:26', ''),
  (42, 1, 1, 41, 1, 1542483297, '2016-09-08 03:24:26', ''),
  (43, 1, 1, 42, 1, 1542483306, '2016-09-09 03:23:09', ''),
  (44, 1, 1, 43, 1, 1542483297, '2016-09-10 03:50:30', ''),
  (45, 1, 1, 44, 1, 1542483297, '2016-09-11 03:15:34', ''),
  (46, 1, 1, 45, 1, 1542483277, '2016-09-12 03:17:15', ''),
  (47, 1, 1, 46, 1, 1542483271, '2016-09-13 03:20:14', ''),
  (48, 1, 1, 47, 1, 1542483301, '2016-09-14 03:19:33', ''),
  (49, 1, 1, 48, 1, 1542483317, '2016-09-15 03:23:43', ''),
  (50, 1, 1, 49, 1, 1542483283, '2016-09-16 03:27:12', ''),
  (51, 1, 1, 50, 1, 1542483272, '2016-09-17 03:28:26', ''),
  (52, 1, 1, 51, 1, 1542483291, '2016-09-18 03:30:44', ''),
  (53, 1, 1, 52, 1, 1542483324, '2016-09-19 03:29:02', ''),
  (54, 1, 1, 53, 1, 623906683, '2016-09-19 13:15:56', ''),
  (55, 1, 1, 54, 1, 1542483265, '2016-09-20 03:38:08', ''),
  (56, 1, 1, 55, 1, 1542483326, '2016-09-21 03:36:51', ''),
  (57, 1, 1, 56, 1, 1542483299, '2016-09-22 03:30:41', ''),
  (58, 1, 1, 57, 1, 1542483280, '2016-09-23 03:39:52', ''),
  (59, 1, 1, 58, 1, 1542483298, '2016-09-24 03:37:59', ''),
  (60, 1, 1, 59, 1, 1542483290, '2016-09-25 03:36:35', ''),
  (61, 1, 1, 60, 1, 1542483314, '2016-09-26 03:38:23', ''),
  (62, 1, 1, 61, 1, 1542483266, '2016-09-27 03:40:32', ''),
  (63, 1, 1, 62, 1, 1542483321, '2016-09-28 03:43:16', ''),
  (64, 1, 1, 63, 1, 1542483321, '2016-09-29 03:48:20', ''),
  (65, 1, 1, 64, 1, 1542483283, '2016-09-30 03:47:40', ''),
  (66, 1, 1, 65, 1, 1542483285, '2016-10-01 03:42:26', ''),
  (67, 1, 1, 66, 1, 1542483265, '2016-10-02 03:58:00', ''),
  (68, 1, 1, 67, 1, 1542483282, '2016-10-03 03:49:20', ''),
  (69, 1, 1, 68, 1, 1542483316, '2016-10-04 04:05:52', ''),
  (70, 1, 1, 69, 1, 1542483268, '2016-10-05 03:56:18', ''),
  (71, 1, 1, 70, 1, 623903183, '2016-10-05 11:33:33', ''),
  (72, 1, 1, 71, 1, 1382287998, '2016-10-05 13:34:27', ''),
  (73, 1, 1, 72, 2, 623903183, '2016-10-05 13:39:17', ''),
  (74, 1, 1, 73, 1, 1542483286, '2016-10-06 03:48:46', ''),
  (75, 1, 1, 71, 1, 1382287998, '2016-10-06 08:15:36', ''),
  (76, 1, 1, 74, 1, 3588407746, '2016-10-06 20:29:05', ''),
  (77, 1, 1, 75, 1, 1542483305, '2016-10-07 04:42:38', ''),
  (78, 1, 1, 71, 1, 1382287998, '2016-10-07 07:55:14', ''),
  (79, 1, 1, 76, 1, 1542483312, '2016-10-08 04:28:50', ''),
  (80, 1, 1, 77, 1, 1542483301, '2016-10-09 04:26:20', ''),
  (81, 1, 1, 78, 1, 1542483289, '2016-10-10 04:26:49', ''),
  (82, 1, 1, 79, 1, 1542483319, '2016-10-11 04:28:01', ''),
  (83, 1, 1, 80, 1, 1542483317, '2016-10-12 04:27:12', ''),
  (84, 1, 1, 81, 1, 1542483266, '2016-10-13 04:29:00', ''),
  (85, 1, 1, 82, 1, 1542483318, '2016-10-14 04:23:29', ''),
  (86, 1, 1, 83, 1, 1542483315, '2016-10-16 04:24:12', ''),
  (87, 1, 1, 84, 1, 1542483319, '2016-10-17 04:21:26', ''),
  (88, 1, 1, 85, 1, 1542483319, '2016-10-18 04:27:35', ''),
  (89, 1, 1, 86, 1, 1542483318, '2016-10-19 04:24:18', ''),
  (90, 1, 1, 87, 1, 1542483316, '2016-10-20 04:21:16', ''),
  (91, 1, 1, 88, 1, 1542483308, '2016-10-21 04:21:14', ''),
  (92, 1, 1, 89, 1, 1542483309, '2016-10-22 04:29:39', ''),
  (93, 1, 1, 90, 1, 1542483296, '2016-10-23 04:33:19', ''),
  (94, 1, 1, 91, 1, 1542483293, '2016-10-24 04:24:32', ''),
  (95, 1, 1, 92, 1, 1542483322, '2016-10-25 04:25:47', ''),
  (96, 1, 1, 93, 1, 1542483272, '2016-10-26 04:24:19', ''),
  (97, 1, 1, 94, 1, 1542483278, '2016-10-27 04:25:11', ''),
  (98, 1, 1, 95, 1, 1542483293, '2016-10-28 04:21:57', ''),
  (99, 1, 1, 96, 1, 1542483321, '2016-10-29 04:28:38', ''),
  (100, 1, 1, 97, 1, 1542483288, '2016-10-30 03:24:50', ''),
  (101, 1, 1, 98, 1, 1542483270, '2016-10-31 04:29:51', ''),
  (102, 1, 1, 99, 1, 1542483275, '2016-11-01 04:33:12', ''),
  (103, 1, 1, 100, 1, 1542483298, '2016-11-02 04:19:16', ''),
  (104, 1, 1, 101, 1, 1542483308, '2016-11-03 04:32:14', ''),
  (105, 1, 1, 102, 1, 623912635, '2016-11-03 08:03:46', ''),
  (106, 1, 1, 103, 1, 1542483326, '2016-11-04 04:26:44', ''),
  (107, 1, 1, 104, 1, 1542483325, '2016-11-05 04:28:35', ''),
  (108, 1, 1, 105, 1, 1542483293, '2016-11-06 04:38:17', ''),
  (109, 1, 1, 106, 1, 1542483302, '2016-11-07 04:33:56', ''),
  (110, 1, 1, 107, 1, 1542483313, '2016-11-08 04:30:09', ''),
  (111, 1, 1, 108, 1, 1542483286, '2016-11-09 05:09:21', ''),
  (112, 1, 1, 109, 1, 1542483286, '2016-11-10 04:29:58', ''),
  (113, 1, 1, 110, 1, 1542483313, '2016-11-11 04:51:13', ''),
  (114, 1, 1, 111, 1, 1542483283, '2016-11-12 04:46:26', ''),
  (115, 1, 1, 112, 1, 1542483315, '2016-11-13 04:31:56', ''),
  (116, 1, 1, 113, 1, 1542483304, '2016-11-14 04:26:45', ''),
  (117, 1, 1, 114, 1, 1542483291, '2016-11-15 04:32:33', ''),
  (118, 1, 1, 115, 1, 1542483286, '2016-11-16 04:41:34', ''),
  (119, 1, 1, 116, 1, 1542483315, '2016-11-17 04:31:08', ''),
  (120, 1, 1, 117, 1, 1542483283, '2016-11-18 04:35:05', ''),
  (121, 1, 1, 118, 1, 1542483307, '2016-11-19 04:26:51', ''),
  (122, 1, 1, 119, 1, 1542483282, '2016-11-20 04:27:34', ''),
  (123, 1, 1, 120, 1, 1542483287, '2016-11-21 04:28:14', ''),
  (124, 1, 1, 121, 1, 1542483268, '2016-11-22 04:27:15', ''),
  (125, 1, 1, 122, 1, 1542483274, '2016-11-23 04:25:18', ''),
  (126, 1, 1, 123, 1, 1542483305, '2016-11-24 04:30:01', ''),
  (127, 1, 1, 124, 1, 1542483305, '2016-11-25 04:33:13', ''),
  (128, 1, 1, 125, 1, 1542483294, '2016-11-26 04:32:24', ''),
  (129, 1, 1, 126, 1, 1542483305, '2016-11-27 04:30:22', ''),
  (130, 1, 1, 127, 1, 1542483284, '2016-11-28 04:28:56', ''),
  (131, 1, 1, 128, 1, 1542483279, '2016-11-29 04:34:52', ''),
  (132, 1, 1, 129, 1, 1542483321, '2016-11-30 04:32:06', ''),
  (133, 1, 1, 130, 1, 1542483273, '2016-12-01 04:40:13', ''),
  (134, 1, 1, 131, 1, 1542483280, '2016-12-02 04:34:35', ''),
  (135, 1, 1, 132, 1, 1542483302, '2016-12-03 04:36:25', ''),
  (136, 1, 1, 133, 1, 1542483292, '2016-12-04 04:36:45', ''),
  (137, 1, 1, 134, 1, 1542483306, '2016-12-05 04:36:08', ''),
  (138, 1, 1, 135, 1, 1542483286, '2016-12-06 04:40:51', ''),
  (139, 1, 1, 136, 1, 1542483288, '2016-12-07 04:41:24', ''),
  (140, 1, 1, 137, 1, 1542483321, '2016-12-08 04:40:44', ''),
  (141, 1, 1, 138, 1, 1542483306, '2016-12-09 04:38:13', ''),
  (142, 1, 1, 139, 1, 1542483289, '2016-12-10 04:45:57', ''),
  (143, 1, 1, 140, 1, 1542483323, '2016-12-11 04:43:30', ''),
  (144, 1, 1, 141, 1, 1542483294, '2016-12-12 04:45:19', ''),
  (145, 1, 1, 142, 1, 1542483290, '2016-12-13 04:47:50', ''),
  (146, 1, 1, 143, 1, 1542483286, '2016-12-14 04:43:38', ''),
  (147, 1, 1, 144, 1, 623913146, '2016-12-14 08:10:39', ''),
  (148, 1, 1, 145, 1, 1542483273, '2016-12-15 04:43:47', ''),
  (149, 1, 1, 146, 1, 1542483295, '2016-12-16 04:41:04', ''),
  (150, 1, 1, 147, 1, 1542483292, '2016-12-17 04:43:37', ''),
  (151, 1, 1, 148, 1, 1542483299, '2016-12-18 04:39:41', ''),
  (152, 1, 1, 149, 1, 1542483289, '2016-12-19 04:32:10', ''),
  (153, 1, 1, 150, 1, 1542483269, '2016-12-20 04:42:56', ''),
  (154, 1, 1, 151, 1, 1542483307, '2016-12-21 04:43:31', ''),
  (155, 1, 1, 152, 1, 1542483266, '2016-12-22 04:39:10', ''),
  (156, 1, 1, 153, 1, 1542483269, '2016-12-23 04:31:23', ''),
  (157, 1, 1, 154, 1, 1542483311, '2016-12-24 04:35:52', ''),
  (158, 1, 1, 155, 1, 1542483272, '2016-12-25 04:29:54', ''),
  (159, 1, 1, 156, 1, 1542483303, '2016-12-26 04:32:40', ''),
  (160, 1, 1, 157, 1, 1542483290, '2016-12-27 04:33:44', ''),
  (161, 1, 1, 158, 1, 1542483271, '2016-12-28 04:24:05', ''),
  (162, 1, 1, 159, 1, 1542483274, '2016-12-29 04:33:16', ''),
  (163, 1, 1, 160, 1, 1542483294, '2016-12-30 04:25:59', ''),
  (164, 1, 1, 161, 1, 1542483307, '2016-12-31 04:33:56', ''),
  (165, 1, 1, 162, 1, 1542483272, '2017-01-01 04:35:33', ''),
  (166, 1, 1, 163, 1, 1542483283, '2017-01-02 04:32:56', ''),
  (167, 1, 1, 164, 1, 1542483303, '2017-01-03 04:32:18', ''),
  (168, 1, 1, 165, 1, 1542483290, '2017-01-04 04:29:47', ''),
  (169, 1, 1, 166, 1, 1542483272, '2017-01-05 04:29:27', ''),
  (170, 1, 1, 167, 1, 1542483276, '2017-01-06 04:29:24', ''),
  (171, 1, 1, 168, 1, 1542483289, '2017-01-07 04:27:30', ''),
  (172, 1, 1, 169, 1, 1542483306, '2017-01-08 04:39:44', ''),
  (173, 1, 1, 170, 1, 1542483326, '2017-01-09 04:28:48', ''),
  (174, 1, 1, 171, 1, 1542483273, '2017-01-10 04:36:47', ''),
  (175, 1, 1, 172, 1, 1542483307, '2017-01-11 04:52:30', ''),
  (176, 1, 1, 173, 1, 1542483277, '2017-01-12 04:39:58', ''),
  (177, 1, 1, 174, 1, 1542483299, '2017-01-13 04:36:21', ''),
  (178, 1, 1, 175, 1, 1542483307, '2017-01-14 04:44:49', ''),
  (179, 1, 1, 176, 1, 1542483273, '2017-01-15 04:42:43', ''),
  (180, 1, 1, 177, 1, 1542483314, '2017-01-16 04:40:37', ''),
  (181, 1, 1, 178, 1, 1542483312, '2017-01-17 04:37:21', ''),
  (182, 1, 1, 179, 1, 1542483269, '2017-01-18 04:39:13', ''),
  (183, 1, 1, 180, 1, 1542483293, '2017-01-19 04:43:05', ''),
  (184, 1, 1, 181, 1, 1542483319, '2017-01-20 04:45:18', ''),
  (185, 1, 1, 182, 1, 1542483268, '2017-01-21 04:46:40', ''),
  (186, 1, 1, 183, 1, 1446888147, '2017-01-21 13:55:57', ''),
  (187, 1, 1, 184, 1, 1542483282, '2017-01-22 04:50:15', ''),
  (188, 1, 1, 185, 1, 1542483290, '2017-01-23 04:48:44', ''),
  (189, 1, 1, 186, 1, 623905096, '2017-01-23 18:30:24', ''),
  (190, 1, 1, 187, 1, 1542483288, '2017-01-24 04:49:43', ''),
  (191, 1, 1, 188, 1, 1382287998, '2017-01-24 09:25:44', ''),
  (192, 1, 1, 189, 1, 1542483296, '2017-01-25 04:48:13', ''),
  (193, 1, 1, 190, 1, 3558098443, '2017-01-25 08:48:01', ''),
  (194, 1, 1, 188, 1, 1382287998, '2017-01-25 14:20:24', ''),
  (195, 1, 1, 191, 1, 1542483317, '2017-01-26 04:53:49', ''),
  (196, 1, 1, 192, 1, 1542483296, '2017-01-27 04:53:05', ''),
  (197, 1, 1, 193, 1, 1542483292, '2017-01-28 04:50:06', ''),
  (198, 1, 1, 194, 1, 1542483313, '2017-01-29 04:47:25', ''),
  (199, 1, 1, 195, 1, 1542483303, '2017-01-30 04:49:13', ''),
  (200, 1, 1, 196, 1, 1542483280, '2017-01-31 04:51:08', ''),
  (201, 1, 1, 197, 1, 1542483311, '2017-02-01 04:48:51', ''),
  (202, 1, 1, 198, 1, 1542483308, '2017-02-02 04:49:40', ''),
  (203, 1, 1, 199, 1, 1542483286, '2017-02-03 04:53:58', ''),
  (204, 1, 1, 200, 1, 1542483266, '2017-02-04 04:58:27', ''),
  (205, 1, 1, 201, 1, 1542483284, '2017-02-05 05:03:23', ''),
  (206, 1, 1, 202, 1, 1542483270, '2017-02-06 04:53:50', ''),
  (207, 1, 1, 203, 1, 1542483283, '2017-02-07 04:56:37', ''),
  (208, 1, 1, 204, 1, 1542483274, '2017-02-08 04:58:49', ''),
  (209, 1, 1, 205, 1, 1542483300, '2017-02-09 04:56:12', ''),
  (210, 1, 1, 206, 1, 1542483282, '2017-02-10 04:59:55', ''),
  (211, 1, 1, 207, 1, 623903996, '2017-02-10 13:27:02', ''),
  (212, 1, 1, 208, 1, 623903996, '2017-02-10 13:27:02', ''),
  (213, 1, 1, 209, 1, 1604901308, '2017-02-10 13:27:08', ''),
  (214, 1, 1, 210, 1, 623903996, '2017-02-10 13:29:52', ''),
  (215, 1, 1, 211, 1, 1542483285, '2017-02-11 01:31:22', ''),
  (216, 1, 1, 212, 1, 1542483321, '2017-02-12 01:33:15', ''),
  (217, 1, 1, 213, 1, 1542483307, '2017-02-13 01:36:09', ''),
  (218, 1, 1, 214, 1, 1542483295, '2017-02-14 01:39:02', ''),
  (219, 1, 1, 215, 1, 1542483299, '2017-02-15 01:42:53', ''),
  (220, 1, 1, 216, 1, 1542483289, '2017-02-16 01:46:07', ''),
  (221, 1, 1, 217, 1, 1542483292, '2017-02-17 01:48:31', ''),
  (222, 1, 1, 218, 1, 1542483297, '2017-02-18 01:49:55', ''),
  (223, 1, 1, 219, 1, 1542483278, '2017-02-19 01:52:04', ''),
  (224, 1, 1, 220, 1, 1542483284, '2017-02-20 01:55:20', ''),
  (225, 1, 1, 221, 1, 1542483272, '2017-02-21 01:56:26', ''),
  (226, 1, 1, 222, 1, 1542483306, '2017-02-22 02:01:10', ''),
  (227, 1, 1, 210, 1, 623911302, '2017-02-22 10:20:20', ''),
  (228, 1, 1, 223, 1, 1542483301, '2017-02-23 02:02:37', ''),
  (229, 1, 1, 224, 1, 1542483323, '2017-02-24 02:11:45', ''),
  (230, 1, 1, 225, 1, 1542483300, '2017-02-25 02:07:32', ''),
  (231, 1, 1, 226, 1, 1542483275, '2017-02-26 02:13:05', ''),
  (232, 1, 1, 227, 1, 1542483307, '2017-02-27 02:06:16', ''),
  (233, 1, 1, 228, 1, 1542483303, '2017-02-28 02:10:51', ''),
  (234, 1, 1, 229, 1, 1542483290, '2017-03-01 02:13:48', ''),
  (235, 1, 1, 230, 1, 1542483324, '2017-03-02 02:21:11', ''),
  (236, 1, 1, 231, 1, 1542483265, '2017-03-03 02:23:39', ''),
  (237, 1, 1, 232, 1, 1542483298, '2017-03-04 02:24:54', ''),
  (238, 1, 1, 233, 1, 1542483273, '2017-03-05 02:24:43', ''),
  (239, 1, 1, 234, 1, 1542483315, '2017-03-06 02:25:24', ''),
  (240, 1, 1, 235, 1, 1542483295, '2017-03-07 02:32:11', ''),
  (241, 1, 1, 236, 1, 1542483290, '2017-03-08 02:30:07', ''),
  (242, 1, 1, 237, 1, 1542483283, '2017-03-09 02:32:00', ''),
  (243, 1, 1, 238, 1, 1542483313, '2017-03-10 02:43:43', ''),
  (244, 1, 1, 239, 1, 1542483301, '2017-03-11 02:46:56', ''),
  (245, 1, 1, 240, 1, 1542483311, '2017-03-12 02:47:12', ''),
  (246, 1, 1, 241, 1, 1542483314, '2017-03-13 02:42:06', ''),
  (247, 1, 1, 242, 1, 1542483304, '2017-03-14 02:49:36', ''),
  (248, 1, 1, 243, 1, 1542483275, '2017-03-15 02:48:28', ''),
  (249, 1, 1, 244, 1, 1542483280, '2017-03-16 02:53:06', ''),
  (250, 1, 1, 245, 1, 1542483273, '2017-03-17 02:55:07', ''),
  (251, 1, 1, 246, 1, 1542483270, '2017-03-18 02:49:53', ''),
  (252, 1, 1, 247, 1, 1542483279, '2017-03-19 02:52:42', ''),
  (253, 1, 1, 248, 1, 1542483300, '2017-03-20 02:46:55', ''),
  (254, 1, 1, 249, 1, 1542483266, '2017-03-21 02:53:01', ''),
  (255, 1, 1, 250, 1, 1542483287, '2017-03-22 02:52:30', ''),
  (256, 1, 1, 251, 1, 1542483295, '2017-03-23 02:53:37', ''),
  (257, 1, 1, 252, 1, 1542483318, '2017-03-24 02:53:19', ''),
  (258, 1, 1, 253, 1, 1542483292, '2017-03-25 02:50:31', ''),
  (259, 1, 1, 254, 1, 1542483321, '2017-03-26 03:52:49', ''),
  (260, 1, 1, 255, 1, 1542483322, '2017-03-27 02:50:36', ''),
  (261, 1, 1, 256, 1, 1542483286, '2017-03-28 02:49:01', ''),
  (262, 1, 1, 257, 1, 1542483268, '2017-03-29 02:51:25', ''),
  (263, 1, 1, 258, 1, 1542483301, '2017-03-30 02:53:21', ''),
  (264, 1, 1, 259, 1, 623911480, '2017-03-30 17:36:26', ''),
  (265, 1, 1, 260, 1, 1542483290, '2017-03-31 02:55:06', ''),
  (266, 1, 1, 261, 1, 1499985763, '2017-03-31 09:56:15', ''),
  (267, 1, 1, 262, 1, 1542483282, '2017-04-01 02:56:17', ''),
  (268, 1, 1, 263, 1, 1542483323, '2017-04-02 02:58:34', ''),
  (269, 1, 1, 264, 1, 1542483321, '2017-04-03 02:54:02', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_connections_page`
--

CREATE TABLE IF NOT EXISTS `ps_connections_page` (
  `id_connections` INT(10) UNSIGNED NOT NULL,
  `id_page`        INT(10) UNSIGNED NOT NULL,
  `time_start`     DATETIME         NOT NULL,
  `time_end`       DATETIME DEFAULT NULL,
  PRIMARY KEY (`id_connections`, `id_page`, `time_start`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_connections_page`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_connections_source`
--

CREATE TABLE IF NOT EXISTS `ps_connections_source` (
  `id_connections_source` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_connections`        INT(10) UNSIGNED NOT NULL,
  `http_referer`          VARCHAR(255)              DEFAULT NULL,
  `request_uri`           VARCHAR(255)              DEFAULT NULL,
  `keywords`              VARCHAR(255)              DEFAULT NULL,
  `date_add`              DATETIME         NOT NULL,
  PRIMARY KEY (`id_connections_source`),
  KEY `connections` (`id_connections`),
  KEY `orderby` (`date_add`),
  KEY `http_referer` (`http_referer`),
  KEY `request_uri` (`request_uri`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_connections_source`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_contact`
--

CREATE TABLE IF NOT EXISTS `ps_contact` (
  `id_contact`       INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `email`            VARCHAR(128)        NOT NULL,
  `customer_service` TINYINT(1)          NOT NULL DEFAULT '0',
  `position`         TINYINT(2) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_contact`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 3;

--
-- Vypisuji data pro tabulku `ps_contact`
--

INSERT INTO `ps_contact` (`id_contact`, `email`, `customer_service`, `position`) VALUES
  (1, 'stoupa.bat@seznam.cz', 1, 0),
  (2, 'stoupa.bat@seznam.cz', 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_contact_lang`
--

CREATE TABLE IF NOT EXISTS `ps_contact_lang` (
  `id_contact`  INT(10) UNSIGNED NOT NULL,
  `id_lang`     INT(10) UNSIGNED NOT NULL,
  `name`        VARCHAR(32)      NOT NULL,
  `description` TEXT,
  PRIMARY KEY (`id_contact`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_contact_lang`
--

INSERT INTO `ps_contact_lang` (`id_contact`, `id_lang`, `name`, `description`) VALUES
  (1, 1, 'Webmaster', 'If a technical problem occurs on this website'),
  (2, 1, 'Customer service', 'For any question about a product, an order');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_contact_shop`
--

CREATE TABLE IF NOT EXISTS `ps_contact_shop` (
  `id_contact` INT(11) UNSIGNED NOT NULL,
  `id_shop`    INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_contact`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_contact_shop`
--

INSERT INTO `ps_contact_shop` (`id_contact`, `id_shop`) VALUES
  (1, 1),
  (2, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_country`
--

CREATE TABLE IF NOT EXISTS `ps_country` (
  `id_country`                 INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_zone`                    INT(10) UNSIGNED    NOT NULL,
  `id_currency`                INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `iso_code`                   VARCHAR(3)          NOT NULL,
  `call_prefix`                INT(10)             NOT NULL DEFAULT '0',
  `active`                     TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `contains_states`            TINYINT(1)          NOT NULL DEFAULT '0',
  `need_identification_number` TINYINT(1)          NOT NULL DEFAULT '0',
  `need_zip_code`              TINYINT(1)          NOT NULL DEFAULT '1',
  `zip_code_format`            VARCHAR(12)         NOT NULL DEFAULT '',
  `display_tax_label`          TINYINT(1)          NOT NULL,
  PRIMARY KEY (`id_country`),
  KEY `country_iso_code` (`iso_code`),
  KEY `country_` (`id_zone`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 245;

--
-- Vypisuji data pro tabulku `ps_country`
--

INSERT INTO `ps_country` (`id_country`, `id_zone`, `id_currency`, `iso_code`, `call_prefix`, `active`, `contains_states`, `need_identification_number`, `need_zip_code`, `zip_code_format`, `display_tax_label`)
VALUES
  (1, 1, 0, 'DE', 49, 0, 0, 0, 1, 'NNNNN', 1),
  (2, 1, 0, 'AT', 43, 0, 0, 0, 1, 'NNNN', 1),
  (3, 1, 0, 'BE', 32, 0, 0, 0, 1, 'NNNN', 1),
  (4, 2, 0, 'CA', 1, 0, 1, 0, 1, 'LNL NLN', 0),
  (5, 3, 0, 'CN', 86, 0, 0, 0, 1, 'NNNNNN', 1),
  (6, 1, 0, 'ES', 34, 0, 0, 1, 1, 'NNNNN', 1),
  (7, 1, 0, 'FI', 358, 0, 0, 0, 1, 'NNNNN', 1),
  (8, 1, 0, 'FR', 33, 0, 0, 0, 1, 'NNNNN', 1),
  (9, 1, 0, 'GR', 30, 0, 0, 0, 1, 'NNNNN', 1),
  (10, 1, 0, 'IT', 39, 0, 1, 0, 1, 'NNNNN', 1),
  (11, 3, 0, 'JP', 81, 0, 1, 0, 1, 'NNN-NNNN', 1),
  (12, 1, 0, 'LU', 352, 0, 0, 0, 1, 'NNNN', 1),
  (13, 1, 0, 'NL', 31, 0, 0, 0, 1, 'NNNN LL', 1),
  (14, 1, 0, 'PL', 48, 0, 0, 0, 1, 'NN-NNN', 1),
  (15, 1, 0, 'PT', 351, 0, 0, 0, 1, 'NNNN-NNN', 1),
  (16, 1, 0, 'CZ', 420, 1, 0, 0, 1, 'NNN NN', 1),
  (17, 1, 0, 'GB', 44, 0, 0, 0, 1, '', 1),
  (18, 1, 0, 'SE', 46, 0, 0, 0, 1, 'NNN NN', 1),
  (19, 7, 0, 'CH', 41, 0, 0, 0, 1, 'NNNN', 1),
  (20, 1, 0, 'DK', 45, 0, 0, 0, 1, 'NNNN', 1),
  (21, 2, 0, 'US', 1, 0, 1, 0, 1, 'NNNNN', 0),
  (22, 3, 0, 'HK', 852, 0, 0, 0, 0, '', 1),
  (23, 7, 0, 'NO', 47, 0, 0, 0, 1, 'NNNN', 1),
  (24, 5, 0, 'AU', 61, 0, 0, 0, 1, 'NNNN', 1),
  (25, 3, 0, 'SG', 65, 0, 0, 0, 1, 'NNNNNN', 1),
  (26, 1, 0, 'IE', 353, 0, 0, 0, 0, '', 1),
  (27, 5, 0, 'NZ', 64, 0, 0, 0, 1, 'NNNN', 1),
  (28, 3, 0, 'KR', 82, 0, 0, 0, 1, 'NNN-NNN', 1),
  (29, 3, 0, 'IL', 972, 0, 0, 0, 1, 'NNNNNNN', 1),
  (30, 4, 0, 'ZA', 27, 0, 0, 0, 1, 'NNNN', 1),
  (31, 4, 0, 'NG', 234, 0, 0, 0, 1, '', 1),
  (32, 4, 0, 'CI', 225, 0, 0, 0, 1, '', 1),
  (33, 4, 0, 'TG', 228, 0, 0, 0, 1, '', 1),
  (34, 6, 0, 'BO', 591, 0, 0, 0, 1, '', 1),
  (35, 4, 0, 'MU', 230, 0, 0, 0, 1, '', 1),
  (36, 1, 0, 'RO', 40, 0, 0, 0, 1, 'NNNNNN', 1),
  (37, 1, 0, 'SK', 421, 0, 0, 0, 1, 'NNN NN', 1),
  (38, 4, 0, 'DZ', 213, 0, 0, 0, 1, 'NNNNN', 1),
  (39, 2, 0, 'AS', 0, 0, 0, 0, 1, '', 1),
  (40, 7, 0, 'AD', 376, 0, 0, 0, 1, 'CNNN', 1),
  (41, 4, 0, 'AO', 244, 0, 0, 0, 0, '', 1),
  (42, 8, 0, 'AI', 0, 0, 0, 0, 1, '', 1),
  (43, 2, 0, 'AG', 0, 0, 0, 0, 1, '', 1),
  (44, 6, 0, 'AR', 54, 0, 1, 0, 1, 'LNNNN', 1),
  (45, 3, 0, 'AM', 374, 0, 0, 0, 1, 'NNNN', 1),
  (46, 8, 0, 'AW', 297, 0, 0, 0, 1, '', 1),
  (47, 3, 0, 'AZ', 994, 0, 0, 0, 1, 'CNNNN', 1),
  (48, 2, 0, 'BS', 0, 0, 0, 0, 1, '', 1),
  (49, 3, 0, 'BH', 973, 0, 0, 0, 1, '', 1),
  (50, 3, 0, 'BD', 880, 0, 0, 0, 1, 'NNNN', 1),
  (51, 2, 0, 'BB', 0, 0, 0, 0, 1, 'CNNNNN', 1),
  (52, 7, 0, 'BY', 0, 0, 0, 0, 1, 'NNNNNN', 1),
  (53, 8, 0, 'BZ', 501, 0, 0, 0, 0, '', 1),
  (54, 4, 0, 'BJ', 229, 0, 0, 0, 0, '', 1),
  (55, 2, 0, 'BM', 0, 0, 0, 0, 1, '', 1),
  (56, 3, 0, 'BT', 975, 0, 0, 0, 1, '', 1),
  (57, 4, 0, 'BW', 267, 0, 0, 0, 1, '', 1),
  (58, 6, 0, 'BR', 55, 0, 0, 0, 1, 'NNNNN-NNN', 1),
  (59, 3, 0, 'BN', 673, 0, 0, 0, 1, 'LLNNNN', 1),
  (60, 4, 0, 'BF', 226, 0, 0, 0, 1, '', 1),
  (61, 3, 0, 'MM', 95, 0, 0, 0, 1, '', 1),
  (62, 4, 0, 'BI', 257, 0, 0, 0, 1, '', 1),
  (63, 3, 0, 'KH', 855, 0, 0, 0, 1, 'NNNNN', 1),
  (64, 4, 0, 'CM', 237, 0, 0, 0, 1, '', 1),
  (65, 4, 0, 'CV', 238, 0, 0, 0, 1, 'NNNN', 1),
  (66, 4, 0, 'CF', 236, 0, 0, 0, 1, '', 1),
  (67, 4, 0, 'TD', 235, 0, 0, 0, 1, '', 1),
  (68, 6, 0, 'CL', 56, 0, 0, 0, 1, 'NNN-NNNN', 1),
  (69, 6, 0, 'CO', 57, 0, 0, 0, 1, 'NNNNNN', 1),
  (70, 4, 0, 'KM', 269, 0, 0, 0, 1, '', 1),
  (71, 4, 0, 'CD', 242, 0, 0, 0, 1, '', 1),
  (72, 4, 0, 'CG', 243, 0, 0, 0, 1, '', 1),
  (73, 8, 0, 'CR', 506, 0, 0, 0, 1, 'NNNNN', 1),
  (74, 7, 0, 'HR', 385, 0, 0, 0, 1, 'NNNNN', 1),
  (75, 8, 0, 'CU', 53, 0, 0, 0, 1, '', 1),
  (76, 1, 0, 'CY', 357, 0, 0, 0, 1, 'NNNN', 1),
  (77, 4, 0, 'DJ', 253, 0, 0, 0, 1, '', 1),
  (78, 8, 0, 'DM', 0, 0, 0, 0, 1, '', 1),
  (79, 8, 0, 'DO', 0, 0, 0, 0, 1, '', 1),
  (80, 3, 0, 'TL', 670, 0, 0, 0, 1, '', 1),
  (81, 6, 0, 'EC', 593, 0, 0, 0, 1, 'CNNNNNN', 1),
  (82, 4, 0, 'EG', 20, 0, 0, 0, 0, '', 1),
  (83, 8, 0, 'SV', 503, 0, 0, 0, 1, '', 1),
  (84, 4, 0, 'GQ', 240, 0, 0, 0, 1, '', 1),
  (85, 4, 0, 'ER', 291, 0, 0, 0, 1, '', 1),
  (86, 1, 0, 'EE', 372, 0, 0, 0, 1, 'NNNNN', 1),
  (87, 4, 0, 'ET', 251, 0, 0, 0, 1, '', 1),
  (88, 8, 0, 'FK', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
  (89, 7, 0, 'FO', 298, 0, 0, 0, 1, '', 1),
  (90, 5, 0, 'FJ', 679, 0, 0, 0, 1, '', 1),
  (91, 4, 0, 'GA', 241, 0, 0, 0, 1, '', 1),
  (92, 4, 0, 'GM', 220, 0, 0, 0, 1, '', 1),
  (93, 3, 0, 'GE', 995, 0, 0, 0, 1, 'NNNN', 1),
  (94, 4, 0, 'GH', 233, 0, 0, 0, 1, '', 1),
  (95, 8, 0, 'GD', 0, 0, 0, 0, 1, '', 1),
  (96, 7, 0, 'GL', 299, 0, 0, 0, 1, '', 1),
  (97, 7, 0, 'GI', 350, 0, 0, 0, 1, '', 1),
  (98, 8, 0, 'GP', 590, 0, 0, 0, 1, '', 1),
  (99, 5, 0, 'GU', 0, 0, 0, 0, 1, '', 1),
  (100, 8, 0, 'GT', 502, 0, 0, 0, 1, '', 1),
  (101, 7, 0, 'GG', 0, 0, 0, 0, 1, 'LLN NLL', 1),
  (102, 4, 0, 'GN', 224, 0, 0, 0, 1, '', 1),
  (103, 4, 0, 'GW', 245, 0, 0, 0, 1, '', 1),
  (104, 6, 0, 'GY', 592, 0, 0, 0, 1, '', 1),
  (105, 8, 0, 'HT', 509, 0, 0, 0, 1, '', 1),
  (106, 5, 0, 'HM', 0, 0, 0, 0, 1, '', 1),
  (107, 7, 0, 'VA', 379, 0, 0, 0, 1, 'NNNNN', 1),
  (108, 8, 0, 'HN', 504, 0, 0, 0, 1, '', 1),
  (109, 7, 0, 'IS', 354, 0, 0, 0, 1, 'NNN', 1),
  (110, 3, 0, 'IN', 91, 0, 0, 0, 1, 'NNN NNN', 1),
  (111, 3, 0, 'ID', 62, 0, 1, 0, 1, 'NNNNN', 1),
  (112, 3, 0, 'IR', 98, 0, 0, 0, 1, 'NNNNN-NNNNN', 1),
  (113, 3, 0, 'IQ', 964, 0, 0, 0, 1, 'NNNNN', 1),
  (114, 7, 0, 'IM', 0, 0, 0, 0, 1, 'CN NLL', 1),
  (115, 8, 0, 'JM', 0, 0, 0, 0, 1, '', 1),
  (116, 7, 0, 'JE', 0, 0, 0, 0, 1, 'CN NLL', 1),
  (117, 3, 0, 'JO', 962, 0, 0, 0, 1, '', 1),
  (118, 3, 0, 'KZ', 7, 0, 0, 0, 1, 'NNNNNN', 1),
  (119, 4, 0, 'KE', 254, 0, 0, 0, 1, '', 1),
  (120, 5, 0, 'KI', 686, 0, 0, 0, 1, '', 1),
  (121, 3, 0, 'KP', 850, 0, 0, 0, 1, '', 1),
  (122, 3, 0, 'KW', 965, 0, 0, 0, 1, '', 1),
  (123, 3, 0, 'KG', 996, 0, 0, 0, 1, '', 1),
  (124, 3, 0, 'LA', 856, 0, 0, 0, 1, '', 1),
  (125, 1, 0, 'LV', 371, 0, 0, 0, 1, 'C-NNNN', 1),
  (126, 3, 0, 'LB', 961, 0, 0, 0, 1, '', 1),
  (127, 4, 0, 'LS', 266, 0, 0, 0, 1, '', 1),
  (128, 4, 0, 'LR', 231, 0, 0, 0, 1, '', 1),
  (129, 4, 0, 'LY', 218, 0, 0, 0, 1, '', 1),
  (130, 1, 0, 'LI', 423, 0, 0, 0, 1, 'NNNN', 1),
  (131, 1, 0, 'LT', 370, 0, 0, 0, 1, 'NNNNN', 1),
  (132, 3, 0, 'MO', 853, 0, 0, 0, 0, '', 1),
  (133, 7, 0, 'MK', 389, 0, 0, 0, 1, '', 1),
  (134, 4, 0, 'MG', 261, 0, 0, 0, 1, '', 1),
  (135, 4, 0, 'MW', 265, 0, 0, 0, 1, '', 1),
  (136, 3, 0, 'MY', 60, 0, 0, 0, 1, 'NNNNN', 1),
  (137, 3, 0, 'MV', 960, 0, 0, 0, 1, '', 1),
  (138, 4, 0, 'ML', 223, 0, 0, 0, 1, '', 1),
  (139, 1, 0, 'MT', 356, 0, 0, 0, 1, 'LLL NNNN', 1),
  (140, 5, 0, 'MH', 692, 0, 0, 0, 1, '', 1),
  (141, 8, 0, 'MQ', 596, 0, 0, 0, 1, '', 1),
  (142, 4, 0, 'MR', 222, 0, 0, 0, 1, '', 1),
  (143, 1, 0, 'HU', 36, 0, 0, 0, 1, 'NNNN', 1),
  (144, 4, 0, 'YT', 262, 0, 0, 0, 1, '', 1),
  (145, 2, 0, 'MX', 52, 0, 1, 1, 1, 'NNNNN', 1),
  (146, 5, 0, 'FM', 691, 0, 0, 0, 1, '', 1),
  (147, 7, 0, 'MD', 373, 0, 0, 0, 1, 'C-NNNN', 1),
  (148, 7, 0, 'MC', 377, 0, 0, 0, 1, '980NN', 1),
  (149, 3, 0, 'MN', 976, 0, 0, 0, 1, '', 1),
  (150, 7, 0, 'ME', 382, 0, 0, 0, 1, 'NNNNN', 1),
  (151, 8, 0, 'MS', 0, 0, 0, 0, 1, '', 1),
  (152, 4, 0, 'MA', 212, 0, 0, 0, 1, 'NNNNN', 1),
  (153, 4, 0, 'MZ', 258, 0, 0, 0, 1, '', 1),
  (154, 4, 0, 'NA', 264, 0, 0, 0, 1, '', 1),
  (155, 5, 0, 'NR', 674, 0, 0, 0, 1, '', 1),
  (156, 3, 0, 'NP', 977, 0, 0, 0, 1, '', 1),
  (157, 8, 0, 'AN', 599, 0, 0, 0, 1, '', 1),
  (158, 5, 0, 'NC', 687, 0, 0, 0, 1, '', 1),
  (159, 8, 0, 'NI', 505, 0, 0, 0, 1, 'NNNNNN', 1),
  (160, 4, 0, 'NE', 227, 0, 0, 0, 1, '', 1),
  (161, 5, 0, 'NU', 683, 0, 0, 0, 1, '', 1),
  (162, 5, 0, 'NF', 0, 0, 0, 0, 1, '', 1),
  (163, 5, 0, 'MP', 0, 0, 0, 0, 1, '', 1),
  (164, 3, 0, 'OM', 968, 0, 0, 0, 1, '', 1),
  (165, 3, 0, 'PK', 92, 0, 0, 0, 1, '', 1),
  (166, 5, 0, 'PW', 680, 0, 0, 0, 1, '', 1),
  (167, 3, 0, 'PS', 0, 0, 0, 0, 1, '', 1),
  (168, 8, 0, 'PA', 507, 0, 0, 0, 1, 'NNNNNN', 1),
  (169, 5, 0, 'PG', 675, 0, 0, 0, 1, '', 1),
  (170, 6, 0, 'PY', 595, 0, 0, 0, 1, '', 1),
  (171, 6, 0, 'PE', 51, 0, 0, 0, 1, '', 1),
  (172, 3, 0, 'PH', 63, 0, 0, 0, 1, 'NNNN', 1),
  (173, 5, 0, 'PN', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
  (174, 8, 0, 'PR', 0, 0, 0, 0, 1, 'NNNNN', 1),
  (175, 3, 0, 'QA', 974, 0, 0, 0, 1, '', 1),
  (176, 4, 0, 'RE', 262, 0, 0, 0, 1, '', 1),
  (177, 7, 0, 'RU', 7, 0, 0, 0, 1, 'NNNNNN', 1),
  (178, 4, 0, 'RW', 250, 0, 0, 0, 1, '', 1),
  (179, 8, 0, 'BL', 0, 0, 0, 0, 1, '', 1),
  (180, 8, 0, 'KN', 0, 0, 0, 0, 1, '', 1),
  (181, 8, 0, 'LC', 0, 0, 0, 0, 1, '', 1),
  (182, 8, 0, 'MF', 0, 0, 0, 0, 1, '', 1),
  (183, 8, 0, 'PM', 508, 0, 0, 0, 1, '', 1),
  (184, 8, 0, 'VC', 0, 0, 0, 0, 1, '', 1),
  (185, 5, 0, 'WS', 685, 0, 0, 0, 1, '', 1),
  (186, 7, 0, 'SM', 378, 0, 0, 0, 1, 'NNNNN', 1),
  (187, 4, 0, 'ST', 239, 0, 0, 0, 1, '', 1),
  (188, 3, 0, 'SA', 966, 0, 0, 0, 1, '', 1),
  (189, 4, 0, 'SN', 221, 0, 0, 0, 1, '', 1),
  (190, 7, 0, 'RS', 381, 0, 0, 0, 1, 'NNNNN', 1),
  (191, 4, 0, 'SC', 248, 0, 0, 0, 1, '', 1),
  (192, 4, 0, 'SL', 232, 0, 0, 0, 1, '', 1),
  (193, 1, 0, 'SI', 386, 0, 0, 0, 1, 'C-NNNN', 1),
  (194, 5, 0, 'SB', 677, 0, 0, 0, 1, '', 1),
  (195, 4, 0, 'SO', 252, 0, 0, 0, 1, '', 1),
  (196, 8, 0, 'GS', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
  (197, 3, 0, 'LK', 94, 0, 0, 0, 1, 'NNNNN', 1),
  (198, 4, 0, 'SD', 249, 0, 0, 0, 1, '', 1),
  (199, 8, 0, 'SR', 597, 0, 0, 0, 1, '', 1),
  (200, 7, 0, 'SJ', 0, 0, 0, 0, 1, '', 1),
  (201, 4, 0, 'SZ', 268, 0, 0, 0, 1, '', 1),
  (202, 3, 0, 'SY', 963, 0, 0, 0, 1, '', 1),
  (203, 3, 0, 'TW', 886, 0, 0, 0, 1, 'NNNNN', 1),
  (204, 3, 0, 'TJ', 992, 0, 0, 0, 1, '', 1),
  (205, 4, 0, 'TZ', 255, 0, 0, 0, 1, '', 1),
  (206, 3, 0, 'TH', 66, 0, 0, 0, 1, 'NNNNN', 1),
  (207, 5, 0, 'TK', 690, 0, 0, 0, 1, '', 1),
  (208, 5, 0, 'TO', 676, 0, 0, 0, 1, '', 1),
  (209, 6, 0, 'TT', 0, 0, 0, 0, 1, '', 1),
  (210, 4, 0, 'TN', 216, 0, 0, 0, 1, '', 1),
  (211, 7, 0, 'TR', 90, 0, 0, 0, 1, 'NNNNN', 1),
  (212, 3, 0, 'TM', 993, 0, 0, 0, 1, '', 1),
  (213, 8, 0, 'TC', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
  (214, 5, 0, 'TV', 688, 0, 0, 0, 1, '', 1),
  (215, 4, 0, 'UG', 256, 0, 0, 0, 1, '', 1),
  (216, 1, 0, 'UA', 380, 0, 0, 0, 1, 'NNNNN', 1),
  (217, 3, 0, 'AE', 971, 0, 0, 0, 1, '', 1),
  (218, 6, 0, 'UY', 598, 0, 0, 0, 1, '', 1),
  (219, 3, 0, 'UZ', 998, 0, 0, 0, 1, '', 1),
  (220, 5, 0, 'VU', 678, 0, 0, 0, 1, '', 1),
  (221, 6, 0, 'VE', 58, 0, 0, 0, 1, '', 1),
  (222, 3, 0, 'VN', 84, 0, 0, 0, 1, 'NNNNNN', 1),
  (223, 2, 0, 'VG', 0, 0, 0, 0, 1, 'CNNNN', 1),
  (224, 2, 0, 'VI', 0, 0, 0, 0, 1, '', 1),
  (225, 5, 0, 'WF', 681, 0, 0, 0, 1, '', 1),
  (226, 4, 0, 'EH', 0, 0, 0, 0, 1, '', 1),
  (227, 3, 0, 'YE', 967, 0, 0, 0, 1, '', 1),
  (228, 4, 0, 'ZM', 260, 0, 0, 0, 1, '', 1),
  (229, 4, 0, 'ZW', 263, 0, 0, 0, 1, '', 1),
  (230, 7, 0, 'AL', 355, 0, 0, 0, 1, 'NNNN', 1),
  (231, 3, 0, 'AF', 93, 0, 0, 0, 0, '', 1),
  (232, 5, 0, 'AQ', 0, 0, 0, 0, 1, '', 1),
  (233, 1, 0, 'BA', 387, 0, 0, 0, 1, '', 1),
  (234, 5, 0, 'BV', 0, 0, 0, 0, 1, '', 1),
  (235, 5, 0, 'IO', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
  (236, 1, 0, 'BG', 359, 0, 0, 0, 1, 'NNNN', 1),
  (237, 8, 0, 'KY', 0, 0, 0, 0, 1, '', 1),
  (238, 3, 0, 'CX', 0, 0, 0, 0, 1, '', 1),
  (239, 3, 0, 'CC', 0, 0, 0, 0, 1, '', 1),
  (240, 5, 0, 'CK', 682, 0, 0, 0, 1, '', 1),
  (241, 6, 0, 'GF', 594, 0, 0, 0, 1, '', 1),
  (242, 5, 0, 'PF', 689, 0, 0, 0, 1, '', 1),
  (243, 5, 0, 'TF', 0, 0, 0, 0, 1, '', 1),
  (244, 7, 0, 'AX', 0, 0, 0, 0, 1, 'NNNNN', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_country_lang`
--

CREATE TABLE IF NOT EXISTS `ps_country_lang` (
  `id_country` INT(10) UNSIGNED NOT NULL,
  `id_lang`    INT(10) UNSIGNED NOT NULL,
  `name`       VARCHAR(64)      NOT NULL,
  PRIMARY KEY (`id_country`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_country_lang`
--

INSERT INTO `ps_country_lang` (`id_country`, `id_lang`, `name`) VALUES
  (1, 1, 'Germany'),
  (2, 1, 'Austria'),
  (3, 1, 'Belgium'),
  (4, 1, 'Canada'),
  (5, 1, 'China'),
  (6, 1, 'Spain'),
  (7, 1, 'Finland'),
  (8, 1, 'France'),
  (9, 1, 'Greece'),
  (10, 1, 'Italy'),
  (11, 1, 'Japan'),
  (12, 1, 'Luxemburg'),
  (13, 1, 'Netherlands'),
  (14, 1, 'Poland'),
  (15, 1, 'Portugal'),
  (16, 1, 'Czech Republic'),
  (17, 1, 'United Kingdom'),
  (18, 1, 'Sweden'),
  (19, 1, 'Switzerland'),
  (20, 1, 'Denmark'),
  (21, 1, 'United States'),
  (22, 1, 'HongKong'),
  (23, 1, 'Norway'),
  (24, 1, 'Australia'),
  (25, 1, 'Singapore'),
  (26, 1, 'Ireland'),
  (27, 1, 'New Zealand'),
  (28, 1, 'South Korea'),
  (29, 1, 'Israel'),
  (30, 1, 'South Africa'),
  (31, 1, 'Nigeria'),
  (32, 1, 'Ivory Coast'),
  (33, 1, 'Togo'),
  (34, 1, 'Bolivia'),
  (35, 1, 'Mauritius'),
  (36, 1, 'Romania'),
  (37, 1, 'Slovakia'),
  (38, 1, 'Algeria'),
  (39, 1, 'American Samoa'),
  (40, 1, 'Andorra'),
  (41, 1, 'Angola'),
  (42, 1, 'Anguilla'),
  (43, 1, 'Antigua and Barbuda'),
  (44, 1, 'Argentina'),
  (45, 1, 'Armenia'),
  (46, 1, 'Aruba'),
  (47, 1, 'Azerbaijan'),
  (48, 1, 'Bahamas'),
  (49, 1, 'Bahrain'),
  (50, 1, 'Bangladesh'),
  (51, 1, 'Barbados'),
  (52, 1, 'Belarus'),
  (53, 1, 'Belize'),
  (54, 1, 'Benin'),
  (55, 1, 'Bermuda'),
  (56, 1, 'Bhutan'),
  (57, 1, 'Botswana'),
  (58, 1, 'Brazil'),
  (59, 1, 'Brunei'),
  (60, 1, 'Burkina Faso'),
  (61, 1, 'Burma (Myanmar)'),
  (62, 1, 'Burundi'),
  (63, 1, 'Cambodia'),
  (64, 1, 'Cameroon'),
  (65, 1, 'Cape Verde'),
  (66, 1, 'Central African Republic'),
  (67, 1, 'Chad'),
  (68, 1, 'Chile'),
  (69, 1, 'Colombia'),
  (70, 1, 'Comoros'),
  (71, 1, 'Congo, Dem. Republic'),
  (72, 1, 'Congo, Republic'),
  (73, 1, 'Costa Rica'),
  (74, 1, 'Croatia'),
  (75, 1, 'Cuba'),
  (76, 1, 'Cyprus'),
  (77, 1, 'Djibouti'),
  (78, 1, 'Dominica'),
  (79, 1, 'Dominican Republic'),
  (80, 1, 'East Timor'),
  (81, 1, 'Ecuador'),
  (82, 1, 'Egypt'),
  (83, 1, 'El Salvador'),
  (84, 1, 'Equatorial Guinea'),
  (85, 1, 'Eritrea'),
  (86, 1, 'Estonia'),
  (87, 1, 'Ethiopia'),
  (88, 1, 'Falkland Islands'),
  (89, 1, 'Faroe Islands'),
  (90, 1, 'Fiji'),
  (91, 1, 'Gabon'),
  (92, 1, 'Gambia'),
  (93, 1, 'Georgia'),
  (94, 1, 'Ghana'),
  (95, 1, 'Grenada'),
  (96, 1, 'Greenland'),
  (97, 1, 'Gibraltar'),
  (98, 1, 'Guadeloupe'),
  (99, 1, 'Guam'),
  (100, 1, 'Guatemala'),
  (101, 1, 'Guernsey'),
  (102, 1, 'Guinea'),
  (103, 1, 'Guinea-Bissau'),
  (104, 1, 'Guyana'),
  (105, 1, 'Haiti'),
  (106, 1, 'Heard Island and McDonald Islands'),
  (107, 1, 'Vatican City State'),
  (108, 1, 'Honduras'),
  (109, 1, 'Iceland'),
  (110, 1, 'India'),
  (111, 1, 'Indonesia'),
  (112, 1, 'Iran'),
  (113, 1, 'Iraq'),
  (114, 1, 'Man Island'),
  (115, 1, 'Jamaica'),
  (116, 1, 'Jersey'),
  (117, 1, 'Jordan'),
  (118, 1, 'Kazakhstan'),
  (119, 1, 'Kenya'),
  (120, 1, 'Kiribati'),
  (121, 1, 'Korea, Dem. Republic of'),
  (122, 1, 'Kuwait'),
  (123, 1, 'Kyrgyzstan'),
  (124, 1, 'Laos'),
  (125, 1, 'Latvia'),
  (126, 1, 'Lebanon'),
  (127, 1, 'Lesotho'),
  (128, 1, 'Liberia'),
  (129, 1, 'Libya'),
  (130, 1, 'Liechtenstein'),
  (131, 1, 'Lithuania'),
  (132, 1, 'Macau'),
  (133, 1, 'Macedonia'),
  (134, 1, 'Madagascar'),
  (135, 1, 'Malawi'),
  (136, 1, 'Malaysia'),
  (137, 1, 'Maldives'),
  (138, 1, 'Mali'),
  (139, 1, 'Malta'),
  (140, 1, 'Marshall Islands'),
  (141, 1, 'Martinique'),
  (142, 1, 'Mauritania'),
  (143, 1, 'Hungary'),
  (144, 1, 'Mayotte'),
  (145, 1, 'Mexico'),
  (146, 1, 'Micronesia'),
  (147, 1, 'Moldova'),
  (148, 1, 'Monaco'),
  (149, 1, 'Mongolia'),
  (150, 1, 'Montenegro'),
  (151, 1, 'Montserrat'),
  (152, 1, 'Morocco'),
  (153, 1, 'Mozambique'),
  (154, 1, 'Namibia'),
  (155, 1, 'Nauru'),
  (156, 1, 'Nepal'),
  (157, 1, 'Netherlands Antilles'),
  (158, 1, 'New Caledonia'),
  (159, 1, 'Nicaragua'),
  (160, 1, 'Niger'),
  (161, 1, 'Niue'),
  (162, 1, 'Norfolk Island'),
  (163, 1, 'Northern Mariana Islands'),
  (164, 1, 'Oman'),
  (165, 1, 'Pakistan'),
  (166, 1, 'Palau'),
  (167, 1, 'Palestinian Territories'),
  (168, 1, 'Panama'),
  (169, 1, 'Papua New Guinea'),
  (170, 1, 'Paraguay'),
  (171, 1, 'Peru'),
  (172, 1, 'Philippines'),
  (173, 1, 'Pitcairn'),
  (174, 1, 'Puerto Rico'),
  (175, 1, 'Qatar'),
  (176, 1, 'Reunion Island'),
  (177, 1, 'Russian Federation'),
  (178, 1, 'Rwanda'),
  (179, 1, 'Saint Barthelemy'),
  (180, 1, 'Saint Kitts and Nevis'),
  (181, 1, 'Saint Lucia'),
  (182, 1, 'Saint Martin'),
  (183, 1, 'Saint Pierre and Miquelon'),
  (184, 1, 'Saint Vincent and the Grenadines'),
  (185, 1, 'Samoa'),
  (186, 1, 'San Marino'),
  (187, 1, 'São Tomé and Príncipe'),
  (188, 1, 'Saudi Arabia'),
  (189, 1, 'Senegal'),
  (190, 1, 'Serbia'),
  (191, 1, 'Seychelles'),
  (192, 1, 'Sierra Leone'),
  (193, 1, 'Slovenia'),
  (194, 1, 'Solomon Islands'),
  (195, 1, 'Somalia'),
  (196, 1, 'South Georgia and the South Sandwich Islands'),
  (197, 1, 'Sri Lanka'),
  (198, 1, 'Sudan'),
  (199, 1, 'Suriname'),
  (200, 1, 'Svalbard and Jan Mayen'),
  (201, 1, 'Swaziland'),
  (202, 1, 'Syria'),
  (203, 1, 'Taiwan'),
  (204, 1, 'Tajikistan'),
  (205, 1, 'Tanzania'),
  (206, 1, 'Thailand'),
  (207, 1, 'Tokelau'),
  (208, 1, 'Tonga'),
  (209, 1, 'Trinidad and Tobago'),
  (210, 1, 'Tunisia'),
  (211, 1, 'Turkey'),
  (212, 1, 'Turkmenistan'),
  (213, 1, 'Turks and Caicos Islands'),
  (214, 1, 'Tuvalu'),
  (215, 1, 'Uganda'),
  (216, 1, 'Ukraine'),
  (217, 1, 'United Arab Emirates'),
  (218, 1, 'Uruguay'),
  (219, 1, 'Uzbekistan'),
  (220, 1, 'Vanuatu'),
  (221, 1, 'Venezuela'),
  (222, 1, 'Vietnam'),
  (223, 1, 'Virgin Islands (British)'),
  (224, 1, 'Virgin Islands (U.S.)'),
  (225, 1, 'Wallis and Futuna'),
  (226, 1, 'Western Sahara'),
  (227, 1, 'Yemen'),
  (228, 1, 'Zambia'),
  (229, 1, 'Zimbabwe'),
  (230, 1, 'Albania'),
  (231, 1, 'Afghanistan'),
  (232, 1, 'Antarctica'),
  (233, 1, 'Bosnia and Herzegovina'),
  (234, 1, 'Bouvet Island'),
  (235, 1, 'British Indian Ocean Territory'),
  (236, 1, 'Bulgaria'),
  (237, 1, 'Cayman Islands'),
  (238, 1, 'Christmas Island'),
  (239, 1, 'Cocos (Keeling) Islands'),
  (240, 1, 'Cook Islands'),
  (241, 1, 'French Guiana'),
  (242, 1, 'French Polynesia'),
  (243, 1, 'French Southern Territories'),
  (244, 1, 'Åland Islands');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_country_shop`
--

CREATE TABLE IF NOT EXISTS `ps_country_shop` (
  `id_country` INT(11) UNSIGNED NOT NULL,
  `id_shop`    INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_country`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_country_shop`
--

INSERT INTO `ps_country_shop` (`id_country`, `id_shop`) VALUES
  (1, 1),
  (2, 1),
  (3, 1),
  (4, 1),
  (5, 1),
  (6, 1),
  (7, 1),
  (8, 1),
  (9, 1),
  (10, 1),
  (11, 1),
  (12, 1),
  (13, 1),
  (14, 1),
  (15, 1),
  (16, 1),
  (17, 1),
  (18, 1),
  (19, 1),
  (20, 1),
  (21, 1),
  (22, 1),
  (23, 1),
  (24, 1),
  (25, 1),
  (26, 1),
  (27, 1),
  (28, 1),
  (29, 1),
  (30, 1),
  (31, 1),
  (32, 1),
  (33, 1),
  (34, 1),
  (35, 1),
  (36, 1),
  (37, 1),
  (38, 1),
  (39, 1),
  (40, 1),
  (41, 1),
  (42, 1),
  (43, 1),
  (44, 1),
  (45, 1),
  (46, 1),
  (47, 1),
  (48, 1),
  (49, 1),
  (50, 1),
  (51, 1),
  (52, 1),
  (53, 1),
  (54, 1),
  (55, 1),
  (56, 1),
  (57, 1),
  (58, 1),
  (59, 1),
  (60, 1),
  (61, 1),
  (62, 1),
  (63, 1),
  (64, 1),
  (65, 1),
  (66, 1),
  (67, 1),
  (68, 1),
  (69, 1),
  (70, 1),
  (71, 1),
  (72, 1),
  (73, 1),
  (74, 1),
  (75, 1),
  (76, 1),
  (77, 1),
  (78, 1),
  (79, 1),
  (80, 1),
  (81, 1),
  (82, 1),
  (83, 1),
  (84, 1),
  (85, 1),
  (86, 1),
  (87, 1),
  (88, 1),
  (89, 1),
  (90, 1),
  (91, 1),
  (92, 1),
  (93, 1),
  (94, 1),
  (95, 1),
  (96, 1),
  (97, 1),
  (98, 1),
  (99, 1),
  (100, 1),
  (101, 1),
  (102, 1),
  (103, 1),
  (104, 1),
  (105, 1),
  (106, 1),
  (107, 1),
  (108, 1),
  (109, 1),
  (110, 1),
  (111, 1),
  (112, 1),
  (113, 1),
  (114, 1),
  (115, 1),
  (116, 1),
  (117, 1),
  (118, 1),
  (119, 1),
  (120, 1),
  (121, 1),
  (122, 1),
  (123, 1),
  (124, 1),
  (125, 1),
  (126, 1),
  (127, 1),
  (128, 1),
  (129, 1),
  (130, 1),
  (131, 1),
  (132, 1),
  (133, 1),
  (134, 1),
  (135, 1),
  (136, 1),
  (137, 1),
  (138, 1),
  (139, 1),
  (140, 1),
  (141, 1),
  (142, 1),
  (143, 1),
  (144, 1),
  (145, 1),
  (146, 1),
  (147, 1),
  (148, 1),
  (149, 1),
  (150, 1),
  (151, 1),
  (152, 1),
  (153, 1),
  (154, 1),
  (155, 1),
  (156, 1),
  (157, 1),
  (158, 1),
  (159, 1),
  (160, 1),
  (161, 1),
  (162, 1),
  (163, 1),
  (164, 1),
  (165, 1),
  (166, 1),
  (167, 1),
  (168, 1),
  (169, 1),
  (170, 1),
  (171, 1),
  (172, 1),
  (173, 1),
  (174, 1),
  (175, 1),
  (176, 1),
  (177, 1),
  (178, 1),
  (179, 1),
  (180, 1),
  (181, 1),
  (182, 1),
  (183, 1),
  (184, 1),
  (185, 1),
  (186, 1),
  (187, 1),
  (188, 1),
  (189, 1),
  (190, 1),
  (191, 1),
  (192, 1),
  (193, 1),
  (194, 1),
  (195, 1),
  (196, 1),
  (197, 1),
  (198, 1),
  (199, 1),
  (200, 1),
  (201, 1),
  (202, 1),
  (203, 1),
  (204, 1),
  (205, 1),
  (206, 1),
  (207, 1),
  (208, 1),
  (209, 1),
  (210, 1),
  (211, 1),
  (212, 1),
  (213, 1),
  (214, 1),
  (215, 1),
  (216, 1),
  (217, 1),
  (218, 1),
  (219, 1),
  (220, 1),
  (221, 1),
  (222, 1),
  (223, 1),
  (224, 1),
  (225, 1),
  (226, 1),
  (227, 1),
  (228, 1),
  (229, 1),
  (230, 1),
  (231, 1),
  (232, 1),
  (233, 1),
  (234, 1),
  (235, 1),
  (236, 1),
  (237, 1),
  (238, 1),
  (239, 1),
  (240, 1),
  (241, 1),
  (242, 1),
  (243, 1),
  (244, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_cronjobs`
--

CREATE TABLE IF NOT EXISTS `ps_cronjobs` (
  `id_cronjob`    INT(10)    NOT NULL AUTO_INCREMENT,
  `id_module`     INT(10)             DEFAULT NULL,
  `description`   TEXT,
  `task`          TEXT,
  `hour`          INT(11)             DEFAULT '-1',
  `day`           INT(11)             DEFAULT '-1',
  `month`         INT(11)             DEFAULT '-1',
  `day_of_week`   INT(11)             DEFAULT '-1',
  `updated_at`    DATETIME            DEFAULT NULL,
  `one_shot`      TINYINT(1) NOT NULL DEFAULT '0',
  `active`        TINYINT(1)          DEFAULT '0',
  `id_shop`       INT(11)             DEFAULT '0',
  `id_shop_group` INT(11)             DEFAULT '0',
  PRIMARY KEY (`id_cronjob`),
  KEY `id_module` (`id_module`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_cronjobs`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_currency`
--

CREATE TABLE IF NOT EXISTS `ps_currency` (
  `id_currency`     INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `name`            VARCHAR(32)         NOT NULL,
  `iso_code`        VARCHAR(3)          NOT NULL DEFAULT '0',
  `iso_code_num`    VARCHAR(3)          NOT NULL DEFAULT '0',
  `sign`            VARCHAR(8)          NOT NULL,
  `blank`           TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `format`          TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `decimals`        TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `conversion_rate` DECIMAL(13, 6)      NOT NULL,
  `deleted`         TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `active`          TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_currency`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 3;

--
-- Vypisuji data pro tabulku `ps_currency`
--

INSERT INTO `ps_currency` (`id_currency`, `name`, `iso_code`, `iso_code_num`, `sign`, `blank`, `format`, `decimals`, `conversion_rate`, `deleted`, `active`)
VALUES
  (1, 'Czech koruna', 'CZK', '203', 'Kč', 1, 2, 1, 1.000000, 0, 1),
  (2, 'Euro', 'EUR', '978', '€', 1, 2, 1, 0.036989, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_currency_shop`
--

CREATE TABLE IF NOT EXISTS `ps_currency_shop` (
  `id_currency`     INT(11) UNSIGNED NOT NULL,
  `id_shop`         INT(11) UNSIGNED NOT NULL,
  `conversion_rate` DECIMAL(13, 6)   NOT NULL,
  PRIMARY KEY (`id_currency`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_currency_shop`
--

INSERT INTO `ps_currency_shop` (`id_currency`, `id_shop`, `conversion_rate`) VALUES
  (1, 1, 1.000000),
  (2, 1, 0.036989);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_customer`
--

CREATE TABLE IF NOT EXISTS `ps_customer` (
  `id_customer`                INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_shop_group`              INT(11) UNSIGNED    NOT NULL DEFAULT '1',
  `id_shop`                    INT(11) UNSIGNED    NOT NULL DEFAULT '1',
  `id_gender`                  INT(10) UNSIGNED    NOT NULL,
  `id_default_group`           INT(10) UNSIGNED    NOT NULL DEFAULT '1',
  `id_lang`                    INT(10) UNSIGNED             DEFAULT NULL,
  `id_risk`                    INT(10) UNSIGNED    NOT NULL DEFAULT '1',
  `company`                    VARCHAR(64)                  DEFAULT NULL,
  `siret`                      VARCHAR(14)                  DEFAULT NULL,
  `ape`                        VARCHAR(5)                   DEFAULT NULL,
  `firstname`                  VARCHAR(32)         NOT NULL,
  `lastname`                   VARCHAR(32)         NOT NULL,
  `email`                      VARCHAR(128)        NOT NULL,
  `passwd`                     VARCHAR(32)         NOT NULL,
  `last_passwd_gen`            TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `birthday`                   DATE                         DEFAULT NULL,
  `newsletter`                 TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `ip_registration_newsletter` VARCHAR(15)                  DEFAULT NULL,
  `newsletter_date_add`        DATETIME                     DEFAULT NULL,
  `optin`                      TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `website`                    VARCHAR(128)                 DEFAULT NULL,
  `outstanding_allow_amount`   DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `show_public_prices`         TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `max_payment_days`           INT(10) UNSIGNED    NOT NULL DEFAULT '60',
  `secure_key`                 VARCHAR(32)         NOT NULL DEFAULT '-1',
  `note`                       TEXT,
  `active`                     TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_guest`                   TINYINT(1)          NOT NULL DEFAULT '0',
  `deleted`                    TINYINT(1)          NOT NULL DEFAULT '0',
  `date_add`                   DATETIME            NOT NULL,
  `date_upd`                   DATETIME            NOT NULL,
  PRIMARY KEY (`id_customer`),
  KEY `customer_email` (`email`),
  KEY `customer_login` (`email`, `passwd`),
  KEY `id_customer_passwd` (`id_customer`, `passwd`),
  KEY `id_gender` (`id_gender`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `id_shop` (`id_shop`, `date_add`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_customer`
--

INSERT INTO `ps_customer` (`id_customer`, `id_shop_group`, `id_shop`, `id_gender`, `id_default_group`, `id_lang`, `id_risk`, `company`, `siret`, `ape`, `firstname`, `lastname`, `email`, `passwd`, `last_passwd_gen`, `birthday`, `newsletter`, `ip_registration_newsletter`, `newsletter_date_add`, `optin`, `website`, `outstanding_allow_amount`, `show_public_prices`, `max_payment_days`, `secure_key`, `note`, `active`, `is_guest`, `deleted`, `date_add`, `date_upd`)
VALUES
  (1, 1, 1, 1, 3, 1, 0, '', '', '', 'John', 'DOE', 'pub@prestashop.com', 'eb8b617d4a0ca739ee0bbf282c7ae102',
                                            '2016-08-04 06:42:31', '1970-01-15', 1, '', '2013-12-13 08:19:15', 1, '',
    0.000000, 0, 0, '2e04279cf59966b1553330bcc89b5605', '', 1, 0, 0, '2016-08-04 12:42:31', '2016-08-04 12:42:31');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_customer_group`
--

CREATE TABLE IF NOT EXISTS `ps_customer_group` (
  `id_customer` INT(10) UNSIGNED NOT NULL,
  `id_group`    INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_customer`, `id_group`),
  KEY `customer_login` (`id_group`),
  KEY `id_customer` (`id_customer`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_customer_group`
--

INSERT INTO `ps_customer_group` (`id_customer`, `id_group`) VALUES
  (1, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_customer_message`
--

CREATE TABLE IF NOT EXISTS `ps_customer_message` (
  `id_customer_message` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_customer_thread`  INT(11)                   DEFAULT NULL,
  `id_employee`         INT(10) UNSIGNED          DEFAULT NULL,
  `message`             MEDIUMTEXT       NOT NULL,
  `file_name`           VARCHAR(18)               DEFAULT NULL,
  `ip_address`          VARCHAR(16)               DEFAULT NULL,
  `user_agent`          VARCHAR(128)              DEFAULT NULL,
  `date_add`            DATETIME         NOT NULL,
  `date_upd`            DATETIME         NOT NULL,
  `private`             TINYINT(4)       NOT NULL DEFAULT '0',
  `read`                TINYINT(1)       NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_customer_message`),
  KEY `id_customer_thread` (`id_customer_thread`),
  KEY `id_employee` (`id_employee`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_customer_message`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_customer_message_sync_imap`
--

CREATE TABLE IF NOT EXISTS `ps_customer_message_sync_imap` (
  `md5_header` VARBINARY(32) NOT NULL,
  KEY `md5_header_index` (`md5_header`(4))
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_customer_message_sync_imap`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_customer_thread`
--

CREATE TABLE IF NOT EXISTS `ps_customer_thread` (
  `id_customer_thread` INT(11) UNSIGNED                                NOT NULL AUTO_INCREMENT,
  `id_shop`            INT(11) UNSIGNED                                NOT NULL DEFAULT '1',
  `id_lang`            INT(10) UNSIGNED                                NOT NULL,
  `id_contact`         INT(10) UNSIGNED                                NOT NULL,
  `id_customer`        INT(10) UNSIGNED                                         DEFAULT NULL,
  `id_order`           INT(10) UNSIGNED                                         DEFAULT NULL,
  `id_product`         INT(10) UNSIGNED                                         DEFAULT NULL,
  `status`             ENUM ('open', 'closed', 'pending1', 'pending2') NOT NULL DEFAULT 'open',
  `email`              VARCHAR(128)                                    NOT NULL,
  `token`              VARCHAR(12)                                              DEFAULT NULL,
  `date_add`           DATETIME                                        NOT NULL,
  `date_upd`           DATETIME                                        NOT NULL,
  PRIMARY KEY (`id_customer_thread`),
  KEY `id_shop` (`id_shop`),
  KEY `id_lang` (`id_lang`),
  KEY `id_contact` (`id_contact`),
  KEY `id_customer` (`id_customer`),
  KEY `id_order` (`id_order`),
  KEY `id_product` (`id_product`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_customer_thread`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_customer_tmsociallogin`
--

CREATE TABLE IF NOT EXISTS `ps_customer_tmsociallogin` (
  `id`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_customer` INT(10) UNSIGNED NOT NULL,
  `id_shop`     INT(11)          NOT NULL DEFAULT '1',
  `social_id`   VARCHAR(50)      NOT NULL,
  `social_type` VARCHAR(50)      NOT NULL,
  PRIMARY KEY (`id`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_customer_tmsociallogin`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_customization`
--

CREATE TABLE IF NOT EXISTS `ps_customization` (
  `id_customization`     INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_product_attribute` INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `id_address_delivery`  INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `id_cart`              INT(10) UNSIGNED    NOT NULL,
  `id_product`           INT(10)             NOT NULL,
  `quantity`             INT(10)             NOT NULL,
  `quantity_refunded`    INT(11)             NOT NULL DEFAULT '0',
  `quantity_returned`    INT(11)             NOT NULL DEFAULT '0',
  `in_cart`              TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_customization`, `id_cart`, `id_product`, `id_address_delivery`),
  KEY `id_product_attribute` (`id_product_attribute`),
  KEY `id_cart_product` (`id_cart`, `id_product`, `id_product_attribute`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_customization`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_customization_field`
--

CREATE TABLE IF NOT EXISTS `ps_customization_field` (
  `id_customization_field` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product`             INT(10) UNSIGNED NOT NULL,
  `type`                   TINYINT(1)       NOT NULL,
  `required`               TINYINT(1)       NOT NULL,
  PRIMARY KEY (`id_customization_field`),
  KEY `id_product` (`id_product`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_customization_field`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_customization_field_lang`
--

CREATE TABLE IF NOT EXISTS `ps_customization_field_lang` (
  `id_customization_field` INT(10) UNSIGNED NOT NULL,
  `id_lang`                INT(10) UNSIGNED NOT NULL,
  `id_shop`                INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `name`                   VARCHAR(255)     NOT NULL,
  PRIMARY KEY (`id_customization_field`, `id_lang`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_customization_field_lang`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_customized_data`
--

CREATE TABLE IF NOT EXISTS `ps_customized_data` (
  `id_customization` INT(10) UNSIGNED NOT NULL,
  `type`             TINYINT(1)       NOT NULL,
  `index`            INT(3)           NOT NULL,
  `value`            VARCHAR(255)     NOT NULL,
  PRIMARY KEY (`id_customization`, `type`, `index`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_customized_data`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_date_range`
--

CREATE TABLE IF NOT EXISTS `ps_date_range` (
  `id_date_range` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `time_start`    DATETIME         NOT NULL,
  `time_end`      DATETIME         NOT NULL,
  PRIMARY KEY (`id_date_range`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_date_range`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_delivery`
--

CREATE TABLE IF NOT EXISTS `ps_delivery` (
  `id_delivery`     INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop`         INT(10) UNSIGNED          DEFAULT NULL,
  `id_shop_group`   INT(10) UNSIGNED          DEFAULT NULL,
  `id_carrier`      INT(10) UNSIGNED NOT NULL,
  `id_range_price`  INT(10) UNSIGNED          DEFAULT NULL,
  `id_range_weight` INT(10) UNSIGNED          DEFAULT NULL,
  `id_zone`         INT(10) UNSIGNED NOT NULL,
  `price`           DECIMAL(20, 6)   NOT NULL,
  PRIMARY KEY (`id_delivery`),
  KEY `id_zone` (`id_zone`),
  KEY `id_carrier` (`id_carrier`, `id_zone`),
  KEY `id_range_price` (`id_range_price`),
  KEY `id_range_weight` (`id_range_weight`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 5;

--
-- Vypisuji data pro tabulku `ps_delivery`
--

INSERT INTO `ps_delivery` (`id_delivery`, `id_shop`, `id_shop_group`, `id_carrier`, `id_range_price`, `id_range_weight`, `id_zone`, `price`)
VALUES
  (1, NULL, NULL, 2, 0, 1, 1, 5.000000),
  (2, NULL, NULL, 2, 0, 1, 2, 5.000000),
  (3, NULL, NULL, 2, 1, 0, 1, 5.000000),
  (4, NULL, NULL, 2, 1, 0, 2, 5.000000);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_employee`
--

CREATE TABLE IF NOT EXISTS `ps_employee` (
  `id_employee`              INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_profile`               INT(10) UNSIGNED    NOT NULL,
  `id_lang`                  INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `lastname`                 VARCHAR(32)         NOT NULL,
  `firstname`                VARCHAR(32)         NOT NULL,
  `email`                    VARCHAR(128)        NOT NULL,
  `passwd`                   VARCHAR(32)         NOT NULL,
  `last_passwd_gen`          TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stats_date_from`          DATE                         DEFAULT NULL,
  `stats_date_to`            DATE                         DEFAULT NULL,
  `stats_compare_from`       DATE                         DEFAULT NULL,
  `stats_compare_to`         DATE                         DEFAULT NULL,
  `stats_compare_option`     INT(1) UNSIGNED     NOT NULL DEFAULT '1',
  `preselect_date_range`     VARCHAR(32)                  DEFAULT NULL,
  `bo_color`                 VARCHAR(32)                  DEFAULT NULL,
  `bo_theme`                 VARCHAR(32)                  DEFAULT NULL,
  `bo_css`                   VARCHAR(64)                  DEFAULT NULL,
  `default_tab`              INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `bo_width`                 INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `bo_menu`                  TINYINT(1)          NOT NULL DEFAULT '1',
  `active`                   TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `optin`                    TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `id_last_order`            INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `id_last_customer_message` INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `id_last_customer`         INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `last_connection_date`     DATE                         DEFAULT '0000-00-00',
  PRIMARY KEY (`id_employee`),
  KEY `employee_login` (`email`, `passwd`),
  KEY `id_employee_passwd` (`id_employee`, `passwd`),
  KEY `id_profile` (`id_profile`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 3;

--
-- Vypisuji data pro tabulku `ps_employee`
--

INSERT INTO `ps_employee` (`id_employee`, `id_profile`, `id_lang`, `lastname`, `firstname`, `email`, `passwd`, `last_passwd_gen`, `stats_date_from`, `stats_date_to`, `stats_compare_from`, `stats_compare_to`, `stats_compare_option`, `preselect_date_range`, `bo_color`, `bo_theme`, `bo_css`, `default_tab`, `bo_width`, `bo_menu`, `active`, `optin`, `id_last_order`, `id_last_customer_message`, `id_last_customer`, `last_connection_date`)
VALUES
  (1, 1, 1, 'Musil', 'Jan', 'stoupa.bat@seznam.cz', 'ad466937e1bae40cbd5ce5d5f74ff46d', '2016-08-04 06:42:29',
      '2017-02-01', '2017-02-21', '0000-00-00', '0000-00-00', 1, '', '', 'default', 'admin-theme.css', 1, 0, 0, 1, 1, 5,
   0, 1, '2017-02-22'),
  (2, 1, 1, 'Trávníčková', 'Adéla', 'travnickova@parexpo.cz', '3ae92c2dc4262bafc5712fc6184eec2c', '2016-10-11 08:46:34',
      '2017-01-01', '2017-01-25', '0000-00-00', '0000-00-00', 1, '', '', 'default', 'admin-theme.css', 1, 0, 1, 1, 1, 5,
   0, 1, '2017-01-25');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_employee_shop`
--

CREATE TABLE IF NOT EXISTS `ps_employee_shop` (
  `id_employee` INT(11) UNSIGNED NOT NULL,
  `id_shop`     INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_employee`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_employee_shop`
--

INSERT INTO `ps_employee_shop` (`id_employee`, `id_shop`) VALUES
  (1, 1),
  (2, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_feature`
--

CREATE TABLE IF NOT EXISTS `ps_feature` (
  `id_feature` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `position`   INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_feature`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 8;

--
-- Vypisuji data pro tabulku `ps_feature`
--

INSERT INTO `ps_feature` (`id_feature`, `position`) VALUES
  (1, 0),
  (2, 1),
  (3, 2),
  (4, 3),
  (6, 4),
  (7, 5);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_feature_lang`
--

CREATE TABLE IF NOT EXISTS `ps_feature_lang` (
  `id_feature` INT(10) UNSIGNED NOT NULL,
  `id_lang`    INT(10) UNSIGNED NOT NULL,
  `name`       VARCHAR(128) DEFAULT NULL,
  PRIMARY KEY (`id_feature`, `id_lang`),
  KEY `id_lang` (`id_lang`, `name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_feature_lang`
--

INSERT INTO `ps_feature_lang` (`id_feature`, `id_lang`, `name`) VALUES
  (3, 1, 'Hloubka'),
  (2, 1, 'Šířka'),
  (6, 1, 'Styl'),
  (4, 1, 'Váha'),
  (7, 1, 'Vlastnosti'),
  (1, 1, 'Výška');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_feature_product`
--

CREATE TABLE IF NOT EXISTS `ps_feature_product` (
  `id_feature`       INT(10) UNSIGNED NOT NULL,
  `id_product`       INT(10) UNSIGNED NOT NULL,
  `id_feature_value` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_feature`, `id_product`),
  KEY `id_feature_value` (`id_feature_value`),
  KEY `id_product` (`id_product`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_feature_product`
--

INSERT INTO `ps_feature_product` (`id_feature`, `id_product`, `id_feature_value`) VALUES
  (6, 1, 11),
  (6, 5, 11),
  (6, 3, 13),
  (6, 6, 13),
  (6, 7, 13),
  (6, 4, 16),
  (7, 1, 17),
  (7, 3, 18),
  (7, 4, 19),
  (7, 6, 19),
  (7, 7, 20),
  (7, 5, 21);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_feature_shop`
--

CREATE TABLE IF NOT EXISTS `ps_feature_shop` (
  `id_feature` INT(11) UNSIGNED NOT NULL,
  `id_shop`    INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_feature`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_feature_shop`
--

INSERT INTO `ps_feature_shop` (`id_feature`, `id_shop`) VALUES
  (1, 1),
  (2, 1),
  (3, 1),
  (4, 1),
  (6, 1),
  (7, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_feature_value`
--

CREATE TABLE IF NOT EXISTS `ps_feature_value` (
  `id_feature_value` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_feature`       INT(10) UNSIGNED NOT NULL,
  `custom`           TINYINT(3) UNSIGNED       DEFAULT NULL,
  PRIMARY KEY (`id_feature_value`),
  KEY `feature` (`id_feature`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 34;

--
-- Vypisuji data pro tabulku `ps_feature_value`
--

INSERT INTO `ps_feature_value` (`id_feature_value`, `id_feature`, `custom`) VALUES
  (10, 6, 0),
  (11, 6, 0),
  (12, 6, 0),
  (13, 6, 0),
  (14, 6, 0),
  (15, 6, 0),
  (16, 6, 0),
  (17, 7, 0),
  (18, 7, 0),
  (19, 7, 0),
  (20, 7, 0),
  (21, 7, 0),
  (22, 1, 1),
  (23, 2, 1),
  (24, 4, 1),
  (25, 3, 1),
  (26, 1, 1),
  (27, 2, 1),
  (28, 4, 1),
  (29, 3, 1),
  (30, 1, 1),
  (31, 2, 1),
  (32, 4, 1),
  (33, 3, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_feature_value_lang`
--

CREATE TABLE IF NOT EXISTS `ps_feature_value_lang` (
  `id_feature_value` INT(10) UNSIGNED NOT NULL,
  `id_lang`          INT(10) UNSIGNED NOT NULL,
  `value`            VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id_feature_value`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_feature_value_lang`
--

INSERT INTO `ps_feature_value_lang` (`id_feature_value`, `id_lang`, `value`) VALUES
  (10, 1, 'Classic'),
  (11, 1, 'Casual'),
  (12, 1, 'Military'),
  (13, 1, 'Girly'),
  (14, 1, 'Rock'),
  (15, 1, 'Basic'),
  (16, 1, 'Dressy'),
  (17, 1, 'Short Sleeve'),
  (18, 1, 'Colorful Dress'),
  (19, 1, 'Short Dress'),
  (20, 1, 'Midi Dress'),
  (21, 1, 'Maxi Dress'),
  (22, 1, '2.75 in'),
  (23, 1, '2.06 in'),
  (24, 1, '49.2 g'),
  (25, 1, '0.26 in'),
  (26, 1, '1.07 in'),
  (27, 1, '1.62 in'),
  (28, 1, '15.5 g'),
  (29, 1, '0.41 in (clip included)'),
  (30, 1, '4.33 in'),
  (31, 1, '2.76 in'),
  (32, 1, '120g'),
  (33, 1, '0.31 in');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_ganalytics`
--

CREATE TABLE IF NOT EXISTS `ps_ganalytics` (
  `id_google_analytics` INT(11) NOT NULL AUTO_INCREMENT,
  `id_order`            INT(11) NOT NULL,
  `id_customer`         INT(10) NOT NULL,
  `id_shop`             INT(11) NOT NULL,
  `sent`                TINYINT(1)       DEFAULT NULL,
  `date_add`            DATETIME         DEFAULT NULL,
  PRIMARY KEY (`id_google_analytics`),
  KEY `id_order` (`id_order`),
  KEY `sent` (`sent`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_ganalytics`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_gender`
--

CREATE TABLE IF NOT EXISTS `ps_gender` (
  `id_gender` INT(11)    NOT NULL AUTO_INCREMENT,
  `type`      TINYINT(1) NOT NULL,
  PRIMARY KEY (`id_gender`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 3;

--
-- Vypisuji data pro tabulku `ps_gender`
--

INSERT INTO `ps_gender` (`id_gender`, `type`) VALUES
  (1, 0),
  (2, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_gender_lang`
--

CREATE TABLE IF NOT EXISTS `ps_gender_lang` (
  `id_gender` INT(10) UNSIGNED NOT NULL,
  `id_lang`   INT(10) UNSIGNED NOT NULL,
  `name`      VARCHAR(20)      NOT NULL,
  PRIMARY KEY (`id_gender`, `id_lang`),
  KEY `id_gender` (`id_gender`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_gender_lang`
--

INSERT INTO `ps_gender_lang` (`id_gender`, `id_lang`, `name`) VALUES
  (1, 1, 'Pan'),
  (2, 1, 'Paní');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_group`
--

CREATE TABLE IF NOT EXISTS `ps_group` (
  `id_group`             INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `reduction`            DECIMAL(17, 2)      NOT NULL DEFAULT '0.00',
  `price_display_method` TINYINT(4)          NOT NULL DEFAULT '0',
  `show_prices`          TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `date_add`             DATETIME            NOT NULL,
  `date_upd`             DATETIME            NOT NULL,
  PRIMARY KEY (`id_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 4;

--
-- Vypisuji data pro tabulku `ps_group`
--

INSERT INTO `ps_group` (`id_group`, `reduction`, `price_display_method`, `show_prices`, `date_add`, `date_upd`) VALUES
  (1, 0.00, 0, 1, '2016-08-04 12:42:23', '2016-08-04 12:42:23'),
  (2, 0.00, 0, 1, '2016-08-04 12:42:23', '2016-08-04 12:42:23'),
  (3, 0.00, 0, 1, '2016-08-04 12:42:23', '2016-08-04 12:42:23');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_group_lang`
--

CREATE TABLE IF NOT EXISTS `ps_group_lang` (
  `id_group` INT(10) UNSIGNED NOT NULL,
  `id_lang`  INT(10) UNSIGNED NOT NULL,
  `name`     VARCHAR(32)      NOT NULL,
  PRIMARY KEY (`id_group`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_group_lang`
--

INSERT INTO `ps_group_lang` (`id_group`, `id_lang`, `name`) VALUES
  (1, 1, 'Visitor'),
  (2, 1, 'Guest'),
  (3, 1, 'Customer');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_group_reduction`
--

CREATE TABLE IF NOT EXISTS `ps_group_reduction` (
  `id_group_reduction` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_group`           INT(10) UNSIGNED      NOT NULL,
  `id_category`        INT(10) UNSIGNED      NOT NULL,
  `reduction`          DECIMAL(4, 3)         NOT NULL,
  PRIMARY KEY (`id_group_reduction`),
  UNIQUE KEY `id_group` (`id_group`, `id_category`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_group_reduction`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_group_shop`
--

CREATE TABLE IF NOT EXISTS `ps_group_shop` (
  `id_group` INT(11) UNSIGNED NOT NULL,
  `id_shop`  INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_group`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_group_shop`
--

INSERT INTO `ps_group_shop` (`id_group`, `id_shop`) VALUES
  (1, 1),
  (2, 1),
  (3, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_gsitemap_sitemap`
--

CREATE TABLE IF NOT EXISTS `ps_gsitemap_sitemap` (
  `link`    VARCHAR(255) DEFAULT NULL,
  `id_shop` INT(11)      DEFAULT '0'
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_gsitemap_sitemap`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_guest`
--

CREATE TABLE IF NOT EXISTS `ps_guest` (
  `id_guest`            INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operating_system` INT(10) UNSIGNED          DEFAULT NULL,
  `id_web_browser`      INT(10) UNSIGNED          DEFAULT NULL,
  `id_customer`         INT(10) UNSIGNED          DEFAULT NULL,
  `javascript`          TINYINT(1)                DEFAULT '0',
  `screen_resolution_x` SMALLINT(5) UNSIGNED      DEFAULT NULL,
  `screen_resolution_y` SMALLINT(5) UNSIGNED      DEFAULT NULL,
  `screen_color`        TINYINT(3) UNSIGNED       DEFAULT NULL,
  `sun_java`            TINYINT(1)                DEFAULT NULL,
  `adobe_flash`         TINYINT(1)                DEFAULT NULL,
  `adobe_director`      TINYINT(1)                DEFAULT NULL,
  `apple_quicktime`     TINYINT(1)                DEFAULT NULL,
  `real_player`         TINYINT(1)                DEFAULT NULL,
  `windows_media`       TINYINT(1)                DEFAULT NULL,
  `accept_language`     VARCHAR(8)                DEFAULT NULL,
  `mobile_theme`        TINYINT(1)       NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_guest`),
  KEY `id_customer` (`id_customer`),
  KEY `id_operating_system` (`id_operating_system`),
  KEY `id_web_browser` (`id_web_browser`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 265;

--
-- Vypisuji data pro tabulku `ps_guest`
--

INSERT INTO `ps_guest` (`id_guest`, `id_operating_system`, `id_web_browser`, `id_customer`, `javascript`, `screen_resolution_x`, `screen_resolution_y`, `screen_color`, `sun_java`, `adobe_flash`, `adobe_director`, `apple_quicktime`, `real_player`, `windows_media`, `accept_language`, `mobile_theme`)
VALUES
  (1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
  (2, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'cs', 0),
  (3, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (4, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'cs', 0),
  (5, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (6, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (7, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (8, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (9, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (10, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (11, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (12, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (13, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (14, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (15, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (16, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (17, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (18, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (19, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (20, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (21, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (22, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (23, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (24, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (25, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (26, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (27, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (28, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (29, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (30, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (31, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (32, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (33, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (34, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (35, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (36, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (37, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (38, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (39, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (40, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (41, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (42, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (43, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (44, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (45, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (46, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (47, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (48, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (49, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (50, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (51, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (52, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (53, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'cs', 0),
  (54, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (55, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (56, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (57, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (58, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (59, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (60, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (61, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (62, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (63, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (64, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (65, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (66, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (67, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (68, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (69, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (70, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
  (71, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'cs', 0),
  (72, 4, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
  (73, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (74, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'cs', 0),
  (75, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (76, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (77, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (78, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (79, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (80, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (81, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (82, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (83, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (84, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (85, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (86, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (87, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (88, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (89, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (90, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (91, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (92, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (93, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (94, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (95, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (96, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (97, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (98, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (99, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (100, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (101, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (102, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'cs', 0),
  (103, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (104, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (105, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (106, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (107, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (108, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (109, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (110, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (111, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (112, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (113, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (114, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (115, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (116, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (117, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (118, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (119, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (120, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (121, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (122, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (123, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (124, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (125, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (126, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (127, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (128, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (129, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (130, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (131, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (132, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (133, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (134, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (135, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (136, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (137, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (138, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (139, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (140, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (141, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (142, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (143, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (144, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'cs', 0),
  (145, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (146, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (147, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (148, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (149, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (150, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (151, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (152, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (153, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (154, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (155, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (156, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (157, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (158, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (159, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (160, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (161, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (162, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (163, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (164, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (165, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (166, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (167, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (168, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (169, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (170, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (171, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (172, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (173, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (174, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (175, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (176, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (177, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (178, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (179, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (180, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (181, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (182, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (183, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'cs', 0),
  (184, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (185, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (186, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
  (187, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (188, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'cs', 0),
  (189, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (190, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
  (191, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (192, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (193, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (194, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (195, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (196, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (197, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (198, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (199, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (200, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (201, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (202, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (203, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (204, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (205, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (206, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (207, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
  (208, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
  (209, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
  (210, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'cs', 0),
  (211, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (212, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (213, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (214, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (215, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (216, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (217, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (218, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (219, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (220, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (221, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (222, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (223, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (224, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (225, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (226, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (227, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (228, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (229, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (230, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (231, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (232, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (233, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (234, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (235, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (236, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (237, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (238, 6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (239, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (240, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (241, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (242, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (243, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (244, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (245, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (246, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (247, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (248, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (249, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (250, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (251, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (252, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (253, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (254, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (255, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (256, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (257, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (258, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (259, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'cs', 0),
  (260, 3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (261, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'cs-cz', 0),
  (262, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (263, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0),
  (264, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'fr', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_homeslider`
--

CREATE TABLE IF NOT EXISTS `ps_homeslider` (
  `id_homeslider_slides` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop`              INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_homeslider_slides`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 4;

--
-- Vypisuji data pro tabulku `ps_homeslider`
--

INSERT INTO `ps_homeslider` (`id_homeslider_slides`, `id_shop`) VALUES
  (1, 1),
  (2, 1),
  (3, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_homeslider_slides`
--

CREATE TABLE IF NOT EXISTS `ps_homeslider_slides` (
  `id_homeslider_slides` INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `position`             INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `active`               TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_homeslider_slides`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 4;

--
-- Vypisuji data pro tabulku `ps_homeslider_slides`
--

INSERT INTO `ps_homeslider_slides` (`id_homeslider_slides`, `position`, `active`) VALUES
  (1, 0, 1),
  (2, 0, 1),
  (3, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_homeslider_slides_lang`
--

CREATE TABLE IF NOT EXISTS `ps_homeslider_slides_lang` (
  `id_homeslider_slides` INT(10) UNSIGNED NOT NULL,
  `id_lang`              INT(10) UNSIGNED NOT NULL,
  `title`                VARCHAR(255)     NOT NULL,
  `description`          TEXT             NOT NULL,
  `legend`               VARCHAR(255)     NOT NULL,
  `url`                  VARCHAR(255)     NOT NULL,
  `image`                VARCHAR(255)     NOT NULL,
  PRIMARY KEY (`id_homeslider_slides`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_homeslider_slides_lang`
--

INSERT INTO `ps_homeslider_slides_lang` (`id_homeslider_slides`, `id_lang`, `title`, `description`, `legend`, `url`, `image`)
VALUES
  (1, 1, 'Okurky',
   '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique in tortor et dignissim. Quisque non tempor leo. Maecenas egestas sem elit</p>\n<h2>EXCEPTEUR<br />OCCAECAT</h2>\n<p><button class="btn btn-default" type="button">Shop now !</button></p>',
   'Okurky', 'http://cockytest.zcom.cz', '29db3728112b46f8dfcccbdf6176a52d63818bca_draw-big1.png'),
  (2, 1, 'Sniper',
   '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique in tortor et dignissim. Quisque non tempor leo. Maecenas egestas sem elit</p>\n<h2>EXCEPTEUR<br />OCCAECAT</h2>\n<p><button class="btn btn-default" type="button">Shop now !</button></p>',
   'Sniper', 'http://cockytest.zcom.cz', '7a5a75420953c0d6ccb89b56fb482645b9717ad9_draw-big2.png'),
  (3, 1, 'Kuk',
   '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique in tortor et dignissim. Quisque non tempor leo. Maecenas egestas sem elit</p>\n<h2>EXCEPTEUR<br />OCCAECAT</h2>\n<p><button class="btn btn-default" type="button">Shop now !</button></p>',
   'Kuk', 'http://cockytest.zcom.cz', 'de59e876932bc856a6e367042bb976ba78f0c8ba_draw-big3.png');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_hook`
--

CREATE TABLE IF NOT EXISTS `ps_hook` (
  `id_hook`     INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`        VARCHAR(64)      NOT NULL,
  `title`       VARCHAR(64)      NOT NULL,
  `description` TEXT,
  `position`    TINYINT(1)       NOT NULL DEFAULT '1',
  `live_edit`   TINYINT(1)       NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_hook`),
  UNIQUE KEY `hook_name` (`name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 192;

--
-- Vypisuji data pro tabulku `ps_hook`
--

INSERT INTO `ps_hook` (`id_hook`, `name`, `title`, `description`, `position`, `live_edit`) VALUES
  (1, 'displayPayment', 'Payment', 'This hook displays new elements on the payment page', 1, 1),
  (2, 'actionValidateOrder', 'New orders', '', 1, 0),
  (3, 'displayMaintenance', 'Maintenance Page', 'This hook displays new elements on the maintenance page', 1, 0),
  (4, 'actionPaymentConfirmation', 'Payment confirmation', 'This hook displays new elements after the payment is validated', 1, 0),
  (5, 'displayPaymentReturn', 'Payment return', '', 1, 0),
  (6, 'actionUpdateQuantity', 'Quantity update', 'Quantity is updated only when a customer effectively places their order', 1, 0),
  (7, 'displayRightColumn', 'Right column blocks', 'This hook displays new elements in the right-hand column', 1, 1),
  (8, 'displayLeftColumn', 'Left column blocks', 'This hook displays new elements in the left-hand column', 1, 1),
  (9, 'displayHome', 'Homepage content', 'This hook displays new elements on the homepage', 1, 1),
  (10, 'Header', 'Pages html head section', 'This hook adds additional elements in the head section of your pages (head section of html)', 1, 0),
  (11, 'actionCartSave', 'Cart creation and update', 'This hook is displayed when a product is added to the cart or if the cart''s content is modified', 1, 0),
  (12, 'actionAuthentication', 'Successful customer authentication', 'This hook is displayed after a customer successfully signs in', 1, 0),
  (13, 'actionProductAdd', 'Product creation', 'This hook is displayed after a product is created', 1, 0),
  (14, 'actionProductUpdate', 'Product update', 'This hook is displayed after a product has been updated', 1, 0),
  (15, 'displayTop', 'Top of pages', 'This hook displays additional elements at the top of your pages', 1, 0),
  (16, 'displayRightColumnProduct', 'New elements on the product page (right column)', 'This hook displays new elements in the right-hand column of the product page', 1, 0),
  (17, 'actionProductDelete', 'Product deletion', 'This hook is called when a product is deleted', 1, 0),
  (18, 'displayFooterProduct', 'Product footer', 'This hook adds new blocks under the product''s description', 1, 1),
  (19, 'displayInvoice', 'Invoice', 'This hook displays new blocks on the invoice (order)', 1, 0),
  (20, 'actionOrderStatusUpdate', 'Order status update - Event', 'This hook launches modules when the status of an order changes.', 1, 0),
  (21, 'displayAdminOrder', 'Display new elements in the Back Office, tab AdminOrder', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office', 1, 0),
  (22, 'displayAdminOrderTabOrder', 'Display new elements in Back Office, AdminOrder, panel Order', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Order panel tabs', 1, 0),
  (23, 'displayAdminOrderTabShip', 'Display new elements in Back Office, AdminOrder, panel Shipping', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Shipping panel tabs', 1, 0),
  (24, 'displayAdminOrderContentOrder', 'Display new elements in Back Office, AdminOrder, panel Order', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Order panel content', 1, 0),
  (25, 'displayAdminOrderContentShip', 'Display new elements in Back Office, AdminOrder, panel Shipping', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Shipping panel content', 1, 0),
  (26, 'displayFooter', 'Footer', 'This hook displays new blocks in the footer', 1, 0),
  (27, 'displayPDFInvoice', 'PDF Invoice', 'This hook allows you to display additional information on PDF invoices', 1, 0),
  (28, 'displayInvoiceLegalFreeText', 'PDF Invoice - Legal Free Text', 'This hook allows you to modify the legal free text on PDF invoices', 1, 0),
  (29, 'displayAdminCustomers', 'Display new elements in the Back Office, tab AdminCustomers', 'This hook launches modules when the AdminCustomers tab is displayed in the Back Office', 1, 0),
  (30, 'displayOrderConfirmation', 'Order confirmation page', 'This hook is called within an order''s confirmation page', 1, 0),
  (31, 'actionCustomerAccountAdd', 'Successful customer account creation', 'This hook is called when a new customer creates an account successfully', 1, 0),
  (32, 'displayCustomerAccount', 'Customer account displayed in Front Office', 'This hook displays new elements on the customer account page', 1, 0),
  (33, 'displayCustomerIdentityForm', 'Customer identity form displayed in Front Office', 'This hook displays new elements on the form to update a customer identity', 1, 0),
  (34, 'actionOrderSlipAdd', 'Order slip creation', 'This hook is called when a new credit slip is added regarding client order', 1, 0),
  (35, 'displayProductTab', 'Tabs on product page', 'This hook is called on the product page''s tab', 1, 0),
  (36, 'displayProductTabContent', 'Tabs content on the product page', 'This hook is called on the product page''s tab', 1, 0),
  (37, 'displayShoppingCartFooter', 'Shopping cart footer', 'This hook displays some specific information on the shopping cart''s page', 1, 0),
  (38, 'displayCustomerAccountForm', 'Customer account creation form', 'This hook displays some information on the form to create a customer account', 1, 0),
  (39, 'displayAdminStatsModules', 'Stats - Modules', '', 1, 0),
  (40, 'displayAdminStatsGraphEngine', 'Graph engines', '', 1, 0),
  (41, 'actionOrderReturn', 'Returned product', 'This hook is displayed when a customer returns a product ', 1, 0),
  (42, 'displayProductButtons', 'Product page actions', 'This hook adds new action buttons on the product page', 1, 0),
  (43, 'displayBackOfficeHome', 'Administration panel homepage', 'This hook is displayed on the admin panel''s homepage', 1, 0),
  (44, 'displayAdminStatsGridEngine', 'Grid engines', '', 1, 0),
  (45, 'actionWatermark', 'Watermark', '', 1, 0),
  (46, 'actionProductCancel', 'Product cancelled', 'This hook is called when you cancel a product in an order', 1, 0),
  (47, 'displayLeftColumnProduct', 'New elements on the product page (left column)', 'This hook displays new elements in the left-hand column of the product page', 1, 0),
  (48, 'actionProductOutOfStock', 'Out-of-stock product', 'This hook displays new action buttons if a product is out of stock', 1, 0),
  (49, 'actionProductAttributeUpdate', 'Product attribute update', 'This hook is displayed when a product''s attribute is updated', 1, 0),
  (50, 'displayCarrierList', 'Extra carrier (module mode)', '', 1, 0),
  (51, 'displayShoppingCart', 'Shopping cart - Additional button', 'This hook displays new action buttons within the shopping cart', 1, 0),
  (52, 'actionSearch', 'Search', '', 1, 0),
  (53, 'displayBeforePayment', 'Redirect during the order process', 'This hook redirects the user to the module instead of displaying payment modules', 1, 0),
  (54, 'actionCarrierUpdate', 'Carrier Update', 'This hook is called when a carrier is updated', 1, 0),
  (55, 'actionOrderStatusPostUpdate', 'Post update of order status', '', 1, 0),
  (56, 'displayCustomerAccountFormTop', 'Block above the form for create an account', 'This hook is displayed above the customer''s account creation form', 1, 0),
  (57, 'displayBackOfficeHeader', 'Administration panel header', 'This hook is displayed in the header of the admin panel', 1, 0),
  (58, 'displayBackOfficeTop', 'Administration panel hover the tabs', 'This hook is displayed on the roll hover of the tabs within the admin panel', 1, 0),
  (59, 'displayBackOfficeFooter', 'Administration panel footer', 'This hook is displayed within the admin panel''s footer', 1, 0),
  (60, 'actionProductAttributeDelete', 'Product attribute deletion', 'This hook is displayed when a product''s attribute is deleted', 1, 0),
  (61, 'actionCarrierProcess', 'Carrier process', '', 1, 0),
  (62, 'actionOrderDetail', 'Order detail', 'This hook is used to set the follow-up in Smarty when an order''s detail is called', 1, 0),
  (63, 'displayBeforeCarrier', 'Before carriers list', 'This hook is displayed before the carrier list in Front Office', 1, 0),
  (64, 'displayOrderDetail', 'Order detail', 'This hook is displayed within the order''s details in Front Office', 1, 0),
  (65, 'actionPaymentCCAdd', 'Payment CC added', '', 1, 0),
  (66, 'displayProductComparison', 'Extra product comparison', '', 1, 0),
  (67, 'actionCategoryAdd', 'Category creation', 'This hook is displayed when a category is created', 1, 0),
  (68, 'actionCategoryUpdate', 'Category modification', 'This hook is displayed when a category is modified', 1, 0),
  (69, 'actionCategoryDelete', 'Category deletion', 'This hook is displayed when a category is deleted', 1, 0),
  (70, 'actionBeforeAuthentication', 'Before authentication', 'This hook is displayed before the customer''s authentication', 1, 0),
  (71, 'displayPaymentTop', 'Top of payment page', 'This hook is displayed at the top of the payment page', 1, 0),
  (72, 'actionHtaccessCreate', 'After htaccess creation', 'This hook is displayed after the htaccess creation', 1, 0),
  (73, 'actionAdminMetaSave', 'After saving the configuration in AdminMeta', 'This hook is displayed after saving the configuration in AdminMeta', 1, 0),
  (74, 'displayAttributeGroupForm', 'Add fields to the form ''attribute group''', 'This hook adds fields to the form ''attribute group''', 1, 0),
  (75, 'actionAttributeGroupSave', 'Saving an attribute group', 'This hook is called while saving an attributes group', 1, 0),
  (76, 'actionAttributeGroupDelete', 'Deleting attribute group', 'This hook is called while deleting an attributes  group', 1, 0),
  (77, 'displayFeatureForm', 'Add fields to the form ''feature''', 'This hook adds fields to the form ''feature''', 1, 0),
  (78, 'actionFeatureSave', 'Saving attributes'' features', 'This hook is called while saving an attributes features', 1, 0),
  (79, 'actionFeatureDelete', 'Deleting attributes'' features', 'This hook is called while deleting an attributes features', 1, 0),
  (80, 'actionProductSave', 'Saving products', 'This hook is called while saving products', 1, 0),
  (81, 'actionProductListOverride', 'Assign a products list to a category', 'This hook assigns a products list to a category', 1, 0),
  (82, 'displayAttributeGroupPostProcess', 'On post-process in admin attribute group', 'This hook is called on post-process in admin attribute group', 1, 0),
  (83, 'displayFeaturePostProcess', 'On post-process in admin feature', 'This hook is called on post-process in admin feature', 1, 0),
  (84, 'displayFeatureValueForm', 'Add fields to the form ''feature value''', 'This hook adds fields to the form ''feature value''', 1, 0),
  (85, 'displayFeatureValuePostProcess', 'On post-process in admin feature value', 'This hook is called on post-process in admin feature value', 1, 0),
  (86, 'actionFeatureValueDelete', 'Deleting attributes'' features'' values', 'This hook is called while deleting an attributes features value', 1, 0),
  (87, 'actionFeatureValueSave', 'Saving an attributes features value', 'This hook is called while saving an attributes features value', 1, 0),
  (88, 'displayAttributeForm', 'Add fields to the form ''attribute value''', 'This hook adds fields to the form ''attribute value''', 1, 0),
  (89, 'actionAttributePostProcess', 'On post-process in admin feature value', 'This hook is called on post-process in admin feature value', 1, 0),
  (90, 'actionAttributeDelete', 'Deleting an attributes features value', 'This hook is called while deleting an attributes features value', 1, 0),
  (91, 'actionAttributeSave', 'Saving an attributes features value', 'This hook is called while saving an attributes features value', 1, 0),
  (92, 'actionTaxManager', 'Tax Manager Factory', '', 1, 0),
  (93, 'displayMyAccountBlock', 'My account block', 'This hook displays extra information within the ''my account'' block"', 1, 0),
  (94, 'actionModuleInstallBefore', 'actionModuleInstallBefore', '', 1, 0),
  (95, 'actionModuleInstallAfter', 'actionModuleInstallAfter', '', 1, 0),
  (96, 'displayHomeTab', 'Home Page Tabs', 'This hook displays new elements on the homepage tabs', 1, 1),
  (97, 'displayHomeTabContent', 'Home Page Tabs Content', 'This hook displays new elements on the homepage tabs content', 1, 1),
  (98, 'displayTopColumn', 'Top column blocks', 'This hook displays new elements in the top of columns', 1, 1),
  (99, 'displayBackOfficeCategory', 'Display new elements in the Back Office, tab AdminCategories', 'This hook launches modules when the AdminCategories tab is displayed in the Back Office', 1, 0),
  (100, 'displayProductListFunctionalButtons', 'Display new elements in the Front Office, products list', 'This hook launches modules when the products list is displayed in the Front Office', 1, 0),
  (101, 'displayNav', 'Navigation', '', 1, 1),
  (102, 'displayOverrideTemplate', 'Change the default template of current controller', '', 1, 0),
  (103, 'actionAdminLoginControllerSetMedia', 'Set media on admin login page header', 'This hook is called after adding media to admin login page header', 1, 0),
  (104, 'actionOrderEdited', 'Order edited', 'This hook is called when an order is edited.', 1, 0),
  (105, 'actionEmailAddBeforeContent', 'Add extra content before mail content', 'This hook is called just before fetching mail template', 1, 0),
  (106, 'actionEmailAddAfterContent', 'Add extra content after mail content', 'This hook is called just after fetching mail template', 1, 0),
  (107, 'actionObjectProductUpdateAfter', 'actionObjectProductUpdateAfter', '', 0, 0),
  (108, 'actionObjectProductDeleteAfter', 'actionObjectProductDeleteAfter', '', 0, 0),
  (109, 'displayCompareExtraInformation', 'displayCompareExtraInformation', '', 1, 1),
  (110, 'displaySocialSharing', 'displaySocialSharing', '', 1, 1),
  (111, 'displayBanner', 'displayBanner', '', 1, 1),
  (112, 'actionObjectLanguageAddAfter', 'actionObjectLanguageAddAfter', '', 0, 0),
  (113, 'displayPaymentEU', 'displayPaymentEU', '', 1, 1),
  (114, 'actionCartListOverride', 'actionCartListOverride', '', 0, 0),
  (115, 'actionAdminMetaControllerUpdate_optionsBefore', 'actionAdminMetaControllerUpdate_optionsBefore', '', 0, 0),
  (116, 'actionAdminLanguagesControllerStatusBefore', 'actionAdminLanguagesControllerStatusBefore', '', 0, 0),
  (117, 'actionObjectCmsUpdateAfter', 'actionObjectCmsUpdateAfter', '', 0, 0),
  (118, 'actionObjectCmsDeleteAfter', 'actionObjectCmsDeleteAfter', '', 0, 0),
  (119, 'actionShopDataDuplication', 'actionShopDataDuplication', '', 0, 0),
  (120, 'actionAdminStoresControllerUpdate_optionsAfter', 'actionAdminStoresControllerUpdate_optionsAfter', '', 0, 0),
  (121, 'actionObjectManufacturerDeleteAfter', 'actionObjectManufacturerDeleteAfter', '', 0, 0),
  (122, 'actionObjectManufacturerAddAfter', 'actionObjectManufacturerAddAfter', '', 0, 0),
  (123, 'actionObjectManufacturerUpdateAfter', 'actionObjectManufacturerUpdateAfter', '', 0, 0),
  (125, 'actionModuleRegisterHookAfter', 'actionModuleRegisterHookAfter', '', 0, 0),
  (126, 'actionModuleUnRegisterHookAfter', 'actionModuleUnRegisterHookAfter', '', 0, 0),
  (127, 'displayMyAccountBlockfooter', 'My account block', 'Display extra informations inside the "my account" block', 1, 0),
  (128, 'displayMobileTopSiteMap', 'displayMobileTopSiteMap', '', 1, 1),
  (129, 'displaySearch', 'displaySearch', '', 1, 1),
  (130, 'actionObjectSupplierDeleteAfter', 'actionObjectSupplierDeleteAfter', '', 0, 0),
  (131, 'actionObjectSupplierAddAfter', 'actionObjectSupplierAddAfter', '', 0, 0),
  (132, 'actionObjectSupplierUpdateAfter', 'actionObjectSupplierUpdateAfter', '', 0, 0),
  (133, 'actionObjectCategoryUpdateAfter', 'actionObjectCategoryUpdateAfter', '', 0, 0),
  (134, 'actionObjectCategoryDeleteAfter', 'actionObjectCategoryDeleteAfter', '', 0, 0),
  (135, 'actionObjectCategoryAddAfter', 'actionObjectCategoryAddAfter', '', 0, 0),
  (136, 'actionObjectCmsAddAfter', 'actionObjectCmsAddAfter', '', 0, 0),
  (137, 'actionObjectProductAddAfter', 'actionObjectProductAddAfter', '', 0, 0),
  (138, 'dashboardZoneOne', 'dashboardZoneOne', '', 0, 0),
  (139, 'dashboardData', 'dashboardData', '', 0, 0),
  (140, 'actionObjectOrderAddAfter', 'actionObjectOrderAddAfter', '', 0, 0),
  (141, 'actionObjectCustomerAddAfter', 'actionObjectCustomerAddAfter', '', 0, 0),
  (142, 'actionObjectCustomerMessageAddAfter', 'actionObjectCustomerMessageAddAfter', '', 0, 0),
  (143, 'actionObjectCustomerThreadAddAfter', 'actionObjectCustomerThreadAddAfter', '', 0, 0),
  (144, 'actionObjectOrderReturnAddAfter', 'actionObjectOrderReturnAddAfter', '', 0, 0),
  (145, 'actionAdminControllerSetMedia', 'actionAdminControllerSetMedia', '', 0, 0),
  (146, 'dashboardZoneTwo', 'dashboardZoneTwo', '', 0, 0),
  (147, 'actionAdminMetaControllerUpdate_optionsAfter', 'actionAdminMetaControllerUpdate_optionsAfter', '', 0, 0),
  (148, 'actionAdminPerformanceControllerSaveAfter', 'actionAdminPerformanceControllerSaveAfter', '', 0, 0),
  (149, 'actionObjectCarrierAddAfter', 'actionObjectCarrierAddAfter', '', 0, 0),
  (150, 'actionObjectContactAddAfter', 'actionObjectContactAddAfter', '', 0, 0),
  (151, 'actionAdminThemesControllerUpdate_optionsAfter', 'actionAdminThemesControllerUpdate_optionsAfter', '', 0, 0),
  (152, 'actionObjectShopUpdateAfter', 'actionObjectShopUpdateAfter', '', 0, 0),
  (153, 'actionAdminPreferencesControllerUpdate_optionsAfter', 'actionAdminPreferencesControllerUpdate_optionsAfter', '', 0, 0),
  (154, 'actionObjectShopAddAfter', 'actionObjectShopAddAfter', '', 0, 0),
  (155, 'actionObjectShopGroupAddAfter', 'actionObjectShopGroupAddAfter', '', 0, 0),
  (156, 'actionObjectCartAddAfter', 'actionObjectCartAddAfter', '', 0, 0),
  (157, 'actionObjectEmployeeAddAfter', 'actionObjectEmployeeAddAfter', '', 0, 0),
  (158, 'actionObjectImageAddAfter', 'actionObjectImageAddAfter', '', 0, 0),
  (159, 'actionObjectCartRuleAddAfter', 'actionObjectCartRuleAddAfter', '', 0, 0),
  (160, 'actionAdminStoresControllerSaveAfter', 'actionAdminStoresControllerSaveAfter', '', 0, 0),
  (161, 'actionAdminWebserviceControllerSaveAfter', 'actionAdminWebserviceControllerSaveAfter', '', 0, 0),
  (162, 'displaySmartBlogLeft', 'displaySmartBlogLeft', 'This is blog page left column', 1, 0),
  (163, 'displaySmartBlogRight', 'displaySmartBlogRight', 'This is blog page Right column', 1, 0),
  (164, 'displaySmartBeforePost', 'displaySmartBeforePost', 'This is blog Single page before blog post', 1, 0),
  (165, 'displaySmartAfterPost', 'displaySmartAfterPost', 'This is blog Single page after blog post', 1, 0),
  (166, 'actionsbnewpost', 'actionsbnewpost', 'this is action hook.', 1, 0),
  (167, 'actionsbupdatepost', 'actionsbupdatepost', 'this is action hook.', 1, 0),
  (168, 'actionsbdeletepost', 'actionsbdeletepost', 'this is action hook.', 1, 0),
  (169, 'actionsbtogglepost', 'actionsbtogglepost', 'this is action hook.', 1, 0),
  (170, 'actionsbnewcat', 'actionsbnewcat', 'this is action hook.', 1, 0),
  (171, 'actionsbupdatecat', 'actionsbupdatecat', 'this is action hook.', 1, 0),
  (172, 'actionsbdeletecat', 'actionsbdeletecat', 'this is action hook.', 1, 0),
  (173, 'actionsbtogglecat', 'actionsbtogglecat', 'this is action hook.', 1, 0),
  (174, 'actionsbpostcomment', 'actionsbpostcomment', 'this is action hook.', 1, 0),
  (175, 'actionsbappcomment', 'actionsbappcomment', 'this is action hook.', 1, 0),
  (176, 'actionsbsingle', 'actionsbsingle', 'this is action hook.', 1, 0),
  (177, 'actionsbcat', 'actionsbcat', 'this is action hook.', 1, 0),
  (178, 'actionsbsearch', 'actionsbsearch', 'this is action hook.', 1, 0),
  (179, 'actionsbheader', 'actionsbheader', 'this is action hook.', 1, 0),
  (180, 'moduleRoutes', 'moduleRoutes', '', 0, 0),
  (181, 'displayHeaderLoginButtons', 'displayHeaderLoginButtons', '', 1, 1),
  (182, 'displaySocialLoginButtons', 'displaySocialLoginButtons', '', 1, 1),
  (183, 'displayAdminProductsExtra', 'displayAdminProductsExtra', '', 1, 1),
  (184, 'displayProductVideoTab', 'displayProductVideoTab', '', 1, 1),
  (185, 'displayProductVideoTabContent', 'displayProductVideoTabContent', '', 1, 1),
  (186, 'displayProductListImages', 'displayProductListImages', '', 1, 1),
  (187, 'tmMegaLayoutTopColumn', 'tmMegaLayoutTopColumn', '', 0, 0),
  (188, 'tmMegaLayoutHome', 'tmMegaLayoutHome', '', 0, 0),
  (189, 'tmMegaLayoutFooter', 'tmMegaLayoutFooter', '', 0, 0),
  (190, 'gSitemapAppendUrls', 'GSitemap Append URLs', 'This hook allows a module to add URLs to a generated sitemap', 1,
   0),
  (191, 'actionProductCoverage', 'actionProductCoverage', '', 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_hook_alias`
--

CREATE TABLE IF NOT EXISTS `ps_hook_alias` (
  `id_hook_alias` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `alias`         VARCHAR(64)      NOT NULL,
  `name`          VARCHAR(64)      NOT NULL,
  PRIMARY KEY (`id_hook_alias`),
  UNIQUE KEY `alias` (`alias`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 87;

--
-- Vypisuji data pro tabulku `ps_hook_alias`
--

INSERT INTO `ps_hook_alias` (`id_hook_alias`, `alias`, `name`) VALUES
  (1, 'payment', 'displayPayment'),
  (2, 'newOrder', 'actionValidateOrder'),
  (3, 'paymentConfirm', 'actionPaymentConfirmation'),
  (4, 'paymentReturn', 'displayPaymentReturn'),
  (5, 'updateQuantity', 'actionUpdateQuantity'),
  (6, 'rightColumn', 'displayRightColumn'),
  (7, 'leftColumn', 'displayLeftColumn'),
  (8, 'home', 'displayHome'),
  (9, 'displayHeader', 'Header'),
  (10, 'cart', 'actionCartSave'),
  (11, 'authentication', 'actionAuthentication'),
  (12, 'addproduct', 'actionProductAdd'),
  (13, 'updateproduct', 'actionProductUpdate'),
  (14, 'top', 'displayTop'),
  (15, 'extraRight', 'displayRightColumnProduct'),
  (16, 'deleteproduct', 'actionProductDelete'),
  (17, 'productfooter', 'displayFooterProduct'),
  (18, 'invoice', 'displayInvoice'),
  (19, 'updateOrderStatus', 'actionOrderStatusUpdate'),
  (20, 'adminOrder', 'displayAdminOrder'),
  (21, 'footer', 'displayFooter'),
  (22, 'PDFInvoice', 'displayPDFInvoice'),
  (23, 'adminCustomers', 'displayAdminCustomers'),
  (24, 'orderConfirmation', 'displayOrderConfirmation'),
  (25, 'createAccount', 'actionCustomerAccountAdd'),
  (26, 'customerAccount', 'displayCustomerAccount'),
  (27, 'orderSlip', 'actionOrderSlipAdd'),
  (28, 'productTab', 'displayProductTab'),
  (29, 'productTabContent', 'displayProductTabContent'),
  (30, 'shoppingCart', 'displayShoppingCartFooter'),
  (31, 'createAccountForm', 'displayCustomerAccountForm'),
  (32, 'AdminStatsModules', 'displayAdminStatsModules'),
  (33, 'GraphEngine', 'displayAdminStatsGraphEngine'),
  (34, 'orderReturn', 'actionOrderReturn'),
  (35, 'productActions', 'displayProductButtons'),
  (36, 'backOfficeHome', 'displayBackOfficeHome'),
  (37, 'GridEngine', 'displayAdminStatsGridEngine'),
  (38, 'watermark', 'actionWatermark'),
  (39, 'cancelProduct', 'actionProductCancel'),
  (40, 'extraLeft', 'displayLeftColumnProduct'),
  (41, 'productOutOfStock', 'actionProductOutOfStock'),
  (42, 'updateProductAttribute', 'actionProductAttributeUpdate'),
  (43, 'extraCarrier', 'displayCarrierList'),
  (44, 'shoppingCartExtra', 'displayShoppingCart'),
  (45, 'search', 'actionSearch'),
  (46, 'backBeforePayment', 'displayBeforePayment'),
  (47, 'updateCarrier', 'actionCarrierUpdate'),
  (48, 'postUpdateOrderStatus', 'actionOrderStatusPostUpdate'),
  (49, 'createAccountTop', 'displayCustomerAccountFormTop'),
  (50, 'backOfficeHeader', 'displayBackOfficeHeader'),
  (51, 'backOfficeTop', 'displayBackOfficeTop'),
  (52, 'backOfficeFooter', 'displayBackOfficeFooter'),
  (53, 'deleteProductAttribute', 'actionProductAttributeDelete'),
  (54, 'processCarrier', 'actionCarrierProcess'),
  (55, 'orderDetail', 'actionOrderDetail'),
  (56, 'beforeCarrier', 'displayBeforeCarrier'),
  (57, 'orderDetailDisplayed', 'displayOrderDetail'),
  (58, 'paymentCCAdded', 'actionPaymentCCAdd'),
  (59, 'extraProductComparison', 'displayProductComparison'),
  (60, 'categoryAddition', 'actionCategoryAdd'),
  (61, 'categoryUpdate', 'actionCategoryUpdate'),
  (62, 'categoryDeletion', 'actionCategoryDelete'),
  (63, 'beforeAuthentication', 'actionBeforeAuthentication'),
  (64, 'paymentTop', 'displayPaymentTop'),
  (65, 'afterCreateHtaccess', 'actionHtaccessCreate'),
  (66, 'afterSaveAdminMeta', 'actionAdminMetaSave'),
  (67, 'attributeGroupForm', 'displayAttributeGroupForm'),
  (68, 'afterSaveAttributeGroup', 'actionAttributeGroupSave'),
  (69, 'afterDeleteAttributeGroup', 'actionAttributeGroupDelete'),
  (70, 'featureForm', 'displayFeatureForm'),
  (71, 'afterSaveFeature', 'actionFeatureSave'),
  (72, 'afterDeleteFeature', 'actionFeatureDelete'),
  (73, 'afterSaveProduct', 'actionProductSave'),
  (74, 'productListAssign', 'actionProductListOverride'),
  (75, 'postProcessAttributeGroup', 'displayAttributeGroupPostProcess'),
  (76, 'postProcessFeature', 'displayFeaturePostProcess'),
  (77, 'featureValueForm', 'displayFeatureValueForm'),
  (78, 'postProcessFeatureValue', 'displayFeatureValuePostProcess'),
  (79, 'afterDeleteFeatureValue', 'actionFeatureValueDelete'),
  (80, 'afterSaveFeatureValue', 'actionFeatureValueSave'),
  (81, 'attributeForm', 'displayAttributeForm'),
  (82, 'postProcessAttribute', 'actionAttributePostProcess'),
  (83, 'afterDeleteAttribute', 'actionAttributeDelete'),
  (84, 'afterSaveAttribute', 'actionAttributeSave'),
  (85, 'taxManager', 'actionTaxManager'),
  (86, 'myAccountBlock', 'displayMyAccountBlock');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_hook_module`
--

CREATE TABLE IF NOT EXISTS `ps_hook_module` (
  `id_module` INT(10) UNSIGNED    NOT NULL,
  `id_shop`   INT(11) UNSIGNED    NOT NULL DEFAULT '1',
  `id_hook`   INT(10) UNSIGNED    NOT NULL,
  `position`  TINYINT(2) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_module`, `id_hook`, `id_shop`),
  KEY `id_hook` (`id_hook`),
  KEY `id_module` (`id_module`),
  KEY `position` (`id_shop`, `position`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_hook_module`
--

INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES
  (1, 1, 10, 1),
  (1, 1, 16, 1),
  (1, 1, 107, 1),
  (1, 1, 108, 1),
  (1, 1, 109, 1),
  (1, 1, 110, 1),
  (2, 1, 111, 1),
  (2, 1, 112, 1),
  (3, 1, 1, 1),
  (3, 1, 5, 1),
  (3, 1, 113, 1),
  (4, 1, 13, 1),
  (4, 1, 14, 1),
  (4, 1, 17, 1),
  (4, 1, 55, 1),
  (5, 1, 114, 1),
  (7, 1, 8, 1),
  (7, 1, 67, 1),
  (7, 1, 68, 1),
  (7, 1, 69, 1),
  (7, 1, 99, 1),
  (7, 1, 115, 1),
  (7, 1, 116, 1),
  (11, 1, 74, 1),
  (11, 1, 75, 1),
  (11, 1, 76, 1),
  (11, 1, 77, 1),
  (11, 1, 78, 1),
  (11, 1, 79, 1),
  (11, 1, 80, 1),
  (11, 1, 81, 1),
  (11, 1, 82, 1),
  (11, 1, 83, 1),
  (11, 1, 84, 1),
  (11, 1, 85, 1),
  (11, 1, 86, 1),
  (11, 1, 87, 1),
  (11, 1, 88, 1),
  (11, 1, 89, 1),
  (11, 1, 90, 1),
  (11, 1, 91, 1),
  (12, 1, 117, 1),
  (12, 1, 118, 1),
  (12, 1, 119, 1),
  (12, 1, 120, 1),
  (16, 1, 121, 1),
  (16, 1, 122, 1),
  (16, 1, 123, 1),
  (17, 1, 125, 1),
  (18, 1, 126, 1),
  (20, 1, 26, 1),
  (20, 1, 31, 1),
  (22, 1, 15, 1),
  (22, 1, 128, 1),
  (22, 1, 129, 1),
  (25, 1, 130, 1),
  (25, 1, 131, 1),
  (25, 1, 132, 1),
  (27, 1, 133, 1),
  (27, 1, 134, 1),
  (27, 1, 135, 1),
  (27, 1, 136, 1),
  (27, 1, 137, 1),
  (28, 1, 101, 1),
  (31, 1, 138, 1),
  (31, 1, 139, 1),
  (31, 1, 140, 1),
  (31, 1, 141, 1),
  (31, 1, 142, 1),
  (31, 1, 143, 1),
  (31, 1, 144, 1),
  (31, 1, 145, 1),
  (32, 1, 146, 1),
  (35, 1, 40, 1),
  (36, 1, 44, 1),
  (37, 1, 98, 1),
  (38, 1, 96, 1),
  (38, 1, 97, 1),
  (40, 1, 39, 1),
  (50, 1, 12, 1),
  (63, 1, 9, 1),
  (63, 1, 57, 1),
  (64, 1, 2, 1),
  (64, 1, 20, 1),
  (64, 1, 95, 1),
  (64, 1, 147, 1),
  (64, 1, 148, 1),
  (64, 1, 149, 1),
  (64, 1, 150, 1),
  (64, 1, 151, 1),
  (64, 1, 152, 1),
  (64, 1, 153, 1),
  (64, 1, 154, 1),
  (64, 1, 155, 1),
  (64, 1, 156, 1),
  (64, 1, 157, 1),
  (64, 1, 158, 1),
  (64, 1, 159, 1),
  (64, 1, 160, 1),
  (64, 1, 161, 1),
  (67, 1, 180, 1),
  (69, 1, 170, 1),
  (69, 1, 171, 1),
  (69, 1, 172, 1),
  (69, 1, 173, 1),
  (70, 1, 166, 1),
  (70, 1, 167, 1),
  (70, 1, 168, 1),
  (70, 1, 169, 1),
  (71, 1, 174, 1),
  (72, 1, 165, 1),
  (73, 1, 176, 1),
  (76, 1, 162, 1),
  (80, 1, 56, 1),
  (80, 1, 181, 1),
  (80, 1, 182, 1),
  (82, 1, 183, 1),
  (82, 1, 184, 1),
  (82, 1, 185, 1),
  (86, 1, 186, 1),
  (87, 1, 15, 1),
  (88, 1, 7, 1),
  (88, 1, 112, 1),
  (96, 1, 187, 1),
  (96, 1, 188, 1),
  (96, 1, 189, 1),
  (97, 1, 47, 1),
  (98, 1, 101, 1),
  (99, 1, 18, 1),
  (99, 1, 37, 1),
  (101, 1, 11, 1),
  (101, 1, 29, 1),
  (101, 1, 93, 1),
  (101, 1, 100, 1),
  (102, 1, 21, 1),
  (102, 1, 30, 1),
  (102, 1, 46, 1),
  (104, 1, 41, 1),
  (105, 1, 6, 1),
  (105, 1, 48, 1),
  (105, 1, 49, 1),
  (105, 1, 60, 1),
  (105, 1, 104, 1),
  (105, 1, 191, 1),
  (2, 1, 10, 2),
  (5, 1, 15, 2),
  (6, 1, 26, 2),
  (11, 1, 8, 2),
  (11, 1, 67, 2),
  (11, 1, 68, 2),
  (11, 1, 69, 2),
  (18, 1, 125, 2),
  (19, 1, 13, 2),
  (19, 1, 14, 2),
  (19, 1, 17, 2),
  (19, 1, 96, 2),
  (19, 1, 97, 2),
  (27, 1, 107, 2),
  (27, 1, 108, 2),
  (27, 1, 117, 2),
  (27, 1, 118, 2),
  (27, 1, 119, 2),
  (27, 1, 121, 2),
  (27, 1, 122, 2),
  (27, 1, 123, 2),
  (27, 1, 130, 2),
  (27, 1, 131, 2),
  (27, 1, 132, 2),
  (30, 1, 1, 2),
  (30, 1, 5, 2),
  (30, 1, 113, 2),
  (32, 1, 139, 2),
  (32, 1, 145, 2),
  (33, 1, 146, 2),
  (39, 1, 42, 2),
  (41, 1, 39, 2),
  (50, 1, 31, 2),
  (60, 1, 52, 2),
  (63, 1, 98, 2),
  (64, 1, 57, 2),
  (64, 1, 120, 2),
  (64, 1, 136, 2),
  (64, 1, 137, 2),
  (64, 1, 141, 2),
  (64, 1, 143, 2),
  (69, 1, 162, 2),
  (70, 1, 9, 2),
  (73, 1, 166, 2),
  (73, 1, 167, 2),
  (73, 1, 168, 2),
  (73, 1, 169, 2),
  (75, 1, 165, 2),
  (80, 1, 32, 2),
  (81, 1, 101, 2),
  (83, 1, 183, 2),
  (84, 1, 133, 2),
  (84, 1, 134, 2),
  (84, 1, 135, 2),
  (94, 1, 7, 2),
  (96, 1, 154, 2),
  (99, 1, 55, 2),
  (100, 1, 18, 2),
  (102, 1, 11, 2),
  (104, 1, 2, 2),
  (104, 1, 16, 2),
  (104, 1, 20, 2),
  (104, 1, 29, 2),
  (104, 1, 37, 2),
  (104, 1, 46, 2),
  (104, 1, 93, 2),
  (105, 1, 41, 2),
  (106, 1, 47, 2),
  (4, 1, 10, 3),
  (4, 1, 96, 3),
  (4, 1, 97, 3),
  (7, 1, 26, 3),
  (9, 1, 9, 3),
  (10, 1, 101, 3),
  (12, 1, 8, 3),
  (23, 1, 13, 3),
  (23, 1, 14, 3),
  (23, 1, 17, 3),
  (27, 1, 15, 3),
  (27, 1, 68, 3),
  (33, 1, 139, 3),
  (33, 1, 145, 3),
  (34, 1, 146, 3),
  (37, 1, 119, 3),
  (42, 1, 39, 3),
  (64, 1, 140, 3),
  (65, 1, 57, 3),
  (65, 1, 125, 3),
  (65, 1, 126, 3),
  (71, 1, 162, 3),
  (74, 1, 166, 3),
  (74, 1, 167, 3),
  (74, 1, 168, 3),
  (74, 1, 169, 3),
  (83, 1, 18, 3),
  (84, 1, 107, 3),
  (84, 1, 108, 3),
  (84, 1, 117, 3),
  (84, 1, 118, 3),
  (84, 1, 121, 3),
  (84, 1, 122, 3),
  (84, 1, 123, 3),
  (84, 1, 130, 3),
  (84, 1, 131, 3),
  (84, 1, 132, 3),
  (84, 1, 136, 3),
  (84, 1, 137, 3),
  (88, 1, 98, 3),
  (90, 1, 183, 3),
  (101, 1, 7, 3),
  (101, 1, 32, 3),
  (101, 1, 42, 3),
  (105, 1, 2, 3),
  (105, 1, 93, 3),
  (5, 1, 10, 4),
  (8, 1, 101, 4),
  (11, 1, 26, 4),
  (13, 1, 9, 4),
  (23, 1, 96, 4),
  (23, 1, 97, 4),
  (28, 1, 15, 4),
  (34, 1, 139, 4),
  (35, 1, 145, 4),
  (38, 1, 13, 4),
  (38, 1, 14, 4),
  (38, 1, 17, 4),
  (43, 1, 39, 4),
  (67, 1, 57, 4),
  (73, 1, 162, 4),
  (75, 1, 166, 4),
  (75, 1, 167, 4),
  (75, 1, 168, 4),
  (75, 1, 169, 4),
  (84, 1, 68, 4),
  (91, 1, 98, 4),
  (102, 1, 18, 4),
  (104, 1, 32, 4),
  (6, 1, 10, 5),
  (12, 1, 26, 5),
  (19, 1, 8, 5),
  (40, 1, 15, 5),
  (44, 1, 39, 5),
  (77, 1, 162, 5),
  (77, 1, 166, 5),
  (77, 1, 167, 5),
  (77, 1, 168, 5),
  (77, 1, 169, 5),
  (82, 1, 145, 5),
  (83, 1, 57, 5),
  (88, 1, 9, 5),
  (88, 1, 101, 5),
  (89, 1, 97, 5),
  (91, 1, 96, 5),
  (100, 1, 13, 5),
  (100, 1, 14, 5),
  (100, 1, 17, 5),
  (105, 1, 32, 5),
  (7, 1, 10, 6),
  (18, 1, 26, 6),
  (21, 1, 8, 6),
  (26, 1, 13, 6),
  (26, 1, 14, 6),
  (26, 1, 17, 6),
  (41, 1, 15, 6),
  (45, 1, 39, 6),
  (75, 1, 162, 6),
  (78, 1, 166, 6),
  (78, 1, 167, 6),
  (78, 1, 168, 6),
  (78, 1, 169, 6),
  (83, 1, 145, 6),
  (84, 1, 57, 6),
  (90, 1, 9, 6),
  (91, 1, 97, 6),
  (8, 1, 10, 7),
  (15, 1, 26, 7),
  (26, 1, 8, 7),
  (46, 1, 39, 7),
  (82, 1, 14, 7),
  (83, 1, 17, 7),
  (85, 1, 57, 7),
  (91, 1, 9, 7),
  (9, 1, 10, 8),
  (47, 1, 39, 8),
  (50, 1, 26, 8),
  (63, 1, 8, 8),
  (83, 1, 14, 8),
  (84, 1, 15, 8),
  (88, 1, 57, 8),
  (93, 1, 9, 8),
  (105, 1, 17, 8),
  (10, 1, 10, 9),
  (48, 1, 39, 9),
  (63, 1, 26, 9),
  (88, 1, 8, 9),
  (88, 1, 15, 9),
  (90, 1, 14, 9),
  (90, 1, 57, 9),
  (94, 1, 9, 9),
  (11, 1, 10, 10),
  (49, 1, 39, 10),
  (79, 1, 26, 10),
  (91, 1, 57, 10),
  (94, 1, 8, 10),
  (101, 1, 15, 10),
  (102, 1, 9, 10),
  (12, 1, 10, 11),
  (51, 1, 39, 11),
  (85, 1, 26, 11),
  (92, 1, 57, 11),
  (14, 1, 10, 12),
  (52, 1, 39, 12),
  (88, 1, 26, 12),
  (94, 1, 57, 12),
  (15, 1, 10, 13),
  (53, 1, 39, 13),
  (94, 1, 26, 13),
  (95, 1, 57, 13),
  (16, 1, 10, 14),
  (54, 1, 39, 14),
  (96, 1, 57, 14),
  (102, 1, 26, 14),
  (17, 1, 10, 15),
  (55, 1, 39, 15),
  (102, 1, 57, 15),
  (18, 1, 10, 16),
  (56, 1, 39, 16),
  (19, 1, 10, 17),
  (57, 1, 39, 17),
  (20, 1, 10, 18),
  (58, 1, 39, 18),
  (21, 1, 10, 19),
  (59, 1, 39, 19),
  (22, 1, 10, 20),
  (23, 1, 10, 20),
  (60, 1, 39, 20),
  (24, 1, 10, 21),
  (61, 1, 39, 21),
  (25, 1, 10, 22),
  (62, 1, 39, 22),
  (26, 1, 10, 23),
  (29, 1, 10, 24),
  (27, 1, 10, 25),
  (37, 1, 10, 25),
  (28, 1, 10, 26),
  (38, 1, 10, 26),
  (63, 1, 10, 27),
  (39, 1, 10, 30),
  (97, 1, 10, 30),
  (98, 1, 10, 31),
  (99, 1, 10, 32),
  (100, 1, 10, 33),
  (67, 1, 10, 34),
  (70, 1, 10, 35),
  (77, 1, 10, 36),
  (80, 1, 10, 37),
  (81, 1, 10, 38),
  (82, 1, 10, 39),
  (83, 1, 10, 40),
  (84, 1, 10, 41),
  (85, 1, 10, 42),
  (86, 1, 10, 43),
  (87, 1, 10, 44),
  (88, 1, 10, 45),
  (89, 1, 10, 46),
  (90, 1, 10, 47),
  (91, 1, 10, 48),
  (92, 1, 10, 49),
  (93, 1, 10, 50),
  (94, 1, 10, 51),
  (96, 1, 10, 53),
  (101, 1, 10, 54),
  (102, 1, 10, 55),
  (105, 1, 10, 56);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_hook_module_exceptions`
--

CREATE TABLE IF NOT EXISTS `ps_hook_module_exceptions` (
  `id_hook_module_exceptions` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop`                   INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_module`                 INT(10) UNSIGNED NOT NULL,
  `id_hook`                   INT(10) UNSIGNED NOT NULL,
  `file_name`                 VARCHAR(255)              DEFAULT NULL,
  PRIMARY KEY (`id_hook_module_exceptions`),
  KEY `id_module` (`id_module`),
  KEY `id_hook` (`id_hook`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 33;

--
-- Vypisuji data pro tabulku `ps_hook_module_exceptions`
--

INSERT INTO `ps_hook_module_exceptions` (`id_hook_module_exceptions`, `id_shop`, `id_module`, `id_hook`, `file_name`)
VALUES
  (1, 1, 4, 8, 'category'),
  (2, 1, 16, 8, 'category'),
  (3, 1, 17, 8, 'category'),
  (4, 1, 21, 8, 'category'),
  (5, 1, 25, 8, 'category'),
  (8, 1, 7, 8, 'category'),
  (9, 1, 11, 8, 'orderopc'),
  (10, 1, 11, 8, 'stores'),
  (11, 1, 11, 8, 'orderslip'),
  (12, 1, 11, 8, 'identity'),
  (13, 1, 11, 8, 'myaccount'),
  (14, 1, 11, 8, 'product'),
  (15, 1, 11, 8, 'discount'),
  (16, 1, 11, 8, 'auth'),
  (17, 1, 11, 8, 'sitemap'),
  (18, 1, 11, 8, 'password'),
  (19, 1, 11, 8, 'cms'),
  (20, 1, 11, 8, 'pagenotfound'),
  (21, 1, 11, 8, 'index'),
  (22, 1, 11, 8, 'orderfollow'),
  (23, 1, 11, 8, 'contact'),
  (24, 1, 11, 8, 'order'),
  (25, 1, 11, 8, 'addresses'),
  (26, 1, 11, 8, 'cart'),
  (27, 1, 11, 8, 'address'),
  (28, 1, 12, 8, 'index'),
  (29, 1, 21, 8, 'category'),
  (30, 1, 21, 8, 'index'),
  (31, 1, 26, 8, 'index'),
  (32, 1, 63, 8, 'index');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_image`
--

CREATE TABLE IF NOT EXISTS `ps_image` (
  `id_image`   INT(10) UNSIGNED     NOT NULL AUTO_INCREMENT,
  `id_product` INT(10) UNSIGNED     NOT NULL,
  `position`   SMALLINT(2) UNSIGNED NOT NULL DEFAULT '0',
  `cover`      TINYINT(1) UNSIGNED           DEFAULT NULL,
  PRIMARY KEY (`id_image`),
  UNIQUE KEY `id_product_cover` (`id_product`, `cover`),
  UNIQUE KEY `idx_product_image` (`id_image`, `id_product`, `cover`),
  KEY `image_product` (`id_product`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 27;

--
-- Vypisuji data pro tabulku `ps_image`
--

INSERT INTO `ps_image` (`id_image`, `id_product`, `position`, `cover`) VALUES
  (1, 1, 3, NULL),
  (2, 1, 4, NULL),
  (3, 1, 5, NULL),
  (4, 1, 6, NULL),
  (8, 3, 1, 1),
  (9, 3, 2, NULL),
  (10, 4, 1, 1),
  (11, 4, 2, NULL),
  (12, 5, 1, 1),
  (13, 5, 2, NULL),
  (14, 5, 3, NULL),
  (15, 5, 4, NULL),
  (16, 6, 1, 1),
  (17, 6, 2, NULL),
  (18, 6, 3, NULL),
  (19, 6, 4, NULL),
  (20, 7, 1, NULL),
  (21, 7, 2, NULL),
  (22, 7, 3, NULL),
  (23, 7, 4, NULL),
  (24, 1, 1, NULL),
  (25, 7, 5, 1),
  (26, 1, 2, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_image_lang`
--

CREATE TABLE IF NOT EXISTS `ps_image_lang` (
  `id_image` INT(10) UNSIGNED NOT NULL,
  `id_lang`  INT(10) UNSIGNED NOT NULL,
  `legend`   VARCHAR(128) DEFAULT NULL,
  PRIMARY KEY (`id_image`, `id_lang`),
  KEY `id_image` (`id_image`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_image_lang`
--

INSERT INTO `ps_image_lang` (`id_image`, `id_lang`, `legend`) VALUES
  (1, 1, ''),
  (2, 1, ''),
  (3, 1, ''),
  (4, 1, ''),
  (8, 1, ''),
  (9, 1, ''),
  (10, 1, ''),
  (11, 1, ''),
  (12, 1, ''),
  (13, 1, ''),
  (14, 1, ''),
  (15, 1, ''),
  (16, 1, ''),
  (17, 1, ''),
  (18, 1, ''),
  (19, 1, ''),
  (20, 1, ''),
  (21, 1, ''),
  (22, 1, ''),
  (23, 1, ''),
  (24, 1, ''),
  (25, 1, ''),
  (26, 1, '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_image_shop`
--

CREATE TABLE IF NOT EXISTS `ps_image_shop` (
  `id_product` INT(10) UNSIGNED NOT NULL,
  `id_image`   INT(11) UNSIGNED NOT NULL,
  `id_shop`    INT(11) UNSIGNED NOT NULL,
  `cover`      TINYINT(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id_image`, `id_shop`),
  UNIQUE KEY `id_product` (`id_product`, `id_shop`, `cover`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_image_shop`
--

INSERT INTO `ps_image_shop` (`id_product`, `id_image`, `id_shop`, `cover`) VALUES
  (1, 1, 1, NULL),
  (1, 2, 1, NULL),
  (1, 3, 1, NULL),
  (1, 4, 1, NULL),
  (1, 24, 1, NULL),
  (1, 26, 1, 1),
  (3, 9, 1, NULL),
  (3, 8, 1, 1),
  (4, 11, 1, NULL),
  (4, 10, 1, 1),
  (5, 13, 1, NULL),
  (5, 14, 1, NULL),
  (5, 15, 1, NULL),
  (5, 12, 1, 1),
  (6, 17, 1, NULL),
  (6, 18, 1, NULL),
  (6, 19, 1, NULL),
  (6, 16, 1, 1),
  (7, 20, 1, NULL),
  (7, 21, 1, NULL),
  (7, 22, 1, NULL),
  (7, 23, 1, NULL),
  (7, 25, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_image_type`
--

CREATE TABLE IF NOT EXISTS `ps_image_type` (
  `id_image_type` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`          VARCHAR(64)      NOT NULL,
  `width`         INT(10) UNSIGNED NOT NULL,
  `height`        INT(10) UNSIGNED NOT NULL,
  `products`      TINYINT(1)       NOT NULL DEFAULT '1',
  `categories`    TINYINT(1)       NOT NULL DEFAULT '1',
  `manufacturers` TINYINT(1)       NOT NULL DEFAULT '1',
  `suppliers`     TINYINT(1)       NOT NULL DEFAULT '1',
  `scenes`        TINYINT(1)       NOT NULL DEFAULT '1',
  `stores`        TINYINT(1)       NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_image_type`),
  KEY `image_type_name` (`name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 26;

--
-- Vypisuji data pro tabulku `ps_image_type`
--

INSERT INTO `ps_image_type` (`id_image_type`, `name`, `width`, `height`, `products`, `categories`, `manufacturers`, `suppliers`, `scenes`, `stores`)
VALUES
  (10, 'small_default', 98, 98, 1, 0, 1, 1, 0, 1),
  (11, 'medium_default', 125, 125, 1, 1, 1, 1, 0, 1),
  (12, 'home_default', 500, 500, 1, 0, 0, 0, 0, 1),
  (13, 'large_default', 800, 800, 1, 0, 1, 1, 0, 1),
  (14, 'thickbox_default', 1000, 1000, 1, 0, 0, 0, 0, 1),
  (15, 'category_default', 270, 270, 0, 1, 0, 0, 0, 1),
  (16, 'scene_default', 520, 189, 0, 0, 0, 0, 1, 1),
  (17, 'm_scene_default', 161, 58, 0, 0, 0, 0, 1, 1),
  (18, 'tm_home_default', 500, 500, 1, 1, 0, 0, 0, 1),
  (19, 'tm_large_default', 800, 800, 1, 0, 0, 0, 0, 1),
  (20, 'tm_small_default', 98, 98, 1, 0, 0, 0, 0, 1),
  (21, 'tm_cart_default', 80, 80, 1, 0, 0, 0, 0, 1),
  (22, 'tm_medium_default', 125, 125, 1, 1, 1, 1, 0, 1),
  (23, 'tm_thickbox_default', 1000, 1000, 1, 0, 0, 0, 0, 1),
  (24, 'tm_category_default', 270, 270, 0, 1, 0, 0, 0, 1),
  (25, 'cart_default', 80, 80, 1, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_import_match`
--

CREATE TABLE IF NOT EXISTS `ps_import_match` (
  `id_import_match` INT(10)     NOT NULL AUTO_INCREMENT,
  `name`            VARCHAR(32) NOT NULL,
  `match`           TEXT        NOT NULL,
  `skip`            INT(2)      NOT NULL,
  PRIMARY KEY (`id_import_match`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_import_match`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_info`
--

CREATE TABLE IF NOT EXISTS `ps_info` (
  `id_info` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` INT(10) UNSIGNED          DEFAULT NULL,
  PRIMARY KEY (`id_info`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 3;

--
-- Vypisuji data pro tabulku `ps_info`
--

INSERT INTO `ps_info` (`id_info`, `id_shop`) VALUES
  (1, 1),
  (2, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_info_lang`
--

CREATE TABLE IF NOT EXISTS `ps_info_lang` (
  `id_info` INT(10) UNSIGNED NOT NULL,
  `id_lang` INT(10) UNSIGNED NOT NULL,
  `text`    TEXT             NOT NULL,
  PRIMARY KEY (`id_info`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_info_lang`
--

INSERT INTO `ps_info_lang` (`id_info`, `id_lang`, `text`) VALUES
  (1, 1,
   '<ul><li><em class="icon-truck" id="icon-truck"></em>A kdeže se tohle to objeví, </li>\n</ul><p>píše se tu</p>\n<h4 class="page-subtitle">Vlastní CMS informační blok</h4>'),
  (2, 1,
   '<h3>Custom Block</h3>\n<p><strong class="dark">Lorem ipsum dolor sit amet conse ctetu</strong></p>\n<p>Sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_lang`
--

CREATE TABLE IF NOT EXISTS `ps_lang` (
  `id_lang`          INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `name`             VARCHAR(32)         NOT NULL,
  `active`           TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `iso_code`         CHAR(2)             NOT NULL,
  `language_code`    CHAR(5)             NOT NULL,
  `date_format_lite` CHAR(32)            NOT NULL DEFAULT 'Y-m-d',
  `date_format_full` CHAR(32)            NOT NULL DEFAULT 'Y-m-d H:i:s',
  `is_rtl`           TINYINT(1)          NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_lang`),
  KEY `lang_iso_code` (`iso_code`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_lang`
--

INSERT INTO `ps_lang` (`id_lang`, `name`, `active`, `iso_code`, `language_code`, `date_format_lite`, `date_format_full`, `is_rtl`)
VALUES
  (1, 'Čeština (Czech)', 1, 'cs', 'cs-cz', 'Y-m-d', 'Y-m-d H:i:s', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_lang_shop`
--

CREATE TABLE IF NOT EXISTS `ps_lang_shop` (
  `id_lang` INT(11) UNSIGNED NOT NULL,
  `id_shop` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_lang`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_lang_shop`
--

INSERT INTO `ps_lang_shop` (`id_lang`, `id_shop`) VALUES
  (1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_layered_category`
--

CREATE TABLE IF NOT EXISTS `ps_layered_category` (
  `id_layered_category` INT(10) UNSIGNED                                                                                                  NOT NULL AUTO_INCREMENT,
  `id_shop`             INT(11) UNSIGNED                                                                                                  NOT NULL,
  `id_category`         INT(10) UNSIGNED                                                                                                  NOT NULL,
  `id_value`            INT(10) UNSIGNED                                                                                                           DEFAULT '0',
  `type`                ENUM ('category', 'id_feature', 'id_attribute_group', 'quantity', 'condition', 'manufacturer', 'weight', 'price') NOT NULL,
  `position`            INT(10) UNSIGNED                                                                                                  NOT NULL,
  `filter_type`         INT(10) UNSIGNED                                                                                                  NOT NULL DEFAULT '0',
  `filter_show_limit`   INT(10) UNSIGNED                                                                                                  NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_layered_category`),
  KEY `id_category` (`id_category`, `type`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 45;

--
-- Vypisuji data pro tabulku `ps_layered_category`
--

INSERT INTO `ps_layered_category` (`id_layered_category`, `id_shop`, `id_category`, `id_value`, `type`, `position`, `filter_type`, `filter_show_limit`)
VALUES
  (1, 1, 2, NULL, 'category', 1, 0, 0),
  (2, 1, 2, 1, 'id_attribute_group', 2, 0, 0),
  (3, 1, 2, 3, 'id_attribute_group', 3, 0, 0),
  (4, 1, 2, 5, 'id_feature', 4, 0, 0),
  (5, 1, 2, 6, 'id_feature', 5, 0, 0),
  (6, 1, 2, 7, 'id_feature', 6, 0, 0),
  (7, 1, 2, NULL, 'quantity', 7, 0, 0),
  (8, 1, 2, NULL, 'manufacturer', 8, 0, 0),
  (9, 1, 2, NULL, 'condition', 9, 0, 0),
  (10, 1, 2, NULL, 'weight', 10, 0, 0),
  (11, 1, 2, NULL, 'price', 11, 0, 0),
  (12, 1, 3, NULL, 'category', 1, 0, 0),
  (13, 1, 3, 1, 'id_attribute_group', 2, 0, 0),
  (14, 1, 3, 3, 'id_attribute_group', 3, 0, 0),
  (15, 1, 3, 5, 'id_feature', 4, 0, 0),
  (16, 1, 3, 6, 'id_feature', 5, 0, 0),
  (17, 1, 3, 7, 'id_feature', 6, 0, 0),
  (18, 1, 3, NULL, 'quantity', 7, 0, 0),
  (19, 1, 3, NULL, 'manufacturer', 8, 0, 0),
  (20, 1, 3, NULL, 'condition', 9, 0, 0),
  (21, 1, 3, NULL, 'weight', 10, 0, 0),
  (22, 1, 3, NULL, 'price', 11, 0, 0),
  (23, 1, 4, NULL, 'category', 1, 0, 0),
  (24, 1, 4, 1, 'id_attribute_group', 2, 0, 0),
  (25, 1, 4, 3, 'id_attribute_group', 3, 0, 0),
  (26, 1, 4, 5, 'id_feature', 4, 0, 0),
  (27, 1, 4, 6, 'id_feature', 5, 0, 0),
  (28, 1, 4, 7, 'id_feature', 6, 0, 0),
  (29, 1, 4, NULL, 'quantity', 7, 0, 0),
  (30, 1, 4, NULL, 'manufacturer', 8, 0, 0),
  (31, 1, 4, NULL, 'condition', 9, 0, 0),
  (32, 1, 4, NULL, 'weight', 10, 0, 0),
  (33, 1, 4, NULL, 'price', 11, 0, 0),
  (34, 1, 8, NULL, 'category', 1, 0, 0),
  (35, 1, 8, 1, 'id_attribute_group', 2, 0, 0),
  (36, 1, 8, 3, 'id_attribute_group', 3, 0, 0),
  (37, 1, 8, 5, 'id_feature', 4, 0, 0),
  (38, 1, 8, 6, 'id_feature', 5, 0, 0),
  (39, 1, 8, 7, 'id_feature', 6, 0, 0),
  (40, 1, 8, NULL, 'quantity', 7, 0, 0),
  (41, 1, 8, NULL, 'manufacturer', 8, 0, 0),
  (42, 1, 8, NULL, 'condition', 9, 0, 0),
  (43, 1, 8, NULL, 'weight', 10, 0, 0),
  (44, 1, 8, NULL, 'price', 11, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_layered_filter`
--

CREATE TABLE IF NOT EXISTS `ps_layered_filter` (
  `id_layered_filter` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`              VARCHAR(64)      NOT NULL,
  `filters`           TEXT,
  `n_categories`      INT(10) UNSIGNED NOT NULL,
  `date_add`          DATETIME         NOT NULL,
  PRIMARY KEY (`id_layered_filter`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_layered_filter`
--

INSERT INTO `ps_layered_filter` (`id_layered_filter`, `name`, `filters`, `n_categories`, `date_add`) VALUES
  (1, 'Moje šablona 2016-08-04',
   'a:13:{s:10:"categories";a:4:{i:0;i:2;i:2;i:3;i:3;i:4;i:5;i:8;}s:9:"shop_list";a:1:{i:1;i:1;}s:31:"layered_selection_subcategories";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:22:"layered_selection_ag_1";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:22:"layered_selection_ag_3";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:24:"layered_selection_feat_5";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:24:"layered_selection_feat_6";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:24:"layered_selection_feat_7";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:23:"layered_selection_stock";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:30:"layered_selection_manufacturer";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:27:"layered_selection_condition";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:31:"layered_selection_weight_slider";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:30:"layered_selection_price_slider";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}}',
   9, '2016-08-04 12:42:41');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_layered_filter_shop`
--

CREATE TABLE IF NOT EXISTS `ps_layered_filter_shop` (
  `id_layered_filter` INT(10) UNSIGNED NOT NULL,
  `id_shop`           INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_layered_filter`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_layered_filter_shop`
--

INSERT INTO `ps_layered_filter_shop` (`id_layered_filter`, `id_shop`) VALUES
  (1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_layered_friendly_url`
--

CREATE TABLE IF NOT EXISTS `ps_layered_friendly_url` (
  `id_layered_friendly_url` INT(11)      NOT NULL AUTO_INCREMENT,
  `url_key`                 VARCHAR(32)  NOT NULL,
  `data`                    VARCHAR(200) NOT NULL,
  `id_lang`                 INT(11)      NOT NULL,
  PRIMARY KEY (`id_layered_friendly_url`),
  KEY `id_lang` (`id_lang`),
  KEY `url_key` (`url_key`(5))
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 56;

--
-- Vypisuji data pro tabulku `ps_layered_friendly_url`
--

INSERT INTO `ps_layered_friendly_url` (`id_layered_friendly_url`, `url_key`, `data`, `id_lang`) VALUES
  (1, 'd67b910eb29d83eb9d903c31e22ca567', 'a:1:{s:8:"category";a:1:{i:1;s:1:"1";}}', 1),
  (2, 'a9281426b476699183aad5bbc2eb9dac', 'a:1:{s:8:"category";a:1:{i:2;s:1:"2";}}', 1),
  (3, '6f4a4923e92fa77baa1be09e18575a8c', 'a:1:{s:8:"category";a:1:{i:3;s:1:"3";}}', 1),
  (4, '2b0769b78490b536de784d99b8507c3b', 'a:1:{s:8:"category";a:1:{i:4;s:1:"4";}}', 1),
  (5, '5bbae538124c2c9db20fc31b436ce9de', 'a:1:{s:8:"category";a:1:{i:8;s:1:"8";}}', 1),
  (6, '16956bc8263d8eb0d95d081d427030f9', 'a:1:{s:8:"category";a:1:{i:5;s:1:"5";}}', 1),
  (7, '249fbd517206c325c31f5c37d0a504b1', 'a:1:{s:8:"category";a:1:{i:7;s:1:"7";}}', 1),
  (8, 'b923a0c4096a52fd26cbb65587e7b3de', 'a:1:{s:8:"category";a:1:{i:9;s:1:"9";}}', 1),
  (9, '58d61d397251cab470327e8d0c8f9cf4', 'a:1:{s:8:"category";a:1:{i:10;s:2:"10";}}', 1),
  (10, 'a4d6a1a51ba848bca78cb8d0da0942de', 'a:1:{s:8:"category";a:1:{i:11;s:2:"11";}}', 1),
  (11, 'c4d7335317f2f1ba381e038fb625d918', 'a:1:{s:10:"id_feature";a:1:{i:1;s:3:"5_1";}}', 1),
  (12, '18f41c9cab1c150e429f1b670cae3bc1', 'a:1:{s:10:"id_feature";a:1:{i:2;s:3:"5_2";}}', 1),
  (13, '823192a052e44927f06b39b32bcef002', 'a:1:{s:10:"id_feature";a:1:{i:3;s:3:"5_3";}}', 1),
  (14, '905fe5b57eb2e1353911171da4ee7706', 'a:1:{s:10:"id_feature";a:1:{i:4;s:3:"5_4";}}', 1),
  (15, 'ebb42f1bbf0d25b40049c14f1860b952', 'a:1:{s:10:"id_feature";a:1:{i:5;s:3:"5_5";}}', 1),
  (16, 'f9a71edd8befbb99baceadc2b2fbe793', 'a:1:{s:10:"id_feature";a:1:{i:6;s:3:"5_6";}}', 1),
  (17, 'e195459fb3d97a32e94673db75dcf299', 'a:1:{s:10:"id_feature";a:1:{i:7;s:3:"5_7";}}', 1),
  (18, 'b7783cae5eeefc81ff4a69f4ea712ea7', 'a:1:{s:10:"id_feature";a:1:{i:8;s:3:"5_8";}}', 1),
  (19, '45f1d9162a9fe2ffcf9f365eace9eeec', 'a:1:{s:10:"id_feature";a:1:{i:9;s:3:"5_9";}}', 1),
  (20, '7a04872959f09781f3b883a91c5332c7', 'a:1:{s:10:"id_feature";a:1:{i:10;s:4:"6_10";}}', 1),
  (21, '025d11eb379709c8e409a7d712d8e362', 'a:1:{s:10:"id_feature";a:1:{i:11;s:4:"6_11";}}', 1),
  (22, 'e224c427b75f7805c14e8b63ca9e4e0c', 'a:1:{s:10:"id_feature";a:1:{i:12;s:4:"6_12";}}', 1),
  (23, '677717092975926de02151dd9227864e', 'a:1:{s:10:"id_feature";a:1:{i:13;s:4:"6_13";}}', 1),
  (24, '00dff7b63b6f7ddb4b341a9308422730', 'a:1:{s:10:"id_feature";a:1:{i:14;s:4:"6_14";}}', 1),
  (25, 'ff721a9727728b15cd4654a462aaeea0', 'a:1:{s:10:"id_feature";a:1:{i:15;s:4:"6_15";}}', 1),
  (26, '0327a5c6fbcd99ae1fa8ef01f1e7e100', 'a:1:{s:10:"id_feature";a:1:{i:16;s:4:"6_16";}}', 1),
  (27, '58ddd7a988c042c25121ffeb149f3ac7', 'a:1:{s:10:"id_feature";a:1:{i:17;s:4:"7_17";}}', 1),
  (28, 'b7248af6c62c1e54b6f13739739e2d17', 'a:1:{s:10:"id_feature";a:1:{i:18;s:4:"7_18";}}', 1),
  (29, 'b97d201e9d169f46c2a9e6fa356e40d2', 'a:1:{s:10:"id_feature";a:1:{i:19;s:4:"7_19";}}', 1),
  (30, 'de50b73f078d5cde7cc9d8efc61c9e55', 'a:1:{s:10:"id_feature";a:1:{i:20;s:4:"7_20";}}', 1),
  (31, '85a3c64761151fe72e5d027e729072a3', 'a:1:{s:10:"id_feature";a:1:{i:21;s:4:"7_21";}}', 1),
  (32, '97d9dd08827238b39342d37e16ee7fc3', 'a:1:{s:18:"id_attribute_group";a:1:{i:1;s:3:"1_1";}}', 1),
  (33, '2f3d5048a6335cac20241e0f8cb5294e', 'a:1:{s:18:"id_attribute_group";a:1:{i:2;s:3:"1_2";}}', 1),
  (34, '19819345209f29bb2865355fa2cdb800', 'a:1:{s:18:"id_attribute_group";a:1:{i:3;s:3:"1_3";}}', 1),
  (35, '27dd5799da96500f9e0ab61387a556b5', 'a:1:{s:18:"id_attribute_group";a:1:{i:4;s:3:"1_4";}}', 1),
  (36, '6a73ce72468db97129f092fa3d9a0b2e', 'a:1:{s:18:"id_attribute_group";a:1:{i:5;s:3:"3_5";}}', 1),
  (37, 'f1fc935c7d64dfac606eb814dcc6c4a7', 'a:1:{s:18:"id_attribute_group";a:1:{i:6;s:3:"3_6";}}', 1),
  (38, 'f036e061c6e0e9cd6b3c463f72f524a5', 'a:1:{s:18:"id_attribute_group";a:1:{i:7;s:3:"3_7";}}', 1),
  (39, '468a278b79ece55c0ed0d3bd1b2dd01f', 'a:1:{s:18:"id_attribute_group";a:1:{i:8;s:3:"3_8";}}', 1),
  (40, '8996dbd99c9d2240f117ba0d26b39b10', 'a:1:{s:18:"id_attribute_group";a:1:{i:9;s:3:"3_9";}}', 1),
  (41, '601a4dd13077730810f102b18680b537', 'a:1:{s:18:"id_attribute_group";a:1:{i:10;s:4:"3_10";}}', 1),
  (42, '0a68b3ba0819d7126935f51335ef9503', 'a:1:{s:18:"id_attribute_group";a:1:{i:11;s:4:"3_11";}}', 1),
  (43, '5f556205d67d7c26c2726dba638c2d95', 'a:1:{s:18:"id_attribute_group";a:1:{i:12;s:4:"3_12";}}', 1),
  (44, '4b4bb79b20455e8047c972f9ca69cd72', 'a:1:{s:18:"id_attribute_group";a:1:{i:13;s:4:"3_13";}}', 1),
  (45, '54dd539ce8bbf02b44485941f2d8d80b', 'a:1:{s:18:"id_attribute_group";a:1:{i:14;s:4:"3_14";}}', 1),
  (46, '73b845a28e9ced9709fa414f9b97dae9', 'a:1:{s:18:"id_attribute_group";a:1:{i:15;s:4:"3_15";}}', 1),
  (47, 'be50cfae4c360fdb124af017a4e80905', 'a:1:{s:18:"id_attribute_group";a:1:{i:16;s:4:"3_16";}}', 1),
  (48, '4c4550abfc4eec4c91e558fa9b5171c9', 'a:1:{s:18:"id_attribute_group";a:1:{i:17;s:4:"3_17";}}', 1),
  (49, 'ab223cc0ca7ebf34af71e067556ee2aa', 'a:1:{s:18:"id_attribute_group";a:1:{i:24;s:4:"3_24";}}', 1),
  (50, 'ae21e283a5214f3ff640bc97757ef38d', 'a:1:{s:8:"quantity";a:1:{i:0;i:0;}}', 1),
  (51, '673308b77e13ad8fe45e053b4491a598', 'a:1:{s:8:"quantity";a:1:{i:0;i:1;}}', 1),
  (52, 'a154f3dcfa2135a4be2c3531c9130d8c', 'a:1:{s:9:"condition";a:1:{s:3:"new";s:3:"new";}}', 1),
  (53, '3cf557b1ffee1338a562ce2432d39bf0', 'a:1:{s:9:"condition";a:1:{s:4:"used";s:4:"used";}}', 1),
  (54, 'f237ae63199ea170f1f08d559b5b3b4c', 'a:1:{s:9:"condition";a:1:{s:11:"refurbished";s:11:"refurbished";}}', 1),
  (55, '73f0d7794af8af54f8341a9747557f74', 'a:1:{s:12:"manufacturer";a:1:{i:1;s:1:"1";}}', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_layered_indexable_attribute_group`
--

CREATE TABLE IF NOT EXISTS `ps_layered_indexable_attribute_group` (
  `id_attribute_group` INT(11)    NOT NULL,
  `indexable`          TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_attribute_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_layered_indexable_attribute_group`
--

INSERT INTO `ps_layered_indexable_attribute_group` (`id_attribute_group`, `indexable`) VALUES
  (1, 1),
  (2, 1),
  (3, 1),
  (4, 1),
  (5, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_layered_indexable_attribute_group_lang_value`
--

CREATE TABLE IF NOT EXISTS `ps_layered_indexable_attribute_group_lang_value` (
  `id_attribute_group` INT(11) NOT NULL,
  `id_lang`            INT(11) NOT NULL,
  `url_name`           VARCHAR(128) DEFAULT NULL,
  `meta_title`         VARCHAR(128) DEFAULT NULL,
  PRIMARY KEY (`id_attribute_group`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_layered_indexable_attribute_group_lang_value`
--

INSERT INTO `ps_layered_indexable_attribute_group_lang_value` (`id_attribute_group`, `id_lang`, `url_name`, `meta_title`)
VALUES
  (1, 1, 'velikost', ''),
  (2, 1, 'dioptrie-pwr', ''),
  (3, 1, 'barva', ''),
  (4, 1, 'zakriveni-bc', ''),
  (5, 1, 'prumer-dia', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_layered_indexable_attribute_lang_value`
--

CREATE TABLE IF NOT EXISTS `ps_layered_indexable_attribute_lang_value` (
  `id_attribute` INT(11) NOT NULL,
  `id_lang`      INT(11) NOT NULL,
  `url_name`     VARCHAR(128) DEFAULT NULL,
  `meta_title`   VARCHAR(128) DEFAULT NULL,
  PRIMARY KEY (`id_attribute`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_layered_indexable_attribute_lang_value`
--

INSERT INTO `ps_layered_indexable_attribute_lang_value` (`id_attribute`, `id_lang`, `url_name`, `meta_title`) VALUES
  (25, 1, '20', ''),
  (26, 1, '195', ''),
  (27, 1, '19', ''),
  (28, 1, '185', ''),
  (29, 1, '18', ''),
  (30, 1, '175', ''),
  (31, 1, '17', ''),
  (32, 1, '165', ''),
  (33, 1, '16', ''),
  (34, 1, '155', ''),
  (35, 1, '15', ''),
  (36, 1, '145', ''),
  (37, 1, '14', ''),
  (38, 1, '135', ''),
  (39, 1, '13', ''),
  (40, 1, '125', ''),
  (41, 1, '12', ''),
  (42, 1, '115', ''),
  (43, 1, '11', ''),
  (44, 1, '105', ''),
  (45, 1, '10', ''),
  (46, 1, '95', ''),
  (47, 1, '9', ''),
  (48, 1, '85', ''),
  (49, 1, '8', ''),
  (50, 1, '75', ''),
  (51, 1, '7', ''),
  (52, 1, '65', ''),
  (53, 1, '6', ''),
  (54, 1, '55', ''),
  (55, 1, '5', ''),
  (56, 1, '45', ''),
  (57, 1, '4', ''),
  (58, 1, '35', ''),
  (59, 1, '3', ''),
  (60, 1, '25', ''),
  (61, 1, '2', ''),
  (62, 1, '15', ''),
  (63, 1, '1', ''),
  (64, 1, '05', ''),
  (65, 1, 'neutral', ''),
  (66, 1, '-05', ''),
  (67, 1, '-1', ''),
  (68, 1, '-15', ''),
  (69, 1, '-2', ''),
  (70, 1, '-25', ''),
  (71, 1, '-3', ''),
  (72, 1, '-35', ''),
  (73, 1, '-4', ''),
  (74, 1, '-45', ''),
  (75, 1, '-5', ''),
  (76, 1, '-55', ''),
  (77, 1, '-6', ''),
  (78, 1, '-65', ''),
  (79, 1, '-7', ''),
  (80, 1, '-75', ''),
  (81, 1, '-8', ''),
  (82, 1, '-85', ''),
  (83, 1, '-9', ''),
  (84, 1, '-95', ''),
  (85, 1, '-10', ''),
  (86, 1, '-105', ''),
  (87, 1, '-11', ''),
  (88, 1, '-115', ''),
  (89, 1, '-12', ''),
  (90, 1, '-125', ''),
  (91, 1, '-13', ''),
  (92, 1, '-135', ''),
  (93, 1, '-14', ''),
  (94, 1, '-145', ''),
  (95, 1, '-15', ''),
  (96, 1, '-155', ''),
  (97, 1, '-16', ''),
  (98, 1, '-165', ''),
  (99, 1, '-17', ''),
  (100, 1, '-175', ''),
  (101, 1, '-18', ''),
  (102, 1, '-185', ''),
  (103, 1, '-19', ''),
  (104, 1, '-195', ''),
  (105, 1, '-20', ''),
  (106, 1, '12', ''),
  (107, 1, '121', ''),
  (108, 1, '122', ''),
  (109, 1, '123', ''),
  (110, 1, '124', ''),
  (111, 1, '125', ''),
  (112, 1, '126', ''),
  (113, 1, '127', ''),
  (114, 1, '128', ''),
  (115, 1, '129', ''),
  (116, 1, '13', ''),
  (117, 1, '131', ''),
  (118, 1, '132', ''),
  (119, 1, '133', ''),
  (120, 1, '134', ''),
  (121, 1, '135', ''),
  (122, 1, '136', ''),
  (123, 1, '137', ''),
  (124, 1, '138', ''),
  (125, 1, '139', ''),
  (126, 1, '14', ''),
  (127, 1, '141', ''),
  (128, 1, '142', ''),
  (129, 1, '143', ''),
  (130, 1, '144', ''),
  (131, 1, '145', ''),
  (132, 1, '146', ''),
  (133, 1, '147', ''),
  (134, 1, '148', ''),
  (135, 1, '149', ''),
  (136, 1, '15', ''),
  (137, 1, '151', ''),
  (138, 1, '152', ''),
  (139, 1, '153', ''),
  (140, 1, '154', ''),
  (141, 1, '155', ''),
  (142, 1, '156', ''),
  (143, 1, '157', ''),
  (144, 1, '158', ''),
  (145, 1, '159', ''),
  (146, 1, '16', ''),
  (147, 1, '161', ''),
  (148, 1, '162', ''),
  (149, 1, '163', ''),
  (150, 1, '164', ''),
  (151, 1, '165', ''),
  (152, 1, '166', ''),
  (153, 1, '167', ''),
  (154, 1, '168', ''),
  (155, 1, '169', ''),
  (156, 1, '17', ''),
  (157, 1, '65', ''),
  (158, 1, '66', ''),
  (159, 1, '67', ''),
  (160, 1, '68', ''),
  (161, 1, '69', ''),
  (162, 1, '7', ''),
  (163, 1, '71', ''),
  (164, 1, '72', ''),
  (165, 1, '73', ''),
  (166, 1, '74', ''),
  (167, 1, '75', ''),
  (168, 1, '76', ''),
  (169, 1, '77', ''),
  (170, 1, '78', ''),
  (171, 1, '79', ''),
  (172, 1, '8', ''),
  (173, 1, '81', ''),
  (174, 1, '82', ''),
  (175, 1, '83', ''),
  (176, 1, '84', ''),
  (177, 1, '85', ''),
  (178, 1, '86', ''),
  (179, 1, '87', ''),
  (180, 1, '88', ''),
  (181, 1, '89', ''),
  (182, 1, '9', ''),
  (183, 1, '91', ''),
  (184, 1, '92', ''),
  (185, 1, '93', ''),
  (186, 1, '94', ''),
  (187, 1, '95', ''),
  (188, 1, '96', ''),
  (189, 1, '97', ''),
  (190, 1, '98', ''),
  (191, 1, '99', ''),
  (192, 1, '10', ''),
  (193, 1, '101', ''),
  (194, 1, '102', ''),
  (195, 1, '103', ''),
  (196, 1, '104', ''),
  (197, 1, '105', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_layered_indexable_feature`
--

CREATE TABLE IF NOT EXISTS `ps_layered_indexable_feature` (
  `id_feature` INT(11)    NOT NULL,
  `indexable`  TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_feature`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_layered_indexable_feature`
--

INSERT INTO `ps_layered_indexable_feature` (`id_feature`, `indexable`) VALUES
  (1, 0),
  (2, 0),
  (3, 0),
  (4, 0),
  (6, 1),
  (7, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_layered_indexable_feature_lang_value`
--

CREATE TABLE IF NOT EXISTS `ps_layered_indexable_feature_lang_value` (
  `id_feature` INT(11)      NOT NULL,
  `id_lang`    INT(11)      NOT NULL,
  `url_name`   VARCHAR(128) NOT NULL,
  `meta_title` VARCHAR(128) DEFAULT NULL,
  PRIMARY KEY (`id_feature`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_layered_indexable_feature_lang_value`
--

INSERT INTO `ps_layered_indexable_feature_lang_value` (`id_feature`, `id_lang`, `url_name`, `meta_title`) VALUES
  (1, 1, 'vyska', ''),
  (2, 1, 'sirka', ''),
  (3, 1, 'hloubka', ''),
  (4, 1, 'vaha', ''),
  (6, 1, 'styl', ''),
  (7, 1, 'vlastnosti', ''),
  (8, 1, 'dioptrie', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_layered_indexable_feature_value_lang_value`
--

CREATE TABLE IF NOT EXISTS `ps_layered_indexable_feature_value_lang_value` (
  `id_feature_value` INT(11) NOT NULL,
  `id_lang`          INT(11) NOT NULL,
  `url_name`         VARCHAR(128) DEFAULT NULL,
  `meta_title`       VARCHAR(128) DEFAULT NULL,
  PRIMARY KEY (`id_feature_value`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_layered_indexable_feature_value_lang_value`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_layered_price_index`
--

CREATE TABLE IF NOT EXISTS `ps_layered_price_index` (
  `id_product`  INT(11) NOT NULL,
  `id_currency` INT(11) NOT NULL,
  `id_shop`     INT(11) NOT NULL,
  `price_min`   INT(11) NOT NULL,
  `price_max`   INT(11) NOT NULL,
  PRIMARY KEY (`id_product`, `id_currency`, `id_shop`),
  KEY `id_currency` (`id_currency`),
  KEY `price_min` (`price_min`),
  KEY `price_max` (`price_max`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_layered_price_index`
--

INSERT INTO `ps_layered_price_index` (`id_product`, `id_currency`, `id_shop`, `price_min`, `price_max`) VALUES
  (1, 1, 1, 16, 20),
  (1, 2, 1, 0, 1),
  (2, 1, 1, 26, 33),
  (2, 2, 1, 0, 1),
  (3, 1, 1, 25, 31),
  (3, 2, 1, 0, 1),
  (4, 1, 1, 50, 62),
  (4, 2, 1, 1, 2),
  (5, 1, 1, 28, 35),
  (5, 2, 1, 1, 1),
  (6, 1, 1, 30, 37),
  (6, 2, 1, 1, 1),
  (7, 1, 1, 16, 20),
  (7, 2, 1, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_layered_product_attribute`
--

CREATE TABLE IF NOT EXISTS `ps_layered_product_attribute` (
  `id_attribute`       INT(10) UNSIGNED NOT NULL,
  `id_product`         INT(10) UNSIGNED NOT NULL,
  `id_attribute_group` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_shop`            INT(10) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_attribute`, `id_product`, `id_shop`),
  UNIQUE KEY `id_attribute_group` (`id_attribute_group`, `id_attribute`, `id_product`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_layered_product_attribute`
--

INSERT INTO `ps_layered_product_attribute` (`id_attribute`, `id_product`, `id_attribute_group`, `id_shop`) VALUES
  (1, 1, 1, 1),
  (1, 2, 1, 1),
  (1, 3, 1, 1),
  (1, 4, 1, 1),
  (1, 5, 1, 1),
  (1, 6, 1, 1),
  (1, 7, 1, 1),
  (2, 1, 1, 1),
  (2, 2, 1, 1),
  (2, 3, 1, 1),
  (2, 4, 1, 1),
  (2, 5, 1, 1),
  (2, 6, 1, 1),
  (2, 7, 1, 1),
  (3, 1, 1, 1),
  (3, 2, 1, 1),
  (3, 3, 1, 1),
  (3, 4, 1, 1),
  (3, 5, 1, 1),
  (3, 6, 1, 1),
  (3, 7, 1, 1),
  (7, 4, 3, 1),
  (8, 2, 3, 1),
  (8, 6, 3, 1),
  (11, 2, 3, 1),
  (11, 5, 3, 1),
  (13, 1, 3, 1),
  (13, 3, 3, 1),
  (13, 5, 3, 1),
  (14, 1, 3, 1),
  (14, 5, 3, 1),
  (15, 7, 3, 1),
  (16, 5, 3, 1),
  (16, 6, 3, 1),
  (16, 7, 3, 1),
  (24, 4, 3, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_linksmenutop`
--

CREATE TABLE IF NOT EXISTS `ps_linksmenutop` (
  `id_linksmenutop` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop`         INT(11) UNSIGNED NOT NULL,
  `new_window`      TINYINT(1)       NOT NULL,
  PRIMARY KEY (`id_linksmenutop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_linksmenutop`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_linksmenutop_lang`
--

CREATE TABLE IF NOT EXISTS `ps_linksmenutop_lang` (
  `id_linksmenutop` INT(11) UNSIGNED NOT NULL,
  `id_lang`         INT(11) UNSIGNED NOT NULL,
  `id_shop`         INT(11) UNSIGNED NOT NULL,
  `label`           VARCHAR(128)     NOT NULL,
  `link`            VARCHAR(128)     NOT NULL,
  KEY `id_linksmenutop` (`id_linksmenutop`, `id_lang`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_linksmenutop_lang`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_log`
--

CREATE TABLE IF NOT EXISTS `ps_log` (
  `id_log`      INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `severity`    TINYINT(1)       NOT NULL,
  `error_code`  INT(11)                   DEFAULT NULL,
  `message`     TEXT             NOT NULL,
  `object_type` VARCHAR(32)               DEFAULT NULL,
  `object_id`   INT(10) UNSIGNED          DEFAULT NULL,
  `id_employee` INT(10) UNSIGNED          DEFAULT NULL,
  `date_add`    DATETIME         NOT NULL,
  `date_upd`    DATETIME         NOT NULL,
  PRIMARY KEY (`id_log`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 315;

--
-- Vypisuji data pro tabulku `ps_log`
--

INSERT INTO `ps_log` (`id_log`, `severity`, `error_code`, `message`, `object_type`, `object_id`, `id_employee`, `date_add`, `date_upd`)
VALUES
  (1, 1, 0, 'Připojení do administrace z 93.190.63.130', '', 0, 1, '2016-08-04 12:44:06', '2016-08-04 12:44:06'),
  (2, 1, 0, 'Připojení do administrace z 37.48.15.123', '', 0, 1, '2016-09-19 13:19:25', '2016-09-19 13:19:25'),
  (3, 1, 0, 'Employee změna', 'Employee', 1, 1, '2016-09-19 13:19:58', '2016-09-19 13:19:58'),
  (4, 1, 0, 'Připojení do administrace z 37.48.15.123', '', 0, 1, '2016-09-19 13:42:19', '2016-09-19 13:42:19'),
  (5, 1, 0, 'Category přidáno', 'Category', 12, 1, '2016-09-19 13:59:11', '2016-09-19 13:59:11'),
  (6, 1, 0, 'Připojení do administrace z 93.190.63.130', '', 0, 1, '2016-09-19 22:12:26', '2016-09-19 22:12:26'),
  (7, 1, 0, 'Připojení do administrace z 37.48.15.123', '', 0, 1, '2016-09-20 09:25:17', '2016-09-20 09:25:17'),
  (8, 1, 0, 'Meta změna', 'Meta', 24, 1, '2016-09-20 09:25:31', '2016-09-20 09:25:31'),
  (9, 1, 0, 'Připojení do administrace z 93.190.63.130', '', 0, 1, '2016-09-20 16:51:08', '2016-09-20 16:51:08'),
  (10, 1, 0, 'Připojení do administrace z 37.48.1.207', '', 0, 1, '2016-10-05 10:31:39', '2016-10-05 10:31:39'),
  (11, 1, 0, 'Připojení do administrace z 37.48.1.207', '', 0, 1, '2016-10-05 10:50:13', '2016-10-05 10:50:13'),
  (12, 1, 0, 'Připojení do administrace z 93.190.63.130', '', 0, 1, '2016-10-11 14:45:08', '2016-10-11 14:45:08'),
  (13, 1, 0, 'Employee přidáno', 'Employee', 2, 1, '2016-10-11 14:46:34', '2016-10-11 14:46:34'),
  (14, 1, 0, 'Připojení do administrace z 37.48.0.32', '', 0, 1, '2016-10-13 08:29:38', '2016-10-13 08:29:38'),
  (15, 1, 0, 'Připojení do administrace z 37.48.38.187', '', 0, 1, '2016-11-03 08:03:00', '2016-11-03 08:03:00'),
  (16, 1, 0, 'Připojení do administrace z 37.48.9.72', '', 0, 1, '2017-01-23 15:33:13', '2017-01-23 15:33:13'),
  (17, 1, 0, 'Připojení do administrace z 37.48.9.72', '', 0, 1, '2017-01-23 15:59:58', '2017-01-23 15:59:58'),
  (18, 1, 0, 'Store změna', 'Store', 1, 1, '2017-01-23 17:26:28', '2017-01-23 17:26:28'),
  (19, 1, 0, 'Category změna', 'Category', 3, 1, '2017-01-23 18:36:08', '2017-01-23 18:36:08'),
  (20, 1, 0, 'Category změna', 'Category', 4, 1, '2017-01-23 18:36:55', '2017-01-23 18:36:55'),
  (21, 1, 0, 'Category změna', 'Category', 8, 1, '2017-01-23 18:37:20', '2017-01-23 18:37:20'),
  (22, 1, 0, 'Category odstraněno', 'Category', 5, 1, '2017-01-23 18:38:02', '2017-01-23 18:38:02'),
  (23, 1, 0, 'Category odstraněno', 'Category', 6, 1, '2017-01-23 18:38:21', '2017-01-23 18:38:21'),
  (24, 1, 0, 'Category odstraněno', 'Category', 7, 1, '2017-01-23 18:38:39', '2017-01-23 18:38:39'),
  (25, 1, 0, 'Category odstraněno', 'Category', 9, 1, '2017-01-23 18:39:12', '2017-01-23 18:39:12'),
  (26, 1, 0, 'Category odstraněno', 'Category', 10, 1, '2017-01-23 18:39:29', '2017-01-23 18:39:29'),
  (27, 1, 0, 'Category odstraněno', 'Category', 11, 1, '2017-01-23 18:39:45', '2017-01-23 18:39:45'),
  (28, 1, 0, 'Category přidáno', 'Category', 13, 1, '2017-01-23 18:40:29', '2017-01-23 18:40:29'),
  (29, 1, 0, 'Category přidáno', 'Category', 14, 1, '2017-01-23 18:41:18', '2017-01-23 18:41:18'),
  (30, 1, 0, 'Category přidáno', 'Category', 15, 1, '2017-01-23 18:41:47', '2017-01-23 18:41:47'),
  (31, 1, 0, 'Category přidáno', 'Category', 16, 1, '2017-01-23 18:42:51', '2017-01-23 18:42:51'),
  (32, 1, 0, 'Category přidáno', 'Category', 17, 1, '2017-01-23 18:43:25', '2017-01-23 18:43:25'),
  (33, 1, 0, 'Category přidáno', 'Category', 18, 1, '2017-01-23 18:43:54', '2017-01-23 18:43:54'),
  (34, 1, 0, 'Category přidáno', 'Category', 19, 1, '2017-01-23 18:44:19', '2017-01-23 18:44:19'),
  (35, 1, 0, 'Category změna', 'Category', 12, 1, '2017-01-23 18:51:40', '2017-01-23 18:51:40'),
  (36, 1, 0, 'Category změna', 'Category', 3, 1, '2017-01-23 18:55:41', '2017-01-23 18:55:41'),
  (37, 1, 0, 'Category přidáno', 'Category', 20, 1, '2017-01-23 18:56:45', '2017-01-23 18:56:45'),
  (38, 1, 0, 'Category změna', 'Category', 15, 1, '2017-01-23 18:58:00', '2017-01-23 18:58:00'),
  (39, 1, 0, 'Category změna', 'Category', 14, 1, '2017-01-23 18:58:30', '2017-01-23 18:58:30'),
  (40, 1, 0, 'Category změna', 'Category', 13, 1, '2017-01-23 18:58:54', '2017-01-23 18:58:54'),
  (41, 1, 0, 'Category změna', 'Category', 8, 1, '2017-01-23 18:59:13', '2017-01-23 18:59:13'),
  (42, 1, 0, 'Category změna', 'Category', 4, 1, '2017-01-23 18:59:31', '2017-01-23 18:59:31'),
  (43, 1, 0, 'Category přidáno', 'Category', 21, 1, '2017-01-23 19:00:03', '2017-01-23 19:00:03'),
  (44, 1, 0, 'Category změna', 'Category', 16, 1, '2017-01-23 19:00:18', '2017-01-23 19:00:18'),
  (45, 1, 0, 'Category změna', 'Category', 17, 1, '2017-01-23 19:00:58', '2017-01-23 19:00:58'),
  (46, 1, 0, 'Category změna', 'Category', 18, 1, '2017-01-23 19:01:20', '2017-01-23 19:01:20'),
  (47, 1, 0, 'Category změna', 'Category', 19, 1, '2017-01-23 19:01:50', '2017-01-23 19:01:50'),
  (48, 1, 0, 'Manufacturer změna', 'Manufacturer', 1, 1, '2017-01-23 19:06:04', '2017-01-23 19:06:04'),
  (49, 1, 0, 'Manufacturer přidáno', 'Manufacturer', 2, 1, '2017-01-23 19:07:59', '2017-01-23 19:07:59'),
  (50, 1, 0, 'Address odstraněno', 'Address', 3, 1, '2017-01-23 19:08:09', '2017-01-23 19:08:09'),
  (51, 1, 0, 'Manufacturer změna', 'Manufacturer', 1, 1, '2017-01-23 19:08:22', '2017-01-23 19:08:22'),
  (52, 1, 0, 'Manufacturer přidáno', 'Manufacturer', 3, 1, '2017-01-23 19:09:02', '2017-01-23 19:09:02'),
  (53, 1, 0, 'Manufacturer přidáno', 'Manufacturer', 4, 1, '2017-01-23 19:09:29', '2017-01-23 19:09:29'),
  (54, 1, 0, 'Manufacturer přidáno', 'Manufacturer', 5, 1, '2017-01-23 19:09:52', '2017-01-23 19:09:52'),
  (55, 1, 0, 'Manufacturer přidáno', 'Manufacturer', 6, 1, '2017-01-23 19:10:12', '2017-01-23 19:10:12'),
  (56, 1, 0, 'Manufacturer přidáno', 'Manufacturer', 7, 1, '2017-01-23 19:10:29', '2017-01-23 19:10:29'),
  (57, 1, 0, 'Manufacturer přidáno', 'Manufacturer', 8, 1, '2017-01-23 19:10:45', '2017-01-23 19:10:45'),
  (58, 1, 0, 'Category přidáno', 'Category', 22, 1, '2017-01-23 19:18:44', '2017-01-23 19:18:44'),
  (59, 1, 0, 'Category přidáno', 'Category', 23, 1, '2017-01-23 19:18:56', '2017-01-23 19:18:56'),
  (60, 1, 0, 'Category přidáno', 'Category', 24, 1, '2017-01-23 19:19:13', '2017-01-23 19:19:13'),
  (61, 1, 0, 'Category přidáno', 'Category', 25, 1, '2017-01-23 19:19:26', '2017-01-23 19:19:26'),
  (62, 1, 0, 'Category změna', 'Category', 12, 1, '2017-01-23 19:20:14', '2017-01-23 19:20:14'),
  (63, 1, 0, 'Category odstraněno', 'Category', 25, 1, '2017-01-23 19:20:40', '2017-01-23 19:20:40'),
  (64, 1, 0, 'Category změna', 'Category', 22, 1, '2017-01-23 19:21:03', '2017-01-23 19:21:03'),
  (65, 1, 0, 'Category přidáno', 'Category', 26, 1, '2017-01-23 19:21:27', '2017-01-23 19:21:27'),
  (66, 1, 0, 'Category přidáno', 'Category', 27, 1, '2017-01-23 19:29:58', '2017-01-23 19:29:58'),
  (67, 1, 0, 'Category přidáno', 'Category', 28, 1, '2017-01-23 19:30:28', '2017-01-23 19:30:28'),
  (68, 1, 0, 'Category přidáno', 'Category', 29, 1, '2017-01-23 19:30:56', '2017-01-23 19:30:56'),
  (69, 1, 0, 'Category přidáno', 'Category', 30, 1, '2017-01-23 19:31:19', '2017-01-23 19:31:19'),
  (70, 1, 0, 'Category přidáno', 'Category', 31, 1, '2017-01-23 19:31:42', '2017-01-23 19:31:42'),
  (71, 1, 0, 'Category přidáno', 'Category', 32, 1, '2017-01-23 19:31:57', '2017-01-23 19:31:57'),
  (72, 1, 0, 'Category přidáno', 'Category', 33, 1, '2017-01-23 19:32:08', '2017-01-23 19:32:08'),
  (73, 1, 0, 'Category přidáno', 'Category', 34, 1, '2017-01-23 19:32:26', '2017-01-23 19:32:26'),
  (74, 1, 0, 'Category změna', 'Category', 23, 1, '2017-01-23 19:33:29', '2017-01-23 19:33:29'),
  (75, 1, 0, 'Category změna', 'Category', 22, 1, '2017-01-23 19:34:59', '2017-01-23 19:34:59'),
  (76, 1, 0, 'CMSCategory přidáno', 'CMSCategory', 2, 1, '2017-01-23 19:50:24', '2017-01-23 19:50:24'),
  (77, 1, 0, 'CMSCategory změna', 'CMSCategory', 2, 1, '2017-01-23 19:51:41', '2017-01-23 19:51:41'),
  (78, 1, 0, 'CMSCategory přidáno', 'CMSCategory', 3, 1, '2017-01-23 19:52:09', '2017-01-23 19:52:09'),
  (79, 1, 0, 'CMSCategory změna', 'CMSCategory', 3, 1, '2017-01-23 19:53:15', '2017-01-23 19:53:15'),
  (80, 1, 0, 'Připojení do administrace z 213.226.193.194', '', 0, 2, '2017-01-24 20:09:22', '2017-01-24 20:09:22'),
  (81, 1, 0, 'Product změna', 'Product', 2, 2, '2017-01-24 20:20:00', '2017-01-24 20:20:00'),
  (82, 1, 0, 'Připojení do administrace z 213.226.193.194', '', 0, 2, '2017-01-24 20:48:30', '2017-01-24 20:48:30'),
  (83, 1, 0, 'Připojení do administrace z 82.100.10.126', '', 0, 2, '2017-01-25 07:37:13', '2017-01-25 07:37:13'),
  (84, 1, 0, 'Připojení do administrace z 82.100.10.126', '', 0, 2, '2017-01-25 08:10:26', '2017-01-25 08:10:26'),
  (85, 1, 0, 'Připojení do administrace z 93.190.63.130', '', 0, 1, '2017-01-25 13:20:52', '2017-01-25 13:20:52'),
  (86, 1, 0, 'Product změna', 'Product', 1, 1, '2017-01-25 13:21:57', '2017-01-25 13:21:57'),
  (87, 1, 0, 'Product změna', 'Product', 7, 1, '2017-01-25 13:35:18', '2017-01-25 13:35:18'),
  (88, 1, 0, 'Připojení do administrace z 82.100.10.126', '', 0, 2, '2017-01-25 14:18:11', '2017-01-25 14:18:11'),
  (89, 1, 0, 'Product změna', 'Product', 1, 2, '2017-01-25 14:20:05', '2017-01-25 14:20:05'),
  (90, 1, 0, 'Category změna', 'Category', 20, 2, '2017-01-25 14:35:12', '2017-01-25 14:35:12'),
  (91, 1, 0, 'Category změna', 'Category', 15, 2, '2017-01-25 14:36:37', '2017-01-25 14:36:37'),
  (92, 1, 0, 'Category změna', 'Category', 15, 2, '2017-01-25 14:38:22', '2017-01-25 14:38:22'),
  (93, 1, 0, 'Feature přidáno', 'Feature', 8, 2, '2017-01-25 14:48:20', '2017-01-25 14:48:20'),
  (94, 1, 0, 'Připojení do administrace z 82.100.10.126', '', 0, 2, '2017-01-25 15:21:00', '2017-01-25 15:21:00'),
  (95, 1, 0, 'Product odstraněno', 'Product', 2, 2, '2017-01-25 15:22:42', '2017-01-25 15:22:42'),
  (96, 1, 0, 'Feature odstraněno', 'Feature', 8, 1, '2017-01-25 15:44:18', '2017-01-25 15:44:18'),
  (97, 1, 0, 'Feature změna', 'Feature', 1, 1, '2017-01-25 15:44:39', '2017-01-25 15:44:39'),
  (98, 1, 0, 'Feature změna', 'Feature', 2, 1, '2017-01-25 15:45:01', '2017-01-25 15:45:01'),
  (99, 1, 0, 'Feature změna', 'Feature', 3, 1, '2017-01-25 15:45:17', '2017-01-25 15:45:17'),
  (100, 1, 0, 'Feature změna', 'Feature', 4, 1, '2017-01-25 15:45:29', '2017-01-25 15:45:29'),
  (101, 1, 0, 'Feature odstraněno', 'Feature', 5, 1, '2017-01-25 15:45:46', '2017-01-25 15:45:46'),
  (102, 1, 0, 'Feature změna', 'Feature', 6, 1, '2017-01-25 15:46:13', '2017-01-25 15:46:13'),
  (103, 1, 0, 'Feature změna', 'Feature', 7, 1, '2017-01-25 15:46:35', '2017-01-25 15:46:35'),
  (104, 1, 0, 'AttributeGroup změna', 'AttributeGroup', 1, 1, '2017-01-25 15:47:08', '2017-01-25 15:47:08'),
  (105, 1, 0, 'AttributeGroup změna', 'AttributeGroup', 2, 1, '2017-01-25 15:47:54', '2017-01-25 15:47:54'),
  (106, 1, 0, 'AttributeGroup změna', 'AttributeGroup', 3, 1, '2017-01-25 15:48:11', '2017-01-25 15:48:11'),
  (107, 1, 0, 'AttributeGroup změna', 'AttributeGroup', 3, 1, '2017-01-25 15:48:42', '2017-01-25 15:48:42'),
  (108, 1, 0, 'AttributeGroup přidáno', 'AttributeGroup', 4, 1, '2017-01-25 15:48:59', '2017-01-25 15:48:59'),
  (109, 1, 0, 'AttributeGroup přidáno', 'AttributeGroup', 5, 1, '2017-01-25 15:49:22', '2017-01-25 15:49:22'),
  (110, 1, 0, 'Feature změna', 'Feature', 6, 1, '2017-01-25 16:00:19', '2017-01-25 16:00:19'),
  (111, 1, 0, 'Připojení do administrace z 37.48.6.23', '', 0, 1, '2017-01-26 10:15:47', '2017-01-26 10:15:47'),
  (112, 1, 0, 'Attribute přidáno', 'Attribute', 25, 1, '2017-01-26 10:18:05', '2017-01-26 10:18:05'),
  (113, 1, 0, 'Attribute přidáno', 'Attribute', 26, 1, '2017-01-26 10:18:18', '2017-01-26 10:18:18'),
  (114, 1, 0, 'Attribute přidáno', 'Attribute', 27, 1, '2017-01-26 10:18:31', '2017-01-26 10:18:31'),
  (115, 1, 0, 'Attribute přidáno', 'Attribute', 28, 1, '2017-01-26 10:18:41', '2017-01-26 10:18:41'),
  (116, 1, 0, 'Attribute přidáno', 'Attribute', 29, 1, '2017-01-26 10:18:47', '2017-01-26 10:18:47'),
  (117, 1, 0, 'Attribute přidáno', 'Attribute', 30, 1, '2017-01-26 10:18:54', '2017-01-26 10:18:54'),
  (118, 1, 0, 'Attribute přidáno', 'Attribute', 31, 1, '2017-01-26 10:18:59', '2017-01-26 10:18:59'),
  (119, 1, 0, 'Attribute přidáno', 'Attribute', 32, 1, '2017-01-26 10:19:05', '2017-01-26 10:19:05'),
  (120, 1, 0, 'Attribute přidáno', 'Attribute', 33, 1, '2017-01-26 10:19:10', '2017-01-26 10:19:10'),
  (121, 1, 0, 'Attribute přidáno', 'Attribute', 34, 1, '2017-01-26 10:19:16', '2017-01-26 10:19:16'),
  (122, 1, 0, 'Attribute přidáno', 'Attribute', 35, 1, '2017-01-26 10:19:21', '2017-01-26 10:19:21'),
  (123, 1, 0, 'Attribute přidáno', 'Attribute', 36, 1, '2017-01-26 10:19:27', '2017-01-26 10:19:27'),
  (124, 1, 0, 'Attribute přidáno', 'Attribute', 37, 1, '2017-01-26 10:19:33', '2017-01-26 10:19:33'),
  (125, 1, 0, 'Attribute přidáno', 'Attribute', 38, 1, '2017-01-26 10:19:40', '2017-01-26 10:19:40'),
  (126, 1, 0, 'Attribute přidáno', 'Attribute', 39, 1, '2017-01-26 10:19:46', '2017-01-26 10:19:46'),
  (127, 1, 0, 'Attribute přidáno', 'Attribute', 40, 1, '2017-01-26 10:19:51', '2017-01-26 10:19:51'),
  (128, 1, 0, 'Attribute přidáno', 'Attribute', 41, 1, '2017-01-26 10:19:56', '2017-01-26 10:19:56'),
  (129, 1, 0, 'Attribute přidáno', 'Attribute', 42, 1, '2017-01-26 10:20:03', '2017-01-26 10:20:03'),
  (130, 1, 0, 'Attribute přidáno', 'Attribute', 43, 1, '2017-01-26 10:20:09', '2017-01-26 10:20:09'),
  (131, 1, 0, 'Attribute přidáno', 'Attribute', 44, 1, '2017-01-26 10:20:15', '2017-01-26 10:20:15'),
  (132, 1, 0, 'Attribute přidáno', 'Attribute', 45, 1, '2017-01-26 10:20:23', '2017-01-26 10:20:23'),
  (133, 1, 0, 'Attribute přidáno', 'Attribute', 46, 1, '2017-01-26 10:20:32', '2017-01-26 10:20:32'),
  (134, 1, 0, 'Attribute přidáno', 'Attribute', 47, 1, '2017-01-26 10:20:38', '2017-01-26 10:20:38'),
  (135, 1, 0, 'Attribute přidáno', 'Attribute', 48, 1, '2017-01-26 10:20:45', '2017-01-26 10:20:45'),
  (136, 1, 0, 'Attribute přidáno', 'Attribute', 49, 1, '2017-01-26 10:20:50', '2017-01-26 10:20:50'),
  (137, 1, 0, 'Attribute přidáno', 'Attribute', 50, 1, '2017-01-26 10:21:03', '2017-01-26 10:21:03'),
  (138, 1, 0, 'Attribute přidáno', 'Attribute', 51, 1, '2017-01-26 10:21:09', '2017-01-26 10:21:09'),
  (139, 1, 0, 'Attribute přidáno', 'Attribute', 52, 1, '2017-01-26 10:21:15', '2017-01-26 10:21:15'),
  (140, 1, 0, 'Attribute přidáno', 'Attribute', 53, 1, '2017-01-26 10:21:21', '2017-01-26 10:21:21'),
  (141, 1, 0, 'Attribute přidáno', 'Attribute', 54, 1, '2017-01-26 10:21:27', '2017-01-26 10:21:27'),
  (142, 1, 0, 'Attribute přidáno', 'Attribute', 55, 1, '2017-01-26 10:21:32', '2017-01-26 10:21:32'),
  (143, 1, 0, 'Attribute přidáno', 'Attribute', 56, 1, '2017-01-26 10:21:39', '2017-01-26 10:21:39'),
  (144, 1, 0, 'Attribute přidáno', 'Attribute', 57, 1, '2017-01-26 10:21:44', '2017-01-26 10:21:44'),
  (145, 1, 0, 'Attribute přidáno', 'Attribute', 58, 1, '2017-01-26 10:21:52', '2017-01-26 10:21:52'),
  (146, 1, 0, 'Attribute přidáno', 'Attribute', 59, 1, '2017-01-26 10:21:57', '2017-01-26 10:21:57'),
  (147, 1, 0, 'Attribute přidáno', 'Attribute', 60, 1, '2017-01-26 10:22:03', '2017-01-26 10:22:03'),
  (148, 1, 0, 'Attribute přidáno', 'Attribute', 61, 1, '2017-01-26 10:22:07', '2017-01-26 10:22:07'),
  (149, 1, 0, 'Attribute přidáno', 'Attribute', 62, 1, '2017-01-26 10:22:13', '2017-01-26 10:22:13'),
  (150, 1, 0, 'Attribute přidáno', 'Attribute', 63, 1, '2017-01-26 10:22:17', '2017-01-26 10:22:17'),
  (151, 1, 0, 'Attribute přidáno', 'Attribute', 64, 1, '2017-01-26 10:22:24', '2017-01-26 10:22:24'),
  (152, 1, 0, 'Attribute přidáno', 'Attribute', 65, 1, '2017-01-26 10:23:00', '2017-01-26 10:23:00'),
  (153, 1, 0, 'Attribute přidáno', 'Attribute', 66, 1, '2017-01-26 10:23:19', '2017-01-26 10:23:19'),
  (154, 1, 0, 'Attribute přidáno', 'Attribute', 67, 1, '2017-01-26 10:23:27', '2017-01-26 10:23:27'),
  (155, 1, 0, 'Attribute přidáno', 'Attribute', 68, 1, '2017-01-26 10:23:42', '2017-01-26 10:23:42'),
  (156, 1, 0, 'Attribute přidáno', 'Attribute', 69, 1, '2017-01-26 10:23:49', '2017-01-26 10:23:49'),
  (157, 1, 0, 'Attribute přidáno', 'Attribute', 70, 1, '2017-01-26 10:23:55', '2017-01-26 10:23:55'),
  (158, 1, 0, 'Attribute přidáno', 'Attribute', 71, 1, '2017-01-26 10:24:03', '2017-01-26 10:24:03'),
  (159, 1, 0, 'Attribute přidáno', 'Attribute', 72, 1, '2017-01-26 10:24:25', '2017-01-26 10:24:25'),
  (160, 1, 0, 'Attribute změna', 'Attribute', 70, 1, '2017-01-26 10:25:05', '2017-01-26 10:25:05'),
  (161, 1, 0, 'Attribute změna', 'Attribute', 71, 1, '2017-01-26 10:25:23', '2017-01-26 10:25:23'),
  (162, 1, 0, 'Attribute změna', 'Attribute', 72, 1, '2017-01-26 10:25:39', '2017-01-26 10:25:39'),
  (163, 1, 0, 'Attribute přidáno', 'Attribute', 73, 1, '2017-01-26 10:26:02', '2017-01-26 10:26:02'),
  (164, 1, 0, 'Attribute přidáno', 'Attribute', 74, 1, '2017-01-26 10:26:08', '2017-01-26 10:26:08'),
  (165, 1, 0, 'Attribute přidáno', 'Attribute', 75, 1, '2017-01-26 10:26:14', '2017-01-26 10:26:14'),
  (166, 1, 0, 'Attribute přidáno', 'Attribute', 76, 1, '2017-01-26 10:26:21', '2017-01-26 10:26:21'),
  (167, 1, 0, 'Attribute přidáno', 'Attribute', 77, 1, '2017-01-26 10:26:26', '2017-01-26 10:26:26'),
  (168, 1, 0, 'Attribute přidáno', 'Attribute', 78, 1, '2017-01-26 10:26:32', '2017-01-26 10:26:32'),
  (169, 1, 0, 'Attribute přidáno', 'Attribute', 79, 1, '2017-01-26 10:26:38', '2017-01-26 10:26:38'),
  (170, 1, 0, 'Attribute přidáno', 'Attribute', 80, 1, '2017-01-26 10:26:44', '2017-01-26 10:26:44'),
  (171, 1, 0, 'Attribute přidáno', 'Attribute', 81, 1, '2017-01-26 10:26:54', '2017-01-26 10:26:54'),
  (172, 1, 0, 'Attribute přidáno', 'Attribute', 82, 1, '2017-01-26 10:27:04', '2017-01-26 10:27:04'),
  (173, 1, 0, 'Attribute přidáno', 'Attribute', 83, 1, '2017-01-26 10:27:12', '2017-01-26 10:27:12'),
  (174, 1, 0, 'Attribute přidáno', 'Attribute', 84, 1, '2017-01-26 10:27:21', '2017-01-26 10:27:21'),
  (175, 1, 0, 'Attribute přidáno', 'Attribute', 85, 1, '2017-01-26 10:27:27', '2017-01-26 10:27:27'),
  (176, 1, 0, 'Attribute přidáno', 'Attribute', 86, 1, '2017-01-26 10:27:34', '2017-01-26 10:27:34'),
  (177, 1, 0, 'Attribute přidáno', 'Attribute', 87, 1, '2017-01-26 10:27:41', '2017-01-26 10:27:41'),
  (178, 1, 0, 'Attribute přidáno', 'Attribute', 88, 1, '2017-01-26 10:27:48', '2017-01-26 10:27:48'),
  (179, 1, 0, 'Attribute přidáno', 'Attribute', 89, 1, '2017-01-26 10:27:53', '2017-01-26 10:27:53'),
  (180, 1, 0, 'Attribute přidáno', 'Attribute', 90, 1, '2017-01-26 10:28:02', '2017-01-26 10:28:02'),
  (181, 1, 0, 'Attribute přidáno', 'Attribute', 91, 1, '2017-01-26 10:28:07', '2017-01-26 10:28:07'),
  (182, 1, 0, 'Attribute přidáno', 'Attribute', 92, 1, '2017-01-26 10:28:13', '2017-01-26 10:28:13'),
  (183, 1, 0, 'Attribute přidáno', 'Attribute', 93, 1, '2017-01-26 10:28:19', '2017-01-26 10:28:19'),
  (184, 1, 0, 'Attribute přidáno', 'Attribute', 94, 1, '2017-01-26 10:28:25', '2017-01-26 10:28:25'),
  (185, 1, 0, 'Attribute přidáno', 'Attribute', 95, 1, '2017-01-26 10:28:33', '2017-01-26 10:28:33'),
  (186, 1, 0, 'Attribute přidáno', 'Attribute', 96, 1, '2017-01-26 10:28:39', '2017-01-26 10:28:39'),
  (187, 1, 0, 'Attribute přidáno', 'Attribute', 97, 1, '2017-01-26 10:28:45', '2017-01-26 10:28:45'),
  (188, 1, 0, 'Attribute přidáno', 'Attribute', 98, 1, '2017-01-26 10:28:52', '2017-01-26 10:28:52'),
  (189, 1, 0, 'Attribute přidáno', 'Attribute', 99, 1, '2017-01-26 10:28:57', '2017-01-26 10:28:57'),
  (190, 1, 0, 'Attribute přidáno', 'Attribute', 100, 1, '2017-01-26 10:29:04', '2017-01-26 10:29:04'),
  (191, 1, 0, 'Attribute přidáno', 'Attribute', 101, 1, '2017-01-26 10:29:16', '2017-01-26 10:29:16'),
  (192, 1, 0, 'Attribute přidáno', 'Attribute', 102, 1, '2017-01-26 10:29:27', '2017-01-26 10:29:27'),
  (193, 1, 0, 'Attribute přidáno', 'Attribute', 103, 1, '2017-01-26 10:29:37', '2017-01-26 10:29:37'),
  (194, 1, 0, 'Attribute přidáno', 'Attribute', 104, 1, '2017-01-26 10:29:44', '2017-01-26 10:29:44'),
  (195, 1, 0, 'Attribute přidáno', 'Attribute', 105, 1, '2017-01-26 10:29:52', '2017-01-26 10:29:52'),
  (196, 1, 0, 'Attribute přidáno', 'Attribute', 106, 1, '2017-01-26 10:30:20', '2017-01-26 10:30:20'),
  (197, 1, 0, 'Attribute přidáno', 'Attribute', 107, 1, '2017-01-26 10:30:26', '2017-01-26 10:30:26'),
  (198, 1, 0, 'Attribute přidáno', 'Attribute', 108, 1, '2017-01-26 10:30:33', '2017-01-26 10:30:33'),
  (199, 1, 0, 'Attribute přidáno', 'Attribute', 109, 1, '2017-01-26 10:30:38', '2017-01-26 10:30:38'),
  (200, 1, 0, 'Attribute přidáno', 'Attribute', 110, 1, '2017-01-26 10:30:45', '2017-01-26 10:30:45'),
  (201, 1, 0, 'Attribute přidáno', 'Attribute', 111, 1, '2017-01-26 10:30:54', '2017-01-26 10:30:54'),
  (202, 1, 0, 'Attribute přidáno', 'Attribute', 112, 1, '2017-01-26 10:30:59', '2017-01-26 10:30:59'),
  (203, 1, 0, 'Attribute přidáno', 'Attribute', 113, 1, '2017-01-26 10:31:05', '2017-01-26 10:31:05'),
  (204, 1, 0, 'Attribute přidáno', 'Attribute', 114, 1, '2017-01-26 10:31:11', '2017-01-26 10:31:11'),
  (205, 1, 0, 'Attribute přidáno', 'Attribute', 115, 1, '2017-01-26 10:31:17', '2017-01-26 10:31:17'),
  (206, 1, 0, 'Attribute přidáno', 'Attribute', 116, 1, '2017-01-26 10:31:22', '2017-01-26 10:31:22'),
  (207, 1, 0, 'Attribute přidáno', 'Attribute', 117, 1, '2017-01-26 10:31:28', '2017-01-26 10:31:28'),
  (208, 1, 0, 'Attribute přidáno', 'Attribute', 118, 1, '2017-01-26 10:31:34', '2017-01-26 10:31:34'),
  (209, 1, 0, 'Attribute přidáno', 'Attribute', 119, 1, '2017-01-26 10:31:41', '2017-01-26 10:31:41'),
  (210, 1, 0, 'Attribute přidáno', 'Attribute', 120, 1, '2017-01-26 10:31:46', '2017-01-26 10:31:46'),
  (211, 1, 0, 'Attribute přidáno', 'Attribute', 121, 1, '2017-01-26 10:31:55', '2017-01-26 10:31:55'),
  (212, 1, 0, 'Attribute přidáno', 'Attribute', 122, 1, '2017-01-26 10:32:01', '2017-01-26 10:32:01'),
  (213, 1, 0, 'Attribute přidáno', 'Attribute', 123, 1, '2017-01-26 10:32:07', '2017-01-26 10:32:07'),
  (214, 1, 0, 'Attribute přidáno', 'Attribute', 124, 1, '2017-01-26 10:32:13', '2017-01-26 10:32:13'),
  (215, 1, 0, 'Attribute přidáno', 'Attribute', 125, 1, '2017-01-26 10:32:19', '2017-01-26 10:32:19'),
  (216, 1, 0, 'Attribute přidáno', 'Attribute', 126, 1, '2017-01-26 10:32:25', '2017-01-26 10:32:25'),
  (217, 1, 0, 'Attribute přidáno', 'Attribute', 127, 1, '2017-01-26 10:32:32', '2017-01-26 10:32:32'),
  (218, 1, 0, 'Attribute přidáno', 'Attribute', 128, 1, '2017-01-26 10:32:38', '2017-01-26 10:32:38'),
  (219, 1, 0, 'Attribute přidáno', 'Attribute', 129, 1, '2017-01-26 10:32:44', '2017-01-26 10:32:44'),
  (220, 1, 0, 'Attribute přidáno', 'Attribute', 130, 1, '2017-01-26 10:32:49', '2017-01-26 10:32:49'),
  (221, 1, 0, 'Attribute přidáno', 'Attribute', 131, 1, '2017-01-26 10:32:55', '2017-01-26 10:32:55'),
  (222, 1, 0, 'Attribute přidáno', 'Attribute', 132, 1, '2017-01-26 10:33:01', '2017-01-26 10:33:01'),
  (223, 1, 0, 'Attribute přidáno', 'Attribute', 133, 1, '2017-01-26 10:33:07', '2017-01-26 10:33:07'),
  (224, 1, 0, 'Attribute přidáno', 'Attribute', 134, 1, '2017-01-26 10:33:13', '2017-01-26 10:33:13'),
  (225, 1, 0, 'Attribute přidáno', 'Attribute', 135, 1, '2017-01-26 10:33:18', '2017-01-26 10:33:18'),
  (226, 1, 0, 'Attribute přidáno', 'Attribute', 136, 1, '2017-01-26 10:33:24', '2017-01-26 10:33:24'),
  (227, 1, 0, 'Attribute přidáno', 'Attribute', 137, 1, '2017-01-26 10:33:31', '2017-01-26 10:33:31'),
  (228, 1, 0, 'Attribute přidáno', 'Attribute', 138, 1, '2017-01-26 10:33:37', '2017-01-26 10:33:37'),
  (229, 1, 0, 'Attribute přidáno', 'Attribute', 139, 1, '2017-01-26 10:33:57', '2017-01-26 10:33:57'),
  (230, 1, 0, 'Attribute přidáno', 'Attribute', 140, 1, '2017-01-26 10:34:04', '2017-01-26 10:34:04'),
  (231, 1, 0, 'Attribute přidáno', 'Attribute', 141, 1, '2017-01-26 10:34:10', '2017-01-26 10:34:10'),
  (232, 1, 0, 'Attribute přidáno', 'Attribute', 142, 1, '2017-01-26 10:34:16', '2017-01-26 10:34:16'),
  (233, 1, 0, 'Attribute přidáno', 'Attribute', 143, 1, '2017-01-26 10:34:24', '2017-01-26 10:34:24'),
  (234, 1, 0, 'Attribute přidáno', 'Attribute', 144, 1, '2017-01-26 10:34:30', '2017-01-26 10:34:30'),
  (235, 1, 0, 'Attribute přidáno', 'Attribute', 145, 1, '2017-01-26 10:34:37', '2017-01-26 10:34:37'),
  (236, 1, 0, 'Attribute přidáno', 'Attribute', 146, 1, '2017-01-26 10:34:43', '2017-01-26 10:34:43'),
  (237, 1, 0, 'Attribute přidáno', 'Attribute', 147, 1, '2017-01-26 10:34:51', '2017-01-26 10:34:51'),
  (238, 1, 0, 'Attribute přidáno', 'Attribute', 148, 1, '2017-01-26 10:34:57', '2017-01-26 10:34:57'),
  (239, 1, 0, 'Attribute přidáno', 'Attribute', 149, 1, '2017-01-26 10:35:03', '2017-01-26 10:35:03'),
  (240, 1, 0, 'Attribute přidáno', 'Attribute', 150, 1, '2017-01-26 10:35:11', '2017-01-26 10:35:11'),
  (241, 1, 0, 'Attribute přidáno', 'Attribute', 151, 1, '2017-01-26 10:35:17', '2017-01-26 10:35:17'),
  (242, 1, 0, 'Attribute přidáno', 'Attribute', 152, 1, '2017-01-26 10:35:24', '2017-01-26 10:35:24'),
  (243, 1, 0, 'Attribute přidáno', 'Attribute', 153, 1, '2017-01-26 10:35:29', '2017-01-26 10:35:29'),
  (244, 1, 0, 'Attribute přidáno', 'Attribute', 154, 1, '2017-01-26 10:35:35', '2017-01-26 10:35:35'),
  (245, 1, 0, 'Attribute přidáno', 'Attribute', 155, 1, '2017-01-26 10:35:42', '2017-01-26 10:35:42'),
  (246, 1, 0, 'Attribute přidáno', 'Attribute', 156, 1, '2017-01-26 10:35:48', '2017-01-26 10:35:48'),
  (247, 1, 0, 'Attribute přidáno', 'Attribute', 157, 1, '2017-01-26 10:37:29', '2017-01-26 10:37:29'),
  (248, 1, 0, 'Attribute přidáno', 'Attribute', 158, 1, '2017-01-26 10:37:38', '2017-01-26 10:37:38'),
  (249, 1, 0, 'Attribute přidáno', 'Attribute', 159, 1, '2017-01-26 10:37:47', '2017-01-26 10:37:47'),
  (250, 1, 0, 'Attribute přidáno', 'Attribute', 160, 1, '2017-01-26 10:37:53', '2017-01-26 10:37:53'),
  (251, 1, 0, 'Attribute přidáno', 'Attribute', 161, 1, '2017-01-26 10:37:59', '2017-01-26 10:37:59'),
  (252, 1, 0, 'Attribute přidáno', 'Attribute', 162, 1, '2017-01-26 10:38:04', '2017-01-26 10:38:04'),
  (253, 1, 0, 'Attribute přidáno', 'Attribute', 163, 1, '2017-01-26 10:38:10', '2017-01-26 10:38:10'),
  (254, 1, 0, 'Attribute přidáno', 'Attribute', 164, 1, '2017-01-26 10:38:16', '2017-01-26 10:38:16'),
  (255, 1, 0, 'Attribute přidáno', 'Attribute', 165, 1, '2017-01-26 10:38:23', '2017-01-26 10:38:23'),
  (256, 1, 0, 'Attribute přidáno', 'Attribute', 166, 1, '2017-01-26 10:38:28', '2017-01-26 10:38:28'),
  (257, 1, 0, 'Attribute přidáno', 'Attribute', 167, 1, '2017-01-26 10:38:35', '2017-01-26 10:38:35'),
  (258, 1, 0, 'Attribute přidáno', 'Attribute', 168, 1, '2017-01-26 10:38:41', '2017-01-26 10:38:41'),
  (259, 1, 0, 'Attribute přidáno', 'Attribute', 169, 1, '2017-01-26 10:38:46', '2017-01-26 10:38:46'),
  (260, 1, 0, 'Attribute přidáno', 'Attribute', 170, 1, '2017-01-26 10:38:52', '2017-01-26 10:38:52'),
  (261, 1, 0, 'Attribute přidáno', 'Attribute', 171, 1, '2017-01-26 10:38:58', '2017-01-26 10:38:58'),
  (262, 1, 0, 'Attribute přidáno', 'Attribute', 172, 1, '2017-01-26 10:39:04', '2017-01-26 10:39:04'),
  (263, 1, 0, 'Attribute přidáno', 'Attribute', 173, 1, '2017-01-26 10:39:10', '2017-01-26 10:39:10'),
  (264, 1, 0, 'Attribute přidáno', 'Attribute', 174, 1, '2017-01-26 10:39:15', '2017-01-26 10:39:15'),
  (265, 1, 0, 'Attribute přidáno', 'Attribute', 175, 1, '2017-01-26 10:39:22', '2017-01-26 10:39:22'),
  (266, 1, 0, 'Attribute přidáno', 'Attribute', 176, 1, '2017-01-26 10:39:28', '2017-01-26 10:39:28'),
  (267, 1, 0, 'Attribute přidáno', 'Attribute', 177, 1, '2017-01-26 10:39:33', '2017-01-26 10:39:33'),
  (268, 1, 0, 'Attribute přidáno', 'Attribute', 178, 1, '2017-01-26 10:39:41', '2017-01-26 10:39:41'),
  (269, 1, 0, 'Attribute přidáno', 'Attribute', 179, 1, '2017-01-26 10:39:47', '2017-01-26 10:39:47'),
  (270, 1, 0, 'Attribute přidáno', 'Attribute', 180, 1, '2017-01-26 10:39:54', '2017-01-26 10:39:54'),
  (271, 1, 0, 'Attribute přidáno', 'Attribute', 181, 1, '2017-01-26 10:40:01', '2017-01-26 10:40:01'),
  (272, 1, 0, 'Attribute přidáno', 'Attribute', 182, 1, '2017-01-26 10:40:06', '2017-01-26 10:40:06'),
  (273, 1, 0, 'Attribute přidáno', 'Attribute', 183, 1, '2017-01-26 10:40:12', '2017-01-26 10:40:12'),
  (274, 1, 0, 'Attribute přidáno', 'Attribute', 184, 1, '2017-01-26 10:40:18', '2017-01-26 10:40:18'),
  (275, 1, 0, 'Attribute přidáno', 'Attribute', 185, 1, '2017-01-26 10:40:24', '2017-01-26 10:40:24'),
  (276, 1, 0, 'Attribute přidáno', 'Attribute', 186, 1, '2017-01-26 10:40:30', '2017-01-26 10:40:30'),
  (277, 1, 0, 'Attribute přidáno', 'Attribute', 187, 1, '2017-01-26 10:40:36', '2017-01-26 10:40:36'),
  (278, 1, 0, 'Attribute přidáno', 'Attribute', 188, 1, '2017-01-26 10:40:44', '2017-01-26 10:40:44'),
  (279, 1, 0, 'Attribute přidáno', 'Attribute', 189, 1, '2017-01-26 10:40:50', '2017-01-26 10:40:50'),
  (280, 1, 0, 'Attribute přidáno', 'Attribute', 190, 1, '2017-01-26 10:40:56', '2017-01-26 10:40:56'),
  (281, 1, 0, 'Attribute přidáno', 'Attribute', 191, 1, '2017-01-26 10:41:01', '2017-01-26 10:41:01'),
  (282, 1, 0, 'Attribute přidáno', 'Attribute', 192, 1, '2017-01-26 10:41:06', '2017-01-26 10:41:06'),
  (283, 1, 0, 'Attribute přidáno', 'Attribute', 193, 1, '2017-01-26 10:41:11', '2017-01-26 10:41:11'),
  (284, 1, 0, 'Attribute přidáno', 'Attribute', 194, 1, '2017-01-26 10:41:18', '2017-01-26 10:41:18'),
  (285, 1, 0, 'Attribute přidáno', 'Attribute', 195, 1, '2017-01-26 10:41:24', '2017-01-26 10:41:24'),
  (286, 1, 0, 'Attribute přidáno', 'Attribute', 196, 1, '2017-01-26 10:41:29', '2017-01-26 10:41:29'),
  (287, 1, 0, 'Attribute přidáno', 'Attribute', 197, 1, '2017-01-26 10:41:36', '2017-01-26 10:41:36'),
  (288, 1, 0, 'Připojení do administrace z 37.48.4.252', '', 0, 1, '2017-02-10 10:13:00', '2017-02-10 10:13:00'),
  (289, 1, 0, 'Category změna', 'Category', 4, 1, '2017-02-10 10:29:43', '2017-02-10 10:29:43'),
  (290, 1, 0, 'Category změna', 'Category', 4, 1, '2017-02-10 10:30:35', '2017-02-10 10:30:35'),
  (291, 1, 0, 'Category změna', 'Category', 4, 1, '2017-02-10 10:33:02', '2017-02-10 10:33:02'),
  (292, 1, 0, 'Category změna', 'Category', 4, 1, '2017-02-10 10:33:39', '2017-02-10 10:33:39'),
  (293, 1, 0, 'Category změna', 'Category', 15, 1, '2017-02-10 10:35:41', '2017-02-10 10:35:41'),
  (294, 1, 0, 'Product změna', 'Product', 3, 1, '2017-02-10 10:36:38', '2017-02-10 10:36:38'),
  (295, 1, 0, 'Category změna', 'Category', 13, 1, '2017-02-10 10:41:03', '2017-02-10 10:41:03'),
  (296, 1, 0, 'Category změna', 'Category', 4, 1, '2017-02-10 10:44:04', '2017-02-10 10:44:04'),
  (297, 1, 0, 'Category změna', 'Category', 4, 1, '2017-02-10 11:01:39', '2017-02-10 11:01:39'),
  (298, 1, 0, 'Category změna', 'Category', 4, 1, '2017-02-10 11:02:45', '2017-02-10 11:02:45'),
  (299, 1, 0, 'Category změna', 'Category', 13, 1, '2017-02-10 11:03:32', '2017-02-10 11:03:32'),
  (300, 1, 0, 'Product změna', 'Product', 1, 1, '2017-02-10 11:04:14', '2017-02-10 11:04:14'),
  (301, 1, 0, 'Category změna', 'Category', 4, 1, '2017-02-10 11:05:18', '2017-02-10 11:05:18'),
  (302, 1, 0, 'Category změna', 'Category', 13, 1, '2017-02-10 11:06:06', '2017-02-10 11:06:06'),
  (303, 1, 0, 'Category změna', 'Category', 4, 1, '2017-02-10 11:09:59', '2017-02-10 11:09:59'),
  (304, 1, 0, 'Category změna', 'Category', 13, 1, '2017-02-10 11:10:28', '2017-02-10 11:10:28'),
  (305, 1, 0, 'Připojení do administrace z 37.48.4.252', '', 0, 1, '2017-02-10 11:28:21', '2017-02-10 11:28:21'),
  (306, 1, 0, 'Product změna', 'Product', 1, 1, '2017-02-10 11:44:49', '2017-02-10 11:44:49'),
  (307, 1, 0, 'Připojení do administrace z 37.48.4.252', '', 0, 1, '2017-02-10 13:30:19', '2017-02-10 13:30:19'),
  (308, 1, 0, 'Připojení do administrace z 37.48.4.252', '', 0, 1, '2017-02-10 13:56:29', '2017-02-10 13:56:29'),
  (309, 1, 0, 'Připojení do administrace z 37.48.4.252', '', 0, 1, '2017-02-10 14:50:11', '2017-02-10 14:50:11'),
  (310, 1, 0, 'Připojení do administrace z 37.48.35.245', '', 0, 1, '2017-02-21 13:07:04', '2017-02-21 13:07:04'),
  (311, 1, 0, 'Připojení do administrace z 37.48.35.245', '', 0, 1, '2017-02-21 14:07:48', '2017-02-21 14:07:48'),
  (312, 1, 0, 'Připojení do administrace z 37.48.33.134', '', 0, 1, '2017-02-22 10:18:08', '2017-02-22 10:18:08'),
  (313, 1, 0, 'Připojení do administrace z 37.48.33.134', '', 0, 1, '2017-02-22 10:38:43', '2017-02-22 10:38:43'),
  (314, 1, 0, 'Připojení do administrace z 37.48.61.63', '', 0, 1, '2017-02-22 15:58:04', '2017-02-22 15:58:04');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_loyalty`
--

CREATE TABLE IF NOT EXISTS `ps_loyalty` (
  `id_loyalty`       INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_loyalty_state` INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_customer`      INT(10) UNSIGNED NOT NULL,
  `id_order`         INT(10) UNSIGNED          DEFAULT NULL,
  `id_cart_rule`     INT(10) UNSIGNED          DEFAULT NULL,
  `points`           INT(11)          NOT NULL DEFAULT '0',
  `date_add`         DATETIME         NOT NULL,
  `date_upd`         DATETIME         NOT NULL,
  PRIMARY KEY (`id_loyalty`),
  KEY `index_loyalty_loyalty_state` (`id_loyalty_state`),
  KEY `index_loyalty_order` (`id_order`),
  KEY `index_loyalty_discount` (`id_cart_rule`),
  KEY `index_loyalty_customer` (`id_customer`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_loyalty`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_loyalty_history`
--

CREATE TABLE IF NOT EXISTS `ps_loyalty_history` (
  `id_loyalty_history` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_loyalty`         INT(10) UNSIGNED          DEFAULT NULL,
  `id_loyalty_state`   INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `points`             INT(11)          NOT NULL DEFAULT '0',
  `date_add`           DATETIME         NOT NULL,
  PRIMARY KEY (`id_loyalty_history`),
  KEY `index_loyalty_history_loyalty` (`id_loyalty`),
  KEY `index_loyalty_history_loyalty_state` (`id_loyalty_state`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_loyalty_history`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_loyalty_state`
--

CREATE TABLE IF NOT EXISTS `ps_loyalty_state` (
  `id_loyalty_state` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_order_state`   INT(10) UNSIGNED          DEFAULT NULL,
  PRIMARY KEY (`id_loyalty_state`),
  KEY `index_loyalty_state_order_state` (`id_order_state`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 6;

--
-- Vypisuji data pro tabulku `ps_loyalty_state`
--

INSERT INTO `ps_loyalty_state` (`id_loyalty_state`, `id_order_state`) VALUES
  (1, 0),
  (4, 0),
  (5, 0),
  (2, 5),
  (3, 6);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_loyalty_state_lang`
--

CREATE TABLE IF NOT EXISTS `ps_loyalty_state_lang` (
  `id_loyalty_state` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_lang`          INT(10) UNSIGNED NOT NULL,
  `name`             VARCHAR(64)      NOT NULL,
  UNIQUE KEY `index_unique_loyalty_state_lang` (`id_loyalty_state`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 6;

--
-- Vypisuji data pro tabulku `ps_loyalty_state_lang`
--

INSERT INTO `ps_loyalty_state_lang` (`id_loyalty_state`, `id_lang`, `name`) VALUES
  (1, 1, 'Awaiting validation'),
  (2, 1, 'Available'),
  (3, 1, 'Cancelled'),
  (4, 1, 'Already converted'),
  (5, 1, 'Unavailable on discounts');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_mail`
--

CREATE TABLE IF NOT EXISTS `ps_mail` (
  `id_mail`   INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `recipient` VARCHAR(126)     NOT NULL,
  `template`  VARCHAR(62)      NOT NULL,
  `subject`   VARCHAR(254)     NOT NULL,
  `id_lang`   INT(11) UNSIGNED NOT NULL,
  `date_add`  TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_mail`),
  KEY `recipient` (`recipient`(10))
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_mail`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_mailalert_customer_oos`
--

CREATE TABLE IF NOT EXISTS `ps_mailalert_customer_oos` (
  `id_customer`          INT(10) UNSIGNED NOT NULL,
  `customer_email`       VARCHAR(128)     NOT NULL,
  `id_product`           INT(10) UNSIGNED NOT NULL,
  `id_product_attribute` INT(10) UNSIGNED NOT NULL,
  `id_shop`              INT(10) UNSIGNED NOT NULL,
  `id_lang`              INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_customer`, `customer_email`, `id_product`, `id_product_attribute`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_mailalert_customer_oos`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_manufacturer`
--

CREATE TABLE IF NOT EXISTS `ps_manufacturer` (
  `id_manufacturer` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`            VARCHAR(64)      NOT NULL,
  `date_add`        DATETIME         NOT NULL,
  `date_upd`        DATETIME         NOT NULL,
  `active`          TINYINT(1)       NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_manufacturer`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 9;

--
-- Vypisuji data pro tabulku `ps_manufacturer`
--

INSERT INTO `ps_manufacturer` (`id_manufacturer`, `name`, `date_add`, `date_upd`, `active`) VALUES
  (1, 'Alcon', '2016-08-04 12:42:30', '2017-01-23 19:08:22', 1),
  (2, 'Barnaux', '2017-01-23 19:07:59', '2017-01-23 19:07:59', 1),
  (3, 'Bausch & Lomb', '2017-01-23 19:09:00', '2017-01-23 19:09:00', 1),
  (4, 'Ciba Vision', '2017-01-23 19:09:29', '2017-01-23 19:09:29', 1),
  (5, 'Clearlab', '2017-01-23 19:09:52', '2017-01-23 19:09:52', 1),
  (6, 'Cooper Vision', '2017-01-23 19:10:12', '2017-01-23 19:10:12', 1),
  (7, 'Johnson & Johnson', '2017-01-23 19:10:29', '2017-01-23 19:10:29', 1),
  (8, 'Wixi', '2017-01-23 19:10:45', '2017-01-23 19:10:45', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_manufacturer_lang`
--

CREATE TABLE IF NOT EXISTS `ps_manufacturer_lang` (
  `id_manufacturer`   INT(10) UNSIGNED NOT NULL,
  `id_lang`           INT(10) UNSIGNED NOT NULL,
  `description`       TEXT,
  `short_description` TEXT,
  `meta_title`        VARCHAR(128) DEFAULT NULL,
  `meta_keywords`     VARCHAR(255) DEFAULT NULL,
  `meta_description`  VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id_manufacturer`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_manufacturer_lang`
--

INSERT INTO `ps_manufacturer_lang` (`id_manufacturer`, `id_lang`, `description`, `short_description`, `meta_title`, `meta_keywords`, `meta_description`)
VALUES
  (1, 1, '', '', '', '', ''),
  (2, 1, '', '', '', '', ''),
  (3, 1, '', '', '', '', ''),
  (4, 1, '', '', '', '', ''),
  (5, 1, '', '', '', '', ''),
  (6, 1, '', '', '', '', ''),
  (7, 1, '', '', '', '', ''),
  (8, 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_manufacturer_shop`
--

CREATE TABLE IF NOT EXISTS `ps_manufacturer_shop` (
  `id_manufacturer` INT(11) UNSIGNED NOT NULL,
  `id_shop`         INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_manufacturer`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_manufacturer_shop`
--

INSERT INTO `ps_manufacturer_shop` (`id_manufacturer`, `id_shop`) VALUES
  (1, 1),
  (2, 1),
  (3, 1),
  (4, 1),
  (5, 1),
  (6, 1),
  (7, 1),
  (8, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_memcached_servers`
--

CREATE TABLE IF NOT EXISTS `ps_memcached_servers` (
  `id_memcached_server` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip`                  VARCHAR(254)     NOT NULL,
  `port`                INT(11) UNSIGNED NOT NULL,
  `weight`              INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_memcached_server`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_memcached_servers`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_message`
--

CREATE TABLE IF NOT EXISTS `ps_message` (
  `id_message`  INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_cart`     INT(10) UNSIGNED             DEFAULT NULL,
  `id_customer` INT(10) UNSIGNED    NOT NULL,
  `id_employee` INT(10) UNSIGNED             DEFAULT NULL,
  `id_order`    INT(10) UNSIGNED    NOT NULL,
  `message`     TEXT                NOT NULL,
  `private`     TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `date_add`    DATETIME            NOT NULL,
  PRIMARY KEY (`id_message`),
  KEY `message_order` (`id_order`),
  KEY `id_cart` (`id_cart`),
  KEY `id_customer` (`id_customer`),
  KEY `id_employee` (`id_employee`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_message`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_message_readed`
--

CREATE TABLE IF NOT EXISTS `ps_message_readed` (
  `id_message`  INT(10) UNSIGNED NOT NULL,
  `id_employee` INT(10) UNSIGNED NOT NULL,
  `date_add`    DATETIME         NOT NULL,
  PRIMARY KEY (`id_message`, `id_employee`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_message_readed`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_meta`
--

CREATE TABLE IF NOT EXISTS `ps_meta` (
  `id_meta`      INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `page`         VARCHAR(64)         NOT NULL,
  `configurable` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_meta`),
  UNIQUE KEY `page` (`page`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 48;

--
-- Vypisuji data pro tabulku `ps_meta`
--

INSERT INTO `ps_meta` (`id_meta`, `page`, `configurable`) VALUES
  (1, 'pagenotfound', 1),
  (2, 'best-sales', 1),
  (3, 'contact', 1),
  (4, 'index', 1),
  (5, 'manufacturer', 1),
  (6, 'new-products', 1),
  (7, 'password', 1),
  (8, 'prices-drop', 1),
  (9, 'sitemap', 1),
  (10, 'supplier', 1),
  (11, 'address', 1),
  (12, 'addresses', 1),
  (13, 'authentication', 1),
  (14, 'cart', 1),
  (15, 'discount', 1),
  (16, 'history', 1),
  (17, 'identity', 1),
  (18, 'my-account', 1),
  (19, 'order-follow', 1),
  (20, 'order-slip', 1),
  (21, 'order', 1),
  (22, 'search', 1),
  (23, 'stores', 1),
  (24, 'order-opc', 1),
  (25, 'guest-tracking', 1),
  (26, 'order-confirmation', 1),
  (27, 'product', 0),
  (28, 'category', 0),
  (29, 'cms', 0),
  (30, 'module-cheque-payment', 0),
  (31, 'module-cheque-validation', 0),
  (32, 'module-bankwire-validation', 0),
  (33, 'module-bankwire-payment', 0),
  (34, 'module-cashondelivery-validation', 0),
  (35, 'products-comparison', 1),
  (36, 'module-blocknewsletter-verification', 1),
  (37, 'module-cronjobs-callback', 1),
  (38, 'module-smartblog-archive', 1),
  (39, 'module-smartblog-category', 1),
  (40, 'module-smartblog-details', 1),
  (41, 'module-smartblog-search', 1),
  (42, 'module-smartblog-tagpost', 1),
  (43, 'module-tmsearch-search', 1),
  (44, 'module-blockwishlist-mywishlist', 1),
  (45, 'module-blockwishlist-view', 1),
  (46, 'module-loyalty-default', 1),
  (47, 'module-mailalerts-account', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_meta_lang`
--

CREATE TABLE IF NOT EXISTS `ps_meta_lang` (
  `id_meta`     INT(10) UNSIGNED NOT NULL,
  `id_shop`     INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang`     INT(10) UNSIGNED NOT NULL,
  `title`       VARCHAR(128)              DEFAULT NULL,
  `description` VARCHAR(255)              DEFAULT NULL,
  `keywords`    VARCHAR(255)              DEFAULT NULL,
  `url_rewrite` VARCHAR(254)     NOT NULL,
  PRIMARY KEY (`id_meta`, `id_shop`, `id_lang`),
  KEY `id_shop` (`id_shop`),
  KEY `id_lang` (`id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_meta_lang`
--

INSERT INTO `ps_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
  (1, 1, 1, '404 error', 'This page cannot be found', '', 'page-not-found'),
  (2, 1, 1, 'Nejprodávanější', 'Our best sales', '', 'Nejprodavanejsi'),
  (3, 1, 1, 'Napište nám', 'Use our form to contact us', '', 'napiste-nam'),
  (4, 1, 1, '', 'Shop powered by PrestaShop', '', ''),
  (5, 1, 1, 'Výrobci', 'Manufacturers list', '', 'vyrobci'),
  (6, 1, 1, 'Novinky', 'Our new products', '', 'novinky'),
  (7, 1, 1, 'Zapomenuté heslo', 'Enter the e-mail address you use to sign in to receive an e-mail with a new password', '', 'password-recovery'),
  (8, 1, 1, 'Slevy', 'Our special products', '', 'slevy'),
  (9, 1, 1, 'Mapa stránek', 'Lost ? Find what your are looking for', '', 'mapa-stranek'),
  (10, 1, 1, 'Dodavatelé', 'Suppliers list', '', 'dodavatele'),
  (11, 1, 1, 'Adresa', '', '', 'adresa'),
  (12, 1, 1, 'Adresy', '', '', 'adresy'),
  (13, 1, 1, 'Login', '', '', 'login'),
  (14, 1, 1, 'Košík', '', '', 'kosik'),
  (15, 1, 1, 'Sleva', '', '', 'sleva'),
  (16, 1, 1, 'Historie objednávek', '', '', 'historie-objednavek'),
  (17, 1, 1, 'Identity', '', '', 'identity'),
  (18, 1, 1, 'Můj účet', '', '', 'muj-ucet'),
  (19, 1, 1, 'Order follow', '', '', 'order-follow'),
  (20, 1, 1, 'Dobropis', '', '', 'dobropis'),
  (21, 1, 1, 'Objednávka', '', '', 'objednavka'),
  (22, 1, 1, 'Vyhledávání', '', '', 'vyhledavani'),
  (23, 1, 1, 'Prodejny', '', '', 'prodejny'),
  (24, 1, 1, 'Objednávka', '', '', 'objednavka2'),
  (25, 1, 1, 'Sledování objednávky návštěvníka', '', '', 'sledovani-objednavky-navstevnika'),
  (26, 1, 1, 'Potvrzení objednávky', '', '', 'potvrzeni-objednavky'),
  (35, 1, 1, 'Porovnání produktů', '', '', 'porovnani-produktu'),
  (36, 1, 1, '', '', '', ''),
  (37, 1, 1, '', '', '', ''),
  (38, 1, 1, '', '', '', ''),
  (39, 1, 1, '', '', '', ''),
  (40, 1, 1, '', '', '', ''),
  (41, 1, 1, '', '', '', ''),
  (42, 1, 1, '', '', '', ''),
  (43, 1, 1, '', '', '', ''),
  (44, 1, 1, '', '', '', ''),
  (45, 1, 1, '', '', '', ''),
  (46, 1, 1, '', '', '', ''),
  (47, 1, 1, '', '', '', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_module`
--

CREATE TABLE IF NOT EXISTS `ps_module` (
  `id_module` INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `name`      VARCHAR(64)         NOT NULL,
  `active`    TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `version`   VARCHAR(8)          NOT NULL,
  PRIMARY KEY (`id_module`),
  KEY `name` (`name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 107;

--
-- Vypisuji data pro tabulku `ps_module`
--

INSERT INTO `ps_module` (`id_module`, `name`, `active`, `version`) VALUES
  (1, 'socialsharing', 1, '1.4.3'),
  (2, 'blockbanner', 1, '1.4.0'),
  (3, 'bankwire', 1, '1.1.2'),
  (4, 'blockbestsellers', 1, '1.8.1'),
  (5, 'blockcart', 1, '1.6.1'),
  (6, 'blocksocial', 1, '1.2.2'),
  (7, 'blockcategories', 1, '2.9.4'),
  (8, 'blockcurrencies', 1, '0.4.1'),
  (9, 'blockfacebook', 1, '1.4.1'),
  (10, 'blocklanguages', 1, '1.5.1'),
  (11, 'blocklayered', 1, '2.2.0'),
  (12, 'blockcms', 1, '2.1.2'),
  (13, 'blockcmsinfo', 1, '1.6.1'),
  (14, 'blockcontact', 1, '1.4.1'),
  (15, 'blockcontactinfos', 1, '1.2.1'),
  (16, 'blockmanufacturer', 1, '1.4.1'),
  (17, 'blockmyaccount', 1, '1.4.1'),
  (18, 'blockmyaccountfooter', 1, '1.6.1'),
  (19, 'blocknewproducts', 1, '1.10.1'),
  (20, 'blocknewsletter', 1, '2.3.2'),
  (21, 'blockpaymentlogo', 1, '0.4.1'),
  (22, 'blocksearch', 1, '1.7.0'),
  (23, 'blockspecials', 1, '1.3.1'),
  (24, 'blockstore', 1, '1.3.1'),
  (25, 'blocksupplier', 1, '1.2.1'),
  (26, 'blocktags', 1, '1.3.1'),
  (27, 'blocktopmenu', 1, '2.2.4'),
  (28, 'blockuserinfo', 1, '0.4.0'),
  (29, 'blockviewed', 1, '1.3.1'),
  (30, 'cheque', 1, '2.7.2'),
  (31, 'dashactivity', 1, '1.0.0'),
  (32, 'dashtrends', 1, '1.0.0'),
  (33, 'dashgoals', 1, '1.0.0'),
  (34, 'dashproducts', 1, '1.0.0'),
  (35, 'graphnvd3', 1, '1.5.1'),
  (36, 'gridhtml', 1, '1.3.1'),
  (37, 'homeslider', 1, '1.6.1'),
  (38, 'homefeatured', 1, '1.8.1'),
  (39, 'productpaymentlogos', 1, '1.4.1'),
  (40, 'pagesnotfound', 1, '1.5.1'),
  (41, 'sekeywords', 1, '1.4.1'),
  (42, 'statsbestcategories', 1, '1.5.1'),
  (43, 'statsbestcustomers', 1, '1.5.1'),
  (44, 'statsbestproducts', 1, '1.5.1'),
  (45, 'statsbestsuppliers', 1, '1.4.1'),
  (46, 'statsbestvouchers', 1, '1.5.1'),
  (47, 'statscarrier', 1, '1.4.1'),
  (48, 'statscatalog', 1, '1.4.0'),
  (49, 'statscheckup', 1, '1.5.0'),
  (50, 'statsdata', 1, '1.6.2'),
  (51, 'statsequipment', 1, '1.3.1'),
  (52, 'statsforecast', 1, '1.4.1'),
  (53, 'statslive', 1, '1.3.1'),
  (54, 'statsnewsletter', 1, '1.4.2'),
  (55, 'statsorigin', 1, '1.4.1'),
  (56, 'statspersonalinfos', 1, '1.4.1'),
  (57, 'statsproduct', 1, '1.5.0'),
  (58, 'statsregistrations', 1, '1.4.1'),
  (59, 'statssales', 1, '1.3.1'),
  (60, 'statssearch', 1, '1.4.1'),
  (61, 'statsstock', 1, '1.6.0'),
  (62, 'statsvisits', 1, '1.6.1'),
  (63, 'themeconfigurator', 1, '2.1.2'),
  (64, 'gamification', 1, '1.12.3'),
  (65, 'cronjobs', 1, '1.3.4'),
  (67, 'smartblog', 1, '2.0.1'),
  (68, 'smartblogfeed', 1, '2'),
  (69, 'smartblogcategories', 1, '2.0.1'),
  (70, 'smartbloghomelatestnews', 1, '2.0.1'),
  (71, 'smartbloglatestcomments', 1, '2.0.1'),
  (72, 'smartblogaddthisbutton', 1, '2.0.1'),
  (73, 'smartblogpopularposts', 1, '2.0.1'),
  (74, 'smartblogrecentposts', 1, '2.0.1'),
  (75, 'smartblogrelatedposts', 1, '2.0.1'),
  (76, 'smartblogsearch', 1, '2.0.1'),
  (77, 'smartblogtag', 1, '2.0.1'),
  (78, 'smartblogarchive', 1, '2.0.1'),
  (79, 'tmolarkchat', 1, '1.0.2'),
  (80, 'tmsociallogin', 1, '1.2.0'),
  (81, 'tmheaderaccount', 1, '1.1.1'),
  (82, 'tmproductvideos', 1, '1.1.0'),
  (83, 'tmrelatedproducts', 1, '1.0.2'),
  (84, 'tmmegamenu', 1, '1.6.0'),
  (85, 'tmnewsletter', 1, '1.1.0'),
  (86, 'tmlistingimages', 1, '1.0.3'),
  (87, 'tmsearch', 1, '1.0.6'),
  (88, 'tmhtmlcontent', 1, '1.1'),
  (89, 'tmhomecarousel', 1, '1.0.3'),
  (90, 'tmproductsslider', 1, '1.2.3'),
  (91, 'tmcategoryproducts', 1, '0.1.3'),
  (92, 'tmmediaparallax', 1, '2.0.0'),
  (93, 'tmgooglemap', 1, '1.1.4'),
  (94, 'tmsocialfeeds', 1, '1.2.3'),
  (95, 'sampledatainstall', 1, '2.2.0'),
  (96, 'tmmegalayout', 1, '0.0.1'),
  (97, 'sendtoafriend', 1, '1.9.0'),
  (98, 'blockpermanentlinks', 1, '0.3.1'),
  (99, 'crossselling', 1, '1.1.2'),
  (100, 'productscategory', 1, '1.8.1'),
  (101, 'blockwishlist', 1, '1.3.2'),
  (102, 'ganalytics', 1, '2.3.4'),
  (103, 'gsitemap', 1, '3.2.1'),
  (104, 'loyalty', 1, '1.2.9'),
  (105, 'mailalerts', 1, '3.6.1'),
  (106, 'blocksharefb', 1, '1.3.1');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_modules_perfs`
--

CREATE TABLE IF NOT EXISTS `ps_modules_perfs` (
  `id_modules_perfs` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `session`          INT(11) UNSIGNED NOT NULL,
  `module`           VARCHAR(62)      NOT NULL,
  `method`           VARCHAR(126)     NOT NULL,
  `time_start`       DOUBLE UNSIGNED  NOT NULL,
  `time_end`         DOUBLE UNSIGNED  NOT NULL,
  `memory_start`     INT(10) UNSIGNED NOT NULL,
  `memory_end`       INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_modules_perfs`),
  KEY `session` (`session`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_modules_perfs`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_module_access`
--

CREATE TABLE IF NOT EXISTS `ps_module_access` (
  `id_profile` INT(10) UNSIGNED NOT NULL,
  `id_module`  INT(10) UNSIGNED NOT NULL,
  `view`       TINYINT(1)       NOT NULL DEFAULT '0',
  `configure`  TINYINT(1)       NOT NULL DEFAULT '0',
  `uninstall`  TINYINT(1)       NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_profile`, `id_module`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_module_access`
--

INSERT INTO `ps_module_access` (`id_profile`, `id_module`, `view`, `configure`, `uninstall`) VALUES
  (2, 1, 1, 1, 1),
  (2, 2, 1, 1, 1),
  (2, 3, 1, 1, 1),
  (2, 4, 1, 1, 1),
  (2, 5, 1, 1, 1),
  (2, 6, 1, 1, 1),
  (2, 7, 1, 1, 1),
  (2, 8, 1, 1, 1),
  (2, 9, 1, 1, 1),
  (2, 10, 1, 1, 1),
  (2, 11, 1, 1, 1),
  (2, 12, 1, 1, 1),
  (2, 13, 1, 1, 1),
  (2, 14, 1, 1, 1),
  (2, 15, 1, 1, 1),
  (2, 16, 1, 1, 1),
  (2, 17, 1, 1, 1),
  (2, 18, 1, 1, 1),
  (2, 19, 1, 1, 1),
  (2, 20, 1, 1, 1),
  (2, 21, 1, 1, 1),
  (2, 22, 1, 1, 1),
  (2, 23, 1, 1, 1),
  (2, 24, 1, 1, 1),
  (2, 25, 1, 1, 1),
  (2, 26, 1, 1, 1),
  (2, 27, 1, 1, 1),
  (2, 28, 1, 1, 1),
  (2, 29, 1, 1, 1),
  (2, 30, 1, 1, 1),
  (2, 31, 1, 1, 1),
  (2, 32, 1, 1, 1),
  (2, 33, 1, 1, 1),
  (2, 34, 1, 1, 1),
  (2, 35, 1, 1, 1),
  (2, 36, 1, 1, 1),
  (2, 37, 1, 1, 1),
  (2, 38, 1, 1, 1),
  (2, 39, 1, 1, 1),
  (2, 40, 1, 1, 1),
  (2, 41, 1, 1, 1),
  (2, 42, 1, 1, 1),
  (2, 43, 1, 1, 1),
  (2, 44, 1, 1, 1),
  (2, 45, 1, 1, 1),
  (2, 46, 1, 1, 1),
  (2, 47, 1, 1, 1),
  (2, 48, 1, 1, 1),
  (2, 49, 1, 1, 1),
  (2, 50, 1, 1, 1),
  (2, 51, 1, 1, 1),
  (2, 52, 1, 1, 1),
  (2, 53, 1, 1, 1),
  (2, 54, 1, 1, 1),
  (2, 55, 1, 1, 1),
  (2, 56, 1, 1, 1),
  (2, 57, 1, 1, 1),
  (2, 58, 1, 1, 1),
  (2, 59, 1, 1, 1),
  (2, 60, 1, 1, 1),
  (2, 61, 1, 1, 1),
  (2, 62, 1, 1, 1),
  (2, 63, 1, 1, 1),
  (2, 64, 1, 1, 1),
  (2, 65, 1, 1, 1),
  (2, 67, 1, 1, 1),
  (2, 68, 1, 1, 1),
  (2, 69, 1, 1, 1),
  (2, 70, 1, 1, 1),
  (2, 71, 1, 1, 1),
  (2, 72, 1, 1, 1),
  (2, 73, 1, 1, 1),
  (2, 74, 1, 1, 1),
  (2, 75, 1, 1, 1),
  (2, 76, 1, 1, 1),
  (2, 77, 1, 1, 1),
  (2, 78, 1, 1, 1),
  (2, 79, 1, 1, 1),
  (2, 80, 1, 1, 1),
  (2, 81, 1, 1, 1),
  (2, 82, 1, 1, 1),
  (2, 83, 1, 1, 1),
  (2, 84, 1, 1, 1),
  (2, 85, 1, 1, 1),
  (2, 86, 1, 1, 1),
  (2, 87, 1, 1, 1),
  (2, 88, 1, 1, 1),
  (2, 89, 1, 1, 1),
  (2, 90, 1, 1, 1),
  (2, 91, 1, 1, 1),
  (2, 92, 1, 1, 1),
  (2, 93, 1, 1, 1),
  (2, 94, 1, 1, 1),
  (2, 95, 1, 1, 1),
  (2, 96, 1, 1, 1),
  (2, 97, 1, 1, 1),
  (2, 98, 1, 1, 1),
  (2, 99, 1, 1, 1),
  (2, 100, 1, 1, 1),
  (2, 101, 1, 1, 1),
  (2, 102, 1, 1, 1),
  (2, 103, 1, 1, 1),
  (2, 104, 1, 1, 1),
  (2, 105, 1, 1, 1),
  (2, 106, 1, 1, 1),
  (3, 1, 1, 0, 0),
  (3, 2, 1, 0, 0),
  (3, 3, 1, 0, 0),
  (3, 4, 1, 0, 0),
  (3, 5, 1, 0, 0),
  (3, 6, 1, 0, 0),
  (3, 7, 1, 0, 0),
  (3, 8, 1, 0, 0),
  (3, 9, 1, 0, 0),
  (3, 10, 1, 0, 0),
  (3, 11, 1, 0, 0),
  (3, 12, 1, 0, 0),
  (3, 13, 1, 0, 0),
  (3, 14, 1, 0, 0),
  (3, 15, 1, 0, 0),
  (3, 16, 1, 0, 0),
  (3, 17, 1, 0, 0),
  (3, 18, 1, 0, 0),
  (3, 19, 1, 0, 0),
  (3, 20, 1, 0, 0),
  (3, 21, 1, 0, 0),
  (3, 22, 1, 0, 0),
  (3, 23, 1, 0, 0),
  (3, 24, 1, 0, 0),
  (3, 25, 1, 0, 0),
  (3, 26, 1, 0, 0),
  (3, 27, 1, 0, 0),
  (3, 28, 1, 0, 0),
  (3, 29, 1, 0, 0),
  (3, 30, 1, 0, 0),
  (3, 31, 1, 0, 0),
  (3, 32, 1, 0, 0),
  (3, 33, 1, 0, 0),
  (3, 34, 1, 0, 0),
  (3, 35, 1, 0, 0),
  (3, 36, 1, 0, 0),
  (3, 37, 1, 0, 0),
  (3, 38, 1, 0, 0),
  (3, 39, 1, 0, 0),
  (3, 40, 1, 0, 0),
  (3, 41, 1, 0, 0),
  (3, 42, 1, 0, 0),
  (3, 43, 1, 0, 0),
  (3, 44, 1, 0, 0),
  (3, 45, 1, 0, 0),
  (3, 46, 1, 0, 0),
  (3, 47, 1, 0, 0),
  (3, 48, 1, 0, 0),
  (3, 49, 1, 0, 0),
  (3, 50, 1, 0, 0),
  (3, 51, 1, 0, 0),
  (3, 52, 1, 0, 0),
  (3, 53, 1, 0, 0),
  (3, 54, 1, 0, 0),
  (3, 55, 1, 0, 0),
  (3, 56, 1, 0, 0),
  (3, 57, 1, 0, 0),
  (3, 58, 1, 0, 0),
  (3, 59, 1, 0, 0),
  (3, 60, 1, 0, 0),
  (3, 61, 1, 0, 0),
  (3, 62, 1, 0, 0),
  (3, 63, 1, 0, 0),
  (3, 64, 1, 0, 0),
  (3, 65, 1, 0, 0),
  (3, 67, 1, 0, 0),
  (3, 68, 1, 0, 0),
  (3, 69, 1, 0, 0),
  (3, 70, 1, 0, 0),
  (3, 71, 1, 0, 0),
  (3, 72, 1, 0, 0),
  (3, 73, 1, 0, 0),
  (3, 74, 1, 0, 0),
  (3, 75, 1, 0, 0),
  (3, 76, 1, 0, 0),
  (3, 77, 1, 0, 0),
  (3, 78, 1, 0, 0),
  (3, 79, 1, 0, 0),
  (3, 80, 1, 0, 0),
  (3, 81, 1, 0, 0),
  (3, 82, 1, 0, 0),
  (3, 83, 1, 0, 0),
  (3, 84, 1, 0, 0),
  (3, 85, 1, 0, 0),
  (3, 86, 1, 0, 0),
  (3, 87, 1, 0, 0),
  (3, 88, 1, 0, 0),
  (3, 89, 1, 0, 0),
  (3, 90, 1, 0, 0),
  (3, 91, 1, 0, 0),
  (3, 92, 1, 0, 0),
  (3, 93, 1, 0, 0),
  (3, 94, 1, 0, 0),
  (3, 95, 1, 0, 0),
  (3, 96, 1, 0, 0),
  (3, 97, 1, 0, 0),
  (3, 98, 1, 0, 0),
  (3, 99, 1, 0, 0),
  (3, 100, 1, 0, 0),
  (3, 101, 1, 0, 0),
  (3, 102, 1, 0, 0),
  (3, 103, 1, 0, 0),
  (3, 104, 1, 0, 0),
  (3, 105, 1, 0, 0),
  (3, 106, 1, 0, 0),
  (4, 1, 1, 1, 1),
  (4, 2, 1, 1, 1),
  (4, 3, 1, 1, 1),
  (4, 4, 1, 1, 1),
  (4, 5, 1, 1, 1),
  (4, 6, 1, 1, 1),
  (4, 7, 1, 1, 1),
  (4, 8, 1, 1, 1),
  (4, 9, 1, 1, 1),
  (4, 10, 1, 1, 1),
  (4, 11, 1, 1, 1),
  (4, 12, 1, 1, 1),
  (4, 13, 1, 1, 1),
  (4, 14, 1, 1, 1),
  (4, 15, 1, 1, 1),
  (4, 16, 1, 1, 1),
  (4, 17, 1, 1, 1),
  (4, 18, 1, 1, 1),
  (4, 19, 1, 1, 1),
  (4, 20, 1, 1, 1),
  (4, 21, 1, 1, 1),
  (4, 22, 1, 1, 1),
  (4, 23, 1, 1, 1),
  (4, 24, 1, 1, 1),
  (4, 25, 1, 1, 1),
  (4, 26, 1, 1, 1),
  (4, 27, 1, 1, 1),
  (4, 28, 1, 1, 1),
  (4, 29, 1, 1, 1),
  (4, 30, 1, 1, 1),
  (4, 31, 1, 1, 1),
  (4, 32, 1, 1, 1),
  (4, 33, 1, 1, 1),
  (4, 34, 1, 1, 1),
  (4, 35, 1, 1, 1),
  (4, 36, 1, 1, 1),
  (4, 37, 1, 1, 1),
  (4, 38, 1, 1, 1),
  (4, 39, 1, 1, 1),
  (4, 40, 1, 1, 1),
  (4, 41, 1, 1, 1),
  (4, 42, 1, 1, 1),
  (4, 43, 1, 1, 1),
  (4, 44, 1, 1, 1),
  (4, 45, 1, 1, 1),
  (4, 46, 1, 1, 1),
  (4, 47, 1, 1, 1),
  (4, 48, 1, 1, 1),
  (4, 49, 1, 1, 1),
  (4, 50, 1, 1, 1),
  (4, 51, 1, 1, 1),
  (4, 52, 1, 1, 1),
  (4, 53, 1, 1, 1),
  (4, 54, 1, 1, 1),
  (4, 55, 1, 1, 1),
  (4, 56, 1, 1, 1),
  (4, 57, 1, 1, 1),
  (4, 58, 1, 1, 1),
  (4, 59, 1, 1, 1),
  (4, 60, 1, 1, 1),
  (4, 61, 1, 1, 1),
  (4, 62, 1, 1, 1),
  (4, 63, 1, 1, 1),
  (4, 64, 1, 1, 1),
  (4, 65, 1, 1, 1),
  (4, 67, 1, 1, 1),
  (4, 68, 1, 1, 1),
  (4, 69, 1, 1, 1),
  (4, 70, 1, 1, 1),
  (4, 71, 1, 1, 1),
  (4, 72, 1, 1, 1),
  (4, 73, 1, 1, 1),
  (4, 74, 1, 1, 1),
  (4, 75, 1, 1, 1),
  (4, 76, 1, 1, 1),
  (4, 77, 1, 1, 1),
  (4, 78, 1, 1, 1),
  (4, 79, 1, 1, 1),
  (4, 80, 1, 1, 1),
  (4, 81, 1, 1, 1),
  (4, 82, 1, 1, 1),
  (4, 83, 1, 1, 1),
  (4, 84, 1, 1, 1),
  (4, 85, 1, 1, 1),
  (4, 86, 1, 1, 1),
  (4, 87, 1, 1, 1),
  (4, 88, 1, 1, 1),
  (4, 89, 1, 1, 1),
  (4, 90, 1, 1, 1),
  (4, 91, 1, 1, 1),
  (4, 92, 1, 1, 1),
  (4, 93, 1, 1, 1),
  (4, 94, 1, 1, 1),
  (4, 95, 1, 1, 1),
  (4, 96, 1, 1, 1),
  (4, 97, 1, 1, 1),
  (4, 98, 1, 1, 1),
  (4, 99, 1, 1, 1),
  (4, 100, 1, 1, 1),
  (4, 101, 1, 1, 1),
  (4, 102, 1, 1, 1),
  (4, 103, 1, 1, 1),
  (4, 104, 1, 1, 1),
  (4, 105, 1, 1, 1),
  (4, 106, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_module_country`
--

CREATE TABLE IF NOT EXISTS `ps_module_country` (
  `id_module`  INT(10) UNSIGNED NOT NULL,
  `id_shop`    INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_country` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_module`, `id_shop`, `id_country`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_module_country`
--

INSERT INTO `ps_module_country` (`id_module`, `id_shop`, `id_country`) VALUES
  (3, 1, 16),
  (30, 1, 16);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_module_currency`
--

CREATE TABLE IF NOT EXISTS `ps_module_currency` (
  `id_module`   INT(10) UNSIGNED NOT NULL,
  `id_shop`     INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_currency` INT(11)          NOT NULL,
  PRIMARY KEY (`id_module`, `id_shop`, `id_currency`),
  KEY `id_module` (`id_module`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_module_currency`
--

INSERT INTO `ps_module_currency` (`id_module`, `id_shop`, `id_currency`) VALUES
  (3, 1, 1),
  (3, 1, 2),
  (30, 1, 1),
  (30, 1, 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_module_group`
--

CREATE TABLE IF NOT EXISTS `ps_module_group` (
  `id_module` INT(10) UNSIGNED NOT NULL,
  `id_shop`   INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_group`  INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_module`, `id_shop`, `id_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_module_group`
--

INSERT INTO `ps_module_group` (`id_module`, `id_shop`, `id_group`) VALUES
  (1, 1, 1),
  (1, 1, 2),
  (1, 1, 3),
  (2, 1, 1),
  (2, 1, 2),
  (2, 1, 3),
  (3, 1, 1),
  (3, 1, 2),
  (3, 1, 3),
  (4, 1, 1),
  (4, 1, 2),
  (4, 1, 3),
  (5, 1, 1),
  (5, 1, 2),
  (5, 1, 3),
  (6, 1, 1),
  (6, 1, 2),
  (6, 1, 3),
  (7, 1, 1),
  (7, 1, 2),
  (7, 1, 3),
  (8, 1, 1),
  (8, 1, 2),
  (8, 1, 3),
  (9, 1, 1),
  (9, 1, 2),
  (9, 1, 3),
  (10, 1, 1),
  (10, 1, 2),
  (10, 1, 3),
  (11, 1, 1),
  (11, 1, 2),
  (11, 1, 3),
  (12, 1, 1),
  (12, 1, 2),
  (12, 1, 3),
  (13, 1, 1),
  (13, 1, 2),
  (13, 1, 3),
  (14, 1, 1),
  (14, 1, 2),
  (14, 1, 3),
  (15, 1, 1),
  (15, 1, 2),
  (15, 1, 3),
  (16, 1, 1),
  (16, 1, 2),
  (16, 1, 3),
  (17, 1, 1),
  (17, 1, 2),
  (17, 1, 3),
  (18, 1, 1),
  (18, 1, 2),
  (18, 1, 3),
  (19, 1, 1),
  (19, 1, 2),
  (19, 1, 3),
  (20, 1, 1),
  (20, 1, 2),
  (20, 1, 3),
  (21, 1, 1),
  (21, 1, 2),
  (21, 1, 3),
  (22, 1, 1),
  (22, 1, 2),
  (22, 1, 3),
  (23, 1, 1),
  (23, 1, 2),
  (23, 1, 3),
  (24, 1, 1),
  (24, 1, 2),
  (24, 1, 3),
  (25, 1, 1),
  (25, 1, 2),
  (25, 1, 3),
  (26, 1, 1),
  (26, 1, 2),
  (26, 1, 3),
  (27, 1, 1),
  (27, 1, 2),
  (27, 1, 3),
  (28, 1, 1),
  (28, 1, 2),
  (28, 1, 3),
  (29, 1, 1),
  (29, 1, 2),
  (29, 1, 3),
  (30, 1, 1),
  (30, 1, 2),
  (30, 1, 3),
  (31, 1, 1),
  (31, 1, 2),
  (31, 1, 3),
  (32, 1, 1),
  (32, 1, 2),
  (32, 1, 3),
  (33, 1, 1),
  (33, 1, 2),
  (33, 1, 3),
  (34, 1, 1),
  (34, 1, 2),
  (34, 1, 3),
  (35, 1, 1),
  (35, 1, 2),
  (35, 1, 3),
  (36, 1, 1),
  (36, 1, 2),
  (36, 1, 3),
  (37, 1, 1),
  (37, 1, 2),
  (37, 1, 3),
  (38, 1, 1),
  (38, 1, 2),
  (38, 1, 3),
  (39, 1, 1),
  (39, 1, 2),
  (39, 1, 3),
  (40, 1, 1),
  (40, 1, 2),
  (40, 1, 3),
  (41, 1, 1),
  (41, 1, 2),
  (41, 1, 3),
  (42, 1, 1),
  (42, 1, 2),
  (42, 1, 3),
  (43, 1, 1),
  (43, 1, 2),
  (43, 1, 3),
  (44, 1, 1),
  (44, 1, 2),
  (44, 1, 3),
  (45, 1, 1),
  (45, 1, 2),
  (45, 1, 3),
  (46, 1, 1),
  (46, 1, 2),
  (46, 1, 3),
  (47, 1, 1),
  (47, 1, 2),
  (47, 1, 3),
  (48, 1, 1),
  (48, 1, 2),
  (48, 1, 3),
  (49, 1, 1),
  (49, 1, 2),
  (49, 1, 3),
  (50, 1, 1),
  (50, 1, 2),
  (50, 1, 3),
  (51, 1, 1),
  (51, 1, 2),
  (51, 1, 3),
  (52, 1, 1),
  (52, 1, 2),
  (52, 1, 3),
  (53, 1, 1),
  (53, 1, 2),
  (53, 1, 3),
  (54, 1, 1),
  (54, 1, 2),
  (54, 1, 3),
  (55, 1, 1),
  (55, 1, 2),
  (55, 1, 3),
  (56, 1, 1),
  (56, 1, 2),
  (56, 1, 3),
  (57, 1, 1),
  (57, 1, 2),
  (57, 1, 3),
  (58, 1, 1),
  (58, 1, 2),
  (58, 1, 3),
  (59, 1, 1),
  (59, 1, 2),
  (59, 1, 3),
  (60, 1, 1),
  (60, 1, 2),
  (60, 1, 3),
  (61, 1, 1),
  (61, 1, 2),
  (61, 1, 3),
  (62, 1, 1),
  (62, 1, 2),
  (62, 1, 3),
  (63, 1, 1),
  (63, 1, 2),
  (63, 1, 3),
  (64, 1, 1),
  (64, 1, 2),
  (64, 1, 3),
  (65, 1, 1),
  (65, 1, 2),
  (65, 1, 3),
  (67, 1, 1),
  (67, 1, 2),
  (67, 1, 3),
  (68, 1, 1),
  (68, 1, 2),
  (68, 1, 3),
  (69, 1, 1),
  (69, 1, 2),
  (69, 1, 3),
  (70, 1, 1),
  (70, 1, 2),
  (70, 1, 3),
  (71, 1, 1),
  (71, 1, 2),
  (71, 1, 3),
  (72, 1, 1),
  (72, 1, 2),
  (72, 1, 3),
  (73, 1, 1),
  (73, 1, 2),
  (73, 1, 3),
  (74, 1, 1),
  (74, 1, 2),
  (74, 1, 3),
  (75, 1, 1),
  (75, 1, 2),
  (75, 1, 3),
  (76, 1, 1),
  (76, 1, 2),
  (76, 1, 3),
  (77, 1, 1),
  (77, 1, 2),
  (77, 1, 3),
  (78, 1, 1),
  (78, 1, 2),
  (78, 1, 3),
  (79, 1, 1),
  (79, 1, 2),
  (79, 1, 3),
  (80, 1, 1),
  (80, 1, 2),
  (80, 1, 3),
  (81, 1, 1),
  (81, 1, 2),
  (81, 1, 3),
  (82, 1, 1),
  (82, 1, 2),
  (82, 1, 3),
  (83, 1, 1),
  (83, 1, 2),
  (83, 1, 3),
  (84, 1, 1),
  (84, 1, 2),
  (84, 1, 3),
  (85, 1, 1),
  (85, 1, 2),
  (85, 1, 3),
  (86, 1, 1),
  (86, 1, 2),
  (86, 1, 3),
  (87, 1, 1),
  (87, 1, 2),
  (87, 1, 3),
  (88, 1, 1),
  (88, 1, 2),
  (88, 1, 3),
  (89, 1, 1),
  (89, 1, 2),
  (89, 1, 3),
  (90, 1, 1),
  (90, 1, 2),
  (90, 1, 3),
  (91, 1, 1),
  (91, 1, 2),
  (91, 1, 3),
  (92, 1, 1),
  (92, 1, 2),
  (92, 1, 3),
  (93, 1, 1),
  (93, 1, 2),
  (93, 1, 3),
  (94, 1, 1),
  (94, 1, 2),
  (94, 1, 3),
  (95, 1, 1),
  (95, 1, 2),
  (95, 1, 3),
  (96, 1, 1),
  (96, 1, 2),
  (96, 1, 3),
  (97, 1, 1),
  (97, 1, 2),
  (97, 1, 3),
  (98, 1, 1),
  (98, 1, 2),
  (98, 1, 3),
  (99, 1, 1),
  (99, 1, 2),
  (99, 1, 3),
  (100, 1, 1),
  (100, 1, 2),
  (100, 1, 3),
  (101, 1, 1),
  (101, 1, 2),
  (101, 1, 3),
  (102, 1, 1),
  (102, 1, 2),
  (102, 1, 3),
  (103, 1, 1),
  (103, 1, 2),
  (103, 1, 3),
  (104, 1, 1),
  (104, 1, 2),
  (104, 1, 3),
  (105, 1, 1),
  (105, 1, 2),
  (105, 1, 3),
  (106, 1, 1),
  (106, 1, 2),
  (106, 1, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_module_preference`
--

CREATE TABLE IF NOT EXISTS `ps_module_preference` (
  `id_module_preference` INT(11)      NOT NULL AUTO_INCREMENT,
  `id_employee`          INT(11)      NOT NULL,
  `module`               VARCHAR(255) NOT NULL,
  `interest`             TINYINT(1)            DEFAULT NULL,
  `favorite`             TINYINT(1)            DEFAULT NULL,
  PRIMARY KEY (`id_module_preference`),
  UNIQUE KEY `employee_module` (`id_employee`, `module`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 51;

--
-- Vypisuji data pro tabulku `ps_module_preference`
--

INSERT INTO `ps_module_preference` (`id_module_preference`, `id_employee`, `module`, `interest`, `favorite`) VALUES
  (1, 1, 'blockcms', NULL, 1),
  (2, 1, 'themeconfigurator', NULL, 1),
  (3, 1, 'blocktopmenu', NULL, 0),
  (4, 1, 'homeslider', NULL, 1),
  (5, 1, 'bankwire', NULL, 1),
  (6, 1, 'blocklayered', NULL, 1),
  (7, 1, 'blockcategories', NULL, 1),
  (8, 1, 'blockcontact', NULL, 1),
  (9, 1, 'blockcontactinfos', NULL, 1),
  (10, 1, 'blockcart', NULL, 1),
  (11, 1, 'blockmyaccountfooter', NULL, 1),
  (12, 1, 'blockbestsellers', NULL, 1),
  (13, 1, 'blocknewsletter', NULL, 1),
  (14, 1, 'blockstore', NULL, 1),
  (15, 1, 'blocksupplier', NULL, 1),
  (16, 1, 'blockspecials', NULL, 1),
  (17, 1, 'blockmyaccount', NULL, 1),
  (18, 1, 'blocknewproducts', NULL, 1),
  (19, 1, 'blocksocial', NULL, 1),
  (20, 1, 'blockviewed', NULL, 1),
  (21, 1, 'blockpermanentlinks', NULL, 1),
  (22, 1, 'blockcurrencies', NULL, 1),
  (23, 1, 'blockmanufacturer', NULL, 1),
  (24, 1, 'cronjobs', NULL, 1),
  (25, 1, 'homefeatured', NULL, 1),
  (26, 1, 'statscarrier', NULL, 0),
  (27, 1, 'loyalty', NULL, 1),
  (28, 1, 'mailalerts', NULL, 1),
  (29, 1, 'productscategory', NULL, 1),
  (30, 1, 'pagesnotfound', NULL, 1),
  (31, 1, 'tmlistingimages', NULL, 1),
  (32, 1, 'tmrelatedproducts', NULL, 1),
  (33, 1, 'tmsearch', NULL, 1),
  (34, 1, 'tmsociallogin', NULL, 0),
  (35, 1, 'tmcategoryproducts', NULL, 1),
  (36, 1, 'tmgooglemap', NULL, 1),
  (37, 1, 'tmheaderaccount', NULL, 1),
  (38, 1, 'tmhomecarousel', NULL, 1),
  (39, 1, 'tmhtmlcontent', NULL, 1),
  (40, 1, 'tmmediaparallax', NULL, 1),
  (41, 1, 'tmmegalayout', NULL, 1),
  (42, 1, 'tmmegamenu', NULL, 1),
  (43, 1, 'tmnewsletter', NULL, 1),
  (44, 1, 'tmproductsslider', NULL, 1),
  (45, 1, 'tmproductvideos', NULL, 1),
  (46, 1, 'tmsocialfeeds', NULL, 1),
  (47, 1, 'blockcmsinfo', NULL, 1),
  (48, 1, 'blockwishlist', NULL, 1),
  (49, 1, 'cheque', NULL, 1),
  (50, 2, 'tmhomecarousel', NULL, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_module_shop`
--

CREATE TABLE IF NOT EXISTS `ps_module_shop` (
  `id_module`     INT(11) UNSIGNED NOT NULL,
  `id_shop`       INT(11) UNSIGNED NOT NULL,
  `enable_device` TINYINT(1)       NOT NULL DEFAULT '7',
  PRIMARY KEY (`id_module`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_module_shop`
--

INSERT INTO `ps_module_shop` (`id_module`, `id_shop`, `enable_device`) VALUES
  (1, 1, 7),
  (3, 1, 7),
  (4, 1, 7),
  (5, 1, 7),
  (6, 1, 7),
  (7, 1, 7),
  (8, 1, 7),
  (9, 1, 7),
  (11, 1, 7),
  (12, 1, 7),
  (13, 1, 1),
  (14, 1, 7),
  (15, 1, 7),
  (16, 1, 7),
  (17, 1, 7),
  (18, 1, 7),
  (19, 1, 7),
  (20, 1, 7),
  (21, 1, 7),
  (23, 1, 7),
  (24, 1, 7),
  (25, 1, 7),
  (29, 1, 7),
  (30, 1, 7),
  (31, 1, 7),
  (32, 1, 7),
  (33, 1, 7),
  (34, 1, 7),
  (35, 1, 7),
  (36, 1, 7),
  (37, 1, 3),
  (38, 1, 7),
  (40, 1, 7),
  (41, 1, 7),
  (42, 1, 7),
  (43, 1, 7),
  (44, 1, 7),
  (45, 1, 7),
  (46, 1, 7),
  (47, 1, 7),
  (48, 1, 7),
  (49, 1, 7),
  (50, 1, 7),
  (51, 1, 7),
  (52, 1, 7),
  (53, 1, 7),
  (54, 1, 7),
  (55, 1, 7),
  (56, 1, 7),
  (57, 1, 7),
  (58, 1, 7),
  (59, 1, 7),
  (60, 1, 7),
  (61, 1, 7),
  (62, 1, 7),
  (63, 1, 7),
  (64, 1, 7),
  (65, 1, 7),
  (67, 1, 7),
  (68, 1, 7),
  (69, 1, 7),
  (70, 1, 7),
  (71, 1, 7),
  (72, 1, 7),
  (73, 1, 7),
  (74, 1, 7),
  (75, 1, 7),
  (76, 1, 7),
  (77, 1, 7),
  (78, 1, 7),
  (81, 1, 7),
  (82, 1, 7),
  (83, 1, 7),
  (84, 1, 7),
  (85, 1, 7),
  (86, 1, 7),
  (87, 1, 7),
  (88, 1, 7),
  (89, 1, 7),
  (90, 1, 7),
  (91, 1, 7),
  (92, 1, 7),
  (93, 1, 7),
  (94, 1, 7),
  (95, 1, 7),
  (96, 1, 7),
  (97, 1, 7),
  (98, 1, 7),
  (99, 1, 7),
  (100, 1, 7),
  (101, 1, 7),
  (102, 1, 7),
  (103, 1, 7),
  (104, 1, 7),
  (105, 1, 7),
  (106, 1, 7);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_newsletter`
--

CREATE TABLE IF NOT EXISTS `ps_newsletter` (
  `id`                         INT(6)           NOT NULL AUTO_INCREMENT,
  `id_shop`                    INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group`              INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `email`                      VARCHAR(255)     NOT NULL,
  `newsletter_date_add`        DATETIME                  DEFAULT NULL,
  `ip_registration_newsletter` VARCHAR(15)      NOT NULL,
  `http_referer`               VARCHAR(255)              DEFAULT NULL,
  `active`                     TINYINT(1)       NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_newsletter`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_operating_system`
--

CREATE TABLE IF NOT EXISTS `ps_operating_system` (
  `id_operating_system` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`                VARCHAR(64)               DEFAULT NULL,
  PRIMARY KEY (`id_operating_system`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 8;

--
-- Vypisuji data pro tabulku `ps_operating_system`
--

INSERT INTO `ps_operating_system` (`id_operating_system`, `name`) VALUES
  (1, 'Windows XP'),
  (2, 'Windows Vista'),
  (3, 'Windows 7'),
  (4, 'Windows 8'),
  (5, 'MacOsX'),
  (6, 'Linux'),
  (7, 'Android');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_orders`
--

CREATE TABLE IF NOT EXISTS `ps_orders` (
  `id_order`                 INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `reference`                VARCHAR(9)                   DEFAULT NULL,
  `id_shop_group`            INT(11) UNSIGNED    NOT NULL DEFAULT '1',
  `id_shop`                  INT(11) UNSIGNED    NOT NULL DEFAULT '1',
  `id_carrier`               INT(10) UNSIGNED    NOT NULL,
  `id_lang`                  INT(10) UNSIGNED    NOT NULL,
  `id_customer`              INT(10) UNSIGNED    NOT NULL,
  `id_cart`                  INT(10) UNSIGNED    NOT NULL,
  `id_currency`              INT(10) UNSIGNED    NOT NULL,
  `id_address_delivery`      INT(10) UNSIGNED    NOT NULL,
  `id_address_invoice`       INT(10) UNSIGNED    NOT NULL,
  `current_state`            INT(10) UNSIGNED    NOT NULL,
  `secure_key`               VARCHAR(32)         NOT NULL DEFAULT '-1',
  `payment`                  VARCHAR(255)        NOT NULL,
  `conversion_rate`          DECIMAL(13, 6)      NOT NULL DEFAULT '1.000000',
  `module`                   VARCHAR(255)                 DEFAULT NULL,
  `recyclable`               TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift`                     TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift_message`             TEXT,
  `mobile_theme`             TINYINT(1)          NOT NULL DEFAULT '0',
  `shipping_number`          VARCHAR(64)                  DEFAULT NULL,
  `total_discounts`          DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_discounts_tax_incl` DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_discounts_tax_excl` DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_paid`               DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_paid_tax_incl`      DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_paid_tax_excl`      DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_paid_real`          DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_products`           DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_products_wt`        DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_shipping`           DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_incl`  DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_excl`  DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `carrier_tax_rate`         DECIMAL(10, 3)      NOT NULL DEFAULT '0.000',
  `total_wrapping`           DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_incl`  DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_excl`  DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `round_mode`               TINYINT(1)          NOT NULL DEFAULT '2',
  `round_type`               TINYINT(1)          NOT NULL DEFAULT '1',
  `invoice_number`           INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `delivery_number`          INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `invoice_date`             DATETIME            NOT NULL,
  `delivery_date`            DATETIME            NOT NULL,
  `valid`                    INT(1) UNSIGNED     NOT NULL DEFAULT '0',
  `date_add`                 DATETIME            NOT NULL,
  `date_upd`                 DATETIME            NOT NULL,
  PRIMARY KEY (`id_order`),
  KEY `reference` (`reference`),
  KEY `id_customer` (`id_customer`),
  KEY `id_cart` (`id_cart`),
  KEY `invoice_number` (`invoice_number`),
  KEY `id_carrier` (`id_carrier`),
  KEY `id_lang` (`id_lang`),
  KEY `id_currency` (`id_currency`),
  KEY `id_address_delivery` (`id_address_delivery`),
  KEY `id_address_invoice` (`id_address_invoice`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `current_state` (`current_state`),
  KEY `id_shop` (`id_shop`),
  KEY `date_add` (`date_add`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 6;

--
-- Vypisuji data pro tabulku `ps_orders`
--

INSERT INTO `ps_orders` (`id_order`, `reference`, `id_shop_group`, `id_shop`, `id_carrier`, `id_lang`, `id_customer`, `id_cart`, `id_currency`, `id_address_delivery`, `id_address_invoice`, `current_state`, `secure_key`, `payment`, `conversion_rate`, `module`, `recyclable`, `gift`, `gift_message`, `mobile_theme`, `shipping_number`, `total_discounts`, `total_discounts_tax_incl`, `total_discounts_tax_excl`, `total_paid`, `total_paid_tax_incl`, `total_paid_tax_excl`, `total_paid_real`, `total_products`, `total_products_wt`, `total_shipping`, `total_shipping_tax_incl`, `total_shipping_tax_excl`, `carrier_tax_rate`, `total_wrapping`, `total_wrapping_tax_incl`, `total_wrapping_tax_excl`, `round_mode`, `round_type`, `invoice_number`, `delivery_number`, `invoice_date`, `delivery_date`, `valid`, `date_add`, `date_upd`)
VALUES
  (1, 'XKBKNABJK', 1, 1, 2, 1, 1, 1, 1, 4, 4, 6, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', 1.000000,
                                              'cheque', 0, 0, '', 0, '', 0.000000, 0.000000, 0.000000, 55.000000,
                                                                         55.000000, 55.000000, 0.000000, 53.000000,
                                                                         53.000000, 2.000000, 2.000000, 2.000000, 0.000,
                                                                                              0.000000, 0.000000,
                                                                                              0.000000, 0, 0, 0, 0,
   '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2016-08-04 12:42:31', '2016-08-04 12:42:37'),
  (2, 'OHSATSERP', 1, 1, 2, 1, 1, 2, 1, 4, 4, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', 1.000000,
                                              'cheque', 0, 0, '', 0, '', 0.000000, 0.000000, 0.000000, 75.900000,
                                                                         75.900000, 75.900000, 0.000000, 73.900000,
                                                                         73.900000, 2.000000, 2.000000, 2.000000, 0.000,
                                                                                              0.000000, 0.000000,
                                                                                              0.000000, 0, 0, 0, 0,
   '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2016-08-04 12:42:31', '2016-08-04 12:42:37'),
  (3, 'UOYEVOLI', 1, 1, 2, 1, 1, 3, 1, 4, 4, 8, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', 1.000000,
                                             'cheque', 0, 0, '', 0, '', 0.000000, 0.000000, 0.000000, 76.010000,
                                                                        76.010000, 76.010000, 0.000000, 74.010000,
                                                                        74.010000, 2.000000, 2.000000, 2.000000, 0.000,
                                                                                             0.000000, 0.000000,
                                                                                             0.000000, 0, 0, 0, 0,
   '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2016-08-04 12:42:31', '2016-08-04 12:42:37'),
  (4, 'FFATNOMMJ', 1, 1, 2, 1, 1, 4, 1, 4, 4, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', 1.000000,
                                              'cheque', 0, 0, '', 0, '', 0.000000, 0.000000, 0.000000, 89.890000,
                                                                         89.890000, 89.890000, 0.000000, 87.890000,
                                                                         87.890000, 2.000000, 2.000000, 2.000000, 0.000,
                                                                                              0.000000, 0.000000,
                                                                                              0.000000, 0, 0, 0, 0,
   '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2016-08-04 12:42:31', '2016-08-04 12:42:37'),
  (5, 'KHWLILZLL', 1, 1, 2, 1, 1, 5, 1, 4, 4, 10, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Bank wire', 1.000000, 'bankwire',
                                              0, 0, '', 0, '', 0.000000, 0.000000, 0.000000, 71.510000, 71.510000,
                                                               71.510000, 0.000000, 69.510000, 69.510000, 2.000000,
    2.000000, 2.000000, 0.000, 0.000000, 0.000000, 0.000000, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00',
   0, '2016-08-04 12:42:31', '2016-08-04 12:42:37');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_carrier`
--

CREATE TABLE IF NOT EXISTS `ps_order_carrier` (
  `id_order_carrier`       INT(11)          NOT NULL AUTO_INCREMENT,
  `id_order`               INT(11) UNSIGNED NOT NULL,
  `id_carrier`             INT(11) UNSIGNED NOT NULL,
  `id_order_invoice`       INT(11) UNSIGNED          DEFAULT NULL,
  `weight`                 DECIMAL(20, 6)            DEFAULT NULL,
  `shipping_cost_tax_excl` DECIMAL(20, 6)            DEFAULT NULL,
  `shipping_cost_tax_incl` DECIMAL(20, 6)            DEFAULT NULL,
  `tracking_number`        VARCHAR(64)               DEFAULT NULL,
  `date_add`               DATETIME         NOT NULL,
  PRIMARY KEY (`id_order_carrier`),
  KEY `id_order` (`id_order`),
  KEY `id_carrier` (`id_carrier`),
  KEY `id_order_invoice` (`id_order_invoice`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 6;

--
-- Vypisuji data pro tabulku `ps_order_carrier`
--

INSERT INTO `ps_order_carrier` (`id_order_carrier`, `id_order`, `id_carrier`, `id_order_invoice`, `weight`, `shipping_cost_tax_excl`, `shipping_cost_tax_incl`, `tracking_number`, `date_add`)
VALUES
  (1, 1, 2, 0, 0.000000, 2.000000, 2.000000, '', '2016-08-04 12:42:37'),
  (2, 2, 2, 0, 0.000000, 2.000000, 2.000000, '', '2016-08-04 12:42:37'),
  (3, 3, 2, 0, 0.000000, 2.000000, 2.000000, '', '2016-08-04 12:42:37'),
  (4, 4, 2, 0, 0.000000, 2.000000, 2.000000, '', '2016-08-04 12:42:37'),
  (5, 5, 2, 0, 0.000000, 2.000000, 2.000000, '', '2016-08-04 12:42:37');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_cart_rule`
--

CREATE TABLE IF NOT EXISTS `ps_order_cart_rule` (
  `id_order_cart_rule` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_order`           INT(10) UNSIGNED NOT NULL,
  `id_cart_rule`       INT(10) UNSIGNED NOT NULL,
  `id_order_invoice`   INT(10) UNSIGNED          DEFAULT '0',
  `name`               VARCHAR(254)     NOT NULL,
  `value`              DECIMAL(17, 2)   NOT NULL DEFAULT '0.00',
  `value_tax_excl`     DECIMAL(17, 2)   NOT NULL DEFAULT '0.00',
  `free_shipping`      TINYINT(1)       NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_order_cart_rule`),
  KEY `id_order` (`id_order`),
  KEY `id_cart_rule` (`id_cart_rule`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_order_cart_rule`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_detail`
--

CREATE TABLE IF NOT EXISTS `ps_order_detail` (
  `id_order_detail`               INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_order`                      INT(10) UNSIGNED    NOT NULL,
  `id_order_invoice`              INT(11)                      DEFAULT NULL,
  `id_warehouse`                  INT(10) UNSIGNED             DEFAULT '0',
  `id_shop`                       INT(11) UNSIGNED    NOT NULL,
  `product_id`                    INT(10) UNSIGNED    NOT NULL,
  `product_attribute_id`          INT(10) UNSIGNED             DEFAULT NULL,
  `product_name`                  VARCHAR(255)        NOT NULL,
  `product_quantity`              INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `product_quantity_in_stock`     INT(10)             NOT NULL DEFAULT '0',
  `product_quantity_refunded`     INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `product_quantity_return`       INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `product_quantity_reinjected`   INT(10) UNSIGNED    NOT NULL DEFAULT '0',
  `product_price`                 DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `reduction_percent`             DECIMAL(10, 2)      NOT NULL DEFAULT '0.00',
  `reduction_amount`              DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `reduction_amount_tax_incl`     DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `reduction_amount_tax_excl`     DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `group_reduction`               DECIMAL(10, 2)      NOT NULL DEFAULT '0.00',
  `product_quantity_discount`     DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `product_ean13`                 VARCHAR(13)                  DEFAULT NULL,
  `product_upc`                   VARCHAR(12)                  DEFAULT NULL,
  `product_reference`             VARCHAR(32)                  DEFAULT NULL,
  `product_supplier_reference`    VARCHAR(32)                  DEFAULT NULL,
  `product_weight`                DECIMAL(20, 6)      NOT NULL,
  `id_tax_rules_group`            INT(11) UNSIGNED             DEFAULT '0',
  `tax_computation_method`        TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `tax_name`                      VARCHAR(16)         NOT NULL,
  `tax_rate`                      DECIMAL(10, 3)      NOT NULL DEFAULT '0.000',
  `ecotax`                        DECIMAL(21, 6)      NOT NULL DEFAULT '0.000000',
  `ecotax_tax_rate`               DECIMAL(5, 3)       NOT NULL DEFAULT '0.000',
  `discount_quantity_applied`     TINYINT(1)          NOT NULL DEFAULT '0',
  `download_hash`                 VARCHAR(255)                 DEFAULT NULL,
  `download_nb`                   INT(10) UNSIGNED             DEFAULT '0',
  `download_deadline`             DATETIME                     DEFAULT NULL,
  `total_price_tax_incl`          DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_price_tax_excl`          DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `unit_price_tax_incl`           DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `unit_price_tax_excl`           DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_shipping_price_tax_incl` DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `total_shipping_price_tax_excl` DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `purchase_supplier_price`       DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `original_product_price`        DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  `original_wholesale_price`      DECIMAL(20, 6)      NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id_order_detail`),
  KEY `order_detail_order` (`id_order`),
  KEY `product_id` (`product_id`),
  KEY `product_attribute_id` (`product_attribute_id`),
  KEY `id_tax_rules_group` (`id_tax_rules_group`),
  KEY `id_order_id_order_detail` (`id_order`, `id_order_detail`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 16;

--
-- Vypisuji data pro tabulku `ps_order_detail`
--

INSERT INTO `ps_order_detail` (`id_order_detail`, `id_order`, `id_order_invoice`, `id_warehouse`, `id_shop`, `product_id`, `product_attribute_id`, `product_name`, `product_quantity`, `product_quantity_in_stock`, `product_quantity_refunded`, `product_quantity_return`, `product_quantity_reinjected`, `product_price`, `reduction_percent`, `reduction_amount`, `reduction_amount_tax_incl`, `reduction_amount_tax_excl`, `group_reduction`, `product_quantity_discount`, `product_ean13`, `product_upc`, `product_reference`, `product_supplier_reference`, `product_weight`, `id_tax_rules_group`, `tax_computation_method`, `tax_name`, `tax_rate`, `ecotax`, `ecotax_tax_rate`, `discount_quantity_applied`, `download_hash`, `download_nb`, `download_deadline`, `total_price_tax_incl`, `total_price_tax_excl`, `unit_price_tax_incl`, `unit_price_tax_excl`, `total_shipping_price_tax_incl`, `total_shipping_price_tax_excl`, `purchase_supplier_price`, `original_product_price`, `original_wholesale_price`)
VALUES
  (1, 1, 0, 0, 1, 2, 10, 'Blouse - Color : White, Size : M', 1, 1, 0, 0, 0, 26.999852, 0.00, 0.000000, 0.000000,
                                                                      0.000000, 0.00, 0.000000, '', '', 'demo_2', '',
                                                                                                    0.000000, 0, 0, '',
                                                                                                    0.000, 0.000000,
                                                                                                    0.000, 0, '', 0,
                                                                                                           '0000-00-00 00:00:00',
                                                                                                           27.000000,
                                                                                                           27.000000,
                                                                                                           27.000000,
                                                                                                           27.000000,
                                                                                                           0.000000,
                                                                                                           0.000000,
   0.000000, 26.999852, 8.100000),
  (2, 1, 0, 0, 1, 3, 13, 'Printed Dress - Color : Orange, Size : S', 1, 1, 0, 0, 0, 25.999852, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_3', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 26.000000, 26.000000, 26.000000, 26.000000, 0.000000, 0.000000, 0.000000, 25.999852, 7.800000),
  (3, 2, 0, 0, 1, 2, 10, 'Blouse - Color : White, Size : M', 1, 1, 0, 0, 0, 26.999852, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_2', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 27.000000, 27.000000, 27.000000, 27.000000, 0.000000, 0.000000, 0.000000, 26.999852, 8.100000),
  (4, 2, 0, 0, 1, 6, 32, 'Printed Summer Dress - Color : Yellow, Size : M', 1, 1, 0, 0, 0, 30.502569, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_6', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 30.500000, 30.500000, 30.500000, 30.500000, 0.000000, 0.000000, 0.000000, 30.502569, 9.150000),
  (5, 2, 0, 0, 1, 7, 34, 'Printed Chiffon Dress - Color : Yellow, Size : S', 1, 1, 0, 0, 0, 20.501236, 20.00, 0.000000, 0.000000, 0.000000, 0.00, 17.400000, '', '', 'demo_7', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 16.400000, 16.400000, 16.400000, 16.400000, 0.000000, 0.000000, 0.000000, 20.501236, 6.150000),
  (6, 3, 0, 0, 1, 1, 1, 'Faded Short Sleeve T-shirts - Color : Orange, Size : S', 1, 1, 0, 0, 0, 16.510000, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_1', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 16.510000, 16.510000, 16.510000, 16.510000, 0.000000, 0.000000, 0.000000, 16.510000, 4.950000),
  (7, 3, 0, 0, 1, 2, 10, 'Blouse - Color : White, Size : M', 1, 1, 0, 0, 0, 26.999852, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_2', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 27.000000, 27.000000, 27.000000, 27.000000, 0.000000, 0.000000, 0.000000, 26.999852, 8.100000),
  (8, 3, 0, 0, 1, 6, 32, 'Printed Summer Dress - Color : Yellow, Size : M', 1, 1, 0, 0, 0, 30.502569, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_6', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 30.500000, 30.500000, 30.500000, 30.500000, 0.000000, 0.000000, 0.000000, 30.502569, 9.150000),
  (9, 4, 0, 0, 1, 1, 1, 'Faded Short Sleeve T-shirts - Color : Orange, Size : S', 1, 1, 0, 0, 0, 16.510000, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_1', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 16.510000, 16.510000, 16.510000, 16.510000, 0.000000, 0.000000, 0.000000, 16.510000, 4.950000),
  (10, 4, 0, 0, 1, 3, 13, 'Printed Dress - Color : Orange, Size : S', 1, 1, 0, 0, 0, 25.999852, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_3', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 26.000000, 26.000000, 26.000000, 26.000000, 0.000000, 0.000000, 0.000000, 25.999852, 7.800000),
  (11, 4, 0, 0, 1, 5, 19, 'Printed Summer Dress - Color : Yellow, Size : S', 1, 1, 0, 0, 0, 30.506321, 5.00, 0.000000, 0.000000, 0.000000, 0.00, 29.980000, '', '', 'demo_5', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 28.980000, 28.980000, 28.980000, 28.980000, 0.000000, 0.000000, 0.000000, 30.506321, 9.150000),
  (12, 4, 0, 0, 1, 7, 34, 'Printed Chiffon Dress - Color : Yellow, Size : S', 1, 1, 0, 0, 0, 20.501236, 20.00, 0.000000,
                                                                                       0.000000, 0.000000, 0.00,
                                                                                       17.400000, '', '', 'demo_7', '',
                                                                                                      0.000000, 0, 0,
                                                                                                      '', 0.000,
                                                                                                      0.000000, 0.000,
    0, '', 0, '0000-00-00 00:00:00', 16.400000, 16.400000, 16.400000, 16.400000, 0.000000, 0.000000, 0.000000,
   20.501236, 6.150000),
  (13, 5, 0, 0, 1, 1, 1, 'Faded Short Sleeve T-shirts - Color : Orange, Size : S', 1, 1, 0, 0, 0, 16.510000, 0.00,
                                                                                            0.000000, 0.000000,
                                                                                            0.000000, 0.00, 0.000000,
                                                                                            '', '', 'demo_1', '',
                                                                                                0.000000, 0, 0, '',
                                                                                                0.000, 0.000000, 0.000,
    0, '', 0, '0000-00-00 00:00:00', 16.510000, 16.510000, 16.510000, 16.510000, 0.000000, 0.000000, 0.000000,
   16.510000, 4.950000),
  (14, 5, 0, 0, 1, 2, 7, 'Blouse - Color : Black, Size : S', 1, 1, 0, 0, 0, 26.999852, 0.00, 0.000000, 0.000000,
                                                                      0.000000, 0.00, 0.000000, '', '', 'demo_2', '',
                                                                                                    0.000000, 0, 0, '',
                                                                                                    0.000, 0.000000,
                                                                                                    0.000, 0, '', 0,
                                                                                                           '0000-00-00 00:00:00',
                                                                                                           27.000000,
                                                                                                           27.000000,
                                                                                                           27.000000,
                                                                                                           27.000000,
                                                                                                           0.000000,
                                                                                                           0.000000,
   0.000000, 26.999852, 8.100000),
  (15, 5, 0, 0, 1, 3, 13, 'Printed Dress - Color : Orange, Size : S', 1, 1, 0, 0, 0, 25.999852, 0.00, 0.000000,
                                                                               0.000000, 0.000000, 0.00, 0.000000, '',
    '', 'demo_3', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 26.000000, 26.000000,
                                                                  26.000000, 26.000000, 0.000000, 0.000000, 0.000000,
   25.999852, 7.800000);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_detail_tax`
--

CREATE TABLE IF NOT EXISTS `ps_order_detail_tax` (
  `id_order_detail` INT(11)        NOT NULL,
  `id_tax`          INT(11)        NOT NULL,
  `unit_amount`     DECIMAL(16, 6) NOT NULL DEFAULT '0.000000',
  `total_amount`    DECIMAL(16, 6) NOT NULL DEFAULT '0.000000',
  KEY `id_order_detail` (`id_order_detail`),
  KEY `id_tax` (`id_tax`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_order_detail_tax`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_history`
--

CREATE TABLE IF NOT EXISTS `ps_order_history` (
  `id_order_history` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_employee`      INT(10) UNSIGNED NOT NULL,
  `id_order`         INT(10) UNSIGNED NOT NULL,
  `id_order_state`   INT(10) UNSIGNED NOT NULL,
  `date_add`         DATETIME         NOT NULL,
  PRIMARY KEY (`id_order_history`),
  KEY `order_history_order` (`id_order`),
  KEY `id_employee` (`id_employee`),
  KEY `id_order_state` (`id_order_state`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 8;

--
-- Vypisuji data pro tabulku `ps_order_history`
--

INSERT INTO `ps_order_history` (`id_order_history`, `id_employee`, `id_order`, `id_order_state`, `date_add`) VALUES
  (1, 0, 1, 1, '2016-08-04 12:42:37'),
  (2, 0, 2, 1, '2016-08-04 12:42:37'),
  (3, 0, 3, 1, '2016-08-04 12:42:37'),
  (4, 0, 4, 1, '2016-08-04 12:42:37'),
  (5, 0, 5, 10, '2016-08-04 12:42:37'),
  (6, 1, 1, 6, '2016-08-04 12:42:37'),
  (7, 1, 3, 8, '2016-08-04 12:42:37');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_invoice`
--

CREATE TABLE IF NOT EXISTS `ps_order_invoice` (
  `id_order_invoice`                INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_order`                        INT(11)          NOT NULL,
  `number`                          INT(11)          NOT NULL,
  `delivery_number`                 INT(11)          NOT NULL,
  `delivery_date`                   DATETIME                  DEFAULT NULL,
  `total_discount_tax_excl`         DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `total_discount_tax_incl`         DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `total_paid_tax_excl`             DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `total_paid_tax_incl`             DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `total_products`                  DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `total_products_wt`               DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_excl`         DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_incl`         DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `shipping_tax_computation_method` INT(10) UNSIGNED NOT NULL,
  `total_wrapping_tax_excl`         DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_incl`         DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `shop_address`                    TEXT,
  `invoice_address`                 TEXT,
  `delivery_address`                TEXT,
  `note`                            TEXT,
  `date_add`                        DATETIME         NOT NULL,
  PRIMARY KEY (`id_order_invoice`),
  KEY `id_order` (`id_order`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_order_invoice`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_invoice_payment`
--

CREATE TABLE IF NOT EXISTS `ps_order_invoice_payment` (
  `id_order_invoice` INT(11) UNSIGNED NOT NULL,
  `id_order_payment` INT(11) UNSIGNED NOT NULL,
  `id_order`         INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_order_invoice`, `id_order_payment`),
  KEY `order_payment` (`id_order_payment`),
  KEY `id_order` (`id_order`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_order_invoice_payment`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_invoice_tax`
--

CREATE TABLE IF NOT EXISTS `ps_order_invoice_tax` (
  `id_order_invoice` INT(11)        NOT NULL,
  `type`             VARCHAR(15)    NOT NULL,
  `id_tax`           INT(11)        NOT NULL,
  `amount`           DECIMAL(10, 6) NOT NULL DEFAULT '0.000000',
  KEY `id_tax` (`id_tax`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_order_invoice_tax`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_message`
--

CREATE TABLE IF NOT EXISTS `ps_order_message` (
  `id_order_message` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date_add`         DATETIME         NOT NULL,
  PRIMARY KEY (`id_order_message`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_order_message`
--

INSERT INTO `ps_order_message` (`id_order_message`, `date_add`) VALUES
  (1, '2016-08-04 12:42:35');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_message_lang`
--

CREATE TABLE IF NOT EXISTS `ps_order_message_lang` (
  `id_order_message` INT(10) UNSIGNED NOT NULL,
  `id_lang`          INT(10) UNSIGNED NOT NULL,
  `name`             VARCHAR(128)     NOT NULL,
  `message`          TEXT             NOT NULL,
  PRIMARY KEY (`id_order_message`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_order_message_lang`
--

INSERT INTO `ps_order_message_lang` (`id_order_message`, `id_lang`, `name`, `message`) VALUES
  (1, 1, 'Delay',
   'Hi,\n\nUnfortunately, an item on your order is currently out of stock. This may cause a slight delay in delivery.\nPlease accept our apologies and rest assured that we are working hard to rectify this.\n\nBest regards,');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_payment`
--

CREATE TABLE IF NOT EXISTS `ps_order_payment` (
  `id_order_payment` INT(11)          NOT NULL AUTO_INCREMENT,
  `order_reference`  VARCHAR(9)                DEFAULT NULL,
  `id_currency`      INT(10) UNSIGNED NOT NULL,
  `amount`           DECIMAL(10, 2)   NOT NULL,
  `payment_method`   VARCHAR(255)     NOT NULL,
  `conversion_rate`  DECIMAL(13, 6)   NOT NULL DEFAULT '1.000000',
  `transaction_id`   VARCHAR(254)              DEFAULT NULL,
  `card_number`      VARCHAR(254)              DEFAULT NULL,
  `card_brand`       VARCHAR(254)              DEFAULT NULL,
  `card_expiration`  CHAR(7)                   DEFAULT NULL,
  `card_holder`      VARCHAR(254)              DEFAULT NULL,
  `date_add`         DATETIME         NOT NULL,
  PRIMARY KEY (`id_order_payment`),
  KEY `order_reference` (`order_reference`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_order_payment`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_return`
--

CREATE TABLE IF NOT EXISTS `ps_order_return` (
  `id_order_return` INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_customer`     INT(10) UNSIGNED    NOT NULL,
  `id_order`        INT(10) UNSIGNED    NOT NULL,
  `state`           TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `question`        TEXT                NOT NULL,
  `date_add`        DATETIME            NOT NULL,
  `date_upd`        DATETIME            NOT NULL,
  PRIMARY KEY (`id_order_return`),
  KEY `order_return_customer` (`id_customer`),
  KEY `id_order` (`id_order`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_order_return`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_return_detail`
--

CREATE TABLE IF NOT EXISTS `ps_order_return_detail` (
  `id_order_return`  INT(10) UNSIGNED NOT NULL,
  `id_order_detail`  INT(10) UNSIGNED NOT NULL,
  `id_customization` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_quantity` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_order_return`, `id_order_detail`, `id_customization`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_order_return_detail`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_return_state`
--

CREATE TABLE IF NOT EXISTS `ps_order_return_state` (
  `id_order_return_state` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `color`                 VARCHAR(32)               DEFAULT NULL,
  PRIMARY KEY (`id_order_return_state`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 6;

--
-- Vypisuji data pro tabulku `ps_order_return_state`
--

INSERT INTO `ps_order_return_state` (`id_order_return_state`, `color`) VALUES
  (1, '#4169E1'),
  (2, '#8A2BE2'),
  (3, '#32CD32'),
  (4, '#DC143C'),
  (5, '#108510');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_return_state_lang`
--

CREATE TABLE IF NOT EXISTS `ps_order_return_state_lang` (
  `id_order_return_state` INT(10) UNSIGNED NOT NULL,
  `id_lang`               INT(10) UNSIGNED NOT NULL,
  `name`                  VARCHAR(64)      NOT NULL,
  PRIMARY KEY (`id_order_return_state`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_order_return_state_lang`
--

INSERT INTO `ps_order_return_state_lang` (`id_order_return_state`, `id_lang`, `name`) VALUES
  (1, 1, 'Waiting for confirmation'),
  (2, 1, 'Waiting for package'),
  (3, 1, 'Package received'),
  (4, 1, 'Return denied'),
  (5, 1, 'Return completed');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_slip`
--

CREATE TABLE IF NOT EXISTS `ps_order_slip` (
  `id_order_slip`           INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `conversion_rate`         DECIMAL(13, 6)      NOT NULL DEFAULT '1.000000',
  `id_customer`             INT(10) UNSIGNED    NOT NULL,
  `id_order`                INT(10) UNSIGNED    NOT NULL,
  `total_products_tax_excl` DECIMAL(20, 6)               DEFAULT NULL,
  `total_products_tax_incl` DECIMAL(20, 6)               DEFAULT NULL,
  `total_shipping_tax_excl` DECIMAL(20, 6)               DEFAULT NULL,
  `total_shipping_tax_incl` DECIMAL(20, 6)               DEFAULT NULL,
  `shipping_cost`           TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `amount`                  DECIMAL(10, 2)      NOT NULL,
  `shipping_cost_amount`    DECIMAL(10, 2)      NOT NULL,
  `partial`                 TINYINT(1)          NOT NULL,
  `order_slip_type`         TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add`                DATETIME            NOT NULL,
  `date_upd`                DATETIME            NOT NULL,
  PRIMARY KEY (`id_order_slip`),
  KEY `order_slip_customer` (`id_customer`),
  KEY `id_order` (`id_order`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_order_slip`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_slip_detail`
--

CREATE TABLE IF NOT EXISTS `ps_order_slip_detail` (
  `id_order_slip`        INT(10) UNSIGNED NOT NULL,
  `id_order_detail`      INT(10) UNSIGNED NOT NULL,
  `product_quantity`     INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `unit_price_tax_excl`  DECIMAL(20, 6)            DEFAULT NULL,
  `unit_price_tax_incl`  DECIMAL(20, 6)            DEFAULT NULL,
  `total_price_tax_excl` DECIMAL(20, 6)            DEFAULT NULL,
  `total_price_tax_incl` DECIMAL(20, 6)            DEFAULT NULL,
  `amount_tax_excl`      DECIMAL(20, 6)            DEFAULT NULL,
  `amount_tax_incl`      DECIMAL(20, 6)            DEFAULT NULL,
  PRIMARY KEY (`id_order_slip`, `id_order_detail`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_order_slip_detail`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_slip_detail_tax`
--

CREATE TABLE IF NOT EXISTS `ps_order_slip_detail_tax` (
  `id_order_slip_detail` INT(11) UNSIGNED NOT NULL,
  `id_tax`               INT(11) UNSIGNED NOT NULL,
  `unit_amount`          DECIMAL(16, 6)   NOT NULL DEFAULT '0.000000',
  `total_amount`         DECIMAL(16, 6)   NOT NULL DEFAULT '0.000000',
  KEY `id_order_slip_detail` (`id_order_slip_detail`),
  KEY `id_tax` (`id_tax`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_order_slip_detail_tax`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_state`
--

CREATE TABLE IF NOT EXISTS `ps_order_state` (
  `id_order_state` INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `invoice`        TINYINT(1) UNSIGNED          DEFAULT '0',
  `send_email`     TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `module_name`    VARCHAR(255)                 DEFAULT NULL,
  `color`          VARCHAR(32)                  DEFAULT NULL,
  `unremovable`    TINYINT(1) UNSIGNED NOT NULL,
  `hidden`         TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `logable`        TINYINT(1)          NOT NULL DEFAULT '0',
  `delivery`       TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `shipped`        TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `paid`           TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `pdf_invoice`    TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `pdf_delivery`   TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `deleted`        TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_order_state`),
  KEY `module_name` (`module_name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 15;

--
-- Vypisuji data pro tabulku `ps_order_state`
--

INSERT INTO `ps_order_state` (`id_order_state`, `invoice`, `send_email`, `module_name`, `color`, `unremovable`, `hidden`, `logable`, `delivery`, `shipped`, `paid`, `pdf_invoice`, `pdf_delivery`, `deleted`)
VALUES
  (1, 0, 1, 'cheque', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0),
  (2, 1, 1, '', '#32CD32', 1, 0, 1, 0, 0, 1, 1, 0, 0),
  (3, 1, 1, '', '#FF8C00', 1, 0, 1, 1, 0, 1, 0, 0, 0),
  (4, 1, 1, '', '#8A2BE2', 1, 0, 1, 1, 1, 1, 0, 0, 0),
  (5, 1, 0, '', '#108510', 1, 0, 1, 1, 1, 1, 0, 0, 0),
  (6, 0, 1, '', '#DC143C', 1, 0, 0, 0, 0, 0, 0, 0, 0),
  (7, 1, 1, '', '#ec2e15', 1, 0, 0, 0, 0, 0, 0, 0, 0),
  (8, 0, 1, '', '#8f0621', 1, 0, 0, 0, 0, 0, 0, 0, 0),
  (9, 1, 1, '', '#FF69B4', 1, 0, 0, 0, 0, 1, 0, 0, 0),
  (10, 0, 1, 'bankwire', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0),
  (11, 0, 0, '', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0),
  (12, 1, 1, '', '#32CD32', 1, 0, 1, 0, 0, 1, 0, 0, 0),
  (13, 0, 1, '', '#FF69B4', 1, 0, 0, 0, 0, 0, 0, 0, 0),
  (14, 0, 0, 'cashondelivery', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_order_state_lang`
--

CREATE TABLE IF NOT EXISTS `ps_order_state_lang` (
  `id_order_state` INT(10) UNSIGNED NOT NULL,
  `id_lang`        INT(10) UNSIGNED NOT NULL,
  `name`           VARCHAR(64)      NOT NULL,
  `template`       VARCHAR(64)      NOT NULL,
  PRIMARY KEY (`id_order_state`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_order_state_lang`
--

INSERT INTO `ps_order_state_lang` (`id_order_state`, `id_lang`, `name`, `template`) VALUES
  (1, 1, 'Čeká se na platbu šekem', 'cheque'),
  (2, 1, 'Platba byla přijata', 'payment'),
  (3, 1, 'Probíhá příprava', 'preparation'),
  (4, 1, 'Odesláno', 'shipped'),
  (5, 1, 'Dodáno', ''),
  (6, 1, 'Zrušeno', 'order_canceled'),
  (7, 1, 'Vracení produktů', 'refund'),
  (8, 1, 'Chyba platby', 'payment_error'),
  (9, 1, 'On backorder (paid)', 'outofstock'),
  (10, 1, 'Awaiting bank wire payment', 'bankwire'),
  (11, 1, 'Awaiting PayPal payment', ''),
  (12, 1, 'Remote payment accepted', 'payment'),
  (13, 1, 'On backorder (not paid)', 'outofstock'),
  (14, 1, 'Awaiting Cash On Delivery validation', 'cashondelivery');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_pack`
--

CREATE TABLE IF NOT EXISTS `ps_pack` (
  `id_product_pack`           INT(10) UNSIGNED NOT NULL,
  `id_product_item`           INT(10) UNSIGNED NOT NULL,
  `id_product_attribute_item` INT(10) UNSIGNED NOT NULL,
  `quantity`                  INT(10) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_product_pack`, `id_product_item`, `id_product_attribute_item`),
  KEY `product_item` (`id_product_item`, `id_product_attribute_item`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_pack`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_page`
--

CREATE TABLE IF NOT EXISTS `ps_page` (
  `id_page`      INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_page_type` INT(10) UNSIGNED NOT NULL,
  `id_object`    INT(10) UNSIGNED          DEFAULT NULL,
  PRIMARY KEY (`id_page`),
  KEY `id_page_type` (`id_page_type`),
  KEY `id_object` (`id_object`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 3;

--
-- Vypisuji data pro tabulku `ps_page`
--

INSERT INTO `ps_page` (`id_page`, `id_page_type`, `id_object`) VALUES
  (1, 1, NULL),
  (2, 2, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_pagenotfound`
--

CREATE TABLE IF NOT EXISTS `ps_pagenotfound` (
  `id_pagenotfound` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop`         INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group`   INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `request_uri`     VARCHAR(256)     NOT NULL,
  `http_referer`    VARCHAR(256)     NOT NULL,
  `date_add`        DATETIME         NOT NULL,
  PRIMARY KEY (`id_pagenotfound`),
  KEY `date_add` (`date_add`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 37;

--
-- Vypisuji data pro tabulku `ps_pagenotfound`
--

INSERT INTO `ps_pagenotfound` (`id_pagenotfound`, `id_shop`, `id_shop_group`, `request_uri`, `http_referer`, `date_add`)
VALUES
  (1, 1, 1, '/img/levne-kontaktni-cocky-oftex-logo-1474379302.jpg%20wid', 'http://oftex.z-web-3.zcom.cz/',
   '2016-09-20 15:48:56'),
  (2, 1, 1, '/img/levne-kontaktni-cocky-oftex-logo-1474379302.jpg%20width', 'http://oftex.z-web-3.zcom.cz/', '2016-09-20 15:48:57'),
  (3, 1, 1, '/module/blockwishlist/mywishlist', 'http://oftex.z-web-3.zcom.cz/mapa-stranek', '2016-09-20 16:33:12'),
  (4, 1, 1, '/sf-with-ul', 'http://oftex.z-web-3.zcom.cz/', '2016-09-20 16:58:59'),
  (5, 1, 1, '/sf-with-ul', 'http://oftex.z-web-3.zcom.cz/', '2016-10-05 10:53:33'),
  (6, 1, 1, '/module/blockwishlist/mywishlist', 'http://oftex.z-web-3.zcom.cz/', '2016-10-05 11:07:06'),
  (7, 1, 1, '/sf-with-ul', 'http://oftex.z-web-3.zcom.cz/', '2016-10-05 11:14:53'),
  (8, 1, 1, '/sf-with-ul', 'http://oftex.z-web-3.zcom.cz/', '2016-10-05 11:43:12'),
  (9, 1, 1, '/www.parexpo.cz', 'http://oftex.z-web-3.zcom.cz/', '2016-10-05 11:50:35'),
  (10, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-05 13:23:41'),
  (11, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-05 13:23:42'),
  (12, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=default-bootstrap&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-05 13:25:32'),
  (13, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=default-bootstrap&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-05 13:25:36'),
  (14, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=default-bootstrap&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-05 13:25:43'),
  (15, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=default-bootstrap&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-05 13:25:44'),
  (16, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=default-bootstrap&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-05 13:25:52'),
  (17, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=default-bootstrap&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-05 13:25:53'),
  (18, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-05 13:39:04'),
  (19, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-05 13:39:05'),
  (20, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', '', '2016-10-05 13:39:17'),
  (21, 1, 1, '/sf-with-ul', 'http://oftex.z-web-3.zcom.cz/', '2016-10-06 08:18:02'),
  (22, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-13 08:35:33'),
  (23, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-13 08:35:34'),
  (24, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-13 08:36:29'),
  (25, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-13 08:38:09'),
  (26, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd', '2016-10-13 08:38:10'),
  (27, 1, 1, '/mails/cs/%7Bshop_logo%7D', 'http://oftex.z-web-3.zcom.cz/mails/cs/cheque.html', '2016-10-13 08:38:57'),
  (28, 1, 1, '/sf-with-ul', 'http://oftex.z-web-3.zcom.cz/objednavka', '2016-10-13 08:46:21'),
  (29, 1, 1, '/data/www/prodej-cocek.cz/public_html/modules/sampledatainstall/output/', 'http://oftex.z-web-3.zcom.cz/admin255znjcdp/index.php?controller=AdminSampleDataInstallExport&token=33ba84e106a78b6904256515a748577d', '2017-01-23 18:32:30'),
  (30, 1, 1, '/admin', '', '2017-02-10 13:30:00'),
  (31, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D', 'http://cockytest.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd', '2017-02-10 15:06:02'),
  (32, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D',
   'http://cockytest.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd',
   '2017-02-10 15:06:03'),
  (33, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D',
   'http://cockytest.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd',
   '2017-02-10 15:06:08'),
  (34, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D',
   'http://cockytest.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd',
   '2017-02-10 15:06:08'),
  (35, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D',
   'http://cockytest.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd',
   '2017-02-10 15:07:34'),
  (36, 1, 1, '/admin255znjcdp/%7Bshop_logo%7D',
   'http://cockytest.zcom.cz/admin255znjcdp/index.php?controller=AdminTranslations&lang=cs&type=mails&theme=theme1353&token=886852d938cb2fd8fb08ead4cd8233fd',
   '2017-02-10 15:07:34');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_page_type`
--

CREATE TABLE IF NOT EXISTS `ps_page_type` (
  `id_page_type` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`         VARCHAR(255)     NOT NULL,
  PRIMARY KEY (`id_page_type`),
  KEY `name` (`name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 3;

--
-- Vypisuji data pro tabulku `ps_page_type`
--

INSERT INTO `ps_page_type` (`id_page_type`, `name`) VALUES
  (1, 'index'),
  (2, 'pagenotfound');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_page_viewed`
--

CREATE TABLE IF NOT EXISTS `ps_page_viewed` (
  `id_page`       INT(10) UNSIGNED NOT NULL,
  `id_shop_group` INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop`       INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_date_range` INT(10) UNSIGNED NOT NULL,
  `counter`       INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_page`, `id_date_range`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_page_viewed`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product`
--

CREATE TABLE IF NOT EXISTS `ps_product` (
  `id_product`                INT(10) UNSIGNED                           NOT NULL AUTO_INCREMENT,
  `id_supplier`               INT(10) UNSIGNED                                    DEFAULT NULL,
  `id_manufacturer`           INT(10) UNSIGNED                                    DEFAULT NULL,
  `id_category_default`       INT(10) UNSIGNED                                    DEFAULT NULL,
  `id_shop_default`           INT(10) UNSIGNED                           NOT NULL DEFAULT '1',
  `id_tax_rules_group`        INT(11) UNSIGNED                           NOT NULL,
  `on_sale`                   TINYINT(1) UNSIGNED                        NOT NULL DEFAULT '0',
  `online_only`               TINYINT(1) UNSIGNED                        NOT NULL DEFAULT '0',
  `ean13`                     VARCHAR(13)                                         DEFAULT NULL,
  `upc`                       VARCHAR(12)                                         DEFAULT NULL,
  `ecotax`                    DECIMAL(17, 6)                             NOT NULL DEFAULT '0.000000',
  `quantity`                  INT(10)                                    NOT NULL DEFAULT '0',
  `minimal_quantity`          INT(10) UNSIGNED                           NOT NULL DEFAULT '1',
  `price`                     DECIMAL(20, 6)                             NOT NULL DEFAULT '0.000000',
  `wholesale_price`           DECIMAL(20, 6)                             NOT NULL DEFAULT '0.000000',
  `unity`                     VARCHAR(255)                                        DEFAULT NULL,
  `unit_price_ratio`          DECIMAL(20, 6)                             NOT NULL DEFAULT '0.000000',
  `additional_shipping_cost`  DECIMAL(20, 2)                             NOT NULL DEFAULT '0.00',
  `reference`                 VARCHAR(32)                                         DEFAULT NULL,
  `supplier_reference`        VARCHAR(32)                                         DEFAULT NULL,
  `location`                  VARCHAR(64)                                         DEFAULT NULL,
  `width`                     DECIMAL(20, 6)                             NOT NULL DEFAULT '0.000000',
  `height`                    DECIMAL(20, 6)                             NOT NULL DEFAULT '0.000000',
  `depth`                     DECIMAL(20, 6)                             NOT NULL DEFAULT '0.000000',
  `weight`                    DECIMAL(20, 6)                             NOT NULL DEFAULT '0.000000',
  `out_of_stock`              INT(10) UNSIGNED                           NOT NULL DEFAULT '2',
  `quantity_discount`         TINYINT(1)                                          DEFAULT '0',
  `customizable`              TINYINT(2)                                 NOT NULL DEFAULT '0',
  `uploadable_files`          TINYINT(4)                                 NOT NULL DEFAULT '0',
  `text_fields`               TINYINT(4)                                 NOT NULL DEFAULT '0',
  `active`                    TINYINT(1) UNSIGNED                        NOT NULL DEFAULT '0',
  `redirect_type`             ENUM ('', '404', '301', '302')             NOT NULL DEFAULT '',
  `id_product_redirected`     INT(10) UNSIGNED                           NOT NULL DEFAULT '0',
  `available_for_order`       TINYINT(1)                                 NOT NULL DEFAULT '1',
  `available_date`            DATE                                       NOT NULL DEFAULT '0000-00-00',
  `condition`                 ENUM ('new', 'used', 'refurbished')        NOT NULL DEFAULT 'new',
  `show_price`                TINYINT(1)                                 NOT NULL DEFAULT '1',
  `indexed`                   TINYINT(1)                                 NOT NULL DEFAULT '0',
  `visibility`                ENUM ('both', 'catalog', 'search', 'none') NOT NULL DEFAULT 'both',
  `cache_is_pack`             TINYINT(1)                                 NOT NULL DEFAULT '0',
  `cache_has_attachments`     TINYINT(1)                                 NOT NULL DEFAULT '0',
  `is_virtual`                TINYINT(1)                                 NOT NULL DEFAULT '0',
  `cache_default_attribute`   INT(10) UNSIGNED                                    DEFAULT NULL,
  `date_add`                  DATETIME                                   NOT NULL,
  `date_upd`                  DATETIME                                   NOT NULL,
  `advanced_stock_management` TINYINT(1)                                 NOT NULL DEFAULT '0',
  `pack_stock_type`           INT(11) UNSIGNED                           NOT NULL DEFAULT '3',
  PRIMARY KEY (`id_product`),
  KEY `product_supplier` (`id_supplier`),
  KEY `product_manufacturer` (`id_manufacturer`, `id_product`),
  KEY `id_category_default` (`id_category_default`),
  KEY `indexed` (`indexed`),
  KEY `date_add` (`date_add`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 8;

--
-- Vypisuji data pro tabulku `ps_product`
--

INSERT INTO `ps_product` (`id_product`, `id_supplier`, `id_manufacturer`, `id_category_default`, `id_shop_default`, `id_tax_rules_group`, `on_sale`, `online_only`, `ean13`, `upc`, `ecotax`, `quantity`, `minimal_quantity`, `price`, `wholesale_price`, `unity`, `unit_price_ratio`, `additional_shipping_cost`, `reference`, `supplier_reference`, `location`, `width`, `height`, `depth`, `weight`, `out_of_stock`, `quantity_discount`, `customizable`, `uploadable_files`, `text_fields`, `active`, `redirect_type`, `id_product_redirected`, `available_for_order`, `available_date`, `condition`, `show_price`, `indexed`, `visibility`, `cache_is_pack`, `cache_has_attachments`, `is_virtual`, `cache_default_attribute`, `date_add`, `date_upd`, `advanced_stock_management`, `pack_stock_type`)
VALUES
  (1, 1, 1, 2, 1, 1, 0, 0, '0', '', 0.000000, 0, 1, 16.510000, 4.950000, '', 0.000000, 0.00, 'demo_1', '', '', 0.000000,
                                                                                                               0.000000,
                                                                                                               0.000000,
                                                                                                               0.000000,
                                                                                                               2, 0, 0,
                                                                                                               0, 0, 1,
    '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 1, '2016-08-04 12:42:30', '2017-02-10 11:44:49', 0, 3),
  (3, 1, 1, 2, 1, 1, 0, 0, '0', '', 0.000000, 0, 1, 25.999852, 7.800000, '', 0.000000, 0.00, 'demo_3', '', '', 0.000000,
                                                                                                               0.000000,
                                                                                                               0.000000,
                                                                                                               0.000000,
                                                                                                               2, 0, 0,
                                                                                                               0, 0, 1,
    '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 13, '2016-08-04 12:42:30', '2017-02-10 10:36:38', 0, 3),
  (4, 1, 1, 10, 1, 1, 0, 0, '0', '', 0.000000, 0, 1, 50.994153, 15.300000, '', 0.000000, 0.00, 'demo_4', '', '',
    0.000000, 0.000000, 0.000000, 0.000000, 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0,
   16, '2016-08-04 12:42:30', '2017-01-25 15:22:42', 0, 3),
  (5, 1, 1, 11, 1, 1, 0, 0, '0', '', 0.000000, 0, 1, 30.506321, 9.150000, '', 0.000000, 0.00, 'demo_5', '', '',
    0.000000, 0.000000, 0.000000, 0.000000, 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0,
   19, '2016-08-04 12:42:30', '2017-01-25 15:22:42', 0, 3),
  (6, 1, 1, 11, 1, 1, 0, 0, '0', '', 0.000000, 0, 1, 30.502569, 9.150000, '', 0.000000, 0.00, 'demo_6', '', '',
    0.000000, 0.000000, 0.000000, 0.000000, 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0,
   31, '2016-08-04 12:42:30', '2017-01-25 15:22:42', 0, 3),
  (7, 1, 1, 2, 1, 1, 0, 0, '0', '', 0.000000, 0, 1, 20.501236, 6.150000, '', 0.000000, 0.00, 'demo_7', '', '', 0.000000,
                                                                                                               0.000000,
                                                                                                               0.000000,
                                                                                                               0.000000,
                                                                                                               2, 0, 0,
                                                                                                               0, 0, 1,
    '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 34, '2016-08-04 12:42:30', '2017-01-25 15:22:42', 0, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_attachment`
--

CREATE TABLE IF NOT EXISTS `ps_product_attachment` (
  `id_product`    INT(10) UNSIGNED NOT NULL,
  `id_attachment` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product`, `id_attachment`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_product_attachment`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_attribute`
--

CREATE TABLE IF NOT EXISTS `ps_product_attribute` (
  `id_product_attribute` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product`           INT(10) UNSIGNED NOT NULL,
  `reference`            VARCHAR(32)               DEFAULT NULL,
  `supplier_reference`   VARCHAR(32)               DEFAULT NULL,
  `location`             VARCHAR(64)               DEFAULT NULL,
  `ean13`                VARCHAR(13)               DEFAULT NULL,
  `upc`                  VARCHAR(12)               DEFAULT NULL,
  `wholesale_price`      DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `price`                DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `ecotax`               DECIMAL(17, 6)   NOT NULL DEFAULT '0.000000',
  `quantity`             INT(10)          NOT NULL DEFAULT '0',
  `weight`               DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `unit_price_impact`    DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `default_on`           TINYINT(1) UNSIGNED       DEFAULT NULL,
  `minimal_quantity`     INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `available_date`       DATE             NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id_product_attribute`),
  UNIQUE KEY `product_default` (`id_product`, `default_on`),
  KEY `product_attribute_product` (`id_product`),
  KEY `reference` (`reference`),
  KEY `supplier_reference` (`supplier_reference`),
  KEY `id_product_id_product_attribute` (`id_product_attribute`, `id_product`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 46;

--
-- Vypisuji data pro tabulku `ps_product_attribute`
--

INSERT INTO `ps_product_attribute` (`id_product_attribute`, `id_product`, `reference`, `supplier_reference`, `location`, `ean13`, `upc`, `wholesale_price`, `price`, `ecotax`, `quantity`, `weight`, `unit_price_impact`, `default_on`, `minimal_quantity`, `available_date`)
VALUES
  (1, 1, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 1, 1, '0000-00-00'),
  (2, 1, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (3, 1, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (4, 1, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (5, 1, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (6, 1, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (13, 3, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 1, 1, '0000-00-00'),
  (14, 3, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (15, 3, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (16, 4, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 1, 1, '0000-00-00'),
  (17, 4, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (18, 4, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (19, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 1, 1, '0000-00-00'),
  (20, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (21, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (22, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (23, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (24, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (25, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (26, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (27, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (28, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (29, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (30, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (31, 6, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 1, 1, '0000-00-00'),
  (32, 6, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (33, 6, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (34, 7, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 1, 1, '0000-00-00'),
  (35, 7, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (36, 7, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (37, 7, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (38, 7, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (39, 7, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (40, 6, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (41, 6, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (42, 6, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (43, 4, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (44, 4, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (45, 4, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, NULL, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_attribute_combination`
--

CREATE TABLE IF NOT EXISTS `ps_product_attribute_combination` (
  `id_attribute`         INT(10) UNSIGNED NOT NULL,
  `id_product_attribute` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_attribute`, `id_product_attribute`),
  KEY `id_product_attribute` (`id_product_attribute`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_product_attribute_combination`
--

INSERT INTO `ps_product_attribute_combination` (`id_attribute`, `id_product_attribute`) VALUES
  (1, 1),
  (13, 1),
  (1, 2),
  (14, 2),
  (2, 3),
  (13, 3),
  (2, 4),
  (14, 4),
  (3, 5),
  (13, 5),
  (3, 6),
  (14, 6),
  (1, 13),
  (13, 13),
  (2, 14),
  (13, 14),
  (3, 15),
  (13, 15),
  (1, 16),
  (7, 16),
  (2, 17),
  (7, 17),
  (3, 18),
  (7, 18),
  (1, 19),
  (16, 19),
  (1, 20),
  (14, 20),
  (1, 21),
  (13, 21),
  (1, 22),
  (11, 22),
  (2, 23),
  (16, 23),
  (2, 24),
  (14, 24),
  (2, 25),
  (13, 25),
  (2, 26),
  (11, 26),
  (3, 27),
  (16, 27),
  (3, 28),
  (14, 28),
  (3, 29),
  (13, 29),
  (3, 30),
  (11, 30),
  (1, 31),
  (16, 31),
  (2, 32),
  (16, 32),
  (3, 33),
  (16, 33),
  (1, 34),
  (16, 34),
  (2, 35),
  (16, 35),
  (3, 36),
  (16, 36),
  (1, 37),
  (15, 37),
  (2, 38),
  (15, 38),
  (3, 39),
  (15, 39),
  (1, 40),
  (8, 40),
  (2, 41),
  (8, 41),
  (3, 42),
  (8, 42),
  (1, 43),
  (24, 43),
  (2, 44),
  (24, 44),
  (3, 45),
  (24, 45);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_attribute_image`
--

CREATE TABLE IF NOT EXISTS `ps_product_attribute_image` (
  `id_product_attribute` INT(10) UNSIGNED NOT NULL,
  `id_image`             INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product_attribute`, `id_image`),
  KEY `id_image` (`id_image`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_product_attribute_image`
--

INSERT INTO `ps_product_attribute_image` (`id_product_attribute`, `id_image`) VALUES
  (1, 1),
  (3, 1),
  (5, 1),
  (1, 2),
  (3, 2),
  (5, 2),
  (2, 3),
  (4, 3),
  (6, 3),
  (2, 4),
  (4, 4),
  (6, 4),
  (16, 10),
  (17, 10),
  (18, 10),
  (43, 11),
  (44, 11),
  (45, 11),
  (19, 12),
  (23, 12),
  (27, 12),
  (20, 13),
  (24, 13),
  (28, 13),
  (21, 14),
  (25, 14),
  (29, 14),
  (22, 15),
  (26, 15),
  (30, 15),
  (31, 16),
  (32, 16),
  (33, 16),
  (31, 17),
  (32, 17),
  (33, 17),
  (40, 18),
  (41, 18),
  (42, 18),
  (40, 19),
  (41, 19),
  (42, 19),
  (34, 20),
  (35, 20),
  (36, 20),
  (34, 21),
  (35, 21),
  (36, 21),
  (37, 22),
  (38, 22),
  (39, 22),
  (37, 23),
  (38, 23),
  (39, 23);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_attribute_shop`
--

CREATE TABLE IF NOT EXISTS `ps_product_attribute_shop` (
  `id_product`           INT(10) UNSIGNED NOT NULL,
  `id_product_attribute` INT(10) UNSIGNED NOT NULL,
  `id_shop`              INT(10) UNSIGNED NOT NULL,
  `wholesale_price`      DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `price`                DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `ecotax`               DECIMAL(17, 6)   NOT NULL DEFAULT '0.000000',
  `weight`               DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `unit_price_impact`    DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `default_on`           TINYINT(1) UNSIGNED       DEFAULT NULL,
  `minimal_quantity`     INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `available_date`       DATE             NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id_product_attribute`, `id_shop`),
  UNIQUE KEY `id_product` (`id_product`, `id_shop`, `default_on`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_product_attribute_shop`
--

INSERT INTO `ps_product_attribute_shop` (`id_product`, `id_product_attribute`, `id_shop`, `wholesale_price`, `price`, `ecotax`, `weight`, `unit_price_impact`, `default_on`, `minimal_quantity`, `available_date`)
VALUES
  (1, 1, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1, 1, '0000-00-00'),
  (1, 2, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (1, 3, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (1, 4, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (1, 5, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (1, 6, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (3, 13, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1, 1, '0000-00-00'),
  (3, 14, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (3, 15, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (4, 16, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1, 1, '0000-00-00'),
  (4, 17, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (4, 18, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (5, 19, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1, 1, '0000-00-00'),
  (5, 20, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (5, 21, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (5, 22, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (5, 23, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (5, 24, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (5, 25, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (5, 26, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (5, 27, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (5, 28, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (5, 29, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (5, 30, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (6, 31, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1, 1, '0000-00-00'),
  (6, 32, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (6, 33, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (7, 34, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1, 1, '0000-00-00'),
  (7, 35, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (7, 36, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (7, 37, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (7, 38, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (7, 39, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (6, 40, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (6, 41, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (6, 42, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (4, 43, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (4, 44, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00'),
  (4, 45, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, NULL, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_carrier`
--

CREATE TABLE IF NOT EXISTS `ps_product_carrier` (
  `id_product`           INT(10) UNSIGNED NOT NULL,
  `id_carrier_reference` INT(10) UNSIGNED NOT NULL,
  `id_shop`              INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product`, `id_carrier_reference`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_product_carrier`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_country_tax`
--

CREATE TABLE IF NOT EXISTS `ps_product_country_tax` (
  `id_product` INT(11) NOT NULL,
  `id_country` INT(11) NOT NULL,
  `id_tax`     INT(11) NOT NULL,
  PRIMARY KEY (`id_product`, `id_country`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_product_country_tax`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_download`
--

CREATE TABLE IF NOT EXISTS `ps_product_download` (
  `id_product_download` INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_product`          INT(10) UNSIGNED    NOT NULL,
  `display_filename`    VARCHAR(255)                 DEFAULT NULL,
  `filename`            VARCHAR(255)                 DEFAULT NULL,
  `date_add`            DATETIME            NOT NULL,
  `date_expiration`     DATETIME                     DEFAULT NULL,
  `nb_days_accessible`  INT(10) UNSIGNED             DEFAULT NULL,
  `nb_downloadable`     INT(10) UNSIGNED             DEFAULT '1',
  `active`              TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `is_shareable`        TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_product_download`),
  UNIQUE KEY `id_product` (`id_product`),
  KEY `product_active` (`id_product`, `active`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_product_download`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_group_reduction_cache`
--

CREATE TABLE IF NOT EXISTS `ps_product_group_reduction_cache` (
  `id_product` INT(10) UNSIGNED NOT NULL,
  `id_group`   INT(10) UNSIGNED NOT NULL,
  `reduction`  DECIMAL(4, 3)    NOT NULL,
  PRIMARY KEY (`id_product`, `id_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_product_group_reduction_cache`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_lang`
--

CREATE TABLE IF NOT EXISTS `ps_product_lang` (
  `id_product`        INT(10) UNSIGNED NOT NULL,
  `id_shop`           INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang`           INT(10) UNSIGNED NOT NULL,
  `description`       TEXT,
  `description_short` TEXT,
  `link_rewrite`      VARCHAR(128)     NOT NULL,
  `meta_description`  VARCHAR(255)              DEFAULT NULL,
  `meta_keywords`     VARCHAR(255)              DEFAULT NULL,
  `meta_title`        VARCHAR(128)              DEFAULT NULL,
  `name`              VARCHAR(128)     NOT NULL,
  `available_now`     VARCHAR(255)              DEFAULT NULL,
  `available_later`   VARCHAR(255)              DEFAULT NULL,
  PRIMARY KEY (`id_product`, `id_shop`, `id_lang`),
  KEY `id_lang` (`id_lang`),
  KEY `name` (`name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_product_lang`
--

INSERT INTO `ps_product_lang` (`id_product`, `id_shop`, `id_lang`, `description`, `description_short`, `link_rewrite`, `meta_description`, `meta_keywords`, `meta_title`, `name`, `available_now`, `available_later`)
VALUES
  (1, 1, 1,
      '<p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman''s wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!</p>',
      '<p>Faded short sleeves t-shirt with high neckline. Soft and stretchy material for a comfortable fit. Accessorize with a straw hat and you''re ready for summer!</p>',
      'faded-short-sleeves-tshirt', '', '', '', 'Faded Short Sleeves T-shirt', 'In stock', ''),
  (3, 1, 1,
      '<p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman''s wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!</p>',
      '<p>100% cotton double printed dress. Black and white striped top and orange high waisted skater skirt bottom.</p>',
      'printed-dress', '', '', '', 'Printed Dress', 'In stock', ''),
  (4, 1, 1,
      '<p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman''s wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!</p>',
      '<p>Printed evening dress with straight sleeves with black thin waist belt and ruffled linings.</p>',
      'printed-dress', '', '', '', 'Printed Dress', 'In stock', ''),
  (5, 1, 1,
      '<p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman''s wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!</p>',
      '<p>Long printed dress with thin adjustable straps. V-neckline and wiring under the bust with ruffles at the bottom of the dress.</p>',
      'printed-summer-dress', '', '', '', 'Printed Summer Dress', 'In stock', ''),
  (6, 1, 1,
      '<p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman''s wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!</p>',
      '<p>Sleeveless knee-length chiffon dress. V-neckline with elastic under the bust lining.</p>',
      'printed-summer-dress', '', '', '', 'Printed Summer Dress', 'In stock', ''),
  (7, 1, 1,
      '<p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman''s wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!</p>',
      '<p>Printed chiffon knee length dress with tank straps. Deep v-neckline.</p>', 'printed-chiffon-dress', '', '',
      '', 'Printed Chiffon Dress', 'In stock', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_sale`
--

CREATE TABLE IF NOT EXISTS `ps_product_sale` (
  `id_product` INT(10) UNSIGNED NOT NULL,
  `quantity`   INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `sale_nbr`   INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `date_upd`   DATE             NOT NULL,
  PRIMARY KEY (`id_product`),
  KEY `quantity` (`quantity`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_product_sale`
--

INSERT INTO `ps_product_sale` (`id_product`, `quantity`, `sale_nbr`, `date_upd`) VALUES
  (1, 3, 3, '2016-08-04'),
  (3, 3, 3, '2016-08-04'),
  (5, 1, 1, '2016-08-04'),
  (6, 2, 2, '2016-08-04'),
  (7, 2, 2, '2016-08-04');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_shop`
--

CREATE TABLE IF NOT EXISTS `ps_product_shop` (
  `id_product`                INT(10) UNSIGNED                           NOT NULL,
  `id_shop`                   INT(10) UNSIGNED                           NOT NULL,
  `id_category_default`       INT(10) UNSIGNED                                    DEFAULT NULL,
  `id_tax_rules_group`        INT(11) UNSIGNED                           NOT NULL,
  `on_sale`                   TINYINT(1) UNSIGNED                        NOT NULL DEFAULT '0',
  `online_only`               TINYINT(1) UNSIGNED                        NOT NULL DEFAULT '0',
  `ecotax`                    DECIMAL(17, 6)                             NOT NULL DEFAULT '0.000000',
  `minimal_quantity`          INT(10) UNSIGNED                           NOT NULL DEFAULT '1',
  `price`                     DECIMAL(20, 6)                             NOT NULL DEFAULT '0.000000',
  `wholesale_price`           DECIMAL(20, 6)                             NOT NULL DEFAULT '0.000000',
  `unity`                     VARCHAR(255)                                        DEFAULT NULL,
  `unit_price_ratio`          DECIMAL(20, 6)                             NOT NULL DEFAULT '0.000000',
  `additional_shipping_cost`  DECIMAL(20, 2)                             NOT NULL DEFAULT '0.00',
  `customizable`              TINYINT(2)                                 NOT NULL DEFAULT '0',
  `uploadable_files`          TINYINT(4)                                 NOT NULL DEFAULT '0',
  `text_fields`               TINYINT(4)                                 NOT NULL DEFAULT '0',
  `active`                    TINYINT(1) UNSIGNED                        NOT NULL DEFAULT '0',
  `redirect_type`             ENUM ('', '404', '301', '302')             NOT NULL DEFAULT '',
  `id_product_redirected`     INT(10) UNSIGNED                           NOT NULL DEFAULT '0',
  `available_for_order`       TINYINT(1)                                 NOT NULL DEFAULT '1',
  `available_date`            DATE                                       NOT NULL DEFAULT '0000-00-00',
  `condition`                 ENUM ('new', 'used', 'refurbished')        NOT NULL DEFAULT 'new',
  `show_price`                TINYINT(1)                                 NOT NULL DEFAULT '1',
  `indexed`                   TINYINT(1)                                 NOT NULL DEFAULT '0',
  `visibility`                ENUM ('both', 'catalog', 'search', 'none') NOT NULL DEFAULT 'both',
  `cache_default_attribute`   INT(10) UNSIGNED                                    DEFAULT NULL,
  `advanced_stock_management` TINYINT(1)                                 NOT NULL DEFAULT '0',
  `date_add`                  DATETIME                                   NOT NULL,
  `date_upd`                  DATETIME                                   NOT NULL,
  `pack_stock_type`           INT(11) UNSIGNED                           NOT NULL DEFAULT '3',
  PRIMARY KEY (`id_product`, `id_shop`),
  KEY `id_category_default` (`id_category_default`),
  KEY `date_add` (`date_add`, `active`, `visibility`),
  KEY `indexed` (`indexed`, `active`, `id_product`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_product_shop`
--

INSERT INTO `ps_product_shop` (`id_product`, `id_shop`, `id_category_default`, `id_tax_rules_group`, `on_sale`, `online_only`, `ecotax`, `minimal_quantity`, `price`, `wholesale_price`, `unity`, `unit_price_ratio`, `additional_shipping_cost`, `customizable`, `uploadable_files`, `text_fields`, `active`, `redirect_type`, `id_product_redirected`, `available_for_order`, `available_date`, `condition`, `show_price`, `indexed`, `visibility`, `cache_default_attribute`, `advanced_stock_management`, `date_add`, `date_upd`, `pack_stock_type`)
VALUES
  (1, 1, 2, 1, 0, 0, 0.000000, 1, 16.510000, 4.950000, '', 0.000000, 0.00, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new',
   1, 1, 'both', 1, 0, '2016-08-04 12:42:30', '2017-02-10 11:44:49', 3),
  (3, 1, 2, 1, 0, 0, 0.000000, 1, 25.999852, 7.800000, '', 0.000000, 0.00, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new',
   1, 1, 'both', 13, 0, '2016-08-04 12:42:30', '2017-02-10 10:36:38', 3),
  (4, 1, 10, 1, 0, 0, 0.000000, 1, 50.994153, 15.300000, '', 0.000000, 0.00, 0, 0, 0, 1, '404', 0, 1, '0000-00-00',
   'new', 1, 1, 'both', 16, 0, '2016-08-04 12:42:30', '2017-01-25 15:22:42', 3),
  (5, 1, 11, 1, 0, 0, 0.000000, 1, 30.506321, 9.150000, '', 0.000000, 0.00, 0, 0, 0, 1, '404', 0, 1, '0000-00-00',
   'new', 1, 1, 'both', 19, 0, '2016-08-04 12:42:30', '2017-01-25 15:22:42', 3),
  (6, 1, 11, 1, 0, 0, 0.000000, 1, 30.502569, 9.150000, '', 0.000000, 0.00, 0, 0, 0, 1, '404', 0, 1, '0000-00-00',
   'new', 1, 1, 'both', 31, 0, '2016-08-04 12:42:30', '2017-01-25 15:22:42', 3),
  (7, 1, 2, 1, 0, 0, 0.000000, 1, 20.501236, 6.150000, '', 0.000000, 0.00, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new',
   1, 1, 'both', 34, 0, '2016-08-04 12:42:30', '2017-01-25 15:22:42', 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_supplier`
--

CREATE TABLE IF NOT EXISTS `ps_product_supplier` (
  `id_product_supplier`        INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product`                 INT(11) UNSIGNED NOT NULL,
  `id_product_attribute`       INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `id_supplier`                INT(11) UNSIGNED NOT NULL,
  `product_supplier_reference` VARCHAR(32)               DEFAULT NULL,
  `product_supplier_price_te`  DECIMAL(20, 6)   NOT NULL DEFAULT '0.000000',
  `id_currency`                INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product_supplier`),
  UNIQUE KEY `id_product` (`id_product`, `id_product_attribute`, `id_supplier`),
  KEY `id_supplier` (`id_supplier`, `id_product`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 29;

--
-- Vypisuji data pro tabulku `ps_product_supplier`
--

INSERT INTO `ps_product_supplier` (`id_product_supplier`, `id_product`, `id_product_attribute`, `id_supplier`, `product_supplier_reference`, `product_supplier_price_te`, `id_currency`)
VALUES
  (1, 1, 0, 1, '', 0.000000, 0),
  (3, 3, 0, 1, '', 0.000000, 0),
  (4, 4, 0, 1, '', 0.000000, 0),
  (5, 5, 0, 1, '', 0.000000, 0),
  (6, 6, 0, 1, '', 0.000000, 0),
  (7, 7, 0, 1, '', 0.000000, 0),
  (14, 1, 1, 1, '', 0.000000, 1),
  (15, 1, 2, 1, '', 0.000000, 1),
  (16, 1, 3, 1, '', 0.000000, 1),
  (17, 1, 4, 1, '', 0.000000, 1),
  (18, 1, 5, 1, '', 0.000000, 1),
  (19, 1, 6, 1, '', 0.000000, 1),
  (20, 7, 34, 1, '', 0.000000, 1),
  (21, 7, 35, 1, '', 0.000000, 1),
  (22, 7, 36, 1, '', 0.000000, 1),
  (23, 7, 37, 1, '', 0.000000, 1),
  (24, 7, 38, 1, '', 0.000000, 1),
  (25, 7, 39, 1, '', 0.000000, 1),
  (26, 3, 13, 1, '', 0.000000, 1),
  (27, 3, 14, 1, '', 0.000000, 1),
  (28, 3, 15, 1, '', 0.000000, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_tag`
--

CREATE TABLE IF NOT EXISTS `ps_product_tag` (
  `id_product` INT(10) UNSIGNED NOT NULL,
  `id_tag`     INT(10) UNSIGNED NOT NULL,
  `id_lang`    INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_product`, `id_tag`),
  KEY `id_tag` (`id_tag`),
  KEY `id_lang` (`id_lang`, `id_tag`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_product_tag`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_video`
--

CREATE TABLE IF NOT EXISTS `ps_product_video` (
  `id_video`   INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop`    INT(10) UNSIGNED NOT NULL,
  `id_product` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_video`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_product_video`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_product_video_lang`
--

CREATE TABLE IF NOT EXISTS `ps_product_video_lang` (
  `id_video`    INT(10) UNSIGNED    NOT NULL,
  `id_shop`     INT(10) UNSIGNED    NOT NULL,
  `id_product`  INT(10) UNSIGNED    NOT NULL,
  `id_lang`     INT(10) UNSIGNED    NOT NULL,
  `link`        VARCHAR(255)        NOT NULL,
  `cover_image` VARCHAR(255)        NOT NULL,
  `name`        VARCHAR(255)        NOT NULL,
  `description` TEXT,
  `sort_order`  INT(10) UNSIGNED    NOT NULL,
  `status`      TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_video`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_product_video_lang`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_profile`
--

CREATE TABLE IF NOT EXISTS `ps_profile` (
  `id_profile` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_profile`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 5;

--
-- Vypisuji data pro tabulku `ps_profile`
--

INSERT INTO `ps_profile` (`id_profile`) VALUES
  (1),
  (2),
  (3),
  (4);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_profile_lang`
--

CREATE TABLE IF NOT EXISTS `ps_profile_lang` (
  `id_lang`    INT(10) UNSIGNED NOT NULL,
  `id_profile` INT(10) UNSIGNED NOT NULL,
  `name`       VARCHAR(128)     NOT NULL,
  PRIMARY KEY (`id_profile`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_profile_lang`
--

INSERT INTO `ps_profile_lang` (`id_lang`, `id_profile`, `name`) VALUES
  (1, 1, 'SuperAdmin'),
  (1, 2, 'Logistician'),
  (1, 3, 'Translator'),
  (1, 4, 'Salesman');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_quick_access`
--

CREATE TABLE IF NOT EXISTS `ps_quick_access` (
  `id_quick_access` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `new_window`      TINYINT(1)       NOT NULL DEFAULT '0',
  `link`            VARCHAR(255)     NOT NULL,
  PRIMARY KEY (`id_quick_access`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 5;

--
-- Vypisuji data pro tabulku `ps_quick_access`
--

INSERT INTO `ps_quick_access` (`id_quick_access`, `new_window`, `link`) VALUES
  (1, 0, 'index.php?controller=AdminCategories&addcategory'),
  (2, 0, 'index.php?controller=AdminProducts&addproduct'),
  (3, 0, 'index.php?controller=AdminCartRules&addcart_rule'),
  (4, 0, 'index.php?controller=AdminModules&token=1540fe797f5f3d4dce3df1d4dfe10d0d&configure=smartblog');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_quick_access_lang`
--

CREATE TABLE IF NOT EXISTS `ps_quick_access_lang` (
  `id_quick_access` INT(10) UNSIGNED NOT NULL,
  `id_lang`         INT(10) UNSIGNED NOT NULL,
  `name`            VARCHAR(32)      NOT NULL,
  PRIMARY KEY (`id_quick_access`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_quick_access_lang`
--

INSERT INTO `ps_quick_access_lang` (`id_quick_access`, `id_lang`, `name`) VALUES
  (1, 1, 'Novou kategorii'),
  (2, 1, 'Nový produkt'),
  (3, 1, 'Nový slevový kupón'),
  (4, 1, 'Smart Blog Setting');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_range_price`
--

CREATE TABLE IF NOT EXISTS `ps_range_price` (
  `id_range_price` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_carrier`     INT(10) UNSIGNED NOT NULL,
  `delimiter1`     DECIMAL(20, 6)   NOT NULL,
  `delimiter2`     DECIMAL(20, 6)   NOT NULL,
  PRIMARY KEY (`id_range_price`),
  UNIQUE KEY `id_carrier` (`id_carrier`, `delimiter1`, `delimiter2`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_range_price`
--

INSERT INTO `ps_range_price` (`id_range_price`, `id_carrier`, `delimiter1`, `delimiter2`) VALUES
  (1, 2, 0.000000, 10000.000000);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_range_weight`
--

CREATE TABLE IF NOT EXISTS `ps_range_weight` (
  `id_range_weight` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_carrier`      INT(10) UNSIGNED NOT NULL,
  `delimiter1`      DECIMAL(20, 6)   NOT NULL,
  `delimiter2`      DECIMAL(20, 6)   NOT NULL,
  PRIMARY KEY (`id_range_weight`),
  UNIQUE KEY `id_carrier` (`id_carrier`, `delimiter1`, `delimiter2`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_range_weight`
--

INSERT INTO `ps_range_weight` (`id_range_weight`, `id_carrier`, `delimiter1`, `delimiter2`) VALUES
  (1, 2, 0.000000, 10000.000000);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_referrer`
--

CREATE TABLE IF NOT EXISTS `ps_referrer` (
  `id_referrer`             INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`                    VARCHAR(64)      NOT NULL,
  `passwd`                  VARCHAR(32)               DEFAULT NULL,
  `http_referer_regexp`     VARCHAR(64)               DEFAULT NULL,
  `http_referer_like`       VARCHAR(64)               DEFAULT NULL,
  `request_uri_regexp`      VARCHAR(64)               DEFAULT NULL,
  `request_uri_like`        VARCHAR(64)               DEFAULT NULL,
  `http_referer_regexp_not` VARCHAR(64)               DEFAULT NULL,
  `http_referer_like_not`   VARCHAR(64)               DEFAULT NULL,
  `request_uri_regexp_not`  VARCHAR(64)               DEFAULT NULL,
  `request_uri_like_not`    VARCHAR(64)               DEFAULT NULL,
  `base_fee`                DECIMAL(5, 2)    NOT NULL DEFAULT '0.00',
  `percent_fee`             DECIMAL(5, 2)    NOT NULL DEFAULT '0.00',
  `click_fee`               DECIMAL(5, 2)    NOT NULL DEFAULT '0.00',
  `date_add`                DATETIME         NOT NULL,
  PRIMARY KEY (`id_referrer`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_referrer`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_referrer_cache`
--

CREATE TABLE IF NOT EXISTS `ps_referrer_cache` (
  `id_connections_source` INT(11) UNSIGNED NOT NULL,
  `id_referrer`           INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_connections_source`, `id_referrer`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_referrer_cache`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_referrer_shop`
--

CREATE TABLE IF NOT EXISTS `ps_referrer_shop` (
  `id_referrer`         INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop`             INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `cache_visitors`      INT(11)                   DEFAULT NULL,
  `cache_visits`        INT(11)                   DEFAULT NULL,
  `cache_pages`         INT(11)                   DEFAULT NULL,
  `cache_registrations` INT(11)                   DEFAULT NULL,
  `cache_orders`        INT(11)                   DEFAULT NULL,
  `cache_sales`         DECIMAL(17, 2)            DEFAULT NULL,
  `cache_reg_rate`      DECIMAL(5, 4)             DEFAULT NULL,
  `cache_order_rate`    DECIMAL(5, 4)             DEFAULT NULL,
  PRIMARY KEY (`id_referrer`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_referrer_shop`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_request_sql`
--

CREATE TABLE IF NOT EXISTS `ps_request_sql` (
  `id_request_sql` INT(11)      NOT NULL AUTO_INCREMENT,
  `name`           VARCHAR(200) NOT NULL,
  `sql`            TEXT         NOT NULL,
  PRIMARY KEY (`id_request_sql`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_request_sql`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_required_field`
--

CREATE TABLE IF NOT EXISTS `ps_required_field` (
  `id_required_field` INT(11)     NOT NULL AUTO_INCREMENT,
  `object_name`       VARCHAR(32) NOT NULL,
  `field_name`        VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id_required_field`),
  KEY `object_name` (`object_name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_required_field`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_risk`
--

CREATE TABLE IF NOT EXISTS `ps_risk` (
  `id_risk` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `percent` TINYINT(3)       NOT NULL,
  `color`   VARCHAR(32)               DEFAULT NULL,
  PRIMARY KEY (`id_risk`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 5;

--
-- Vypisuji data pro tabulku `ps_risk`
--

INSERT INTO `ps_risk` (`id_risk`, `percent`, `color`) VALUES
  (1, 0, '#32CD32'),
  (2, 35, '#FF8C00'),
  (3, 75, '#DC143C'),
  (4, 100, '#ec2e15');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_risk_lang`
--

CREATE TABLE IF NOT EXISTS `ps_risk_lang` (
  `id_risk` INT(10) UNSIGNED NOT NULL,
  `id_lang` INT(10) UNSIGNED NOT NULL,
  `name`    VARCHAR(20)      NOT NULL,
  PRIMARY KEY (`id_risk`, `id_lang`),
  KEY `id_risk` (`id_risk`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_risk_lang`
--

INSERT INTO `ps_risk_lang` (`id_risk`, `id_lang`, `name`) VALUES
  (1, 1, 'Žádné'),
  (2, 1, 'Nízká'),
  (3, 1, 'Střední'),
  (4, 1, 'Vysoká');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_scene`
--

CREATE TABLE IF NOT EXISTS `ps_scene` (
  `id_scene` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `active`   TINYINT(1)       NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_scene`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_scene`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_scene_category`
--

CREATE TABLE IF NOT EXISTS `ps_scene_category` (
  `id_scene`    INT(10) UNSIGNED NOT NULL,
  `id_category` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_scene`, `id_category`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_scene_category`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_scene_lang`
--

CREATE TABLE IF NOT EXISTS `ps_scene_lang` (
  `id_scene` INT(10) UNSIGNED NOT NULL,
  `id_lang`  INT(10) UNSIGNED NOT NULL,
  `name`     VARCHAR(100)     NOT NULL,
  PRIMARY KEY (`id_scene`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_scene_lang`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_scene_products`
--

CREATE TABLE IF NOT EXISTS `ps_scene_products` (
  `id_scene`    INT(10) UNSIGNED NOT NULL,
  `id_product`  INT(10) UNSIGNED NOT NULL,
  `x_axis`      INT(4)           NOT NULL,
  `y_axis`      INT(4)           NOT NULL,
  `zone_width`  INT(3)           NOT NULL,
  `zone_height` INT(3)           NOT NULL,
  PRIMARY KEY (`id_scene`, `id_product`, `x_axis`, `y_axis`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_scene_products`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_scene_shop`
--

CREATE TABLE IF NOT EXISTS `ps_scene_shop` (
  `id_scene` INT(11) UNSIGNED NOT NULL,
  `id_shop`  INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_scene`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_scene_shop`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_search_engine`
--

CREATE TABLE IF NOT EXISTS `ps_search_engine` (
  `id_search_engine` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `server`           VARCHAR(64)      NOT NULL,
  `getvar`           VARCHAR(16)      NOT NULL,
  PRIMARY KEY (`id_search_engine`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 39;

--
-- Vypisuji data pro tabulku `ps_search_engine`
--

INSERT INTO `ps_search_engine` (`id_search_engine`, `server`, `getvar`) VALUES
  (1, 'google', 'q'),
  (2, 'aol', 'q'),
  (3, 'yandex', 'text'),
  (4, 'ask.com', 'q'),
  (5, 'nhl.com', 'q'),
  (6, 'yahoo', 'p'),
  (7, 'baidu', 'wd'),
  (8, 'lycos', 'query'),
  (9, 'exalead', 'q'),
  (10, 'search.live', 'q'),
  (11, 'voila', 'rdata'),
  (12, 'altavista', 'q'),
  (13, 'bing', 'q'),
  (14, 'daum', 'q'),
  (15, 'eniro', 'search_word'),
  (16, 'naver', 'query'),
  (17, 'msn', 'q'),
  (18, 'netscape', 'query'),
  (19, 'cnn', 'query'),
  (20, 'about', 'terms'),
  (21, 'mamma', 'query'),
  (22, 'alltheweb', 'q'),
  (23, 'virgilio', 'qs'),
  (24, 'alice', 'qs'),
  (25, 'najdi', 'q'),
  (26, 'mama', 'query'),
  (27, 'seznam', 'q'),
  (28, 'onet', 'qt'),
  (29, 'szukacz', 'q'),
  (30, 'yam', 'k'),
  (31, 'pchome', 'q'),
  (32, 'kvasir', 'q'),
  (33, 'sesam', 'q'),
  (34, 'ozu', 'q'),
  (35, 'terra', 'query'),
  (36, 'mynet', 'q'),
  (37, 'ekolay', 'q'),
  (38, 'rambler', 'words');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_search_index`
--

CREATE TABLE IF NOT EXISTS `ps_search_index` (
  `id_product` INT(11) UNSIGNED     NOT NULL,
  `id_word`    INT(11) UNSIGNED     NOT NULL,
  `weight`     SMALLINT(4) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_word`, `id_product`),
  KEY `id_product` (`id_product`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_search_index`
--

INSERT INTO `ps_search_index` (`id_product`, `id_word`, `weight`) VALUES
  (1, 1, 7),
  (4, 2, 2),
  (6, 2, 2),
  (1, 3, 7),
  (4, 3, 1),
  (1, 4, 7),
  (1, 5, 10),
  (3, 5, 10),
  (4, 5, 10),
  (5, 5, 10),
  (6, 5, 10),
  (7, 5, 10),
  (1, 6, 1),
  (3, 6, 1),
  (1, 7, 1),
  (5, 7, 1),
  (6, 7, 1),
  (7, 7, 1),
  (1, 8, 1),
  (1, 9, 1),
  (1, 10, 1),
  (1, 11, 1),
  (1, 12, 1),
  (1, 13, 1),
  (1, 14, 1),
  (1, 15, 1),
  (1, 16, 2),
  (3, 16, 1),
  (4, 16, 1),
  (5, 16, 1),
  (6, 16, 1),
  (7, 16, 1),
  (1, 17, 1),
  (5, 17, 9),
  (6, 17, 9),
  (1, 18, 2),
  (3, 18, 2),
  (4, 18, 5),
  (5, 18, 5),
  (6, 18, 5),
  (7, 18, 2),
  (1, 19, 1),
  (3, 19, 1),
  (4, 19, 1),
  (5, 19, 1),
  (6, 19, 1),
  (7, 19, 1),
  (1, 20, 1),
  (3, 20, 1),
  (4, 20, 1),
  (5, 20, 1),
  (6, 20, 1),
  (7, 20, 1),
  (1, 21, 1),
  (3, 21, 1),
  (4, 21, 1),
  (5, 21, 1),
  (6, 21, 1),
  (7, 21, 1),
  (1, 22, 1),
  (3, 22, 1),
  (4, 22, 1),
  (5, 22, 1),
  (6, 22, 1),
  (7, 22, 1),
  (1, 23, 2),
  (3, 23, 2),
  (4, 23, 2),
  (5, 23, 2),
  (6, 23, 2),
  (7, 23, 2),
  (1, 24, 1),
  (3, 24, 1),
  (4, 24, 1),
  (5, 24, 1),
  (6, 24, 1),
  (7, 24, 1),
  (1, 25, 1),
  (3, 25, 1),
  (4, 25, 1),
  (5, 25, 1),
  (6, 25, 1),
  (7, 25, 1),
  (1, 26, 1),
  (3, 26, 1),
  (4, 26, 1),
  (5, 26, 1),
  (6, 26, 1),
  (7, 26, 1),
  (1, 27, 1),
  (3, 27, 1),
  (4, 27, 1),
  (5, 27, 1),
  (6, 27, 1),
  (7, 27, 1),
  (1, 28, 1),
  (3, 28, 1),
  (4, 28, 1),
  (5, 28, 1),
  (6, 28, 1),
  (7, 28, 1),
  (1, 29, 1),
  (3, 29, 1),
  (4, 29, 1),
  (5, 29, 1),
  (6, 29, 1),
  (7, 29, 1),
  (1, 30, 1),
  (3, 30, 1),
  (4, 30, 1),
  (5, 30, 1),
  (6, 30, 1),
  (7, 30, 1),
  (1, 31, 1),
  (3, 31, 1),
  (4, 31, 1),
  (5, 31, 1),
  (6, 31, 1),
  (7, 31, 1),
  (1, 32, 1),
  (3, 32, 1),
  (4, 32, 1),
  (5, 32, 1),
  (6, 32, 1),
  (7, 32, 1),
  (1, 33, 1),
  (3, 33, 1),
  (4, 33, 4),
  (5, 33, 4),
  (6, 33, 4),
  (7, 33, 1),
  (1, 34, 1),
  (3, 34, 1),
  (4, 34, 1),
  (5, 34, 1),
  (6, 34, 1),
  (7, 34, 1),
  (1, 35, 1),
  (3, 35, 1),
  (4, 35, 1),
  (5, 35, 1),
  (6, 35, 1),
  (7, 35, 1),
  (1, 36, 1),
  (3, 36, 1),
  (4, 36, 1),
  (5, 36, 1),
  (6, 36, 1),
  (7, 36, 1),
  (1, 37, 1),
  (3, 37, 1),
  (4, 37, 1),
  (5, 37, 1),
  (6, 37, 1),
  (7, 37, 1),
  (1, 38, 1),
  (3, 38, 1),
  (4, 38, 1),
  (5, 38, 1),
  (6, 38, 1),
  (7, 38, 1),
  (1, 39, 1),
  (3, 39, 1),
  (4, 39, 1),
  (5, 39, 1),
  (6, 39, 1),
  (7, 39, 1),
  (1, 40, 1),
  (3, 40, 1),
  (4, 40, 1),
  (5, 40, 1),
  (6, 40, 1),
  (7, 40, 1),
  (1, 41, 1),
  (3, 41, 1),
  (4, 41, 1),
  (5, 41, 1),
  (6, 41, 1),
  (7, 41, 1),
  (1, 42, 1),
  (3, 42, 1),
  (4, 42, 1),
  (5, 42, 1),
  (6, 42, 1),
  (7, 42, 1),
  (1, 43, 1),
  (3, 43, 1),
  (4, 43, 1),
  (5, 43, 1),
  (6, 43, 1),
  (7, 43, 1),
  (1, 44, 1),
  (3, 44, 1),
  (4, 44, 1),
  (5, 44, 1),
  (6, 44, 1),
  (7, 44, 1),
  (1, 45, 1),
  (3, 45, 1),
  (4, 45, 1),
  (5, 45, 1),
  (6, 45, 1),
  (7, 45, 1),
  (1, 46, 1),
  (3, 46, 1),
  (4, 46, 1),
  (5, 46, 1),
  (6, 46, 1),
  (7, 46, 1),
  (1, 47, 1),
  (3, 47, 1),
  (4, 47, 1),
  (5, 47, 1),
  (6, 47, 1),
  (7, 47, 1),
  (1, 48, 1),
  (3, 48, 1),
  (4, 48, 1),
  (5, 48, 1),
  (6, 48, 1),
  (7, 48, 1),
  (1, 49, 1),
  (3, 49, 1),
  (4, 49, 1),
  (5, 49, 1),
  (6, 49, 1),
  (7, 49, 1),
  (1, 50, 1),
  (3, 50, 1),
  (4, 50, 1),
  (5, 50, 1),
  (6, 50, 1),
  (7, 50, 1),
  (1, 51, 1),
  (3, 51, 1),
  (4, 51, 1),
  (5, 51, 1),
  (6, 51, 1),
  (7, 51, 1),
  (1, 52, 1),
  (3, 52, 1),
  (4, 52, 1),
  (5, 52, 1),
  (6, 52, 1),
  (7, 52, 1),
  (1, 53, 1),
  (3, 53, 1),
  (4, 53, 1),
  (5, 53, 1),
  (6, 53, 1),
  (7, 53, 1),
  (1, 54, 1),
  (3, 54, 1),
  (4, 54, 1),
  (5, 54, 1),
  (6, 54, 1),
  (7, 54, 1),
  (1, 55, 1),
  (3, 55, 1),
  (4, 55, 1),
  (5, 55, 1),
  (6, 55, 1),
  (7, 55, 1),
  (1, 56, 1),
  (3, 56, 1),
  (4, 56, 1),
  (5, 56, 1),
  (6, 56, 1),
  (7, 56, 1),
  (1, 57, 1),
  (3, 57, 1),
  (4, 57, 1),
  (5, 57, 1),
  (6, 57, 1),
  (7, 57, 1),
  (1, 58, 1),
  (3, 58, 1),
  (4, 58, 1),
  (5, 58, 1),
  (6, 58, 1),
  (7, 58, 1),
  (1, 59, 1),
  (3, 59, 1),
  (4, 59, 1),
  (5, 59, 1),
  (6, 59, 1),
  (7, 59, 1),
  (1, 60, 1),
  (3, 60, 1),
  (4, 60, 1),
  (5, 60, 1),
  (6, 60, 1),
  (7, 60, 1),
  (1, 61, 1),
  (3, 61, 1),
  (4, 61, 1),
  (5, 61, 1),
  (6, 61, 1),
  (7, 61, 1),
  (1, 62, 1),
  (3, 62, 1),
  (4, 62, 1),
  (5, 62, 1),
  (6, 62, 1),
  (7, 62, 1),
  (1, 63, 1),
  (3, 63, 1),
  (4, 63, 1),
  (5, 63, 1),
  (6, 63, 1),
  (7, 63, 1),
  (1, 64, 1),
  (3, 64, 1),
  (4, 64, 1),
  (5, 64, 1),
  (6, 64, 1),
  (7, 64, 1),
  (1, 65, 1),
  (3, 65, 1),
  (4, 65, 1),
  (5, 65, 1),
  (6, 65, 1),
  (7, 65, 1),
  (1, 66, 1),
  (3, 66, 1),
  (4, 66, 1),
  (5, 66, 1),
  (6, 66, 1),
  (7, 66, 1),
  (1, 67, 1),
  (3, 67, 1),
  (4, 67, 1),
  (5, 67, 1),
  (6, 67, 1),
  (7, 67, 1),
  (1, 68, 1),
  (3, 68, 1),
  (4, 68, 1),
  (5, 68, 1),
  (6, 68, 1),
  (7, 68, 1),
  (4, 70, 3),
  (5, 70, 3),
  (6, 70, 3),
  (1, 71, 6),
  (3, 71, 7),
  (5, 71, 6),
  (1, 72, 6),
  (5, 72, 6),
  (5, 74, 2),
  (4, 81, 1),
  (5, 81, 6),
  (6, 82, 6),
  (3, 141, 7),
  (4, 141, 7),
  (5, 141, 7),
  (6, 141, 6),
  (7, 141, 7),
  (3, 142, 9),
  (4, 142, 9),
  (5, 142, 10),
  (6, 142, 9),
  (7, 142, 9),
  (3, 143, 1),
  (3, 144, 1),
  (3, 145, 1),
  (3, 146, 1),
  (3, 147, 1),
  (3, 148, 1),
  (3, 149, 1),
  (3, 150, 1),
  (5, 150, 1),
  (3, 151, 2),
  (6, 151, 2),
  (7, 151, 2),
  (3, 152, 2),
  (4, 213, 4),
  (4, 214, 1),
  (4, 215, 1),
  (5, 215, 1),
  (4, 216, 1),
  (4, 217, 1),
  (4, 218, 1),
  (4, 219, 1),
  (4, 220, 6),
  (4, 221, 6),
  (4, 222, 2),
  (5, 222, 2),
  (4, 223, 2),
  (5, 283, 1),
  (5, 284, 1),
  (5, 285, 1),
  (7, 285, 1),
  (5, 286, 1),
  (5, 287, 1),
  (6, 287, 1),
  (5, 288, 1),
  (5, 289, 6),
  (6, 289, 6),
  (7, 289, 6),
  (5, 290, 2),
  (6, 356, 1),
  (6, 357, 1),
  (7, 357, 1),
  (6, 358, 1),
  (7, 358, 1),
  (6, 359, 1),
  (7, 359, 7),
  (6, 360, 1),
  (6, 361, 1),
  (6, 362, 2),
  (7, 362, 2),
  (7, 426, 1),
  (7, 427, 1),
  (7, 428, 6),
  (7, 429, 2),
  (3, 430, 1),
  (3, 431, 1),
  (3, 432, 1),
  (1, 433, 3),
  (3, 433, 3),
  (1, 434, 3),
  (3, 434, 3),
  (7, 461, 3),
  (7, 462, 3),
  (1, 502, 9),
  (1, 503, 2),
  (1, 504, 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_search_word`
--

CREATE TABLE IF NOT EXISTS `ps_search_word` (
  `id_word` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop` INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` INT(10) UNSIGNED NOT NULL,
  `word`    VARCHAR(15)      NOT NULL,
  PRIMARY KEY (`id_word`),
  UNIQUE KEY `id_lang` (`id_lang`, `id_shop`, `word`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 505;

--
-- Vypisuji data pro tabulku `ps_search_word`
--

INSERT INTO `ps_search_word` (`id_word`, `id_shop`, `id_lang`, `word`) VALUES
  (143, 1, 1, '100'),
  (24, 1, 1, '2010'),
  (64, 1, 1, 'accessories'),
  (13, 1, 1, 'accessorize'),
  (284, 1, 1, 'adjustable'),
  (434, 1, 1, 'alcon'),
  (60, 1, 1, 'attention'),
  (54, 1, 1, 'beautiful'),
  (220, 1, 1, 'beige'),
  (217, 1, 1, 'belt'),
  (68, 1, 1, 'belts'),
  (431, 1, 1, 'black'),
  (76, 1, 1, 'blouse'),
  (80, 1, 1, 'blouses'),
  (72, 1, 1, 'blue'),
  (150, 1, 1, 'bottom'),
  (25, 1, 1, 'brand'),
  (287, 1, 1, 'bust'),
  (503, 1, 1, 'casual'),
  (47, 1, 1, 'chic'),
  (359, 1, 1, 'chiffon'),
  (37, 1, 1, 'collection'),
  (22, 1, 1, 'collections'),
  (152, 1, 1, 'colorful'),
  (11, 1, 1, 'comfortable'),
  (45, 1, 1, 'cool'),
  (430, 1, 1, 'cotton'),
  (19, 1, 1, 'creating'),
  (427, 1, 1, 'deep'),
  (29, 1, 1, 'delivering'),
  (5, 1, 1, 'demo'),
  (21, 1, 1, 'designed'),
  (28, 1, 1, 'designs'),
  (79, 1, 1, 'detail'),
  (433, 1, 1, 'domu'),
  (144, 1, 1, 'double'),
  (78, 1, 1, 'draped'),
  (142, 1, 1, 'dress'),
  (33, 1, 1, 'dresses'),
  (223, 1, 1, 'dressy'),
  (46, 1, 1, 'easy'),
  (360, 1, 1, 'elastic'),
  (50, 1, 1, 'elegance'),
  (213, 1, 1, 'evening'),
  (38, 1, 1, 'every'),
  (34, 1, 1, 'evolved'),
  (62, 1, 1, 'extends'),
  (1, 1, 1, 'faded'),
  (18, 1, 1, 'fashion'),
  (27, 1, 1, 'feminine'),
  (12, 1, 1, 'fit'),
  (35, 1, 1, 'full'),
  (151, 1, 1, 'girly'),
  (59, 1, 1, 'greatest'),
  (428, 1, 1, 'green'),
  (15, 1, 1, 'hat'),
  (67, 1, 1, 'hats'),
  (6, 1, 1, 'high'),
  (65, 1, 1, 'including'),
  (57, 1, 1, 'italy'),
  (39, 1, 1, 'item'),
  (357, 1, 1, 'knee'),
  (358, 1, 1, 'length'),
  (361, 1, 1, 'lining'),
  (219, 1, 1, 'linings'),
  (283, 1, 1, 'long'),
  (48, 1, 1, 'looks'),
  (56, 1, 1, 'made'),
  (58, 1, 1, 'manufactured'),
  (70, 1, 1, 'manufacturer'),
  (10, 1, 1, 'material'),
  (290, 1, 1, 'maxi'),
  (429, 1, 1, 'midi'),
  (7, 1, 1, 'neckline'),
  (61, 1, 1, 'now'),
  (26, 1, 1, 'offers'),
  (71, 1, 1, 'orange'),
  (41, 1, 1, 'part'),
  (55, 1, 1, 'pieces'),
  (221, 1, 1, 'pink'),
  (362, 1, 1, 'polyester'),
  (141, 1, 1, 'printed'),
  (63, 1, 1, 'range'),
  (16, 1, 1, 'ready'),
  (44, 1, 1, 'result'),
  (218, 1, 1, 'ruffled'),
  (288, 1, 1, 'ruffles'),
  (31, 1, 1, 'separates'),
  (4, 1, 1, 'shirt'),
  (69, 1, 1, 'shirts'),
  (66, 1, 1, 'shoes'),
  (502, 1, 1, 'short'),
  (52, 1, 1, 'signature'),
  (23, 1, 1, 'since'),
  (148, 1, 1, 'skater'),
  (149, 1, 1, 'skirt'),
  (504, 1, 1, 'sleeve'),
  (77, 1, 1, 'sleeved'),
  (356, 1, 1, 'sleeveless'),
  (3, 1, 1, 'sleeves'),
  (8, 1, 1, 'soft'),
  (32, 1, 1, 'statement'),
  (214, 1, 1, 'straight'),
  (285, 1, 1, 'straps'),
  (14, 1, 1, 'straw'),
  (9, 1, 1, 'stretchy'),
  (145, 1, 1, 'striped'),
  (53, 1, 1, 'style'),
  (30, 1, 1, 'stylish'),
  (17, 1, 1, 'summer'),
  (426, 1, 1, 'tank'),
  (215, 1, 1, 'thin'),
  (146, 1, 1, 'top'),
  (51, 1, 1, 'unmistakable'),
  (222, 1, 1, 'viscose'),
  (40, 1, 1, 'vital'),
  (216, 1, 1, 'waist'),
  (147, 1, 1, 'waisted'),
  (43, 1, 1, 'wardrobe'),
  (36, 1, 1, 'wear'),
  (20, 1, 1, 'well'),
  (432, 1, 1, 'white'),
  (286, 1, 1, 'wiring'),
  (42, 1, 1, 'woman'),
  (289, 1, 1, 'yellow'),
  (49, 1, 1, 'youthful');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_sekeyword`
--

CREATE TABLE IF NOT EXISTS `ps_sekeyword` (
  `id_sekeyword`  INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop`       INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group` INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `keyword`       VARCHAR(256)     NOT NULL,
  `date_add`      DATETIME         NOT NULL,
  PRIMARY KEY (`id_sekeyword`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_sekeyword`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_shop`
--

CREATE TABLE IF NOT EXISTS `ps_shop` (
  `id_shop`       INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop_group` INT(11) UNSIGNED NOT NULL,
  `name`          VARCHAR(64)      NOT NULL,
  `id_category`   INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_theme`      INT(1) UNSIGNED  NOT NULL,
  `active`        TINYINT(1)       NOT NULL DEFAULT '1',
  `deleted`       TINYINT(1)       NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`, `deleted`),
  KEY `id_category` (`id_category`),
  KEY `id_theme` (`id_theme`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_shop`
--

INSERT INTO `ps_shop` (`id_shop`, `id_shop_group`, `name`, `id_category`, `id_theme`, `active`, `deleted`) VALUES
  (1, 1, 'Levné kontaktní čočky - OFTEX', 2, 2, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_shop_group`
--

CREATE TABLE IF NOT EXISTS `ps_shop_group` (
  `id_shop_group`  INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`           VARCHAR(64)      NOT NULL,
  `share_customer` TINYINT(1)       NOT NULL,
  `share_order`    TINYINT(1)       NOT NULL,
  `share_stock`    TINYINT(1)       NOT NULL,
  `active`         TINYINT(1)       NOT NULL DEFAULT '1',
  `deleted`        TINYINT(1)       NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_shop_group`),
  KEY `deleted` (`deleted`, `name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_shop_group`
--

INSERT INTO `ps_shop_group` (`id_shop_group`, `name`, `share_customer`, `share_order`, `share_stock`, `active`, `deleted`)
VALUES
  (1, 'Default', 0, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_shop_url`
--

CREATE TABLE IF NOT EXISTS `ps_shop_url` (
  `id_shop_url`  INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop`      INT(11) UNSIGNED NOT NULL,
  `domain`       VARCHAR(150)     NOT NULL,
  `domain_ssl`   VARCHAR(150)     NOT NULL,
  `physical_uri` VARCHAR(64)      NOT NULL,
  `virtual_uri`  VARCHAR(64)      NOT NULL,
  `main`         TINYINT(1)       NOT NULL,
  `active`       TINYINT(1)       NOT NULL,
  PRIMARY KEY (`id_shop_url`),
  UNIQUE KEY `full_shop_url` (`domain`, `physical_uri`, `virtual_uri`),
  UNIQUE KEY `full_shop_url_ssl` (`domain_ssl`, `physical_uri`, `virtual_uri`),
  KEY `id_shop` (`id_shop`, `main`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_shop_url`
--

INSERT INTO `ps_shop_url` (`id_shop_url`, `id_shop`, `domain`, `domain_ssl`, `physical_uri`, `virtual_uri`, `main`, `active`)
VALUES
  (1, 1, 'cockytest.zcom.cz', 'cockytest.zcom.cz', '/', '', 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smarty_cache`
--

CREATE TABLE IF NOT EXISTS `ps_smarty_cache` (
  `id_smarty_cache` CHAR(40)  NOT NULL,
  `name`            CHAR(40)  NOT NULL,
  `cache_id`        VARCHAR(254)       DEFAULT NULL,
  `modified`        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content`         LONGTEXT  NOT NULL,
  PRIMARY KEY (`id_smarty_cache`),
  KEY `name` (`name`),
  KEY `cache_id` (`cache_id`),
  KEY `modified` (`modified`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_smarty_cache`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smarty_last_flush`
--

CREATE TABLE IF NOT EXISTS `ps_smarty_last_flush` (
  `type`       ENUM ('compile', 'template') NOT NULL DEFAULT 'compile',
  `last_flush` DATETIME                     NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`type`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_smarty_last_flush`
--

INSERT INTO `ps_smarty_last_flush` (`type`, `last_flush`) VALUES
  ('template', '2017-02-21 14:59:34');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smarty_lazy_cache`
--

CREATE TABLE IF NOT EXISTS `ps_smarty_lazy_cache` (
  `template_hash` VARCHAR(32)  NOT NULL DEFAULT '',
  `cache_id`      VARCHAR(255) NOT NULL DEFAULT '',
  `compile_id`    VARCHAR(32)  NOT NULL DEFAULT '',
  `filepath`      VARCHAR(255) NOT NULL DEFAULT '',
  `last_update`   DATETIME     NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`template_hash`, `cache_id`, `compile_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_smarty_lazy_cache`
--

INSERT INTO `ps_smarty_lazy_cache` (`template_hash`, `cache_id`, `compile_id`, `filepath`, `last_update`) VALUES
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170222|1|1|1|16', '',
   'blockspecials_tab/20170222/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php',
   '2017-02-22 02:01:08'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170223|1|1|1|16', '', 'blockspecials_tab/20170223/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-02-23 02:02:37'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170224|1|1|1|16', '', 'blockspecials_tab/20170224/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-02-24 02:11:45'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170225|1|1|1|16', '', 'blockspecials_tab/20170225/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-02-25 02:07:32'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170226|1|1|1|16', '', 'blockspecials_tab/20170226/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-02-26 02:13:05'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170227|1|1|1|16', '', 'blockspecials_tab/20170227/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-02-27 02:06:16'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170228|1|1|1|16', '', 'blockspecials_tab/20170228/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-02-28 02:10:50'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170301|1|1|1|16', '', 'blockspecials_tab/20170301/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-01 02:13:48'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170302|1|1|1|16', '', 'blockspecials_tab/20170302/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-02 02:21:11'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170303|1|1|1|16', '', 'blockspecials_tab/20170303/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-03 02:23:39'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170304|1|1|1|16', '', 'blockspecials_tab/20170304/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-04 02:24:54'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170305|1|1|1|16', '', 'blockspecials_tab/20170305/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-05 02:24:43'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170306|1|1|1|16', '', 'blockspecials_tab/20170306/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-06 02:25:24'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170307|1|1|1|16', '', 'blockspecials_tab/20170307/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-07 02:32:11'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170308|1|1|1|16', '', 'blockspecials_tab/20170308/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-08 02:30:07'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170309|1|1|1|16', '', 'blockspecials_tab/20170309/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-09 02:32:00'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170310|1|1|1|16', '', 'blockspecials_tab/20170310/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-10 02:43:43'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170311|1|1|1|16', '', 'blockspecials_tab/20170311/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-11 02:46:55'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170312|1|1|1|16', '', 'blockspecials_tab/20170312/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-12 02:47:12'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170313|1|1|1|16', '', 'blockspecials_tab/20170313/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-13 02:42:06'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170314|1|1|1|16', '', 'blockspecials_tab/20170314/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-14 02:49:36'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170315|1|1|1|16', '', 'blockspecials_tab/20170315/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-15 02:48:28'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170316|1|1|1|16', '', 'blockspecials_tab/20170316/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-16 02:53:06'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170317|1|1|1|16', '', 'blockspecials_tab/20170317/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-17 02:55:07'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170318|1|1|1|16', '', 'blockspecials_tab/20170318/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-18 02:49:53'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170319|1|1|1|16', '', 'blockspecials_tab/20170319/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-19 02:52:42'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170320|1|1|1|16', '', 'blockspecials_tab/20170320/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-20 02:46:55'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170321|1|1|1|16', '', 'blockspecials_tab/20170321/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-21 02:53:01'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170322|1|1|1|16', '', 'blockspecials_tab/20170322/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-22 02:52:30'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170323|1|1|1|16', '', 'blockspecials_tab/20170323/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-23 02:53:37'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170324|1|1|1|16', '', 'blockspecials_tab/20170324/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-24 02:53:19'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170325|1|1|1|16', '', 'blockspecials_tab/20170325/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-25 02:50:31'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170326|1|1|1|16', '', 'blockspecials_tab/20170326/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-26 03:52:49'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170327|1|1|1|16', '', 'blockspecials_tab/20170327/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-27 02:50:36'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170328|1|1|1|16', '', 'blockspecials_tab/20170328/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-28 02:49:01'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170329|1|1|1|16', '', 'blockspecials_tab/20170329/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-29 02:51:25'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170330|1|1|1|16', '', 'blockspecials_tab/20170330/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-30 02:53:21'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170331|1|1|1|16', '', 'blockspecials_tab/20170331/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-03-31 02:55:06'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170401|1|1|1|16', '', 'blockspecials_tab/20170401/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-04-01 02:56:17'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170402|1|1|1|16', '', 'blockspecials_tab/20170402/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-04-02 02:58:34'),
  ('0f21b785e8414b46ca1d0a761f614482', 'blockspecials-tab|20170403|1|1|1|16', '', 'blockspecials_tab/20170403/1/1/1/16/12/9c/60/129c60bf4771c1c26ce0164a1369338d58ef5832.tab.tpl.php', '2017-04-03 02:54:02'),
  ('0f7bac9e1012768e5e83309ac7317bb4', 'blockcategories|1|1|1|16|1', '', 'blockcategories/1/1/1/16/1/af/2c/da/af2cda38a2548b439f5aab2969d24fd36b356b40.blockcategories_footer.tpl.php', '2017-02-22 02:01:10'),
  ('18f1bf4c50d4147c3d594422eda79263', 'blockcontactinfos|1|1|1|16', '', 'blockcontactinfos/1/1/1/16/03/93/ad/0393ad4bfb06610793fdd8963bf97aa517b26ba2.blockcontactinfos.tpl.php', '2017-02-22 02:01:10'),
  ('316743e32fea0c7139d15ba64b720942', 'homefeatured|1|1|1|16', '', 'homefeatured/1/1/1/16/51/92/27/519227cc7b276b100e8de838de74ecbadd8419eb.homefeatured.tpl.php', '2017-02-22 02:01:08'),
  ('3805eee6bd8c0c6476d378fa3c8a2894', 'tmmegamenu|1|1|1|16', '', 'tmmegamenu/1/1/1/16/36/f4/11/36f411f1c902e02d78b98f5863422525ff8c25bf.menu.tpl.php', '2017-02-22 02:01:08'),
  ('4aa9fd636e35860803390375c3f198a2', 'smartbloghomelatestnews|1|1|1|16', '', 'smartbloghomelatestnews/1/1/1/16/57/74/88/577488a1302b95d909239200cda490a9afe539dd.smartblog_latest_news.tpl.php', '2017-02-22 02:01:08'),
  ('4e69d409c6620dec042ee12507d6a227', 'homeslider|1|1|1|16', '', 'homeslider/1/1/1/16/50/ff/d8/50ffd817b63376a08d29017f94ed2a9e4803d34d.homeslider.tpl.php', '2017-02-22 02:01:10'),
  ('531f9dcc5dd7d7ed7d92fa053c544a5e', 'blocksocial|1|1|1|16', '', 'blocksocial/1/1/1/16/22/75/93/227593d9100853366d553b30e83f1829678b3273.blocksocial.tpl.php', '2017-02-22 02:01:10'),
  ('5c68b707c49090ff94cd601b0f141425', '', '', '1f/61/61/1f616153f2b3b802b4607fe1415c1e6c1698ac9b.blockwishlist_button.tpl.php', '2017-02-22 02:01:09'),
  ('7048625e43fed13a5566e283a62d5567', 'tmnewsletter|1|1|1|16', '', 'tmnewsletter/1/1/1/16/ca/6d/67/ca6d672e06c730fa71c9de0239d1b89fd3d900d5.tmnewsletter_guest.tpl.php', '2017-02-22 10:20:23'),
  ('71570ad427d0da742954488bf8794d43', 'blockcmsinfo|1|1|1|16', '', 'blockcmsinfo/1/1/1/16/1e/88/77/1e887794795c2e947daa42c4d15d3f4d5e476641.blockcmsinfo.tpl.php', '2017-02-22 02:01:08'),
  ('7ad1828c0ef52207923791b8ab1b4927', 'homefeatured-tab|1|1|1|16', '', 'homefeatured_tab/1/1/1/16/6d/dc/2d/6ddc2d6d75989688a72881fecbece6e6e8c1c06a.tab.tpl.php', '2017-02-22 02:01:08'),
  ('80cdc058fe2ee829c28c5584ff7e83aa', 'blockmyaccountfooter|1|1|1|16', '', 'blockmyaccountfooter/1/1/1/16/4e/c4/08/4ec4089b17f6474da38248014c8fbedcf932cc0b.blockmyaccountfooter.tpl.php', '2017-02-22 02:01:10'),
  ('9f746c1bfc436df1991220de66f83716', 'blockfacebook|1|1|1|16', '', 'blockfacebook/1/1/1/16/6e/f4/12/6ef412fcdce49078be7b4faa64fb0222ec912aca.blockfacebook.tpl.php', '2017-02-22 02:01:08'),
  ('a3afc6734aa43f49cf0ff37890b70b9b', 'blockbestsellers-home|1|1|1|16', '', 'blockbestsellers_home/1/1/1/16/a8/df/30/a8df30c8b854002a7c2faef32818a800e1b4c816.blockbestsellers-home.tpl.php', '2017-02-22 02:01:09'),
  ('a6a852d634f20769fe6f3fb57c935a35', 'blockbestsellers-tab|1|1|1|16', '', 'blockbestsellers_tab/1/1/1/16/50/a7/f4/50a7f40c0165ecc3daee9bfead97b452669431f1.tab.tpl.php', '2017-02-22 02:01:08'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170223|1|1|1|16', '', '', '2017-02-23 02:02:37'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170224|1|1|1|16', '', '', '2017-02-24 02:11:45'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170225|1|1|1|16', '', '', '2017-02-25 02:07:32'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170226|1|1|1|16', '', '', '2017-02-26 02:13:05'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170227|1|1|1|16', '', '', '2017-02-27 02:06:16'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170228|1|1|1|16', '', '', '2017-02-28 02:10:50'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170301|1|1|1|16', '', '', '2017-03-01 02:13:48'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170302|1|1|1|16', '', '', '2017-03-02 02:21:11'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170303|1|1|1|16', '', '', '2017-03-03 02:23:39'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170304|1|1|1|16', '', '', '2017-03-04 02:24:54'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170305|1|1|1|16', '', '', '2017-03-05 02:24:43'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170306|1|1|1|16', '', '', '2017-03-06 02:25:24'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170307|1|1|1|16', '', '', '2017-03-07 02:32:11'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170308|1|1|1|16', '', '', '2017-03-08 02:30:07'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170309|1|1|1|16', '', '', '2017-03-09 02:32:00'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170310|1|1|1|16', '', '', '2017-03-10 02:43:43'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170311|1|1|1|16', '', '', '2017-03-11 02:46:56'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170312|1|1|1|16', '', '', '2017-03-12 02:47:12'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170313|1|1|1|16', '', '', '2017-03-13 02:42:06'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170314|1|1|1|16', '', '', '2017-03-14 02:49:36'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170315|1|1|1|16', '', '', '2017-03-15 02:48:28'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170316|1|1|1|16', '', '', '2017-03-16 02:53:06'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170317|1|1|1|16', '', '', '2017-03-17 02:55:07'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170318|1|1|1|16', '', '', '2017-03-18 02:49:53'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170319|1|1|1|16', '', '', '2017-03-19 02:52:42'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170320|1|1|1|16', '', '', '2017-03-20 02:46:55'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170321|1|1|1|16', '', '', '2017-03-21 02:53:01'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170322|1|1|1|16', '', '', '2017-03-22 02:52:30'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170323|1|1|1|16', '', '', '2017-03-23 02:53:37'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170324|1|1|1|16', '', '', '2017-03-24 02:53:19'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170325|1|1|1|16', '', '', '2017-03-25 02:50:31'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170326|1|1|1|16', '', '', '2017-03-26 03:52:49'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170327|1|1|1|16', '', '', '2017-03-27 02:50:36'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170328|1|1|1|16', '', '', '2017-03-28 02:49:01'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170329|1|1|1|16', '', '', '2017-03-29 02:51:25'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170331|1|1|1|16', '', '', '2017-03-31 10:14:54'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170401|1|1|1|16', '', '', '2017-04-01 02:56:17'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170402|1|1|1|16', '', '', '2017-04-02 02:58:34'),
  ('b111259e0857f1d87d6e73e8ba394e67', 'blocknewproducts-home|20170403|1|1|1|16', '', '', '2017-04-03 02:54:02'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170223|1|1|1|16', '', '', '2017-02-23 02:02:37'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170224|1|1|1|16', '', '', '2017-02-24 02:11:45'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170225|1|1|1|16', '', '', '2017-02-25 02:07:31'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170226|1|1|1|16', '', '', '2017-02-26 02:13:05'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170227|1|1|1|16', '', '', '2017-02-27 02:06:16'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170228|1|1|1|16', '', '', '2017-02-28 02:10:50'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170301|1|1|1|16', '', '', '2017-03-01 02:13:48'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170302|1|1|1|16', '', '', '2017-03-02 02:21:11'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170303|1|1|1|16', '', '', '2017-03-03 02:23:39'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170304|1|1|1|16', '', '', '2017-03-04 02:24:54'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170305|1|1|1|16', '', '', '2017-03-05 02:24:43'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170306|1|1|1|16', '', '', '2017-03-06 02:25:24'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170307|1|1|1|16', '', '', '2017-03-07 02:32:11'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170308|1|1|1|16', '', '', '2017-03-08 02:30:07'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170309|1|1|1|16', '', '', '2017-03-09 02:32:00'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170310|1|1|1|16', '', '', '2017-03-10 02:43:43'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170311|1|1|1|16', '', '', '2017-03-11 02:46:55'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170312|1|1|1|16', '', '', '2017-03-12 02:47:12'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170313|1|1|1|16', '', '', '2017-03-13 02:42:06'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170314|1|1|1|16', '', '', '2017-03-14 02:49:36'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170315|1|1|1|16', '', '', '2017-03-15 02:48:28'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170316|1|1|1|16', '', '', '2017-03-16 02:53:06'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170317|1|1|1|16', '', '', '2017-03-17 02:55:07'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170318|1|1|1|16', '', '', '2017-03-18 02:49:53'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170319|1|1|1|16', '', '', '2017-03-19 02:52:42'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170320|1|1|1|16', '', '', '2017-03-20 02:46:55'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170321|1|1|1|16', '', '', '2017-03-21 02:53:01'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170322|1|1|1|16', '', '', '2017-03-22 02:52:30'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170323|1|1|1|16', '', '', '2017-03-23 02:53:37'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170324|1|1|1|16', '', '', '2017-03-24 02:53:19'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170325|1|1|1|16', '', '', '2017-03-25 02:50:31'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170326|1|1|1|16', '', '', '2017-03-26 03:52:49'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170327|1|1|1|16', '', '', '2017-03-27 02:50:36'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170328|1|1|1|16', '', '', '2017-03-28 02:49:01'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170329|1|1|1|16', '', '', '2017-03-29 02:51:25'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170331|1|1|1|16', '', '', '2017-03-31 10:14:54'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170401|1|1|1|16', '', '', '2017-04-01 02:56:17'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170402|1|1|1|16', '', '', '2017-04-02 02:58:34'),
  ('bb464a540ee4bba9ce83f1df0647dbcf', 'blocknewproducts-tab|20170403|1|1|1|16', '', '', '2017-04-03 02:54:02'),
  ('cdacbe67360bf327ff5be68fc3a5b30a', 'tmsearch|1|1|1|16', '', 'tmsearch/1/1/1/16/6c/5a/d4/6c5ad44faa9c3aa8eda9cabb49bb04c60b15f7c9.tmsearch.tpl.php', '2017-02-22 02:01:08'),
  ('d22e011ba659b15ed132004f18c440a9', '', '', 'ee/c1/be/eec1be1e9df8b86676c75e55aa07c96b7d07a30b.tmlistingimages.tpl.php', '2017-02-22 02:01:09'),
  ('d3daffa9c8b689c04b1bf65cc2b511ee', 'tmmediaparallax|1|1|1|16', '', 'tmmediaparallax/1/1/1/16/1b/68/a6/1b68a6fed955b04c78fccbcd2613e9fd8d14a013.layouts.tpl.php', '2017-02-22 02:01:08'),
  ('e7a93cfd84c79b39c29aa7cb629697bc', 'blockcms|2|1|1|1|16', '', 'blockcms/2/1/1/1/16/99/c8/b1/99c8b18265a5da528aca65299ce3e727add4bdeb.blockcms.tpl.php', '2017-02-22 02:01:10'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170222|1|1|1|16', '', 'blockspecials_home/20170222/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-02-22 02:01:09'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170223|1|1|1|16', '', 'blockspecials_home/20170223/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-02-23 02:02:37'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170224|1|1|1|16', '', 'blockspecials_home/20170224/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-02-24 02:11:45'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170225|1|1|1|16', '', 'blockspecials_home/20170225/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-02-25 02:07:32'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170226|1|1|1|16', '', 'blockspecials_home/20170226/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-02-26 02:13:05'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170227|1|1|1|16', '', 'blockspecials_home/20170227/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-02-27 02:06:16'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170228|1|1|1|16', '', 'blockspecials_home/20170228/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-02-28 02:10:51'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170301|1|1|1|16', '', 'blockspecials_home/20170301/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-01 02:13:48'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170302|1|1|1|16', '', 'blockspecials_home/20170302/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-02 02:21:11'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170303|1|1|1|16', '', 'blockspecials_home/20170303/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-03 02:23:39'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170304|1|1|1|16', '', 'blockspecials_home/20170304/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-04 02:24:54'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170305|1|1|1|16', '', 'blockspecials_home/20170305/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-05 02:24:43'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170306|1|1|1|16', '', 'blockspecials_home/20170306/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-06 02:25:24'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170307|1|1|1|16', '', 'blockspecials_home/20170307/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-07 02:32:11'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170308|1|1|1|16', '', 'blockspecials_home/20170308/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-08 02:30:07'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170309|1|1|1|16', '', 'blockspecials_home/20170309/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-09 02:32:00'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170310|1|1|1|16', '', 'blockspecials_home/20170310/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-10 02:43:43'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170311|1|1|1|16', '', 'blockspecials_home/20170311/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-11 02:46:56'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170312|1|1|1|16', '', 'blockspecials_home/20170312/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-12 02:47:12'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170313|1|1|1|16', '', 'blockspecials_home/20170313/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-13 02:42:06'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170314|1|1|1|16', '', 'blockspecials_home/20170314/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-14 02:49:36'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170315|1|1|1|16', '', 'blockspecials_home/20170315/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-15 02:48:28'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170316|1|1|1|16', '', 'blockspecials_home/20170316/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-16 02:53:06'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170317|1|1|1|16', '', 'blockspecials_home/20170317/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-17 02:55:07'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170318|1|1|1|16', '', 'blockspecials_home/20170318/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-18 02:49:53'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170319|1|1|1|16', '', 'blockspecials_home/20170319/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-19 02:52:42'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170320|1|1|1|16', '', 'blockspecials_home/20170320/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-20 02:46:55'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170321|1|1|1|16', '', 'blockspecials_home/20170321/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-21 02:53:01'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170322|1|1|1|16', '', 'blockspecials_home/20170322/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-22 02:52:30'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170323|1|1|1|16', '', 'blockspecials_home/20170323/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-23 02:53:37'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170324|1|1|1|16', '', 'blockspecials_home/20170324/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-24 02:53:19'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170325|1|1|1|16', '', 'blockspecials_home/20170325/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-25 02:50:31'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170326|1|1|1|16', '', 'blockspecials_home/20170326/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php', '2017-03-26 03:52:49'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170327|1|1|1|16', '',
   'blockspecials_home/20170327/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php',
   '2017-03-27 02:50:36'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170328|1|1|1|16', '',
   'blockspecials_home/20170328/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php',
   '2017-03-28 02:49:01'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170329|1|1|1|16', '',
   'blockspecials_home/20170329/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php',
   '2017-03-29 02:51:25'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170330|1|1|1|16', '',
   'blockspecials_home/20170330/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php',
   '2017-03-30 02:53:21'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170331|1|1|1|16', '',
   'blockspecials_home/20170331/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php',
   '2017-03-31 02:55:06'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170401|1|1|1|16', '',
   'blockspecials_home/20170401/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php',
   '2017-04-01 02:56:17'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170402|1|1|1|16', '',
   'blockspecials_home/20170402/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php',
   '2017-04-02 02:58:34'),
  ('ea1959f3682a7f1b78997b46d8d7b5e5', 'blockspecials-home|20170403|1|1|1|16', '',
   'blockspecials_home/20170403/1/1/1/16/15/9c/ed/159ced2f56127a460bde641bee3f3076259fb586.blockspecials-home.tpl.php',
   '2017-04-03 02:54:02'),
  ('ef11b6322484fd36c3ae1f7769d81b08', 'blockpermanentlinks-header|1|1|1|16', '',
   'blockpermanentlinks_header/1/1/1/16/bd/e7/76/bde776fd5dd9cf835aa05254a597c72dd7b32a0e.blockpermanentlinks-header.tpl.php',
   '2017-02-22 02:01:10');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_category`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_category` (
  `id_smart_blog_category` INT(11)  NOT NULL AUTO_INCREMENT,
  `id_parent`              VARCHAR(45)       DEFAULT NULL,
  `position`               VARCHAR(45)       DEFAULT NULL,
  `desc_limit`             VARCHAR(45)       DEFAULT NULL,
  `active`                 INT(11)           DEFAULT NULL,
  `created`                DATETIME NOT NULL,
  `modified`               DATETIME          DEFAULT NULL,
  PRIMARY KEY (`id_smart_blog_category`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_smart_blog_category`
--

INSERT INTO `ps_smart_blog_category` (`id_smart_blog_category`, `id_parent`, `position`, `desc_limit`, `active`, `created`, `modified`)
VALUES
  (1, '0', NULL, NULL, 1, '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_category_lang`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_category_lang` (
  `id_smart_blog_category` INT(11) NOT NULL,
  `id_lang`                INT(11) NOT NULL DEFAULT '0',
  `meta_title`             VARCHAR(150)     DEFAULT NULL,
  `meta_keyword`           VARCHAR(200)     DEFAULT NULL,
  `meta_description`       VARCHAR(350)     DEFAULT NULL,
  `description`            VARCHAR(10000)   DEFAULT NULL,
  `link_rewrite`           VARCHAR(100)     DEFAULT NULL,
  PRIMARY KEY (`id_smart_blog_category`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_smart_blog_category_lang`
--

INSERT INTO `ps_smart_blog_category_lang` (`id_smart_blog_category`, `id_lang`, `meta_title`, `meta_keyword`, `meta_description`, `description`, `link_rewrite`)
VALUES
  (1, 1, 'Uncategories', NULL, NULL, NULL, 'uncategories');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_category_shop`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_category_shop` (
  `id_smart_blog_category_shop` INT(11) NOT NULL AUTO_INCREMENT,
  `id_smart_blog_category`      INT(11) NOT NULL,
  `id_shop`                     INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_smart_blog_category_shop`, `id_smart_blog_category`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_smart_blog_category_shop`
--

INSERT INTO `ps_smart_blog_category_shop` (`id_smart_blog_category_shop`, `id_smart_blog_category`, `id_shop`) VALUES
  (1, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_comment`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_comment` (
  `id_smart_blog_comment` INT(11)  NOT NULL AUTO_INCREMENT,
  `id_parent`             INT(11)           DEFAULT NULL,
  `id_customer`           INT(11)           DEFAULT NULL,
  `id_post`               INT(11)           DEFAULT NULL,
  `name`                  VARCHAR(256)      DEFAULT NULL,
  `email`                 VARCHAR(90)       DEFAULT NULL,
  `website`               VARCHAR(128)      DEFAULT NULL,
  `content`               TEXT,
  `active`                INT(11)           DEFAULT NULL,
  `created`               DATETIME NOT NULL,
  PRIMARY KEY (`id_smart_blog_comment`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_smart_blog_comment`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_comment_shop`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_comment_shop` (
  `id_smart_blog_comment_shop` INT(11) NOT NULL AUTO_INCREMENT,
  `id_smart_blog_comment`      INT(11) NOT NULL,
  `id_shop`                    INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_smart_blog_comment_shop`, `id_smart_blog_comment`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_smart_blog_comment_shop`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_imagetype`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_imagetype` (
  `id_smart_blog_imagetype` INT(11) NOT NULL AUTO_INCREMENT,
  `type_name`               VARCHAR(45)      DEFAULT NULL,
  `width`                   VARCHAR(45)      DEFAULT NULL,
  `height`                  VARCHAR(45)      DEFAULT NULL,
  `type`                    VARCHAR(45)      DEFAULT NULL,
  `active`                  INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id_smart_blog_imagetype`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 8;

--
-- Vypisuji data pro tabulku `ps_smart_blog_imagetype`
--

INSERT INTO `ps_smart_blog_imagetype` (`id_smart_blog_imagetype`, `type_name`, `width`, `height`, `type`, `active`)
VALUES
  (1, 'home-default', '270', '180', 'post', 1),
  (2, 'home-small', '98', '65', 'post', 1),
  (3, 'single-default', '870', '580', 'post', 1),
  (4, 'home-small', '65', '65', 'Category', 1),
  (5, 'home-default', '240', '240', 'Category', 1),
  (6, 'single-default', '800', '800', 'Category', 1),
  (7, 'author-default', '100', '100', 'Author', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_media`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_media` (
  `id_media`          INT(11) NOT NULL AUTO_INCREMENT,
  `id_post`           INT(11)          DEFAULT NULL,
  `id_parent`         INT(11)          DEFAULT NULL,
  `position`          INT(11)          DEFAULT NULL,
  `media_path`        VARCHAR(45)      DEFAULT NULL,
  `media_name`        VARCHAR(45)      DEFAULT NULL,
  `media_description` VARCHAR(45)      DEFAULT NULL,
  PRIMARY KEY (`id_media`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_smart_blog_media`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_post`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_post` (
  `id_smart_blog_post` INT(11)  NOT NULL AUTO_INCREMENT,
  `id_author`          INT(11)           DEFAULT NULL,
  `id_category`        INT(11)           DEFAULT NULL,
  `position`           INT(11)           DEFAULT NULL,
  `active`             INT(11)           DEFAULT NULL,
  `available`          INT(11)           DEFAULT NULL,
  `created`            DATETIME NOT NULL,
  `modified`           DATETIME          DEFAULT NULL,
  `viewed`             INT(11)           DEFAULT NULL,
  `is_featured`        INT(11)           DEFAULT NULL,
  `comment_status`     INT(11)           DEFAULT NULL,
  `post_type`          VARCHAR(45)       DEFAULT NULL,
  `image`              VARCHAR(245)      DEFAULT NULL,
  PRIMARY KEY (`id_smart_blog_post`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 5;

--
-- Vypisuji data pro tabulku `ps_smart_blog_post`
--

INSERT INTO `ps_smart_blog_post` (`id_smart_blog_post`, `id_author`, `id_category`, `position`, `active`, `available`, `created`, `modified`, `viewed`, `is_featured`, `comment_status`, `post_type`, `image`)
VALUES
  (1, 1, 1, 0, 1, 1, '2016-09-19 13:46:20', NULL, 0, NULL, 1, '0', NULL),
  (2, 1, 1, 0, 1, 1, '2016-09-19 13:46:20', NULL, 0, NULL, 1, '0', NULL),
  (3, 1, 1, 0, 1, 1, '2016-09-19 13:46:20', NULL, 0, NULL, 1, '0', NULL),
  (4, 1, 1, 0, 1, 1, '2016-09-19 13:46:20', NULL, 1, NULL, 1, '0', NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_post_category`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_post_category` (
  `id_smart_blog_post_category` INT(11) NOT NULL,
  `id_smart_blog_category`      INT(11) DEFAULT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_smart_blog_post_category`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_post_lang`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_post_lang` (
  `id_smart_blog_post` INT(11)     NOT NULL,
  `id_lang`            VARCHAR(45) NOT NULL DEFAULT '',
  `meta_title`         VARCHAR(150)         DEFAULT NULL,
  `meta_keyword`       VARCHAR(200)         DEFAULT NULL,
  `meta_description`   VARCHAR(450)         DEFAULT NULL,
  `short_description`  VARCHAR(450)         DEFAULT NULL,
  `content`            TEXT,
  `link_rewrite`       VARCHAR(45)          DEFAULT NULL,
  PRIMARY KEY (`id_smart_blog_post`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_smart_blog_post_lang`
--

INSERT INTO `ps_smart_blog_post_lang` (`id_smart_blog_post`, `id_lang`, `meta_title`, `meta_keyword`, `meta_description`, `short_description`, `content`, `link_rewrite`)
VALUES
  (1, '1', 'Lorem ipsum dolor sit amet', NULL,
   'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vestibulum erat at neque fermentum, in auctor justo hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id felis tempor, mollis ligula ac, iaculis massa. Aliquam nec mollis neque, eget laoreet nibh. Vivamus dictum tempor enim, a porttitor dui lacinia vitae.',
   'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vestibulum erat at neque fermentum, in auctor justo hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id feli',
   'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vestibulum erat at neque fermentum, in auctor justo hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id felis tempor, mollis ligula ac, iaculis massa. Aliquam nec mollis neque, eget laoreet nibh. Vivamus dictum tempor enim, a porttitor dui lacinia vitae.',
   'lorem-ipsum-dolor-sit-amet'),
  (2, '1', 'Cras in sem in arcu ultrices', NULL,
   'Cras in sem in arcu ultrices egestas sit amet nec metus. Suspendisse magna nisi, cursus ut condimentum eu, dapibus at dolor. Sed venenatis sapien quis urna consequat, quis tempor neque porttitor. Vivamus iaculis eleifend varius. Vestibulum quis justo massa. Mauris et eros mollis, placerat mauris nec, mattis purus.',
   'Cras in sem in arcu ultrices egestas sit amet nec metus. Suspendisse magna nisi, cursus ut condimentum eu, dapibus at dolor. Sed venenatis sapien quis urna consequat, quis tempor neque porttitor. Viva',
   'Cras in sem in arcu ultrices egestas sit amet nec metus. Suspendisse magna nisi, cursus ut condimentum eu, dapibus at dolor. Sed venenatis sapien quis urna consequat, quis tempor neque porttitor. Vivamus iaculis eleifend varius. Vestibulum quis justo massa. Mauris et eros mollis, placerat mauris nec, mattis purus.',
   'cras-in-sem-in-arcu-ultrices'),
  (3, '1', 'Aliquam elementum lorem ac efficitur tristique', NULL,
   ' Aliquam elementum lorem ac efficitur tristique. Duis vehicula non sapien eget rhoncus. Nulla et congue nunc, id eleifend neque. In hac habitasse platea dictumst. Nunc lacinia fringilla mi. Praesent quam nunc, pretium et aliquam ut, sollicitudin vel augue. Donec semper luctus felis, ut condimentum nisl facilisis eget. Morbi ac laoreet neque. Sed luctus dapibus lorem ut egestas.',
   ' Aliquam elementum lorem ac efficitur tristique. Duis vehicula non sapien eget rhoncus. Nulla et congue nunc, id eleifend neque. In hac habitasse platea dictumst. Nunc lacinia fringilla mi. Praesent q',
   ' Aliquam elementum lorem ac efficitur tristique. Duis vehicula non sapien eget rhoncus. Nulla et congue nunc, id eleifend neque. In hac habitasse platea dictumst. Nunc lacinia fringilla mi. Praesent quam nunc, pretium et aliquam ut, sollicitudin vel augue. Donec semper luctus felis, ut condimentum nisl facilisis eget. Morbi ac laoreet neque. Sed luctus dapibus lorem ut egestas.',
   'aliquam-elementum-lorem-ac-efficitur-tristiqu'),
  (4, '1', 'Pellentesque sollicitudin iaculis gravida', NULL,
   'Pellentesque sollicitudin iaculis gravida. Praesent ac tortor dapibus, imperdiet arcu in, aliquet est. Praesent ut risus tincidunt, euismod diam sit amet, vulputate dui. Cras luctus turpis non quam pretium, quis ornare libero vulputate. Nullam felis felis, vestibulum ac placerat sit amet, pretium ut dolor.',
   'Pellentesque sollicitudin iaculis gravida. Praesent ac tortor dapibus, imperdiet arcu in, aliquet est. Praesent ut risus tincidunt, euismod diam sit amet, vulputate dui. Cras luctus turpis non quam pr',
   'Pellentesque sollicitudin iaculis gravida. Praesent ac tortor dapibus, imperdiet arcu in, aliquet est. Praesent ut risus tincidunt, euismod diam sit amet, vulputate dui. Cras luctus turpis non quam pretium, quis ornare libero vulputate. Nullam felis felis, vestibulum ac placerat sit amet, pretium ut dolor.',
   'pellentesque-sollicitudin-iaculis-gravida');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_post_related`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_post_related` (
  `id_post`         INT(11) NOT NULL,
  `related_post_id` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id_post`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_smart_blog_post_related`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_post_shop`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_post_shop` (
  `id_smart_blog_post_shop` INT(11) NOT NULL AUTO_INCREMENT,
  `id_smart_blog_post`      INT(11) NOT NULL,
  `id_shop`                 INT(11) NOT NULL,
  PRIMARY KEY (`id_smart_blog_post_shop`, `id_smart_blog_post`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 5;

--
-- Vypisuji data pro tabulku `ps_smart_blog_post_shop`
--

INSERT INTO `ps_smart_blog_post_shop` (`id_smart_blog_post_shop`, `id_smart_blog_post`, `id_shop`) VALUES
  (1, 1, 1),
  (2, 2, 1),
  (3, 3, 1),
  (4, 4, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_post_tag`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_post_tag` (
  `id_tag`  INT(11) NOT NULL,
  `id_post` INT(11) DEFAULT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_smart_blog_post_tag`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_product_related`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_product_related` (
  `id_post`    INT(11) NOT NULL,
  `id_product` INT(11) DEFAULT NULL,
  `position`   INT(11) DEFAULT NULL,
  PRIMARY KEY (`id_post`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_smart_blog_product_related`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_smart_blog_tag`
--

CREATE TABLE IF NOT EXISTS `ps_smart_blog_tag` (
  `id_tag`  INT(11) NOT NULL AUTO_INCREMENT,
  `id_lang` INT(11)          DEFAULT NULL,
  `name`    VARCHAR(45)      DEFAULT NULL,
  PRIMARY KEY (`id_tag`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_smart_blog_tag`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_specific_price`
--

CREATE TABLE IF NOT EXISTS `ps_specific_price` (
  `id_specific_price`      INT(10) UNSIGNED              NOT NULL AUTO_INCREMENT,
  `id_specific_price_rule` INT(11) UNSIGNED              NOT NULL,
  `id_cart`                INT(11) UNSIGNED              NOT NULL,
  `id_product`             INT(10) UNSIGNED              NOT NULL,
  `id_shop`                INT(11) UNSIGNED              NOT NULL DEFAULT '1',
  `id_shop_group`          INT(11) UNSIGNED              NOT NULL,
  `id_currency`            INT(10) UNSIGNED              NOT NULL,
  `id_country`             INT(10) UNSIGNED              NOT NULL,
  `id_group`               INT(10) UNSIGNED              NOT NULL,
  `id_customer`            INT(10) UNSIGNED              NOT NULL,
  `id_product_attribute`   INT(10) UNSIGNED              NOT NULL,
  `price`                  DECIMAL(20, 6)                NOT NULL,
  `from_quantity`          MEDIUMINT(8) UNSIGNED         NOT NULL,
  `reduction`              DECIMAL(20, 6)                NOT NULL,
  `reduction_tax`          TINYINT(1)                    NOT NULL DEFAULT '1',
  `reduction_type`         ENUM ('amount', 'percentage') NOT NULL,
  `from`                   DATETIME                      NOT NULL,
  `to`                     DATETIME                      NOT NULL,
  PRIMARY KEY (`id_specific_price`),
  UNIQUE KEY `id_product_2` (`id_product`, `id_product_attribute`, `id_customer`, `id_cart`, `from`, `to`, `id_shop`, `id_shop_group`, `id_currency`, `id_country`, `id_group`, `from_quantity`, `id_specific_price_rule`),
  KEY `id_product` (`id_product`, `id_shop`, `id_currency`, `id_country`, `id_group`, `id_customer`, `from_quantity`, `from`, `to`),
  KEY `from_quantity` (`from_quantity`),
  KEY `id_specific_price_rule` (`id_specific_price_rule`),
  KEY `id_cart` (`id_cart`),
  KEY `id_product_attribute` (`id_product_attribute`),
  KEY `id_shop` (`id_shop`),
  KEY `id_customer` (`id_customer`),
  KEY `from` (`from`),
  KEY `to` (`to`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 3;

--
-- Vypisuji data pro tabulku `ps_specific_price`
--

INSERT INTO `ps_specific_price` (`id_specific_price`, `id_specific_price_rule`, `id_cart`, `id_product`, `id_shop`, `id_shop_group`, `id_currency`, `id_country`, `id_group`, `id_customer`, `id_product_attribute`, `price`, `from_quantity`, `reduction`, `reduction_tax`, `reduction_type`, `from`, `to`)
VALUES
  (1, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, -1.000000, 1, 0.050000, 1, 'percentage', '0000-00-00 00:00:00',
   '0000-00-00 00:00:00'),
  (2, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, -1.000000, 1, 0.200000, 1, 'percentage', '0000-00-00 00:00:00',
   '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_specific_price_priority`
--

CREATE TABLE IF NOT EXISTS `ps_specific_price_priority` (
  `id_specific_price_priority` INT(11)     NOT NULL AUTO_INCREMENT,
  `id_product`                 INT(11)     NOT NULL,
  `priority`                   VARCHAR(80) NOT NULL,
  PRIMARY KEY (`id_specific_price_priority`, `id_product`),
  UNIQUE KEY `id_product` (`id_product`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 5;

--
-- Vypisuji data pro tabulku `ps_specific_price_priority`
--

INSERT INTO `ps_specific_price_priority` (`id_specific_price_priority`, `id_product`, `priority`) VALUES
  (1, 2, 'id_shop;id_currency;id_country;id_group'),
  (2, 1, 'id_shop;id_currency;id_country;id_group'),
  (3, 7, 'id_shop;id_currency;id_country;id_group'),
  (4, 3, 'id_shop;id_currency;id_country;id_group');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_specific_price_rule`
--

CREATE TABLE IF NOT EXISTS `ps_specific_price_rule` (
  `id_specific_price_rule` INT(10) UNSIGNED              NOT NULL AUTO_INCREMENT,
  `name`                   VARCHAR(255)                  NOT NULL,
  `id_shop`                INT(11) UNSIGNED              NOT NULL DEFAULT '1',
  `id_currency`            INT(10) UNSIGNED              NOT NULL,
  `id_country`             INT(10) UNSIGNED              NOT NULL,
  `id_group`               INT(10) UNSIGNED              NOT NULL,
  `from_quantity`          MEDIUMINT(8) UNSIGNED         NOT NULL,
  `price`                  DECIMAL(20, 6)                         DEFAULT NULL,
  `reduction`              DECIMAL(20, 6)                NOT NULL,
  `reduction_tax`          TINYINT(1)                    NOT NULL DEFAULT '1',
  `reduction_type`         ENUM ('amount', 'percentage') NOT NULL,
  `from`                   DATETIME                      NOT NULL,
  `to`                     DATETIME                      NOT NULL,
  PRIMARY KEY (`id_specific_price_rule`),
  KEY `id_product` (`id_shop`, `id_currency`, `id_country`, `id_group`, `from_quantity`, `from`, `to`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_specific_price_rule`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_specific_price_rule_condition`
--

CREATE TABLE IF NOT EXISTS `ps_specific_price_rule_condition` (
  `id_specific_price_rule_condition`       INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_specific_price_rule_condition_group` INT(11) UNSIGNED NOT NULL,
  `type`                                   VARCHAR(255)     NOT NULL,
  `value`                                  VARCHAR(255)     NOT NULL,
  PRIMARY KEY (`id_specific_price_rule_condition`),
  KEY `id_specific_price_rule_condition_group` (`id_specific_price_rule_condition_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_specific_price_rule_condition`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_specific_price_rule_condition_group`
--

CREATE TABLE IF NOT EXISTS `ps_specific_price_rule_condition_group` (
  `id_specific_price_rule_condition_group` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_specific_price_rule`                 INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_specific_price_rule_condition_group`, `id_specific_price_rule`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_specific_price_rule_condition_group`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_state`
--

CREATE TABLE IF NOT EXISTS `ps_state` (
  `id_state`     INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_country`   INT(11) UNSIGNED NOT NULL,
  `id_zone`      INT(11) UNSIGNED NOT NULL,
  `name`         VARCHAR(64)      NOT NULL,
  `iso_code`     VARCHAR(7)       NOT NULL,
  `tax_behavior` SMALLINT(1)      NOT NULL DEFAULT '0',
  `active`       TINYINT(1)       NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_state`),
  KEY `id_country` (`id_country`),
  KEY `name` (`name`),
  KEY `id_zone` (`id_zone`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 313;

--
-- Vypisuji data pro tabulku `ps_state`
--

INSERT INTO `ps_state` (`id_state`, `id_country`, `id_zone`, `name`, `iso_code`, `tax_behavior`, `active`) VALUES
  (1, 21, 2, 'Alabama', 'AL', 0, 1),
  (2, 21, 2, 'Alaska', 'AK', 0, 1),
  (3, 21, 2, 'Arizona', 'AZ', 0, 1),
  (4, 21, 2, 'Arkansas', 'AR', 0, 1),
  (5, 21, 2, 'California', 'CA', 0, 1),
  (6, 21, 2, 'Colorado', 'CO', 0, 1),
  (7, 21, 2, 'Connecticut', 'CT', 0, 1),
  (8, 21, 2, 'Delaware', 'DE', 0, 1),
  (9, 21, 2, 'Florida', 'FL', 0, 1),
  (10, 21, 2, 'Georgia', 'GA', 0, 1),
  (11, 21, 2, 'Hawaii', 'HI', 0, 1),
  (12, 21, 2, 'Idaho', 'ID', 0, 1),
  (13, 21, 2, 'Illinois', 'IL', 0, 1),
  (14, 21, 2, 'Indiana', 'IN', 0, 1),
  (15, 21, 2, 'Iowa', 'IA', 0, 1),
  (16, 21, 2, 'Kansas', 'KS', 0, 1),
  (17, 21, 2, 'Kentucky', 'KY', 0, 1),
  (18, 21, 2, 'Louisiana', 'LA', 0, 1),
  (19, 21, 2, 'Maine', 'ME', 0, 1),
  (20, 21, 2, 'Maryland', 'MD', 0, 1),
  (21, 21, 2, 'Massachusetts', 'MA', 0, 1),
  (22, 21, 2, 'Michigan', 'MI', 0, 1),
  (23, 21, 2, 'Minnesota', 'MN', 0, 1),
  (24, 21, 2, 'Mississippi', 'MS', 0, 1),
  (25, 21, 2, 'Missouri', 'MO', 0, 1),
  (26, 21, 2, 'Montana', 'MT', 0, 1),
  (27, 21, 2, 'Nebraska', 'NE', 0, 1),
  (28, 21, 2, 'Nevada', 'NV', 0, 1),
  (29, 21, 2, 'New Hampshire', 'NH', 0, 1),
  (30, 21, 2, 'New Jersey', 'NJ', 0, 1),
  (31, 21, 2, 'New Mexico', 'NM', 0, 1),
  (32, 21, 2, 'New York', 'NY', 0, 1),
  (33, 21, 2, 'North Carolina', 'NC', 0, 1),
  (34, 21, 2, 'North Dakota', 'ND', 0, 1),
  (35, 21, 2, 'Ohio', 'OH', 0, 1),
  (36, 21, 2, 'Oklahoma', 'OK', 0, 1),
  (37, 21, 2, 'Oregon', 'OR', 0, 1),
  (38, 21, 2, 'Pennsylvania', 'PA', 0, 1),
  (39, 21, 2, 'Rhode Island', 'RI', 0, 1),
  (40, 21, 2, 'South Carolina', 'SC', 0, 1),
  (41, 21, 2, 'South Dakota', 'SD', 0, 1),
  (42, 21, 2, 'Tennessee', 'TN', 0, 1),
  (43, 21, 2, 'Texas', 'TX', 0, 1),
  (44, 21, 2, 'Utah', 'UT', 0, 1),
  (45, 21, 2, 'Vermont', 'VT', 0, 1),
  (46, 21, 2, 'Virginia', 'VA', 0, 1),
  (47, 21, 2, 'Washington', 'WA', 0, 1),
  (48, 21, 2, 'West Virginia', 'WV', 0, 1),
  (49, 21, 2, 'Wisconsin', 'WI', 0, 1),
  (50, 21, 2, 'Wyoming', 'WY', 0, 1),
  (51, 21, 2, 'Puerto Rico', 'PR', 0, 1),
  (52, 21, 2, 'US Virgin Islands', 'VI', 0, 1),
  (53, 21, 2, 'District of Columbia', 'DC', 0, 1),
  (54, 145, 2, 'Aguascalientes', 'AGS', 0, 1),
  (55, 145, 2, 'Baja California', 'BCN', 0, 1),
  (56, 145, 2, 'Baja California Sur', 'BCS', 0, 1),
  (57, 145, 2, 'Campeche', 'CAM', 0, 1),
  (58, 145, 2, 'Chiapas', 'CHP', 0, 1),
  (59, 145, 2, 'Chihuahua', 'CHH', 0, 1),
  (60, 145, 2, 'Coahuila', 'COA', 0, 1),
  (61, 145, 2, 'Colima', 'COL', 0, 1),
  (62, 145, 2, 'Distrito Federal', 'DIF', 0, 1),
  (63, 145, 2, 'Durango', 'DUR', 0, 1),
  (64, 145, 2, 'Guanajuato', 'GUA', 0, 1),
  (65, 145, 2, 'Guerrero', 'GRO', 0, 1),
  (66, 145, 2, 'Hidalgo', 'HID', 0, 1),
  (67, 145, 2, 'Jalisco', 'JAL', 0, 1),
  (68, 145, 2, 'Estado de México', 'MEX', 0, 1),
  (69, 145, 2, 'Michoacán', 'MIC', 0, 1),
  (70, 145, 2, 'Morelos', 'MOR', 0, 1),
  (71, 145, 2, 'Nayarit', 'NAY', 0, 1),
  (72, 145, 2, 'Nuevo León', 'NLE', 0, 1),
  (73, 145, 2, 'Oaxaca', 'OAX', 0, 1),
  (74, 145, 2, 'Puebla', 'PUE', 0, 1),
  (75, 145, 2, 'Querétaro', 'QUE', 0, 1),
  (76, 145, 2, 'Quintana Roo', 'ROO', 0, 1),
  (77, 145, 2, 'San Luis Potosí', 'SLP', 0, 1),
  (78, 145, 2, 'Sinaloa', 'SIN', 0, 1),
  (79, 145, 2, 'Sonora', 'SON', 0, 1),
  (80, 145, 2, 'Tabasco', 'TAB', 0, 1),
  (81, 145, 2, 'Tamaulipas', 'TAM', 0, 1),
  (82, 145, 2, 'Tlaxcala', 'TLA', 0, 1),
  (83, 145, 2, 'Veracruz', 'VER', 0, 1),
  (84, 145, 2, 'Yucatán', 'YUC', 0, 1),
  (85, 145, 2, 'Zacatecas', 'ZAC', 0, 1),
  (86, 4, 2, 'Ontario', 'ON', 0, 1),
  (87, 4, 2, 'Quebec', 'QC', 0, 1),
  (88, 4, 2, 'British Columbia', 'BC', 0, 1),
  (89, 4, 2, 'Alberta', 'AB', 0, 1),
  (90, 4, 2, 'Manitoba', 'MB', 0, 1),
  (91, 4, 2, 'Saskatchewan', 'SK', 0, 1),
  (92, 4, 2, 'Nova Scotia', 'NS', 0, 1),
  (93, 4, 2, 'New Brunswick', 'NB', 0, 1),
  (94, 4, 2, 'Newfoundland and Labrador', 'NL', 0, 1),
  (95, 4, 2, 'Prince Edward Island', 'PE', 0, 1),
  (96, 4, 2, 'Northwest Territories', 'NT', 0, 1),
  (97, 4, 2, 'Yukon', 'YT', 0, 1),
  (98, 4, 2, 'Nunavut', 'NU', 0, 1),
  (99, 44, 6, 'Buenos Aires', 'B', 0, 1),
  (100, 44, 6, 'Catamarca', 'K', 0, 1),
  (101, 44, 6, 'Chaco', 'H', 0, 1),
  (102, 44, 6, 'Chubut', 'U', 0, 1),
  (103, 44, 6, 'Ciudad de Buenos Aires', 'C', 0, 1),
  (104, 44, 6, 'Córdoba', 'X', 0, 1),
  (105, 44, 6, 'Corrientes', 'W', 0, 1),
  (106, 44, 6, 'Entre Ríos', 'E', 0, 1),
  (107, 44, 6, 'Formosa', 'P', 0, 1),
  (108, 44, 6, 'Jujuy', 'Y', 0, 1),
  (109, 44, 6, 'La Pampa', 'L', 0, 1),
  (110, 44, 6, 'La Rioja', 'F', 0, 1),
  (111, 44, 6, 'Mendoza', 'M', 0, 1),
  (112, 44, 6, 'Misiones', 'N', 0, 1),
  (113, 44, 6, 'Neuquén', 'Q', 0, 1),
  (114, 44, 6, 'Río Negro', 'R', 0, 1),
  (115, 44, 6, 'Salta', 'A', 0, 1),
  (116, 44, 6, 'San Juan', 'J', 0, 1),
  (117, 44, 6, 'San Luis', 'D', 0, 1),
  (118, 44, 6, 'Santa Cruz', 'Z', 0, 1),
  (119, 44, 6, 'Santa Fe', 'S', 0, 1),
  (120, 44, 6, 'Santiago del Estero', 'G', 0, 1),
  (121, 44, 6, 'Tierra del Fuego', 'V', 0, 1),
  (122, 44, 6, 'Tucumán', 'T', 0, 1),
  (123, 10, 1, 'Agrigento', 'AG', 0, 1),
  (124, 10, 1, 'Alessandria', 'AL', 0, 1),
  (125, 10, 1, 'Ancona', 'AN', 0, 1),
  (126, 10, 1, 'Aosta', 'AO', 0, 1),
  (127, 10, 1, 'Arezzo', 'AR', 0, 1),
  (128, 10, 1, 'Ascoli Piceno', 'AP', 0, 1),
  (129, 10, 1, 'Asti', 'AT', 0, 1),
  (130, 10, 1, 'Avellino', 'AV', 0, 1),
  (131, 10, 1, 'Bari', 'BA', 0, 1),
  (132, 10, 1, 'Barletta-Andria-Trani', 'BT', 0, 1),
  (133, 10, 1, 'Belluno', 'BL', 0, 1),
  (134, 10, 1, 'Benevento', 'BN', 0, 1),
  (135, 10, 1, 'Bergamo', 'BG', 0, 1),
  (136, 10, 1, 'Biella', 'BI', 0, 1),
  (137, 10, 1, 'Bologna', 'BO', 0, 1),
  (138, 10, 1, 'Bolzano', 'BZ', 0, 1),
  (139, 10, 1, 'Brescia', 'BS', 0, 1),
  (140, 10, 1, 'Brindisi', 'BR', 0, 1),
  (141, 10, 1, 'Cagliari', 'CA', 0, 1),
  (142, 10, 1, 'Caltanissetta', 'CL', 0, 1),
  (143, 10, 1, 'Campobasso', 'CB', 0, 1),
  (144, 10, 1, 'Carbonia-Iglesias', 'CI', 0, 1),
  (145, 10, 1, 'Caserta', 'CE', 0, 1),
  (146, 10, 1, 'Catania', 'CT', 0, 1),
  (147, 10, 1, 'Catanzaro', 'CZ', 0, 1),
  (148, 10, 1, 'Chieti', 'CH', 0, 1),
  (149, 10, 1, 'Como', 'CO', 0, 1),
  (150, 10, 1, 'Cosenza', 'CS', 0, 1),
  (151, 10, 1, 'Cremona', 'CR', 0, 1),
  (152, 10, 1, 'Crotone', 'KR', 0, 1),
  (153, 10, 1, 'Cuneo', 'CN', 0, 1),
  (154, 10, 1, 'Enna', 'EN', 0, 1),
  (155, 10, 1, 'Fermo', 'FM', 0, 1),
  (156, 10, 1, 'Ferrara', 'FE', 0, 1),
  (157, 10, 1, 'Firenze', 'FI', 0, 1),
  (158, 10, 1, 'Foggia', 'FG', 0, 1),
  (159, 10, 1, 'Forlì-Cesena', 'FC', 0, 1),
  (160, 10, 1, 'Frosinone', 'FR', 0, 1),
  (161, 10, 1, 'Genova', 'GE', 0, 1),
  (162, 10, 1, 'Gorizia', 'GO', 0, 1),
  (163, 10, 1, 'Grosseto', 'GR', 0, 1),
  (164, 10, 1, 'Imperia', 'IM', 0, 1),
  (165, 10, 1, 'Isernia', 'IS', 0, 1),
  (166, 10, 1, 'L''Aquila', 'AQ', 0, 1),
  (167, 10, 1, 'La Spezia', 'SP', 0, 1),
  (168, 10, 1, 'Latina', 'LT', 0, 1),
  (169, 10, 1, 'Lecce', 'LE', 0, 1),
  (170, 10, 1, 'Lecco', 'LC', 0, 1),
  (171, 10, 1, 'Livorno', 'LI', 0, 1),
  (172, 10, 1, 'Lodi', 'LO', 0, 1),
  (173, 10, 1, 'Lucca', 'LU', 0, 1),
  (174, 10, 1, 'Macerata', 'MC', 0, 1),
  (175, 10, 1, 'Mantova', 'MN', 0, 1),
  (176, 10, 1, 'Massa', 'MS', 0, 1),
  (177, 10, 1, 'Matera', 'MT', 0, 1),
  (178, 10, 1, 'Medio Campidano', 'VS', 0, 1),
  (179, 10, 1, 'Messina', 'ME', 0, 1),
  (180, 10, 1, 'Milano', 'MI', 0, 1),
  (181, 10, 1, 'Modena', 'MO', 0, 1),
  (182, 10, 1, 'Monza e della Brianza', 'MB', 0, 1),
  (183, 10, 1, 'Napoli', 'NA', 0, 1),
  (184, 10, 1, 'Novara', 'NO', 0, 1),
  (185, 10, 1, 'Nuoro', 'NU', 0, 1),
  (186, 10, 1, 'Ogliastra', 'OG', 0, 1),
  (187, 10, 1, 'Olbia-Tempio', 'OT', 0, 1),
  (188, 10, 1, 'Oristano', 'OR', 0, 1),
  (189, 10, 1, 'Padova', 'PD', 0, 1),
  (190, 10, 1, 'Palermo', 'PA', 0, 1),
  (191, 10, 1, 'Parma', 'PR', 0, 1),
  (192, 10, 1, 'Pavia', 'PV', 0, 1),
  (193, 10, 1, 'Perugia', 'PG', 0, 1),
  (194, 10, 1, 'Pesaro-Urbino', 'PU', 0, 1),
  (195, 10, 1, 'Pescara', 'PE', 0, 1),
  (196, 10, 1, 'Piacenza', 'PC', 0, 1),
  (197, 10, 1, 'Pisa', 'PI', 0, 1),
  (198, 10, 1, 'Pistoia', 'PT', 0, 1),
  (199, 10, 1, 'Pordenone', 'PN', 0, 1),
  (200, 10, 1, 'Potenza', 'PZ', 0, 1),
  (201, 10, 1, 'Prato', 'PO', 0, 1),
  (202, 10, 1, 'Ragusa', 'RG', 0, 1),
  (203, 10, 1, 'Ravenna', 'RA', 0, 1),
  (204, 10, 1, 'Reggio Calabria', 'RC', 0, 1),
  (205, 10, 1, 'Reggio Emilia', 'RE', 0, 1),
  (206, 10, 1, 'Rieti', 'RI', 0, 1),
  (207, 10, 1, 'Rimini', 'RN', 0, 1),
  (208, 10, 1, 'Roma', 'RM', 0, 1),
  (209, 10, 1, 'Rovigo', 'RO', 0, 1),
  (210, 10, 1, 'Salerno', 'SA', 0, 1),
  (211, 10, 1, 'Sassari', 'SS', 0, 1),
  (212, 10, 1, 'Savona', 'SV', 0, 1),
  (213, 10, 1, 'Siena', 'SI', 0, 1),
  (214, 10, 1, 'Siracusa', 'SR', 0, 1),
  (215, 10, 1, 'Sondrio', 'SO', 0, 1),
  (216, 10, 1, 'Taranto', 'TA', 0, 1),
  (217, 10, 1, 'Teramo', 'TE', 0, 1),
  (218, 10, 1, 'Terni', 'TR', 0, 1),
  (219, 10, 1, 'Torino', 'TO', 0, 1),
  (220, 10, 1, 'Trapani', 'TP', 0, 1),
  (221, 10, 1, 'Trento', 'TN', 0, 1),
  (222, 10, 1, 'Treviso', 'TV', 0, 1),
  (223, 10, 1, 'Trieste', 'TS', 0, 1),
  (224, 10, 1, 'Udine', 'UD', 0, 1),
  (225, 10, 1, 'Varese', 'VA', 0, 1),
  (226, 10, 1, 'Venezia', 'VE', 0, 1),
  (227, 10, 1, 'Verbano-Cusio-Ossola', 'VB', 0, 1),
  (228, 10, 1, 'Vercelli', 'VC', 0, 1),
  (229, 10, 1, 'Verona', 'VR', 0, 1),
  (230, 10, 1, 'Vibo Valentia', 'VV', 0, 1),
  (231, 10, 1, 'Vicenza', 'VI', 0, 1),
  (232, 10, 1, 'Viterbo', 'VT', 0, 1),
  (233, 111, 3, 'Aceh', 'AC', 0, 1),
  (234, 111, 3, 'Bali', 'BA', 0, 1),
  (235, 111, 3, 'Bangka', 'BB', 0, 1),
  (236, 111, 3, 'Banten', 'BT', 0, 1),
  (237, 111, 3, 'Bengkulu', 'BE', 0, 1),
  (238, 111, 3, 'Central Java', 'JT', 0, 1),
  (239, 111, 3, 'Central Kalimantan', 'KT', 0, 1),
  (240, 111, 3, 'Central Sulawesi', 'ST', 0, 1),
  (241, 111, 3, 'Coat of arms of East Java', 'JI', 0, 1),
  (242, 111, 3, 'East kalimantan', 'KI', 0, 1),
  (243, 111, 3, 'East Nusa Tenggara', 'NT', 0, 1),
  (244, 111, 3, 'Lambang propinsi', 'GO', 0, 1),
  (245, 111, 3, 'Jakarta', 'JK', 0, 1),
  (246, 111, 3, 'Jambi', 'JA', 0, 1),
  (247, 111, 3, 'Lampung', 'LA', 0, 1),
  (248, 111, 3, 'Maluku', 'MA', 0, 1),
  (249, 111, 3, 'North Maluku', 'MU', 0, 1),
  (250, 111, 3, 'North Sulawesi', 'SA', 0, 1),
  (251, 111, 3, 'North Sumatra', 'SU', 0, 1),
  (252, 111, 3, 'Papua', 'PA', 0, 1),
  (253, 111, 3, 'Riau', 'RI', 0, 1),
  (254, 111, 3, 'Lambang Riau', 'KR', 0, 1),
  (255, 111, 3, 'Southeast Sulawesi', 'SG', 0, 1),
  (256, 111, 3, 'South Kalimantan', 'KS', 0, 1),
  (257, 111, 3, 'South Sulawesi', 'SN', 0, 1),
  (258, 111, 3, 'South Sumatra', 'SS', 0, 1),
  (259, 111, 3, 'West Java', 'JB', 0, 1),
  (260, 111, 3, 'West Kalimantan', 'KB', 0, 1),
  (261, 111, 3, 'West Nusa Tenggara', 'NB', 0, 1),
  (262, 111, 3, 'Lambang Provinsi Papua Barat', 'PB', 0, 1),
  (263, 111, 3, 'West Sulawesi', 'SR', 0, 1),
  (264, 111, 3, 'West Sumatra', 'SB', 0, 1),
  (265, 111, 3, 'Yogyakarta', 'YO', 0, 1),
  (266, 11, 3, 'Aichi', '23', 0, 1),
  (267, 11, 3, 'Akita', '05', 0, 1),
  (268, 11, 3, 'Aomori', '02', 0, 1),
  (269, 11, 3, 'Chiba', '12', 0, 1),
  (270, 11, 3, 'Ehime', '38', 0, 1),
  (271, 11, 3, 'Fukui', '18', 0, 1),
  (272, 11, 3, 'Fukuoka', '40', 0, 1),
  (273, 11, 3, 'Fukushima', '07', 0, 1),
  (274, 11, 3, 'Gifu', '21', 0, 1),
  (275, 11, 3, 'Gunma', '10', 0, 1),
  (276, 11, 3, 'Hiroshima', '34', 0, 1),
  (277, 11, 3, 'Hokkaido', '01', 0, 1),
  (278, 11, 3, 'Hyogo', '28', 0, 1),
  (279, 11, 3, 'Ibaraki', '08', 0, 1),
  (280, 11, 3, 'Ishikawa', '17', 0, 1),
  (281, 11, 3, 'Iwate', '03', 0, 1),
  (282, 11, 3, 'Kagawa', '37', 0, 1),
  (283, 11, 3, 'Kagoshima', '46', 0, 1),
  (284, 11, 3, 'Kanagawa', '14', 0, 1),
  (285, 11, 3, 'Kochi', '39', 0, 1),
  (286, 11, 3, 'Kumamoto', '43', 0, 1),
  (287, 11, 3, 'Kyoto', '26', 0, 1),
  (288, 11, 3, 'Mie', '24', 0, 1),
  (289, 11, 3, 'Miyagi', '04', 0, 1),
  (290, 11, 3, 'Miyazaki', '45', 0, 1),
  (291, 11, 3, 'Nagano', '20', 0, 1),
  (292, 11, 3, 'Nagasaki', '42', 0, 1),
  (293, 11, 3, 'Nara', '29', 0, 1),
  (294, 11, 3, 'Niigata', '15', 0, 1),
  (295, 11, 3, 'Oita', '44', 0, 1),
  (296, 11, 3, 'Okayama', '33', 0, 1),
  (297, 11, 3, 'Okinawa', '47', 0, 1),
  (298, 11, 3, 'Osaka', '27', 0, 1),
  (299, 11, 3, 'Saga', '41', 0, 1),
  (300, 11, 3, 'Saitama', '11', 0, 1),
  (301, 11, 3, 'Shiga', '25', 0, 1),
  (302, 11, 3, 'Shimane', '32', 0, 1),
  (303, 11, 3, 'Shizuoka', '22', 0, 1),
  (304, 11, 3, 'Tochigi', '09', 0, 1),
  (305, 11, 3, 'Tokushima', '36', 0, 1),
  (306, 11, 3, 'Tokyo', '13', 0, 1),
  (307, 11, 3, 'Tottori', '31', 0, 1),
  (308, 11, 3, 'Toyama', '16', 0, 1),
  (309, 11, 3, 'Wakayama', '30', 0, 1),
  (310, 11, 3, 'Yamagata', '06', 0, 1),
  (311, 11, 3, 'Yamaguchi', '35', 0, 1),
  (312, 11, 3, 'Yamanashi', '19', 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_statssearch`
--

CREATE TABLE IF NOT EXISTS `ps_statssearch` (
  `id_statssearch` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_shop`        INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group`  INT(10) UNSIGNED NOT NULL DEFAULT '1',
  `keywords`       VARCHAR(255)     NOT NULL,
  `results`        INT(6)           NOT NULL DEFAULT '0',
  `date_add`       DATETIME         NOT NULL,
  PRIMARY KEY (`id_statssearch`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_statssearch`
--

INSERT INTO `ps_statssearch` (`id_statssearch`, `id_shop`, `id_shop_group`, `keywords`, `results`, `date_add`) VALUES
  (1, 1, 1, 'silhani', 0, '2017-01-25 15:04:32');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_stock`
--

CREATE TABLE IF NOT EXISTS `ps_stock` (
  `id_stock`             INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_warehouse`         INT(11) UNSIGNED NOT NULL,
  `id_product`           INT(11) UNSIGNED NOT NULL,
  `id_product_attribute` INT(11) UNSIGNED NOT NULL,
  `reference`            VARCHAR(32)      NOT NULL,
  `ean13`                VARCHAR(13)               DEFAULT NULL,
  `upc`                  VARCHAR(12)               DEFAULT NULL,
  `physical_quantity`    INT(11) UNSIGNED NOT NULL,
  `usable_quantity`      INT(11) UNSIGNED NOT NULL,
  `price_te`             DECIMAL(20, 6)            DEFAULT '0.000000',
  PRIMARY KEY (`id_stock`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `id_product` (`id_product`),
  KEY `id_product_attribute` (`id_product_attribute`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_stock`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_stock_available`
--

CREATE TABLE IF NOT EXISTS `ps_stock_available` (
  `id_stock_available`   INT(11) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_product`           INT(11) UNSIGNED    NOT NULL,
  `id_product_attribute` INT(11) UNSIGNED    NOT NULL,
  `id_shop`              INT(11) UNSIGNED    NOT NULL,
  `id_shop_group`        INT(11) UNSIGNED    NOT NULL,
  `quantity`             INT(10)             NOT NULL DEFAULT '0',
  `depends_on_stock`     TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `out_of_stock`         TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_stock_available`),
  UNIQUE KEY `product_sqlstock` (`id_product`, `id_product_attribute`, `id_shop`, `id_shop_group`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `id_product` (`id_product`),
  KEY `id_product_attribute` (`id_product_attribute`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 54;

--
-- Vypisuji data pro tabulku `ps_stock_available`
--

INSERT INTO `ps_stock_available` (`id_stock_available`, `id_product`, `id_product_attribute`, `id_shop`, `id_shop_group`, `quantity`, `depends_on_stock`, `out_of_stock`)
VALUES
  (1, 1, 0, 1, 0, 1799, 0, 2),
  (3, 3, 0, 1, 0, 899, 0, 2),
  (4, 4, 0, 1, 0, 900, 0, 2),
  (5, 5, 0, 1, 0, 3600, 0, 2),
  (6, 6, 0, 1, 0, 900, 0, 2),
  (7, 7, 0, 1, 0, 1800, 0, 2),
  (8, 1, 1, 1, 0, 299, 0, 2),
  (9, 1, 2, 1, 0, 300, 0, 2),
  (10, 1, 3, 1, 0, 300, 0, 2),
  (11, 1, 4, 1, 0, 300, 0, 2),
  (12, 1, 5, 1, 0, 300, 0, 2),
  (13, 1, 6, 1, 0, 300, 0, 2),
  (20, 3, 13, 1, 0, 299, 0, 2),
  (21, 3, 14, 1, 0, 300, 0, 2),
  (22, 3, 15, 1, 0, 300, 0, 2),
  (23, 4, 16, 1, 0, 300, 0, 2),
  (24, 4, 17, 1, 0, 300, 0, 2),
  (25, 4, 18, 1, 0, 300, 0, 2),
  (26, 5, 19, 1, 0, 300, 0, 2),
  (27, 5, 20, 1, 0, 300, 0, 2),
  (28, 5, 21, 1, 0, 300, 0, 2),
  (29, 5, 22, 1, 0, 300, 0, 2),
  (30, 5, 23, 1, 0, 300, 0, 2),
  (31, 5, 24, 1, 0, 300, 0, 2),
  (32, 5, 25, 1, 0, 300, 0, 2),
  (33, 5, 26, 1, 0, 300, 0, 2),
  (34, 5, 27, 1, 0, 300, 0, 2),
  (35, 5, 28, 1, 0, 300, 0, 2),
  (36, 5, 29, 1, 0, 300, 0, 2),
  (37, 5, 30, 1, 0, 300, 0, 2),
  (38, 6, 31, 1, 0, 300, 0, 2),
  (39, 6, 32, 1, 0, 300, 0, 2),
  (40, 6, 33, 1, 0, 300, 0, 2),
  (41, 7, 34, 1, 0, 300, 0, 2),
  (42, 7, 35, 1, 0, 300, 0, 2),
  (43, 7, 36, 1, 0, 300, 0, 2),
  (44, 7, 37, 1, 0, 300, 0, 2),
  (45, 7, 38, 1, 0, 300, 0, 2),
  (46, 7, 39, 1, 0, 300, 0, 2),
  (47, 6, 40, 1, 0, 0, 0, 2),
  (48, 6, 41, 1, 0, 0, 0, 2),
  (49, 6, 42, 1, 0, 0, 0, 2),
  (50, 4, 43, 1, 0, 0, 0, 2),
  (51, 4, 44, 1, 0, 0, 0, 2),
  (52, 4, 45, 1, 0, 0, 0, 2),
  (53, 2, 0, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_stock_mvt`
--

CREATE TABLE IF NOT EXISTS `ps_stock_mvt` (
  `id_stock_mvt`        BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_stock`            INT(11) UNSIGNED    NOT NULL,
  `id_order`            INT(11) UNSIGNED             DEFAULT NULL,
  `id_supply_order`     INT(11) UNSIGNED             DEFAULT NULL,
  `id_stock_mvt_reason` INT(11) UNSIGNED    NOT NULL,
  `id_employee`         INT(11) UNSIGNED    NOT NULL,
  `employee_lastname`   VARCHAR(32)                  DEFAULT '',
  `employee_firstname`  VARCHAR(32)                  DEFAULT '',
  `physical_quantity`   INT(11) UNSIGNED    NOT NULL,
  `date_add`            DATETIME            NOT NULL,
  `sign`                TINYINT(1)          NOT NULL DEFAULT '1',
  `price_te`            DECIMAL(20, 6)               DEFAULT '0.000000',
  `last_wa`             DECIMAL(20, 6)               DEFAULT '0.000000',
  `current_wa`          DECIMAL(20, 6)               DEFAULT '0.000000',
  `referer`             BIGINT(20) UNSIGNED          DEFAULT NULL,
  PRIMARY KEY (`id_stock_mvt`),
  KEY `id_stock` (`id_stock`),
  KEY `id_stock_mvt_reason` (`id_stock_mvt_reason`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_stock_mvt`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_stock_mvt_reason`
--

CREATE TABLE IF NOT EXISTS `ps_stock_mvt_reason` (
  `id_stock_mvt_reason` INT(11) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `sign`                TINYINT(1)          NOT NULL DEFAULT '1',
  `date_add`            DATETIME            NOT NULL,
  `date_upd`            DATETIME            NOT NULL,
  `deleted`             TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_stock_mvt_reason`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 9;

--
-- Vypisuji data pro tabulku `ps_stock_mvt_reason`
--

INSERT INTO `ps_stock_mvt_reason` (`id_stock_mvt_reason`, `sign`, `date_add`, `date_upd`, `deleted`) VALUES
  (1, 1, '2016-08-04 12:42:25', '2016-08-04 12:42:25', 0),
  (2, -1, '2016-08-04 12:42:25', '2016-08-04 12:42:25', 0),
  (3, -1, '2016-08-04 12:42:25', '2016-08-04 12:42:25', 0),
  (4, -1, '2016-08-04 12:42:25', '2016-08-04 12:42:25', 0),
  (5, 1, '2016-08-04 12:42:25', '2016-08-04 12:42:25', 0),
  (6, -1, '2016-08-04 12:42:25', '2016-08-04 12:42:25', 0),
  (7, 1, '2016-08-04 12:42:25', '2016-08-04 12:42:25', 0),
  (8, 1, '2016-08-04 12:42:25', '2016-08-04 12:42:25', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_stock_mvt_reason_lang`
--

CREATE TABLE IF NOT EXISTS `ps_stock_mvt_reason_lang` (
  `id_stock_mvt_reason` INT(11) UNSIGNED NOT NULL,
  `id_lang`             INT(11) UNSIGNED NOT NULL,
  `name`                VARCHAR(255)     NOT NULL,
  PRIMARY KEY (`id_stock_mvt_reason`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_stock_mvt_reason_lang`
--

INSERT INTO `ps_stock_mvt_reason_lang` (`id_stock_mvt_reason`, `id_lang`, `name`) VALUES
  (1, 1, 'Zvýšení'),
  (2, 1, 'Snížení'),
  (3, 1, 'Customer Order'),
  (4, 1, 'Regulation following an inventory of stock'),
  (5, 1, 'Regulation following an inventory of stock'),
  (6, 1, 'Transfer to another warehouse'),
  (7, 1, 'Transfer from another warehouse'),
  (8, 1, 'Objednávky dodavatelům');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_store`
--

CREATE TABLE IF NOT EXISTS `ps_store` (
  `id_store`   INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_country` INT(10) UNSIGNED    NOT NULL,
  `id_state`   INT(10) UNSIGNED             DEFAULT NULL,
  `name`       VARCHAR(128)        NOT NULL,
  `address1`   VARCHAR(128)        NOT NULL,
  `address2`   VARCHAR(128)                 DEFAULT NULL,
  `city`       VARCHAR(64)         NOT NULL,
  `postcode`   VARCHAR(12)         NOT NULL,
  `latitude`   DECIMAL(13, 8)               DEFAULT NULL,
  `longitude`  DECIMAL(13, 8)               DEFAULT NULL,
  `hours`      VARCHAR(254)                 DEFAULT NULL,
  `phone`      VARCHAR(16)                  DEFAULT NULL,
  `fax`        VARCHAR(16)                  DEFAULT NULL,
  `email`      VARCHAR(128)                 DEFAULT NULL,
  `note`       TEXT,
  `active`     TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add`   DATETIME            NOT NULL,
  `date_upd`   DATETIME            NOT NULL,
  PRIMARY KEY (`id_store`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 6;

--
-- Vypisuji data pro tabulku `ps_store`
--

INSERT INTO `ps_store` (`id_store`, `id_country`, `id_state`, `name`, `address1`, `address2`, `city`, `postcode`, `latitude`, `longitude`, `hours`, `phone`, `fax`, `email`, `note`, `active`, `date_add`, `date_upd`)
VALUES
  (1, 16, 0, 'Oční ordinace Hradec Králové', 'Švehlova 504', '', 'Hradec Králové', '50002', 50.21135720, 15.82583440,
      'a:7:{i:0;s:13:"09:00 - 19:00";i:1;s:13:"09:00 - 19:00";i:2;s:13:"09:00 - 19:00";i:3;s:13:"09:00 - 19:00";i:4;s:13:"09:00 - 19:00";i:5;s:13:"10:00 - 16:00";i:6;s:13:"10:00 - 16:00";}',
   '608 800 886', '', 'info@ocni-centrum.eu', '', 1, '2016-08-04 12:42:37', '2017-01-23 17:26:28'),
  (2, 21, 9, 'E Fort Lauderdale', '1000 Northeast 4th Ave Fort Lauderdale', '', 'Miami', ' 33304', 26.13793600,
      -80.13943500,
      'a:7:{i:0;s:13:"09:00 - 19:00";i:1;s:13:"09:00 - 19:00";i:2;s:13:"09:00 - 19:00";i:3;s:13:"09:00 - 19:00";i:4;s:13:"09:00 - 19:00";i:5;s:13:"10:00 - 16:00";i:6;s:13:"10:00 - 16:00";}',
   '', '', '', '', 1, '2016-08-04 12:42:37', '2016-08-04 12:42:37'),
  (3, 21, 9, 'Pembroke Pines', '11001 Pines Blvd Pembroke Pines', '', 'Miami', '33026', 26.00998700, -80.29447200,
      'a:7:{i:0;s:13:"09:00 - 19:00";i:1;s:13:"09:00 - 19:00";i:2;s:13:"09:00 - 19:00";i:3;s:13:"09:00 - 19:00";i:4;s:13:"09:00 - 19:00";i:5;s:13:"10:00 - 16:00";i:6;s:13:"10:00 - 16:00";}',
   '', '', '', '', 1, '2016-08-04 12:42:37', '2016-08-04 12:42:37'),
  (4, 21, 9, 'Coconut Grove', '2999 SW 32nd Avenue', '', 'Miami', ' 33133', 25.73629600, -80.24479700,
      'a:7:{i:0;s:13:"09:00 - 19:00";i:1;s:13:"09:00 - 19:00";i:2;s:13:"09:00 - 19:00";i:3;s:13:"09:00 - 19:00";i:4;s:13:"09:00 - 19:00";i:5;s:13:"10:00 - 16:00";i:6;s:13:"10:00 - 16:00";}',
   '', '', '', '', 1, '2016-08-04 12:42:37', '2016-08-04 12:42:37'),
  (5, 21, 9, 'N Miami/Biscayne', '12055 Biscayne Blvd', '', 'Miami', '33181', 25.88674000, -80.16329200,
      'a:7:{i:0;s:13:"09:00 - 19:00";i:1;s:13:"09:00 - 19:00";i:2;s:13:"09:00 - 19:00";i:3;s:13:"09:00 - 19:00";i:4;s:13:"09:00 - 19:00";i:5;s:13:"10:00 - 16:00";i:6;s:13:"10:00 - 16:00";}',
   '', '', '', '', 1, '2016-08-04 12:42:37', '2016-08-04 12:42:37');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_store_shop`
--

CREATE TABLE IF NOT EXISTS `ps_store_shop` (
  `id_store` INT(11) UNSIGNED NOT NULL,
  `id_shop`  INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_store`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_store_shop`
--

INSERT INTO `ps_store_shop` (`id_store`, `id_shop`) VALUES
  (1, 1),
  (2, 1),
  (3, 1),
  (4, 1),
  (5, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_supplier`
--

CREATE TABLE IF NOT EXISTS `ps_supplier` (
  `id_supplier` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`        VARCHAR(64)      NOT NULL,
  `date_add`    DATETIME         NOT NULL,
  `date_upd`    DATETIME         NOT NULL,
  `active`      TINYINT(1)       NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_supplier`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_supplier`
--

INSERT INTO `ps_supplier` (`id_supplier`, `name`, `date_add`, `date_upd`, `active`) VALUES
  (1, 'Fashion Supplier', '2016-08-04 12:42:30', '2016-08-04 12:42:30', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_supplier_lang`
--

CREATE TABLE IF NOT EXISTS `ps_supplier_lang` (
  `id_supplier`      INT(10) UNSIGNED NOT NULL,
  `id_lang`          INT(10) UNSIGNED NOT NULL,
  `description`      TEXT,
  `meta_title`       VARCHAR(128) DEFAULT NULL,
  `meta_keywords`    VARCHAR(255) DEFAULT NULL,
  `meta_description` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id_supplier`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_supplier_lang`
--

INSERT INTO `ps_supplier_lang` (`id_supplier`, `id_lang`, `description`, `meta_title`, `meta_keywords`, `meta_description`)
VALUES
  (1, 1, '', '', '', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_supplier_shop`
--

CREATE TABLE IF NOT EXISTS `ps_supplier_shop` (
  `id_supplier` INT(11) UNSIGNED NOT NULL,
  `id_shop`     INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_supplier`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_supplier_shop`
--

INSERT INTO `ps_supplier_shop` (`id_supplier`, `id_shop`) VALUES
  (1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_supply_order`
--

CREATE TABLE IF NOT EXISTS `ps_supply_order` (
  `id_supply_order`        INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_supplier`            INT(11) UNSIGNED NOT NULL,
  `supplier_name`          VARCHAR(64)      NOT NULL,
  `id_lang`                INT(11) UNSIGNED NOT NULL,
  `id_warehouse`           INT(11) UNSIGNED NOT NULL,
  `id_supply_order_state`  INT(11) UNSIGNED NOT NULL,
  `id_currency`            INT(11) UNSIGNED NOT NULL,
  `id_ref_currency`        INT(11) UNSIGNED NOT NULL,
  `reference`              VARCHAR(64)      NOT NULL,
  `date_add`               DATETIME         NOT NULL,
  `date_upd`               DATETIME         NOT NULL,
  `date_delivery_expected` DATETIME                  DEFAULT NULL,
  `total_te`               DECIMAL(20, 6)            DEFAULT '0.000000',
  `total_with_discount_te` DECIMAL(20, 6)            DEFAULT '0.000000',
  `total_tax`              DECIMAL(20, 6)            DEFAULT '0.000000',
  `total_ti`               DECIMAL(20, 6)            DEFAULT '0.000000',
  `discount_rate`          DECIMAL(20, 6)            DEFAULT '0.000000',
  `discount_value_te`      DECIMAL(20, 6)            DEFAULT '0.000000',
  `is_template`            TINYINT(1)                DEFAULT '0',
  PRIMARY KEY (`id_supply_order`),
  KEY `id_supplier` (`id_supplier`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `reference` (`reference`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_supply_order`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_supply_order_detail`
--

CREATE TABLE IF NOT EXISTS `ps_supply_order_detail` (
  `id_supply_order_detail`        INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_supply_order`               INT(11) UNSIGNED NOT NULL,
  `id_currency`                   INT(11) UNSIGNED NOT NULL,
  `id_product`                    INT(11) UNSIGNED NOT NULL,
  `id_product_attribute`          INT(11) UNSIGNED NOT NULL,
  `reference`                     VARCHAR(32)      NOT NULL,
  `supplier_reference`            VARCHAR(32)      NOT NULL,
  `name`                          VARCHAR(128)     NOT NULL,
  `ean13`                         VARCHAR(13)               DEFAULT NULL,
  `upc`                           VARCHAR(12)               DEFAULT NULL,
  `exchange_rate`                 DECIMAL(20, 6)            DEFAULT '0.000000',
  `unit_price_te`                 DECIMAL(20, 6)            DEFAULT '0.000000',
  `quantity_expected`             INT(11) UNSIGNED NOT NULL,
  `quantity_received`             INT(11) UNSIGNED NOT NULL,
  `price_te`                      DECIMAL(20, 6)            DEFAULT '0.000000',
  `discount_rate`                 DECIMAL(20, 6)            DEFAULT '0.000000',
  `discount_value_te`             DECIMAL(20, 6)            DEFAULT '0.000000',
  `price_with_discount_te`        DECIMAL(20, 6)            DEFAULT '0.000000',
  `tax_rate`                      DECIMAL(20, 6)            DEFAULT '0.000000',
  `tax_value`                     DECIMAL(20, 6)            DEFAULT '0.000000',
  `price_ti`                      DECIMAL(20, 6)            DEFAULT '0.000000',
  `tax_value_with_order_discount` DECIMAL(20, 6)            DEFAULT '0.000000',
  `price_with_order_discount_te`  DECIMAL(20, 6)            DEFAULT '0.000000',
  PRIMARY KEY (`id_supply_order_detail`),
  KEY `id_supply_order` (`id_supply_order`, `id_product`),
  KEY `id_product_attribute` (`id_product_attribute`),
  KEY `id_product_product_attribute` (`id_product`, `id_product_attribute`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_supply_order_detail`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_supply_order_history`
--

CREATE TABLE IF NOT EXISTS `ps_supply_order_history` (
  `id_supply_order_history` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_supply_order`         INT(11) UNSIGNED NOT NULL,
  `id_employee`             INT(11) UNSIGNED NOT NULL,
  `employee_lastname`       VARCHAR(32)               DEFAULT '',
  `employee_firstname`      VARCHAR(32)               DEFAULT '',
  `id_state`                INT(11) UNSIGNED NOT NULL,
  `date_add`                DATETIME         NOT NULL,
  PRIMARY KEY (`id_supply_order_history`),
  KEY `id_supply_order` (`id_supply_order`),
  KEY `id_employee` (`id_employee`),
  KEY `id_state` (`id_state`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_supply_order_history`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_supply_order_receipt_history`
--

CREATE TABLE IF NOT EXISTS `ps_supply_order_receipt_history` (
  `id_supply_order_receipt_history` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_supply_order_detail`          INT(11) UNSIGNED NOT NULL,
  `id_employee`                     INT(11) UNSIGNED NOT NULL,
  `employee_lastname`               VARCHAR(32)               DEFAULT '',
  `employee_firstname`              VARCHAR(32)               DEFAULT '',
  `id_supply_order_state`           INT(11) UNSIGNED NOT NULL,
  `quantity`                        INT(11) UNSIGNED NOT NULL,
  `date_add`                        DATETIME         NOT NULL,
  PRIMARY KEY (`id_supply_order_receipt_history`),
  KEY `id_supply_order_detail` (`id_supply_order_detail`),
  KEY `id_supply_order_state` (`id_supply_order_state`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_supply_order_receipt_history`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_supply_order_state`
--

CREATE TABLE IF NOT EXISTS `ps_supply_order_state` (
  `id_supply_order_state` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `delivery_note`         TINYINT(1)       NOT NULL DEFAULT '0',
  `editable`              TINYINT(1)       NOT NULL DEFAULT '0',
  `receipt_state`         TINYINT(1)       NOT NULL DEFAULT '0',
  `pending_receipt`       TINYINT(1)       NOT NULL DEFAULT '0',
  `enclosed`              TINYINT(1)       NOT NULL DEFAULT '0',
  `color`                 VARCHAR(32)               DEFAULT NULL,
  PRIMARY KEY (`id_supply_order_state`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 7;

--
-- Vypisuji data pro tabulku `ps_supply_order_state`
--

INSERT INTO `ps_supply_order_state` (`id_supply_order_state`, `delivery_note`, `editable`, `receipt_state`, `pending_receipt`, `enclosed`, `color`)
VALUES
  (1, 0, 1, 0, 0, 0, '#faab00'),
  (2, 1, 0, 0, 0, 0, '#273cff'),
  (3, 0, 0, 0, 1, 0, '#ff37f5'),
  (4, 0, 0, 1, 1, 0, '#ff3e33'),
  (5, 0, 0, 1, 0, 1, '#00d60c'),
  (6, 0, 0, 0, 0, 1, '#666666');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_supply_order_state_lang`
--

CREATE TABLE IF NOT EXISTS `ps_supply_order_state_lang` (
  `id_supply_order_state` INT(11) UNSIGNED NOT NULL,
  `id_lang`               INT(11) UNSIGNED NOT NULL,
  `name`                  VARCHAR(128) DEFAULT NULL,
  PRIMARY KEY (`id_supply_order_state`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_supply_order_state_lang`
--

INSERT INTO `ps_supply_order_state_lang` (`id_supply_order_state`, `id_lang`, `name`) VALUES
  (1, 1, '1 - Creation in progress'),
  (2, 1, '2 - Order validated'),
  (3, 1, '3 - Pending receipt'),
  (4, 1, '4 - Order received in part'),
  (5, 1, '5 - Order received completely'),
  (6, 1, '6 - Order canceled');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tab`
--

CREATE TABLE IF NOT EXISTS `ps_tab` (
  `id_tab`         INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_parent`      INT(11)          NOT NULL,
  `class_name`     VARCHAR(64)      NOT NULL,
  `module`         VARCHAR(64)               DEFAULT NULL,
  `position`       INT(10) UNSIGNED NOT NULL,
  `active`         TINYINT(1)       NOT NULL DEFAULT '1',
  `hide_host_mode` TINYINT(1)       NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_tab`),
  KEY `class_name` (`class_name`),
  KEY `id_parent` (`id_parent`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 121;

--
-- Vypisuji data pro tabulku `ps_tab`
--

INSERT INTO `ps_tab` (`id_tab`, `id_parent`, `class_name`, `module`, `position`, `active`, `hide_host_mode`) VALUES
  (1, 0, 'AdminDashboard', NULL, 0, 1, 0),
  (2, -1, 'AdminCms', NULL, 0, 1, 0),
  (3, -1, 'AdminCmsCategories', NULL, 1, 1, 0),
  (4, -1, 'AdminAttributeGenerator', NULL, 2, 1, 0),
  (5, -1, 'AdminSearch', NULL, 3, 1, 0),
  (6, -1, 'AdminLogin', NULL, 4, 1, 0),
  (7, -1, 'AdminShop', NULL, 5, 1, 0),
  (8, -1, 'AdminShopUrl', NULL, 6, 1, 0),
  (9, 0, 'AdminCatalog', NULL, 1, 1, 0),
  (10, 0, 'AdminParentOrders', NULL, 2, 1, 0),
  (11, 0, 'AdminParentCustomer', NULL, 3, 1, 0),
  (12, 0, 'AdminPriceRule', NULL, 4, 1, 0),
  (13, 0, 'AdminParentModules', NULL, 5, 1, 0),
  (14, 0, 'AdminParentShipping', NULL, 6, 1, 0),
  (15, 0, 'AdminParentLocalization', NULL, 7, 1, 0),
  (16, 0, 'AdminParentPreferences', NULL, 8, 1, 0),
  (17, 0, 'AdminTools', NULL, 9, 1, 0),
  (18, 0, 'AdminAdmin', NULL, 10, 1, 0),
  (19, 0, 'AdminParentStats', NULL, 11, 1, 0),
  (20, 0, 'AdminStock', NULL, 12, 1, 0),
  (21, 9, 'AdminProducts', NULL, 0, 1, 0),
  (22, 9, 'AdminCategories', NULL, 1, 1, 0),
  (23, 9, 'AdminTracking', NULL, 2, 1, 0),
  (24, 9, 'AdminAttributesGroups', NULL, 3, 1, 0),
  (25, 9, 'AdminFeatures', NULL, 4, 1, 0),
  (26, 9, 'AdminManufacturers', NULL, 5, 1, 0),
  (27, 9, 'AdminSuppliers', NULL, 6, 1, 0),
  (28, 9, 'AdminTags', NULL, 7, 1, 0),
  (29, 9, 'AdminAttachments', NULL, 8, 1, 0),
  (30, 10, 'AdminOrders', NULL, 0, 1, 0),
  (31, 10, 'AdminInvoices', NULL, 1, 1, 0),
  (32, 10, 'AdminReturn', NULL, 2, 1, 0),
  (33, 10, 'AdminDeliverySlip', NULL, 3, 1, 0),
  (34, 10, 'AdminSlip', NULL, 4, 1, 0),
  (35, 10, 'AdminStatuses', NULL, 5, 1, 0),
  (36, 10, 'AdminOrderMessage', NULL, 6, 1, 0),
  (37, 11, 'AdminCustomers', NULL, 0, 1, 0),
  (38, 11, 'AdminAddresses', NULL, 1, 1, 0),
  (39, 11, 'AdminGroups', NULL, 2, 1, 0),
  (40, 11, 'AdminCarts', NULL, 3, 1, 0),
  (41, 11, 'AdminCustomerThreads', NULL, 4, 1, 0),
  (42, 11, 'AdminContacts', NULL, 5, 1, 0),
  (43, 11, 'AdminGenders', NULL, 6, 1, 0),
  (44, 11, 'AdminOutstanding', '', 7, 0, 0),
  (45, 12, 'AdminCartRules', NULL, 0, 1, 0),
  (46, 12, 'AdminSpecificPriceRule', NULL, 1, 1, 0),
  (47, 12, 'AdminMarketing', NULL, 2, 1, 0),
  (48, 14, 'AdminCarriers', NULL, 0, 1, 0),
  (49, 14, 'AdminShipping', NULL, 1, 1, 0),
  (50, 14, 'AdminCarrierWizard', NULL, 2, 1, 0),
  (51, 15, 'AdminLocalization', NULL, 0, 1, 0),
  (52, 15, 'AdminLanguages', NULL, 1, 1, 0),
  (53, 15, 'AdminZones', NULL, 2, 1, 0),
  (54, 15, 'AdminCountries', NULL, 3, 1, 0),
  (55, 15, 'AdminStates', NULL, 4, 1, 0),
  (56, 15, 'AdminCurrencies', NULL, 5, 1, 0),
  (57, 15, 'AdminTaxes', NULL, 6, 1, 0),
  (58, 15, 'AdminTaxRulesGroup', NULL, 7, 1, 0),
  (59, 15, 'AdminTranslations', NULL, 8, 1, 0),
  (60, 13, 'AdminModules', NULL, 0, 1, 0),
  (61, 13, 'AdminAddonsCatalog', NULL, 1, 1, 0),
  (62, 13, 'AdminModulesPositions', NULL, 2, 1, 0),
  (63, 13, 'AdminPayment', NULL, 3, 1, 0),
  (64, 16, 'AdminPreferences', NULL, 0, 1, 0),
  (65, 16, 'AdminOrderPreferences', NULL, 1, 1, 0),
  (66, 16, 'AdminPPreferences', NULL, 2, 1, 0),
  (67, 16, 'AdminCustomerPreferences', NULL, 3, 1, 0),
  (68, 16, 'AdminThemes', NULL, 4, 1, 0),
  (69, 16, 'AdminMeta', NULL, 5, 1, 0),
  (70, 16, 'AdminCmsContent', NULL, 6, 1, 0),
  (71, 16, 'AdminImages', NULL, 7, 1, 0),
  (72, 16, 'AdminStores', NULL, 8, 1, 0),
  (73, 16, 'AdminSearchConf', NULL, 9, 1, 0),
  (74, 16, 'AdminMaintenance', NULL, 10, 1, 0),
  (75, 16, 'AdminGeolocation', NULL, 11, 1, 0),
  (76, 17, 'AdminInformation', NULL, 0, 1, 0),
  (77, 17, 'AdminPerformance', NULL, 1, 1, 0),
  (78, 17, 'AdminEmails', NULL, 2, 1, 0),
  (79, 17, 'AdminShopGroup', NULL, 3, 0, 0),
  (80, 17, 'AdminImport', NULL, 4, 1, 0),
  (81, 17, 'AdminBackup', NULL, 5, 1, 0),
  (82, 17, 'AdminRequestSql', NULL, 6, 1, 0),
  (83, 17, 'AdminLogs', NULL, 7, 1, 0),
  (84, 17, 'AdminWebservice', NULL, 8, 1, 0),
  (85, 18, 'AdminAdminPreferences', NULL, 0, 1, 0),
  (86, 18, 'AdminQuickAccesses', NULL, 1, 1, 0),
  (87, 18, 'AdminEmployees', NULL, 2, 1, 0),
  (88, 18, 'AdminProfiles', NULL, 3, 1, 0),
  (89, 18, 'AdminAccess', NULL, 4, 1, 0),
  (90, 18, 'AdminTabs', NULL, 5, 1, 0),
  (91, 19, 'AdminStats', NULL, 0, 1, 0),
  (92, 19, 'AdminSearchEngines', NULL, 1, 1, 0),
  (93, 19, 'AdminReferrers', NULL, 2, 1, 0),
  (94, 20, 'AdminWarehouses', NULL, 0, 1, 0),
  (95, 20, 'AdminStockManagement', NULL, 1, 1, 0),
  (96, 20, 'AdminStockMvt', NULL, 2, 1, 0),
  (97, 20, 'AdminStockInstantState', NULL, 3, 1, 0),
  (98, 20, 'AdminStockCover', NULL, 4, 1, 0),
  (99, 20, 'AdminSupplyOrders', NULL, 5, 1, 0),
  (100, 20, 'AdminStockConfiguration', NULL, 6, 1, 0),
  (101, -1, 'AdminBlockCategories', 'blockcategories', 7, 1, 0),
  (102, -1, 'AdminDashgoals', 'dashgoals', 8, 1, 0),
  (103, -1, 'AdminThemeConfigurator', 'themeconfigurator', 9, 1, 0),
  (104, 18, 'AdminGamification', 'gamification', 6, 1, 0),
  (105, -1, 'AdminCronJobs', 'cronjobs', 10, 1, 0),
  (107, 0, 'AdminSmartBlog', '', 13, 1, 0),
  (108, 107, 'AdminBlogCategory', 'smartblog', 1, 1, 0),
  (109, 107, 'AdminBlogcomment', 'smartblog', 2, 1, 0),
  (110, 107, 'AdminBlogPost', 'smartblog', 3, 1, 0),
  (111, 107, 'AdminImageType', 'smartblog', 4, 1, 0),
  (112, -1, 'AdminTMProductVideos', 'tmproductvideos', 11, 1, 0),
  (113, -1, 'AdminTMMegaMenu', 'tmmegamenu', 12, 1, 0),
  (114, -1, 'AdminHtmlContent', 'tmhtmlcontent', 13, 1, 0),
  (115, -1, 'AdminTMProductsSlider', 'tmproductsslider', 14, 1, 0),
  (116, 0, 'AdminSampleDataInstall', 'sampledatainstall', 14, 1, 0),
  (117, 116, 'AdminSampleDataInstallImport', 'sampledatainstall', 1, 1, 0),
  (118, 116, 'AdminSampleDataInstallExport', 'sampledatainstall', 2, 1, 0),
  (119, -1, 'AdminTMMegaLayout', 'tmmegalayout', 15, 1, 0),
  (120, -1, 'AdminGanalyticsAjax', 'ganalytics', 16, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tab_advice`
--

CREATE TABLE IF NOT EXISTS `ps_tab_advice` (
  `id_tab`    INT(11) NOT NULL,
  `id_advice` INT(11) NOT NULL,
  PRIMARY KEY (`id_tab`, `id_advice`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_tab_advice`
--

INSERT INTO `ps_tab_advice` (`id_tab`, `id_advice`) VALUES
  (0, 4),
  (0, 8),
  (0, 12),
  (0, 16),
  (0, 20),
  (0, 24),
  (0, 28),
  (0, 32),
  (0, 36),
  (0, 40),
  (0, 44),
  (0, 48),
  (0, 52),
  (59, 1),
  (59, 5),
  (59, 9),
  (59, 13),
  (59, 17),
  (59, 21),
  (59, 25),
  (59, 29),
  (59, 33),
  (59, 37),
  (59, 41),
  (59, 45),
  (59, 49),
  (63, 2),
  (63, 6),
  (63, 10),
  (63, 14),
  (63, 18),
  (63, 22),
  (63, 26),
  (63, 30),
  (63, 34),
  (63, 38),
  (63, 42),
  (63, 46),
  (63, 50),
  (65, 3),
  (65, 7),
  (65, 11),
  (65, 15),
  (65, 19),
  (65, 23),
  (65, 27),
  (65, 31),
  (65, 35),
  (65, 39),
  (65, 43),
  (65, 47),
  (65, 51);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tab_lang`
--

CREATE TABLE IF NOT EXISTS `ps_tab_lang` (
  `id_tab`  INT(10) UNSIGNED NOT NULL,
  `id_lang` INT(10) UNSIGNED NOT NULL,
  `name`    VARCHAR(64) DEFAULT NULL,
  PRIMARY KEY (`id_tab`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_tab_lang`
--

INSERT INTO `ps_tab_lang` (`id_tab`, `id_lang`, `name`) VALUES
  (1, 1, 'Nástěnka'),
  (2, 1, 'Stránky CMS'),
  (3, 1, 'CMS kategorie'),
  (4, 1, 'Generátor kombinací'),
  (5, 1, 'Vyhledávání'),
  (6, 1, 'Login'),
  (7, 1, 'Obchody'),
  (8, 1, 'Odkazy'),
  (9, 1, 'Katalog'),
  (10, 1, 'Objednávky'),
  (11, 1, 'Zákazníci'),
  (12, 1, 'Pravidla cen'),
  (13, 1, 'Moduly a Služby'),
  (14, 1, 'Doručení'),
  (15, 1, 'Lokalizace'),
  (16, 1, 'Konfigurace'),
  (17, 1, 'Nástroje'),
  (18, 1, 'Administrace'),
  (19, 1, 'Statistiky'),
  (20, 1, 'Zásoby'),
  (21, 1, 'Produkty'),
  (22, 1, 'Kategorie'),
  (23, 1, 'Monitorování'),
  (24, 1, 'Atributy produktů'),
  (25, 1, 'Vlastnosti produktů'),
  (26, 1, 'Výrobci'),
  (27, 1, 'Dodavatelé'),
  (28, 1, 'Štítky'),
  (29, 1, 'Přílohy'),
  (30, 1, 'Objednávky'),
  (31, 1, 'Faktury'),
  (32, 1, 'Vrácení zboží'),
  (33, 1, 'Dodací listy'),
  (34, 1, 'Dobropisy'),
  (35, 1, 'Stavy objednávek'),
  (36, 1, 'Zprávy k objednávkám'),
  (37, 1, 'Zákazníci'),
  (38, 1, 'Adresy'),
  (39, 1, 'Skupiny zákazníků'),
  (40, 1, 'Nákupní košíky'),
  (41, 1, 'Zákaznický servis'),
  (42, 1, 'Kontakty na zaměstnance'),
  (43, 1, 'Oslovení skupiny'),
  (44, 1, 'Outstanding'),
  (45, 1, 'Pravidla pro košík'),
  (46, 1, 'Pravidla pro katalog'),
  (47, 1, 'Marketing'),
  (48, 1, 'Dopravci'),
  (49, 1, 'Konfigurace'),
  (50, 1, 'Dopravce'),
  (51, 1, 'Lokalizace'),
  (52, 1, 'Jazyky'),
  (53, 1, 'Zóny'),
  (54, 1, 'Země'),
  (55, 1, 'Státy'),
  (56, 1, 'Měny'),
  (57, 1, 'DPH'),
  (58, 1, 'Daňová pravidla'),
  (59, 1, 'Překlady'),
  (60, 1, 'Moduly a Služby'),
  (61, 1, 'Katalog modulů a šablon'),
  (62, 1, 'Pozice modulů'),
  (63, 1, 'Platba'),
  (64, 1, 'Hlavní'),
  (65, 1, 'Objednávky'),
  (66, 1, 'Produkty'),
  (67, 1, 'Zákazníci'),
  (68, 1, 'Šablony'),
  (69, 1, 'SEO a URLs'),
  (70, 1, 'CMS'),
  (71, 1, 'Obrázky'),
  (72, 1, 'Kontakty na prodejny'),
  (73, 1, 'Vyhledávání'),
  (74, 1, 'Údržba obchodu'),
  (75, 1, 'Geolokace'),
  (76, 1, 'Informace o konfiguraci'),
  (77, 1, 'Výkon'),
  (78, 1, 'E-maily'),
  (79, 1, 'Obchody Multistore'),
  (80, 1, 'Import'),
  (81, 1, 'Záloha DB'),
  (82, 1, 'SQL manažer'),
  (83, 1, 'Logy'),
  (84, 1, 'Webservice'),
  (85, 1, 'Konfigurace'),
  (86, 1, 'Rychlý přístup'),
  (87, 1, 'Zaměstnanci'),
  (88, 1, 'Profily'),
  (89, 1, 'Oprávnění přístupu'),
  (90, 1, 'Menus'),
  (91, 1, 'Statistiky'),
  (92, 1, 'Vyhledávače'),
  (93, 1, 'Odkud přišli'),
  (94, 1, 'Sklady'),
  (95, 1, 'Řízení zásob'),
  (96, 1, 'Skladové pohyby'),
  (97, 1, 'Okamžitý stav zásob'),
  (98, 1, 'Stav zásob'),
  (99, 1, 'Objednávky dodavatelům'),
  (100, 1, 'Konfigurace'),
  (101, 1, 'BlockCategories'),
  (102, 1, 'Dashgoals'),
  (103, 1, 'themeconfigurator'),
  (104, 1, 'Merchant Expertise'),
  (105, 1, 'Cron Jobs'),
  (107, 1, 'Blog'),
  (108, 1, 'Blog Category'),
  (109, 1, 'Blog Comments'),
  (110, 1, 'Blog Post'),
  (111, 1, 'Image Type'),
  (112, 1, 'tmproductvideos'),
  (113, 1, 'tmmegamenu'),
  (114, 1, 'tmhtmlcontent'),
  (115, 1, 'tmproductsslider'),
  (116, 1, 'Sample Data Install'),
  (117, 1, 'Import Data'),
  (118, 1, 'Export Data'),
  (119, 1, 'tmmegalayout'),
  (120, 1, 'Google Analytics Ajax');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tab_module_preference`
--

CREATE TABLE IF NOT EXISTS `ps_tab_module_preference` (
  `id_tab_module_preference` INT(11)      NOT NULL AUTO_INCREMENT,
  `id_employee`              INT(11)      NOT NULL,
  `id_tab`                   INT(11)      NOT NULL,
  `module`                   VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_tab_module_preference`),
  UNIQUE KEY `employee_module` (`id_employee`, `id_tab`, `module`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_tab_module_preference`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tag`
--

CREATE TABLE IF NOT EXISTS `ps_tag` (
  `id_tag`  INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_lang` INT(10) UNSIGNED NOT NULL,
  `name`    VARCHAR(32)      NOT NULL,
  PRIMARY KEY (`id_tag`),
  KEY `tag_name` (`name`),
  KEY `id_lang` (`id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_tag`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tag_count`
--

CREATE TABLE IF NOT EXISTS `ps_tag_count` (
  `id_group` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_tag`   INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_lang`  INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_shop`  INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `counter`  INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_group`, `id_tag`),
  KEY `id_group` (`id_group`, `id_lang`, `id_shop`, `counter`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_tag_count`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tax`
--

CREATE TABLE IF NOT EXISTS `ps_tax` (
  `id_tax`  INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `rate`    DECIMAL(10, 3)      NOT NULL,
  `active`  TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_tax`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 30;

--
-- Vypisuji data pro tabulku `ps_tax`
--

INSERT INTO `ps_tax` (`id_tax`, `rate`, `active`, `deleted`) VALUES
  (1, 21.000, 1, 0),
  (2, 15.000, 1, 0),
  (3, 20.000, 1, 0),
  (4, 21.000, 1, 0),
  (5, 20.000, 1, 0),
  (6, 19.000, 1, 0),
  (7, 19.000, 1, 0),
  (8, 25.000, 1, 0),
  (9, 20.000, 1, 0),
  (10, 21.000, 1, 0),
  (11, 24.000, 1, 0),
  (12, 20.000, 1, 0),
  (13, 20.000, 1, 0),
  (14, 23.000, 1, 0),
  (15, 25.000, 1, 0),
  (16, 27.000, 1, 0),
  (17, 23.000, 1, 0),
  (18, 22.000, 1, 0),
  (19, 21.000, 1, 0),
  (20, 17.000, 1, 0),
  (21, 21.000, 1, 0),
  (22, 18.000, 1, 0),
  (23, 21.000, 1, 0),
  (24, 23.000, 1, 0),
  (25, 23.000, 1, 0),
  (26, 24.000, 1, 0),
  (27, 25.000, 1, 0),
  (28, 22.000, 1, 0),
  (29, 20.000, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tax_lang`
--

CREATE TABLE IF NOT EXISTS `ps_tax_lang` (
  `id_tax`  INT(10) UNSIGNED NOT NULL,
  `id_lang` INT(10) UNSIGNED NOT NULL,
  `name`    VARCHAR(32)      NOT NULL,
  PRIMARY KEY (`id_tax`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_tax_lang`
--

INSERT INTO `ps_tax_lang` (`id_tax`, `id_lang`, `name`) VALUES
  (1, 1, 'DPH CZ 21%'),
  (2, 1, 'DPH CZ 15%'),
  (3, 1, 'USt. AT 20%'),
  (4, 1, 'TVA BE 21%'),
  (5, 1, 'ДДС BG 20%'),
  (6, 1, 'ΦΠΑ CY 19%'),
  (7, 1, 'MwSt. DE 19%'),
  (8, 1, 'moms DK 25%'),
  (9, 1, 'km EE 20%'),
  (10, 1, 'IVA ES 21%'),
  (11, 1, 'ALV FI 24%'),
  (12, 1, 'TVA FR 20%'),
  (13, 1, 'VAT UK 20%'),
  (14, 1, 'ΦΠΑ GR 23%'),
  (15, 1, 'Croatia PDV 25%'),
  (16, 1, 'ÁFA HU 27%'),
  (17, 1, 'VAT IE 23%'),
  (18, 1, 'IVA IT 22%'),
  (19, 1, 'PVM LT 21%'),
  (20, 1, 'TVA LU 17%'),
  (21, 1, 'PVN LV 21%'),
  (22, 1, 'VAT MT 18%'),
  (23, 1, 'BTW NL 21%'),
  (24, 1, 'PTU PL 23%'),
  (25, 1, 'IVA PT 23%'),
  (26, 1, 'TVA RO 24%'),
  (27, 1, 'Moms SE 25%'),
  (28, 1, 'DDV SI 22%'),
  (29, 1, 'DPH SK 20%');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tax_rule`
--

CREATE TABLE IF NOT EXISTS `ps_tax_rule` (
  `id_tax_rule`        INT(11)      NOT NULL AUTO_INCREMENT,
  `id_tax_rules_group` INT(11)      NOT NULL,
  `id_country`         INT(11)      NOT NULL,
  `id_state`           INT(11)      NOT NULL,
  `zipcode_from`       VARCHAR(12)  NOT NULL,
  `zipcode_to`         VARCHAR(12)  NOT NULL,
  `id_tax`             INT(11)      NOT NULL,
  `behavior`           INT(11)      NOT NULL,
  `description`        VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_tax_rule`),
  KEY `id_tax_rules_group` (`id_tax_rules_group`),
  KEY `id_tax` (`id_tax`),
  KEY `category_getproducts` (`id_tax_rules_group`, `id_country`, `id_state`, `zipcode_from`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 85;

--
-- Vypisuji data pro tabulku `ps_tax_rule`
--

INSERT INTO `ps_tax_rule` (`id_tax_rule`, `id_tax_rules_group`, `id_country`, `id_state`, `zipcode_from`, `zipcode_to`, `id_tax`, `behavior`, `description`)
VALUES
  (1, 1, 3, 0, '0', '0', 1, 0, ''),
  (2, 1, 236, 0, '0', '0', 1, 0, ''),
  (3, 1, 16, 0, '0', '0', 1, 0, ''),
  (4, 1, 20, 0, '0', '0', 1, 0, ''),
  (5, 1, 1, 0, '0', '0', 1, 0, ''),
  (6, 1, 86, 0, '0', '0', 1, 0, ''),
  (7, 1, 9, 0, '0', '0', 1, 0, ''),
  (8, 1, 6, 0, '0', '0', 1, 0, ''),
  (9, 1, 8, 0, '0', '0', 1, 0, ''),
  (10, 1, 26, 0, '0', '0', 1, 0, ''),
  (11, 1, 10, 0, '0', '0', 1, 0, ''),
  (12, 1, 76, 0, '0', '0', 1, 0, ''),
  (13, 1, 125, 0, '0', '0', 1, 0, ''),
  (14, 1, 131, 0, '0', '0', 1, 0, ''),
  (15, 1, 12, 0, '0', '0', 1, 0, ''),
  (16, 1, 143, 0, '0', '0', 1, 0, ''),
  (17, 1, 139, 0, '0', '0', 1, 0, ''),
  (18, 1, 13, 0, '0', '0', 1, 0, ''),
  (19, 1, 2, 0, '0', '0', 1, 0, ''),
  (20, 1, 14, 0, '0', '0', 1, 0, ''),
  (21, 1, 15, 0, '0', '0', 1, 0, ''),
  (22, 1, 36, 0, '0', '0', 1, 0, ''),
  (23, 1, 193, 0, '0', '0', 1, 0, ''),
  (24, 1, 37, 0, '0', '0', 1, 0, ''),
  (25, 1, 7, 0, '0', '0', 1, 0, ''),
  (26, 1, 18, 0, '0', '0', 1, 0, ''),
  (27, 1, 17, 0, '0', '0', 1, 0, ''),
  (28, 1, 74, 0, '0', '0', 1, 0, ''),
  (29, 2, 3, 0, '0', '0', 2, 0, ''),
  (30, 2, 236, 0, '0', '0', 2, 0, ''),
  (31, 2, 16, 0, '0', '0', 2, 0, ''),
  (32, 2, 20, 0, '0', '0', 2, 0, ''),
  (33, 2, 1, 0, '0', '0', 2, 0, ''),
  (34, 2, 86, 0, '0', '0', 2, 0, ''),
  (35, 2, 9, 0, '0', '0', 2, 0, ''),
  (36, 2, 6, 0, '0', '0', 2, 0, ''),
  (37, 2, 8, 0, '0', '0', 2, 0, ''),
  (38, 2, 26, 0, '0', '0', 2, 0, ''),
  (39, 2, 10, 0, '0', '0', 2, 0, ''),
  (40, 2, 76, 0, '0', '0', 2, 0, ''),
  (41, 2, 125, 0, '0', '0', 2, 0, ''),
  (42, 2, 131, 0, '0', '0', 2, 0, ''),
  (43, 2, 12, 0, '0', '0', 2, 0, ''),
  (44, 2, 143, 0, '0', '0', 2, 0, ''),
  (45, 2, 139, 0, '0', '0', 2, 0, ''),
  (46, 2, 13, 0, '0', '0', 2, 0, ''),
  (47, 2, 2, 0, '0', '0', 2, 0, ''),
  (48, 2, 14, 0, '0', '0', 2, 0, ''),
  (49, 2, 15, 0, '0', '0', 2, 0, ''),
  (50, 2, 36, 0, '0', '0', 2, 0, ''),
  (51, 2, 193, 0, '0', '0', 2, 0, ''),
  (52, 2, 37, 0, '0', '0', 2, 0, ''),
  (53, 2, 7, 0, '0', '0', 2, 0, ''),
  (54, 2, 18, 0, '0', '0', 2, 0, ''),
  (55, 2, 17, 0, '0', '0', 2, 0, ''),
  (56, 2, 74, 0, '0', '0', 2, 0, ''),
  (57, 3, 16, 0, '0', '0', 1, 0, ''),
  (58, 3, 2, 0, '0', '0', 3, 0, ''),
  (59, 3, 3, 0, '0', '0', 4, 0, ''),
  (60, 3, 236, 0, '0', '0', 5, 0, ''),
  (61, 3, 76, 0, '0', '0', 6, 0, ''),
  (62, 3, 1, 0, '0', '0', 7, 0, ''),
  (63, 3, 20, 0, '0', '0', 8, 0, ''),
  (64, 3, 86, 0, '0', '0', 9, 0, ''),
  (65, 3, 6, 0, '0', '0', 10, 0, ''),
  (66, 3, 7, 0, '0', '0', 11, 0, ''),
  (67, 3, 8, 0, '0', '0', 12, 0, ''),
  (68, 3, 17, 0, '0', '0', 13, 0, ''),
  (69, 3, 9, 0, '0', '0', 14, 0, ''),
  (70, 3, 74, 0, '0', '0', 15, 0, ''),
  (71, 3, 143, 0, '0', '0', 16, 0, ''),
  (72, 3, 26, 0, '0', '0', 17, 0, ''),
  (73, 3, 10, 0, '0', '0', 18, 0, ''),
  (74, 3, 131, 0, '0', '0', 19, 0, ''),
  (75, 3, 12, 0, '0', '0', 20, 0, ''),
  (76, 3, 125, 0, '0', '0', 21, 0, ''),
  (77, 3, 139, 0, '0', '0', 22, 0, ''),
  (78, 3, 13, 0, '0', '0', 23, 0, ''),
  (79, 3, 14, 0, '0', '0', 24, 0, ''),
  (80, 3, 15, 0, '0', '0', 25, 0, ''),
  (81, 3, 36, 0, '0', '0', 26, 0, ''),
  (82, 3, 18, 0, '0', '0', 27, 0, ''),
  (83, 3, 193, 0, '0', '0', 28, 0, ''),
  (84, 3, 37, 0, '0', '0', 29, 0, '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tax_rules_group`
--

CREATE TABLE IF NOT EXISTS `ps_tax_rules_group` (
  `id_tax_rules_group` INT(11)             NOT NULL AUTO_INCREMENT,
  `name`               VARCHAR(50)         NOT NULL,
  `active`             INT(11)             NOT NULL,
  `deleted`            TINYINT(1) UNSIGNED NOT NULL,
  `date_add`           DATETIME            NOT NULL,
  `date_upd`           DATETIME            NOT NULL,
  PRIMARY KEY (`id_tax_rules_group`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 4;

--
-- Vypisuji data pro tabulku `ps_tax_rules_group`
--

INSERT INTO `ps_tax_rules_group` (`id_tax_rules_group`, `name`, `active`, `deleted`, `date_add`, `date_upd`) VALUES
  (1, 'CZ Standard Rate (21%)', 1, 0, '2016-08-04 12:42:28', '2016-08-04 12:42:28'),
  (2, 'CZ Reduced Rate (15%)', 1, 0, '2016-08-04 12:42:28', '2016-08-04 12:42:28'),
  (3, 'EU VAT For Virtual Products', 1, 0, '2016-08-04 12:42:28', '2016-08-04 12:42:28');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tax_rules_group_shop`
--

CREATE TABLE IF NOT EXISTS `ps_tax_rules_group_shop` (
  `id_tax_rules_group` INT(11) UNSIGNED NOT NULL,
  `id_shop`            INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_tax_rules_group`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_tax_rules_group_shop`
--

INSERT INTO `ps_tax_rules_group_shop` (`id_tax_rules_group`, `id_shop`) VALUES
  (1, 1),
  (2, 1),
  (3, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_theme`
--

CREATE TABLE IF NOT EXISTS `ps_theme` (
  `id_theme`             INT(11)          NOT NULL AUTO_INCREMENT,
  `name`                 VARCHAR(64)      NOT NULL,
  `directory`            VARCHAR(64)      NOT NULL,
  `responsive`           TINYINT(1)       NOT NULL DEFAULT '0',
  `default_left_column`  TINYINT(1)       NOT NULL DEFAULT '0',
  `default_right_column` TINYINT(1)       NOT NULL DEFAULT '0',
  `product_per_page`     INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_theme`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 3;

--
-- Vypisuji data pro tabulku `ps_theme`
--

INSERT INTO `ps_theme` (`id_theme`, `name`, `directory`, `responsive`, `default_left_column`, `default_right_column`, `product_per_page`)
VALUES
  (1, 'default-bootstrap', 'default-bootstrap', 1, 1, 0, 12),
  (2, 'Oftex - prodej čoček', 'theme1353', 1, 1, 0, 12);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_themeconfigurator`
--

CREATE TABLE IF NOT EXISTS `ps_themeconfigurator` (
  `id_item`    INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_shop`    INT(10) UNSIGNED    NOT NULL,
  `id_lang`    INT(10) UNSIGNED    NOT NULL,
  `item_order` INT(10) UNSIGNED    NOT NULL,
  `title`      VARCHAR(100)                 DEFAULT NULL,
  `title_use`  TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `hook`       VARCHAR(100)                 DEFAULT NULL,
  `url`        TEXT,
  `target`     TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `image`      VARCHAR(100)                 DEFAULT NULL,
  `image_w`    VARCHAR(10)                  DEFAULT NULL,
  `image_h`    VARCHAR(10)                  DEFAULT NULL,
  `html`       TEXT,
  `active`     TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_item`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 8;

--
-- Vypisuji data pro tabulku `ps_themeconfigurator`
--

INSERT INTO `ps_themeconfigurator` (`id_item`, `id_shop`, `id_lang`, `item_order`, `title`, `title_use`, `hook`, `url`, `target`, `image`, `image_w`, `image_h`, `html`, `active`)
VALUES
  (1, 1, 1, 1, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img1.jpg', '383', '267', '', 1),
  (2, 1, 1, 2, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img2.jpg', '383', '267', '', 1),
  (3, 1, 1, 3, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img3.jpg', '383', '267', '', 1),
  (4, 1, 1, 4, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img4.jpg', '383', '142', '', 1),
  (5, 1, 1, 5, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img5.jpg', '777', '142', '', 1),
  (6, 1, 1, 6, '', 0, 'top', 'http://www.prestashop.com/', 0, 'banner-img6.jpg', '381', '219', '', 1),
  (7, 1, 1, 7, '', 0, 'top', 'http://www.prestashop.com/', 0, 'banner-img7.jpg', '381', '219', '', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_theme_meta`
--

CREATE TABLE IF NOT EXISTS `ps_theme_meta` (
  `id_theme_meta` INT(11)          NOT NULL AUTO_INCREMENT,
  `id_theme`      INT(11)          NOT NULL,
  `id_meta`       INT(10) UNSIGNED NOT NULL,
  `left_column`   TINYINT(1)       NOT NULL DEFAULT '1',
  `right_column`  TINYINT(1)       NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_theme_meta`),
  UNIQUE KEY `id_theme_2` (`id_theme`, `id_meta`),
  KEY `id_theme` (`id_theme`),
  KEY `id_meta` (`id_meta`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 91;

--
-- Vypisuji data pro tabulku `ps_theme_meta`
--

INSERT INTO `ps_theme_meta` (`id_theme_meta`, `id_theme`, `id_meta`, `left_column`, `right_column`) VALUES
  (1, 1, 1, 0, 0),
  (2, 1, 2, 1, 0),
  (3, 1, 3, 0, 0),
  (4, 1, 4, 0, 0),
  (5, 1, 5, 1, 0),
  (6, 1, 6, 1, 0),
  (7, 1, 7, 0, 0),
  (8, 1, 8, 1, 0),
  (9, 1, 9, 1, 0),
  (10, 1, 10, 0, 0),
  (11, 1, 11, 0, 0),
  (12, 1, 12, 0, 0),
  (13, 1, 13, 0, 0),
  (14, 1, 14, 0, 0),
  (15, 1, 15, 0, 0),
  (16, 1, 16, 0, 0),
  (17, 1, 17, 0, 0),
  (18, 1, 18, 0, 0),
  (19, 1, 19, 0, 0),
  (20, 1, 20, 0, 0),
  (21, 1, 21, 0, 0),
  (22, 1, 22, 1, 0),
  (23, 1, 23, 0, 0),
  (24, 1, 24, 0, 0),
  (25, 1, 25, 0, 0),
  (26, 1, 26, 0, 0),
  (27, 1, 28, 1, 0),
  (28, 1, 29, 0, 0),
  (29, 1, 27, 0, 0),
  (30, 1, 30, 0, 0),
  (31, 1, 31, 0, 0),
  (32, 1, 32, 0, 0),
  (33, 1, 33, 0, 0),
  (34, 1, 34, 0, 0),
  (35, 1, 36, 1, 0),
  (36, 1, 37, 1, 0),
  (37, 2, 2, 1, 0),
  (38, 2, 3, 0, 0),
  (39, 2, 4, 0, 0),
  (40, 2, 5, 1, 0),
  (41, 2, 6, 1, 0),
  (42, 2, 7, 0, 0),
  (43, 2, 8, 1, 0),
  (44, 2, 9, 0, 0),
  (45, 2, 10, 1, 0),
  (46, 2, 11, 1, 0),
  (47, 2, 12, 1, 0),
  (48, 2, 13, 1, 0),
  (49, 2, 14, 0, 0),
  (50, 2, 15, 1, 0),
  (51, 2, 16, 0, 0),
  (52, 2, 17, 1, 0),
  (53, 2, 18, 0, 0),
  (54, 2, 19, 0, 0),
  (55, 2, 20, 1, 0),
  (56, 2, 21, 0, 0),
  (57, 2, 22, 0, 0),
  (58, 2, 23, 0, 0),
  (59, 2, 24, 0, 0),
  (60, 2, 25, 1, 0),
  (61, 2, 26, 0, 0),
  (62, 2, 27, 0, 0),
  (63, 2, 28, 1, 0),
  (64, 2, 29, 0, 0),
  (65, 2, 30, 1, 0),
  (66, 2, 31, 1, 0),
  (67, 2, 32, 1, 0),
  (68, 2, 33, 1, 0),
  (69, 2, 35, 0, 0),
  (70, 2, 37, 1, 0),
  (71, 1, 38, 1, 0),
  (72, 2, 38, 1, 0),
  (73, 1, 39, 1, 0),
  (74, 2, 39, 1, 0),
  (75, 1, 40, 1, 0),
  (76, 2, 40, 1, 0),
  (77, 1, 41, 1, 0),
  (78, 2, 41, 1, 0),
  (79, 1, 42, 1, 0),
  (80, 2, 42, 1, 0),
  (81, 1, 43, 1, 0),
  (82, 2, 43, 1, 0),
  (83, 1, 44, 1, 0),
  (84, 2, 44, 1, 0),
  (85, 1, 45, 1, 0),
  (86, 2, 45, 1, 0),
  (87, 1, 46, 1, 0),
  (88, 2, 46, 1, 0),
  (89, 1, 47, 1, 0),
  (90, 2, 47, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_theme_specific`
--

CREATE TABLE IF NOT EXISTS `ps_theme_specific` (
  `id_theme`  INT(11) UNSIGNED NOT NULL,
  `id_shop`   INT(11) UNSIGNED NOT NULL,
  `entity`    INT(11) UNSIGNED NOT NULL,
  `id_object` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_theme`, `id_shop`, `entity`, `id_object`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_theme_specific`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_timezone`
--

CREATE TABLE IF NOT EXISTS `ps_timezone` (
  `id_timezone` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`        VARCHAR(32)      NOT NULL,
  PRIMARY KEY (`id_timezone`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 561;

--
-- Vypisuji data pro tabulku `ps_timezone`
--

INSERT INTO `ps_timezone` (`id_timezone`, `name`) VALUES
  (1, 'Africa/Abidjan'),
  (2, 'Africa/Accra'),
  (3, 'Africa/Addis_Ababa'),
  (4, 'Africa/Algiers'),
  (5, 'Africa/Asmara'),
  (6, 'Africa/Asmera'),
  (7, 'Africa/Bamako'),
  (8, 'Africa/Bangui'),
  (9, 'Africa/Banjul'),
  (10, 'Africa/Bissau'),
  (11, 'Africa/Blantyre'),
  (12, 'Africa/Brazzaville'),
  (13, 'Africa/Bujumbura'),
  (14, 'Africa/Cairo'),
  (15, 'Africa/Casablanca'),
  (16, 'Africa/Ceuta'),
  (17, 'Africa/Conakry'),
  (18, 'Africa/Dakar'),
  (19, 'Africa/Dar_es_Salaam'),
  (20, 'Africa/Djibouti'),
  (21, 'Africa/Douala'),
  (22, 'Africa/El_Aaiun'),
  (23, 'Africa/Freetown'),
  (24, 'Africa/Gaborone'),
  (25, 'Africa/Harare'),
  (26, 'Africa/Johannesburg'),
  (27, 'Africa/Kampala'),
  (28, 'Africa/Khartoum'),
  (29, 'Africa/Kigali'),
  (30, 'Africa/Kinshasa'),
  (31, 'Africa/Lagos'),
  (32, 'Africa/Libreville'),
  (33, 'Africa/Lome'),
  (34, 'Africa/Luanda'),
  (35, 'Africa/Lubumbashi'),
  (36, 'Africa/Lusaka'),
  (37, 'Africa/Malabo'),
  (38, 'Africa/Maputo'),
  (39, 'Africa/Maseru'),
  (40, 'Africa/Mbabane'),
  (41, 'Africa/Mogadishu'),
  (42, 'Africa/Monrovia'),
  (43, 'Africa/Nairobi'),
  (44, 'Africa/Ndjamena'),
  (45, 'Africa/Niamey'),
  (46, 'Africa/Nouakchott'),
  (47, 'Africa/Ouagadougou'),
  (48, 'Africa/Porto-Novo'),
  (49, 'Africa/Sao_Tome'),
  (50, 'Africa/Timbuktu'),
  (51, 'Africa/Tripoli'),
  (52, 'Africa/Tunis'),
  (53, 'Africa/Windhoek'),
  (54, 'America/Adak'),
  (55, 'America/Anchorage '),
  (56, 'America/Anguilla'),
  (57, 'America/Antigua'),
  (58, 'America/Araguaina'),
  (59, 'America/Argentina/Buenos_Aires'),
  (60, 'America/Argentina/Catamarca'),
  (61, 'America/Argentina/ComodRivadavia'),
  (62, 'America/Argentina/Cordoba'),
  (63, 'America/Argentina/Jujuy'),
  (64, 'America/Argentina/La_Rioja'),
  (65, 'America/Argentina/Mendoza'),
  (66, 'America/Argentina/Rio_Gallegos'),
  (67, 'America/Argentina/Salta'),
  (68, 'America/Argentina/San_Juan'),
  (69, 'America/Argentina/San_Luis'),
  (70, 'America/Argentina/Tucuman'),
  (71, 'America/Argentina/Ushuaia'),
  (72, 'America/Aruba'),
  (73, 'America/Asuncion'),
  (74, 'America/Atikokan'),
  (75, 'America/Atka'),
  (76, 'America/Bahia'),
  (77, 'America/Barbados'),
  (78, 'America/Belem'),
  (79, 'America/Belize'),
  (80, 'America/Blanc-Sablon'),
  (81, 'America/Boa_Vista'),
  (82, 'America/Bogota'),
  (83, 'America/Boise'),
  (84, 'America/Buenos_Aires'),
  (85, 'America/Cambridge_Bay'),
  (86, 'America/Campo_Grande'),
  (87, 'America/Cancun'),
  (88, 'America/Caracas'),
  (89, 'America/Catamarca'),
  (90, 'America/Cayenne'),
  (91, 'America/Cayman'),
  (92, 'America/Chicago'),
  (93, 'America/Chihuahua'),
  (94, 'America/Coral_Harbour'),
  (95, 'America/Cordoba'),
  (96, 'America/Costa_Rica'),
  (97, 'America/Cuiaba'),
  (98, 'America/Curacao'),
  (99, 'America/Danmarkshavn'),
  (100, 'America/Dawson'),
  (101, 'America/Dawson_Creek'),
  (102, 'America/Denver'),
  (103, 'America/Detroit'),
  (104, 'America/Dominica'),
  (105, 'America/Edmonton'),
  (106, 'America/Eirunepe'),
  (107, 'America/El_Salvador'),
  (108, 'America/Ensenada'),
  (109, 'America/Fort_Wayne'),
  (110, 'America/Fortaleza'),
  (111, 'America/Glace_Bay'),
  (112, 'America/Godthab'),
  (113, 'America/Goose_Bay'),
  (114, 'America/Grand_Turk'),
  (115, 'America/Grenada'),
  (116, 'America/Guadeloupe'),
  (117, 'America/Guatemala'),
  (118, 'America/Guayaquil'),
  (119, 'America/Guyana'),
  (120, 'America/Halifax'),
  (121, 'America/Havana'),
  (122, 'America/Hermosillo'),
  (123, 'America/Indiana/Indianapolis'),
  (124, 'America/Indiana/Knox'),
  (125, 'America/Indiana/Marengo'),
  (126, 'America/Indiana/Petersburg'),
  (127, 'America/Indiana/Tell_City'),
  (128, 'America/Indiana/Vevay'),
  (129, 'America/Indiana/Vincennes'),
  (130, 'America/Indiana/Winamac'),
  (131, 'America/Indianapolis'),
  (132, 'America/Inuvik'),
  (133, 'America/Iqaluit'),
  (134, 'America/Jamaica'),
  (135, 'America/Jujuy'),
  (136, 'America/Juneau'),
  (137, 'America/Kentucky/Louisville'),
  (138, 'America/Kentucky/Monticello'),
  (139, 'America/Knox_IN'),
  (140, 'America/La_Paz'),
  (141, 'America/Lima'),
  (142, 'America/Los_Angeles'),
  (143, 'America/Louisville'),
  (144, 'America/Maceio'),
  (145, 'America/Managua'),
  (146, 'America/Manaus'),
  (147, 'America/Marigot'),
  (148, 'America/Martinique'),
  (149, 'America/Mazatlan'),
  (150, 'America/Mendoza'),
  (151, 'America/Menominee'),
  (152, 'America/Merida'),
  (153, 'America/Mexico_City'),
  (154, 'America/Miquelon'),
  (155, 'America/Moncton'),
  (156, 'America/Monterrey'),
  (157, 'America/Montevideo'),
  (158, 'America/Montreal'),
  (159, 'America/Montserrat'),
  (160, 'America/Nassau'),
  (161, 'America/New_York'),
  (162, 'America/Nipigon'),
  (163, 'America/Nome'),
  (164, 'America/Noronha'),
  (165, 'America/North_Dakota/Center'),
  (166, 'America/North_Dakota/New_Salem'),
  (167, 'America/Panama'),
  (168, 'America/Pangnirtung'),
  (169, 'America/Paramaribo'),
  (170, 'America/Phoenix'),
  (171, 'America/Port-au-Prince'),
  (172, 'America/Port_of_Spain'),
  (173, 'America/Porto_Acre'),
  (174, 'America/Porto_Velho'),
  (175, 'America/Puerto_Rico'),
  (176, 'America/Rainy_River'),
  (177, 'America/Rankin_Inlet'),
  (178, 'America/Recife'),
  (179, 'America/Regina'),
  (180, 'America/Resolute'),
  (181, 'America/Rio_Branco'),
  (182, 'America/Rosario'),
  (183, 'America/Santarem'),
  (184, 'America/Santiago'),
  (185, 'America/Santo_Domingo'),
  (186, 'America/Sao_Paulo'),
  (187, 'America/Scoresbysund'),
  (188, 'America/Shiprock'),
  (189, 'America/St_Barthelemy'),
  (190, 'America/St_Johns'),
  (191, 'America/St_Kitts'),
  (192, 'America/St_Lucia'),
  (193, 'America/St_Thomas'),
  (194, 'America/St_Vincent'),
  (195, 'America/Swift_Current'),
  (196, 'America/Tegucigalpa'),
  (197, 'America/Thule'),
  (198, 'America/Thunder_Bay'),
  (199, 'America/Tijuana'),
  (200, 'America/Toronto'),
  (201, 'America/Tortola'),
  (202, 'America/Vancouver'),
  (203, 'America/Virgin'),
  (204, 'America/Whitehorse'),
  (205, 'America/Winnipeg'),
  (206, 'America/Yakutat'),
  (207, 'America/Yellowknife'),
  (208, 'Antarctica/Casey'),
  (209, 'Antarctica/Davis'),
  (210, 'Antarctica/DumontDUrville'),
  (211, 'Antarctica/Mawson'),
  (212, 'Antarctica/McMurdo'),
  (213, 'Antarctica/Palmer'),
  (214, 'Antarctica/Rothera'),
  (215, 'Antarctica/South_Pole'),
  (216, 'Antarctica/Syowa'),
  (217, 'Antarctica/Vostok'),
  (218, 'Arctic/Longyearbyen'),
  (219, 'Asia/Aden'),
  (220, 'Asia/Almaty'),
  (221, 'Asia/Amman'),
  (222, 'Asia/Anadyr'),
  (223, 'Asia/Aqtau'),
  (224, 'Asia/Aqtobe'),
  (225, 'Asia/Ashgabat'),
  (226, 'Asia/Ashkhabad'),
  (227, 'Asia/Baghdad'),
  (228, 'Asia/Bahrain'),
  (229, 'Asia/Baku'),
  (230, 'Asia/Bangkok'),
  (231, 'Asia/Beirut'),
  (232, 'Asia/Bishkek'),
  (233, 'Asia/Brunei'),
  (234, 'Asia/Calcutta'),
  (235, 'Asia/Choibalsan'),
  (236, 'Asia/Chongqing'),
  (237, 'Asia/Chungking'),
  (238, 'Asia/Colombo'),
  (239, 'Asia/Dacca'),
  (240, 'Asia/Damascus'),
  (241, 'Asia/Dhaka'),
  (242, 'Asia/Dili'),
  (243, 'Asia/Dubai'),
  (244, 'Asia/Dushanbe'),
  (245, 'Asia/Gaza'),
  (246, 'Asia/Harbin'),
  (247, 'Asia/Ho_Chi_Minh'),
  (248, 'Asia/Hong_Kong'),
  (249, 'Asia/Hovd'),
  (250, 'Asia/Irkutsk'),
  (251, 'Asia/Istanbul'),
  (252, 'Asia/Jakarta'),
  (253, 'Asia/Jayapura'),
  (254, 'Asia/Jerusalem'),
  (255, 'Asia/Kabul'),
  (256, 'Asia/Kamchatka'),
  (257, 'Asia/Karachi'),
  (258, 'Asia/Kashgar'),
  (259, 'Asia/Kathmandu'),
  (260, 'Asia/Katmandu'),
  (261, 'Asia/Kolkata'),
  (262, 'Asia/Krasnoyarsk'),
  (263, 'Asia/Kuala_Lumpur'),
  (264, 'Asia/Kuching'),
  (265, 'Asia/Kuwait'),
  (266, 'Asia/Macao'),
  (267, 'Asia/Macau'),
  (268, 'Asia/Magadan'),
  (269, 'Asia/Makassar'),
  (270, 'Asia/Manila'),
  (271, 'Asia/Muscat'),
  (272, 'Asia/Nicosia'),
  (273, 'Asia/Novosibirsk'),
  (274, 'Asia/Omsk'),
  (275, 'Asia/Oral'),
  (276, 'Asia/Phnom_Penh'),
  (277, 'Asia/Pontianak'),
  (278, 'Asia/Pyongyang'),
  (279, 'Asia/Qatar'),
  (280, 'Asia/Qyzylorda'),
  (281, 'Asia/Rangoon'),
  (282, 'Asia/Riyadh'),
  (283, 'Asia/Saigon'),
  (284, 'Asia/Sakhalin'),
  (285, 'Asia/Samarkand'),
  (286, 'Asia/Seoul'),
  (287, 'Asia/Shanghai'),
  (288, 'Asia/Singapore'),
  (289, 'Asia/Taipei'),
  (290, 'Asia/Tashkent'),
  (291, 'Asia/Tbilisi'),
  (292, 'Asia/Tehran'),
  (293, 'Asia/Tel_Aviv'),
  (294, 'Asia/Thimbu'),
  (295, 'Asia/Thimphu'),
  (296, 'Asia/Tokyo'),
  (297, 'Asia/Ujung_Pandang'),
  (298, 'Asia/Ulaanbaatar'),
  (299, 'Asia/Ulan_Bator'),
  (300, 'Asia/Urumqi'),
  (301, 'Asia/Vientiane'),
  (302, 'Asia/Vladivostok'),
  (303, 'Asia/Yakutsk'),
  (304, 'Asia/Yekaterinburg'),
  (305, 'Asia/Yerevan'),
  (306, 'Atlantic/Azores'),
  (307, 'Atlantic/Bermuda'),
  (308, 'Atlantic/Canary'),
  (309, 'Atlantic/Cape_Verde'),
  (310, 'Atlantic/Faeroe'),
  (311, 'Atlantic/Faroe'),
  (312, 'Atlantic/Jan_Mayen'),
  (313, 'Atlantic/Madeira'),
  (314, 'Atlantic/Reykjavik'),
  (315, 'Atlantic/South_Georgia'),
  (316, 'Atlantic/St_Helena'),
  (317, 'Atlantic/Stanley'),
  (318, 'Australia/ACT'),
  (319, 'Australia/Adelaide'),
  (320, 'Australia/Brisbane'),
  (321, 'Australia/Broken_Hill'),
  (322, 'Australia/Canberra'),
  (323, 'Australia/Currie'),
  (324, 'Australia/Darwin'),
  (325, 'Australia/Eucla'),
  (326, 'Australia/Hobart'),
  (327, 'Australia/LHI'),
  (328, 'Australia/Lindeman'),
  (329, 'Australia/Lord_Howe'),
  (330, 'Australia/Melbourne'),
  (331, 'Australia/North'),
  (332, 'Australia/NSW'),
  (333, 'Australia/Perth'),
  (334, 'Australia/Queensland'),
  (335, 'Australia/South'),
  (336, 'Australia/Sydney'),
  (337, 'Australia/Tasmania'),
  (338, 'Australia/Victoria'),
  (339, 'Australia/West'),
  (340, 'Australia/Yancowinna'),
  (341, 'Europe/Amsterdam'),
  (342, 'Europe/Andorra'),
  (343, 'Europe/Athens'),
  (344, 'Europe/Belfast'),
  (345, 'Europe/Belgrade'),
  (346, 'Europe/Berlin'),
  (347, 'Europe/Bratislava'),
  (348, 'Europe/Brussels'),
  (349, 'Europe/Bucharest'),
  (350, 'Europe/Budapest'),
  (351, 'Europe/Chisinau'),
  (352, 'Europe/Copenhagen'),
  (353, 'Europe/Dublin'),
  (354, 'Europe/Gibraltar'),
  (355, 'Europe/Guernsey'),
  (356, 'Europe/Helsinki'),
  (357, 'Europe/Isle_of_Man'),
  (358, 'Europe/Istanbul'),
  (359, 'Europe/Jersey'),
  (360, 'Europe/Kaliningrad'),
  (361, 'Europe/Kiev'),
  (362, 'Europe/Lisbon'),
  (363, 'Europe/Ljubljana'),
  (364, 'Europe/London'),
  (365, 'Europe/Luxembourg'),
  (366, 'Europe/Madrid'),
  (367, 'Europe/Malta'),
  (368, 'Europe/Mariehamn'),
  (369, 'Europe/Minsk'),
  (370, 'Europe/Monaco'),
  (371, 'Europe/Moscow'),
  (372, 'Europe/Nicosia'),
  (373, 'Europe/Oslo'),
  (374, 'Europe/Paris'),
  (375, 'Europe/Podgorica'),
  (376, 'Europe/Prague'),
  (377, 'Europe/Riga'),
  (378, 'Europe/Rome'),
  (379, 'Europe/Samara'),
  (380, 'Europe/San_Marino'),
  (381, 'Europe/Sarajevo'),
  (382, 'Europe/Simferopol'),
  (383, 'Europe/Skopje'),
  (384, 'Europe/Sofia'),
  (385, 'Europe/Stockholm'),
  (386, 'Europe/Tallinn'),
  (387, 'Europe/Tirane'),
  (388, 'Europe/Tiraspol'),
  (389, 'Europe/Uzhgorod'),
  (390, 'Europe/Vaduz'),
  (391, 'Europe/Vatican'),
  (392, 'Europe/Vienna'),
  (393, 'Europe/Vilnius'),
  (394, 'Europe/Volgograd'),
  (395, 'Europe/Warsaw'),
  (396, 'Europe/Zagreb'),
  (397, 'Europe/Zaporozhye'),
  (398, 'Europe/Zurich'),
  (399, 'Indian/Antananarivo'),
  (400, 'Indian/Chagos'),
  (401, 'Indian/Christmas'),
  (402, 'Indian/Cocos'),
  (403, 'Indian/Comoro'),
  (404, 'Indian/Kerguelen'),
  (405, 'Indian/Mahe'),
  (406, 'Indian/Maldives'),
  (407, 'Indian/Mauritius'),
  (408, 'Indian/Mayotte'),
  (409, 'Indian/Reunion'),
  (410, 'Pacific/Apia'),
  (411, 'Pacific/Auckland'),
  (412, 'Pacific/Chatham'),
  (413, 'Pacific/Easter'),
  (414, 'Pacific/Efate'),
  (415, 'Pacific/Enderbury'),
  (416, 'Pacific/Fakaofo'),
  (417, 'Pacific/Fiji'),
  (418, 'Pacific/Funafuti'),
  (419, 'Pacific/Galapagos'),
  (420, 'Pacific/Gambier'),
  (421, 'Pacific/Guadalcanal'),
  (422, 'Pacific/Guam'),
  (423, 'Pacific/Honolulu'),
  (424, 'Pacific/Johnston'),
  (425, 'Pacific/Kiritimati'),
  (426, 'Pacific/Kosrae'),
  (427, 'Pacific/Kwajalein'),
  (428, 'Pacific/Majuro'),
  (429, 'Pacific/Marquesas'),
  (430, 'Pacific/Midway'),
  (431, 'Pacific/Nauru'),
  (432, 'Pacific/Niue'),
  (433, 'Pacific/Norfolk'),
  (434, 'Pacific/Noumea'),
  (435, 'Pacific/Pago_Pago'),
  (436, 'Pacific/Palau'),
  (437, 'Pacific/Pitcairn'),
  (438, 'Pacific/Ponape'),
  (439, 'Pacific/Port_Moresby'),
  (440, 'Pacific/Rarotonga'),
  (441, 'Pacific/Saipan'),
  (442, 'Pacific/Samoa'),
  (443, 'Pacific/Tahiti'),
  (444, 'Pacific/Tarawa'),
  (445, 'Pacific/Tongatapu'),
  (446, 'Pacific/Truk'),
  (447, 'Pacific/Wake'),
  (448, 'Pacific/Wallis'),
  (449, 'Pacific/Yap'),
  (450, 'Brazil/Acre'),
  (451, 'Brazil/DeNoronha'),
  (452, 'Brazil/East'),
  (453, 'Brazil/West'),
  (454, 'Canada/Atlantic'),
  (455, 'Canada/Central'),
  (456, 'Canada/East-Saskatchewan'),
  (457, 'Canada/Eastern'),
  (458, 'Canada/Mountain'),
  (459, 'Canada/Newfoundland'),
  (460, 'Canada/Pacific'),
  (461, 'Canada/Saskatchewan'),
  (462, 'Canada/Yukon'),
  (463, 'CET'),
  (464, 'Chile/Continental'),
  (465, 'Chile/EasterIsland'),
  (466, 'CST6CDT'),
  (467, 'Cuba'),
  (468, 'EET'),
  (469, 'Egypt'),
  (470, 'Eire'),
  (471, 'EST'),
  (472, 'EST5EDT'),
  (473, 'Etc/GMT'),
  (474, 'Etc/GMT+0'),
  (475, 'Etc/GMT+1'),
  (476, 'Etc/GMT+10'),
  (477, 'Etc/GMT+11'),
  (478, 'Etc/GMT+12'),
  (479, 'Etc/GMT+2'),
  (480, 'Etc/GMT+3'),
  (481, 'Etc/GMT+4'),
  (482, 'Etc/GMT+5'),
  (483, 'Etc/GMT+6'),
  (484, 'Etc/GMT+7'),
  (485, 'Etc/GMT+8'),
  (486, 'Etc/GMT+9'),
  (487, 'Etc/GMT-0'),
  (488, 'Etc/GMT-1'),
  (489, 'Etc/GMT-10'),
  (490, 'Etc/GMT-11'),
  (491, 'Etc/GMT-12'),
  (492, 'Etc/GMT-13'),
  (493, 'Etc/GMT-14'),
  (494, 'Etc/GMT-2'),
  (495, 'Etc/GMT-3'),
  (496, 'Etc/GMT-4'),
  (497, 'Etc/GMT-5'),
  (498, 'Etc/GMT-6'),
  (499, 'Etc/GMT-7'),
  (500, 'Etc/GMT-8'),
  (501, 'Etc/GMT-9'),
  (502, 'Etc/GMT0'),
  (503, 'Etc/Greenwich'),
  (504, 'Etc/UCT'),
  (505, 'Etc/Universal'),
  (506, 'Etc/UTC'),
  (507, 'Etc/Zulu'),
  (508, 'Factory'),
  (509, 'GB'),
  (510, 'GB-Eire'),
  (511, 'GMT'),
  (512, 'GMT+0'),
  (513, 'GMT-0'),
  (514, 'GMT0'),
  (515, 'Greenwich'),
  (516, 'Hongkong'),
  (517, 'HST'),
  (518, 'Iceland'),
  (519, 'Iran'),
  (520, 'Israel'),
  (521, 'Jamaica'),
  (522, 'Japan'),
  (523, 'Kwajalein'),
  (524, 'Libya'),
  (525, 'MET'),
  (526, 'Mexico/BajaNorte'),
  (527, 'Mexico/BajaSur'),
  (528, 'Mexico/General'),
  (529, 'MST'),
  (530, 'MST7MDT'),
  (531, 'Navajo'),
  (532, 'NZ'),
  (533, 'NZ-CHAT'),
  (534, 'Poland'),
  (535, 'Portugal'),
  (536, 'PRC'),
  (537, 'PST8PDT'),
  (538, 'ROC'),
  (539, 'ROK'),
  (540, 'Singapore'),
  (541, 'Turkey'),
  (542, 'UCT'),
  (543, 'Universal'),
  (544, 'US/Alaska'),
  (545, 'US/Aleutian'),
  (546, 'US/Arizona'),
  (547, 'US/Central'),
  (548, 'US/East-Indiana'),
  (549, 'US/Eastern'),
  (550, 'US/Hawaii'),
  (551, 'US/Indiana-Starke'),
  (552, 'US/Michigan'),
  (553, 'US/Mountain'),
  (554, 'US/Pacific'),
  (555, 'US/Pacific-New'),
  (556, 'US/Samoa'),
  (557, 'UTC'),
  (558, 'W-SU'),
  (559, 'WET'),
  (560, 'Zulu');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmcategoryproducts`
--

CREATE TABLE IF NOT EXISTS `ps_tmcategoryproducts` (
  `id_tab`       INT(11) NOT NULL AUTO_INCREMENT,
  `category`     INT(11) NOT NULL,
  `num`          INT(11) NOT NULL,
  `mode`         INT(11) NOT NULL,
  `status`       INT(11) NOT NULL,
  `use_carousel` INT(11) NOT NULL,
  PRIMARY KEY (`id_tab`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_tmcategoryproducts`
--

INSERT INTO `ps_tmcategoryproducts` (`id_tab`, `category`, `num`, `mode`, `status`, `use_carousel`) VALUES
  (1, 2, 12, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmcategoryproducts_shop`
--

CREATE TABLE IF NOT EXISTS `ps_tmcategoryproducts_shop` (
  `id_tab`  INT(11) NOT NULL,
  `id_shop` INT(11) NOT NULL,
  PRIMARY KEY (`id_tab`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_tmcategoryproducts_shop`
--

INSERT INTO `ps_tmcategoryproducts_shop` (`id_tab`, `id_shop`) VALUES
  (1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmhtmlcontent`
--

CREATE TABLE IF NOT EXISTS `ps_tmhtmlcontent` (
  `id_item`        INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_shop`        INT(10) UNSIGNED    NOT NULL,
  `id_lang`        INT(10) UNSIGNED    NOT NULL,
  `item_order`     INT(10) UNSIGNED    NOT NULL,
  `title`          VARCHAR(100)                 DEFAULT NULL,
  `title_use`      TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `hook`           VARCHAR(100)                 DEFAULT NULL,
  `specific_class` VARCHAR(100)                 DEFAULT NULL,
  `url`            TEXT,
  `target`         TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `image`          VARCHAR(100)                 DEFAULT NULL,
  `image_w`        VARCHAR(10)                  DEFAULT NULL,
  `image_h`        VARCHAR(10)                  DEFAULT NULL,
  `html`           TEXT,
  `active`         TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_item`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_tmhtmlcontent`
--

INSERT INTO `ps_tmhtmlcontent` (`id_item`, `id_shop`, `id_lang`, `item_order`, `title`, `title_use`, `hook`, `specific_class`, `url`, `target`, `image`, `image_w`, `image_h`, `html`, `active`)
VALUES
  (1, 1, 1, 1, 'Oční klinika', 0, 'home', '', '', 0, '', '0', '0',
   '<h2>Oční klinika Pardubice</h2>\r\n<h4>Na oční klinice Vám provedeme komplexní vyšetření zraku, konzultace a přípavu k odstranění dioptrických vad, operaci šedého zákalu, léčbě onemocnění sítnice a odstranění zákalků sklivce.</h4>\r\n<a href="http://www.oftex.cz/ocni-klinika" target="_blank">Objednat se</a>\r\n\r\n',
   1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmediaparallax`
--

CREATE TABLE IF NOT EXISTS `ps_tmmediaparallax` (
  `id_item`    INT(11) NOT NULL AUTO_INCREMENT,
  `id_shop`    INT(11) NOT NULL,
  `selector`   VARCHAR(100)     DEFAULT NULL,
  `inverse`    INT(11) NOT NULL,
  `offset`     INT(11) NOT NULL,
  `speed`      FLOAT            DEFAULT NULL,
  `fade`       INT(11) NOT NULL,
  `full_width` INT(11) NOT NULL,
  `active`     INT(11) NOT NULL,
  PRIMARY KEY (`id_item`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_tmmediaparallax`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmediaparallax_layouts`
--

CREATE TABLE IF NOT EXISTS `ps_tmmediaparallax_layouts` (
  `id_layout`      INT(11) NOT NULL AUTO_INCREMENT,
  `id_parent`      INT(11) NOT NULL,
  `fade`           INT(11) NOT NULL,
  `speed`          FLOAT            DEFAULT NULL,
  `sort_order`     INT(11) NOT NULL,
  `type`           VARCHAR(100)     DEFAULT NULL,
  `inverse`        INT(11) NOT NULL,
  `offset`         INT(11) NOT NULL,
  `image`          VARCHAR(100)     DEFAULT NULL,
  `video_mp4`      VARCHAR(100)     DEFAULT NULL,
  `video_webm`     VARCHAR(100)     DEFAULT NULL,
  `active`         INT(11) NOT NULL,
  `video_link`     VARCHAR(100)     DEFAULT NULL,
  `specific_class` VARCHAR(100)     DEFAULT NULL,
  PRIMARY KEY (`id_layout`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_tmmediaparallax_layouts`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmediaparallax_layouts_lang`
--

CREATE TABLE IF NOT EXISTS `ps_tmmediaparallax_layouts_lang` (
  `id_layout` INT(11) NOT NULL,
  `id_lang`   INT(11) NOT NULL,
  `content`   TEXT    NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_tmmediaparallax_layouts_lang`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegalayout`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegalayout` (
  `id_layout`   INT(11) NOT NULL AUTO_INCREMENT,
  `id_hook`     INT(11) NOT NULL,
  `id_shop`     INT(11) NOT NULL,
  `layout_name` VARCHAR(100)     DEFAULT NULL,
  `status`      INT(11) NOT NULL,
  PRIMARY KEY (`id_layout`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 12;

--
-- Vypisuji data pro tabulku `ps_tmmegalayout`
--

INSERT INTO `ps_tmmegalayout` (`id_layout`, `id_hook`, `id_shop`, `layout_name`, `status`) VALUES
  (1, 26, 1, 'Footer1', 0),
  (2, 26, 1, 'Footer2', 0),
  (3, 26, 1, 'Footer3', 1),
  (4, 9, 1, 'Home1', 0),
  (5, 9, 1, 'Home2', 0),
  (6, 9, 1, 'Home3', 0),
  (7, 98, 1, 'TopColumn1', 0),
  (8, 98, 1, 'TopColumn2', 0),
  (9, 98, 1, 'TopColumn3', 0),
  (10, 9, 1, 'Home-Oftex', 1),
  (11, 98, 1, 'Top-Oftex', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegalayout_items`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegalayout_items` (
  `id_item`        INT(11) NOT NULL AUTO_INCREMENT,
  `id_layout`      INT(11) NOT NULL,
  `id_parent`      INT(11) NOT NULL,
  `type`           VARCHAR(100)     DEFAULT NULL,
  `sort_order`     INT(11) NOT NULL,
  `specific_class` VARCHAR(100)     DEFAULT NULL,
  `col_xs`         VARCHAR(100)     DEFAULT NULL,
  `col_sm`         VARCHAR(100)     DEFAULT NULL,
  `col_md`         VARCHAR(100)     DEFAULT NULL,
  `col_lg`         VARCHAR(100)     DEFAULT NULL,
  `module_name`    VARCHAR(100)     DEFAULT NULL,
  `id_unique`      VARCHAR(100)     DEFAULT NULL,
  PRIMARY KEY (`id_item`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 137;

--
-- Vypisuji data pro tabulku `ps_tmmegalayout_items`
--

INSERT INTO `ps_tmmegalayout_items` (`id_item`, `id_layout`, `id_parent`, `type`, `sort_order`, `specific_class`, `col_xs`, `col_sm`, `col_md`, `col_lg`, `module_name`, `id_unique`)
VALUES
  (1, 1, 0, 'wrapper', 1, 'container border-top', '', '', '', '', '', 'it_AMZZQGVZOSFK'),
  (2, 1, 1, 'row', 1, '', '', '', '', '', '', 'it_HPZIDMPXTNFK'),
  (3, 1, 2, 'col', 1, '', 'col-xs-12', 'col-sm-6', 'col-md-3', '', '', 'it_VALBJMKYQBBC'),
  (4, 1, 3, 'module', 1, '', '', '', '', '', 'blockcategories', 'it_LRJHWFXPEXLM'),
  (5, 1, 2, 'col', 2, '', 'col-xs-12', 'col-sm-6', 'col-md-3', '', '', 'it_FVVGDTMUECLO'),
  (6, 1, 5, 'module', 1, '', '', '', '', '', 'blockmyaccountfooter', 'it_EZWMTYHLYZUM'),
  (7, 1, 2, 'col', 3, '', 'col-xs-12', 'col-sm-6', 'col-md-3', '', '', 'it_GVNUGIPYWWMK'),
  (8, 1, 7, 'module', 1, '', '', '', '', '', 'blocknewsletter', 'it_ENCMUSYCCGRT'),
  (9, 1, 2, 'col', 4, '', 'col-xs-12', 'col-sm-6', 'col-md-3', '', '', 'it_SSXOJESVZPTI'),
  (10, 1, 9, 'module', 1, '', '', '', '', '', 'tmsocialfeeds', 'it_XGMMBZNXLGTC'),
  (11, 1, 0, 'row', 2, '', '', '', '', '', '', 'it_GHIYCYDDPAYF'),
  (12, 1, 11, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_AQCKJOPBGKKM'),
  (13, 1, 12, 'module', 1, '', '', '', '', '', 'blocklayered', 'it_MUEJXELWXACF'),
  (14, 1, 12, 'module', 2, '', '', '', '', '', 'statsdata', 'it_DNRXLSEMIEUJ'),
  (15, 1, 12, 'module', 3, '', '', '', '', '', 'themeconfigurator', 'it_OWEMJEXKTOLB'),
  (16, 1, 12, 'module', 4, '', '', '', '', '', 'tmolarkchat', 'it_OTXBUGVBORZE'),
  (17, 1, 12, 'module', 5, '', '', '', '', '', 'tmnewsletter', 'it_JCQAISGVOISK'),
  (18, 2, 0, 'wrapper', 1, 'container', '', '', '', '', '', 'it_DGGNFAWQWKHY'),
  (19, 2, 18, 'row', 1, '', '', '', '', '', '', 'it_MLRYDSRSNDRW'),
  (20, 2, 19, 'col', 1, '', 'col-xs-12', 'col-sm-6', 'col-md-3', '', '', 'it_XAKTPCQAHRJI'),
  (21, 2, 20, 'module', 1, '', '', '', '', '', 'blockcategories', 'it_ZWBNQEQSUWLV'),
  (22, 2, 19, 'col', 2, '', 'col-xs-12', 'col-sm-6', 'col-md-3', '', '', 'it_HZLUNIGSHUIS'),
  (23, 2, 22, 'module', 1, '', '', '', '', '', 'blockcontactinfos', 'it_UPRIJXUEJXUI'),
  (24, 2, 19, 'col', 3, '', 'col-xs-12', 'col-sm-6', 'col-md-3', '', '', 'it_RXLSERYOWCVS'),
  (25, 2, 24, 'module', 1, '', '', '', '', '', 'tmsocialfeeds', 'it_BSJSXHYYRMCM'),
  (26, 2, 19, 'col', 4, '', 'col-xs-12', 'col-sm-6', 'col-md-3', '', '', 'it_MQRRILWTTJKJ'),
  (27, 2, 26, 'module', 1, '', '', '', '', '', 'blocknewsletter', 'it_AXMOVMINVFMX'),
  (28, 2, 0, 'row', 2, '', '', '', '', '', '', 'it_YHKDNNXDUBDY'),
  (29, 2, 28, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_UYHTUCRFPKXT'),
  (30, 2, 29, 'module', 1, '', '', '', '', '', 'blocklayered', 'it_VORCIAGURSAW'),
  (31, 2, 29, 'module', 2, '', '', '', '', '', 'statsdata', 'it_OEBHMJXRQMDE'),
  (32, 2, 29, 'module', 3, '', '', '', '', '', 'themeconfigurator', 'it_YKXJUCGPCPJA'),
  (33, 2, 29, 'module', 4, '', '', '', '', '', 'tmolarkchat', 'it_LZEBUTYDCVEI'),
  (34, 2, 29, 'module', 5, '', '', '', '', '', 'tmnewsletter', 'it_PKEXADBQTSRS'),
  (35, 3, 0, 'wrapper', 1, 'container', '', '', '', '', '', 'it_ODORCGGPOZCF'),
  (36, 3, 35, 'row', 1, '', '', '', '', '', '', 'it_FWCZZILTCLCL'),
  (37, 3, 36, 'col', 1, '', 'col-xs-12', 'col-sm-3', '', '', '', 'it_MVPWMKGHACLX'),
  (38, 3, 37, 'module', 1, '', '', '', '', '', 'blockcms', 'it_KYXXGHFLNBRB'),
  (39, 3, 36, 'col', 2, '', 'col-xs-12', 'col-sm-3', '', '', '', 'it_LWHCNAAGCHQM'),
  (40, 3, 39, 'module', 1, '', '', '', '', '', 'blockcategories', 'it_RGZIMBVLRYBX'),
  (41, 3, 36, 'col', 3, '', 'col-xs-12', 'col-sm-3', '', '', '', 'it_TQVNTLMPQODW'),
  (42, 3, 41, 'module', 1, '', '', '', '', '', 'blockmyaccountfooter', 'it_JYDNZVQSIVKJ'),
  (43, 3, 36, 'col', 4, '', 'col-xs-12', 'col-sm-3', '', '', '', 'it_EUDYSYKIGGNC'),
  (44, 3, 43, 'module', 1, '', '', '', '', '', 'blockcontactinfos', 'it_KCAVHNHMEUED'),
  (45, 3, 0, 'row', 2, '', '', '', '', '', '', 'it_XMFEJPMCXAUY'),
  (46, 3, 45, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_CSIPNERLGFFF'),
  (47, 3, 46, 'module', 1, '', '', '', '', '', 'blocklayered', 'it_ZYYBOHYPROWR'),
  (48, 3, 46, 'module', 2, '', '', '', '', '', 'statsdata', 'it_FNBTWJVXYZVI'),
  (49, 3, 46, 'module', 3, '', '', '', '', '', 'themeconfigurator', 'it_ROQQQTJDUMPN'),
  (50, 3, 46, 'module', 4, '', '', '', '', '', 'tmolarkchat', 'it_UZPSEQXBLLHV'),
  (51, 3, 46, 'module', 5, '', '', '', '', '', 'tmnewsletter', 'it_KAHYEEZKIEBP'),
  (52, 4, 0, 'row', 1, '', '', '', '', '', '', 'it_BUUAKWROBDUQ'),
  (53, 4, 52, 'col', 1, 'banner-background', 'col-xs-12', '', '', '', '', 'it_XZZSFUQINCHV'),
  (54, 4, 53, 'module', 1, '', '', '', '', '', 'tmhtmlcontent', 'it_BFWYLRCHYYRT'),
  (55, 4, 52, 'col', 2, 'parallax_banner-2', 'col-xs-12', '', '', '', '', 'it_YYQOBZFIEAKE'),
  (56, 4, 55, 'module', 1, '', '', '', '', '', 'themeconfigurator', 'it_LIYCWHNGAEVA'),
  (57, 4, 0, 'wrapper', 2, 'container', '', '', '', '', '', 'it_IACUUBFFLZTG'),
  (58, 4, 57, 'row', 1, '', '', '', '', '', '', 'it_KHSTIKIWSBKW'),
  (59, 4, 58, 'col', 2, '', 'col-xs-12', '', '', '', '', 'it_HWHQYLEMUMQL'),
  (60, 4, 59, 'module', 1, '', '', '', '', '', 'smartbloghomelatestnews', 'it_FSHTEIAAHKSB'),
  (61, 5, 0, 'wrapper', 1, 'gray-background offset-top', '', '', '', '', '', 'it_KAUPIBBUAFOB'),
  (62, 5, 61, 'row', 1, 'container', '', '', '', '', '', 'it_NJRHBSAFULMU'),
  (63, 5, 62, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_HGBOJURTMIRL'),
  (64, 5, 63, 'module', 1, '', '', '', '', '', 'tmproductsslider', 'it_AMWIUXBROGEH'),
  (65, 5, 0, 'wrapper', 2, 'container', '', '', '', '', '', 'it_ZCMZSTSEWLOG'),
  (66, 5, 65, 'row', 1, '', '', '', '', '', '', 'it_YWPBJQNBKFKZ'),
  (67, 5, 66, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_RPUQSKWWLQAO'),
  (68, 5, 67, 'module', 1, '', '', '', '', '', 'tmcategoryproducts', 'it_DLZWYLNVTAIR'),
  (69, 5, 0, 'row', 3, '', '', '', '', '', '', 'it_VPXMXCSKAGOI'),
  (70, 5, 69, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_GUZHKAJNCDTS'),
  (71, 5, 70, 'module', 1, '', '', '', '', '', 'tmhtmlcontent', 'it_BXVUVJRHKBQB'),
  (72, 5, 0, 'wrapper', 4, 'container', '', '', '', '', '', 'it_HGAGXCEWTCSQ'),
  (73, 5, 72, 'row', 1, '', '', '', '', '', '', 'it_PCVGTHDTHRNS'),
  (74, 5, 73, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_AODPYMNXEMUL'),
  (75, 5, 74, 'module', 1, '', '', '', '', '', 'smartbloghomelatestnews', 'it_ICZHQHTVGZDA'),
  (76, 6, 0, 'row', 1, '', '', '', '', '', '', 'it_JDJJYBRQBIFI'),
  (77, 6, 76, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_IKHOCGVLUKCM'),
  (78, 6, 77, 'module', 1, '', '', '', '', '', 'tmhtmlcontent', 'it_MULFGVIXWRVD'),
  (79, 6, 0, 'wrapper', 2, 'gray-background', '', '', '', '', '', 'it_BTUYLZRNKULT'),
  (80, 6, 79, 'row', 1, 'container', '', '', '', '', '', 'it_OQHBVRWBMPKE'),
  (81, 6, 80, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_INVDPFBODGHR'),
  (82, 6, 81, 'module', 1, '', '', '', '', '', 'tmproductsslider', 'it_ZJAIHWEVWTXL'),
  (83, 6, 0, 'wrapper', 3, 'container', '', '', '', '', '', 'it_ATOIQZSXLBFL'),
  (84, 6, 83, 'row', 1, '', '', '', '', '', '', 'it_UHMWTHQNKMMX'),
  (85, 6, 84, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_LEBFHKHDKQTT'),
  (86, 6, 85, 'module', 1, '', '', '', '', '', 'tmcategoryproducts', 'it_IXLFOPQJPPVR'),
  (87, 6, 0, 'row', 4, '', '', '', '', '', '', 'it_RCQBSJASLQKU'),
  (88, 6, 87, 'col', 1, 'home-blog-wrapper', 'col-xs-12', '', '', '', '', 'it_TOGOTRUAERAO'),
  (89, 6, 88, 'module', 1, '', '', '', '', '', 'smartbloghomelatestnews', 'it_WBEAFJWTKQRS'),
  (90, 6, 0, 'row', 5, '', '', '', '', '', '', 'it_AYFEGHBCLEAW'),
  (91, 6, 90, 'col', 1, '', '', '', '', 'col-lg-12', '', 'it_FYYQZTPVCWQQ'),
  (92, 6, 91, 'module', 1, '', '', '', '', '', 'tmgooglemap', 'it_HYTUMILSHKEJ'),
  (93, 7, 0, 'row', 1, 'row-gutter', '', '', '', '', '', 'it_FBBAMRCMTZXY'),
  (94, 7, 93, 'col', 1, 'row-slider-extra', 'col-xs-12', '', '', '', '', 'it_TCOUGFPMDCOI'),
  (95, 7, 94, 'module', 1, '', '', '', '', '', 'homeslider', 'it_UMVUXQHTWRMQ'),
  (96, 7, 93, 'col', 2, 'banner-wrapper banner-row-extra', 'col-xs-12', '', '', '', '', 'it_SXUXIHZAEEMT'),
  (97, 7, 96, 'module', 1, '', '', '', '', '', 'themeconfigurator', 'it_ZEBMZTJJKAMH'),
  (98, 8, 0, 'row', 1, 'row-gutter', '', '', '', '', '', 'it_XFIIVWQPYZZO'),
  (99, 8, 98, 'col', 1, 'slider-wrapper', 'col-xs-12', '', '', 'col-lg-7', '', 'it_OYIVYNDLAPBL'),
  (100, 8, 99, 'module', 1, '', '', '', '', '', 'homeslider', 'it_PECEJTHQHWMX'),
  (101, 8, 98, 'col', 2, '', 'col-xs-12', '', '', 'col-lg-5', '', 'it_UGOFPEUMYCTY'),
  (102, 8, 101, 'module', 1, '', '', '', '', '', 'themeconfigurator', 'it_PKQIDSNFBOBK'),
  (103, 9, 0, 'row', 1, '', '', '', '', '', '', 'it_XKFUEYRZWRLU'),
  (104, 9, 103, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_SWBAQWBXJMOG'),
  (105, 9, 104, 'module', 1, '', '', '', '', '', 'homeslider', 'it_QLLPENVXYKSQ'),
  (106, 9, 0, 'wrapper', 2, 'container', '', '', '', '', '', 'it_SXHESSBPSIUQ'),
  (107, 9, 106, 'row', 1, '', '', '', '', '', '', 'it_GVJOCQKNXJRU'),
  (108, 9, 107, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_YKHCVQWPPGOE'),
  (109, 9, 108, 'module', 1, '', '', '', '', '', 'tmhtmlcontent', 'it_GEEBLPBUDMUP'),
  (113, 10, 0, 'row', 1, '', '', '', '', '', '', 'it_HLUJHQGJYGRA'),
  (114, 10, 113, 'col', 1, 'banner-background', 'col-xs-12', '', '', '', '', 'it_LLRAPPBZDDLA'),
  (115, 10, 114, 'module', 1, '', '', '', '', '', 'tmhtmlcontent', 'it_GXRRVYWYMNDI'),
  (116, 10, 0, 'wrapper', 2, 'container', '', '', '', '', '', 'it_WPOQMBTVLQSR'),
  (117, 10, 116, 'row', 1, '', '', '', '', '', '', 'it_ENSVEQDIFOBH'),
  (118, 10, 117, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_TCYMMDXXYOVI'),
  (119, 10, 118, 'module', 1, '', '', '', '', '', 'smartbloghomelatestnews', 'it_DEBJLORYWCLL'),
  (120, 10, 0, 'wrapper', 3, '', '', '', '', '', '', 'it_WQWQVXNYUAIZ'),
  (121, 10, 120, 'row', 1, '', '', '', '', '', '', 'it_ICRMGCCRRQSE'),
  (122, 10, 121, 'col', 1, '', 'col-xs-12', '', '', '', '', 'it_MVXTPMQSVHXG'),
  (128, 10, 122, 'module', 1, '', '', '', '', '', 'tmgooglemap', 'it_WIRDXAPFBFZV'),
  (130, 11, 0, 'row', 1, 'row-gutter', '', '', '', '', '', 'it_PJTXCZZXDKUD'),
  (131, 11, 130, 'col', 1, 'row-slider-extra', 'col-xs-12', '', '', '', '', 'it_FZYOFGPAOJCW'),
  (132, 11, 131, 'module', 1, '', '', '', '', '', 'homeslider', 'it_RFWNPBMHOHLT'),
  (133, 11, 0, 'wrapper', 2, '', '', '', '', '', '', 'it_DUNKZGOQCLYH'),
  (134, 11, 133, 'row', 1, '', '', '', '', '', '', 'it_MVITRLNABGXU'),
  (135, 11, 134, 'col', 1, 'sd', 'col-xs-2', '', '', '', '', 'it_FZXZAJQAIRRO'),
  (136, 11, 135, 'module', 1, '', '', '', '', '', 'tmcategoryproducts', 'it_QSABDOSYCQLS');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegamenu`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegamenu` (
  `id_item`        INT(11) NOT NULL AUTO_INCREMENT,
  `id_shop`        INT(11) NOT NULL DEFAULT '1',
  `sort_order`     INT(11) NOT NULL DEFAULT '0',
  `specific_class` VARCHAR(100)     DEFAULT NULL,
  `is_mega`        INT(11) NOT NULL DEFAULT '0',
  `is_simple`      INT(11) NOT NULL DEFAULT '0',
  `is_custom_url`  INT(11) NOT NULL DEFAULT '0',
  `url`            VARCHAR(100)     DEFAULT NULL,
  `active`         INT(11) NOT NULL DEFAULT '1',
  `unique_code`    VARCHAR(100)     DEFAULT NULL,
  PRIMARY KEY (`id_item`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 6;

--
-- Vypisuji data pro tabulku `ps_tmmegamenu`
--

INSERT INTO `ps_tmmegamenu` (`id_item`, `id_shop`, `sort_order`, `specific_class`, `is_mega`, `is_simple`, `is_custom_url`, `url`, `active`, `unique_code`)
VALUES
  (1, 1, 2, '', 0, 0, 0, 'CAT26', 1, 'it_49252376'),
  (2, 1, 1, '', 1, 0, 0, 'CAT3', 1, 'it_29338515'),
  (3, 1, 3, '', 0, 1, 0, 'CAT12', 1, 'it_96319154'),
  (4, 1, 4, '', 0, 0, 0, 'CAT22', 1, 'it_39966516'),
  (5, 1, 5, '', 0, 0, 0, 'CMS_CAT2', 1, 'it_38093594');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegamenu_banner`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegamenu_banner` (
  `id_item`        INT(11) NOT NULL AUTO_INCREMENT,
  `id_shop`        INT(11) NOT NULL DEFAULT '1',
  `specific_class` VARCHAR(100)     DEFAULT NULL,
  `blank`          INT(11) NOT NULL,
  PRIMARY KEY (`id_item`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_tmmegamenu_banner`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegamenu_banner_lang`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegamenu_banner_lang` (
  `id_item`      INT(10) UNSIGNED NOT NULL,
  `id_lang`      INT(11)          NOT NULL,
  `title`        VARCHAR(100) DEFAULT NULL,
  `url`          VARCHAR(100)     NOT NULL,
  `image`        VARCHAR(100)     NOT NULL,
  `public_title` VARCHAR(100) DEFAULT NULL,
  `description`  TEXT             NOT NULL,
  PRIMARY KEY (`id_item`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_tmmegamenu_banner_lang`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegamenu_html`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegamenu_html` (
  `id_item`        INT(11) NOT NULL AUTO_INCREMENT,
  `id_shop`        INT(11) NOT NULL DEFAULT '1',
  `specific_class` VARCHAR(100)     DEFAULT NULL,
  PRIMARY KEY (`id_item`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_tmmegamenu_html`
--

INSERT INTO `ps_tmmegamenu_html` (`id_item`, `id_shop`, `specific_class`) VALUES
  (1, 1, '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegamenu_html_lang`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegamenu_html_lang` (
  `id_item` INT(10) UNSIGNED NOT NULL,
  `id_lang` INT(11)          NOT NULL,
  `title`   VARCHAR(100) DEFAULT NULL,
  `content` TEXT             NOT NULL,
  PRIMARY KEY (`id_item`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_tmmegamenu_html_lang`
--

INSERT INTO `ps_tmmegamenu_html_lang` (`id_item`, `id_lang`, `title`, `content`) VALUES
  (1, 1, 'HTML', '<p>ahoj, to sem já</p>');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegamenu_items`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegamenu_items` (
  `id`       INT(11) NOT NULL AUTO_INCREMENT,
  `id_tab`   INT(11) NOT NULL,
  `row`      INT(11) NOT NULL DEFAULT '1',
  `col`      INT(11) NOT NULL DEFAULT '1',
  `width`    INT(11)          DEFAULT NULL,
  `class`    VARCHAR(100)     DEFAULT NULL,
  `type`     INT(11) NOT NULL DEFAULT '0',
  `is_mega`  INT(11) NOT NULL DEFAULT '0',
  `settings` VARCHAR(10000)   DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 102;

--
-- Vypisuji data pro tabulku `ps_tmmegamenu_items`
--

INSERT INTO `ps_tmmegamenu_items` (`id`, `id_tab`, `row`, `col`, `width`, `class`, `type`, `is_mega`, `settings`) VALUES
  (1, 1, 1, 1, NULL, NULL, 0, 0, 'CMS1,CMS2'),
  (2, 2, 1, 1, NULL, NULL, 0, 0, 'CAT3'),
  (88, 3, 1, 1, 4, '', 0, 1, 'CAT24'),
  (89, 3, 1, 2, 4, '', 0, 1, 'CAT23'),
  (90, 3, 1, 3, 4, '', 0, 1, ''),
  (91, 3, 1, 1, NULL, NULL, 0, 0, 'CAT24,CAT23'),
  (98, 2, 1, 2, 4, '', 0, 1, 'CAT20'),
  (99, 2, 1, 3, 4, '', 0, 1, 'CAT21'),
  (100, 2, 1, 4, 4, '', 0, 1, 'ALLMAN0'),
  (101, 2, 1, 5, 4, '', 0, 1, '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegamenu_lang`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegamenu_lang` (
  `id`      INT(11) NOT NULL AUTO_INCREMENT,
  `id_item` INT(11) NOT NULL,
  `id_lang` INT(11) NOT NULL,
  `title`   VARCHAR(100)     DEFAULT NULL,
  `badge`   VARCHAR(100)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 25;

--
-- Vypisuji data pro tabulku `ps_tmmegamenu_lang`
--

INSERT INTO `ps_tmmegamenu_lang` (`id`, `id_item`, `id_lang`, `title`, `badge`) VALUES
  (16, 1, 1, 'Výživa zraku', ''),
  (19, 4, 1, 'Vyšetření a aplikace', ''),
  (21, 3, 1, 'Příslušenství', ''),
  (22, 5, 1, 'Vše o nákupu', ''),
  (24, 2, 1, 'Kontaktní čočky', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegamenu_link`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegamenu_link` (
  `id_item`        INT(11) NOT NULL AUTO_INCREMENT,
  `id_shop`        INT(11) NOT NULL DEFAULT '1',
  `specific_class` VARCHAR(100)     DEFAULT NULL,
  `blank`          INT(11) NOT NULL,
  PRIMARY KEY (`id_item`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_tmmegamenu_link`
--

INSERT INTO `ps_tmmegamenu_link` (`id_item`, `id_shop`, `specific_class`, `blank`) VALUES
  (1, 1, '', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegamenu_link_lang`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegamenu_link_lang` (
  `id_item` INT(10) UNSIGNED NOT NULL,
  `id_lang` INT(11)          NOT NULL,
  `title`   VARCHAR(100) DEFAULT NULL,
  `url`     VARCHAR(255)     NOT NULL,
  PRIMARY KEY (`id_item`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_tmmegamenu_link_lang`
--

INSERT INTO `ps_tmmegamenu_link_lang` (`id_item`, `id_lang`, `title`, `url`) VALUES
  (1, 1, 'Odkaz', 'kontakt');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegamenu_map`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegamenu_map` (
  `id_item`   INT(11)      NOT NULL AUTO_INCREMENT,
  `id_shop`   INT(11)      NOT NULL DEFAULT '1',
  `latitude`  VARCHAR(100)          DEFAULT NULL,
  `longitude` VARCHAR(100)          DEFAULT NULL,
  `scale`     INT(11)      NOT NULL,
  `marker`    VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_item`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_tmmegamenu_map`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegamenu_map_lang`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegamenu_map_lang` (
  `id_item`     INT(10) UNSIGNED NOT NULL,
  `id_lang`     INT(11)          NOT NULL,
  `title`       VARCHAR(100) DEFAULT NULL,
  `description` TEXT             NOT NULL,
  PRIMARY KEY (`id_item`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_tmmegamenu_map_lang`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegamenu_video`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegamenu_video` (
  `id_item` INT(11) NOT NULL AUTO_INCREMENT,
  `id_shop` INT(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_item`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_tmmegamenu_video`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmmegamenu_video_lang`
--

CREATE TABLE IF NOT EXISTS `ps_tmmegamenu_video_lang` (
  `id_item` INT(10) UNSIGNED NOT NULL,
  `id_lang` INT(11)          NOT NULL,
  `title`   VARCHAR(100) DEFAULT NULL,
  `url`     VARCHAR(100)     NOT NULL,
  `type`    VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`id_item`, `id_lang`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_tmmegamenu_video_lang`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmnewsletter`
--

CREATE TABLE IF NOT EXISTS `ps_tmnewsletter` (
  `id_tmnewsletter` INT(11) NOT NULL AUTO_INCREMENT,
  `id_guest`        INT(11) NOT NULL,
  `id_user`         INT(11) NOT NULL,
  `id_shop`         INT(11) NOT NULL,
  `status`          INT(1)  NOT NULL,
  PRIMARY KEY (`id_tmnewsletter`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 7;

--
-- Vypisuji data pro tabulku `ps_tmnewsletter`
--

INSERT INTO `ps_tmnewsletter` (`id_tmnewsletter`, `id_guest`, `id_user`, `id_shop`, `status`) VALUES
  (1, 53, 0, 1, 0),
  (2, 71, 0, 1, 0),
  (3, 183, 0, 1, 0),
  (4, 188, 0, 1, 0),
  (5, 190, 0, 1, 0),
  (6, 210, 0, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmnewsletter_settings`
--

CREATE TABLE IF NOT EXISTS `ps_tmnewsletter_settings` (
  `id_tmnewsletter` INT(11) NOT NULL AUTO_INCREMENT,
  `id_shop`         INT(11) NOT NULL,
  `is_guest`        INT(11) NOT NULL,
  `verification`    INT(11) NOT NULL,
  `timeout`         FLOAT   NOT NULL,
  `ft_delay`        FLOAT   NOT NULL,
  `status`          INT(1)  NOT NULL,
  PRIMARY KEY (`id_tmnewsletter`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 3;

--
-- Vypisuji data pro tabulku `ps_tmnewsletter_settings`
--

INSERT INTO `ps_tmnewsletter_settings` (`id_tmnewsletter`, `id_shop`, `is_guest`, `verification`, `timeout`, `ft_delay`, `status`)
VALUES
  (1, 1, 0, 0, 1, 0.25, 1),
  (2, 1, 1, 0, 1, 0.25, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmnewsletter_settings_lang`
--

CREATE TABLE IF NOT EXISTS `ps_tmnewsletter_settings_lang` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `id_tmnewsletter` INT(11) NOT NULL,
  `id_lang`         INT(11) NOT NULL,
  `title`           VARCHAR(100)     DEFAULT NULL,
  `content`         TEXT    NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 3;

--
-- Vypisuji data pro tabulku `ps_tmnewsletter_settings_lang`
--

INSERT INTO `ps_tmnewsletter_settings_lang` (`id`, `id_tmnewsletter`, `id_lang`, `title`, `content`) VALUES
  (1, 1, 1, 'Subscribe to our newsletter',
   'Enter your email address to receive all news, updates on new arrivals, special offers and other discount information.'),
  (2, 2, 1, 'Subscribe to our newsletter',
   'Enter your email address to receive all news, updates on new arrivals, special offers and other discount information.');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmproductsslider_item`
--

CREATE TABLE IF NOT EXISTS `ps_tmproductsslider_item` (
  `id_slide`     INT(11)             NOT NULL AUTO_INCREMENT,
  `id_shop`      INT(11)             NOT NULL,
  `id_product`   INT(10)             NOT NULL,
  `slide_order`  INT(11)             NOT NULL,
  `slide_status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_slide`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_tmproductsslider_item`
--

INSERT INTO `ps_tmproductsslider_item` (`id_slide`, `id_shop`, `id_product`, `slide_order`, `slide_status`) VALUES
  (1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmproductsslider_settings`
--

CREATE TABLE IF NOT EXISTS `ps_tmproductsslider_settings` (
  `id_slider`            INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_shop`              INT(10) UNSIGNED    NOT NULL,
  `slider_width`         INT(10) UNSIGNED             DEFAULT '1170',
  `slider_type`          VARCHAR(255)                 DEFAULT 'fade',
  `slider_speed`         INT(10) UNSIGNED             DEFAULT '500',
  `slider_pause`         INT(10) UNSIGNED             DEFAULT '3000',
  `slider_loop`          TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `slider_pause_h`       TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `slider_pager`         TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `slider_controls`      TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `slider_auto_controls` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_slider`, `id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2;

--
-- Vypisuji data pro tabulku `ps_tmproductsslider_settings`
--

INSERT INTO `ps_tmproductsslider_settings` (`id_slider`, `id_shop`, `slider_width`, `slider_type`, `slider_speed`, `slider_pause`, `slider_loop`, `slider_pause_h`, `slider_pager`, `slider_controls`, `slider_auto_controls`)
VALUES
  (1, 1, 1170, 'fade', 500, 3000, 1, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmrelatedproducts`
--

CREATE TABLE IF NOT EXISTS `ps_tmrelatedproducts` (
  `id_shop`   INT(10) UNSIGNED NOT NULL,
  `id_master` INT(11)          NOT NULL,
  `id_slave`  INT(11)          NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_tmrelatedproducts`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_tmsocialfeed`
--

CREATE TABLE IF NOT EXISTS `ps_tmsocialfeed` (
  `id_item`         INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `id_shop`         INT(10) UNSIGNED    NOT NULL,
  `item_order`      INT(10) UNSIGNED    NOT NULL,
  `item_type`       VARCHAR(100)                 DEFAULT NULL,
  `hook`            VARCHAR(100)                 DEFAULT NULL,
  `item_theme`      VARCHAR(100)                 DEFAULT NULL,
  `item_width`      INT(10) UNSIGNED    NOT NULL,
  `item_height`     INT(10) UNSIGNED    NOT NULL,
  `item_limit`      INT(10) UNSIGNED    NOT NULL,
  `item_header`     TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `item_footer`     TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `item_border`     TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `item_replies`    TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `item_scroll`     TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `item_background` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `item_col_width`  INT(10) UNSIGNED    NOT NULL,
  `active`          TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_item`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_tmsocialfeed`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_warehouse`
--

CREATE TABLE IF NOT EXISTS `ps_warehouse` (
  `id_warehouse`    INT(11) UNSIGNED            NOT NULL AUTO_INCREMENT,
  `id_currency`     INT(11) UNSIGNED            NOT NULL,
  `id_address`      INT(11) UNSIGNED            NOT NULL,
  `id_employee`     INT(11) UNSIGNED            NOT NULL,
  `reference`       VARCHAR(32)                          DEFAULT NULL,
  `name`            VARCHAR(45)                 NOT NULL,
  `management_type` ENUM ('WA', 'FIFO', 'LIFO') NOT NULL DEFAULT 'WA',
  `deleted`         TINYINT(1) UNSIGNED         NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_warehouse`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_warehouse`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_warehouse_carrier`
--

CREATE TABLE IF NOT EXISTS `ps_warehouse_carrier` (
  `id_carrier`   INT(11) UNSIGNED NOT NULL,
  `id_warehouse` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_warehouse`, `id_carrier`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `id_carrier` (`id_carrier`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_warehouse_carrier`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_warehouse_product_location`
--

CREATE TABLE IF NOT EXISTS `ps_warehouse_product_location` (
  `id_warehouse_product_location` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product`                    INT(11) UNSIGNED NOT NULL,
  `id_product_attribute`          INT(11) UNSIGNED NOT NULL,
  `id_warehouse`                  INT(11) UNSIGNED NOT NULL,
  `location`                      VARCHAR(64)               DEFAULT NULL,
  PRIMARY KEY (`id_warehouse_product_location`),
  UNIQUE KEY `id_product` (`id_product`, `id_product_attribute`, `id_warehouse`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_warehouse_product_location`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_warehouse_shop`
--

CREATE TABLE IF NOT EXISTS `ps_warehouse_shop` (
  `id_shop`      INT(11) UNSIGNED NOT NULL,
  `id_warehouse` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_warehouse`, `id_shop`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_warehouse_shop`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_webservice_account`
--

CREATE TABLE IF NOT EXISTS `ps_webservice_account` (
  `id_webservice_account` INT(11)     NOT NULL AUTO_INCREMENT,
  `key`                   VARCHAR(32) NOT NULL,
  `description`           TEXT,
  `class_name`            VARCHAR(50) NOT NULL DEFAULT 'WebserviceRequest',
  `is_module`             TINYINT(2)  NOT NULL DEFAULT '0',
  `module_name`           VARCHAR(50)          DEFAULT NULL,
  `active`                TINYINT(2)  NOT NULL,
  PRIMARY KEY (`id_webservice_account`),
  KEY `key` (`key`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_webservice_account`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_webservice_account_shop`
--

CREATE TABLE IF NOT EXISTS `ps_webservice_account_shop` (
  `id_webservice_account` INT(11) UNSIGNED NOT NULL,
  `id_shop`               INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_webservice_account`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_webservice_account_shop`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_webservice_permission`
--

CREATE TABLE IF NOT EXISTS `ps_webservice_permission` (
  `id_webservice_permission` INT(11)                                       NOT NULL AUTO_INCREMENT,
  `resource`                 VARCHAR(50)                                   NOT NULL,
  `method`                   ENUM ('GET', 'POST', 'PUT', 'DELETE', 'HEAD') NOT NULL,
  `id_webservice_account`    INT(11)                                       NOT NULL,
  PRIMARY KEY (`id_webservice_permission`),
  UNIQUE KEY `resource_2` (`resource`, `method`, `id_webservice_account`),
  KEY `resource` (`resource`),
  KEY `method` (`method`),
  KEY `id_webservice_account` (`id_webservice_account`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_webservice_permission`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_web_browser`
--

CREATE TABLE IF NOT EXISTS `ps_web_browser` (
  `id_web_browser` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`           VARCHAR(64)               DEFAULT NULL,
  PRIMARY KEY (`id_web_browser`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 12;

--
-- Vypisuji data pro tabulku `ps_web_browser`
--

INSERT INTO `ps_web_browser` (`id_web_browser`, `name`) VALUES
  (1, 'Safari'),
  (2, 'Safari iPad'),
  (3, 'Firefox'),
  (4, 'Opera'),
  (5, 'IE 6'),
  (6, 'IE 7'),
  (7, 'IE 8'),
  (8, 'IE 9'),
  (9, 'IE 10'),
  (10, 'IE 11'),
  (11, 'Chrome');

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_wishlist`
--

CREATE TABLE IF NOT EXISTS `ps_wishlist` (
  `id_wishlist`   INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_customer`   INT(10) UNSIGNED NOT NULL,
  `token`         VARCHAR(64)      NOT NULL,
  `name`          VARCHAR(64)      NOT NULL,
  `counter`       INT(10) UNSIGNED          DEFAULT NULL,
  `id_shop`       INT(10) UNSIGNED          DEFAULT '1',
  `id_shop_group` INT(10) UNSIGNED          DEFAULT '1',
  `date_add`      DATETIME         NOT NULL,
  `date_upd`      DATETIME         NOT NULL,
  `default`       INT(10) UNSIGNED          DEFAULT '0',
  PRIMARY KEY (`id_wishlist`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_wishlist`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_wishlist_email`
--

CREATE TABLE IF NOT EXISTS `ps_wishlist_email` (
  `id_wishlist` INT(10) UNSIGNED NOT NULL,
  `email`       VARCHAR(128)     NOT NULL,
  `date_add`    DATETIME         NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_wishlist_email`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_wishlist_product`
--

CREATE TABLE IF NOT EXISTS `ps_wishlist_product` (
  `id_wishlist_product`  INT(10)          NOT NULL AUTO_INCREMENT,
  `id_wishlist`          INT(10) UNSIGNED NOT NULL,
  `id_product`           INT(10) UNSIGNED NOT NULL,
  `id_product_attribute` INT(10) UNSIGNED NOT NULL,
  `quantity`             INT(10) UNSIGNED NOT NULL,
  `priority`             INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_wishlist_product`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

--
-- Vypisuji data pro tabulku `ps_wishlist_product`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_wishlist_product_cart`
--

CREATE TABLE IF NOT EXISTS `ps_wishlist_product_cart` (
  `id_wishlist_product` INT(10) UNSIGNED NOT NULL,
  `id_cart`             INT(10) UNSIGNED NOT NULL,
  `quantity`            INT(10) UNSIGNED NOT NULL,
  `date_add`            DATETIME         NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_wishlist_product_cart`
--


-- --------------------------------------------------------

--
-- Struktura tabulky `ps_zone`
--

CREATE TABLE IF NOT EXISTS `ps_zone` (
  `id_zone` INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
  `name`    VARCHAR(64)         NOT NULL,
  `active`  TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_zone`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 9;

--
-- Vypisuji data pro tabulku `ps_zone`
--

INSERT INTO `ps_zone` (`id_zone`, `name`, `active`) VALUES
  (1, 'Europe', 1),
  (2, 'North America', 1),
  (3, 'Asia', 1),
  (4, 'Africa', 1),
  (5, 'Oceania', 1),
  (6, 'South America', 1),
  (7, 'Europe (non-EU)', 1),
  (8, 'Central America/Antilla', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ps_zone_shop`
--

CREATE TABLE IF NOT EXISTS `ps_zone_shop` (
  `id_zone` INT(11) UNSIGNED NOT NULL,
  `id_shop` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_zone`, `id_shop`),
  KEY `id_shop` (`id_shop`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Vypisuji data pro tabulku `ps_zone_shop`
--

INSERT INTO `ps_zone_shop` (`id_zone`, `id_shop`) VALUES
  (1, 1),
  (2, 1),
  (3, 1),
  (4, 1),
  (5, 1),
  (6, 1),
  (7, 1),
  (8, 1);
SET FOREIGN_KEY_CHECKS = 1;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
