<?php

/*
* ODEBRANO MCRYPT
*/

class RijndaelCore
{
    protected $_key;
    protected $_iv;

    public function __construct($key, $iv) {
        $this->_key = $key;
        $this->_iv = base64_decode($iv);
    }

    /**
     * Base64 is not required, but it is be more compact than urlencode
     * @param string $plaintext
     * @return bool|string
     */
    public function encrypt($plaintext) {
        $length = (ini_get('mbstring.func_overload') & 2) ? mb_strlen($plaintext, ini_get('default_charset')) : strlen($plaintext);

        if ($length >= 1048576) {
            return false;
        }

        return base64_encode($plaintext) . sprintf('%06d', $length);
    }

    public function decrypt($ciphertext) {
        if (ini_get('mbstring.func_overload') & 2) {
            $length = intval(mb_substr($ciphertext, -6, 6, ini_get('default_charset')));
            $ciphertext = mb_substr($ciphertext, 0, -6, ini_get('default_charset'));

            return mb_substr(
                base64_decode($ciphertext),
                0,
                $length,
                ini_get('default_charset')
            );
        } else {
            $length = intval(substr($ciphertext, -6));
            $ciphertext = substr($ciphertext, 0, -6);

            return substr(base64_decode($ciphertext), 0, $length);
        }
    }
}
