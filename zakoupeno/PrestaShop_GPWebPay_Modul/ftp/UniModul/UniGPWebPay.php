<?php
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#


require_once(dirname(__FILE__) . "/UniModul.php");
require_once(dirname(__FILE__) . "/muzo.php");

class UniGPWebPayConfig {
    var $urlMuzoCreateOrder;
    var $publicKeyFile;
    var $privateKeyFile;
    var $privateKeyPass;
    var $merchantNumbers;
    var $depositFlag;
    var $gwOrderNumberOffset;
    public $convertToCurrencyIfUnsupported; //jedna mena
}


class UniGPWebPay extends UniModul {
    public $uniModulProperties = array('HonorsShopOrderNumberIsAlpha' => true);

    public function __construct($configSetting, $subMethod) {
        parent::__construct("GPWebPay", $configSetting, $subMethod);
        $this->setConfigFromData($configSetting);
    }

    function setConfigFromData($configSetting) {
        $this->config = new UniGPWebPayConfig();
        if ($configSetting != null && $configSetting->configData != null) {
            $configData = $configSetting->configData;
            $this->config->urlMuzoCreateOrder = $configData['urlMuzoCreateOrder'];
            $this->config->privateKeyFile = $configData['privateKeyFile'];
            $this->config->privateKeyPass = $configData['privateKeyPass'];
            $this->config->merchantNumbers = array();
            if (isset($configData['merchantNumberCZK'])) {
                $this->config->merchantNumbers['CZK'] = $configData['merchantNumberCZK'];
            }
            if (isset($configData['merchantNumberEUR'])) {
                $this->config->merchantNumbers['EUR'] = $configData['merchantNumberEUR'];
            }
            if (isset($configData['merchantNumberUSD'])) {
                $this->config->merchantNumbers['USD'] = $configData['merchantNumberUSD'];
            }
            if (isset($configData['merchantNumberGBP'])) {
                $this->config->merchantNumbers['GBP'] = $configData['merchantNumberGBP'];
            }
            if (isset($configData['merchantNumberPLN'])) {
                $this->config->merchantNumbers['PLN'] = $configData['merchantNumberPLN'];
            }
            if (isset($configData['merchantNumberHUF'])) {
                $this->config->merchantNumbers['HUF'] = $configData['merchantNumberHUF'];
            }
            if (isset($configData['merchantNumberRUB'])) {
                $this->config->merchantNumbers['RUB'] = $configData['merchantNumberRUB'];
            }
            $this->config->depositFlag = $configData['depositFlag'];
            $this->config->publicKeyFile = $configData['publicKeyFile'];
            $this->config->gwOrderNumberOffset = $configData['gwOrderNumberOffset'];
            $this->config->convertToCurrencyIfUnsupported = $configSetting->configData['convertToCurrencyIfUnsupported'];
        }
    }

    function getConfigInfo($language = 'en') {

        $d = $this->dictionary;
        $d->setDefaultLanguage($language);

        $configInfo = new ConfigInfo();

        $configFields = array();

        $configField = new ConfigField();
        $configField->name = 'urlMuzoCreateOrder';
        $configField->type = ConfigFieldType::$text;
        $configField->label = $d->get('urlMuzoCreateOrder');
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'merchantNumberCZK';
        $configField->label = $d->get('merchantNumberCZK');
        $configField->type = ConfigFieldType::$text;
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'merchantNumberEUR';
        $configField->label = $d->get('merchantNumberEUR');
        $configField->type = ConfigFieldType::$text;
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'merchantNumberUSD';
        $configField->label = $d->get('merchantNumberUSD');
        $configField->type = ConfigFieldType::$text;
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'merchantNumberGBP';
        $configField->label = $d->get('merchantNumberGBP');
        $configField->type = ConfigFieldType::$text;
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'merchantNumberPLN';
        $configField->label = $d->get('merchantNumberPLN');
        $configField->type = ConfigFieldType::$text;
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'merchantNumberHUF';
        $configField->label = $d->get('merchantNumberHUF');
        $configField->type = ConfigFieldType::$text;
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'merchantNumberRUB';
        $configField->label = $d->get('merchantNumberRUB');
        $configField->type = ConfigFieldType::$text;
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'publicKeyFile';
        $configField->label = $d->get('publicKeyFile');
        $configField->type = ConfigFieldType::$text;
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'privateKeyFile';
        $configField->label = $d->get('privateKeyFile');
        $configField->type = ConfigFieldType::$text;
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'privateKeyPass';
        $configField->label = $d->get('privateKeyPass');
        $configField->type = ConfigFieldType::$text;
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'depositFlag';
        $configField->label = $d->get('depositFlag');
        $configField->type = ConfigFieldType::$choice;
        $configField->choiceItems = array(1 => $d->get('deposit_1'), 0 => $d->get('deposit_0'));
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'gwOrderNumberOffset';
        $configField->label = $d->get('gwOrderNumberOffset');
        $configField->type = ConfigFieldType::$text;
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'convertToCurrencyIfUnsupported';
        $configField->label = $d->get('convertToCurrencyIfUnsupported');
        $configField->type = ConfigFieldType::$text;
        $configFields[] = $configField;

        $configField = new ConfigField();
        $configField->name = 'orderStatusSuccessfull';
        $configField->label = $d->get('orderStatusSuccessfull');
        $configField->type = ConfigFieldType::$orderStatus;
        $configFields[] = $configField;


        $configInfo->configFields = $configFields;

        return $configInfo;
    }


    function queryPrePayGWInfo($orderToPayInfo) {
        $currencySupported = (isset($this->config->merchantNumbers[$orderToPayInfo->currency]) && trim($this->config->merchantNumbers[$orderToPayInfo->currency]) != '');
        list($isPossible, $newcur, $newtotal, $forexMessage, $forexNote, $orderReplyStatusFail) = $this->fixCurrency($orderToPayInfo, $currencySupported);

        $prePayGWInfo = new PrePayGWInfo();
        $prePayGWInfo->paymentMethodName = $this->dictionary->get('payment_method_name', $orderToPayInfo->language);

        $prePayGWInfo->isPossible = $isPossible;
        $prePayGWInfo->forexMessage = $forexMessage;

        return $prePayGWInfo;
    }

    function gatewayOrderRedirectAction($orderToPayInfo) {
        $currencySupported = (isset($this->config->merchantNumbers[$orderToPayInfo->currency]) && trim($this->config->merchantNumbers[$orderToPayInfo->currency]) != '');
        list($isPossible, $newcur, $newtotal, $forexMessage, $forexNote, $orderReplyStatusFail) = $this->fixCurrency($orderToPayInfo, $currencySupported);

        if (!$isPossible) {
            $transactionPK = $this->writeOrderToDb($orderToPayInfo->shopOrderNumber, $orderToPayInfo->shopPairingInfo, null, $forexNote, $orderReplyStatusFail->orderStatus, $orderToPayInfo->uniAdapterData);
            $this->logger->writeLog("CANNOT SEND ORDER " . print_r($orderToPayInfo, true) . "   resultText:" . $orderReplyStatusFail->resultText);

            $redirectActionFail = new RedirectAction();
            $redirectActionFail->orderReplyStatus = $orderReplyStatusFail;

            return $redirectActionFail;
        }

        $amount = round($newtotal * 100);

        $currencyCodes = array("CZK" => 203, "EUR" => 978, "GBP" => 826, "USD" => 840, "PLN" => 985, "HUF" => 348, "RUB" => 643);
        $currency = $currencyCodes[$newcur];
        $merchantNumber = $this->config->merchantNumbers[$newcur];

        $transactionPK = $this->writeOrderToDb($orderToPayInfo->shopOrderNumber, $orderToPayInfo->shopPairingInfo, null, $forexNote, null, $orderToPayInfo->uniAdapterData, $merchantNumber);
        $gwOrderNumber = $transactionPK + $this->config->gwOrderNumberOffset;
        $this->updateGwOrderNumber($transactionPK, $gwOrderNumber);

        $isNumericShopOrderNumber = preg_match('/^[0-9]+$/', $orderToPayInfo->shopOrderNumber);
        $merOrderNum = $isNumericShopOrderNumber ? $orderToPayInfo->shopOrderNumber : $gwOrderNumber;

        $description = '';
        if (!$isNumericShopOrderNumber && $orderToPayInfo->shopOrderNumber != '') {
            $description .= "(Ord.No.:" . $orderToPayInfo->shopOrderNumber . ") ";
        }
        $description .= $orderToPayInfo->description;
        require_once('toascii.php');
        $description = toASCII($description);
        $description = substr($description, 0, 255);

        $pth = dirname(__FILE__) . "/certs/";
        $orderUrl = muzo_CreateOrderExt(    // funkce presmeruje browser s pozadavkem na server Muzo
            $this->config->urlMuzoCreateOrder,      // adresa kam posilat pozadavek do Muzo
            $orderToPayInfo->replyUrl,                // adresa kam ma Muzo presmerovat odpoved
            $pth . $this->config->privateKeyFile,          // soubor s privatnim klicem
            $this->config->privateKeyPass,          // heslo privatniho klice
            $merchantNumber,          // cislo obchodnika
            $gwOrderNumber,             // cislo objednavky
            $amount,                  // hodnota objednavky v halerich
            $currency,                // kod meny, CZK..203, EUR..978, GBP..826, USD..840, povolene meny zalezi na smlouve s bankou
            $this->config->depositFlag,             // uhrada okamzite "1", nebo uhrada az z admin rozhrani
            $merOrderNum,             // identifikace objednavky pro obchodnika
            $description,             // popis nakupu, pouze ASCII
            "X",                     // data obchodnika, pouze ASCII
            $orderToPayInfo->language
        );
        $this->logger->writeLog("MAKING_ORDER_URL " . $orderUrl . "   " . $_SERVER['REMOTE_ADDR'] . " " . $_SERVER['REQUEST_URI']);

        $redirectAction = new RedirectAction();
        $redirectAction->redirectUrl = $orderUrl;

        return $redirectAction;
    }

    public function gatewayReceiveReply($language = 'en') {
        $pth = dirname(__FILE__) . "/certs/";

        // jen pro zjisteni $gwOrderNumber bez overeni podpisu, pro nasledne ziskani merchantNumber a kontrolu podpisu, lepsi by byla spesl fce na to v knihovne, ale takto se to stejne neloguje takze ok
        muzo_ReceiveReply($pth . $this->config->publicKeyFile, $gwOrderNumber, $merOrderNum, $md, $prCode, $srCode, $resultText);
        $transactionRecord = $this->getOrderTransactionRecordFromDbUnique($gwOrderNumber);
        $merchantNumber = $transactionRecord->uniModulData;

        $sigValid = muzo_ReceiveReply($pth . $this->config->publicKeyFile, $gwOrderNumber, $merOrderNum, $md, $prCode, $srCode, $resultText, $merchantNumber);
        $paymentOk = ($sigValid && $prCode == 0 && $srCode == 0);

        $this->logger->writeLog("REPLY Signature result=" . $paymentOk . " " . ($sigValid ? "OK" : "INVALID") . "  " . $_SERVER['REMOTE_ADDR'] . " " . $_SERVER['REQUEST_URI']);


        if ($paymentOk) {
            $errMsg = null;
        } else {
            $errClass = classify_error($sigValid, $prCode, $srCode);
            $errMsgMap = array(GPWP_E_3DSECURE => 'GPWP_E_3DSECURE', GPWP_E_BLOCKED => 'GPWP_E_BLOCKED', GPWP_E_LIMIT => 'GPWP_E_LIMIT', GPWP_E_TECHNICAL => 'GPWP_E_TECHNICAL', GPWP_E_CANCELED => 'GPWP_E_CANCELED');
            $errMsg = $this->dictionary->get($errMsgMap[$errClass], $language);  // kde tady vzit jazyk?
        }

        $orderReplyStatus = new OrderReplyStatus();
        $orderReplyStatus->orderStatus = ($paymentOk ? OrderStatus::$successful : OrderStatus::$failedRetriable);
        $orderReplyStatus->resultText = $errMsg;
        $orderReplyStatus->gwOrderNumber = $gwOrderNumber;
        $orderReplyStatus->shopOrderNumber = $transactionRecord->shopOrderNumber;
        $orderReplyStatus->shopPairingInfo = $transactionRecord->shopPairingInfo;
        $orderReplyStatus->forexNote = $transactionRecord->forexNote;
        $orderReplyStatus->uniAdapterData = $transactionRecord->uniAdapterData;
        $this->updateOrderReplyStatusGwOrdNumInDb($orderReplyStatus);

        $this->logger->writeLog("orderReplyStatus=" . $orderReplyStatus->orderStatus);

        return $orderReplyStatus;
    }

    public function getInfoBoxData($uniAdapterName, $language) {
        $infoBoxData = parent::getInfoBoxData($uniAdapterName, $language);
        $infoBoxData->title = $this->dictionary->get('infoBoxTitle', $language);
        $infoBoxData->link = null;
        $infoBoxData->image = 'visamastersecure.png';

        return $infoBoxData;
    }

}


define('GPWP_E_OK', 0);
define('GPWP_E_3DSECURE', 1);
define('GPWP_E_BLOCKED', 2);
define('GPWP_E_LIMIT', 3);
define('GPWP_E_TECHNICAL', 4);
define('GPWP_E_CANCELED', 5);

function classify_error($valid, $prCode, $srCode) {
    if ($valid && $prCode == 0 && $srCode == 0) {
        return GPWP_E_OK;
    }
    if (!$valid) {
        return GPWP_E_TECHNICAL;
    }
    if ($prCode == 28 && false !== array_search($srCode, array(3000, 3002, 3004, 3005, 3008))) {
        return GPWP_E_3DSECURE;
    } else {
        if ($prCode == 30 && false !== array_search($srCode, array(1001, 1002))) {
            return GPWP_E_BLOCKED;
        } else {
            if ($prCode == 30 && false !== array_search($srCode, array(1003, 1005))) {
                return GPWP_E_LIMIT;
            } else {
                if ($prCode == 50) {
                    return GPWP_E_CANCELED;
                } else {
                    return GPWP_E_TECHNICAL;
                }
            }
        }
    }
}
