<?php
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#


class OrderToPayInfo {
    var $shopOrderNumber;  // pouze pokud je to znamo dopredu, jinak null
    var $shopPairingInfo;
    var $uniAdapterData;        // serialized to blob, sessions atp
    var $amount;
    var $currency;        // ciselnik CZK, EUR, USD, GBP
    var $ccBrand;        //ciselnik dle CS pro CS, Tato polozka se jiz nepouziva
    var $customerData;   //type CustomerData
    var $language;  //ciselnik  cz, sk, en, de, pl   // ISO-639 http:www.ics.uci.edu/pub/ietf/http/related/iso639.txt (currently cs, en)
    var $description;
    var $replyUrl;
    var $notifyUrl;
    var $uniModulDirUrl;  // url adresare UniModul v rootu eshopu
    var $currencyRates; //array(ISO=>rate)
    var $subMethod; // nazev vybrane submetody, prazdne pro defaultni metodu
    var $adapterProperties; // asoc pole vlastnosti/pozadavku Adapteru
    var $recurrenceType; // RecurrenceType
    var $recurrenceParentShopOrderNumber; // pro recurrenceType::child je to prislusny ShopOrderNumber s recurrenceType::parent
    var $recurrenceDateTo; // YYYY-MM-DD
    var $cartItems; // array of CartItem
}

class CustomerData {
    var $email;
    var $first_name;
    var $last_name;
    var $street;
    var $houseNumber;
    var $city;
    var $post_code;
    var $state;  // iso AZ, TX, ...http://en.wikipedia.org/wiki/ISO_3166-2:US
    var $country; // iso CZ, US, ...https://cs.wikipedia.org/wiki/ISO_3166-1
    var $phone;
    var $identifier; // identifikator zakaznika v ramci eshopu

}

class UniCartItem extends CheckNonexistentFields {
    var $name;
    var $unitPrice;   // s dani
    var $quantity;
    var $taxRate; // v procentech
    var $unitTaxAmount;   // samotna dan, dodana pozdeji, nektere adaptery ji nemusi doplnovat
}

class CheckNonexistentFields {
    function __set($name, $value) {
        user_error("Assigning nonexistent field $name");
        $this->$name = $value;
    }
}

class TransactionRecord {
    var $transactionPK;
    var $uniModulName;
    var $gwOrderNumber;
    var $shopOrderNumber;
    var $shopPairingInfo;
    var $uniAdapterData;
    var $uniModulData;
    var $forexNote;
    var $orderStatus;
    var $dateCreated;
    var $dateModified;
}

// {{{ Konfigurace

class ConfigSetting {
    var $uniModulConfig; //UniModulConfig
    var $configData; //array (n=>v)
}

class ConfigInfo {
    var $configFields; //Array of ConfigField
}

class ConfigField {
    var $name;
    var $type; //ConfigFieldType
    var $choiceItems; //array(v=>l)
    var $label;
    var $comment;
}

class ConfigFieldType {
    static $text = 1;
    static $choice = 2;
    static $orderStatus = 3;
    static $subMethodsSelection = 4;
}

class RecurrenceType {
    const none = 0;
    const parent = 1;
    const child = 2;
}

// }}} Konfigurace


class PrePayGWInfo {
    var $paymentMethodName;
    var $paymentMethodDescription;
    var $paymentMethodIconUrl;
    var $isPossible; // bool

    var $selectCsPayBrand; // bool, vyber brandu pro CS
    var $selectCsPayBrandTitle; // text pro nadpis ke kart�m

    var $forexMessage; // string, info o automatickem prevodu

    var $convertToCurrency;    // pro konverzi
    var $convertToCurrencyAmount;

    var $subMethods;  //submetody pouzitelne pro platbu   - pokud null, tak jen hlavni modul, pokud jich je vice tak prazdny retezec znamena hlavni modul
}

class FormChoice {
    var $formTitle;
    var $formKey;
    var $formItems; // Array of (value=>text)
}

class OrderReplyStatus {
    var $shopOrderNumber;  // melo by stacit jen to pairing info, asi toto odstranit
    var $shopPairingInfo;
    var $gwOrderNumber;
    var $orderStatus; //typ OrderStatus
    var $resultText;
    var $forexNote;
    var $uniAdapterData;        // serialized from blob
}

class OrderStatus {
    static $initiated = 0;
    static $successful = 1;
    static $pending = 2;
    static $failedRetriable = 3;
    static $failedFinal = 4;
    static $invalidReply = 5;  // muze byt pouzito i pro nic nerikajici stavy, aby se nezpracovali, napr PayU stav 1.
}

class RedirectAction {
    // bude zadne z nasledujicich tri, nebo vyplneno redirectUrl nebo redirectForm nebo inlineForm, pokud inlineForm tak totez i v redirectForm pro zpetnou kompatibilitu
    var $redirectUrl;
    var $redirectForm;
    var $inlineForm;
    var $orderReplyStatus;  //vysledek hned, obvykle kdyz nelze provest platbu, nebo kdyz je nutno vytvorit objednavku soucasne s presmerovanim na branu
}

class InfoBoxData {
    public $title;
    public $image;
    public $link;
    /*
    public $platitiLink;
    public $platitiLinkText;
    */
}
