<!--
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#
-->

{foreach from=$submetTempl item=item}
    <p class="payment_module">
        <a href="{$item.redirUrl}" title="{$item.methodName}">
            <img src="{$item.uniModulLogo}" alt="{$item.methodName}" class="UniModulMethodLogo" width="86" xheight="35"/>
            {$item.methodName}
        </a>
    </p>
{/foreach}
