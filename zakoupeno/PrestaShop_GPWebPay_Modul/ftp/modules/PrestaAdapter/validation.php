<?php
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#


include_once('../../config/config.inc.php');
include_once('../../init.php');
require 'PrestaAdapter.php';

try {
    BeginUniErr();
    session_start();

    $adapter = new PrestaAdapter(null, $_GET['uniModul']);

    // k cemu?, kdyz stejne cart konstruhuju nize?
    //if ($cart->id_customer == 0 OR $cart->id_address_delivery == 0 OR $cart->id_address_invoice == 0 OR !$adapter->active)
    //	Tools::redirectLink(__PS_BASE_URI__.'order.php?step=1');

    global $cookie;
    $language = Language::getIsoById($cookie->id_lang ? (int)($cookie->id_lang) : 1);
    $orderReplyStatus = $adapter->uniModul->gatewayReceiveReply($language);
    $adapter = new PrestaAdapter(null, $_GET['uniModul']);
    $adapter->procesReplyStatus($orderReplyStatus, true);
    EndUniErr();
} catch (Exception $ex) {
    ExceptionUniErr($ex);
}