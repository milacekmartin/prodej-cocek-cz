<?php
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#


include_once(dirname(__FILE__) . '/../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../init.php');
require dirname(__FILE__) . '/PrestaAdapter.php';

BeginUniErr();
$adapter = new PrestaAdapter(null, $_GET['uniModul']);
$orderReplyStatus = $adapter->uniModul->gatewayReceiveNotification();
ob_flush(); // presta nekdy ve validaci vola ob_clean() a pak by smazala OK atp generovane fci gatewayReceiveNotification
$adapter = new PrestaAdapter(null, $_GET['uniModul']);
$adapter->procesReplyStatus($orderReplyStatus, false);
EndUniErr();

