<?php
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#

require_once(_PS_ROOT_DIR_ . "/UniModul/UniModul.php");

if (!class_exists("PrestaAdapter", false)) {  // kvuli Presta 1.3.7 eval pro vsechny moduly, chtelo by to dat adapter do UniModul...

    class PrestaAdapter extends PaymentModule {
        private $_html = '';
        private $_postErrors = array();

        public $uniModul;
        protected $paymentModule;
        protected $configInfo;

        public function __construct($paymentModule = null, $uniModul = null, $subMethod = null) {
            try {
                BeginUniErr();

                if (is_null($paymentModule)) {      // pro pripad, ze je Adapter inicializovany jako Prestovy modul sam o sobe, tak procedeme nektere zakladni inicializace
                    $paymentModule = $this;
                }
                $this->paymentModule = $paymentModule;

                //var_dump($paymentModule);
                if (_PS_VERSION_ >= '1.4.0') {
                    $paymentModule->tab = 'payments_gateways';
                } else {
                    $paymentModule->tab = 'Payment';
                }
                $paymentModule->version = preg_replace("/\D*0*(\d+)-(\d+)-.*/", "$1.$2", "#Ver:PRV075-64-gd93ef80:2017-05-04#");
                //		$this->currencies = true;
                //		$this->currencies_mode = 'radio';


                $paymentModule->page = basename(__FILE__, '.php');  // ?? K cemu dobre?
                $paymentModule->author = "www.platiti.cz";
                //$this->description = $this->l('Accepting credit and debit card payments with');
                $paymentModule->confirmUninstall = $this->l('Are you sure you want to delete your details ?');

                if (is_null($uniModul)) {
                    $paymentModule->name = 'PrestaAdapter';  // musi byt shodne s Binder modulem, Presta toto pole pouziva
                    $paymentModule->displayName = "PrestaAdapter - servis modul";
                    parent::__construct();

                    return EndUniErr();
                }

                $paymentModule->name = $uniModul . "Binder" . $subMethod;  // musi byt shodne s Binder modulem, Presta toto pole pouziva
                parent::__construct();

                $uniFact = new UniModulFactory();

                $this->configInfo = $uniFact->getConfigInfo($uniModul, $this->getLanguage(), $subMethod);

                $configSetting = $this->getConfigData($this->configInfo, $uniModul);
                $this->uniModul = $uniFact->createUniModul($uniModul, $configSetting, $subMethod);

                $paymentModule->displayName = $this->uniModul->getModuleSubMethodName($this->getLanguage(), '');

                EndUniErr();
            } catch (Exception $ex) {
                ExceptionUniErr($ex);
            }
        }

        function getShortConfigName($name, $uniModulName = null) {
            if ($uniModulName === null) {  // protoze pri konstrukci jeste uniModul neni dosazen
                $uniModulName = $this->uniModul->name;
            }
            $nn = $uniModulName . "B" . "_" . $name;
            $nn = substr($nn, 0, 32);  // max omezeni na delku cfg itemu

            return $nn;
        }

        protected function getConfigData($configInfo, $uniModulName) {
            $configData = array();
            foreach ($configInfo->configFields as $configField) {
                $configData[$configField->name] = Configuration::get($this->getShortConfigName($configField->name, $uniModulName));
            }

            $uniModulConfig = new UniModulConfig();
            $uniModulConfig->mysql_server = _DB_SERVER_;
            $uniModulConfig->mysql_dbname = _DB_NAME_;
            $uniModulConfig->mysql_login = _DB_USER_;
            $uniModulConfig->mysql_password = _DB_PASSWD_;
            $uniModulConfig->uniModulDirUrl = $this->getHttpHost() . __PS_BASE_URI__ . '/UniModul/';
            $uniModulConfig->funcGetCallbackUrl = array($this, 'getCallbackUrl');
            $uniModulConfig->adapterName = 'PrestaShop';
            //_DB_PREFIX_ ..
            //if (_DB_TYPE_ != 'MySQL') user_error('Eshop pouziva jinou nez mysql databazi');  // 1.5 uz zas pouziva jen MySql
            $cfgs = create_initialize_object('ConfigSetting', array('configData' => $configData, 'uniModulConfig' => $uniModulConfig));

            return $cfgs;
        }

        public function l($string, $specific = false, $id_lang = null) {
            try {
                BeginUniErr();
                $tmp = $this->name;
                $this->name = "PrestaAdapter";
                $ret = parent::l($string, $specific, $id_lang);
                $this->name = $tmp;

                return EndUniErr($ret);
            } catch (Exception $ex) {
                ExceptionUniErr($ex);
            }
        }


        ////// instalace

        public function install() {
            try {
                BeginUniErr();
                $failinst = !call_user_func([$this->paymentModule, 'PaymentModule::install']);
                if ($this->uniModul != null) {
                    $failinst = $failinst
                    OR !$this->paymentModule->registerHook('payment')
                    OR !$this->paymentModule->registerHook('paymentOptions')
                    OR !$this->paymentModule->registerHook('paymentReturn')
                    OR !$this->paymentModule->registerHook('adminOrder')
                    OR !$this->paymentModule->registerHook('extraRight')
                    OR !$this->paymentModule->registerHook('actionPDFInvoiceRender')
                    OR !$this->installDb();
                    // if ($this->uniModul->subMethod == null) {
                    // 	$failinst = $failinst
                    // 	OR !$this->registerHook('leftColumn');
                    // }
                    return EndUniErr(!$failinst);
                } else {
                    return EndUniErr(true);
                }

                return EndUniErr(!$failinst);
            } catch (Exception $ex) {
                ExceptionUniErr($ex);
            }
        }

        private function installDb() {
            $sql = file_get_contents(_PS_ROOT_DIR_ . "/UniModul/UniModul.sql");

            return Db::getInstance()->Execute($sql);
        }


        /// cetelem
        public function hookExtraRight($params) {

            if (!$this->paymentModule->active) {
                return;
            }

            global $smarty;

            $id_product = Tools::getValue('id_product');

            if (is_numeric($id_product)) {

                global $currency;
                $current_currency = $currency->iso_code;

                $product = new Product((int)($id_product));
                $product_price = $product->getPrice(Product::$_taxCalculationMethod == PS_TAX_INC, false);

                $shopBaseUrl = $this->getHttpHost() . __PS_BASE_URI__;
                $embedHtml = $this->uniModul->ProductGetInstallmentEmbedHtml($shopBaseUrl, $current_currency, $product_price);

                /*
                $smarty->assign(array(....));
                if (Validate::isLoadedObject($product)) return $this->display(__FILE__, 'product.tpl');
                */

                return $embedHtml;
            }

            return false;
        }


        ////// zobrazeni v administraci a konfigurace

        public function getContent() {
            try {
                BeginUniErr();
                if ($this->uniModul != null) {
                    $this->_html = '<h2>' . $this->uniModul->name . $this->uniModul->subMethod . '</h2>';
                    if (isset($_POST['submitUniAdapter'])) {
                        if ($this->uniModul->subMethod == null) {
                            foreach ($this->configInfo->configFields as $configField) {
                                if ($configField->type == ConfigFieldType::$subMethodsSelection) {
                                    $submehods = $configField->choiceItems + $this->uniModul->getSubMethods();
                                    $cumar = array();
                                    foreach ($submehods as $sname) {
                                        if (isset($_POST[$this->getShortConfigName($configField->name) . '_' . $sname])) {
                                            $cumar[] = $sname;
                                        }
                                    }
                                    Configuration::updateValue($this->getShortConfigName($configField->name), strval(implode(' ', $cumar)));
                                } else {
                                    if ($configField->type == ConfigFieldType::$orderStatus) {
                                        // preskakujeme
                                    } else {
                                        Configuration::updateValue($this->getShortConfigName($configField->name), strval($_POST[$this->getShortConfigName($configField->name)]));
                                    }
                                }
                            }

                            $fldnameNormal = 'orderStatusSuccessfull';
                            Configuration::updateValue($this->getShortConfigName($fldnameNormal), strval($_POST[$this->getShortConfigName($fldnameNormal)]));
                            $fldnameNormal = 'orderStatusPending';
                            Configuration::updateValue($this->getShortConfigName($fldnameNormal), strval($_POST[$this->getShortConfigName($fldnameNormal)]));
                            $fldnameNormal = 'orderStatusFailed';
                            Configuration::updateValue($this->getShortConfigName($fldnameNormal), strval($_POST[$this->getShortConfigName($fldnameNormal)]));
                            $fldnameNormal = 'createOrderBeforePayment';
                            Configuration::updateValue($this->getShortConfigName($fldnameNormal), isset($_POST[$this->getShortConfigName($fldnameNormal)]));
                            $fldnameNormal = 'showConfirmationPage';
                            Configuration::updateValue($this->getShortConfigName($fldnameNormal), strval($_POST[$this->getShortConfigName($fldnameNormal)]));

                        }
                        $this->displayConf();
                    }

                    $this->displayFormSettings();
                } else {
                    $this->_html = '<h2>UniAdapter - servis modul</h2>  no settings';
                }

                return EndUniErr($this->_html);
            } catch (Exception $ex) {
                ExceptionUniErr($ex);
            }
        }

        public function displayConf() {
            try {
                BeginUniErr();
                $this->_html .= '
			<div class="conf confirm">
				<img src="../img/admin/ok.gif" alt="' . $this->l('Confirmation') . '" />
				' . $this->l('Settings updated') . '
			</div>';
                EndUniErr();
            } catch (Exception $ex) {
                ExceptionUniErr($ex);
            }
        }


        public function displayFormSettings() {
            try {
                BeginUniErr();
                $this->_html .= '
			<form action="' . $_SERVER['REQUEST_URI'] . '" method="post" style="clear: both;">
			
			
			<fieldset>
				<legend><img src="../img/admin/contact.gif" />' . $this->l('Settings') . '</legend>

				';

                if ($this->uniModul->subMethod == null) {
                    foreach ($this->configInfo->configFields as $configField) {
                        if ($configField->type != ConfigFieldType::$orderStatus) {
                            $fldname = $this->getShortConfigName($configField->name);
                            $conf = Configuration::getMultiple(array($fldname));
                            $value = array_key_exists($fldname, $_POST) ? $_POST[$fldname] : (array_key_exists($fldname, $conf) ? $conf[$fldname] : '');

                            $this->_html .= '<label>' . $configField->label . '</label>';
                            if ($configField->type == ConfigFieldType::$text) {
                                $this->_html .= '<div class="margin-form"><input type="text" size="33" name="' . $fldname . '" value="' . $value . '" /></div><br/>';
                            } else {
                                if ($configField->type == ConfigFieldType::$choice) {
                                    $this->_html .= '<td><select name="' . $fldname . '">';
                                    foreach ($configField->choiceItems as $val => $lab) {
                                        $this->_html .= '<option value="' . $val . '" ' . (($val !== null && $val == $value) ? ' selected="selected"' : '') . '> ' . $lab . '</option>\n';
                                    }
                                    $this->_html .= '</select></td>';
                                } else {
                                    if ($configField->type == ConfigFieldType::$subMethodsSelection) {
                                        $this->_html .= '<td>';

                                        $submehods = $configField->choiceItems + $this->uniModul->getSubMethods();

                                        $language = $this->getLanguage();
                                        $cumar = explode(' ', $value);
                                        foreach ($submehods as $sname) {
                                            $subname = $this->uniModul->getModuleSubMethodName($language, $sname);
                                            $checked = isset($_POST[$fldname . '_submit']) ? isset($_POST[$fldname . '_' . $sname]) : (in_array($sname, $cumar));
                                            $this->_html .= '<div class="margin-form"><input type="checkbox" name="' . $fldname . '_' . $sname . '" value="' . $sname . '" ' . ($checked ? ' checked ' : '') . ' /> ' . $subname . '</div><br/>';
                                        }
                                        $this->_html .= '<input type="hidden" name="' . $fldname . '_submit" value="x" />';
                                        $this->_html .= '</td>';
                                    }
                                }
                            }

                            $this->_html .= '<div style="clear: both">&nbsp;</div>';  // je to proto, kdyz dlouhy nazev, tak se rozhodi layout
                        }
                    }
                }

                // orderStatusy
                $this->_html .= $this->displayFormSettings_orderStatus('orderStatusSuccessfull');
                $this->_html .= $this->displayFormSettings_orderStatus('orderStatusPending');
                $this->_html .= $this->displayFormSettings_orderStatus('orderStatusFailed');

                // createOrderBeforePayment
                $fldnameNormal = 'createOrderBeforePayment';
                $label = $this->l('Create order before payment');
                $this->_html .= '<label>' . $label . '</label>';
                $fldname = $this->getShortConfigName($fldnameNormal, $this->uniModul->name);
                $checked = isset($_POST[$fldname]) ? isset($_POST[$fldname]) : Configuration::get($fldname);
                $this->_html .= '<div class="margin-form"><input type="checkbox" name="' . $fldname . '" value="1" ' . ($checked ? ' checked ' : '') . ' /></div><br/>';
                $this->_html .= '<input type="hidden" name="' . $fldname . '_submit" value="x" />';

                // showConfirmationPage
                $fldnameNormal = 'showConfirmationPage';
                $label = $this->l('Show confirmation page');
                $this->_html .= '<label>' . $label . '</label>';
                $this->_html .= $this->uniModul->getMiniLogoSpan();
                $fldname = $this->getShortConfigName($fldnameNormal, $this->uniModul->name);
                $this->_html .= '<td><select name="' . $fldname . '">';
                $value = isset($_POST[$fldname]) ? $_POST[$fldname] : Configuration::get($fldname);
                $confirmationPageOptions = array(0 => $this->l('Always'), 1 => $this->l('If message'), 2 => $this->l('Never'));
                foreach ($confirmationPageOptions as $confiCode => $confiName) {
                    $this->_html .= '<option value="' . $confiCode . '" ' . (($confiCode !== null && $confiCode == $value) ? ' selected="selected"' : '') . '> ' . $confiName . '</option>\n';
                }
                $this->_html .= '</select></td>';
                $this->_html .= '<div style="clear: both">&nbsp;</div>';  // je to proto, kdyz dlouhy nazev, tak se rozhodi layout

                ///

                $this->_html .= '	
				<br /><center><input type="submit" name="submitUniAdapter" value="' . $this->l('Update settings') . '" class="button" /></center>
				</fieldset>
				</form><br /><br />
				';
                EndUniErr();
            } catch (Exception $ex) {
                ExceptionUniErr($ex);
            }
        }


        private function displayFormSettings_orderStatus($fldnameNormal) {
            global $cookie;
            $fldname = $this->getShortConfigName($fldnameNormal, $this->uniModul->name);
            $label = $this->uniModul->dictionary->get($fldnameNormal, $this->getLanguage());
            $html = '<label>' . $label . '</label>';
            $orderStates = OrderState::getOrderStates($GLOBALS['cookie']->id_lang);
            $html .= '<td><select name="' . $fldname . '">';
            $value = isset($_POST[$fldname]) ? $_POST[$fldname] : Configuration::get($fldname);
            foreach ($orderStates as $orderState) {
                $val = $orderState['id_order_state'];
                $lab = $orderState['name'];
                $html .= '<option value="' . $val . '" ' . (($val !== null && $val == $value) ? ' selected="selected"' : '') . '> ' . $lab . '</option>\n';
            }
            $html .= '</select></td>';
            $html .= '<div style="clear: both">&nbsp;</div>';  // je to proto, kdyz dlouhy nazev, tak se rozhodi layout

            return $html;
        }

        ///////////// placeni

        public function hookPayment($params) {
            try {
                BeginUniErr();
                global $smarty;

                if (!$this->paymentModule->active) {
                    return EndUniErr();
                }
                if (!$this->_checkCurrency($params['cart'])) {
                    return EndUniErr();
                }

                $language = $this->getLanguage();

                //$orderToPayInfo = new OrderToPayInfo();
                //$orderToPayInfo->language = $language;
                $orderToPayInfo = $this->getOrderToPayInfo();


                $prePayGWInfo = $this->uniModul->queryPrePayGWInfo($orderToPayInfo);
                if (!$prePayGWInfo->isPossible) {
                    return EndUniErr();
                }

                if ($prePayGWInfo->subMethods == null) {
                    $prePayGWInfo->subMethods = array('');
                }
                $submethods = $prePayGWInfo->subMethods;

                // $GLOBALS['UniPrestaShop_MethodsOrder'] = array('metoda1', 'metoda2'), nejmenovane metody se zaradi na konec v puvodnim poradi, nenalezene metody jsou ignorovany (uzitecne pokud instalovano vice modulu), obecna metodda je ''
                if (isset($GLOBALS['UniPrestaShop_MethodsOrder'])) {
                    $newOrder = $GLOBALS['UniPrestaShop_MethodsOrder'];
                    $submethods = array_intersect($submethods, $newOrder) + array_diff($submethods, $newOrder);
                }

                foreach ($submethods as $sname) {
                    $submetTempl[$sname]['uniModulLogo'] = $this->uniModul->getModulSubMethodLogoImage($this->getHttpHost() . __PS_BASE_URI__, $sname);
                    $submetTempl[$sname]['methodName'] = $this->uniModul->getModuleSubMethodName($language, $sname);
                    $submetTempl[$sname]['redirUrl'] = $this->getHttpHost() . __PS_BASE_URI__ . "modules/PrestaAdapter/redirect.php?uniModul=" . urlencode($this->uniModul->name) . '&subMethod=' . urlencode($sname);
                }
                $smarty->assign('submetTempl', $submetTempl);

                return EndUniErr($this->display(__FILE__, 'PrestaAdapter.tpl'));
            } catch (Exception $ex) {
                ExceptionUniErr($ex);
            }
        }

        public function hookPaymentOptions($params) {
            try {
                BeginUniErr();

                if (!$this->paymentModule->active) {
                    return EndUniErr();
                }
                if (!$this->_checkCurrency($params['cart'])) {
                    return EndUniErr();
                }

                $language = $this->getLanguage();
                $orderToPayInfo = $this->getOrderToPayInfo();
                $prePayGWInfo = $this->uniModul->queryPrePayGWInfo($orderToPayInfo);
                if (!$prePayGWInfo->isPossible) {
                    return EndUniErr();
                }

                if ($prePayGWInfo->subMethods == null) {
                    $prePayGWInfo->subMethods = array('');
                }
                $submethods = $prePayGWInfo->subMethods;

                // $GLOBALS['UniPrestaShop_MethodsOrder'] = array('metoda1', 'metoda2'), nejmenovane metody se zaradi na konec v puvodnim poradi, nenalezene metody jsou ignorovany (uzitecne pokud instalovano vice modulu), obecna metodda je ''
                if (isset($GLOBALS['UniPrestaShop_MethodsOrder'])) {
                    $newOrder = $GLOBALS['UniPrestaShop_MethodsOrder'];
                    $submethods = array_intersect($submethods, $newOrder) + array_diff($submethods, $newOrder);
                }

                $payment_options = [];
                foreach ($submethods as $sname) {
                    $externalOption = new \PrestaShop\PrestaShop\Core\Payment\PaymentOption();
                    $externalOption->setCallToActionText($this->uniModul->getModuleSubMethodName($language, $sname))
                        ->setAction($this->getHttpHost() . __PS_BASE_URI__ . "modules/PrestaAdapter/redirect.php?uniModul=" . urlencode($this->uniModul->name) . '&subMethod=' . urlencode($sname))
                        //->setAdditionalInformation($this->context->smarty->fetch('module:paymentexample/views/templates/front/payment_infos.tpl'))
                        ->setLogo($this->uniModul->getModulSubMethodLogoImage($this->getHttpHost() . __PS_BASE_URI__, $sname));
                    $paymentOptions[] = $externalOption;
                }

                return EndUniErr($paymentOptions);
            } catch (Exception $ex) {
                ExceptionUniErr($ex);
            }
        }


        public function hookPaymentReturn($params) {
            try {
                BeginUniErr();
                if (!$this->paymentModule->active) {
                    return EndUniErr();
                }

                global $smarty;
                $smarty->assign('binderName', $this->name);

                $methodName = $this->uniModul->getModuleSubMethodName($this->getLanguage(), '');
                $smarty->assign('methodName', $methodName);
                if (isset($this->context)) {  //pro PS<=1.4 je to shop_name jiz nastaveno z jadra
                    $smarty->assign('shop_name', $this->context->shop->name);
                }
                EndUniErr();
                BeginUniErr(E_UNIERR_DEFAULT & ~E_WARNING);

                return EndUniErr($this->display(__FILE__, 'confirmation.tpl'));
            } catch (Exception $ex) {
                ExceptionUniErr($ex);
            }
        }

        public function hookActionPDFInvoiceRender($params) {
            if (_PS_VERSION_ >= '1.7.0.0' && _PS_VERSION_ < '1.7.0.3') {
                $rp = new ReflectionProperty('SmartyLazyRegister', 'registry');
                $rp->setAccessible(true);
                $a = $rp->getValue(SmartyLazyRegister::getInstance());
                unset($a['displayPriceSmarty']);
                $rp->setValue(SmartyLazyRegister::getInstance(), $a);
            }
        }


        public function validateOrder($id_cart, $id_order_state, $amountPaid, $paymentMethod = 'Unknown', $message = null, $extraVars = array(), $currency_special = null, $dont_touch_amount = false, $secure_key = false, Shop $shop = null) {
            try {
                BeginUniErr();
                if (!$this->paymentModule->active) {
                    return EndUniErr();
                }

                BeginUniErr(E_UNIERR_DEFAULT & ~E_WARNING);
                call_user_func([$this->paymentModule, 'PaymentModule::validateOrder'], $id_cart, $id_order_state, $amountPaid, $paymentMethod, $message, $extraVars, $currency_special, $dont_touch_amount, $secure_key, $shop);
                EndUniErr();
                EndUniErr();
            } catch (Exception $ex) {
                ExceptionUniErr($ex);
            }
        }


        public function procesReplyStatus($orderReplyStatus, $frontend_redir = false) {
            global $cookie;

            switch ($orderReplyStatus->orderStatus) {
                case OrderStatus::$successful:
                    $newState = Configuration::get($this->getShortConfigName('orderStatusSuccessfull', $this->uniModul->name));
                    $guiOk = true;
                    break;
                case OrderStatus::$pending:
                    $newState = Configuration::get($this->getShortConfigName('orderStatusPending', $this->uniModul->name));
                    $guiOk = true;
                    break;
                case OrderStatus::$failedFinal:
                case OrderStatus::$failedRetriable;
                case OrderStatus::$invalidReply;
                    $newState = Configuration::get($this->getShortConfigName('orderStatusFailed', $this->uniModul->name));
                    $guiOk = false;
                    break;
                default:
            }


            if ($orderReplyStatus->orderStatus != OrderStatus::$invalidReply || $frontend_redir) {  // invalid reply resime jen pro frontend, jinak ignorujeme

                BeginSynchronized();
                $cart = new Cart(intval($orderReplyStatus->shopPairingInfo));
                if (_PS_VERSION_ >= '1.5.0') {
                    Cache::clean('Cart::orderExists_' . (int)$cart->id); // od verze 1.5.6.1 minimalne to jinak zdvojuje ordery
                }
                $orderExists = $cart->OrderExists();    //?? if (Validate::isLoadedObject($cart) AND $cart->OrderExists() == false)
                if (!$orderExists) {
                    if ($orderReplyStatus->orderStatus == OrderStatus::$successful || $orderReplyStatus->orderStatus == OrderStatus::$pending) {   // objednavku vytvorime jen pro pozitivni vysledky
                        $mailVars = array();

                        $order_total = $orderReplyStatus->uniAdapterData['order_total'];

                        if (substr(_PS_VERSION_, 0, 4) == '1.3.') {
                            $this->validateOrder($cart->id, $newState, $order_total, $this->displayName, null, $mailVars, $cart->id_currency, false);
                        } else { // novejsi 1.4
                            $this->validateOrder($cart->id, $newState, $order_total, $this->displayName, null, $mailVars, $cart->id_currency, false, $cart->secure_key);
                        }
                        $order = new Order($this->currentOrder);
                        $this->uniModul->updateShopOrderNumber($orderReplyStatus, $order->id);
                        $this->uniModul->logger->writeLog("ADAPTER: Creating shop order number " . $order->id . ", cart:" . $cart->id . ", new order state " . $newState);
                    }
                } else {   // order uz existuje, jen ho updatnu
                    //if ($orderReplyStatus->shopOrderNumber === null or $orderReplyStatus->shopOrderNumber=='') {
                    //	user_error('Chybi shopOrderNumber, mozna refresh na odeslanou platbu...');
                    //}
                    $orderId = Order::getOrderByCartId($orderReplyStatus->shopPairingInfo);
                    $order = new Order($orderId);
                    $transientStates = array(
                        Configuration::get($this->getShortConfigName('orderStatusPending', $this->uniModul->name)),
                        Configuration::get($this->getShortConfigName('orderStatusFailed', $this->uniModul->name)),
                    );
                    if ($order->getCurrentState() != $newState && in_array($order->getCurrentState(), $transientStates)) {
                        $history = new OrderHistory();
                        $history->id_order = (int)$order->id;
                        BeginUniErr(E_UNIERR_DEFAULT & ~E_USER_WARNING & ~E_WARNING);
                        $history->changeIdOrderState($newState, $order->id);
                        EndUniErr();
                        $history->addWithemail(true  /*, $extraVars*/);
                        $this->uniModul->logger->writeLog("ADAPTER: Updating shop order number " . $order->id . ", cart:" . $cart->id . ", old order state " . $order->getCurrentState() . ", new order state " . $newState);
                    } else {
                        $this->uniModul->logger->writeLog("ADAPTER: Not updating shop order number " . $order->id . ", cart:" . $cart->id . ", old order state " . $order->getCurrentState() . ", new order state " . $newState);
                    }
                }
                EndSynchronized();
            }

            if ($frontend_redir) {
                if (!isset($order)) {
                    $order = Order::getOrderByCartId($orderReplyStatus->shopPairingInfo);
                }
                if ($guiOk) {
                    $uer = ResetUniErr();
                    Tools::redirectLink(__PS_BASE_URI__ . 'order-confirmation.php?id_cart=' . $order->id_cart . '&id_module=' . $this->id . '&id_order=' . $order->id . '&key=' . $order->secure_key);
                    ResetUniErr($uer);
                } else {
                    if ($order != null) {
                        // musime obnovit kosik
                        $oldCart = new Cart(Order::getCartIdStatic((int)$order->id, (int)$cookie->id_customer));
                        $duplication = $oldCart->duplicate();
                        if (!$duplication OR !Validate::isLoadedObject($duplication['cart'])) {
                            // nelze obnovit, bude prazdny ..., pak pridat logovani toho
                            //$this->errors[] = Tools::displayError('Sorry, we cannot renew your order.');
                            //elseif (!$duplication['success'])
                            //$this->errors[] = Tools::displayError('Missing items - we are unable to renew your order');
                        } else {
                            $cookie->id_cart = $duplication['cart']->id;
                            $cookie->write();
                        }
                    }

                    $failUrl = $this->getHttpHost() . __PS_BASE_URI__ . "modules/PrestaAdapter/fail.php?uniModul=" . urlencode($this->uniModul->name) . "&errorMessage=" . urlencode($orderReplyStatus->resultText);  // TODO urlencode
                    $uer = ResetUniErr();
                    Tools::redirectLink($failUrl);
                    ResetUniErr($uer);
                }
            }
        }


        //// zobrazeni v administraci v detailu objednavky

        public function hookAdminOrder($params) {
            try {
                BeginUniErr();
                if ($this->uniModul->subMethod != null) {
                    return EndUniErr(''); // pro submoduly (jen PayUCreditCard) nehookujem, zajisti to ten hlavni
                }
                $order_id = (int)$params['id_order'];
                $order = new Order($order_id);
                $transactionRecord = $this->uniModul->getOrderTransactionRecordFromDbLast(null, $order->id_cart);
                if ($transactionRecord === null) {
                    return EndUniErr(''); // hook se vola pro pro vsechnyh moduly, i kdyz nejsou pro tuto objednavku urceny
                }

                $language = $this->getLanguage();
                $this->_html .= '
			<br />
			<fieldset style="width:400px">
				<legend><img src="../img/admin/gold.gif" /> ' . $this->l('Payment module info') /* to se vypisuje vzdy EN ale melo by byt ok!? */ . '</legend>
				' . $this->uniModul->dictionary->get('orderDetailGwOrdNum', $language) . ': ' . $transactionRecord->gwOrderNumber;

                if ($transactionRecord->forexNote !== null) {
                    $this->_html .= "<br/>" . $this->uniModul->dictionary->get('forexNoteLabel', $language) . ': ' . $transactionRecord->forexNote;
                }
                $this->_html .= '</fieldset>';

                /*
                        <div class="module_confirmation conf confirm" style="width: 400px;">
                            '.$this->uniModul->dictionary->get('orderDetailGwOrdNum', $language). ': '.$transactionRecord->gwOrderNumber.'
                        </div>';
                */

                return EndUniErr($this->_html);
            } catch (Exception $ex) {
                ExceptionUniErr($ex);
            }
        }


        public function getOrderToPayInfo($subMethod = null, $possiblyCreateOrder = false) {
            global $cookie;
            $cart = new Cart(intval($cookie->id_cart));
            $state = null;
            $customer = new Customer(intval($cart->id_customer));
            $currency_order = new Currency(intval($cart->id_currency));
            //VEN $currency_module = $this->getCurrency();

            $order_total = $cart->getOrderTotal(true, 3);

            // volitelne zaokrouhleni tak, jak to presta zobrazuje, (i kdyz to ma vnitrne presta nezaoukrouhene) a pak se musi pri potvrzeni platby stejne udat nezaokrouhlena castka, jinak presta varuje
            $c_decimals = (int)$currency_order->decimals * _PS_PRICE_DISPLAY_PRECISION_;
            if (isset($GLOBALS['UniPrestaShop_RoundPrice']) && $GLOBALS['UniPrestaShop_RoundPrice']) {
                $order_total_round = Tools::ps_round($order_total, $c_decimals);
            } else {
                $order_total_round = $order_total;
            }


            $currencies = Currency::getCurrencies(false, 0);
            $currencyRates = array();
            foreach ($currencies as $cur) {
                $currencyRates[$cur['iso_code']] = $cur['conversion_rate'];
            }

            if ($possiblyCreateOrder && Configuration::get($this->getShortConfigName('createOrderBeforePayment', $this->uniModul->name))) {
                $newState = Configuration::get($this->getShortConfigName('orderStatusPending', $this->uniModul->name));
                $mailVars = array();
                if (substr(_PS_VERSION_, 0, 4) == '1.3.') {
                    $this->validateOrder($cart->id, $newState, $order_total, $this->displayName, null, $mailVars, $cart->id_currency, false);
                } else { // novejsi 1.4
                    $this->validateOrder($cart->id, $newState, $order_total, $this->displayName, null, $mailVars, $cart->id_currency, false, $cart->secure_key);
                }
                $order = new Order($this->currentOrder);
                if (isset($order->reference) && empty($GLOBALS['UniPrestaShop_UseOrderId'])) {  // reference neni pro presta 1.4
                    $orderId = $order->reference;
                } else {
                    $orderId = $order->id;
                }
                $this->uniModul->logger->writeLog("ADAPTER: created order before payment " . $order->id . ", ref:" . $orderId . ", cart:" . $cart->id . ", new order state " . $newState);
            } else {
                $orderId = null;
            }


            $orderToPayInfo = new OrderToPayInfo();
            $orderToPayInfo->subMethod = $subMethod;
            $orderToPayInfo->shopOrderNumber = $orderId;
            $orderToPayInfo->shopPairingInfo = $cart->id;
            $orderToPayInfo->amount = $order_total_round;
            $orderToPayInfo->currency = $currency_order->iso_code;
            $orderToPayInfo->language = $this->getLanguage();
            $orderToPayInfo->description = Configuration::get('PS_SHOP_NAME');

            $address = new Address((int)($cart->id_address_delivery));
            $country = new Country((int)($address->id_country));
            if ($address->id_state) {
                $state = new State((int)$address->id_state);
                $state_iso_code = $state->iso_code;
            } else {
                $state_iso_code = null;
            }

            $customerData = new CustomerData();
            $customerData->first_name = $customer->firstname;
            $customerData->last_name = $customer->lastname;
            $customerData->email = $customer->email;
            $customerData->street = $address->address1 . ' ' . $address->address2;
            $customerData->city = $address->city;
            $customerData->post_code = $address->postcode;
            $customerData->state = $state_iso_code;
            $customerData->country = $country->iso_code;
            $customerData->phone = $address->phone;
            $customerData->identifier = $customer->id;  // pro guetsa presta vzdy vytvori noveho uzivatele pro kazdou objednavku
            $orderToPayInfo->customerData = $customerData;

            $products = $cart->getProducts();
            $cartItems = [];
            foreach ($products as $product) {
                $cartItem = new UniCartItem();
                $cartItem->name = $product['name'];
                $cartItem->quantity = $product['cart_quantity'];
                $cartItem->unitPrice = $product['price_wt'];
                $cartItem->unitTaxAmount = $product['price_wt'] - $product['total'] / $product['cart_quantity'];
                $cartItem->taxRate = $product['rate'];
                $cartItems[] = $cartItem;

            }

            // vouchery
            $sumdet = $cart->getSummaryDetails();
            foreach ($sumdet['discounts'] as $disc) {
                $cartItem = new UniCartItem();
                $cartItem->name = $disc['description'];
                $cartItem->quantity = 1;
                $cartItem->unitPrice = -$disc['value_real'];
                $cartItem->unitTaxAmount = -($disc['value_real'] - $disc['value_tax_exc']);
                $cartItem->taxRate = round(-$cartItem->unitTaxAmount / $disc['value_tax_exc'] * 100);
                $cartItems[] = $cartItem;
            }

            $cartItem = new UniCartItem();
            $cartItem->name = 'shipping / doprava';
            $cartItem->quantity = 1;
            $cartItem->unitPrice = $cart->getOrderTotal(true, Cart::ONLY_SHIPPING) + $cart->getOrderTotal(true, Cart::ONLY_WRAPPING);
            $bezDane = $cart->getOrderTotal(false, Cart::ONLY_SHIPPING) + $cart->getOrderTotal(false, Cart::ONLY_WRAPPING);
            $cartItem->unitTaxAmount = $cartItem->unitPrice - $bezDane;
            if (method_exists($sumdet['carrier'], '')) {
                $cartItem->taxRate = $sumdet['carrier']->getTaxesRate($sumdet['invoice']);
            } else {   // pro PS 1.4
                $cartItem->taxRate = (float)Tax::getCarrierTaxRate($cart->id_carrier, (int)$cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
            }
            $cartItems[] = $cartItem;

            $orderToPayInfo->cartItems = $cartItems;


            $replyUrl = $this->getHttpHost() . __PS_BASE_URI__ . "modules/PrestaAdapter/validation.php?uniModul=" . $this->uniModul->name;
            $orderToPayInfo->replyUrl = $replyUrl;

            $notifyUrl = $this->getHttpHost() . __PS_BASE_URI__ . "modules/PrestaAdapter/notification.php?uniModul=" . $this->uniModul->name;
            $orderToPayInfo->notifyUrl = $notifyUrl;

            $orderToPayInfo->uniModulDirUrl = $this->getHttpHost() . __PS_BASE_URI__ . "UniModul/";

            $orderToPayInfo->currencyRates = $currencyRates;

            $orderToPayInfo->uniAdapterData = array('subMethod' => $orderToPayInfo->subMethod, 'order_total' => $order_total);

            return $orderToPayInfo;
        }



        ///// pomocne funkce

        // protoze presta 1.2.5 nema Tools::getHttpHost
        // volano z redirectu
        public function getHttpHost() {
            return (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://') . htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8');
        }

        private function _checkCurrency($cart) {
            $currency_order = new Currency((int)($cart->id_currency));
            $currencies_module = $this->paymentModule->getCurrency((int)$cart->id_currency);
            $currency_default = Configuration::get('PS_CURRENCY_DEFAULT');

            if (is_array($currencies_module)) {
                foreach ($currencies_module AS $currency_module)
                    if ($currency_order->id == $currency_module['id_currency']) {
                        return true;
                    }
            }
        }


        /// infoBox
        function hookLeftColumn($params) {
            try {
                BeginUniErr();
                $infoBoxData = $this->uniModul->getInfoBoxData('PrestaShop', $this->getLanguage());
                $image = "UniModul/" . $infoBoxData->image;

                global $smarty;
                $smarty->assign('ibtitle', $infoBoxData->title);
                $smarty->assign('iblink', $infoBoxData->link);
                $smarty->assign('ibimage', $image);
                $smarty->assign('platitiLink', $infoBoxData->platitiLink);
                $smarty->assign('platitiLinkText', $infoBoxData->platitiLinkText);

                return EndUniErr($this->display(__FILE__, 'infoBox.tpl'));
            } catch (Exception $ex) {
                ExceptionUniErr($ex);
            }
        }

        function hookRightColumn($params) {
            return $this->hookLeftColumn($params);
        }

        function hookFooter($params) {
            return $this->hookLeftColumn($params);
        }

        public function getTemplatePath($template) {
            if (Tools::file_exists_cache(_PS_THEME_DIR_ . 'modules/PrestaAdapter/' . basename($template))) {
                $template = _PS_THEME_DIR_ . 'modules/PrestaAdapter/' . basename($template);
            } else {
                $template = dirname(__FILE__) . '/' . basename($template);
            }

            return $template;
        }


        public function displayTemplate($template, $smarty) {
            if (_PS_VERSION_ >= '1.5.0') {
                if (_PS_VERSION_ >= '1.7.0') {
                    $template = str_replace('.tpl', '.17.tpl', $template);
                }
                $template = $this->getTemplatePath($template);
                $hdrctr = new PrestaAdapterFrontController($template, $smarty);
                try {
                    BeginUniErr(E_UNIERR_DEFAULT & ~E_USER_WARNING & ~E_WARNING);
                    $hdrctr->run();
                    EndUniErr();
                } catch (Exception $ex) {
                    ExceptionUniErr($ex);
                }
            } else {
                $tmpuri = $_SERVER['REQUEST_URI']; // hack kvuli body@id, ktere se odvozuje od url u modulu vcetne query stringu. body@id je potrebne pro stylovani a query string vadi
                $_SERVER['REQUEST_URI'] = preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']);
                include(dirname(__FILE__) . '/../../header.php');
                $_SERVER['REQUEST_URI'] = $tmpuri;
                echo $this->display(dirname($template) . '/PrestaAdapter', basename($template));
                include(dirname(__FILE__) . '/../../footer.php');
            }
        }

        function getCallbackUrl($callbackName, $arguments) {
            return $this->getHttpHost() . __PS_BASE_URI__ . "/modules/PrestaAdapter/callback.php?_uniModul=" . $this->uniModul->name . "&_callbackName=" . urlencode($callbackName) . "&" . http_build_query($arguments);
        }

        function getLanguage() {
            global $cookie;

            return Tools::strtolower(Language::getIsoById($cookie->id_lang ? (int)$cookie->id_lang : Configuration::get('PS_LANG_DEFAULT')));
        }

        function displayAdditionalOrderForm($prePayGWInfo, $subMethod, $inlineForm = null) {
            global $smarty;
            $adapter = $this;

            if (file_exists(_PS_ROOT_DIR_ . "/UniModul/Uni" . $adapter->uniModul->name . $subMethod . "Logo.png")) {
                $submetLogoPiece = $subMethod;
            } else {
                $submetLogoPiece = "";
            }
            $smarty->assign('uniModulLogo', $adapter->getHttpHost() . __PS_BASE_URI__ . "UniModul/Uni" . urlencode($adapter->uniModul->name . $submetLogoPiece) . "Logo.png");

            $language = $adapter->getLanguage();
            $smarty->assign('iso_code', $language);

            if ($prePayGWInfo != null) {
                $smarty->assign('uniModulLogo', $adapter->getHttpHost() . __PS_BASE_URI__ . "UniModul/Uni" . urlencode($adapter->uniModul->name . $submetLogoPiece) . "Logo.png");
                $subname = $adapter->uniModul->getModuleSubMethodName($language, $subMethod);

                $smarty->assign('total', $GLOBALS['cart']->getOrderTotal(true, Cart::BOTH));
                $smarty->assign('totalf', Tools::displayPrice($GLOBALS['cart']->getOrderTotal(true, Cart::BOTH)));
                $smarty->assign('methodName', $subname);
                $smarty->assign('minilogo', $adapter->uniModul->getMiniLogoSpan());
                $smarty->assign('selectCsPayBrand', $prePayGWInfo->selectCsPayBrand);
                $smarty->assign('ccBrand_title', $adapter->uniModul->dictionary->get('ccBrand_title', $language));
                $smarty->assign('forexMessage', isset($prePayGWInfo) ? $prePayGWInfo->forexMessage : null);
            }

            $smarty->assign('inlineForm', $inlineForm);


            $smarty->assign('hide_left_column', true);
            $smarty->assign('hide_right_column', true);
            $smarty->assign('page_name', 'module-PrestaAdapter-redirect'); // nekdo chtel body id fini

            $adapter->displayTemplate(dirname(__FILE__) . '/redirect.tpl', $smarty);

        }

    }

    if (_PS_VERSION_ >= '1.5.0') {
        class PrestaAdapterFrontController extends FrontController {
            public function __construct($template, $smarty) {
                parent::__construct();
                $this->template = $template;
                $this->context->smarty = $smarty;
            }
        }
    }

}
