<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{prestaadapter}prestashop>confirmation_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Tu pedido en';
$_MODULE['<{prestaadapter}prestashop>confirmation_75fbf512d744977d62599cc3f0ae2bb4'] = 'está completo.';
$_MODULE['<{prestaadapter}prestashop>confirmation_91675397c13245179e4bc959514fc2ca'] = 'Has elegido el';
$_MODULE['<{prestaadapter}prestashop>confirmation_2c987f462c2ab7fc63e061e871e6a97d'] = 'método.';
$_MODULE['<{prestaadapter}prestashop>confirmation_e6dc7945b557a1cd949bea92dd58963e'] = 'Tu pedido se enviará muy pronto.';
$_MODULE['<{prestaadapter}prestashop>confirmation_0db71da7150c27142eef9d22b843b4a9'] = 'Si tiene alguna pregunta o necesita más información, contacte con nuestro';
$_MODULE['<{prestaadapter}prestashop>confirmation_64430ad2835be8ad60c59e7d44e4b0b1'] = 'servicio de asistencia al cliente';
$_MODULE['<{prestaadapter}prestashop>fail_f1d3b424cd68795ecaa552883759aceb'] = 'Resumen del pedido';
$_MODULE['<{prestaadapter}prestashop>fail_95350c890417685a7f379c5b22934f97'] = 'Pago rechazado';
$_MODULE['<{prestaadapter}prestashop>fail_4559b990b919669b87142824b56eb463'] = 'Se ha rechazado tu pago';
$_MODULE['<{prestaadapter}prestashop>fail_f915a95e609bbd517a8a1e7bdcceef37'] = 'Vuelve a intentarlo';
$_MODULE['<{prestaadapter}prestashop>prestaadapter_65b16349bb2beeea42b65c2bd7323d7d'] = 'Se aceptan pagos con tarjeta de crédito y débito con';
$_MODULE['<{prestaadapter}prestashop>prestaadapter_fa214007826415a21a8456e3e09f999d'] = '¿Seguro que quieres borrar tus detalles?';
$_MODULE['<{prestaadapter}prestashop>prestaadapter_f4d1ea475eaa85102e2b4e6d95da84bd'] = 'Confirmación';
$_MODULE['<{prestaadapter}prestashop>prestaadapter_c888438d14855d7d96a2724ee9c306bd'] = 'Configuración actualizada';
$_MODULE['<{prestaadapter}prestashop>prestaadapter_f4f70727dc34561dfde1a3c529b6205c'] = 'Configuración';
$_MODULE['<{prestaadapter}prestashop>prestaadapter_b17f3f4dcf653a5776792498a9b44d6a'] = 'Actualizar configuración';
$_MODULE['<{prestaadapter}prestashop>prestaadapter_64c517b469d85c78717a182bf90fb3a0'] = 'Información del módulo de pago';
$_MODULE['<{prestaadapter}prestashop>redirect_c453a4b8e8d98e82f35b67f433e3b4da'] = 'Pago';
$_MODULE['<{prestaadapter}prestashop>redirect_0c25b529b4d690c39b0831840d0ed01c'] = 'Suma total del pedido';
$_MODULE['<{prestaadapter}prestashop>redirect_91675397c13245179e4bc959514fc2ca'] = 'Has elegido el';
$_MODULE['<{prestaadapter}prestashop>redirect_2c987f462c2ab7fc63e061e871e6a97d'] = 'método.';
$_MODULE['<{prestaadapter}prestashop>redirect_e2867a925cba382f1436d1834bb52a1c'] = 'La cantidad total de tu pedido es';
$_MODULE['<{prestaadapter}prestashop>redirect_1f87346a16cf80c372065de3c54c86d9'] = '(impuestos incluidos)';
$_MODULE['<{prestaadapter}prestashop>redirect_d638ae6423caa29591ef29f4b4593608'] = 'Se te redirigirá a la plataforma de pago después de confirmar tu pedido.';
$_MODULE['<{prestaadapter}prestashop>redirect_0881a11f7af33bc1b43e437391129d66'] = 'Confirma tu pedido haciendo clic en \"Confirmo mi pedido\"';
$_MODULE['<{prestaadapter}prestashop>redirect_569fd05bdafa1712c4f6be5b153b8418'] = 'Otros métodos de pago';
$_MODULE['<{prestaadapter}prestashop>redirect_46b9e3665f187c739c55983f757ccda0'] = 'Confirmo mi pedido';
