<!--
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#
-->

{capture name=path}{l s='Payment' mod='PrestaAdapter'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

{if !empty($total)}
    <h2>{l s='Order summation' mod='PrestaAdapter'}</h2>
    {assign var='current_step' value='payment'}
    {include file="$tpl_dir./order-steps.tpl"}
    <h3>{$methodName}</h3>
    {$minilogo}
    <p>
        <img src="{$uniModulLogo}" alt="{$methodName}" class="UniModulMethodLogo" style="float:left; margin: 0px 10px 5px 0px;"/>
        {l s='You have chosen the' mod='PrestaAdapter'} {$methodName} {l s='method.' mod='PrestaAdapter'}
        <br/><br/>
        {l s='The total amount of your order is' mod='PrestaAdapter'}
        <span id="amount_{$currencies.0.id_currency}" class="price">{convertPrice price=$total}</span>
        {if $use_taxes == 1}
            {l s='(tax incl.)' mod='PrestaAdapter'}
        {/if}
    </p>
    {if isset($forexMessage)}
        &nbsp;
        <p style="text-align:left">{$forexMessage}</p>
    {/if}

    {if !isset($inlineForm)}
        <form method="post">
            {if $selectCsPayBrand === true}
                <br/>
                <p style="text-align:left">
                    <b>{$ccBrand_title}</b><br/> <input type="radio" name="ccBrand" value="VISA" checked> VISA</input>
                    <br/> <input type="radio" name="ccBrand" value="MasterCard"> MasterCard</input><br/>
                    <input type="radio" name="ccBrand" value="VisaElectron"> VisaElectron</input><br/>
                    <input type="radio" name="ccBrand" value="Maestro"> Maestro</input><br/>
                </p>
            {/if}
            <p>
                <br/>
                {l s='You will be redirected to the payment gateway after you confirm the order.' mod='PrestaAdapter'}
            </p>
            <p>
                <br/> <b>{l s='Please confirm your order by clicking \'I confirm my order\'' mod='PrestaAdapter'}.</b>
            </p>
            <p class="cart_navigation">
                <a href="{$link->getPageLink('order.php', true)}?step=3" class="button_large">{l s='Other payment methods' mod='PrestaAdapter'}</a>
                <input type="submit" name="uniPayCont" value="{l s='I confirm my order' mod='PrestaAdapter'}" class="exclusive_large"/>
                {$minilogo}
            </p>
        </form>
    {/if}
{/if}
{if isset($inlineForm)}
    &nbsp;
    <p style="text-align:left">{$inlineForm}</p>
{/if}
