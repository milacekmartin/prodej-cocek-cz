<!--
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#
-->

<div class="block" id="categories_block_left">
    <h4>{$ibtitle}</h4>
    <div class="block_content" style="text-align:center">

        {if $iblink!==null}
        <a href="{$iblink}">
            {/if}
            <img style="margin-top:10px" src="{$content_dir}{$ibimage}" alt="platitipti">
            {if $iblink!==null}
        </a>
        {/if}

        <!--<br>
                <a href="{$platitiLink}"><span style="font-size:5pt">{$platitiLinkText}</span></a>-->
    </div>
</div>
