<!--
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#
-->

{extends file='page.tpl'}

{block name='page_content_container'}
    <section id="content-hook_order_confirmation" class="card">
        <div class="card-block">
            <div class="row">
                <div class="col-md-12">


                    {$methodName}
                    <img src="{$uniModulLogo}" alt="{$methodName}" style="float:right; margin: 0px 10px 5px 0px;"/>{$minilogo nofilter}

                    <h3>{l s='Payment rejected' mod='PrestaAdapter'}</h3>


                    <p>
                        {l s='Your payment has been rejected' mod='PrestaAdapter'}
                        <br/>
                        {$errorMessage}
                    </p>

                    <p class="cart_navigation">
                        <a href="{$link->getPageLink('order.php', true)}?step=3" class="button_large">{l s='Try again' mod='PrestaAdapter'}</a>
                    </p>


                </div>
            </div>
        </div>
    </section>
{/block}

