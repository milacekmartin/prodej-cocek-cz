<?php
/**
* used in free module version only
*/
  class cMap {
	
	 private $function;
	 
	 public function __construct($function) {
	 	$this->function=$function;	 
	 }
	 
	
	 /**
	 * builds heureka tree from stored map
	 * 
	 */
	public function buildTaxonomyTree($state) {
	        $field=$this->function.'_category';
			
			$sql='SHOW COLUMNS FROM  '._DB_PREFIX_.'category LIKE "'.$field.'"';
			$test= Db::getInstance()->executeS($sql);
			if($test && is_array($test) ) {
			$cache_path=dirname(__FILE__).'/cache/'.$this->function.'_'.Context::getContext()->shop->id;
			if($state == 'start') {   
				$retval=array();
				$sql='SELECT c.'.$field.',   c.`id_category`
				FROM 
				`'._DB_PREFIX_.'category` c 
				LEFT JOIN `'._DB_PREFIX_.'category_shop` cs
				ON c.id_category =  cs.id_category
				WHERE cs.id_shop='.(int)Context::getContext()->shop->id.' 
				GROUP BY  c.`id_category`
				'; 
				$ct= Db::getInstance()->ExecuteS($sql);

				foreach($ct as $cat) {
				$retval[$cat['id_category']]= $cat[$field]; 
				} 
				file_put_contents($cache_path, json_encode($retval)); 
			}
			 
			return json_decode(file_get_contents($cache_path), true);
			} 
	 }
	 
	
	 
	
}
?>
