<?php
/**
 * Modul Zboží: Srovnávače zboží - export xml pro Prestashop
 *
 * PHP version 5
 *
 * LICENSE: The buyer can free use/edit/modify this software in anyway
 * The buyer is NOT allowed to redistribute this module in anyway or resell it 
 * or redistribute it to third party
 *
 * @package    zbozi
 * @author    Vaclav Mach <info@prestahost.cz>
 * @copyright 2014,2015 Vaclav Mach
 * @license   EULA
 * @version    1.0
 * @link       http://www.prestahost.eu
 */
 require_once("cFeed.php");
  class FeedSeznam extends cFeed {
   protected  $feedname='zbozi_seznam.xml';

   
   protected function StartFeed($fp) {
          
    fputs($fp,  "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
    
    fputs($fp,  "<SHOP xmlns=\"http://www.zbozi.cz/ns/offer/1.0\">\n"); 
  }      
    
   protected function CloseFeed($fp) {
      fputs($fp,  "</SHOP>");  
     
 } 
 
 protected function createItem($product, $url, $imgurl) {
    $item= "\t\t<SHOPITEM>\n";
    $optvals=array(
         'name'=>$this->prepareString($product['name']),
         'manufacturer'=>$this->prepareString($product['manufacturer_name']),
         'reference'=>$this->prepareString($product['reference']),
         'ean'=>$this->prepareString($product['ean13'])
         );
         
      $item.=$this->createTag('PRODUCTNAME', $this->compileOptimisedTag('zbozi', 'productname', $optvals));
      $item.=$this->createTag('PRODUCT', $this->compileOptimisedTag('zbozi', 'product', $optvals).$this->addExtendedText($product));
    
    $item.=$this->getCategoryText($product['categorytext_seznam']);

    
    $item.= "\t\t\t<DESCRIPTION>".$this->prepareString($this->getDescription($product))."</DESCRIPTION>\n";
    $item.= "\t\t\t<URL>".$this->prepareString($url)."</URL>\n";
   // $item.= "\t\t\t<SHOP_DEPOTS>dets2908</SHOP_DEPOTS>\n";
 
     if($imgurl) {
            $item.=$this->createTag('IMGURL', $this->prepareString($imgurl));   
      }
    $item.= "\t\t\t<PRICE_VAT>".$this->prepareString($product['price'])."</PRICE_VAT>\n"; 
    $item.= "\t\t\t<DELIVERY_DATE>".$this->getAvailability($product)."</DELIVERY_DATE>\n";
     if($product['ean13'])
      $item.=$this->createTag('EAN', $this->prepareString($product['ean13']));
    $item.="\t\t</SHOPITEM>\n";

    return $item;
 
 }    
   
 protected function getCategoryText($categorytext)  {
     $item='';
      if(!empty($categorytext)  && is_array($categorytext)) {
    foreach($categorytext as $category) {
     $item.='<CATEGORYTEXT>'.$this->prepareString($category).'</CATEGORYTEXT>';
    }
    } 
    elseif(!empty($categorytext)) {
        $item.='<CATEGORYTEXT>'.$this->prepareString($categorytext).'</CATEGORYTEXT>';  
    }
    
    return $item; 
 }
 
 protected function getAvailability($item) {
 	$availability=parent::getAvailability($item);
 	
 	if((int)Configuration::get('ZBOZI_SKLADzb')  
 	 && (int)$this->availability_mode == 0
 	 && !empty($item['available_now'])) {
 	 	if((int)Configuration::get('ZBOZI_SKLADzb') == 1) {
 	 	  if($availability == 1)
 	 	    return 0;
		}
 	 	elseif((int)Configuration::get('ZBOZI_SKLADzb') == 2) {
 	 	  $text=mb_strtolower($item['available_now'], 'UTF-8');
 	 	  $pos=strpos($text, 'skladem');
 	 	  if(!($pos === false))
 	 	    return 0;
		}  
	 }
 	return $availability;
 }
      
  }
?>
