<?php
/**
 * Modul Zboží: Srovnávače zboží - export xml pro Prestashop
 *
 * PHP version 5
 *
 * LICENSE: The buyer can free use/edit/modify this software in anyway
 * The buyer is NOT allowed to redistribute this module in anyway or resell it 
 * or redistribute it to third party
 *
 * @package    zbozi
 * @author    Vaclav Mach <info@prestahost.cz>
 * @copyright 2014,2015 Vaclav Mach
 * @license   EULA
 * @version    1.0
 * @link       http://www.prestahost.eu
 */
 require_once("cFeed.php");
  class FeedGoogle extends cFeed {
  protected  $feedname='zbozi_google.xml';
  protected $googleAttributes;
  protected $GoogleCombinations;
  protected $zonemap=array();
  protected $carriers;

  protected $free_shipping_price=0;
protected $free_shipping_weight=0;
  
  public function __construct() {
  	  // klice jsou skupiny z eshopu hodnoty z google
      parent::__construct();
      if(file_exists(dirname(__FILE__).'/ZboziAttributes.php')) {
  	  $this->googleAttributes=json_decode(Configuration::get('ZBOZI_GATTRIBUTES'), true);
  	  $this->GoogleCombinations = new GoogleCombinations();
  	    }
 
   $sql='SELECT DISTINCT z.id_zone  FROM '._DB_PREFIX_.'carrier_zone z';
   $ret = Db::getInstance()->executeS($sql);
   
	$this->zonemap= $this->zonemap($ret);  
    $this->carriers=json_decode(Configuration::get("ZBOZI_CARRIERSG"), true);
     $this->free_shipping_price =Configuration::get('PS_SHIPPING_FREE_PRICE');
   
     $this->free_shipping_weight =Configuration::get('PS_SHIPPING_FREE_WEIGHT');
     

  }
  protected function StartFeed($fp) {
  	    $shop_url=ShopUrl::getMainShopDomain();
        fputs($fp,  "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n
        <feed xmlns=\"http://www.w3.org/2005/Atom\" xmlns:g=\"http://base.google.com/ns/1.0\">\n
        <title>".$shop_url."</title>\n
        <link href=\"http://".$shop_url."/xml/google.xml\" rel=\"alternate\" type=\"text/html\" />
        <updated>".date('Y-m-d H:m:i')."</updated>\n
        <id>tag:".$shop_url.",".date('Y-m-d')."</id>\n");
  }     
   protected function CloseFeed($fp) {
     fputs($fp,  "</feed>");
     fclose($fp);  
 }   
     
protected function getItemGroup($product, $url, $cover) {
	 $itemgroup='';
     foreach($product['attributes'] as $combination) {
             if($this->jen_skladem &&   $combination['quantity'] <=0)
              continue;
            if((float)($product['price'] + $combination['price'] > 0)) {
            if($this->GoogleCombinations->remap($combination, $this->googleAttributes, $product['id_product']) !== false)
                 $itemgroup.=$this->createItemCombination($product, $combination, $url, $cover); 
			} 
        }
     return $itemgroup;
}

protected function createItem($product, $url, $imgurl) {
       $item= "\t\t<entry>\n";
       $item.=$this->createTag('g:id', $this->getUniqueId());
       $item.=$this->createTag('title', $this->prepareString($product['name']));
       $item.=$this->createTag('description', $this->prepareString($this->getDescription($product)));
       $item.=$this->createTag('g:product_type', $this->prepareString($this->getCategoryText($product['categorytext_seznam'])));
       $item.=$this->createTag('link', $this->prepareString($url));  
       $item.=$this->createTag('updated', $this->prepareString($product['date_upd']));  
       $item.=$this->createTag('g:image_link', $this->prepareString($imgurl)); 
       $item.=$this->createTag('g:condition', $this->prepareString('new'));  
      
        if (isset($product['ean13'])  && !empty($product['ean13'])) {
        	      $item.=$this->createTag('g:gtin', $this->prepareString($product['ean13']));
		}
		if (isset($product['reference'])  && !empty($product['reference'])) {
        	      $item.=$this->createTag('g:mpn', $this->prepareString($product['reference']));
		}
		elseif(isset($product['supplier_reference'])  && !empty($product['supplier_reference'])) {
		          $item.=$this->createTag('g:mpn', $this->prepareString($product['supplier_reference']));
		}
		
		if(isset($product['categorytext_google'])) {
			$item.=$this->createTag('g:google_product_category', $this->prepareString($product['categorytext_google'])); 
		}
	   $item.=$this->createTag('g:availability', $this->prepareString($this->getAvailability($product)));
       $item.=$this->createTag('g:price', $this->prepareString($product['price']).' CZK');
       $item.=$this->createTag('g:brand', $this->prepareString($product['manufacturer_name'])); 
       $item.=$this->getDoprava($product);
        $item.="\t\t</entry>\n";
     return $item;
}
      
protected function createItemCombination($product, $combination, $url, $imgurl) {
       $item= "\t\t<entry>\n";
       $item.=$this->createTag('g:id', $this->getUniqueId());
       $item.=$this->createTag('g:item_group_id', $product['id_product']);
       $item.=$this->createTag('title', $this->prepareString($product['name'].$this->getCombinationName($combination['attributes'])));
       $item.=$this->createTag('description', $this->prepareString($this->getDescription($product)));
       $item.=$this->createTag('g:product_type', $this->prepareString($this->getCategoryText($product['categorytext_seznam'])));
       $item.=$this->createTag('link', $this->prepareString($url));  
       $item.=$this->createTag('updated', $this->prepareString($product['date_upd']));  
       $item.=$this->createTag('g:condition', $this->prepareString('new'));  
	
      
      if($combination['id_image']) {
             $name=$this->toUrl($product['name']);
             global $link;
             $imgurl=$link->getImageLink($name, $product['id_product'].'-'.$combination['id_image'], $this->imagetype);  
             $item.=$this->createTag('g:image_link', $this->prepareString($imgurl));   
      }
      elseif($imgurl) {
            $item.=$this->createTag('g:image_link', $this->prepareString($imgurl));   
      }
      
     if(isset($this->cache[$product['id_product']][$combination['id_product_attribute']]) 
       &&  $this->cache[$product['id_product']][$combination['id_product_attribute']]['date_upd'] == $product['date_upd']
       &&  $this->cache[$product['id_product']][$combination['id_product_attribute']]['product_price'] == $product['price'] 
         &&  $this->cache[$product['id_product']][$combination['id_product_attribute']]['attribute_price'] == $combination['price'] 
     ) {
        $price=$this->cache[$product['id_product']][$combination['id_product_attribute']]['price'];  
     }
     else {
      $price=Product::getPriceStatic($product['id_product'], true, $combination['id_product_attribute'],2);
        $this->cache[$product['id_product']][$combination['id_product_attribute']]['price']=$price;
        $this->cache[$product['id_product']][$combination['id_product_attribute']]['date_upd']=$product['date_upd']; 
        $this->cache[$product['id_product']][$combination['id_product_attribute']]['product_price']=$product['price']; 
        $this->cache[$product['id_product']][$combination['id_product_attribute']]['attribute_price']=$combination['price']; 
     } 
    
      $item.=$this->createTag('g:price', $this->prepareString($price).' CZK');
      $item.=$this->addAttributes($combination['attributes']);
	  if($combination['ean13'])
         $item.=$this->createTag('g:gtin', $this->prepareString($combination['ean13']));
      elseif($product['ean13'])
         $item.=$this->createTag('g:gtin', $this->prepareString($product['ean13']));
		
		if (isset($combination['reference'])  && !empty($combination['reference'])) {
        	      $item.=$this->createTag('g:mpn', $this->prepareString($combination['reference']));
		}
		elseif (isset($product['reference'])  && !empty($product['reference'])) {
        	      $item.=$this->createTag('g:mpn', $this->prepareString($product['reference']));
		}
		elseif(isset($product['supplier_reference'])  && !empty($product['supplier_reference'])) {
		          $item.=$this->createTag('g:mpn', $this->prepareString($product['supplier_reference']));
		}
		
		if(isset($product['categorytext_google'])) {
			$item.=$this->createTag('g:google_product_category', $this->prepareString($product['categorytext_google'])); 
		}
	   $item.=$this->createTag('g:availability', $this->prepareString($this->getAvailability($product)));
      
       $item.=$this->createTag('g:brand', $this->prepareString($product['manufacturer_name'])); 
       
       $item.=$this->getDoprava($product);
        $item.="\t\t</entry>\n";
     return $item;   
   }   
         
  protected function getCategoryText($categorytext)  {
   return str_replace('|', '&gt;', $categorytext);
 } 
 
   private function cantor($id_product, $id_attribute=0)
{
	if(!$id_attribute)
	  return $id_product;
    // ((x + y) * (x + y + 1)) / 2 + y;
    if(function_exists('bcadd'))
    return bcadd(bcdiv(bcmul(bcadd($x, $y), bcadd(bcadd($x, $y), 1)), 2), $y);
    
    return (($x + $y) * ($x + $y + 1)) / 2 + $y;
}   

 protected function getAvailability($product) {
        
          // respektuji rizeni skladu
          if($this->availability_mode == 0 || empty($this->availability_mode)) {
               if($this->stock_management) {
                     if(isset($product["quantity"]) && $product["quantity"] > 0) {
                        return 'in stock';
                     }
                     else {
                     	   return 'out of stock';   
                     }
               }
                  else
                 return (int) $this->availability; 
             }
          // parsuje text 
          elseif($this->availability_mode==1) {
               if ( $this->parseAvailability($product['available_now'], 'available_now') > 2)   //todo
                     return 'out of stock';
            return 'in stock'; 
          }
          elseif($this->availability_mode==2) {    // respektuje jen vychozi hodoty
            if ((int) $this->availability > 0)
              return 'out of stock';
            return 'in stock'; 
          }
   }
   
   private function addAttributes($attributes) {
   	   $retval='';
   	   foreach($attributes as $attribute) {
   	   	  		  $retval.=$this->createTag('g:'.$attribute[0],  $attribute[1]);
	   }
   	   return $retval;
   }
   
 
   protected function getUniqueId() {
   global $uniqueId;
   return ++$uniqueId;   
 }
 

protected function getDoprava($product) {
if(!Configuration::get("ZBOZI_DOPRAVAG_ON"))
  return '';
 $sql='
            SELECT c.*
            FROM `'._DB_PREFIX_.'product_carrier` pc
            INNER JOIN `'._DB_PREFIX_.'carrier` c
                ON (c.`id_reference` = pc.`id_carrier_reference` AND c.`deleted` = 0)
            WHERE pc.`id_product` = '.(int)$product['id_product'].'
                AND pc.`id_shop` = '.(int)Context::getContext()->shop->id;
 $result= Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
 if(!$result || !count($result)) {
     $result=  Carrier::getCarriers(Configuration::get('PS_LANG_DEFAULT'), true, false, null, array(Configuration::get('PS_UNIDENTIFIED_GROUP')), Carrier::PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE);
 }
$product_price_tax=$product['price']*(100 + $product['rate'])/100; 
 if(is_array($result) && count($result)) {
     $retval=array();
  foreach ($result as $k => $row) {
        
        if($this->carrierAllowed($row['id_carrier']) === false)
          continue;
        
           $sql='SELECT id_zone FROM '._DB_PREFIX_.'carrier_zone WHERE id_carrier='.(int)$row['id_carrier'];
            $zones=Db::getInstance()->executeS($sql);
       
         $carrier=new Carrier($row['id_carrier']);
         $shipping_method = $carrier->getShippingMethod();
         
        foreach($zones as $zone) {
        
        $id_zone = $zone['id_zone'];
        $id_country=Db::getInstance()->getValue('SELECT id_country FROM '._DB_PREFIX_.'country WHERE id_zone='.(int)$id_zone);
        if(isset($this->carrierCache[$row['id_carrier']][$id_zone])) {
        	if(!($this->carrierCache[$row['id_carrier']][$id_zone] === false)) {
        		 $retval[]=array($carrier->name, $this->carrierCache[$row['id_carrier']][$id_zone], $id_zone);
			}
			continue;
		}
        
        
        $price=false;
        
        
        	
            if ($shipping_method != Carrier::SHIPPING_METHOD_FREE)
            {
                // Get only carriers that are compliant with shipping method
                if (($shipping_method == Carrier::SHIPPING_METHOD_WEIGHT && $carrier->getMaxDeliveryPriceByWeight($id_zone) === false)
                    || ($shipping_method == Carrier::SHIPPING_METHOD_PRICE && $carrier->getMaxDeliveryPriceByPrice($id_zone) === false))
                {
                     $this->carrierCache[$row['id_carrier']][$id_zone]=false;
                     continue;
                }

                // If out-of-range behavior carrier is set on "Desactivate carrier"
                if ($row['range_behavior'])
                {
                    // Get only carriers that have a range compatible with cart
                    if (($shipping_method == Carrier::SHIPPING_METHOD_WEIGHT
                        && (!Carrier::checkDeliveryPriceByWeight($row['id_carrier'], $product['weight'], $id_zone)))
                        || ($shipping_method == Carrier::SHIPPING_METHOD_PRICE
                        && (!Carrier::checkDeliveryPriceByPrice($row['id_carrier'], $product_price_tax, $id_zone, Context::getContext()->currency->id))))
                    {
                    	$this->carrierCache[$row['id_carrier']][$id_zone]=false;
                        continue;
                    }
                }
            }
            if($shipping_method == Carrier::SHIPPING_METHOD_WEIGHT) {
                if($this->free_shipping_weight > 0 && $product['weight'] >  $this->free_shipping_weight)
                   $price=0;
                else
                  $price=$carrier->getDeliveryPriceByWeight($product['weight'], $id_zone);
                
            }
           elseif ($shipping_method == Carrier::SHIPPING_METHOD_PRICE) {
               if($this->free_shipping_price > 0 && $product_price_tax >  $this->free_shipping_price) {
                   $price=0;
			   }
               else  {
                 $price=$carrier->getDeliveryPriceByPrice($product_price_tax, $id_zone);
               }
           }
        
     $price=(float)$price;
     /*
           $sql='SELECT t.rate FROM '._DB_PREFIX_.'tax_rule r LEFT JOIN  '._DB_PREFIX_.'tax t ON
           t.id_tax = r.id_tax WHERE r.id_tax_rules_group='.(int)$carrier->id_tax_rules_group.' AND
           r.id_country='.(int)$id_country; // chyba ... 
     $rate= Db::getInstance()->getValue($sql);
           if($rate)
             $price=round($price*(100+$rate)/100);
     */        
           $this->carrierCache[$row['id_carrier']][$id_zone]=$price;
           $retval[]=array($carrier->name, $price, $id_zone, $carrier->id_tax_rules_group);
  
    }
  }
   if(count($retval))
     return $this->compile_delivery($retval);  
   
   return '';
 }

}

protected function compile_delivery($carriers) {
   $retval=''; 
    while(list($key,$carrier)=each($carriers)) {
    // name, price, id_zone, id_tax_rules_group
    if(isset($this->zonemap[$carrier[2]][$carrier[3]])) {
    $map =$this->zonemap[$carrier[2]][$carrier[3]];
	}
    elseif($carrier[3] == 0) {
		 $keys=array_keys($this->zonemap[$carrier[2]]);
		 $map =$this->zonemap[$carrier[2]][$keys[0]];
	}
	else
	  continue;
	  
    foreach ($map as $country) {
	    $item=$this->createTag('g:country',$country['iso_code']);
	    $item.=$this->createTag('g:service',$carrier[0]);
	    if($carrier[3] == 0)
	     $price=round($carrier[1]);
	    else
	    $price=round($carrier[1]*(100+$country['rate'])/100);
	    $item.=$this->createTag('g:price',$price);
	    //$retval.=$this->createTag('g:shipping',"\n".$item);
	      $retval.= "\t\t\t<g:shipping>\n$item\t\t\t</g:shipping>\n";  
	}
    }
    
    return $retval;
    
}

protected function carrierAllowed($id_carrier) {
    if(!is_array($this->carriers) ||
       !count($this->carriers) || 
       !isset($this->carriers[$id_carrier]) ||
        empty($this->carriers[$id_carrier])
     )
      return false;
    
  
   
     return  true;

}

protected function zonemap($rows) {
	$ret=array();
	foreach($rows as $row) {
		$sql='SELECT c.id_country, c.iso_code, r.id_tax_rules_group, r.id_tax, t.rate FROM 
		'._DB_PREFIX_.'country c LEFT JOIN 
		('._DB_PREFIX_.'tax_rule r LEFT JOIN '._DB_PREFIX_.'tax t on r.id_tax = t.id_tax )ON
		c.id_country = r.id_country
		WHERE c.active =1 AND c.id_zone='.(int)$row['id_zone'];
		$countries=Db::getInstance()->executeS($sql);
		foreach($countries as $country) {
			$ret[$row['id_zone']][$country['id_tax_rules_group']][]=$country;
		}
	
	}	
	return $ret;
}

  }
?>
