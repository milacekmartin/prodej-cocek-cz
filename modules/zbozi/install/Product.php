<?php
  class Product extends ProductCore
{
    public $heureka_category;
    public $zbozi_text;
    public $heureka_cpc;
    public $max_cpc;
    public $max_cpc_search;
    

  public function __construct($id_product = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null)
    {
    self::$definition['fields']['heureka_category'] = array('type' => self::TYPE_STRING,  'validate' => 'isString', 'required' => false, 'size' => 255);
    self::$definition['fields']['zbozi_text'] = array('type' => self::TYPE_STRING,  'validate' => 'isString', 'required' => false, 'size' => 255);
    self::$definition['fields']['heureka_cpc'] = array('type' => self::TYPE_STRING,  'validate' => 'isString', 'required' => false, 'size' => 20);
    self::$definition['fields']['max_cpc'] = array('type' => self::TYPE_STRING,  'validate' => 'isString', 'required' => false, 'size' => 20);
    self::$definition['fields']['max_cpc_search'] = array('type' => self::TYPE_STRING,  'validate' => 'isString', 'required' => false, 'size' => 20);

  parent::__construct($id_product , $full , $id_lang, $id_shop,$context );
  }
  
    protected function copyFromPost(&$object, $table)
    {
        parent::copyFromPost($object, $table);
        $object->heureka_category = (string)Tools::getValue('heureka_category');
        $object->zbozi_text = (string)Tools::getValue('zbozi_text');
        $object->heureka_cpc = (string)Tools::getValue('heureka_cpc');
        $object->max_cpc = (string)Tools::getValue('max_cpc');
        $object->max_cpc_search = (string)Tools::getValue('max_cpc_search');
      
    }
}
