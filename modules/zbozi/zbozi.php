<?php
/**
 * Modul Zboží: Srovnávače zboží - export xml pro Prestashop
 *
 * PHP version 5
 *
 * LICENSE: The buyer can free use/edit/modify this software in anyway
 * The buyer is NOT allowed to redistribute this module in anyway or resell it 
 * or redistribute it to third party
 *
 * @package    zbozi
 * @author    Vaclav Mach <info@prestahost.cz>
 * @copyright 2014,2015 Vaclav Mach
 * @license   EULA
 * @version    1.0
 * @link       http://www.prestahost.eu
 */

if (!defined('_PS_VERSION_'))
    exit;
 

class Zbozi extends Module
{

  protected $_html = '';
    protected $_postErrors = array();
    protected $feeds=array("seznam", "heureka", "google");
    protected $feedsUsed=array();
    protected $feeddir= 'xml';    
    protected $availability=0; 
    protected $availability_later=10;
    protected $availability_mode =0;
    protected $text_fields=array('ZBOZI_IMG', 'ZBOZI_DESCRIPTION', 'ZBOZI_ALLOWTAGS',
     'ZBOZI_CPC', 'ZBOZI_FORBIDDEN');
    protected $text_defaults=array('medium', 'description_short', '<b>,<u>,<i>,<p>', '', '','');
    protected $config=array();
    protected $do_attributes;
    protected $carriers;
    protected $carriersG;
    protected $cods;
    private $currentTab =0;

 public function __construct()
    {
          $this->name = 'zbozi';
          $this->do_attributes=0; 
          if(file_exists(dirname(__FILE__).'/ZboziAttributes.php')) {
               $this->do_attributes=1; 
          }
        $this->version = 2.721;
        
        $this->tab = 'advertising_marketing';
                $this->author = 'PrestaHost.eu)';
        
        $config = Configuration::getMultiple(array(
         'ZBOZI_SEZNAM', 'ZBOZI_HEUREKA', 
        'ZBOZI_GOOGLE', 'ZBOZI_AVAILABILITY', 'ZBOZI_AVAILABILITY_LATER',
         'ZBOZI_IMG', 'ZBOZI_DESCRIPTION', 'ZBOZI_ALLOWTAGS', 'ZBOZI_CPC', 'ZBOZI_AVAILABILITY_MODE', 'ZBOZI_FORBIDDEN',
        'ZBOZI_SKLADEM'));
        $this->feedsUsed["seznam"]=empty($config['ZBOZI_SEZNAM'])?0:1; 
        $this->feedsUsed["heureka"]=empty($config['ZBOZI_HEUREKA'])?0:1;    
      
        $this->feedsUsed["google"]=empty($config['ZBOZI_GOOGLE'])?0:1;   
        $this->availability= intval($config['ZBOZI_AVAILABILITY']);
        if(isset($config['ZBOZI_AVAILABILITY_LATER']))
        $this->availability_later= intval($config['ZBOZI_AVAILABILITY_LATER']);
        
        $this->availability_mode= intval($config['ZBOZI_AVAILABILITY_MODE']);
      
       foreach($this->text_fields as $field) {
           $this->config[$field] = $config[$field];
       }
     

        parent::__construct();

        $this->displayName = 'Zboži';
        if($this->do_attributes)
             $this->displayName .=' s variantami produktů';     
        $this->description = 'Modul pro export zboží do služby  zbozi.cz a dalších';
        $this->confirmUninstall ='Odinstalovat ?';
        $val=0;
       
        foreach($this->feedsUsed as $feed) {
         $val+=$feed;   
        }
        
        if(!$val && Module::isEnabled($this->name))
        $this->warning[] = 'není nastavena tvorba  žádného feedu'; 
           
        $this->carriers=json_decode(Configuration::get('ZBOZI_CARRIERS'), true);
        $this->carriersG=json_decode(Configuration::get('ZBOZI_CARRIERSG'), true);
        $this->cods=json_decode(Configuration::get('ZBOZI_CARRIERSCOD'), true);
        $this->currentTab=(int)Tools::getValue('currentTab');
    }
    
  
    
    public function hookActionCarrierUpdate($params)
{
    // Update the id for carrier 1
    
    $carriers=array();
    while(list($key,$val)=each($this->carriers)) {
         if ((int)($params['id_carrier']) == $key && (int)$params['carrier']->id)
          $carriers[$params['carrier']->id] = $val;
         else
          $carriers[$key] = $val;
    }
    Configuration::updateValue('ZBOZI_CARRIERS', json_encode($carriers));
    
      $carriers=array();
    while(list($key,$val)=each($this->carriersG)) {
         if ((int)($params['id_carrier']) == $key && (int)$params['carrier']->id)
          $carriers[$params['carrier']->id] = $val;
         else
          $carriers[$key] = $val;
    }
    Configuration::updateValue('ZBOZI_CARRIERSG', json_encode($carriers));
}    
    
public  function GetSetting($key) {
     switch($key) {
         case "feeds":  return $this->feeds; 
         case "feeddir": return $this->feeddir;
         case "avilability": return $this->availability;   
         case "avilability_later": return $this->availability_later;     
     }
       
    }

  public  function install()
    {   
 
   if(! $this->check_feeddir()) {
          return false;
   } 
   
   
     if(!$this->addSqlField('category','heureka_category' )  ||
        !$this->addSqlField('category','google_category' ) 
     ) {
         $this->warning[]='Nepodařilo se přidat pole Heureka Category resp. Google Category, proto nebyly instalovány soubory pro práci s tímto polem';   
     }
     else { 
         
         if(
        $this->installOverridesP(
             array(
  0=>array('source'=>_PS_MODULE_DIR_.$this->name.'/install/AdminCategoriesController.php',
           'target'=>_PS_OVERRIDE_DIR_.'controllers/admin/AdminCategoriesController.php',
           'targetdir'=>_PS_OVERRIDE_DIR_.'controllers/admin/'),
  1=>array('source'=>_PS_MODULE_DIR_.$this->name.'/install/Category.php',
           'target'=>_PS_OVERRIDE_DIR_.'classes/Category.php',
           'targetdir'=>_PS_OVERRIDE_DIR_.'classes/')
  )
        ) )
          if(file_exists(_PS_ROOT_DIR_.'/cache/class_index.php'))
      unlink(_PS_ROOT_DIR_.'/cache/class_index.php');
   
     }
  
    
    if(is_array($this->warning) && count($this->warning) ) {
       $protocol =implode("\n", $this->warning);    
        Configuration::updateValue('ZBOZI_PROTOCOL', $protocol);
    }
   Configuration::updateValue(  'ZBOZI_SEZNAM', 1);
    Configuration::updateValue(  'ZBOZI_HEUREKA', 1);
     Configuration::updateValue(  'ZBOZI_PERPASS', 10000);
      Configuration::updateValue(  'ZBOZI_NEXTROUND', 0);
    
      Configuration::updateValue(  'ZBOZI_PARTIAL_UNISTALL', 1);
      Configuration::updateValue(  'ZBOZI_ROUND_PRICES', 1);
  
    for($i=0; $i<count($this->text_fields); $i++) {
      Configuration::updateValue($this->text_fields[$i], $this->text_defaults[$i], true);    
    }
        if (!parent::install())
            return false;  
            
      if(! $this->registerHook('actionCarrierUpdate'))
       return false;
       
       $optr=array('name'=>array('poradi'=>1,'pouzit'=>1), 
                  'manufacturer'=>array('poradi'=>2,'pouzit'=>1), 
                  'reference'=>array('poradi'=>3,'pouzit'=>1),
                  'ean'=>array('poradi'=>4,'pouzit'=>0),
                  'custom'=>array('poradi'=>5,'pouzit'=>0,'custom'=>''));
                  
       $optim['heureka']['productname']=$optr;
       $optim['zbozi']['productname']=$optr;
       $optim['heureka']['product']=$optr;
       $optim['zbozi']['product']=$optr;
       Configuration::updateValue('ZBOZI_OPTIM', json_encode($optim)); 
  
        return true;
   }

 
   
   public function uninstall()
    {
        
         for($i=0; $i<count($this->text_fields); $i++) {
             if (!Configuration::deleteByName($this->text_fields[$i]))
               return false; 
          }
   if((int)Configuration::get("ZBOZI_PARTIAL_UNISTALL") == 0) { 
       $this->removeSqlField('category', "heureka_category");  
       $this->removeSqlField('category', "google_category");            
   }          
                          
        if (!$this->unregisterHook('actionCarrierUpdate')  
            OR !parent::uninstall())
            return false;
   

    $this->unistallOverrides(
            array(
              0=>array('source'=>_PS_MODULE_DIR_.$this->name.'/install/AdminCategoriesController.php',
                       'target'=>_PS_OVERRIDE_DIR_.'controllers/admin/AdminCategoriesController.php',
                       'targetdir'=>_PS_OVERRIDE_DIR_.'controllers/admin/'),
              1=>array('source'=>_PS_MODULE_DIR_.$this->name.'/install/Category.php',
                       'target'=>_PS_OVERRIDE_DIR_.'classes/Category.php',
                       'targetdir'=>_PS_OVERRIDE_DIR_.'classes/')
              )
    );   
    if((int)Configuration::get('ZBOZI_CATSPERPRODUCT') ==1 ) {
       $this->unistallExtendedProduct();  
     }       
        
    
    if(file_exists(_PS_ROOT_DIR_.'/cache/class_index.php'))
      unlink(_PS_ROOT_DIR_.'/cache/class_index.php');
   
    $confarr=array('ZBOZI_AVAILABILITY','ZBOZI_AVAILABILITY_MODE','ZBOZI_AVAILABILITY_LATER',
    'ZBOZI_CARRIERS','ZBOZI_CARRIERSG','ZBOZI_CATSPERPRODUCT', 'ZBOZI_CARRIERSCOD','ZBOZI_CURRENT_STATE',
    'ZBOZI_DOPRAVA_ON','ZBOZI_DOPRAVAG_ON', 
    'ZBOZI_FORBIDDEN','ZBOZI_GATTRIBUTES','ZBOZI_GOOGLE','ZBOZI_HEUREKA', 'ZBOZI_NEXTROUND',
    'ZBOZI_OPTIM','ZBOZI_PARTIAL_UNISTALL','ZBOZI_PROTOCOL','ZBOZI_PERPASS','ZBOZI_ROUND_PRICES',
    'ZBOZI_SEZNAM', 'ZBOZI_SKLADEM','ZBOZI_SKLADzb','ZBOZI_START',    
    'ZBOZI_TRANSFORMED','ZBOZI_TRANSFORMED_COUNT','ZBOZI_UNIQUE_ID'
   );  
   foreach($confarr as $conf) {
   	    Configuration::deleteByName($conf);
   }

 return true;
    }
    
    
    protected function _postValidation()
    {
        if (isset($_POST['btnSubmit']))
        {
         ;
        }
    }

    protected function _postProcess()
    {
    	if(Tools::isSubmit('cmd_feeds')) {
    		 reset ($this->feeds);
            
            foreach($this->feeds as $feed) {
               $this->feedsUsed[$feed]= intval(Tools::getValue($feed));
               $key="ZBOZI_".strtoupper($feed);
               Configuration::updateValue($key, $this->feedsUsed[$feed]);  
            }
            $keys=array('ZBOZI_NEXTROUND','ZBOZI_PERPASS');
            foreach($keys as $key) { 
			  	   Configuration::updateValue($key, (int)Tools::getValue($key)); 
			}
			  $this->showSaveResult();
		}
		
		if(Tools::isSubmit('cmd_general')) {
			  $keys=array('ZBOZI_PARTIAL_UNISTALL', 'ZBOZI_SKLADEM', 'ZBOZI_ROUND_PRICES',);
			  foreach($keys as $key) {
			  	   Configuration::updateValue($key,(int) Tools::getValue($key)); 
			  }
			  $keys=array('ZBOZI_DESCRIPTION','ZBOZI_IMG','ZBOZI_CPC','ZBOZI_FORBIDDEN');
			  foreach($keys as $key) {
			  	   Configuration::updateValue($key, Tools::getValue($key)); 
			  }
			  
			  $keys=array( 'ZBOZI_ALLOWTAGS' );
			  foreach($keys as $key) {
			  	   Configuration::updateValue($key, Tools::getValue($key), true); 
			  }
			    $this->showSaveResult();
		}
		
		if(Tools::isSubmit('cmd_dostupnost')) {
			  $keys=array('ZBOZI_AVAILABILITY_MODE','ZBOZI_AVAILABILITY_LATER','ZBOZI_AVAILABILITY');
			  foreach($keys as $key) {
			  	   Configuration::updateValue($key, (int)Tools::getValue($key)); 
			  }
			  $this->availability =intval($_POST['ZBOZI_AVAILABILITY']); 
			  $this->availability_later =intval($_POST['ZBOZI_AVAILABILITY_LATER']);
			  $this->availability_mode= intval($_POST['ZBOZI_AVAILABILITY_MODE']); 
			    $this->showSaveResult();  
		}
		
		if(Tools::isSubmit('cmd_heureka')) {
			  Configuration::updateValue('ZBOZI_DOPRAVA_ON', intval(Tools::getValue('ZBOZI_DOPRAVA_ON')));  
         
           $carriers=array();
           $cods=array();
            while(list($key,$val)=each($_POST['carrier'])){
              $carriers[$key]=$val;
              $cods[$key]=(float)$_POST['carriercod'][$key];
            }  
             $this->carriers=$carriers;
             $this->cods=$cods;
             Configuration::updateValue('ZBOZI_CARRIERS', json_encode($carriers)); 
             Configuration::updateValue('ZBOZI_CARRIERSCOD', json_encode($cods)); 
            
           
             Configuration::updateValue('ZBOZI_TRANSFORMED_COUNT', intval(Tools::getValue('ZBOZI_TRANSFORMED_COUNT')));  
            $transformed=array();
      
              for($i=0;$i<count($_POST['attr']); $i++) {
                  if(isset($_POST['attr'][$i]) && strlen($_POST['attr'][$i]) && strlen($_POST['param'][$i]) )
                       $transformed[]=array(0=>$_POST['attr'][$i], 1=>$_POST['param'][$i]);
            }  
            Configuration::updateValue('ZBOZI_TRANSFORMED', json_encode($transformed)); 
            
		    $this->updateOptim('heureka');
              $this->showSaveResult();
		   }
		   
		   if(Tools::isSubmit('cmd_google')) {
		   	      $googleAttributes=array();
		   if(isset($_POST['gattr'])) {
	            for($i=0;$i<count($_POST['gattr']); $i++) {
            		$googleAttributes[$_POST['gattr'][0]]='color';
            		$googleAttributes[$_POST['gattr'][1]]='material';
            		$googleAttributes[$_POST['gattr'][2]]='pattern';
            		$googleAttributes[$_POST['gattr'][3]]='size';
				}
		   }
             Configuration::updateValue('ZBOZI_GATTRIBUTES', json_encode($googleAttributes)); 
             Configuration::updateValue('ZBOZI_DOPRAVAG_ON', intval(Tools::getValue('ZBOZI_DOPRAVAG_ON'))); 
             
               $carriers=array();
            if(isset($_POST['carrierG'])) {
            while(list($key,$val)=each($_POST['carrierG'])){
              $carriers[$key]=$val;
            }  
			}
             $this->carriersG=$carriers;
             Configuration::updateValue('ZBOZI_CARRIERSG', json_encode($carriers)); 
             
             $this->showSaveResult();
             
		   }
    	   
    	    if(Tools::isSubmit('cmd_seznam')) {
    	    	 $this->updateOptim('zbozi');
    	    	 Configuration::updateValue('ZBOZI_SKLADzb', intval(Tools::getValue('ZBOZI_SKLADzb'))); 
    	    	 $this->showSaveResult();
			}
       
        if(Tools::getValue('cmd_clear')) {
            Configuration::updateValue('ZBOZI_PROTOCOL', '');  
        }
        
        if(Tools::isSubmit('cmdHeurekaCats'))
        {
            Configuration::updateValue('ZBOZI_PARTIAL_UNISTALL', intval(Tools::getValue('ZBOZI_PARTIAL_UNISTALL')));  
         
            if((int)Configuration::get('ZBOZI_CATSPERPRODUCT') == 0) {
               
                $ZBOZI_CATSPERPRODUCT=1;
                $path= _PS_ADMIN_DIR_.'/themes/default/template/controllers/products/associations.tpl';
                if(file_exists($path)) {
                if(!$this->isAlreadyIncluded($path, 'heureka.tpl')) 
                   file_put_contents($path, "{include file=\"controllers/products/heureka.tpl\"}", FILE_APPEND);
                }
                else  {
                $ZBOZI_CATSPERPRODUCT=0;
                }
                
                $path= _PS_ADMIN_DIR_.'/themes/default/template/controllers/products/informations.tpl';
                if(file_exists($path)) {
                if(!$this->isAlreadyIncluded($path, 'zbozi_text.tpl')) 
                file_put_contents($path, "{include file=\"controllers/products/zbozi_text.tpl\"}", FILE_APPEND);
                }
                else  {
                $ZBOZI_CATSPERPRODUCT=0;
                }

                if($ZBOZI_CATSPERPRODUCT) {       
                    
                    if(!$this->addSqlField('product','heureka_category')) {
                    $this->warning[]='Nepodařilo se přidat pole Heureka Category k tabulce produkty, proto nebyly instalovány soubory pro práci s tímto polem';   
                    $ZBOZI_CATSPERPRODUCT=0;
                    }
                  if(!$this->addSqlField('product','zbozi_text')) {
                     $this->warning[]='Nepodařilo se přidat pole pro rozšiřující text k tabulce produkty, proto nebyly instalovány soubory pro práci s tímto polem';   
                    $ZBOZI_CATSPERPRODUCT=0;
                    }
                   if(!$this->addSqlField('product','heureka_cpc')) {
                     $this->warning[]='Nepodařilo se přidat pole pro heureka cpc k tabulce produkty, proto nebyly instalovány soubory pro práci s tímto polem';   
                    $ZBOZI_CATSPERPRODUCT=0;
                    }
                     if(!$this->addSqlField('product','max_cpc')) {
                     $this->warning[]='Nepodařilo se přidat pole pro max cpc k tabulce produkty, proto nebyly instalovány soubory pro práci s tímto polem';   
                    $ZBOZI_CATSPERPRODUCT=0;
                    }
                      if(!$this->addSqlField('product','max_cpc_search')) {
                     $this->warning[]='Nepodařilo se přidat pole pro max cpc search k tabulce produkty, proto nebyly instalovány soubory pro práci s tímto polem';   
                    $ZBOZI_CATSPERPRODUCT=0;
                    }
                 
                }
                
                if($ZBOZI_CATSPERPRODUCT) { 
                    if(!$this->installOverridesP(array(
                    0=>array('source'=>_PS_MODULE_DIR_.$this->name.'/install/Product.php',
                    'target'=>_PS_OVERRIDE_DIR_.'classes/Product.php',
                    'targetdir'=>_PS_OVERRIDE_DIR_.'classes/'),
                    1=>array('source'=>_PS_MODULE_DIR_.$this->name.'/install/heureka.tpl',
                    'target'=>_PS_ADMIN_DIR_.'/themes/default/template/controllers/products/heureka.tpl',
                    'targetdir'=>_PS_ADMIN_DIR_.'/themes/default/template/controllers/products/'),
                     2=>array('source'=>_PS_MODULE_DIR_.$this->name.'/install/zbozi_text.tpl',
                    'target'=>_PS_ADMIN_DIR_.'/themes/default/template/controllers/products/zbozi_text.tpl',
                    'targetdir'=>_PS_ADMIN_DIR_.'/themes/default/template/controllers/products/')
                    )) )
                    $ZBOZI_CATSPERPRODUCT=0;
                }   

                if($ZBOZI_CATSPERPRODUCT) {
                    if(file_exists(_PS_ROOT_DIR_.'/cache/class_index.php'))
                    unlink(_PS_ROOT_DIR_.'/cache/class_index.php');           
                    Configuration::updateValue('ZBOZI_CATSPERPRODUCT',1);   
                } 
            }
            elseif((int)Configuration::get('ZBOZI_CATSPERPRODUCT') ==1 ) {
                 $this->unistallExtendedProduct();
            } 
            
            $this->showSaveResult();
        }
        
 
        
       
    }
    
    private function showSaveResult() {
    	 if(is_array($this->warning) && count($this->warning)) {
           $this->_html .= '<div class="error">'.implode('<br />'.$this->warning).'</div>';  
        }
        else
          $this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('OK').'" />Změna byla uložena</div>';
	}

    protected function displayIntro($tabnum)
    {
        $display='<table><tr>';
        $display .= '<td>
        Modul pro export zboží do služby  heureka.cz, zbozi.cz a Google nákupy.  Feed Heureka je bez úprav použitelný
         i pro většinu dalších srovnávačů.<br />  <br />
        <ul>
        <li>použitelný i pro velké eshopy s tisíci kusů zboží</li>
        <li>podpora multishop</li>
        <li>vynechání vybraných produktů</li>
        <li>přesné párování Heureka</li>
        <li>export dopravy</li>
        <li>podrobné nastavení dostupnosti</li>';
        
        if($this->do_attributes==1)
        $display .= '<li><b>varianty zboží</b></li>  
        <li><b>cpc pro Heureka i Zbozi</b></li>  
        <li><b>zjednodušené mapování do Google a Heureka kategorií</b></li>  
        </ul> ';
        else
         $display .= '<li style="color:red"><b>nelze exportovat varianty zboží</b>. 
        <br /> Pro export variant a pohodlnější párování je potřeba modul 
       <b> <a href="http://prestahost.eu/prestashop-modules/cs/import-export/20-export-heureka-zbozi-varianty-produktu.html" target="_blank" style="color:blue">Zbožíplus</a></b>
       
       </li>  </ul></td></tr> ';
        
          $display .='<td><a href="http://www.prestahost.cz" target="_blank"><img src="../modules/'.$this->name.'/prestahost.gif"></a> 
       <br />  
       <b><a href="http://www.prestahost.cz" target="_blank">Prestahost.cz</a> </b>: 
       <ul>
       <li>česká podpora Prestashopu</li>
       <li>specializovaný hosting</li>
       <li>moduly na míru</li>
       <li><b>import xml feedů dodavatelů</b> (pokročilý modul zajišťující importy a pravidelné update zboží, informace na 
       <a href="mailto:info@prestahost.cz">info@prestahost.cz</a></li>
       </ul>
       </td></tr> ';  
       return $display.'</table>';
    }
               
    protected function displayGeneral($tabnum)
    { 
    $display=$this->displayIntro($tabnum).'<br />';    
     $url=$_SERVER['HTTP_HOST']."/modules/".$this->name."/feeds.php";
     if(Context::getContext()->shop->isFeatureActive())
        	$url.='?id_shop='.Context::getContext()->shop->id;
    $display .= 'Pro vytvoření feedu je potřeba spouštět skript <br /> <a href="http://'.$url.' " target="_blank" style="color:blue">'.$url.'</a> - viz. záložka feedy <br /><br />';   
    $display.='<form action="'.$_SERVER['REQUEST_URI'].'" method="post">';
    $display.='<input type="hidden" name="currentTab" value="'.$tabnum.'" />';
   $display.=' <fieldset><legend>Základní nastavení</legend>';  
    $display.='<p class="napoveda">
       	   <a class="napoveda" href="http://prestahost.eu/navody/index.php/4-karta-zakladni-nastaveni" target="_blank">Nápověda</a>
       	   </p>';
       


            $checkbox="<br /><input type=\"checkbox\" name=\"ZBOZI_SKLADEM\" value='1' style=\"color:blue\"";
            if(Configuration::get("ZBOZI_SKLADEM"))
                 $checkbox.=" checked=\"checked\" ";       

            $checkbox.="/> Exportovat pouze zboží skladem"; 
       
            if(!(int)Configuration::get('PS_STOCK_MANAGEMENT')) {
                  
            $checkbox.=" <b>POZOR řízení skladu je vypnuto. Zaškrtněte pouze pokud opravdu udržujete číselné zásob</b> ";


            } 
         $display .=$checkbox." &nbsp;";

                   
          
               $checkbox="<input type=\"checkbox\" name=\"ZBOZI_ROUND_PRICES\" value='1' style=\"color:blue\"";
            if(Configuration::get("ZBOZI_ROUND_PRICES"))
                 $checkbox.=" checked=\"checked\" ";
            $checkbox.="/>Ceny zaokrouhlovat jako v eshopu"; 
 $display .=$checkbox."<br /><br />";
               
       
       
         $display .='
         Velikosti obrázků: <select name ="ZBOZI_IMG">';
         
       $images=ImageType::getImagesTypes('products');
       $selimage=Configuration::get("ZBOZI_IMG");
       if(empty($selimage))
         $selimage=$images[0]['name'];
        foreach($images as $image) {
            if($image['height'] < 800 &&  $image['width'] < 800) {
    $display .="<option value=\"".$image['name']."\"";
              if($image['name'] == $selimage)
      $display.=" selected=\"selected\"";
    $display.=">".$image['name'].' ('.$image['width'].'x'.$image['height'].")</option>";
            }
        }    
        $display .='</select>
        vyberte jakou velikost obrázků ve feedu chcete použít
        <br /><br />';
        
     $display .='
        Pole pro popis: <select name ="ZBOZI_DESCRIPTION">';
        $keys=array('description_short', 'description');
        foreach($keys as $key) {
    $display .="<option value=\"$key\"";
               if($key == Configuration::get('ZBOZI_DESCRIPTION'))
      $display.=" selected=\"selected\"";
    $display.=">$key</option>";
        }    
        $display .='</select><br />';
        
       $display .='
        Dovolené html tagy:   <input  type="text" name="ZBOZI_ALLOWTAGS" value="'.Configuration::get('ZBOZI_ALLOWTAGS').'" /> oddělené čárkou    <br />';
      
         $display .='Vynechané produkty - ID produktů oddělené čárkou: <br /> <textarea  rows="4" cols="60" name="ZBOZI_FORBIDDEN" />'.Configuration::get("ZBOZI_FORBIDDEN").'</textarea><br /><br />';
      
      if(!$this->do_attributes) {  
       $display .='
        Heureka CPC: <select name ="ZBOZI_CPC">';
        $keys=array('', 'wholesale_price' , 'manufacturer_reference', 'reference', 'ean13' , 'upc');
        $vals=array('nic', 'nákupní cena ' , 'kód zboží dodavatele', 'kód zboží', 'EAN13 nebo JAN', 'UPC');
        $counter=0;
        foreach($keys as $key) {
    $display .="<option value=\"$key\"";
               if($key == Configuration::get("ZBOZI_CPC"))
      $display.=" selected=\"selected\"";
    $display.=">{$vals[$counter]}</option>";
        $counter++;
        }    
        $display .='</select>  <br />
        Pokud používáte heureka CPC, můžete pro ně využít některé z polí určených pro jiné účely, pokud to neovlivní funkci eshopu.
        Pokud je dané pole u produktu vyplněno, bude hodnota dosazena za <a href="http://sluzby.heureka.cz/napoveda/xml-feed/">HEUREKA_CPC</a>.
        
        <br />  <br />';   
      }  
      else {
      $display .='<input type="hidden" name="ZBOZI_CPC" value="0" />';    
          
      }
        
    
        
        $display .='
      
            </fieldset>
            <br />';
       $display.=$this->displayExtendedProduct();
        $display.=  '<input class="button" name="cmd_general" value="Uložit změny" type="submit" /> 
        </form>';
        return $display;
    }   
   
 
 	protected function displayFeeds($tabnum) {
 	    $url=$_SERVER['HTTP_HOST']."/modules/".$this->name."/feeds.php";
 	     $display='<form action="'.$_SERVER['REQUEST_URI'].'" method="post">';
 	     $display.='<input type="hidden" name="currentTab" value="'.$tabnum.'" />';
 		if(Context::getContext()->shop->isFeatureActive())
        	$url.='?id_shop='.Context::getContext()->shop->id;
         
        $display .='
            <fieldset>';
          $protocol=Configuration::get('ZBOZI_PROTOCOL');
          if($protocol && strlen($protocol)){
               $display .= '<span style="color:red"><h4>Instalace není úplná</h4>'.nl2br($protocol).'</span></br>
               Více informací najdete v <a href="http://prestahost.eu/navody/index.php/3-instalace" target="_blank">manuálu</a> </br>
               <input type="submit" name="cmd_clear" value="Vyčistit instalační protokol"><br /><br />';
               
          }  
  
            $display .= '<legend><img src="../img/admin/contact.gif" />'.$this->l('Feedy').'</legend>';
              $display.='<p class="napoveda">
       	   <a class="napoveda" href="http://prestahost.eu/navody/index.php/5-feedy" target="_blank">Nápověda</a>
       	   </p>';
             
                $display .= 'Pro vytvoření feedu je potřeba spouštět skript <br /> <a href="http://'.$url.' " target="_blank" style="color:blue">'.$url.'</a> <br />
               Požádejte svého poskytovatele o instalaci kronu spouštějícího  uvedené url. U velkých eshopů může být potřeba
               feedy vytvářet postupně opakovaným spouštěním skriptu.  
                <br />   
               
                 <br /> 
                Pokud hostujete na Prestahost.cz, zapněte si kron v záložce Hosting (hlavní lišta záložek)  <br />   
                <br />';
                
        if(!$this->check_feeddir()) {    
           $display .= "<b>POZOR adresář pro skladování feedů " .$_SERVER['HTTP_HOST']."/".$this->feeddir." nelze vytvořit nebo není zapisovatelný</b>";
        }        

         
           $display.='<br /> Počet produktů ke zpracování při jednom spuštění <input type="text" size=4 name="ZBOZI_PERPASS" value="'.Configuration::get('ZBOZI_PERPASS').'" /> <br /> <br />';   
           $display.='<br /> Minimmální časový odstup mezi dokončením feedů <input type="text" size=4 name="ZBOZI_NEXTROUND" value="'.Configuration::get('ZBOZI_NEXTROUND').'" /> minut <br /> <br />'; 
           $display.='<h4>Vytvářené feedy</h4>';  
      
           reset($this->feeds);
           
           foreach($this->feeds as $feed) {
            $file="zbozi_".$feed.".xml";
            $path="../".$this->feeddir."/$file";
            
            $checkbox="$feed - <input type=\"checkbox\" name=\"$feed\" value='1' style=\"color:blue\"";
            if($this->feedsUsed[$feed])
                 $checkbox.=" checked=\"checked\" ";
            $checkbox.="/>"; 
            
        
          
              $display .=$checkbox."<br />"; 
           }
           
           
           
             $shopName='';
             if(Context::getContext()->shop->isFeatureActive()) {
                require_once('cFeed.php');
             	$shopName = cFeed::addShopName(Context::getContext()->shop->id);
             	$shopName=Tools::substr($shopName,0,strlen($shopName)-1);
			 }
             
             $display .=  '<h4>Existující feedy '.$shopName.'</h4>Pokud je nějaký feed již vytvořen, zobrazuje se odkaz níže, kliknutím na něj se feed otevře v prohlížeči. 
               Tím zjistíte url které je potřeba zadat do administrace zboží nebo heureka.<br />';
             
              $feedscreated=scandir(_PS_ROOT_DIR_.'/'.$this->feeddir);  
              
              foreach($feedscreated as $created) {
                //  $display.=$created;
                  $koncovka=substr($created, strrpos($created, '.'));
                  if($koncovka == '.xml') {
                  if (!(strpos($created, 'zbozi') === false)) {
                    if($this->testForShopname($shopName, $created)) {
                    $url="http://".$_SERVER['HTTP_HOST']."/".$this->feeddir."/".$created;
                    $display.="<a href=\"$url\" target='_blank' style='color:blue'>$url</a> ".date('d.m.Y H:i', filemtime("../".$this->feeddir.'/'.$created));
                    if(file_exists(_PS_ROOT_DIR_.'/'.$this->feeddir.'/'.$created.'.zip')) {
                    	   $url.='.zip';
                    $display.=" zip:<i><a href=\"$url\" target='_blank' style='color:blue'>$url</a> ".date('d.m.Y H:i', filemtime("../".$this->feeddir.'/'.$created.'.zip')).'</i>';	   
					}
                    $display.='<br />';  
					}   
				  }
                  }
              } 
      
                    $display.='</fieldset><br />';  
        
       
     
      
       
      
       $display.='<br /> <input type="submit" class="button" name="cmd_feeds" value="Uložit" /></form>';
       return $display;
	}
	
	private function testForShopname($shopName, $created) {
		if(empty($shopName))
		  return true;
		  
		if(strpos($created, $shopName) === false)
		  return false;
		  
		return true;
		
	}
	
	protected function displayHeurekaMapBasic() {
		  $display.='
             <p>
             Tímto způsobem můžete snadno dosáhnout spárování svého zboží na Heureka.cz. K tomu využijete pole Heureka category které najdete v administraci (karta Kategorie).
             </p>
             <p>
             Do pole se zadávejte  hotnoty   CATEGORY_FULLNAME podle
             <a href="http://www.heureka.cz/direct/xml-export/shops/heureka-sekce.xml" target="_blank">Specifikaci Heureka</a>. Příklad:
             "Heureka.cz | Oblečení a móda | Dětské oblečení | Dětské plavky"
             </p>';  
             
          $display.='<p><b>V placené verzi navíc</b>: 
          <ul>
           <li> centrální editace pole Heureka category s našeptávačem kategorií Heureky.</li>
            <li> možnost párovat i na úrovni jednotlivých produktů.</li>
          </ul></p>'; 
          return $display;
	}
	
		protected function displayHeurekaMapAdvanced() {
			
		  $display='<a  class="mapbutton" href="#" onclick="fbox(\'heureka\');return false">'.$this->l('Namapovat kategorie').'</a><br />';
          $display.='<p>Po kliknutí se objeví centrální editor s našeptávačem.
             Tímto způsobem můžete snadno dosáhnout spárování svého zboží na Heureka.cz.
              Jedná se o pohodlnější alternativu k vyplňování pole Heureka category v  kartě individuálních kategorií (Katalog - Kategorie. 
             </p>
             <p>
            Pokud potřebujete upřesnit párování pro některé produkty, aktivujte nejprve "Rozšířené vlastnosti" v záložce "Základní nastavení". Pole se pak objeví v podzáložce "Associace" (Katalog - Produkty - Detail produktu)
             </p>';  
    
          return $display;
	}
	
	
	protected function displayHeureka($tabnum) {
		 $display='<form action="'.$_SERVER['REQUEST_URI'].'" method="post">';
	     $display.='<input type="hidden" name="currentTab" value="'.$tabnum.'" />';
	     
	       
	      $display.='<fieldset><legend>Mapovování kategorií Heureka</legend>';
	        $display.='<p class="napoveda">
       	   <a class="napoveda" href="http://prestahost.eu/navody/index.php/7-modul-zbozi-karta-heureka" target="_blank">Nápověda (heureka)</a>
       	   <a class="napoveda" href="http://prestahost.eu/navody/index.php/8-modul-zbozi-mapovani-kategorii" target="_blank">Nápověda (mapování kategorií)</a>
       	   </p>';
       	   
       	   
	           if($this->do_attributes)
             $display.=$this->displayHeurekaMapAdvanced();
           else
              $display.=$this->displayHeurekaMapBasic();
	      $display.='</fieldset>'; 
	      
	      
		 $display.=$this-> displayDoprava();
		 if($this->do_attributes) {
           $display .= $this->displayAttributes().'<br /><br />';
       }
		$display.=$this-> displayOptimisationHeureka();
		 $display.=  '<input class="button" name="cmd_heureka" value="Uložit změny" type="submit" /> 
        </form>';
        return $display;
	}
	
	protected function displaySeznam($tabnum) {
		 $display='<form action="'.$_SERVER['REQUEST_URI'].'" method="post">';
		 $display.='<input type="hidden" name="currentTab" value="'.$tabnum.'" />';
		$display.=$this-> displayOptimisationZbozi();
		$appr2=(int)Configuration::get('ZBOZI_SKLADzb');
		$labels=array('Nepoužívat','1 den jako ihned','Text skladem vždy jako ihned');
		$display.='<fieldset><legend>Rozšířená interpretace skladu:</legend>';
		for($i=0;$i<count($labels);$i++) {
			$display.='<input type="radio" name="ZBOZI_SKLADzb" value="'.$i.'"';
			if($i == $appr2)
			  $display.=' checked="checked"';
			$display.='/> '.$labels[$i].'<br />';
		}
		$display.='upravuje číslo nalezené v řetězci "available_now"
		
		</fieldset>';
		 $display.=  '<input class="button" name="cmd_seznam" value="Uložit změny" type="submit" /> 
        </form>';
        return $display;
	}
	
	protected function displayGoogle($tabnum) {
		$display='<form action="'.$_SERVER['REQUEST_URI'].'" method="post">';
		$display.='<input type="hidden" name="currentTab" value="'.$tabnum.'" />';
		if($this->do_attributes) {
		  $display.='<fieldset><legend>Mapování kategorií Google</legend>';
		    $display.='<p class="napoveda">
       	   <a class="napoveda" href="http://prestahost.eu/navody/index.php/9-modul-zbozi-karta-google" target="_blank">Nápověda (google)</a>
       	   <a class="napoveda" href="http://prestahost.eu/navody/index.php/8-modul-zbozi-mapovani-kategorii" target="_blank">Nápověda (mapování kategorií)</a>
       	   </p>';
		  $display.='<a  class="mapbutton" href="#" onclick="fbox(\'google\');return false">'.$this->l('Namapovat kategorie').'</a><br />';
          $display.='<p>Po kliknutí se objeví centrální editor s našeptávačem.
             Tímto způsobem můžete snadno dosáhnout spárování svého zboží na Google Nákupy.
             </p></fieldset>';  
    
		
		
		  $sql='SELECT name FROM '._DB_PREFIX_.'attribute_group_lang WHERE id_lang='.(int)Configuration::get('PS_LANG_DEFAULT');
		  $skupiny=Db::getInstance()->executeS($sql);
		 
		  $display.='<fieldset><legend>Kombinace zboží Google</legend>';
             $display.='Google používá pro kombinace pouze následující vlastnosti:
             ‘color’, ‘material’, ‘pattern’, ‘size’. Pokud se odpovídající skupiny vlastností na eshopu jmenují jinak,
             je potřeba je přiřadit níže.<br />';
             
             if($skupiny && is_array($skupiny)) {
                $display.='Skupiny vlastností v eshopu jsou: ';
                foreach($skupiny as $skupina) {
                	$display.=' | '.$skupina['name'];
				}
			 }
             
            $display.='<table><tr><td width="200px">Jméno Google</td><td width="200px">Název attributu v eshopu</td>';
            $keys=array('color', 'material', 'pattern', 'size');
            $i=0;
            $googleAttributes=json_decode(Configuration::get('ZBOZI_GATTRIBUTES'), true);
            if(is_array($googleAttributes))
            $googleAttributes= array_flip($googleAttributes); 
            foreach($keys as $key) {
            	$val=isset($googleAttributes[$key])?$googleAttributes[$key]:$key;
                $display.='<tr><td>'.$key.'</td><td><input type="text" name="gattr['.$i.']" value="'.$val.'"/></td></tr>';
                $i++;
			}
            $display.='</table></fieldset>';
           
        }
        else {
        	  $display.='Modul umožňuje tvorbu feedu pro Google nákupy';
        	  $display.= 'V placené verzi navíc:<ul>
        	  <li>Export kombinací zboží</li>
        	  <li>Převod vlastností produktů podle požadavků Google</li>
        	  <li>Centrální mapování do kategorií podle Google taxonomie</li>
        	  </ul>';
		}
		
       $display.='<fieldset><legend>Doprava</legend>
       Zahrnout informace o dopravě pro feeed Google
       <input type="checkbox" value=1 name="ZBOZI_DOPRAVAG_ON"';
       if(Configuration::get("ZBOZI_DOPRAVAG_ON"))
                $display .=' checked=\"checked\"';
        $display.='/><br /><br />';
        
        
        
        $carriers=Carrier::getCarriers(Configuration::get("PS_LANG_DEFAULT", true));
        $display.='Dopravci pro export - podle směrnic Google by neměli být exportování
        dopravci typu "osobní odběr"</span><br />.
        ';
      $display.='<table><td>Dopravce</td><td>Povolen</td></tr>'; 
       foreach($carriers as $carrier) {
       	   $checked='';
       	   if(isset($this->carriersG[$carrier['id_carrier']])) {
                  $checked=  ' checked="checked"';
            }
           
            
             $display.='<tr>'; 
            $display.='<td>'.$carrier['name'].'</td><td>
            <input type="checkbox" name="carrierG['.$carrier['id_carrier'].']" value="1" '.$checked.'></td>';
  
            $display.='</tr>'; 
       }
       $display.='</table><br /></fieldset>';  
        
        
        
		  $display.=  '<input class="button" name="cmd_google" value="Uložit změny" type="submit" />';
        $display.= '</form>'; 
        return $display;
	}
   
  protected function displayExtendedProduct() {
     $display =' <fieldset><legend>Rozšířené vlastnosti</legend>';
     $display.='Administrace::Katalog::Produkty::Detail produktu:
     <ul>
     <li>Podzáložka Associace: Přidá pole pro <b>Heureka kategorie</b> - párování heureka kategorií pro individuální produkty</li>
     <li>Podzáložka Informace: Přidá pole pro <b>Text do srovnávačů</b> - rozšiřující text do tagu PRODUCT  v xml exportu Heureka a Zboží, individuální
     pro jednotlivé produkty</li>
     <li>Podzáložka Informace: Přidá samostatné pole pro <b>Heureka CPC</b></li>
     </ul>';
   
          if(!$this->do_attributes) {   
               $display.= '<b>Pro tuto funkci je potřeba placená verze modulu </b><br />';
      
               
                        $checkbox="<br /><br />  <input type=\"checkbox\" name=\"ZBOZI_PARTIAL_UNISTALL\" value='1' style=\"color:blue\"";
            if(Configuration::get("ZBOZI_PARTIAL_UNISTALL"))
                 $checkbox.=" checked=\"checked\" ";
            $checkbox.="/>Při odinstalaci modulu nebo zakázání rozšířených vlastností produktů  nemazat uložené hodnoty   &nbsp;"; 
           $display.= $checkbox." &nbsp;<br /><br />";  
          }
          else {
          if((int)Configuration::get('ZBOZI_CATSPERPRODUCT') ==1 )
           $display.='Je již povoleno: <input type="submit" name="cmdHeurekaCats" value="Zakázat" />';
          else
           $display.='Není povoleno: <input type="submit" name="cmdHeurekaCats" value="Povolit" />';
            $checkbox="<br /><br />  <input type=\"checkbox\" name=\"ZBOZI_PARTIAL_UNISTALL\" value='1' style=\"color:blue\"";
            if(Configuration::get("ZBOZI_PARTIAL_UNISTALL"))
                 $checkbox.=" checked=\"checked\" ";
            $checkbox.="/>Při odinstalaci modulu nebo zakázání rozšířených vlastností produktů  nemazat uložené hodnoty   &nbsp;"; 
           $display.= $checkbox." &nbsp;<br /><br />";
           
        
          ;
      $display.='</fieldset>';
          
       
          }   
            return $display;  
  } 
  
   protected function displayOptimisationHeureka() {
      $optim=json_decode(Configuration::get('ZBOZI_OPTIM'), true); 
               
      $display =' <fieldset><legend>Nastavení tagů PRODUCT a PRODUCTNAME</legend>';
      
     $display.='<table  cellpadding="10"><tr> <td>Feed</td><td>Tag feedu</td><td>Hodnota</td> <td>Pořadí</td><td>Použít?</td> </tr>'; 
     
      $feednames=array('Heureka');
      $tagnames=array('PRODUCTNAME', 'PRODUCT');
      
      for($i=0;$i<count($feednames);$i++) {
            $fname=  strtolower($feednames[$i]);
              for($j=0;$j<count($tagnames);$j++) {
             $tagname=strtolower($tagnames[$j]);
            $optr=$optim[$fname][$tagname];
            
            $checked=(isset($optr['name']['pouzit']) && (int)$optr['name']['pouzit'])?' checked="checked"':'';
            $disabled='';
            if($i == 0) {
               $checked=' checked="checked"';
               $disabled=' DISABLED';
			} 
  
            $display.='<tr> <td style="padding:10px">'.$feednames[$i].'</td><td style="padding:10px">'.$tagnames[$j].'</td><td>jméno produktu</td><td><input type="text" size="4" name="optimporadi_'.$fname.'_'.$tagname.'_name" value="'.(int)$optr['name']['poradi'].'" /></td> <td><input type="checkbox" name="optimuse_'.$fname.'_'.$tagname.'_name" value="1" '.$checked.$disabled.'/></td> </tr>'; 

            $checked=(isset($optr['manufacturer']['pouzit']) && (int)$optr['manufacturer']['pouzit'])?' checked="checked"':'';
            $display.='<tr> <td style="padding:10px">'.$feednames[$i].'</td><td style="padding:10px">'.$tagnames[$j].'</td><td>jméno výrobce</td><td><input type="text" size="4" name="optimporadi_'.$fname.'_'.$tagname.'_manufacturer" value="'.(int)$optr['manufacturer']['poradi'].'" /></td> <td><input type="checkbox" name="optimuse_'.$fname.'_'.$tagname.'_manufacturer" value="1" '.$checked.'"/></td> </tr>'; 

            $checked= (isset($optr['reference']['pouzit']) && (int)$optr['reference']['pouzit'])?' checked="checked"':'';
            $display.='<tr> <td style="padding:10px">'.$feednames[$i].'</td><td style="padding:10px">'.$tagnames[$j].'</td><td>kód produktu</td><td><input type="text" size="4" name="optimporadi_'.$fname.'_'.$tagname.'_reference" value="'.(int)$optr['reference']['poradi'].'" /></td> <td><input type="checkbox" name="optimuse_'.$fname.'_'.$tagname.'_reference" value="1" '.$checked.'"/></td> </tr>'; 

            $checked=(isset($optr['ean']['pouzit']) &&(int)$optr['ean']['pouzit'])?' checked="checked"':'';
            $display.='<tr> <td style="padding:10px">'.$feednames[$i].'</td><td style="padding:10px">'.$tagnames[$j].'</td><td>EAN 13</td><td><input type="text" size="8" name="optimporadi_'.$fname.'_'.$tagname.'_ean" value="'.(int)$optr['ean']['poradi'].'" /></td> <td><input type="checkbox" name="optimuse_'.$fname.'_'.$tagname.'_ean" value="1"'.$checked.'"/></td> </tr>'; 

            $checked=(isset($optr['custom']['pouzit']) && (int)$optr['custom']['pouzit'])?' checked="checked"':'';
            $display.='<tr> <td style="padding:10px">'.$feednames[$i].'</td><td style="padding:10px">'.$tagnames[$j].'</td><td>Vlastní hodnota:<br /> <input type="text" size="22" name="optimcustom_'.$fname.'_'.$tagname.'_custom" value="'.(string)$optr['custom']['custom'].'" /> </td><td><input type="text" size="8" name="optimporadi_'.$fname.'_'.$tagname.'_custom" value="'.(int)$optr['custom']['poradi'].'" /></td> <td><input type="checkbox" name="optimuse_'.$fname.'_'.$tagname.'_custom" value="1" '.$checked.'"/></td> </tr>'; 
              }
      
      }
      
         
     $display .='</table>
     <h4>Individuální rozšíření názvu produktů</h4>
     <ul>
     <li>V záložce Základní nastavení povolte rozšířené vlastnosti produktů</li>
     <li>Katalog - Produkty - Karta produktu - podzáložka Informace - vyplňte rozšiřující text</li>
     <li>Vyplněný text bude přidán  do elementu PRODUCT</li>
     </ul>
     <br />'; 
      
      $display .='</fieldset>';
       return $display;
   } 
   
   
     protected function displayOptimisationZbozi() {
      $optim=json_decode(Configuration::get('ZBOZI_OPTIM'), true); 
               
      $display =' <fieldset><legend>Nastavení tagů PRODUCT a PRODUCTNAME</legend>';
      
     $display.='<table  cellpadding="10"><tr> <td>Feed</td><td>Tag feedu</td><td>Hodnota</td> <td>Pořadí</td><td>Použít?</td> </tr>'; 
     
      $feednames=array('Zbozi');
      $tagnames=array('PRODUCTNAME', 'PRODUCT');
      
      for($i=0;$i<count($feednames);$i++) {
            $fname=  strtolower($feednames[$i]);
              for($j=0;$j<count($tagnames);$j++) {
             $tagname=strtolower($tagnames[$j]);
            $optr=$optim[$fname][$tagname];
            
            $checked=(isset($optr['name']['pouzit']) && (int)$optr['name']['pouzit'])?' checked="checked"':'';
            $disabled='';
            if($i == 0) {
               $checked=' checked="checked"';
               $disabled=' DISABLED';
			} 
            
            
            $display.='<tr> <td style="padding:10px">'.$feednames[$i].'</td><td style="padding:10px">'.$tagnames[$j].'</td><td>jméno produktu</td><td><input type="text" size="4" name="optimporadi_'.$fname.'_'.$tagname.'_name" value="'.(int)$optr['name']['poradi'].'" /></td> <td><input type="checkbox" name="optimuse_'.$fname.'_'.$tagname.'_name" value="1" '.$checked.$disabled.'/></td> </tr>'; 

            $checked=(isset($optr['manufacturer']['pouzit']) && (int)$optr['manufacturer']['pouzit'])?' checked="checked"':'';
            $display.='<tr> <td style="padding:10px">'.$feednames[$i].'</td><td style="padding:10px">'.$tagnames[$j].'</td><td>jméno výrobce</td><td><input type="text" size="4" name="optimporadi_'.$fname.'_'.$tagname.'_manufacturer" value="'.(int)$optr['manufacturer']['poradi'].'" /></td> <td><input type="checkbox" name="optimuse_'.$fname.'_'.$tagname.'_manufacturer" value="1" '.$checked.'"/></td> </tr>'; 

            $checked= (isset($optr['reference']['pouzit']) && (int)$optr['reference']['pouzit'])?' checked="checked"':'';
            
            $display.='<tr> <td style="padding:10px">'.$feednames[$i].'</td><td style="padding:10px">'.$tagnames[$j].'</td><td>kód produktu</td><td><input type="text" size="4" name="optimporadi_'.$fname.'_'.$tagname.'_reference" value="'.(int)$optr['reference']['poradi'].'" /></td> <td><input type="checkbox" name="optimuse_'.$fname.'_'.$tagname.'_reference" value="1" '.$checked.'"/></td> </tr>'; 

            $checked=(isset($optr['ean']['pouzit']) &&(int)$optr['ean']['pouzit'])?' checked="checked"':'';
            $display.='<tr> <td style="padding:10px">'.$feednames[$i].'</td><td style="padding:10px">'.$tagnames[$j].'</td><td>EAN 13</td><td><input type="text" size="8" name="optimporadi_'.$fname.'_'.$tagname.'_ean" value="'.(int)$optr['ean']['poradi'].'" /></td> <td><input type="checkbox" name="optimuse_'.$fname.'_'.$tagname.'_ean" value="1"'.$checked.'"/></td> </tr>'; 

            $checked=(isset($optr['custom']['pouzit']) && (int)$optr['custom']['pouzit'])?' checked="checked"':'';
            $display.='<tr> <td style="padding:10px">'.$feednames[$i].'</td><td style="padding:10px">'.$tagnames[$j].'</td><td>Vlastní hodnota:<br /> <input type="text" size="22" name="optimcustom_'.$fname.'_'.$tagname.'_custom" value="'.(string)$optr['custom']['custom'].'" /> </td><td><input type="text" size="8" name="optimporadi_'.$fname.'_'.$tagname.'_custom" value="'.(int)$optr['custom']['poradi'].'" /></td> <td><input type="checkbox" name="optimuse_'.$fname.'_'.$tagname.'_custom" value="1" '.$checked.'"/></td> </tr>'; 
              }
      
      }
      
         
     $display .='</table>
     <h4>Individuální rozšíření názvu produktů</h4>
     <ul>
     <li>V záložce Základní nastavení povolte rozšířené vlastnosti produktů</li>
     <li>Katalog - Produkty - Karta produktu - podzáložka Informace - vyplňte rozšiřující text</li>
     <li>Vyplněný text bude přidán  do elementu PRODUCT</li>
     </ul>
     <br />';
      
      $display .='</fieldset>';
       return $display;
   } 
   
   protected function displayDostupnost($tabnum) {
   	   $display='<form action="'.$_SERVER['REQUEST_URI'].'" method="post">';
   	   	 $display.='<input type="hidden" name="currentTab" value="'.$tabnum.'" />';
      $display.=' <fieldset><legend>Dostupnost</legend>'; 
        $display.='<p class="napoveda">
       	   <a class="napoveda" href="http://prestahost.eu/navody/index.php/6-dostupnosti" target="_blank">Nápověda</a>
       	   </p>'; 
        $display .='Některé srovnávače cen, například Heuréka manuálně kontrolují obchody a
         porovnávají dostupnost z feedu s údaji u zboží nebo v dodacích podmínkách. Určitě se tedy vyplatí
         věnovat se správnému nastavení v této sekci<br /><br />';

          $display .='<input type="radio" name="ZBOZI_AVAILABILITY_MODE" value="0" '.$this->optAvailabilityMode(0).'/> 
           Zohlednit řízení skladu: Modul bude respektovat zda je nastaveno řízení skladu.
          
           <br />';
                    
           if((int)Configuration::get('PS_STOCK_MANAGEMENT')) {
             $display .=' <small> Řízení skladu je zapnuto   použije se číslovka z textu zobrazovaného pokud <b>je nebo respektive není zboží skladem</b>. Pokud v textu není číslovka, použije se výchozí hodnota </b></small></br>';   
           }
           else {
              $display .=' <small> Řízení skladu je vypnuto   použije se vždy  výchozí hodnota. Modul se nebude pokoušet hledat čísla v textech o dostupnosti.  Rízení skladu lze zapnout v Nastavení - Produkty </small></br>';   
           }
           
          $display .='<input type="radio" name="ZBOZI_AVAILABILITY_MODE" value="1" '.$this->optAvailabilityMode(1).'/> Vždy hledat číslo.
          <small>
           bez ohledu na řízení je hledána číselná hodnota v textu. V závislosti na počtu kusů
           se 
            zjišťuje se číslo z textu zadávaného <b> pokud je nebo není zboží skladem</b>. Pokud  v textu chybí číslo, použije se výchozí hodnota. Aby bylo možné texty dopsat (Detail produktu - Množství), je potřeba řízení skladu
           na chvíli zapnout</small><br />';
          $display .='<input type="radio" name="ZBOZI_AVAILABILITY_MODE" value="2" '.$this->optAvailabilityMode(2).'/> Nikdy nehledat číslo.  
           <small>Vždy se použije výchozí hodnota.</small><br /><br />';
        
        
        $display .=' Výchozí hodnota  <input  type="text" name="ZBOZI_AVAILABILITY" value="'.$this->availability.'" />    dnů (0 = skladem)  
          použije se pokud je nenulový počet kusů, nebo je zvoleno "Nikdy nehledat číslo".
         <br />   <br />';
         $display .=' Výchozí hodnota 2 <input  type="text" name="ZBOZI_AVAILABILITY_LATER" value="'.$this->availability_later.'" />    dnů  
           použije se pokud je nula kusů (nikoliv ale při nastavení "Nikdy nehledat číslo")
         <br />   <br />';
         
         
        
       
       
         $display .='Výjimky pro zpracování textu  pro zboží skladem, které se vyhodnotí jako okamžitě k odběru:<br /> 
         <ul>
         <li>text je roven "skladem"</li>
         <li>obsahuje "ihned" a neobsahuje číslovku</li> 
         <li>obsahuje číslovku 24</li>
         </ul>';
         
       
       $display .=' </fieldset>';
             $display.=  '<input class="button" name="cmd_dostupnost" value="Uložit změny" type="submit" /> 
        </form>';
        return $display;

   } 
    protected function displayAttributes() {
           $transformed=json_decode(Configuration::get('ZBOZI_TRANSFORMED'), true);
            $pocet=Configuration::get('ZBOZI_TRANSFORMED_COUNT')>0?Configuration::get('ZBOZI_TRANSFORMED_COUNT'):5;
           $display ='<fieldset><legend>Kombinace zboží Heureka</legend>';
            $display .='Kombinace zboží se vytvoří automaticky. Nicméně pro Heureka feed může být výhodné přemapovat některé
            Attributy do skupin vlastností. Vytvořené kombinace pak budou mít navíc jeden nebo více tagů PARAM. Více informací najdete v 
            <a href="http://prestahost.eu/navody/index.php/7-modul-zbozi-karta-heureka" target="_blank">manuálu</a>'
            ; 
            $display.='<table><tr><td width="200px">Název attributu v eshopu</td><td width="200px">Požadované jméno parametru pro Heureka</td>';
            for($i=0;$i<$pocet;$i++) {
            $val1=  isset($transformed[$i][0])?(string)$transformed[$i][0]:'';
            $val2=  isset($transformed[$i][1])?(string)$transformed[$i][1]:'';
            $display.='<tr><td><input type="text" name="attr['.$i.']" value="'.$val1.'"/></td>
                           <td><input type="text" name="param['.$i.']" value="'.$val2.'"/></td>
                      </tr>';
            }
            $display.='</table>
           <br />
           Počet řádek v tabulce   <input type="text" size=2 name="ZBOZI_TRANSFORMED_COUNT" value="'.$pocet.'"/>
            
            </fieldset>';
            
            
           
            
             
            return $display;
    }
    
    protected function displayDoprava() {
           $this->carriers=json_decode(Configuration::get('ZBOZI_CARRIERS'), true);
         $display ='<fieldset><legend>Doprava</legend>
       Zahrnout informace o dopravě pro feed Heureka: <input type="checkbox" value=1 name="ZBOZI_DOPRAVA_ON"';
       
       if(Configuration::get("ZBOZI_DOPRAVA_ON"))
                $display .=' checked=\"checked\"';
        $display.='/><br />';
       
       $heurekaCarriers=array('CESKA_POSTA','CESKA_POSTA_NA_POSTU','CSAD_LOGISTIK_OSTRAVA');
       $carriers=Carrier::getCarriers(Configuration::get("PS_LANG_DEFAULT", true));
        $display.='Do pole Dopravce heureka dopište aktuální <a href="http://sluzby.heureka.cz/napoveda/xml-feed/#DELIVERY" target="_blank">zkratky podporovaných dopravců</a> <br />
        Příklady: <br /> <span style="font:arial">CESKA_POSTA, CESKA_POSTA_NA_POSTU, PPL, DPD, DHL,  VLASTNI_PREPRAVA</span><br />.
        ';
      $display.='<table><td>Dopravce</td><td>Dopravce heureka</td><td>COD</td></tr>'; 
       foreach($carriers as $carrier) {
            $val='';
            $cod=0;
            if(isset($this->carriers[$carrier['id_carrier']])) {
                  $val=  $this->carriers[$carrier['id_carrier']];
                  $cod=  $this->cods[$carrier['id_carrier']];
            }
             $display.='<tr>'; 
            $display.='<td>'.$carrier['name'].'</td><td><input type="text" name="carrier['.$carrier['id_carrier'].']" value="'.$val.'"></td>';
            $display.='</td><td><input type="text" name="carriercod['.$carrier['id_carrier'].']" value="'.$cod.'"></td>';
            $display.='</tr>'; 
       }
       $display.='</table>'; 
       $display.='</fieldset>';
       return $display;
    }
    
      

   protected function optAvailabilityMode($value) {
       if($value==0 && ($value == $this->availability_mode || empty($this->availability_mode)))
         return " checked='checked'";
         
       if($value == $this->availability_mode)
           return " checked='checked'";
           
       return '';
           
   }
    
    protected function check_feeddir() {
      $dir="../".$this->feeddir;
      if(!is_dir($dir)) {
         mkdir($dir);
      } 
     if(!is_dir($dir)) {
         $this->_errors[]=$this->l('Důvod selhání: nepodařilo se vytvořit adresář ').$this->feeddir .'.';
         return 0;
     } 
     if(!is_writable($dir)) {
         chmod($dir, 0755);
     }
     if(!is_writable($dir)) {
                $this->_errors[]=$this->l('Důvod selhání: nelze zapisovat do adresáře ').$this->feeddir.'.';
            return 0;
     }
        return 1;
    /* chmod(
     $fp=fopen($dir."/test.txt", "w+");
     if(!
    */    
    }
    
 public   function getContent()
    {   
    	$this->context->controller->addCSS($this->_path.'css/zbozi.css', 'all');
        $this->_html = '<h2>'.$this->displayName.'</h2>';
        if(Shop::isFeatureActive() && Shop::getContextShopID(true) === null) {
    		$this->_html = '<h2>'.$this->displayName.'</h2>';
    		$this->_html .='Používáte multishop. 
    		<b>Pro nastavení specifické pro konkrétní shop do něj přepněte.</b>';
    		
		}
        if (!empty($_POST))
        {
            $this->_postValidation();
            if (!sizeof($this->_postErrors))
                $this->_postProcess();
            else
                 foreach ($this->_postErrors AS $err)
                    $this->_html .= '<div class="alert error">'. $err .'</div>';
        }
        else
            $this->_html .= '<br />';

        if($_SERVER['HTTP_HOST'] == 'localhost:8080')
		    $module_url='/';
		else {
		  $sql='SELECT id_shop_url FROM '._DB_PREFIX_.'shop_url WHERE id_shop='.(int)Context::getContext()->shop->id.' AND active=1';
		  $shopUrl =new ShopUrl(Db::getInstance()->getValue($sql));
		  $module_url=  $shopUrl->getURL();
		}
	
		$module_url.='modules/'.$this->name;
	    $tabs=array('general','feeds','dostupnost','heureka','seznam','google');
        $tabnames=array('Základní nastavení','Feedy','Dostupnosti','Heureka','Zbozi','Google');
        $this->_html .=  '<script language="JavaScript">
        <!--
        var currentTab='.(int)$this->currentTab.';    
        window.onload=function(){showTab(currentTab);}
          function showTab(num) {
              currentTab=num;
              var tabcount='. count($tabs).'
              for(i=0;i< tabcount;i++) {
                  var idtab= "#zbozitab"+i;
                  $(idtab).hide();
                   var navtab= "#navtab"+i;
                   $(navtab).removeClass("red"); 
              }
               var idtab= "#zbozitab"+num;
                $(idtab).show(); 
                 var navtab= "#navtab"+num;
                   $(navtab).addClass("red"); 
               return false;
          }
       
        function fbox(funkce) {
  
        if(funkce) {
        var url = "'.$module_url.'/categorymap.php?function="+funkce+"&id_shop='.Context::getContext()->shop->id.'"; 
        $.fancybox({
        type: "iframe",
        href: url,
        "width": "90%",
        "height": "75%",
        "autoScale" : false,
        "fitToView" :  false
        });
        }
        }
        //-->
        </script>';    
        
        
        $this->_html.='<div id="navcontainer"><ul id="navlist">';
       $class='red';
        for($i=0; $i < count($tabs); $i ++) {
 
           $this->_html.='<li style="float:left;padding-right:20px;" ><a  class="'.$class.'" id=navtab'.$i.' href="#" onClick="showTab('.$i.'); return false;">'.$tabnames[$i].'</a></li>'; 
           $class='';
        }
        $this->_html.='</ul></div><div style="clear:left"></div><br /><br />';
        $display='block';    
       for($i=0; $i < count($tabs); $i ++) {
           $function='display'.ucfirst($tabs[$i]);
           $this->_html.='<div id="zbozitab'.$i.'" style="display:'.$display.'">';
           $this->_html.=$this->{$function}($i);
           $this->_html.='</div>';
           $display='none';
		} 
        return $this->_html;
    }    
    
private function installOverridesP($overrides) {
      $retval=true;
       foreach($overrides as $override) {
       if(file_exists($override['target']) && filesize($override['target']) > 86 ) {
       	   if(! $this->canOverride($override['target'], $override['source'])) {
                $this->_postErrors[]='Soubor '.$override['target'].' již existuje a namůže být nahrazen, prosím upravte jej ručně';
                $retval=false; 
            }
            else
                return true;
       }
       if(!is_writable($override['targetdir'])) { 
           $this->_postErrors[]='Adresář '.$override['targetdir'].' není zapisovatelný'; 
           $retval=false; 
      }
      elseif(!copy($override['source'],$override['target'])) {
        $this->_postErrors[]='Nepodařilo se překopírovat '.$override['source'].' do '.$override['target'];   
        $retval=false; 
      }
   }
   return $retval;
}

// check if it is older version of module own override
private function canOverride($target, $source) {
  if(crc32(file_get_contents($target)) == crc32(file_get_contents($source))) {
  	  return true;
  }
  $s=file_get_contents($target);
  $pos=strpos($s, "renderForm16");
  if($pos > 0)
    return true;

  $t=file_get_contents($source);
  $max=Tools::strlen($t);
  $pos=strpos($s, "heureka_category");
  if($pos > 0 && Tools::strlen($s) < $max)
    return true;
 
  return false;
}

	private function unistallOverrides($overrides) {
		foreach($overrides as $override) {
				if(file_exists($override['target'])) {
					if($this->canOverride($override['target'],$override['source'])) {
					unlink($override['target']);
				}
			}
		} 
	}

private function addSqlField($tablename, $columnname) {
      $tablename=pSQL($tablename);
      $columnname=pSQL($columnname);
        $sql='SELECT column_name
                    FROM information_schema.columns 
                    WHERE table_schema = "'._DB_NAME_.'" 
                    AND table_name   = "'._DB_PREFIX_.$tablename.'"
                    AND column_name  = "'.$columnname.'"';
                    $column_exists=Db::getInstance()->getValue($sql);

                    if($column_exists == false) {

                    $sql='ALTER TABLE '._DB_PREFIX_.$tablename.' ADD '.$columnname.' VARCHAR (250) COLLATE utf8_czech_ci DEFAULT NULL';
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
                    } 


                    $sql='SELECT    '.$columnname.' FROM '._DB_PREFIX_.$tablename.'   WHERE 1 LIMIT 1';
                    $test=Db::getInstance()->Execute($sql); 


                    if(!$test) {
                     return false;
                    }
     return true;
}
	
private function removeSqlField($tablename, $columname) {  
           $tablename=pSQL($tablename);
            $columname=pSQL($columname);
  
                   $sql='SELECT column_name
                    FROM information_schema.columns 
                    WHERE table_schema = "'._DB_NAME_.'" 
                    AND table_name   = "'._DB_PREFIX_.$tablename.'"
                    AND column_name  = "'.$columname.'"';
                    $column_exists=Db::getInstance()->getValue($sql);
                
                    if($column_exists == $columname) {
                   $sql='ALTER TABLE '._DB_PREFIX_.$tablename.' DROP COLUMN '.$columname;
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
                    }
    
}


private function  unistallExtendedProduct() {
    if((int)$this->do_attributes == 0)
       return;
                   $path= _PS_ADMIN_DIR_.'/themes/default/template/controllers/products/associations.tpl';
                   
                if(file_exists($path)) {
                    $content=file_get_contents($path);
                    $content = str_replace("{include file=\"controllers/products/heureka.tpl\"}","", $content);
                   file_put_contents($path, $content);
                }
                $path= _PS_ADMIN_DIR_.'/themes/default/template/controllers/products/informations.tpl';
                   
                if(file_exists($path)) {
                    $content=file_get_contents($path);
                    $content = str_replace("{include file=\"controllers/products/zbozi_text.tpl\"}","", $content);
                   file_put_contents($path, $content);
                }
                
                if((int)Configuration::get("ZBOZI_PARTIAL_UNISTALL") == 0) {    
                    $this->removeSqlField('product','heureka_category');   
                    $this->removeSqlField('product','zbozi_text'); 
                    $this->removeSqlField('product','heureka_cpc'); 
                     $this->removeSqlField('product','max_cpc'); 
                    $this->removeSqlField('product','max_cpc_search');      
                } 
                  $this->unistallOverrides(array(
                    0=>array('source'=>_PS_MODULE_DIR_.$this->name.'/install/Product.php',
                    'target'=>_PS_OVERRIDE_DIR_.'classes/Product.php',
                    'targetdir'=>_PS_OVERRIDE_DIR_.'classes/'),
                    1=>array('source'=>_PS_MODULE_DIR_.$this->name.'/install/heureka.tpl',
                    'target'=>_PS_ADMIN_DIR_.'/themes/default/template/controllers/products/heureka.tpl',
                    'targetdir'=>_PS_ADMIN_DIR_.'/themes/default/template/controllers/products/'),
                     2=>array('source'=>_PS_MODULE_DIR_.$this->name.'/install/zbozi_text.tpl',
                    'target'=>_PS_ADMIN_DIR_.'/themes/default/template/controllers/products/zbozi_text.tpl',
                    'targetdir'=>_PS_ADMIN_DIR_.'/themes/default/template/controllers/products/')
                    ));
                
                  
                 if(file_exists(_PS_ROOT_DIR_.'/cache/class_index.php'))
                unlink(_PS_ROOT_DIR_.'/cache/class_index.php');           
                Configuration::updateValue('ZBOZI_CATSPERPRODUCT',0);
}

private function isAlreadyIncluded($path, $needle) {
   $s=file_get_contents($path);
   $pos=strpos($s, $needle);
   if($pos === false) {
      return false; // not included
   }   

    return true; 
}

private function updateOptim($feedname) {

				$tagnames=array('productname', 'product');     
				$optim=array(); 
				foreach($tagnames as $tagname) {
				$optr=array();
				$keys=array('name', 'manufacturer','reference','ean', 'custom');
				foreach($keys as $key) {
				$poradiname='optimporadi_'.$feedname.'_'.$tagname.'_'.$key;
				$usename='optimuse_'.$feedname.'_'.$tagname.'_'.$key;
				$poradi=(int)Tools::getValue($poradiname);
				$use=isset($_POST[$usename])?1:0;
				if($key == 'name')
				  $use=1;
				if($key == 'custom') {
				$customname='optimcustom_'.$feedname.'_'.$tagname.'_'.$key;
				$custom=(string)Tools::getValue($customname);
				$optr[$key]=array('poradi'=>$poradi, 'pouzit'=>$use, 'custom'=>$custom);
				}
				else
				$optr[$key]=array('poradi'=>$poradi, 'pouzit'=>$use);
				}


				$optim[$feedname][$tagname]=$optr;   
				}
				
				$stored=json_decode(Configuration::get('ZBOZI_OPTIM'),true);
				$stored[$feedname]=$optim[$feedname];
				Configuration::updateValue('ZBOZI_OPTIM', json_encode($stored));
}
}
