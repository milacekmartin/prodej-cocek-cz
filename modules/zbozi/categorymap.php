<?php
/**
 * Modul Zboží: Srovnávače zboží - export xml pro Prestashop
 *
 * PHP version 5
 *
 * LICENSE: The buyer can free use/edit/modify this software in anyway
 * The buyer is NOT allowed to redistribute this module in anyway or resell it 
 * or redistribute it to third party
 *
 * @package    zbozi
 * @author    Vaclav Mach <info@prestahost.cz>
 * @copyright 2014,2015 Vaclav Mach
 * @license   EULA
 * @version    1.0
 * @link       http://www.prestahost.eu
 */
include(dirname(__FILE__).'/../../config/config.inc.php');
$shop=new Shop(Tools::getValue('id_shop'));
$function=Tools::getValue('function');
Context::getContext()->shop=$shop;
$module=Module::getInstanceByName('zbozi');
require_once(_PS_MODULE_DIR_.$module->name.'/ZboziAttributes.php'); // obsahuje cMap
$cMap=new cMap($function);

if($_SERVER['HTTP_HOST'] == 'localhost:8080')
		    $module_url='/';
		else {
		  $shopUrl =new ShopUrl(Context::getContext()->shop->id);
		  $module_url=  $shopUrl->getURL();
		}
		$module_url.='modules/'.$module->name;
$field=$function.'_category';
$locale=Configuration::get('PS_LOCALE_COUNTRY');
if($locale=='cz')
   $locale='cs-CZ';
elseif($locale=='sk')
   $locale='sk-SK';
else
   $locale='cs-CZ';  

$local_file=_PS_MODULE_DIR_.$module->name.'/'.$function.'_'.$locale.'_taxonomy.txt';
if(isset($_POST['cmd_save']) && strlen($function)) {
       $counter=0;
	   while(list($key,$val)=each($_POST['chck_update'])) {
	   		$name=$_POST["mapped"][$key];
	   	   
	   	    if(isset($_POST['chck_propagate'][$key]))  {
	   	    	propagateChange($key, $field, $name, $counter);
			}
			else {
				 $sql='UPDATE '._DB_PREFIX_.'category SET '.$field.'= "'.pSQL($name).'" WHERE id_category='.(int)$key;
	   	         Db::getInstance()->execute($sql);
	   	         $counter++;
			}
	   	    
	   }
    	echo '<b>' .$counter.' '. $module->l(' records updated').'</b>';
}
if(!file_exists($local_file) || mktime(true)-filemtime($local_file) > 86400*7) {
	 switch($function) {
	 	    case 'heureka': {
	 	    	if($locale == "sk-SK")
	 	    		$remote='http://www.heureka.sk/direct/xml-export/shops/heureka-sekce.xml';
				else 
			       $remote='http://www.heureka.cz/direct/xml-export/shops/heureka-sekce.xml';
				if($s=read_remote_file($remote)) {
	 	    			$data=transform_heureka($s);
	 	    			file_put_contents($local_file, $data);
					}
			}; break;
			case 'google': {
				
				if($s=read_remote_file('http://www.google.com/basepages/producttype/taxonomy.'.$locale.'.txt')) {
					$arr=explode(chr(10),$s);
					unset($arr[0]);
					$s=implode(chr(10),$arr);
	 	    		file_put_contents($local_file, $s);
				}
				
			}
	 }
	
}
$source=false;
if($function == 'heureka') {
	$source= 'http://www.heureka.cz/direct/xml-export/shops/heureka-sekce.xml';
}
elseif($function == 'google') {
	$source= 'http://www.google.com/basepages/producttype/taxonomy.'.$locale.'.xls';
}
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    </head>
<script type="text/javascript">
              var currentId=0;
			$(document).ready(function(){
				$('.txtnapoveda').keyup(function(){   
				 
				    var text= $(this).val();
				    if(text.length < 1)
				      return;
				    
				    currentId = $(this).attr('id');
				    $(this).parent().append($('#selnapoveda'));
					$.ajax({url: '<? echo $module_url;?>/ajax.php?text='+ text + '&function=<? echo $function;?>' + '&locale=<? echo $locale;?>',
					success: function(output) {
					   if(output.length > 0) {
						$('#selnapoveda').html(output);
						$('#selnapoveda').show();
					   }
					   else {
					   $('#selnapoveda').hide();
					   }
					}
					});
				});
				$('.txtnapoveda').mouseleave(function(){   
				 // $('#selnapoveda').hide();
				 // currentId=0;
 
				});
			});
            function setSelected() {
            if(currentId) {
               var selval=$('#selnapoveda').val();
               $('#'+currentId).val(selval);
               
               currentId = currentId.replace("t", "ch");
               $( '#' + currentId ).prop( "checked", true );
			}
               
           $('#selnapoveda').hide();
            currentId=0;	
			}
           function changeUpd(el, id) {
           	  if(el.checked) {
           	  	   $( '#ch' + id ).prop( "checked", true );
			  }
		   }
</script>
<body>

<form method=post>
<input type='hidden' value='<?php echo $function;?>' name='function' />
<input type='hidden' value='<?php echo Tools::getValue('id_shop');?>' name='id_shop' />
<input type='submit' name='cmd_save' value='<?php echo $module->l('Save');?>' />
<select   id='selnapoveda' onclick='setSelected()' style='display:none;max-width:90%;' size=10 ></select>  
<h1><? echo $module->l('Mapa kategorií '); echo $function; ?></h1>
<a href='<?php echo $source;?>' target='_blank'><?php echo $module->l('Zdrojová data');?></a>
<fieldset>
<legend></legend>
<table> 
<tr>
<th><?php echo $module->l('Kategorie na eshopu');?></th>
<th><?php echo $module->l('Kategorie zdroj');?></th>
<th style='max-width:50px'><?php echo $module->l('Změnit');?></th>
<th style='max-width:100px'><?php echo $module->l('Zahrnout podkategorie');?></th>
</tr>
<? 
    $blockCategTree=$cMap->getTree(6);    
            printCategories($blockCategTree);
?>
</table>
</fieldset>     
<input type='submit' name='cmd_save' value='<?php echo $module->l('Save');?>' />

</form>
</body>
<?
   
 function printCategories($cats) {
 	 
     $test=count($cats);
     if($test == 5 && (int)$cats['id']) {
     	printLine($cats);
     	if(isset($cats['children']))
 	       printCategories($cats['children']);
	 }
	 else
	 {
	   foreach($cats as $cat) {
	   if((int)$cat['id']) {
	   printLine($cat);
		   if(isset($cat['children']))
		       printCategories($cat['children']);
		   }
	   }  	 
	 }
 }   
 
 function printLine($cat) {
 	 $style='';
 	 if($cat['level'] == 2) {
 	 	$style='font-weight:800;color: blue'; 
	 }
	 elseif($cat['level'] == 3) {
	 	$style='font-weight:600';  
	 }
 	   
 	 echo '<tr>';
         echo '<td style="'.$style.'">'.str_repeat('&nbsp;', $cat['level']*4);
         echo $cat['id'].'  '.$cat['name'].'</td>';
         $value=$cat['mapped'] && strlen($cat['mapped'])?  (string)$cat['mapped']:'';
      ?>   
        <td>
<input type='text' size='80' autocomplete='off' id='t<? echo $cat['id'];?>' name='mapped[<? echo $cat['id'];?>]' class='txtnapoveda'  value="<? echo  $value; ?>" /> 
</td>
<td>
<input type='checkbox'  id='ch<? echo $cat['id'];?>' name='chck_update[<? echo $cat['id'];?>]' value='1' />
</td> 
<td>
<input type='checkbox'   name='chck_propagate[<? echo $cat['id'];?>]' value='1' onchange="changeUpd(this,<? echo $cat['id'];?>)"/>
</td> 
 <?          
 	  echo '</tr>';
 }
 
	  function getValue($key, $default_value = false)
    {
        if (!isset($key) || empty($key) || !is_string($key))
            return false;
        $ret = (isset($_POST[$key]) ? $_POST[$key] : (isset($_GET[$key]) ? $_GET[$key] : $default_value));

        if (is_string($ret) === true)
            $ret = urldecode(preg_replace('/((\%5C0+)|(\%00+))/i', '', urlencode($ret)));
        return !is_string($ret)? $ret : stripslashes($ret);
    }
    
    function read_remote_file($url) {
    	     $ch=curl_init ();
            curl_setopt($ch, CURLOPT_MAXCONNECTS, 10);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
           // curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            
            $rawdata=curl_exec($ch);
            $info = curl_getinfo($ch);
              if($info['http_code']!=200) {
          return false;
        }
        if(!$rawdata || strlen($rawdata) < 10  ) {
           return false;    
        }
        
        return  $rawdata;
    	
	}
	
	function transform_heureka($s) {
		$xml=simplexml_load_string($s);
		$output='';
		vetev($xml, $output);
		return $output;
	}
	
		function vetev($PARENT, &$output) {
		if(isset($PARENT->CATEGORY)) {
		
		foreach($PARENT->CATEGORY as $CHILD) {
			$CATEGORY_FULLNAME = isset($CHILD->CATEGORY_FULLNAME) ? $CHILD->CATEGORY_FULLNAME : "";
			$s=(string) $CHILD->CATEGORY_FULLNAME;
			if(strlen($s))
			   $output.=$s."\n";
			vetev($CHILD, $output);
		}
		}
     }
     
    function propagateChange($key, $field, $name, &$counter) {
    	     $sql='UPDATE '._DB_PREFIX_.'category SET '.$field.'= "'.pSQL($name).'" WHERE id_category='.(int)$key;
	   	     Db::getInstance()->execute($sql);
	   	     $counter++; 
	   	     $sql='SELECT id_category FROM '._DB_PREFIX_.'category WHERE id_parent='.(int)$key;
	   	     $children= Db::getInstance()->executeS($sql);
	   	     if(is_array($children) && count($children)) {
	   	     	foreach($children as $child) {
	   	     	    propagateChange($child['id_category'],  $field, $name, $counter);
				} 
			 }
	}
?>