<?php
/**
 * Modul Zboží: Srovnávače zboží - export xml pro Prestashop
 *
 * PHP version 5
 *
 * LICENSE: The buyer can free use/edit/modify this software in anyway
 * The buyer is NOT allowed to redistribute this module in anyway or resell it 
 * or redistribute it to third party
 *
 * @package    zbozi
 * @author    Vaclav Mach <info@prestahost.cz>
 * @copyright 2014,2015 Vaclav Mach
 * @license   EULA
 * @version    1.0
 * @link       http://www.prestahost.eu
 */
require_once(dirname(__FILE__).'/../../config/config.inc.php'); 
require_once(dirname(__FILE__).'/../../init.php');
require_once("cFeed.php");
/**
*  prvni potencialni funkce aplikovana na vystup z database
*  vhodna pokud je cestina v databasi ulozena jako html entity 
*/
define ("HTML_ENTITY_DECODE", 1);
define('FILTER_CATEGORIES', 0); // 1 pro netypicke nastaveni multishop
if($_SERVER['HTTP_HOST'] == 'localhost:8080xxx') {
define ("DEBUG", 1);
$step=10;
$total=40; 

}
else   {
define ("DEBUG", 0);
$step=200;
$total=Configuration::get('ZBOZI_PERPASS');
if(!(int)$total)
  $total=1000;
}




register_shutdown_function('processEnd');
$lockfile= _PS_MODULE_DIR_.'zbozi/Skkdj'._COOKIE_IV_;
if(!$lock=acquireLock($lockfile)) {
  echo 'existuje lock soubor '.$lockfile.' mladší než jednu hodinu. Pravděpodobně to znamená že běží další
  instance tohoto skriptu.';
}

/**
* druha potencialni funkce odstrani html tagy jeste pred pripadnym kodovanim entit
* 
*/
define("REMOVE_HTML_TAGS", 1);

/**
* treti funkce pouzita na vystup z database
* 0 .... nic, feed nebude pravdepodobne validni
* 1 .... htmlspecialchars zakoduje specialni html znaky
* 2 .... htmlentities   zakoduje veskere html
*/
define("ENCODE_ENTITIES", 1); 

define("DEF_AVAILABLE_LATER", 10);  // při zapnutém skladu pro zboží které není skladem



 
define("ZIP_FILE", 1); 
 
 if(DEBUG == 1) {
   ob_start();
 
 }  


 $starttime=time();
 // 
 $id_lang=(int)Configuration::get('PS_LANG_DEFAULT');

if(!$id_lang) {
    $sql='SELECT id_lang FROM '._DB_PREFIX_.'_lang  WHERE iso_code="cs"';
    $id_lang = (int)Db::getInstance()->getValue($sql);
}
Context::getContext()->language->id=$id_lang;

if((int)Tools::getValue('id_shop')) {
	Context::getContext()->shop->setContext(Shop::CONTEXT_SHOP, (int)Tools::getValue('id_shop') ); // not necessary?
}
else {
	Context::getContext()->shop->setContext(Shop::CONTEXT_ALL);
}


if(file_exists(dirname(__FILE__).'/ZboziAttributes.php')) {
    $do_attribudes=1;
    require_once(dirname(__FILE__).'/ZboziAttributes.php');
}
else {
    $do_attribudes=0;
    require_once(dirname(__FILE__).'/cMapMini.php');
}
require_once(dirname(__FILE__).'/zbozi.php');   
$c= new Zbozi();
$ff=$c->GetSetting("feeds");
$feeddir= '../../'.$c->GetSetting("feeddir"); 
 if(!is_dir($feeddir)) {
     mkdir($feeddir);
 }  
 
$tmps=array();
$nextround=(int)Configuration::get('ZBOZI_NEXTROUND')*60;
 $state=Configuration::get('ZBOZI_CURRENT_STATE');
foreach($ff as $f) {
$test=Configuration::get('ZBOZI_'.strtoupper($f));
if($test) {
  $feedpath=$feeddir.'/'.cFeed::addShopName().'zbozi_'.$f.'.xml'; 
  $diff=file_exists($feedpath)?($starttime - filemtime($feedpath)):0;
  
   if(file_exists($feedpath) &&  $diff < $nextround && (int)DEBUG != 1) {
    echo 'Detekován dokončený feed '.$feedpath.' který není starší než '.(int)Configuration::get('ZBOZI_NEXTROUND').' minut, <b>exiting</b>';
    exit;
   }
   if($state=='continue' && !file_exists($feeddir.'/'.cFeed::addShopName().'zbozi_'.$f.'.xml.tmp')) {
      $state='start'; 
   }
  $feeds[]=$f;  
}
}

  


if($state=='end' || !$state)
$state='start';



$heurekaTree=array();
if(feedActive('heureka', $feeds)) {
	  $map=new cMap('heureka');
	  $heurekaTree=$map->buildTaxonomyTree($state);
}
$googleTree=array();
if(feedActive('google', $feeds)) {
	  $map=new cMap('google');
	  $googleTree=$map->buildTaxonomyTree($state);
}



$catTree=array();


$cache_path=dirname(__FILE__).'/cache/cats_'.Context::getContext()->shop->id;
if($state == 'start') {
$sql='SELECT MAX(level_depth) FROM '._DB_PREFIX_.'category c LEFT JOIN  '._DB_PREFIX_.'category_shop cs ON
    c.id_category=cs.id_category 
      WHERE cs.id_shop='.(int)Context::getContext()->shop->id;
 $maxLevel= Db::getInstance()->getValue($sql);

 $sql='SELECT  level_depth FROM '._DB_PREFIX_.'category c WHERE 
       c.is_root_category = 1 AND c.id_category='.(int)Context::getContext()->shop->id_category;
 $start_level = Db::getInstance()->getRow($sql);
 if(!(int)$start_level) {
   $root_category=2;
   define('START_CATEGORY_LEVEL', 2); 
 }
 else  {
 	 $root_category=(int)Context::getContext()->shop->id_category;
 	   define('START_CATEGORY_LEVEL', ++$start_level['level_depth']); 
 }


if($maxLevel > 6)
 $maxLevel=6;    
getCategoryTree(START_CATEGORY_LEVEL, $maxLevel, $catTree,'', $root_category);
 file_put_contents($cache_path, json_encode($catTree));
}
else
$catTree =json_decode(file_get_contents($cache_path), true);


$forbidden=array();
$zs=Configuration::get("ZBOZI_FORBIDDEN");
if(strlen($zs)) {
  $a=explode(',',$zs);
  foreach($a as $id_product) {
    if((int) $id_product > 0)
    $forbidden[]=$id_product;
  }  
    
}


foreach($feeds as $feed) {

$classname= "Feed".ucfirst($feed);

    if(file_exists($classname.".php")) {
     require_once($classname.".php");
     $Feed=new  $classname;
     $Feed->initFeed( $feeddir, $state); 
    
    } 
}

   if($state == 'start') {
      $state='continue'; 
      Configuration::updateValue('ZBOZI_START', 0); 
      Configuration::updateValue('ZBOZI_UNIQUE_ID', 0); 
     }

$offset=(int)Configuration::get('ZBOZI_START');
$uniqueId=(int)Configuration::get('ZBOZI_UNIQUE_ID');

$counter=0;
set_time_limit (600); 

for($start=$offset; $start < ($total + $offset); $start+=$step) {

 
    $products =Product::getProducts(      $id_lang,         $start,      $step,  'id_product', 'asc',      false,      true);
     if(count($products) < $step) {
         $state='end';
     }    
    foreach($products as $key=>&$product) {
        if(in_array($product['id_product'], $forbidden)) {
         unset($products[$key] );
         
        }
        elseif(Shop::isFeatureActive() && (int)FILTER_CATEGORIES == 1 && !verifyCategoryShop($Context::getContext()->shop->id,  $product['id_category_default'])){
           unset($products[$key] );
        }
        else {
     
         $product['price'] = Product::getPriceStatic($product['id_product'], true, ((isset($product['id_product_attribute']) AND !empty($product['id_product_attribute'])) ? intval($product['id_product_attribute']) : NULL), 2);
         $product['quantity'] = Product::getQuantity($product['id_product']);
        if($do_attribudes) { 
           $product['attributes'] =ZboziAttributes::getProductAttributes($product['id_product']);// Product::getProductAttributesIds($product['id_product']);
           $product['features'] = ZboziAttributes::getProductFeatures($product['id_product']);
        }  
 
         $product['categorytext_seznam'] ='';
         $product['categorytext_heureka'] ='';
         if(isset($catTree[$product['id_category_default']])) {
         	 $product['categorytext_seznam'] = $catTree[$product['id_category_default']];
             $product['categorytext_heureka'] = $catTree[$product['id_category_default']]; 
		 }
		 if(is_array($heurekaTree) && count($heurekaTree) && ! empty($heurekaTree[$product['id_category_default']]) ) {
		  	$product['categorytext_heureka'] = $heurekaTree[$product['id_category_default']];  
		 }
		 if(is_array($googleTree) && count($googleTree) && ! empty($googleTree[$product['id_category_default']]) ) {
		  	$product['categorytext_google'] = $googleTree[$product['id_category_default']];  
		 }

         if((int)Configuration::get('ZBOZI_CATSPERPRODUCT') == 1) {
          $sql= "SELECT heureka_category, heureka_cpc, max_cpc, max_cpc_search,  zbozi_text FROM "._DB_PREFIX_.'product WHERE id_product='.(int)$product['id_product'];
          $data=Db::getInstance()->getRow($sql);
          $product['zbozi_text'] = (string)$data['zbozi_text'];
          $product['heureka_cpc'] = (string)$data['heureka_cpc'];
          $product['max_cpc'] = (string)$data['max_cpc'];
          $product['max_cpc_search'] = (string)$data['max_cpc_search'];
           $specific_category=$data['heureka_category'];
           if($specific_category && strlen($specific_category))
                    $product['categorytext_heureka']= $specific_category;
         }
        }  
        $counter ++;
    }
 
reset ($feeds);    
foreach($feeds as $feed) {

$classname= "Feed".ucfirst($feed);

    if(file_exists($classname.".php")) {

     require_once($classname.".php");
     $Feed=new  $classname;
     $Feed->createFeed($products, $feeddir);  
     unset($Feed); 
    } 
} 
if(DEBUG == 1) {
    echo $start.': '.time()-$starttime.' sec  MEM:'.memory_get_usage()."\n";
    ob_flush();
    flush();
 } 
 unset($products);
} 

//Configuration::updateValue('ZBOZI_START', $offset+$counter); // zdroj chyb pokud dale nedobehne  
//Configuration::updateValue('ZBOZI_UNIQUE_ID', $uniqueId);   
if($state=='end') {
reset ($feeds);
foreach($feeds as $feed) {
 echo 'finishing';
$classname= "Feed".ucfirst($feed);

    if(file_exists($classname.".php")) {

     require_once($classname.".php");
     $Feed=new  $classname;
     $Feed->finishFeed( $feeddir);   
    } 
}
}
//Configuration::updateValue('ZBOZI_CURRENT_STATE', $state);


 

 


function  getCategoryTree($level, $maxlevel, &$catTree, $name='', $parent=2) {
    global $id_lang;
    if($level > $maxlevel) {
      return;
    }
 
    $sql='SELECT cl.name,   c.`id_category`
         FROM 
        `'._DB_PREFIX_.'category` c LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
        ON c.id_category = cl.id_category
           LEFT JOIN `'._DB_PREFIX_.'category_shop` cs
        ON c.id_category =  cs.id_category
        WHERE cl.id_lang='.(int)$id_lang.' AND c.level_depth='.(int)$level.
        ' AND cs.id_shop='.(int)Context::getContext()->shop->id.' 
        AND c.id_parent='.(int)$parent.' GROUP BY  c.`id_category`
        ';
  
    
    $ct= Db::getInstance()->ExecuteS($sql);
    foreach($ct as $cat) {
        if($name == '')
           $catname=$cat['name'];
        else
        $catname=$name.' | '.$cat['name'];
      
        $catTree[$cat['id_category']]= $catname;
        getCategoryTree($level+1,$maxlevel, $catTree, $catname, $cat['id_category']); 
    }
     
}


 
  function friendly_url($nadpis) {
    $prevodni_tabulka = Array(
  'ä'=>'a',
  'Ä'=>'A',
  'á'=>'a',
  'Á'=>'A',
  'à'=>'a',
  'À'=>'A',
  'ã'=>'a',
  'Ã'=>'A',
  'â'=>'a',
  'Â'=>'A',
  'č'=>'c',
  'Č'=>'C',
  'ć'=>'c',
  'Ć'=>'C',
  'ď'=>'d',
  'Ď'=>'D',
  'ě'=>'e',
  'Ě'=>'E',
  'é'=>'e',
  'É'=>'E',
  'ë'=>'e',
  'Ë'=>'E',
  'è'=>'e',
  'È'=>'E',
  'ê'=>'e',
  'Ê'=>'E',
  'í'=>'i',
  'Í'=>'I',
  'ï'=>'i',
  'Ï'=>'I',
  'ì'=>'i',
  'Ì'=>'I',
  'î'=>'i',
  'Î'=>'I',
  'ľ'=>'l',
  'Ľ'=>'L',
  'ĺ'=>'l',
  'Ĺ'=>'L',
  'ń'=>'n',
  'Ń'=>'N',
  'ň'=>'n',
  'Ň'=>'N',
  'ñ'=>'n',
  'Ñ'=>'N',
  'ó'=>'o',
  'Ó'=>'O',
  'ö'=>'o',
  'Ö'=>'O',
  'ô'=>'o',
  'Ô'=>'O',
  'ò'=>'o',
  'Ò'=>'O',
  'õ'=>'o',
  'Õ'=>'O',
  'ő'=>'o',
  'Ő'=>'O',
  'ř'=>'r',
  'Ř'=>'R',
  'ŕ'=>'r',
  'Ŕ'=>'R',
  'š'=>'s',
  'Š'=>'S',
  'ś'=>'s',
  'Ś'=>'S',
  'ť'=>'t',
  'Ť'=>'T',
  'ú'=>'u',
  'Ú'=>'U',
  'ů'=>'u',
  'Ů'=>'U',
  'ü'=>'u',
  'Ü'=>'U',
  'ù'=>'u',
  'Ù'=>'U',
  'ũ'=>'u',
  'Ũ'=>'U',
  'û'=>'u',
  'Û'=>'U',
  'ý'=>'y',
  'Ý'=>'Y',
  'ž'=>'z',
  'Ž'=>'Z',
  'ź'=>'z',
  'Ź'=>'Z'
  
);
    
    $nadpis =strtolower(( strtr($nadpis, $prevodni_tabulka)));   
    $url = $nadpis;
    $url=str_replace(' ', '_', $url);
    $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
    $url = trim($url, "-");
    $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
    return $url;
}

function verifyCategoryShop($id_shop, $id_category) {
     $sql='SELECT id_category FROM '._DB_PREFIX_.'category_shop WHERE id_shop='.(int)$id_shop .' AND id_category='.(int)$id_category;
     return  (bool)Db::getInstance()->getValue($sql);
       
}

function acquireLock($lockfile) {
    if(file_exists($lockfile)) {
        if(mktime() - filemtime($lockfile) < 3600)
          return false;
          
          Configuration::updateValue('ZBOZI_START', 0); 
          unlink($lockfile);    
    }
    $fp=fopen($lockfile, 'w+');
    fputs($fp, mktime(true));
    return true;
}

function processEnd() {
 global $offset;
 global $counter;
 global $uniqueId;
 global $state;
 global $lockfile;

Configuration::updateValue('ZBOZI_START', $offset+$counter); // zdroj chyb pokud dale nedobehne  
Configuration::updateValue('ZBOZI_UNIQUE_ID', $uniqueId); 
Configuration::updateValue('ZBOZI_CURRENT_STATE', $state);  
if(file_exists($lockfile))
  unlink($lockfile);  
}

function feedActive($feedname, $feeds) {
	 while(list($key,$val)=each($feeds)) {
	 	if($feedname == $val)
	 	    return true; 
	 }
	 return false;
}
