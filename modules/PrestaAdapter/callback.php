<?php
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#


include(dirname(__FILE__) . '/../../config/config.inc.php');
include(dirname(__FILE__) . '/../../init.php');
require_once dirname(__FILE__) . '/PrestaAdapter.php';

try {
    BeginUniErr();

    session_start();
    $uniModulName = $_GET['_uniModul'];
    if (!$uniModulName) {
        $uniModulName = $_GET['_unimodul'];
    } // v jedne verzi bylo na PrestaAdapter nestandardne malymi pismeny _unimodul, asi jen jedna instalace. Takze pro zachovani zpetne kompatibility
    $adapter = new PrestaAdapter(null, $uniModulName);
    parse_str($_SERVER['QUERY_STRING'], $cbArgs);
    $html = $adapter->uniModul->processCallbackRequest($_GET['_callbackName'], $cbArgs);
    if ($html != '') {
        $adapter->displayAdditionalOrderForm(null, '', $html);
    }
    EndUniErr();
} catch (Exception $ex) {
    ExceptionUniErr($ex);
}
