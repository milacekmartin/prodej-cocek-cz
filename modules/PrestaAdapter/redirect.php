<?php
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#


include(dirname(__FILE__) . '/../../config/config.inc.php');

//$tmpuri = $_SERVER['REQUEST_URI']; // hack kvuli body@id, ktere se odvozuje od url u modulu vcetne query stringu. body@id je potrebne pro stylovani a query string vadi
//$_SERVER['REQUEST_URI'] = preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']);
//$tmpmeth = $_SERVER['REQUEST_METHOD'];
//$_SERVER['REQUEST_METHOD'] = 'POST'; // dalsi hack kvuli hacku, kdy pro ssl a non-POST to chce redirectovat, ale URI je uz zmenene
include(dirname(__FILE__) . '/../../init.php');
//$_SERVER['REQUEST_METHOD'] = $tmpmeth;
//$_SERVER['REQUEST_URI'] = $tmpuri;

require dirname(__FILE__) . '/PrestaAdapter.php';


BeginUniErr();
session_start();
$adapter = new PrestaAdapter(null, $_GET['uniModul']);
$orderToPayInfo = $adapter->getOrderToPayInfo($_GET['subMethod']);
$prePayGWInfo = $adapter->uniModul->queryPrePayGWInfo($orderToPayInfo);

$showConfirmationPage = Configuration::get($adapter->getShortConfigName('showConfirmationPage', $adapter->uniModul->name));

if (!(isset($_REQUEST['uniPayCont']) || $showConfirmationPage == 2 || ($showConfirmationPage == 1 && $prePayGWInfo->forexMessage == "" && !$prePayGWInfo->selectCsPayBrand))) {
    $adapter->displayAdditionalOrderForm($prePayGWInfo, $_GET['subMethod']);
} else {
    $orderToPayInfo = $adapter->getOrderToPayInfo($_GET['subMethod'], true);
    if (isset($_REQUEST['ccBrand'])) {
        $orderToPayInfo->ccBrand = $_REQUEST['ccBrand'];
    }

    $redirectAction = $adapter->uniModul->gatewayOrderRedirectAction($orderToPayInfo);

    if ($redirectAction->orderReplyStatus != null) {   // okamzita odpoved muze byt i zaroven s redirektem - napr. Cetelem
        $frontend_redir = $redirectAction->redirectUrl == null && $redirectAction->redirectForm == null;
        $adapter->procesReplyStatus($redirectAction->orderReplyStatus, $frontend_redir);
    }
    if ($redirectAction->inlineForm != null) {
        $adapter->displayAdditionalOrderForm($prePayGWInfo, $_GET['subMethod'], $redirectAction->inlineForm);
    } else {
        if ($redirectAction->redirectForm != null) {
            $adapter->uniModul->formRedirect($redirectAction->redirectForm);
        } else {
            if ($redirectAction->redirectUrl != null) {
                header('Location: ' . $redirectAction->redirectUrl);
                ResetUniErr();
                exit();
            }
        }
    }
}
EndUniErr();





