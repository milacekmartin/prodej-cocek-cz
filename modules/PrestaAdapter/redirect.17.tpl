<!--
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#
-->


{extends file='page.tpl'}

{block name='page_content_container'}
    <section id="content-hook_order_confirmation" class="card">
        <div class="card-block">
            <div class="row">
                <div class="col-md-12">


                    {* end - vlastni text *}


                    {if !empty($total)}
                        <h3>{$methodName}</h3>
                        {$minilogo nofilter}
                        <p>
                            <img src="{$uniModulLogo}" alt="{$methodName}" class="UniModulMethodLogo" style="float:left; margin: 0px 10px 5px 0px;"/>
                            {l s='You have chosen the' mod='PrestaAdapter'} {$methodName} {l s='method.' mod='PrestaAdapter'}
                            <br/><br/>
                            {l s='The total amount of your order is' mod='PrestaAdapter'} {$totalf}
                        </p>
                        {if isset($forexMessage)}
                            &nbsp;
                            <p style="text-align:left">{$forexMessage nofilter}</p>
                        {/if}

                        {if !isset($inlineForm)}
                            <form method="post">
                                {if $selectCsPayBrand === true}
                                    <br/>
                                    <p style="text-align:left">
                                        <b>{$ccBrand_title}</b><br/>
                                        <input type="radio" name="ccBrand" value="VISA" checked> VISA</input><br/>
                                        <input type="radio" name="ccBrand" value="MasterCard"> MasterCard</input><br/>
                                        <input type="radio" name="ccBrand" value="VisaElectron"> VisaElectron</input>
                                        <br/> <input type="radio" name="ccBrand" value="Maestro"> Maestro</input><br/>
                                    </p>
                                {/if}
                                <p>
                                    <br/>
                                    {l s='You will be redirected to the payment gateway after you confirm the order.' mod='PrestaAdapter'}
                                </p>
                                <p>
                                    <br/>
                                    <b>{l s='Please confirm your order by clicking \'I confirm my order\'' mod='PrestaAdapter'}.</b>
                                </p>
                                <p class="cart_navigation">
                                    <a href="{$link->getPageLink('order.php', true)}?step=3" class="btn">{l s='Other payment methods' mod='PrestaAdapter'}</a>
                                    <input type="submit" name="uniPayCont" value="{l s='I confirm my order' mod='PrestaAdapter'}" class="btn btn-primary"/>
                                    {$minilogo nofilter}
                                </p>
                            </form>
                        {/if}
                    {/if}

                    {if isset($inlineForm)}
                        &nbsp;
                        <p style="text-align:left">{$inlineForm nofilter}</p>
                    {/if}


                    {* end - vlastni text *}

                </div>
            </div>
        </div>
    </section>
{/block}

