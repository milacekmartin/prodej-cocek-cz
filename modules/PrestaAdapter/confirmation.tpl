<!--
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#
--><p>{l s='Your order on' mod='PrestaAdapter'}
    <span class="bold">{$shop_name}</span> {l s='is complete.' mod='PrestaAdapter'}
    <br/><br/>
    {l s='You have chosen the' mod='PrestaAdapter'} {$methodName} {l s='method.' mod='PrestaAdapter'}
    <br/><br/><span class="bold">{l s='Your order will be sent very soon.' mod='PrestaAdapter'}</span>
    {if _PS_VERSION_ ge '1.7'}
        {*<br /><br />{l s='If you have questions, comments or concerns, please contact our [1]expert customer support team[/1].' mod='ps_wirepayment' tags=["<a href='{$contact_url}'>"]} <a href="{$base_dir_ssl}contact-form.php">{l s='customer support' mod='PrestaAdapter'}</a>.*}
        <br/>
        <br/>
        {l s='For any questions or for further information, please contact our' mod='PrestaAdapter'} {l s='customer support' mod='PrestaAdapter'}</a>.
    {else}
        <br/>
        <br/>
        {l s='For any questions or for further information, please contact our' mod='PrestaAdapter'}
        <a href="{$base_dir_ssl}contact-form.php">{l s='customer support' mod='PrestaAdapter'}</a>
        .
    {/if}

</p>
