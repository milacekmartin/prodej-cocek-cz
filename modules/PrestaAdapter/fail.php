<?php
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#


include(dirname(__FILE__) . '/../../config/config.inc.php');

include(dirname(__FILE__) . '/../../init.php');

require_once dirname(__FILE__) . '/PrestaAdapter.php';

try {
    BeginUniErr();

    session_start();
    $adapter = new PrestaAdapter(null, $_GET['uniModul']);

    $smarty->assign('binderName', $adapter->name);
    $smarty->assign('errorMessage', $_REQUEST['errorMessage']);

    $submetLogoPiece = "";
    $smarty->assign('uniModulLogo', $adapter->getHttpHost() . __PS_BASE_URI__ . "UniModul/Uni" . urlencode($adapter->uniModul->name . $submetLogoPiece) . "Logo.png");

    $methodName = $adapter->uniModul->getModuleSubMethodName($adapter->getLanguage(), '');
    $smarty->assign('methodName', $methodName);

    $smarty->assign('hide_left_column', true);
    $smarty->assign('hide_right_column', true);
    $smarty->assign('minilogo', $adapter->uniModul->getMiniLogoSpan());

    $adapter->displayTemplate(dirname(__FILE__) . '/fail.tpl', $smarty);

    EndUniErr();
} catch (Exception $ex) {
    ExceptionUniErr($ex);
}
