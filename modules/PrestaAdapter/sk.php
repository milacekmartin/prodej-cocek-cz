<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{prestaadapter}default-bootstrap>confirmation_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Vaša objednávka pre';
$_MODULE['<{prestaadapter}default-bootstrap>confirmation_75fbf512d744977d62599cc3f0ae2bb4'] = 'je skompletizovaná.';
$_MODULE['<{prestaadapter}default-bootstrap>confirmation_91675397c13245179e4bc959514fc2ca'] = 'Vybrali ste si';
$_MODULE['<{prestaadapter}default-bootstrap>confirmation_2c987f462c2ab7fc63e061e871e6a97d'] = 'metódu.';
$_MODULE['<{prestaadapter}default-bootstrap>confirmation_e6dc7945b557a1cd949bea92dd58963e'] = 'Vaša objednávka bude čoskoro odoslaná.';
$_MODULE['<{prestaadapter}default-bootstrap>confirmation_0db71da7150c27142eef9d22b843b4a9'] = 'V prípade otázok nás prosím kontaktujte';
$_MODULE['<{prestaadapter}default-bootstrap>confirmation_64430ad2835be8ad60c59e7d44e4b0b1'] = 'zákaznícka podpora';
