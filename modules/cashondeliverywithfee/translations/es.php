<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_271486149ca62b2f6ae5d9ae97cb769a'] = 'Pago contra reembolso';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_d0e9561b285e8f0f87786d02ebdaec7a'] = 'Aceptamos pago contra reembolso';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_5663f7ec8dc30d52e2348c9b4f6fce80'] = 'Este modulo permite aceptar pagos contra reembolso añadiendo un pequeño recargo';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_f4f70727dc34561dfde1a3c529b6205c'] = 'Opciones';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_eb7297e94e2cb86e90a6ead067666717'] = 'Cargo';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_a1fa27779242b4902f7ae3bdd5c6d508'] = 'Tipo';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_4457d440870ad6d42bab9082d9bf9b61'] = 'Fijo';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_b2f40690858b404ed10e62bdf422c704'] = 'Cantidad';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_37be07209f53a5d636d5c904ca9ae64c'] = 'Porcentaje';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_3b004a0ef57c5118565bdc42433ecabc'] = 'Cargo mínimo';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_209e1cf195878f91470df4aa41405219'] = 'Cargo máximo';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_b17f3f4dcf653a5776792498a9b44d6a'] = 'Actualizar opciones';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_a2e8a6e2cc7b5fab15bf76f8fa3a64d1'] = 'Instrucciones:';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_948a2e3548aaf7f9941a3192fa607d51'] = 'Créditos';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_f4d1ea475eaa85102e2b4e6d95da84bd'] = 'Confirmar';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_c888438d14855d7d96a2724ee9c306bd'] = 'Opciones actualizadas';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_6357d3551190ec7e79371a8570121d3a'] = 'Hay';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_4ce81305b7edb043d0a7a5c75cab17d0'] = 'Hay';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_07213a0161f52846ab198be103b5ab43'] = 'errores';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_cb5e100e5a9a3e7f6d1fd97512215282'] = 'error';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_1fbde5bd96bc1e729aa85bab48692e2d'] = 'El mensaje de pago no es válido, por favor revise su modulo.';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_34fa8046b912bfa38acd4fea4460050c'] = 'Elija su opción en los botones de selección (cargo fijo, cargo variable o ambos).';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_5b2e17b7de8875fbe0f763a3d063d868'] = 'Establezca el valor en el campo cantidad y/o en el campo porcentaje, dependiendo de su elección.';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_22a05870c4d0e02ef3767aeff9d2b6d6'] = 'Si usted quiere establecer un valor mínimo para el cargo por porcentaje o el cargo por fijo más porcentaje, establézcalo en el campo cargo mínimo.';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_c23daddc7c4f309359aa6f58f86dfb7f'] = 'Si usted quiere establecer un valor máximo para el cargo por porcentaje o el cargo por fijo más porcentaje, establézcalo en el campo cargo máximo.';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_6a3bc93eca0db956d8144ab90b97f093'] = 'Si el cargo mínimo es mayor que el cargo máximo el cargo máximo no actúa.';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_bbda38a130c729dcd1ad267ff127d0f8'] = 'Para desactivar los cargos mínimo y máximo establézcalos al valor 0 (cero).';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_0bcbbf9650e2f6b1107156d7cd307d89'] = 'Módulo modificado con tasa fija y/o porcentual con valores mínimo y máximo (v0.6) por Santos Aranda Mateos para ';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_42d99dd82dd222892cc86e1e4eda6b45'] = 'Módulo original con tasa fija o porcentual con valor mínimo (v0.4) por ';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_c2808546f3e14d267d798f4e0e6f102e'] = 'Personalizado';
$_MODULE['<{cashondeliverywithfee}prestashop>cashondeliverywithfee_9137796c15dd92e5553c3f29574d0968'] = 'Código del vale:';

$_MODULE['<{cashondeliverywithfee}prestashop>payment_1f9497d3e8bac9b50151416f04119cec'] = 'Pago contra reembolso';
$_MODULE['<{cashondeliverywithfee}prestashop>payment_5aff2f0dc03142b6d18d9269906e285e'] = 'Pago contra reembolso: + ';