<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{cashondelivery}default>cashondelivery_22d2c7f90dc862df71ac7aff95183fb2'] = 'Rembours';
$_MODULE['<{cashondelivery}default>cashondelivery_7a3ef27eb0b1895ebf263ad7dd949fb6'] = 'Accepteren rembours betalingen';
$_MODULE['<{cashondelivery}default>cashondelivery_536dc7424180872c8c2488ae0286fb53'] = 'U betaalt voor de goederen bij levering';
$_MODULE['<{cashondelivery}default>validation_0c25b529b4d690c39b0831840d0ed01c'] = 'Bestelling overzicht';
$_MODULE['<{cashondelivery}default>validation_fecdb10bd3a542f48600f853eaf14150'] = 'Rembours betaling';
$_MODULE['<{cashondelivery}default>validation_15888961d683d9b7da21fb4655728edf'] = 'U heeft gekozen voor rembours betaling';
$_MODULE['<{cashondelivery}default>validation_e2867a925cba382f1436d1834bb52a1c'] = 'Het totale bedrag van uw bestelling is';
$_MODULE['<{cashondelivery}default>validation_1a55daa6d9397f1af9eff130cbfb6bcd'] = 'AUB niet vergeten uw bestelling te bevestigen door op de Bevestiging knop te drukken';
$_MODULE['<{cashondelivery}default>validation_d883181d6ae2e27678706b77cb77f042'] = 'Ik bevestig mijn bestelling';

?>