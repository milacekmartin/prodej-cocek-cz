<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{cashondelivery}prestashop>cashondelivery_1f9497d3e8bac9b50151416f04119cec'] = 'Dobírka';
$_MODULE['<{cashondelivery}prestashop>cashondelivery_7a3ef27eb0b1895ebf263ad7dd949fb6'] = 'Přijímat platby na dobírku';
$_MODULE['<{cashondelivery}prestashop>payment_22d2c7f90dc862df71ac7aff95183fb2'] = 'Platba dobírkou';
$_MODULE['<{cashondelivery}prestashop>payment_08152755b6d60e422c959ccefd41b090'] = 'Při platbě dobírkou platíte za zboží až při předání zboží';
$_MODULE['<{cashondelivery}prestashop>validation_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Doručení';
$_MODULE['<{cashondelivery}prestashop>validation_0c25b529b4d690c39b0831840d0ed01c'] = 'Shrnutí objednávky';
$_MODULE['<{cashondelivery}prestashop>validation_d538c5b86e9a71455ba27412f4e9ab51'] = 'Dobírka';
$_MODULE['<{cashondelivery}prestashop>validation_fecdb10bd3a542f48600f853eaf14150'] = 'Platba na dobírku';
$_MODULE['<{cashondelivery}prestashop>validation_8861c5d3fa54b330d1f60ba50fcc4aab'] = 'Jako způsob platby jste si vybrali dobírku.';
$_MODULE['<{cashondelivery}prestashop>validation_e2867a925cba382f1436d1834bb52a1c'] = 'Celková hodnota objednávky činí';
$_MODULE['<{cashondelivery}prestashop>validation_0881a11f7af33bc1b43e437391129d66'] = 'Prosím potvrďte objednávku kliknutím na \"Potvrzuji objednávku\"';
$_MODULE['<{cashondelivery}prestashop>validation_569fd05bdafa1712c4f6be5b153b8418'] = 'Další způsoby platby';
$_MODULE['<{cashondelivery}prestashop>validation_46b9e3665f187c739c55983f757ccda0'] = 'Potvrzuji objednávku';