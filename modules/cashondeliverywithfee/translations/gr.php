<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{cashondelivery}prestashop>cashondelivery_1f9497d3e8bac9b50151416f04119cec'] = 'Μετρητοίς';
$_MODULE['<{cashondelivery}prestashop>cashondelivery_7a3ef27eb0b1895ebf263ad7dd949fb6'] = 'Δεχόμαστε πληρωμές σε μετρητά';
$_MODULE['<{cashondelivery}prestashop>payment_22d2c7f90dc862df71ac7aff95183fb2'] = 'Μετρητά με την παράδοση';
$_MODULE['<{cashondelivery}prestashop>payment_08152755b6d60e422c959ccefd41b090'] = 'Μετρητά με την παράδοση: πληρώνετε για τα προϊόντα κατά την παράδοση';
$_MODULE['<{cashondelivery}prestashop>validation_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Μεταφορές';
$_MODULE['<{cashondelivery}prestashop>validation_0c25b529b4d690c39b0831840d0ed01c'] = 'Περίληψη παραγγελίας';
$_MODULE['<{cashondelivery}prestashop>validation_d538c5b86e9a71455ba27412f4e9ab51'] = 'Πληρωμή σε μετρητά κατά την παράδοση';
$_MODULE['<{cashondelivery}prestashop>validation_fecdb10bd3a542f48600f853eaf14150'] = 'Πληρωμή σε μετρητά';
$_MODULE['<{cashondelivery}prestashop>validation_8861c5d3fa54b330d1f60ba50fcc4aab'] = 'Έχετε επιλέξει να πληρώσετε σε Μετρητά κατά την παράδοση.';
$_MODULE['<{cashondelivery}prestashop>validation_e2867a925cba382f1436d1834bb52a1c'] = 'Το σύνολο αξίας για την παραγγελία σας είναι';
$_MODULE['<{cashondelivery}prestashop>validation_0881a11f7af33bc1b43e437391129d66'] = 'Παρακαλώ επιβεβαιώστε την παραγγελία σας κάνοντας κλίκ στο \"Επιβεβαιώνω την παραγγελία\"';
$_MODULE['<{cashondelivery}prestashop>validation_569fd05bdafa1712c4f6be5b153b8418'] = 'Άλλοι τρόποι πληρωμής';
$_MODULE['<{cashondelivery}prestashop>validation_46b9e3665f187c739c55983f757ccda0'] = 'Επιβεβαιώνω την παραγγελία';

?>