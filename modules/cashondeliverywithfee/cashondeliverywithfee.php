<?php

/*
*  LEGAL NOTICE
* Prestaworks� - http://www.prestaworks.com
Copyright (c) 2008
by Prestaworks
* Permission is hereby granted, to the buyer of this software to use it freely in association with prestashop. 
* The buyer are free to use/edit/modify this software in anyway he/she see fit.
* The buyer are NOT allowed to redistribute this module in anyway or resell it or redistribute it to third party.
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class CashOnDeliveryWithFee extends PaymentModule {
    private $_html = '';
    private $_postErrors = array();

    function __construct() {
        $this->name = 'cashondeliverywithfee';
        $this->tab = 'payments_gateways';
        $this->version = 0.7;

        $this->controllers = array('validation');
        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        parent::__construct();

        /* The parent construct is required for translations */
        //$this->page = basename(__FILE__, '.php');
        $this->displayName = $this->l('Cash on delivery with fee (COD)');
        $this->description = $this->l('Accept cash on delivery payments with extra fee');
    }

    function install() {
        if (!parent::install() OR !Configuration::updateValue('COD_TAX', '0') OR !Configuration::updateValue('COD_FEE', '0') OR !Configuration::updateValue('COD_FEE_TYPE', '0') OR !Configuration::updateValue('COD_FEE_MIN', '0') OR !Configuration::updateValue('COD_FEE_MAX', '0') OR !Configuration::updateValue('COD_NO_FEE_PRICE', '0')
            OR !$this->registerHook('payment') OR !$this->registerHook('displayPaymentEU') OR !$this->registerHook('paymentReturn')) {
            return false;
        }

        return true;
    }

    public function uninstall() {
        if (
            !Configuration::deleteByName('COD_TAX') OR !Configuration::deleteByName('COD_FEE') OR (!Configuration::deleteByName('COD_FEE_TYPE')) OR !parent::uninstall() OR (!Configuration::deleteByName('COD_FEE_MIN')) OR (!Configuration::deleteByName('COD_FEE_MAX')) OR !Configuration::deleteByName('COD_NO_FEE_PRICE', '0')) {
            return false;
        }

        return true;
    }

    public function hasProductDownload($cart) {
        foreach ($cart->getProducts() AS $product) {
            $pd = ProductDownload::getIdFromIdProduct((int)($product['id_product']));
            if ($pd AND Validate::isUnsignedInt($pd)) {
                return true;
            }
        }

        return false;
    }

    public function getContent() {
        $this->_html = '<h2>CASH ON DELIVERY</h2>';
        if (isset($_POST['submitCOD'])) {
            if (empty($_POST['tax'])) {
                $_POST['tax'] = '0';
            }
            if (empty($_POST['fee'])) {
                $_POST['fee'] = '0';
            }
            if (empty($_POST['feetype'])) {
                $_POST['feetype'] = '0';
            }
            if (empty($_POST['feemin'])) {
                $_POST['feemin'] = '0';
            }
            if (empty($_POST['feemax'])) {
                $_POST['feemax'] = '0';
            }
            if (empty($_POST['nofeeprice'])) {
                $_POST['nofeeprice'] = '0';
            }
            if (!sizeof($this->_postErrors)) {
                Configuration::updateValue('COD_TAX', floatval($_POST['tax']));
                Configuration::updateValue('COD_FEE', floatval($_POST['fee']));
                Configuration::updateValue('COD_FEE_TYPE', floatval($_POST['feetype']));
                Configuration::updateValue('COD_FEE_MIN', floatval($_POST['feemin']));
                Configuration::updateValue('COD_FEE_MAX', floatval($_POST['feemax']));
                Configuration::updateValue('COD_NO_FEE_PRICE', floatval($_POST['nofeeprice']));
                $this->displayConf();
            } else {
                $this->displayErrors();
            }
        }

        $this->displayCOD();
        $this->displayFormSettings();

        return $this->_html;
    }

    public function displayCOD() {
        $this->_html .= '
		<img src="../modules/cashondelivery/logo.gif" style="float:left; margin-right:15px;" />
		<b>' . $this->l('This module allows you to accept payments by Cash on delivery and charge a extra fee.') . '</b><br /><br /><br />';
    }


    public function displayFormSettings() {
        $conf = Configuration::getMultiple(array('COD_TAX', 'COD_FEE', 'COD_FEE_TYPE', 'COD_FEE_MIN', 'COD_FEE_MAX', 'COD_NO_FEE_PRICE'));
        $tax = array_key_exists('tax', $_POST) ? $_POST['tax'] : (array_key_exists('COD_TAX', $conf) ? $conf['COD_TAX'] : '');
        $fee = array_key_exists('fee', $_POST) ? $_POST['fee'] : (array_key_exists('COD_FEE', $conf) ? $conf['COD_FEE'] : '');
        $feetype = array_key_exists('feetype', $_POST) ? $_POST['feetype'] : (array_key_exists('COD_FEE_TYPE', $conf) ? $conf['COD_FEE_TYPE'] : '');
        $feemin = array_key_exists('feemin', $_POST) ? $_POST['feemin'] : (array_key_exists('COD_FEE_MIN', $conf) ? $conf['COD_FEE_MIN'] : '');
        $feemax = array_key_exists('feemax', $_POST) ? $_POST['feemax'] : (array_key_exists('COD_FEE_MAX', $conf) ? $conf['COD_FEE_MAX'] : '');
        $nofeeprice = array_key_exists('nofeeprice', $_POST) ? $_POST['nofeeprice'] : (array_key_exists('COD_NO_FEE_PRICE', $conf) ? $conf['COD_NO_FEE_PRICE'] : '');

        $this->_html .= '
		<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
		<fieldset>
			<legend><img src="../img/admin/contact.gif" />' . $this->l('Settings') . '</legend>
			<label>' . $this->l('Percentage') . '</label>
			<div class="margin-form"><input type="text" size="33" name="tax" value="' . htmlentities($tax, ENT_COMPAT, 'UTF-8') . '" /></div>
			<label>' . $this->l('Amount') . '</label>
			<div class="margin-form"><input type="text" size="33" name="fee" value="' . htmlentities($fee, ENT_COMPAT, 'UTF-8') . '" /></div>
			<br />
			<label>' . $this->l('Type') . '</label>
			<div class="margin-form">
			<input type="radio" name="feetype" value="0" ' . ($feetype == '0' ? 'checked="checked"' : '') . ' /> ' . $this->l('Fixed') . '
			<input type="radio" name="feetype" value="1" ' . ($feetype == '1' ? 'checked="checked"' : '') . ' /> ' . $this->l('Percentage') . '
			<input type="radio" name="feetype" value="2" ' . ($feetype == '2' ? 'checked="checked"' : '') . ' /> ' . $this->l('Fixed') . '+' . $this->l('Percentage') . '
			</div>
			<label>' . $this->l('Minimum Fee') . '</label>
			<div class="margin-form"><input type="text" size="33" name="feemin" value="' . htmlentities($feemin, ENT_COMPAT, 'UTF-8') . '" /></div>
			<label>' . $this->l('Maximum Fee') . '</label>
			<div class="margin-form"><input type="text" size="33" name="feemax" value="' . htmlentities($feemax, ENT_COMPAT, 'UTF-8') . '" /></div>
			<label>' . $this->l('No Fee from') . '</label>
			<div class="margin-form"><input type="text" size="33" name="nofeeprice" value="' . htmlentities($nofeeprice, ENT_COMPAT, 'UTF-8') . '" /></div>
			<br />
			<br /><center><input type="submit" name="submitCOD" value="' . $this->l('Update settings') . '" class="button" /></center>
		</fieldset>
		</form><br /><br />';

        $this->_html .= '<fieldset class="space">
			<legend><img src="../img/admin/unknown.gif" alt="" class="middle" />Info</legend>
			 <h4>' . $this->l('User instructions') . '</h4>
			 <ol>
				<li>' . $this->l('Set your choice in radio buttons (fixed tax, percentage tax or both).') . '</li>
			 	<li>' . $this->l('Set the value in the amount box and/or in the percentage box, depends of your choice.') . '</li>
			 	<li>' . $this->l('If you wan\'t a minimum value for percentage or fixed plus percentage fees, enter the value in minimum fee box.') . '</li>
			 	<li>' . $this->l('If you wan\'t a maximum value for percentage or fixed plus percentage fees, enter the value in maximum fee box.') . '</li>
			 	<li>' . $this->l('If minimum fee is higher than maximum fee maximum fee doesn\'t works.') . '</li>
			 	<li>' . $this->l('To deactivate the minimum and maximum fees set it to 0 (zero) value.') . '</li>
			</ol>
			<h4>' . $this->l('Credits') . '</h4>
			' . $this->l('Modified fixed and/or percentaje taxes with minimal and maximal values (v0.6) module by Santos Aranda Mateos for ') . '<a href="http://www.ps3pic.es" title="PS3Pic" alt="www.ps3pic.es">PS3Pic</a>.
			<br />
			' . $this->l('Original fixed or percentaje taxes with minimum value (v0.4) module by ') . '<a href="http://www.prestaworks.com" title="Prestaworks" alt="www.prestaworks.com">Prestaworks</a>.
		</fieldset>';
    }


    public function displayConf() {
        $this->_html .= '
		<div class="conf confirm">
			<img src="../img/admin/ok.gif" alt="' . $this->l('Confirmation') . '" />
			' . $this->l('Settings updated') . '
		</div>';
    }

    public function displayErrors() {
        $nbErrors = sizeof($this->_postErrors);
        $this->_html .= '
		<div class="alert error">
			<h3>' . ($nbErrors > 1 ? $this->l('There are') : $this->l('There is')) . ' ' . $nbErrors . ' ' . ($nbErrors > 1 ? $this->l('errors') : $this->l('error')) . '</h3>
			<ol>';
        foreach ($this->_postErrors AS $error)
            $this->_html .= '<li>' . $error . '</li>';
        $this->_html .= '
			</ol>
		</div>';
    }

    function hookPayment($params) {
        if (!$this->active) {
            return;
        }

        global $smarty;

        // Check if cart has product download
        if ($this->hasProductDownload($params['cart'])) {
            return false;
        }

        //$currency = $this->getCurrency();
        /* Photo is copyrighted by Leticia Wilson - Fotolia.com, licenced to PrestaShop company */
        $smarty->assign(
            array(
                'this_path' => $this->_path, //keep for retro compat
                'this_path_cod' => $this->_path,
                'fee' => number_format($this->getCost($params), 2, '.', ''),
                'this_path_ssl' => Tools::getShopDomainSsl(true, true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/'
            )
        );

        return $this->display(__FILE__, 'payment.tpl');
    }

    //Return the fee cost
    function getCost($params) {
        //pokud cena objednavky prekrocila hodnotu pro neplaceni dobirkovneho
        $nofeeproce = floatval(Configuration::get('COD_NO_FEE_PRICE'));
        if ($nofeeproce > 0) {
            if ($params['cart']->getOrderTotal(true, 3) >= $nofeeproce) {
                return floatval(0);
            }
        }

        if (Configuration::get('COD_FEE_TYPE') == 0) {
            return floatval(Configuration::get('COD_FEE'));
        } else {

            if (Configuration::get('COD_FEE_TYPE') == 1) {

                $minimalfee = floatval(Configuration::get('COD_FEE_MIN'));
                $maximalfee = floatval(Configuration::get('COD_FEE_MAX'));
                $cartvalue = floatval($params['cart']->getOrderTotal(true, 3));
                $percent = floatval(Configuration::get('COD_TAX'));
                $percent = $percent / 100;
                $fee = $cartvalue * $percent;

                if (($fee < $minimalfee) & ($minimalfee != 0)) {
                    $fee = $minimalfee;
                } else {
                    if (($fee > $maximalfee) & ($maximalfee != 0)) {
                        $fee = $maximalfee;
                    }
                }

                return floatval($fee);
            } else {
                $minimalfee = floatval(Configuration::get('COD_FEE_MIN'));
                $maximalfee = floatval(Configuration::get('COD_FEE_MAX'));
                $cartvalue = floatval($params['cart']->getOrderTotal(true, 3));
                $percent = floatval(Configuration::get('COD_TAX'));
                $percent = $percent / 100;
                $tax = floatval(Configuration::get('COD_FEE'));
                $fee = ($cartvalue * $percent) + $tax;

                if (($fee < $minimalfee) & ($minimalfee != 0)) {
                    $fee = $minimalfee;
                } else {
                    if (($fee > $maximalfee) & ($maximalfee != 0)) {
                        $fee = $maximalfee;
                    }
                }

                return floatval($fee);
            }
        }
    }

    //Return the fee cost
    function getCostValidated($cart) {
        //pokud cena objednavky prekrocila hodnotu pro neplaceni dobirkovneho
        $nofeeproce = floatval(Configuration::get('COD_NO_FEE_PRICE'));
        if ($nofeeproce > 0) {
            if ($cart->getOrderTotal(true, 3) >= $nofeeproce) {
                return floatval(0);
            }
        }

        if (Configuration::get('COD_FEE_TYPE') == 0) {
            return floatval(Configuration::get('COD_FEE'));
        } else {
            if (Configuration::get('COD_FEE_TYPE') == 1) {
                $minimalfee = floatval(Configuration::get('COD_FEE_MIN'));
                $maximalfee = floatval(Configuration::get('COD_FEE_MAX'));
                $cartvalue = floatval($cart->getOrderTotal(true, 3));
                $percent = floatval(Configuration::get('COD_TAX'));
                $percent = $percent / 100;
                $fee = $cartvalue * $percent;

                if (($fee < $minimalfee) & ($minimalfee != 0)) {
                    $fee = $minimalfee;
                } else {
                    if (($fee > $maximalfee) & ($maximalfee != 0)) {
                        $fee = $maximalfee;
                    }
                }

                return floatval($fee);
            } else {
                $minimalfee = floatval(Configuration::get('COD_FEE_MIN'));
                $maximalfee = floatval(Configuration::get('COD_FEE_MAX'));
                $cartvalue = floatval($cart->getOrderTotal(true, 3));
                $percent = floatval(Configuration::get('COD_TAX'));
                $percent = $percent / 100;
                $tax = floatval(Configuration::get('COD_FEE'));
                $fee = ($cartvalue * $percent) + $tax;

                if (($fee < $minimalfee) & ($minimalfee != 0)) {
                    $fee = $minimalfee;
                } else {
                    if (($fee > $maximalfee) & ($maximalfee != 0)) {
                        $fee = $maximalfee;
                    }
                }

                return floatval($fee);
            }
        }
    }

    function hookPaymentReturn($params) {
        if (!$this->active) {
            return;
        }

        return $this->display(__FILE__, 'confirmation.tpl');
    }

    /**
     * Validate an order in database AND ADD EXTRA COST
     * Function called from a payment module
     * @param integer $id_cart Value
     * @param integer $id_order_state Value
     * @param float $amountPaid Amount really paid by customer (in the default currency)
     * @param string $paymentMethod Payment method (eg. 'Credit cart')
     * @param string $message Message to attach to order
     * @return bool
     */

    function validateOrderCOD($id_cart, $id_order_state, $amountPaid, $paymentMethod = 'Unknown', $message = null, $extraVars = array(), $currency_special = null) {
        $cart = new Cart(intval($id_cart));

        $CODfee = $this->getCostValidated($cart);

        /* Does order already exists ? */
        if (Validate::isLoadedObject($cart) AND $cart->OrderExists() == 0) {
            /* Copying data from cart */
            $order = new Order();
            $order->id_carrier = intval($cart->id_carrier);
            $order->id_customer = intval($cart->id_customer);
            $order->id_address_invoice = intval($cart->id_address_invoice);
            $order->id_address_delivery = intval($cart->id_address_delivery);
            $vat_address = new Address(intval($order->id_address_delivery));
            $id_zone = Address::getZoneById(intval($vat_address->id));
            $order->id_currency = ($currency_special ? intval($currency_special) : intval($cart->id_currency));
            $order->id_lang = intval($cart->id_lang);
            $order->id_cart = intval($cart->id);
            $customer = new Customer(intval($order->id_customer));
            $order->secure_key = pSQL($customer->secure_key);
            $order->payment = substr($paymentMethod, 0, 32);
            if (isset($this->name)) {
                $order->module = $this->name;
            }
            $order->total_paid_real = floatval(number_format($amountPaid + $CODfee, 2, '.', ''));
            $order->recyclable = $cart->recyclable;
            $order->gift = intval($cart->gift);
            $order->gift_message = $cart->gift_message . "BLA:" . $CODfee;
            $currency = new Currency($order->id_currency);
            $amountPaid = floatval(Tools::convertPrice(floatval(number_format($amountPaid + $CODfee, 2, '.', '')), $currency));
            $order->total_paid_real = $amountPaid;
            $order->total_products = floatval(Tools::convertPrice(floatval(number_format($cart->getOrderTotal(false, 1), 2, '.', '')), $currency));
            $order->total_products_wt = floatval(Tools::convertPrice(floatval(number_format($cart->getOrderTotal(true, 1), 2, '.', '')), $currency));
            $order->total_discounts = floatval(Tools::convertPrice(floatval(number_format(abs($cart->getOrderTotal(true, 2)), 2, '.', '')), $currency));
            $order->total_shipping = floatval(Tools::convertPrice(floatval(number_format($cart->getOrderShippingCost() + $CODfee, 2, '.', '')), $currency));
            $order->total_wrapping = floatval(Tools::convertPrice(floatval(number_format(abs($cart->getOrderTotal(true, 6)), 2, '.', '')), $currency));

            $order->total_paid = floatval(Tools::convertPrice(floatval(number_format($cart->getOrderTotal(true, 3) + $CODfee, 2, '.', '')), $currency));
            /* Amount paid by customer is not the right one -> Status = payment error */
            if ($order->total_paid != $order->total_paid_real) {
                $id_order_state = _PS_OS_ERROR_;
            }

            /* Creating order */
            $result = $order->add();

            /* Next ! */
            if ($result AND isset($order->id)) {
                /* Set order state in order history */
                $history = new OrderHistory();
                $history->id_order = intval($order->id);
                $history->changeIdOrderState(intval($id_order_state), intval($order->id));
                $history->addWithemail(true, $extraVars);

                /* Optional message to attach to this order */
                if (isset($message) AND !empty($message)) {
                    $msg = new Message();
                    $message = strip_tags($message, '<br>');
                    if (!Validate::isMessage($message)) {
                        $message = $this->l('Payment message is not valid, please check your module!');
                    }
                    $msg->message = $message;
                    $msg->id_order = intval($order->id);
                    $msg->private = 1;
                    $msg->add();
                }

                /* Insert products from cart into order_detail table */
                $products = $cart->getProducts();
                $productsList = '';
                $db = Db::getInstance();
                $query = 'INSERT INTO `' . _DB_PREFIX_ . 'order_detail`
					(`id_order`, `product_id`, `product_attribute_id`, `product_name`, `product_quantity`, `product_price`, `product_quantity_discount`, `product_ean13`, `product_reference`, `product_supplier_reference`, `product_weight`, `tax_name`, `tax_rate`, `ecotax`, `download_deadline`, `download_hash`)
				VALUES ';

                $customizedDatas = Product::getAllCustomizedDatas(intval($order->id_cart));
                Product::addCustomizationPrice($products, $customizedDatas);
                foreach ($products AS $key => $product) {
                    if ($id_order_state != _PS_OS_CANCELED_ AND $id_order_state != _PS_OS_ERROR_) {
                        if (($id_order_state != _PS_OS_OUTOFSTOCK_) AND (($updateResult = Product::updateQuantity($product)) === false OR $updateResult === -1)) {
                            $id_order_state = _PS_OS_OUTOFSTOCK_;
                            $history = new OrderHistory();
                            $history->id_order = intval($order->id);
                            $history->changeIdOrderState(_PS_OS_OUTOFSTOCK_, intval($order->id));
                            $history->addWithemail();
                        }
                        Hook::updateQuantity($product, $order);
                    }
                    $price = Tools::convertPrice(Product::getPriceStatic(intval($product['id_product']), false, ($product['id_product_attribute'] ? intval($product['id_product_attribute']) : null), 4), $currency);
                    $price_wt = Tools::convertPrice(Product::getPriceStatic(intval($product['id_product']), true, ($product['id_product_attribute'] ? intval($product['id_product_attribute']) : null), 4), $currency);

                    /* Add some informations for virtual products */
                    $deadline = '0000-00-00 00:00:00';
                    $download_hash = null;
                    $productDownload = new ProductDownload();
                    if ($id_product_download = $productDownload->getIdFromIdProduct(intval($product['id_product']))) {
                        $productDownload = new ProductDownload(intval($id_product_download));
                        $deadline = $productDownload->getDeadLine();
                        $download_hash = $productDownload->getHash();
                    }

                    /* Exclude VAT */
                    if (Tax::excludeTaxeOption()) {
                        $product['tax'] = 0;
                        $product['rate'] = 0;
                    } else {
                        $tax = Tax::getApplicableTax(intval($product['id_tax']), floatval($product['rate']));
                    }

                    /* Quantity discount */
                    $reduc = 0.0;
                    if ($product['cart_quantity'] > 1 AND ($qtyD = QuantityDiscount::getDiscountFromQuantity($product['id_product'], $product['cart_quantity']))) {
                        $reduc = QuantityDiscount::getValue($price_wt, $qtyD->id_discount_type, $qtyD->value);
                        $price -= $reduc / (1 + floatval($tax) / 100);
                    }

                    /* Query */
                    $query .= '(' . intval($order->id) . ',
						' . intval($product['id_product']) . ',
						' . (isset($product['id_product_attribute']) ? intval($product['id_product_attribute']) : 'NULL') . ',
						\'' . pSQL($product['name'] . ((isset($product['attributes']) AND $product['attributes'] != null) ? ' - ' . $product['attributes'] : '')) . '\',
						' . intval($product['cart_quantity']) . ',
						' . floatval($price) . ',
						' . floatval($reduc) . ',
						' . (empty($product['ean13']) ? 'NULL' : '\'' . pSQL($product['ean13']) . '\'') . ',
						' . (empty($product['reference']) ? 'NULL' : '\'' . pSQL($product['reference']) . '\'') . ',
						' . (empty($product['supplier_reference']) ? 'NULL' : '\'' . pSQL($product['supplier_reference']) . '\'') . ',
						' . floatval($product['weight']) . ',
						\'' . (!$tax ? '' : pSQL($product['tax'])) . '\',
						' . floatval($tax) . ',
						' . floatval($product['ecotax']) . ',
						\'' . pSQL($deadline) . '\',
						\'' . pSQL($download_hash) . '\'),';

                    $priceWithTax = number_format($price * (($tax + 100) / 100), 2, '.', '');
                    $customizationQuantity = 0;
                    if (isset($customizedDatas[$product['id_product']][$product['id_product_attribute']])) {
                        $customizationQuantity = intval($product['customizationQuantityTotal']);
                        $productsList .=
                            '<tr style="background-color:' . ($key % 2 ? '#DDE2E6' : '#EBECEE') . ';">
							<td style="padding:0.6em 0.4em;">' . $product['reference'] . '</td>
							<td style="padding:0.6em 0.4em;"><strong>' . $product['name'] . (isset($product['attributes_small']) ? ' ' . $product['attributes_small'] : '') . ' - ' . $this->l('Customized') . '</strong></td>
							<td style="padding:0.6em 0.4em; text-align:right;">' . Tools::displayPrice($price * ($tax + 100) / 100, $currency, false, false) . '</td>
							<td style="padding:0.6em 0.4em; text-align:center;">' . $customizationQuantity . '</td>
							<td style="padding:0.6em 0.4em; text-align:right;">' . Tools::displayPrice($customizationQuantity * $priceWithTax, $currency, false, false) . '</td>
						</tr>';
                    }

                    if (!$customizationQuantity OR intval($product['cart_quantity']) > $customizationQuantity) {
                        $productsList .=
                            '<tr style="background-color:' . ($key % 2 ? '#DDE2E6' : '#EBECEE') . ';">
							<td style="padding:0.6em 0.4em;">' . $product['reference'] . '</td>
							<td style="padding:0.6em 0.4em;"><strong>' . $product['name'] . (isset($product['attributes_small']) ? ' ' . $product['attributes_small'] : '') . '</strong></td>
							<td style="padding:0.6em 0.4em; text-align:right;">' . Tools::displayPrice($price * ($tax + 100) / 100, $currency, false, false) . '</td>
							<td style="padding:0.6em 0.4em; text-align:center;">' . (intval($product['cart_quantity']) - $customizationQuantity) . '</td>
							<td style="padding:0.6em 0.4em; text-align:right;">' . Tools::displayPrice((intval($product['cart_quantity']) - $customizationQuantity) * $priceWithTax, $currency, false, false) . '</td>
						</tr>';
                    }
                } /* end foreach ($products) */
                $query = rtrim($query, ',');
                $result = $db->Execute($query);

                /* Insert discounts from cart into order_discount table */
                $discounts = $cart->getDiscounts();
                $discountsList = '';
                foreach ($discounts AS $discount) {
                    $objDiscount = new Discount(intval($discount['id_discount']));
                    $value = $objDiscount->getValue(sizeof($discounts), $cart->getOrderTotal(true, 1), $order->total_shipping, $cart->id);
                    $order->addDiscount($objDiscount->id, $objDiscount->name, $value);
                    if ($id_order_state != _PS_OS_ERROR_ AND $id_order_state != _PS_OS_CANCELED_) {
                        $objDiscount->quantity = $objDiscount->quantity - 1;
                    }
                    $objDiscount->update();

                    $discountsList .=
                        '<tr style="background-color:#EBECEE;">
							<td colspan="4" style="padding:0.6em 0.4em; text-align:right;">' . $this->l('Voucher code:') . ' ' . $objDiscount->name . '</td>
							<td style="padding:0.6em 0.4em; text-align:right;">-' . Tools::displayPrice($value, $currency, false, false) . '</td>
					</tr>';
                }

                /* Specify order id for message */
                $oldMessage = Message::getMessageByCartId(intval($cart->id));
                if ($oldMessage) {
                    $message = new Message(intval($oldMessage['id_message']));
                    $message->id_order = intval($order->id);
                    $message->update();
                }

                /* Hook new order */
                $orderStatus = new OrderState(intval($id_order_state));
                if (Validate::isLoadedObject($orderStatus)) {
                    Hook::newOrder($cart, $order, $customer, $currency, $orderStatus);
                    foreach ($cart->getProducts() as $product)
                        if ($orderStatus->logable) {
                            ProductSale::addProductSale($product['id_product'], $product['cart_quantity']);
                        }
                }
                /* Send an e-mail to customer */
                if ($id_order_state != _PS_OS_ERROR_ AND $id_order_state != _PS_OS_CANCELED_ AND $customer->id) {
                    $invoice = new Address(intval($order->id_address_invoice));
                    $delivery = new Address(intval($order->id_address_delivery));
                    $carrier = new Carrier(intval($order->id_carrier));
                    $delivery_state = $delivery->id_state ? new State(intval($delivery->id_state)) : false;
                    $invoice_state = $invoice->id_state ? new State(intval($invoice->id_state)) : false;

                    $data = array(
                        '{firstname}' => $customer->firstname,
                        '{lastname}' => $customer->lastname,
                        '{email}' => $customer->email,
                        '{delivery_firstname}' => $delivery->firstname,
                        '{delivery_lastname}' => $delivery->lastname,
                        '{delivery_address1}' => $delivery->address1,
                        '{delivery_address2}' => $delivery->address2,
                        '{delivery_city}' => $delivery->city,
                        '{delivery_postal_code}' => $delivery->postcode,
                        '{delivery_country}' => $delivery->country,
                        '{delivery_state}' => $delivery->id_state ? $delivery_state->name : '',
                        '{delivery_phone}' => $delivery->phone,
                        '{invoice_firstname}' => $invoice->firstname,
                        '{invoice_lastname}' => $invoice->lastname,
                        '{invoice_address2}' => $invoice->address2,
                        '{invoice_address1}' => $invoice->address1,
                        '{invoice_city}' => $invoice->city,
                        '{invoice_postal_code}' => $invoice->postcode,
                        '{invoice_country}' => $invoice->country,
                        '{invoice_state}' => $invoice->id_state ? $invoice_state->name : '',
                        '{invoice_phone}' => $invoice->phone,
                        '{order_name}' => sprintf("#%06d", intval($order->id)),
                        '{date}' => Tools::displayDate(date('Y-m-d h:i:s'), intval($order->id_lang), 1),
                        '{carrier}' => (strval($carrier->name) != '0' ? $carrier->name : Configuration::get('PS_SHOP_NAME')),
                        '{payment}' => $order->payment,
                        '{products}' => $productsList,
                        '{discounts}' => $discountsList,
                        '{total_paid}' => Tools::displayPrice($order->total_paid, $currency, false, false),
                        '{total_products}' => Tools::displayPrice($order->total_paid - $order->total_shipping - $order->total_wrapping + $order->total_discounts, $currency, false, false),
                        '{total_discounts}' => Tools::displayPrice($order->total_discounts, $currency, false, false),
                        '{total_shipping}' => Tools::displayPrice($order->total_shipping, $currency, false, false),
                        '{total_wrapping}' => Tools::displayPrice($order->total_wrapping, $currency, false, false)
                    );

                    if (is_array($extraVars)) {
                        $data = array_merge($data, $extraVars);
                    }

                    /* Join PDF invoice */
                    if (intval(Configuration::get('PS_INVOICE')) AND Validate::isLoadedObject($orderStatus) AND $orderStatus->invoice) {
                        $fileAttachment['content'] = PDF::invoice($order, 'S');
                        $fileAttachment['name'] = sprintf('FA%06d.pdf', intval($order->id));
                        $fileAttachment['mime'] = 'application/pdf';
                    } else {
                        $fileAttachment = null;
                    }

                    if ($orderStatus->send_email AND Validate::isEmail($customer->email)) {
                        Mail::Send(intval($order->id_lang), 'order_conf', 'Order confirmation', $data, $customer->email, $customer->firstname . ' ' . $customer->lastname, null, null, $fileAttachment);
                    }

                    $this->currentOrder = intval($order->id);

                    return true;
                }

                return true;
            } else {
                die(Tools::displayError('Order creation failed'));
            }
        } else {
            die(Tools::displayError('An order has already been placed using this cart'));
        }
    }

    public function hookDisplayPaymentEU($params) {
        if (!$this->active) {
            return;
        }

        // Check if cart has product download
        if ($this->hasProductDownload($params['cart'])) {
            return false;
        }

        return array(
            'cta_text' => $this->l('Pay with cash on delivery (COD)'),
            'logo' => Media::getMediaPath(dirname(__FILE__) . '/cashondelivery.jpg'),
            'action' => $this->context->link->getModuleLink($this->name, 'validation', array('confirm' => true), true)
        );
    }

}