<?php
// Autor (c) Miroslav Novak, www.platiti.cz
// Pouzivani bez souhlasu autora neni povoleno
// #Ver:PRV075-64-gd93ef80:2017-05-04#


require_once _PS_ROOT_DIR_ . "/modules/PrestaAdapter/PrestaAdapter.php";

class GPWebPayBinder extends PaymentModule {
    public $adapter;

    public function __construct() {
        $this->adapter = new PrestaAdapter($this, 'GPWebPay', '');
        parent::__construct();
    }

    public function install() {
        return $this->adapter->install();
    }

    public function getContent() {
        return $this->adapter->getContent();
    }


    public function hookPayment($params) {
        return $this->adapter->hookPayment($params);
    }

    public function hookPaymentOptions($params) {
        return $this->adapter->hookPaymentOptions($params);
    }

    public function hookPaymentReturn($params) {
        return $this->adapter->hookPaymentReturn($params);
    }

    public function hookAdminOrder($params) {
        return $this->adapter->hookAdminOrder($params);
    }

    public function hookActionPDFInvoiceRender($params) {
        return $this->adapter->hookActionPDFInvoiceRender($params);
    }


    public function hookRightColumn($params) {
        return $this->adapter->hookRightColumn($params);
    }

    public function hookFooter($params) {
        return $this->adapter->hookFooter($params);
    }

    public function hookLeftColumn($params) {
        return $this->adapter->hookLeftColumn($params);
    }

    public function hookExtraRight($params) {
        return $this->adapter->hookExtraRight($params);
    }


    public function validateOrder($id_cart, $id_order_state, $amountPaid, $paymentMethod = 'Unknown', $message = null, $extraVars = array(), $currency_special = null, $dont_touch_amount = false, $secure_key = false, Shop $shop = null) {
        $this->adapter->validateOrder($id_cart, $id_order_state, $amountPaid, $paymentMethod, $message, $extraVars, $currency_special, $dont_touch_amount, $secure_key, $shop);
    }


}
