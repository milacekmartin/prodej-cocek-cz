{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{*extends file="helpers/print/print.tpl"*}

{block name="override_tpl"}
    <style type="text/css">
        .print-hidden {
            display: none;
        }

        .alert {
            display: none;
        }
    </style>
    <script type="text/javascript">
        window.print();
    </script>
    {*assign var="hook_invoice" value={hook h="displayInvoice" id_order=$order->id}}
        {if ($hook_invoice)}
            <div>{$hook_invoice}</div>
        {/if*}
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-credit-card"></i>
            {l s='Order'}
            <span class="badge">{$order->reference}</span> <span class="badge">{l s="#"}{$order->id}</span>
            <div class="panel-heading-action">
                <div class="btn-group">
                    <a class="btn btn-default{if !$previousOrder} disabled{/if}" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$previousOrder|intval}">
                        <i class="icon-backward"></i> </a>
                    <a class="btn btn-default{if !$nextOrder} disabled{/if}" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$nextOrder|intval}">
                        <i class="icon-forward"></i> </a>
                </div>
            </div>
        </div>

        <!-- Tab content -->
        <div class="tab-content panel">
            <!-- Tab status -->
            <h4>Historie objednávky</h4>
            <!-- History of status -->
            <div class="table-responsive">
                <table class="table history-status row-margin-bottom">
                    <tbody>
                    {foreach from=$history item=row key=key}
                        {if ($key == 0)}
                            <tr>
                                <td style="background-color:{$row['color']}">
                                    <img src="../img/os/{$row['id_order_state']|intval}.gif" width="16" height="16" alt="{$row['ostate_name']|stripslashes}"/>
                                </td>
                                <td style="background-color:{$row['color']};color:{$row['text-color']}">{$row['ostate_name']|stripslashes}</td>
                                <td style="background-color:{$row['color']};color:{$row['text-color']}">{if $row['employee_lastname']}{$row['employee_firstname']|stripslashes} {$row['employee_lastname']|stripslashes}{/if}</td>
                                <td style="background-color:{$row['color']};color:{$row['text-color']}">{dateFormat date=$row['date_add'] full=true}</td>

                            </tr>
                        {else}
                            <tr>
                                <td>
                                    <img src="../img/os/{$row['id_order_state']|intval}.gif" width="16" height="16"/>
                                </td>
                                <td>{$row['ostate_name']|stripslashes}</td>
                                <td>{if $row['employee_lastname']}{$row['employee_firstname']|stripslashes} {$row['employee_lastname']|stripslashes}{else}&nbsp;{/if}</td>
                                <td>{dateFormat date=$row['date_add'] full=true}</td>

                            </tr>
                        {/if}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
        <hr/>
        <div class="tab-content panel">
            <!-- Tab shipping -->
            <h4>Doprava</h4>
            <!-- Shipping block -->
            {if !$order->isVirtual()}
                <div class="form-horizontal">
                    {if $order->gift_message}
                        <div class="form-group">
                            <label class="control-label col-lg-3">{l s='Message'}</label>
                            <div class="col-lg-9">
                                <p class="form-control-static">{$order->gift_message|nl2br}</p>
                            </div>
                        </div>
                    {/if}
                    {include file='controllers/orders/_shipping.tpl'}
                    {if $carrierModuleCall}
                        {$carrierModuleCall}
                    {/if}

                </div>
            {/if}
        </div>

        <div class="tab-content panel">
            <h4>Platba</h4>

            {if count($order->getOrderPayments()) > 0}
                <p class="alert alert-danger"{if round($orders_total_paid_tax_incl, 2) == round($total_paid, 2) || (isset($currentState) && $currentState->id == 6)} style="display: none;"{/if}>
                    {l s='Warning'}
                    <strong>{displayPrice price=$total_paid currency=$currency->id}</strong>
                    {l s='paid instead of'}
                    <strong class="total_paid">{displayPrice price=$orders_total_paid_tax_incl currency=$currency->id}</strong>
                    {foreach $order->getBrother() as $brother_order}
                        {if $brother_order@first}
                            {if count($order->getBrother()) == 1}
                                <br/>
                                {l s='This warning also concerns order '}
                            {else}
                                <br/>
                                {l s='This warning also concerns the next orders:'}
                            {/if}
                        {/if}
                        <a href="{$current_index}&amp;vieworder&amp;id_order={$brother_order->id}&amp;token={$smarty.get.token|escape:'html':'UTF-8'}"> #{'%06d'|sprintf:$brother_order->id}
                        </a>
                    {/foreach}
                </p>
            {/if}
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th><span class="title_box ">{l s='Date'}</span></th>
                        <th><span class="title_box ">{l s='Payment method'}</span></th>
                        <th><span class="title_box ">{l s='Transaction ID'}</span></th>
                        <th><span class="title_box ">{l s='Amount'}</span></th>
                        <th><span class="title_box ">{l s='Invoice'}</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$order->getOrderPaymentCollection() item=payment}
                        <tr>
                            <td>{dateFormat date=$payment->date_add full=true}</td>
                            <td>{$payment->payment_method|escape:'html':'UTF-8'}</td>
                            <td>{$payment->transaction_id|escape:'html':'UTF-8'}</td>
                            <td>{displayPrice price=$payment->amount currency=$payment->id_currency}</td>
                            <td>
                                {if $invoice = $payment->getOrderInvoice($order->id)}
                                    {$invoice->getInvoiceNumberFormatted($current_id_lang, $order->id_shop)}
                                {else}
                                {/if}
                            </td>
                        </tr>
                        <tr class="payment_information" style="display: none;">
                            <td colspan="5">
                                <p>
                                    <b>{l s='Card Number'}</b>&nbsp;
                                    {if $payment->card_number}
                                        {$payment->card_number}
                                    {else}
                                        <i>{l s='Not defined'}</i>
                                    {/if}
                                </p>
                                <p>
                                    <b>{l s='Card Brand'}</b>&nbsp;
                                    {if $payment->card_brand}
                                        {$payment->card_brand}
                                    {else}
                                        <i>{l s='Not defined'}</i>
                                    {/if}
                                </p>
                                <p>
                                    <b>{l s='Card Expiration'}</b>&nbsp;
                                    {if $payment->card_expiration}
                                        {$payment->card_expiration}
                                    {else}
                                        <i>{l s='Not defined'}</i>
                                    {/if}
                                </p>
                                <p>
                                    <b>{l s='Card Holder'}</b>&nbsp;
                                    {if $payment->card_holder}
                                        {$payment->card_holder}
                                    {else}
                                        <i>{l s='Not defined'}</i>
                                    {/if}
                                </p>
                            </td>
                        </tr>
                        {foreachelse}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Customer informations -->
    <div class="panel">
        <!-- Tab nav -->
        <div class="row">
            <!-- Tab content -->
            <div class="tab-content panel">
                <!-- Tab status -->
                <!-- Addresses -->
                <h4>{l s='Shipping address'}</h4>
                {if !$order->isVirtual()}
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-6">
                                {* tento prazdny foreach tu musi byt kvuli nacteni jednotlivych polozek adresy *}
                                {foreach from=$customer_addresses item=address}{/foreach}

                                {$address['lastname']} {$address['firstname']}<br>
                                {$address['address1']}<br>
                                {$address['postcode']} {$address['city']}<br>
                                {$address['country']}<br>
                                {if !empty($address['phone'])}{$address['phone']}<br>{/if}
                                {if !empty($address['phone_mobile'])}{$address['phone_mobile']}<br>{/if}
                                {if !empty($addresses.delivery->other)}
                                    <hr/>
                                    {$addresses.delivery->other}
                                    <br/>
                                {/if}
                            </div>
                        </div>
                    </div>
                {/if}
            </div>
            <div class="tab-content panel">
                <!-- Invoice address -->
                <h4>{l s='Invoice address'}</h4>

                <div class="well">
                    <div class="row">
                        <div class="col-sm-6">
                            {* tento prazdny foreach tu musi byt kvuli nacteni jednotlivych polozek adresy *}
                            {foreach from=$customer_addresses item=address}{/foreach}

                            {$address['lastname']} {$address['firstname']}<br>
                            {$address['address1']}<br>
                            {$address['postcode']} {$address['city']}<br>
                            {$address['country']}<br>
                            {if !empty($address['phone'])}{$address['phone']}<br>{/if}
                            {if !empty($address['phone_mobile'])}{$address['phone_mobile']}<br>{/if}
                            {if !empty($addresses.invoice->other)}
                                <hr/>
                                {$addresses.invoice->other}
                                <br/>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-heading"><i class="icon-envelope"></i> {l s='Messages'}</div>
        {if (sizeof($messages))}
            <div class="panel panel-highlighted">
                <div class="message-item">
                    {foreach from=$messages item=message}
                        <div class="message-avatar">
                            <div class="avatar-md">
                                <i class="icon-user icon-2x"></i>
                            </div>
                        </div>
                        <div class="message-body">

									<span class="message-date">&nbsp;<i class="icon-calendar"></i>
                                        {dateFormat date=$message['date_add']} -
									</span>
                            <h4 class="message-item-heading">
                                {if ($message['elastname']|escape:'html':'UTF-8')}{$message['efirstname']|escape:'html':'UTF-8'}
                                    {$message['elastname']|escape:'html':'UTF-8'}{else}{$message['cfirstname']|escape:'html':'UTF-8'} {$message['clastname']|escape:'html':'UTF-8'}
                                {/if}
                                {if ($message['private'] == 1)}
                                    <span class="badge badge-info">{l s='Private'}</span>
                                {/if}
                            </h4>
                            <p class="message-item-text">
                                {$message['message']|escape:'html':'UTF-8'|nl2br}
                            </p>
                        </div>
                        {*if ($message['is_new_for_me'])}
                            <a class="new_message" title="{l s='Mark this message as \'viewed\''}" href="{$smarty.server.REQUEST_URI}&amp;token={$smarty.get.token}&amp;messageReaded={$message['id_message']}">
                                <i class="icon-ok"></i>
                            </a>
                        {/if*}
                    {/foreach}
                </div>
            </div>
        {/if}
    </div>
    <input type="hidden" name="id_order" value="{$order->id}"/>
    <div style="display: none">
        <input type="hidden" value="{$order->getWarehouseList()|implode}" id="warehouse_list"/>
    </div>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-shopping-cart"></i>
            {l s='Products'}
        </div>

        {capture "TaxMethod"}
            {if ($order->getTaxCalculationMethod() == $smarty.const.PS_TAX_EXC)}
                {l s='tax excluded.'}
            {else}
                {l s='tax included.'}
            {/if}
        {/capture}

        <div class="table-responsive">
            <table class="table" id="orderProducts">
                <thead>
                <tr>
                    <th></th>
                    <th><span class="title_box ">{l s='Product'}</span></th>
                    {if ($order->getTaxCalculationMethod() != $smarty.const.PS_TAX_EXC)}
                        <th>
                            <span class="title_box ">{l s='Unit Price'}</span>
                            <small class="text-muted">{l s='tax excluded.'}</small>
                        </th>
                    {/if}
                    <th>
                        <span class="title_box ">{l s='Unit Price'}</span>
                        <small class="text-muted">{$smarty.capture.TaxMethod}</small>
                    </th>
                    <th class="text-center"><span class="title_box ">{l s='Qty'}</span></th>
                    {if $display_warehouse}
                        <th><span class="title_box ">{l s='Warehouse'}</span></th>{/if}
                    {if ($order->hasBeenPaid())}
                        <th class="text-center"><span class="title_box ">{l s='Refunded'}</span></th>{/if}
                    {if ($order->hasBeenDelivered() || $order->hasProductReturned())}
                        <th class="text-center"><span class="title_box ">{l s='Returned'}</span></th>
                    {/if}
                    {if $stock_management}
                        <th class="text-center"><span class="title_box ">{l s='Available quantity'}</span>
                        </th>{/if}
                    <th>
                        <span class="title_box ">{l s='Total'}</span>
                        <small class="text-muted">{$smarty.capture.TaxMethod}</small>
                    </th>
                    <th style="display: none;" class="add_product_fields"></th>
                    <th style="display: none;" class="edit_product_fields"></th>
                    <th style="display: none;" class="standard_refund_fields"></th>
                    <th style="display:none" class="partial_refund_fields"></th>
                </tr>
                </thead>
                <tbody>
                {foreach from=$products item=product key=k}
                    {* Include customized datas partial *}
                    {include file='controllers/orders/_customized_data.tpl'}
                    {* Include product line partial *}
                    {include file='controllers/orders/_product_line.tpl'}
                {/foreach}
                {if $can_edit}
                    {include file='controllers/orders/_new_product.tpl'}
                {/if}
                </tbody>
            </table>
        </div>

        <div class="clear">&nbsp;</div>
        <div class="row">
            <div class="col-xs-6">
                <div class="alert alert-warning">
                    {l s='For this customer group, prices are displayed as: [1]%s[/1]' sprintf=[$smarty.capture.TaxMethod] tags=['<strong>']}
                    {if !Configuration::get('PS_ORDER_RETURN')}
                        <br/>
                        <strong>{l s='Merchandise returns are disabled'}</strong>
                    {/if}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="panel panel-vouchers" style="{if !sizeof($discounts)}display:none;{/if}">
                    {if (sizeof($discounts) || $can_edit)}
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>
													<span class="title_box ">
														{l s='Discount name'}
													</span>
                                    </th>
                                    <th>
													<span class="title_box ">
														{l s='Value'}
													</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach from=$discounts item=discount}
                                    <tr>
                                        <td>{$discount['name']}</td>
                                        <td>
                                            {if $discount['value'] != 0.00}
                                                -
                                            {/if}
                                            {displayPrice price=$discount['value'] currency=$currency->id}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                        <div class="current-edit" id="voucher_form" style="display:none;">
                            {include file='controllers/orders/_discount_form.tpl'}
                        </div>
                    {/if}
                </div>
                <div class="panel panel-total">
                    <div class="table-responsive">
                        <table class="table">
                            {* Assign order price *}
                            {if ($order->getTaxCalculationMethod() == $smarty.const.PS_TAX_EXC)}
                                {assign var=order_product_price value=($order->total_products)}
                                {assign var=order_discount_price value=$order->total_discounts_tax_excl}
                                {assign var=order_wrapping_price value=$order->total_wrapping_tax_excl}
                                {assign var=order_shipping_price value=$order->total_shipping_tax_excl}
                            {else}
                                {assign var=order_product_price value=$order->total_products_wt}
                                {assign var=order_discount_price value=$order->total_discounts_tax_incl}
                                {assign var=order_wrapping_price value=$order->total_wrapping_tax_incl}
                                {assign var=order_shipping_price value=$order->total_shipping_tax_incl}
                            {/if}
                            <tr id="total_products">
                                <td class="text-right">{l s='Products:'}</td>
                                <td class="amount text-right nowrap">
                                    {displayPrice price=$order_product_price currency=$currency->id}
                                </td>
                                <td class="partial_refund_fields current-edit" style="display:none;"></td>
                            </tr>
                            <tr id="total_discounts" {if $order->total_discounts_tax_incl == 0}style="display: none;"{/if}>
                                <td class="text-right">{l s='Discounts'}</td>
                                <td class="amount text-right nowrap">
                                    -{displayPrice price=$order_discount_price currency=$currency->id}
                                </td>
                                <td class="partial_refund_fields current-edit" style="display:none;"></td>
                            </tr>
                            <tr id="total_wrapping" {if $order->total_wrapping_tax_incl == 0}style="display: none;"{/if}>
                                <td class="text-right">{l s='Wrapping'}</td>
                                <td class="amount text-right nowrap">
                                    {displayPrice price=$order_wrapping_price currency=$currency->id}
                                </td>
                                <td class="partial_refund_fields current-edit" style="display:none;"></td>
                            </tr>
                            <tr id="total_shipping">
                                <td class="text-right">{l s='Shipping'}</td>
                                <td class="amount text-right nowrap">
                                    {displayPrice price=$order_shipping_price currency=$currency->id}
                                </td>
                                <td class="partial_refund_fields current-edit" style="display:none;">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            {$currency->prefix}
                                            {$currency->suffix}
                                        </div>
                                        <input type="text" name="partialRefundShippingCost" value="0"/>
                                    </div>
                                    <p class="help-block">
                                        <i class="icon-warning-sign"></i> {l s='(%s)' sprintf=$smarty.capture.TaxMethod}
                                    </p>
                                </td>
                            </tr>
                            {if ($order->getTaxCalculationMethod() == $smarty.const.PS_TAX_EXC)}
                                <tr id="total_taxes">
                                    <td class="text-right">{l s='Taxes'}</td>
                                    <td class="amount text-right nowrap">{displayPrice price=($order->total_paid_tax_incl-$order->total_paid_tax_excl) currency=$currency->id}</td>
                                    <td class="partial_refund_fields current-edit" style="display:none;"></td>
                                </tr>
                            {/if}
                            {assign var=order_total_price value=$order->total_paid_tax_incl}
                            <tr id="total_order">
                                <td class="text-right"><strong>{l s='Total'}</strong></td>
                                <td class="amount text-right nowrap">
                                    <strong>{displayPrice price=$order_total_price currency=$currency->id}</strong>
                                </td>
                                <td class="partial_refund_fields current-edit" style="display:none;"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none;" class="standard_refund_fields form-horizontal panel">
            <div class="form-group">
                {if ($order->hasBeenDelivered() && Configuration::get('PS_ORDER_RETURN'))}
                    <p class="checkbox">
                        <label for="reinjectQuantities">
                            <input type="checkbox" id="reinjectQuantities" name="reinjectQuantities"/>
                            {l s='Re-stock products'}
                        </label>
                    </p>
                {/if}
                {if ((!$order->hasBeenDelivered() && $order->hasBeenPaid()) || ($order->hasBeenDelivered() && Configuration::get('PS_ORDER_RETURN')))}
                    <p class="checkbox">
                        <label for="generateCreditSlip">
                            <input type="checkbox" id="generateCreditSlip" name="generateCreditSlip" onclick="toggleShippingCost()"/>
                            {l s='Generate a credit slip'}
                        </label>
                    </p>
                    <p class="checkbox">
                        <label for="generateDiscount">
                            <input type="checkbox" id="generateDiscount" name="generateDiscount" onclick="toggleShippingCost()"/>
                            {l s='Generate a voucher'}
                        </label>
                    </p>
                    <p class="checkbox" id="spanShippingBack" style="display:none;">
                        <label for="shippingBack"> <input type="checkbox" id="shippingBack" name="shippingBack"/>
                            {l s='Repay shipping costs'}
                        </label>
                    </p>
                    {if $order->total_discounts_tax_excl > 0 || $order->total_discounts_tax_incl > 0}
                        <br/>
                        <p>{l s='This order has been partially paid by voucher. Choose the amount you want to refund:'}</p>
                        <p class="radio">
                            <label id="lab_refund_total_1" for="refund_total_1">
                                <input type="radio" value="0" name="refund_total_voucher_off" id="refund_total_1" checked="checked"/>
                                {l s='Include amount of initial voucher: '}
                            </label>
                        </p>
                        <p class="radio">
                            <label id="lab_refund_total_2" for="refund_total_2">
                                <input type="radio" value="1" name="refund_total_voucher_off" id="refund_total_2"/>
                                {l s='Exclude amount of initial voucher: '}
                            </label>
                        </p>
                        <div class="nowrap radio-inline">
                            <label id="lab_refund_total_3" class="pull-left" for="refund_total_3">
                                {l s='Amount of your choice: '}
                                <input type="radio" value="2" name="refund_total_voucher_off" id="refund_total_3"/>
                            </label>
                            <div class="input-group col-lg-1 pull-left">
                                <div class="input-group-addon">
                                    {$currency->prefix}
                                    {$currency->suffix}
                                </div>
                                <input type="text" class="input fixed-width-md" name="refund_total_voucher_choose" value="0"/>
                            </div>
                        </div>
                    {/if}
                {/if}
            </div>
            {if (!$order->hasBeenDelivered() || ($order->hasBeenDelivered() && Configuration::get('PS_ORDER_RETURN')))}
                <div class="row">
                    <input type="submit" name="cancelProduct" value="{if $order->hasBeenDelivered()}{l s='Return products'}{elseif $order->hasBeenPaid()}{l s='Refund products'}{else}{l s='Cancel products'}{/if}" class="btn btn-default"/>
                </div>
            {/if}
        </div>
        <div style="display:none;" class="partial_refund_fields">
            <p class="checkbox">
                <label for="reinjectQuantitiesRefund">
                    <input type="checkbox" id="reinjectQuantitiesRefund" name="reinjectQuantities"/>
                    {l s='Re-stock products'}
                </label>
            </p>
            <p class="checkbox">
                <label for="generateDiscountRefund">
                    <input type="checkbox" id="generateDiscountRefund" name="generateDiscountRefund" onclick="toggleShippingCost()"/>
                    {l s='Generate a voucher'}
                </label>
            </p>
            {if $order->total_discounts_tax_excl > 0 || $order->total_discounts_tax_incl > 0}
                <p>{l s='This order has been partially paid by voucher. Choose the amount you want to refund:'}</p>
                <p class="radio">
                    <label id="lab_refund_1" for="refund_1">
                        <input type="radio" value="0" name="refund_voucher_off" id="refund_1" checked="checked"/>
                        {l s='Product(s) price: '}
                    </label>
                </p>
                <p class="radio">
                    <label id="lab_refund_2" for="refund_2">
                        <input type="radio" value="1" name="refund_voucher_off" id="refund_2"/>
                        {l s='Product(s) price, excluding amount of initial voucher: '}
                    </label>
                </p>
                <div class="nowrap radio-inline">
                    <label id="lab_refund_3" class="pull-left" for="refund_3">
                        {l s='Amount of your choice: '}
                        <input type="radio" value="2" name="refund_voucher_off" id="refund_3"/> </label>
                    <div class="input-group col-lg-1 pull-left">
                        <div class="input-group-addon">
                            {$currency->prefix}
                            {$currency->suffix}
                        </div>
                        <input type="text" class="input fixed-width-md" name="refund_voucher_choose" value="0"/>
                    </div>
                </div>
            {/if}
            <br/>
            <button type="submit" name="partialRefund" class="btn btn-default">
                <i class="icon-check"></i> {l s='Partial refund'}
            </button>
        </div>
    </div>
{/block}
